import shutil
from itertools import combinations, permutations
from rdf import objets
import json, os, random
import string
from nltk.corpus import stopwords

#les entrées seront en petite quantité si debug est à true
debug = False
verbose = True
dirname = "cs1s8-distinct_anonym_words_null-lex"


inputs = {}
outputs = {}
decoding_material = {}
lexicalisations = {}

csv_changement = []
csv_changement.append(";".join(["origine", "destination", 'diffAB', 'diffBA']))
#les entrées vont être très proches, et on veut les mêmes données d'exemple pour chaque modèle testé.
random_lines = {}

if not os.path.isdir(dirname + "/data semantic"):
    os.makedirs(dirname + "/data semantic")

def write(filename: str, data, dir, semantic=''):
    '''
    Vise l'écriture d'un fichier d'apprentissage
    :param filename: nom souhaité
    :param data: tableau des lignes à enregistrer
    :param dir : répertoire des fichiers (sous-réperoire de dirname/)
    :param semantic: données permettant le décodage ligne à ligne des résulats
    :return:
    '''
    if not os.path.isdir(dirname + "/" + dir):
        os.makedirs(dirname + "/" + dir)
    # si jamais un fichier train existe on sort. On ne va pas remplacer du travail.
    if os.path.isfile(dirname + "/" + dir + "/training_results/data-train.t7"):
        print(dir + " in progress, not rewritten.")
    else:
        fjson = open(dirname + "/" + dir + '/' + filename, "w")
        fjson.write(("\n".join(data)).replace("  ", " "))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        if verbose:
            print('written '+ os.path.abspath(dirname + "/" + dir + '/' + filename))

        if semantic != '':
            if not os.path.isdir(dirname + "/" + dir + "/data semantic"):
                os.makedirs(dirname + "/" + dir + "/data semantic")
            with open(dirname + "/" + dir + "/data semantic/" + filename, "w") as fjson:
                json.dump(semantic, fjson, ensure_ascii=False, indent=2)
    return os.path.abspath(dirname + "/" + dir + '/' + filename)


if not os.path.isdir(dirname + "/data semantic"):
    os.makedirs(dirname + "/data semantic")

def writeJSON(filename: str, data: []):
    with open(dirname + "/data semantic/" + filename, "w") as fjson:
        json.dump(data, fjson, ensure_ascii=False, indent=2)

def loadJSON(filename: str, type: type):
    if os.path.isfile(dirname + "/data semantic/" + filename):
        with open(dirname + "/data semantic/" + filename, 'r') as f:
            json_data = json.load(f)
        return json_data
    else:
        if type == dict:
            return {}
        else:
            return []

# dictionnaire = loadJSON("dictionnaire.json", dict)
dictionnaire = {}

files =["valid", "test", "train"]
testBLEU = {}
testMonoBLEU = {}
testDuoBLEU = {}

# concevons maintenant nos fichiers par cible d'usage.

# pour stocker la longueur maximale des séquences
src_max_length = {}
tgt_max_length = {}

for file in files:
    materiel = {}

    if verbose:
        print('fichier ' + file + '[src/tgt] amorcé')

    xml = objets.XML("../prepareCorpus/webnlgparser_data/working_data_"+file+".xml", dirname)

    for package in xml.parseData():
        entrée_logique = package[0][0]
        lexicalisation = package[1][0]
        contraintes = package[2]
        dictionnairesLexicalisations = package[3]

        modele = str(entrée_logique.__hash__()) + "-" + str(lexicalisation.__hash__())

        if modele not in materiel.keys():
            materiel[modele] = {}

        if entrée_logique.value not in materiel[modele].keys():
            materiel[modele][entrée_logique.value] = [[], [], [], [], []]
        materiel[modele][entrée_logique.value][0].append(lexicalisation.value)

        tab_contrainte = []
        for contrainte in contraintes:
            if contrainte.positive:
                tab_contrainte.append(contrainte.name.replace(' ', '_'))
        if len(tab_contrainte) == 0:
            tab_contrainte.append('sans_contrainte')
        materiel[modele][entrée_logique.value][1].append(set(tab_contrainte))


        materiel[modele][entrée_logique.value][2].append({"clé fichier": modele,
                                                 'entrées': entrée_logique.toDic(),
                                                 'count': 0,
                                                 'sorties': lexicalisation.toDic()})

        tmp_interprétation_material = {}
        for entrée_dic in dictionnairesLexicalisations:
            tmp_interprétation_material[entrée_dic.code if entrée_dic.id is None else entrée_dic.id] = entrée_dic.value

        materiel[modele][entrée_logique.value][3].append(tmp_interprétation_material)
        materiel[modele][entrée_logique.value][4].append(xml.corpusLexicalisation.sentences(lexicalisation.entrée_originelle,
                                                lexicalisation.encodage.value.__str__(), 3,
                                                lexicalisation.value.replace("  ", " ")))
        if debug and len(materiel[modele]) > 100:
            break

    for test in ['add_']:
        for modele in materiel.keys():
            test = test + modele
            if test not in inputs.keys():
                inputs[test] = {}
                outputs[test] = {}
                decoding_material[test] = {}
                lexicalisations[test] = {}
                random_lines[test] = {}
            if file not in inputs[test].keys():
                inputs[test][file] = []
                outputs[test][file] = []
                decoding_material[test][file] = []
                lexicalisations[test][file] = []
                random_lines[test][file] = []
            for entrée_logique in materiel[modele].keys():

                i_tab = []
                if len(materiel[modele][entrée_logique][1]) > 1:
                    for i in range(len(materiel[modele][entrée_logique][1])):
                        i_tab.append(i)
                tab_possibilités = permutations(i_tab, 2)
                tab_possi = [k_pos for k_pos in tab_possibilités]
                random.shuffle(tab_possi)

                for poss in tab_possi:
                    entrée = materiel[modele][entrée_logique][0][poss[1]]

                    sortie = materiel[modele][entrée_logique][0][poss[0]]
                    dico = materiel[modele][entrée_logique][3][poss[0]]
                    lexicalisations_sortie = materiel[modele][entrée_logique][4][poss[0]]

                    #for code in dico.keys():
                    #    if "_" in code:
                     #       entrée = entrée.replace(code.strip(), dico[code].strip().replace(" ", "_"))
                     #       sortie = sortie.replace(code.strip(), dico[code].strip().replace(" ", "_"))

                    tmp1 = sortie[:]
                    tmp2 = entrée[:]

                    for data in dico.keys():
                        tmp1 = tmp1.replace(data, " ").replace("  ", " ")
                        tmp2 = tmp2.replace(data, " ").replace("  ", " ")
                    for ponct in string.punctuation:
                        tmp1 = tmp1.replace(ponct, " ").replace("  ", " ")
                        tmp2 = tmp2.replace(ponct, " ").replace("  ", " ")
                    phrase_a = set(tmp1.lower().strip().split(" "))
                    phrase_b = set(tmp2.lower().strip().split(" "))


                    stop = set(stopwords.words('english'))
                    phrase_a = phrase_a - stop
                    phrase_b = phrase_b - stop

                    diffAB = phrase_a - phrase_b
                    diffBA = phrase_b - phrase_a

                    #diff = []
                    relations = materiel[modele][entrée_logique][1][poss[0]] - materiel[modele][entrée_logique][1][poss[1]]
                    relationsinv = materiel[modele][entrée_logique][1][poss[1]] - materiel[modele][entrée_logique][1][poss[0]]
                    if len(relations) == 1 and len(relationsinv) == 1:
                        csv_changement.append(";".join([list(relations)[0], list(relationsinv)[0], len(diffAB).__str__(), len(diffBA).__str__()]))

                    for diff in relations:

                     #   diff.append(a.strip())
                    #diff = " ".join(sorted(diff))

                        if len(relations) == 1 and len(relationsinv) == 1 and len(diffAB) == 0:
                            inputs[test][file].append(list(relations)[0].strip() + " " + list(relationsinv)[0].strip() + " " + entrée)
                            outputs[test][file].append(sortie)
                            lexicalisations[test][file].append(lexicalisations_sortie)
                            decoding_material[test][file].append(dico)
                            random_lines[test][file].append(len(inputs[test][file])-1)

    ls_keys = list(inputs.keys())

    for cle in ls_keys:
        #file et l'ordonnancement dans file permet de calculé la table max de la seq, c'est pour cela qu'on fait le traitement à ce niveau d'imbrication
        if verbose:
            print('fichier ' + file + '[src/tgt] pré-traité pour étape '+cle+', génération fichier ' + cle)
        # on shuffle pour que les données ne se suivent pas
        # si et seulement si on est sur le train, c'est plus sympa de regarder l'impact des contraintes linéairement

        #on initialise nos longueurs de chaînes
        if cle not in src_max_length.keys():
            src_max_length[cle] = 0
            tgt_max_length[cle] = 0

        dataset = [[], [], []]
        BLEUset = [[], [], []]

        if file == 'train' :
            # je validerai sur 10% de mon corpus d'apprentissage, et à la fin, évaluerai sur test_src pour évaluer le bleu sur corpus non vu
            random.shuffle(random_lines[cle]['valid'])

            for each in random_lines[cle]['valid']:
                dataset[0].append(inputs[cle]['valid'][each])
                dataset[1].append(outputs[cle]['valid'][each])
                dataset[2].append(decoding_material[cle]['valid'][each])
                BLEUset[0].append(lexicalisations[cle]['valid'][each][0])
                BLEUset[1].append(lexicalisations[cle]['valid'][each][1])
                BLEUset[2].append(lexicalisations[cle]['valid'][each][2])

                len_input = len(inputs[cle]['valid'][each].split(" "))
                len_output = len(outputs[cle]['valid'][each].split(" "))
                if len_input > src_max_length[cle]:
                    src_max_length[cle] = len_input
                if len_output > tgt_max_length[cle]:
                    tgt_max_length[cle] = len_output

        if file == "train":
            random.shuffle(random_lines[cle][file])

        for each in random_lines[cle][file]:
            if file=='train':
                position = random.randint(0, len(dataset[0])-1)
            else:
                position = -1
            dataset[0].insert(position, inputs[cle][file][each])
            dataset[1].insert(position, outputs[cle][file][each])
            dataset[2].insert(position, decoding_material[cle][file][each])
            BLEUset[0].insert(position, lexicalisations[cle][file][each][0])
            BLEUset[1].insert(position, lexicalisations[cle][file][each][1])
            BLEUset[2].insert(position, lexicalisations[cle][file][each][2])

            len_input = len(inputs[cle][file][each].split(" "))
            len_output = len(outputs[cle][file][each].split(" "))
            if len_input > src_max_length[cle]:
                src_max_length[cle] = len_input
            if len_output > tgt_max_length[cle]:
                tgt_max_length[cle] = len_output


        f0 = write(file + "_src", dataset[0], cle, dataset[2])
        f1 = write(file + "_tgt", dataset[1], cle)
        f2 = write(file + "_references_1.lex", BLEUset[0], cle)
        f3 = write(file + "_references_2.lex", BLEUset[1], cle)
        f4 = write(file + "_references_3.lex", BLEUset[2], cle)

        # réalisons en passant les scripts pour multiBLEU
        sep = ">"
        if file != 'valid':
            if cle not in testBLEU.keys():

                testBLEU[cle] = []
                testBLEU[cle].append("export TEST=\"training_results/data_test_predictions.txt\"")
                testBLEU[cle].append("export TRAIN=\"training_results/data_train_predictions.txt\"")
                testDuoBLEU[cle] = []
                testDuoBLEU[cle].append("export TEST=\"training_results/data_test_predictions.txt\"")
                testDuoBLEU[cle].append("export TRAIN=\"training_results/data_train_predictions.txt\"")
                testMonoBLEU[cle] = []
                testMonoBLEU[cle].append("export TEST=\"training_results/data_test_predictions.txt\"")
                testMonoBLEU[cle].append("export TRAIN=\"training_results/data_train_predictions.txt\"")

            testBLEU[cle].append("export HYPOTHESIS=\"" + f2 + '"')
            testMonoBLEU[cle].append("export HYPOTHESIS=\"" + f2 + '"')
            testDuoBLEU[cle].append("export HYPOTHESIS=\"" + f2 + '"')
            testMonoBLEU[cle].append("export " + file.upper() + "REF0=\"" + f1 + '"')
            testBLEU[cle].append("export " + file.upper() + "REF0=\"" + f2 + '"')
            testBLEU[cle].append("export " + file.upper() + "REF1=\"" + f3 + '"')
            testDuoBLEU[cle].append("export " + file.upper() + "REF0=\"" + f2 + '"')
            testDuoBLEU[cle].append("export " + file.upper() + "REF1=\"" + f3 + '"')
            testBLEU[cle].append("export " + file.upper() + "REF2=\"" + f4 + '"')
            testBLEU[cle].append('../multi-bleu.perl ${' + file.upper() + 'REF0} ${' + file.upper() + 'REF1} ${' + file.upper() + 'REF2}  < ${HYPOTHESIS} ' + sep + ' bleu_' + file + '_references.txt')
            testBLEU[cle].append('../multi-bleu.perl ${' + file.upper() + 'REF0} ${' + file.upper() + 'REF1} ${' + file.upper() + 'REF2}  < ${' + file.upper() + '} ' + sep + ' bleu_' + file + '.txt')
            testDuoBLEU[cle].append('../multi-bleu.perl ${' + file.upper() + 'REF0} ${' + file.upper() + 'REF1} < ${' + file.upper() + '} ' + sep + ' duo_bleu_' + file + '.txt')
            testMonoBLEU[cle].append('../multi-bleu.perl ${' + file.upper() + 'REF0} < ${' + file.upper() + '} ' + sep + ' mono_bleu_' + file + '.txt')



        # Nous allons maintenant générer les données d'évaluation

        # on clôt le xml, ce qui provoque la sauvegarde des lexicalisations possibles.
        xml.close()
if verbose:
    print('génération scripts d\'apprentissage')
# générons maintenant nos script d'apprentissage
big_gpu_script = []

for key in inputs.keys():
    #on va mettre un descriptif du fichier dans chaque répertoire
    if not os.path.isfile(dirname + "/" + key + "/training_results/data.train.1/pt"):
        if not os.path.isdir(dirname + "/" + key + "/training_results"):
            os.makedirs(dirname + "/" + key + "/training_results")
        for value in range(2):
            script = []
            script.append('# prepare vocab ')
            script.append("cd ~/OpenNMT-py")
            script.append("python3 preprocess.py " +
                          " -train_src " + os.path.abspath(dirname + "/" + key + '/train_src') +
                          " -train_tgt " + os.path.abspath(dirname + "/" + key + '/train_tgt') +
                          " -valid_src " + os.path.abspath(dirname + "/" + key + '/valid_src') +
                          " -valid_tgt " + os.path.abspath(dirname + "/" + key + '/valid_tgt') +
                          " -src_seq_length " + src_max_length[key].__str__() +
                          " -tgt_seq_length " + tgt_max_length[key].__str__() +
                          " -save_data " + os.path.abspath(dirname + "/" + key + "/training_results/data"))
            script.append("THC_CACHING_ALLOCATOR=1 python3 train.py  -encoder_type brnn -global_attention general -epochs 25 " +
                          " -data " + os.path.abspath(dirname + "/" + key) + "/training_results/data" +
                          " -save_model " + os.path.abspath(dirname + "/" + key + "/training_results/data")+
                          " -gpuid 0 ")

            script.append("ls -R " + os.path.abspath(dirname + "/" + key) + "/training_results/ > " + os.path.abspath(dirname + "/" + key) + "/" + key + "-epochs.txt")

            for epoch in range(16):
                script.append("rm "+ os.path.abspath(dirname + "/" + key) + "/training_results/data_acc_*_e" + (epoch+1).__str__() + ".pt7")

            script.append("python3 translate.py   "+
                              " -model " + os.path.abspath(dirname + "/" + key + "/training_results/") + "/data_acc_*_e25.pt " +
                              " -src  " + os.path.abspath(dirname + "/" + key + '/train_src') +
                              " -output " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_train_predictions.txt " +
                              "  -verbose -gpu 0")

            script.append("python3 translate.py   " +
                              " -model " + os.path.abspath(dirname + "/" + key + "/training_results/") + "/data_acc_*_e25.pt " +
                              " -src  " + os.path.abspath(dirname + "/" + key + '/test_src') +
                              " -output " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_test_predictions.txt " +
                              "  -verbose -gpu 0")

            script.append("")
            write("OpenNMT-gpu-py.bash", script, key)
            big_gpu_script.append("bash " + os.path.abspath(dirname + "/" + key + "/" + "OpenNMT-gpu-py.bash"))

    elif os.path.isfile(dirname + "/" + key + "/training_results/data_train_predictions.txt"):
        print(key + " - train in progress or done.")
    elif os.path.isfile(dirname + "/" + key + "/training_results/data_epoch25_valid_translation.txt"):
        print(key + " - train in progress.")
    else:
        print(key + " in progress.")

big_gpu_script.append('')
write("OpenNMT-gpu-py.sh", big_gpu_script, '')

# sauvegardons les données générales...
writeJSON("dictionnaire.json", dictionnaire)

#Enregistrons les scripts pour les bleus:

if verbose:
    print('génération scripts des BLEU')

launchable = []
for key in testBLEU.keys():
    if launchable == []:
        launchable.append("#!/bin/bash")
        launchable.append(' cd ' + key)
    else:
        launchable.append(' cd ../' + key)
    launchable += testBLEU[key]
write("calculer_BLEUs.bash", launchable, '')


launchable = []
for key in testMonoBLEU.keys():
    if launchable == []:
        launchable.append("#!/bin/bash")
        launchable.append(' cd ' + key)
    else:
        launchable.append(' cd ../' + key)
    launchable += testMonoBLEU[key]
write("calculer_mono_BLEUs.bash", launchable, '')



launchable = []
for key in testDuoBLEU.keys():
    if launchable == []:
        launchable.append("#!/bin/bash")
        launchable.append(' cd ' + key)
    else:
        launchable.append(' cd ../' + key)
    launchable += testDuoBLEU[key]
write("calculer_duo_BLEUs.bash", launchable, '')

write("csv_changement.csv", csv_changement, '')