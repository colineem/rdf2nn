from xml.etree import ElementTree
import pygraphviz as pgv
from enum import Enum
import difflib
from nltk.stem.porter import PorterStemmer
from itertools import groupby
import os
import json
import statistics
v2 = True
v3 = False
v4 = False
acteurs_relation = {}
objets_relation = {}

class SaveObject():

    def __init__(self, dirname:str):
        self.dirname = dirname

        if not os.path.isdir(self.dirname + "/data semantic"):
            os.makedirs(self.dirname +"/data semantic")


    def writeJSON(self, filename: str, data: []):
        with open(self.dirname + "/data semantic/" + filename, "w") as fjson:
            json.dump(data, fjson, ensure_ascii=False, indent=2)


    def loadJSON(self, filename: str, type: type):
        if os.path.isfile(self.dirname + "/data semantic/" + filename):
            with open(self.dirname + "/data semantic/" + filename, 'r') as f:
                json_data = json.load(f)
            return json_data
        else:
            if type == dict:
                return {}
            else:
                return []


# https://swizec.com/blog/measuring-vocabulary-richness-with-python/swizec/2528
# Yule’s K characteristic (or rather its inverse, Yule’s I)
def words(entry):
    return filter(lambda w: len(w) > 0, [w.strip("0123456789!:,.?(){}[]") for w in entry.split()])

def yule(entry):
    # yule's I measure (the inverse of yule's K measure)
    # higher number is higher diversity - richer vocabulary
    d = {}
    stemmer = PorterStemmer()
    for w in words(entry):
        w = stemmer.stem(w).lower()
        try:
            d[w] += 1
        except KeyError:
            d[w] = 1

    M1 = float(len(d))
    M2 = sum([len(list(g)) * (freq ** 2) for freq, g in groupby(sorted(d.values()))])

    try:
        return (M1 * M1) / (M2 - M1)
    except ZeroDivisionError:
        return 0


class LearningItem:
    '''
    Permet de construire des objets
    '''

    class Type(Enum):
        '''
        Codification Acteur/Prédicat
        '''
        ENTREE = 0
        SORTIE = 1

    def __init__(self, nom: str, valeur: str, exploitation_type: Type):
        self.nom = nom
        self.valeur = valeur
        self.exploitation_type = exploitation_type


class LexicalisationItem:
    '''
    LexicalisationItem peut être soit un Acteur, soit un Prédicat
    '''

    class Type(Enum):
        '''
        Codification Acteur/Prédicat
        '''
        ACTEUR = 0
        PREDICAT = 1

    def __init__(self, type: Type, code: str, value: str, id: str = None):
        '''

        :param type: code acteur/prédicat
        :param code: représentation logique de l'item issue du corpus, d'un pré-parsing
        :param value: valeur textuelle
        :param id: identifiant généré (a** pour un acteur, r** pour un prédicat)
        '''
        self.type = type
        self._code = code
        self._value = value
        self._id = id

    def __hash__(self) -> int:
        '''
        Hash de l'idem
        :return: hash du code pour un prédicat, du code et de la valeur pour un acteur
        '''
        if self.type == self.Type.PREDICAT:
            return hash(self._code)
        else:
            return hash(self._code + self._value)

    def __eq__(self, o: object) -> bool:
        '''
        Re-définition de la fonction d'égalité
        :param o: objet à comparer
        :return: True si equal
        '''
        return isinstance(o, LexicalisationItem) and \
               ((self.type == self.Type.PREDICAT and o._code == self._code) or \
                (self.type == self.Type.ACTEUR and o._code == self._code and o._value == self._value))

    def __ne__(self, o: object) -> bool:
        '''
        Redéfinition de la fonction d'inégalité
        :param o: objet à comparer
        :return: True si inégal
        '''
        if isinstance(o, LexicalisationItem):
            return not (o.__eq__(self))
        else:
            return True

    @property
    def id(self) -> str:
        '''
        Identifiant de l'objet
        :return: identifiant pré-fixé par a si acteur, par r si prédicat
        '''
        return self._id

    @property
    def code(self) -> str:
        '''
        code logique issu du parsing
        :return: code identifiant l'objet
        '''
        return self._code

    @property
    def value(self) -> str:
        '''
        valeur de l'objet
        :return: la valeur textuelle de l'item
        '''
        return self._value

    def force_id(self, id):
        '''
        Ré-écriture de l'identifiant
        :param id: identifiant à placer de force
        :return: void
        '''
        self._id = id

    def __str__(self):
        if self.type == self.Type.PREDICAT:
            return self.type.name + " : " + self.value + " codifié " + self.code
        else:
            return self.type.name + " : " + self.value + " codifié " + self.id + "(" + self.code + ")"


class Gestionnaire:
    '''
    Gestionnaire de groupes d'acteurs
    '''

    def __init__(self):
        '''
        Création d'un nouveau set
        '''
        self.__groupe = set()

    @property
    def compteur(self) -> int:
        '''
        Taille du groupe d'acteurs
        :return: taille du groupe
        '''
        return len(self.__groupe)

    def add(self, object: object):
        '''
        Ajot d'un objet dans le groupe
        :param object: Objet, théoriquement, mais non vérifié, Acteur
        :return: void
        '''
        self.__groupe.add(object)

    @property
    def groupe(self) -> set:
        '''
        Set d'objets
        :return: le groupe
        '''
        return self.__groupe


class Acteur:
    '''
    Un acteur est sujet ou objet d'un prédicat. Son identifiant est préfixé par a.
    '''

    def __init__(self, gestionnaire: Gestionnaire, nom: str, code: str = "", value:str= "", id:str=""):
        '''
        :param gestionnaire: gestionnaire de l'acteur pour le lot de triplets considéré
        :param nom: nom de l'acteur
        :param code: code de l'acteur
        '''
        if gestionnaire is None:
            self.gestionnaire = None
            self._nom = nom
            self._code = code
            self._value = value
            self.value = value
            self._id = id
            self.id = id
            self._identifiant = id
            self.type = LexicalisationItem.Type.ACTEUR
        else:
            self.gestionnaire = gestionnaire
            # on crée un acteur fantôme
            self._nom = nom
            self._id = id
            if code == "":
                self._code = nom
            else:
                self._code = code
            if self.gestionnaire.groupe.__contains__(self):
                # a priori obligé d'itérer
                for acteur in self.gestionnaire.groupe:
                    if acteur == self:
                        # petit souci de cache python semble-t-il
                        # forçage valeurs
                        self._identifiant = acteur.identifiant
                        self._nom = acteur.nom
                        self._code = acteur.code
                        self = acteur
            else:
                self._identifiant = "a" + str(self.gestionnaire.compteur.__str__()).zfill(2) + " "
                self.gestionnaire.add(self)

    @property
    def identifiant(self) -> str:
        return self._identifiant

    @property
    def nom(self) -> str:
        return self._nom

    @property
    def code(self) -> str:
        return self._code

    @property
    def lexicalisationItem(self) -> LexicalisationItem:
        return LexicalisationItem(LexicalisationItem.Type.ACTEUR, self._code, self._nom, self.identifiant)

    @nom.setter
    def nom(self, nom: str):
        self._nom = nom

    def __hash__(self) -> int:
        return hash(self._code)

    def __eq__(self, o: object) -> bool:
        return isinstance(o, Acteur) and o.code == self.code

    def __ne__(self, o: object) -> bool:
        if isinstance(o, Acteur):
            return not (o.code == self.code)
        else:
            return True

class Prédicat:
    '''
    Définition d'un prédicat avec génération automatique d'un identifiant qui sera réexploité d'un lot de triplets à l'autre et pré-fixé par "r"
    '''

    __compteur = 0
    __groupe = {}

    def __init__(self, nom):
        '''
        Création d'un nouveau prédicat ou ré-exploitation d'un prédicat précédemment créé.
        :param nom: Nom du prédicat
        '''
        self._nom = nom
        if nom in Prédicat.__groupe.keys():
            self._identifiant = Prédicat.__groupe[self.nom]
        else:
            self._identifiant = "r" + str(Prédicat.__compteur).zfill(5) + " "
            Prédicat.__compteur += 1
            Prédicat.__groupe[self.nom] = self._identifiant

    @property
    def identifiant(self) -> str:
        '''
        Identifiant
        :return: identifiant du prédicat (r*)
        '''
        return self._identifiant

    @property
    def nom(self) -> str:
        '''
        Nom du prédicat
        :return: le nom
        '''
        return self._nom

    # une s'identifie par son identifiant
    def __hash__(self) -> int:
        '''
        Fonction de hachage
        :return: hash du nom
        '''
        return hash(self._nom)

    def __eq__(self, o: object) -> bool:
        '''
        Re-défintion de l'égalité : égalité sur le nom
        :param o: objet à comparer
        :return: True si égal
        '''
        return isinstance(o, Prédicat) and o._nom == self._nom

    def __ne__(self, o: object) -> bool:
        '''
        Re-définition de non-equal : à objet de même type, la différence se fait sur le nom.
        :param o: objet à comparer
        :return: True si l'objet à comparer est différent
        '''
        if isinstance(o, Prédicat):
            return not (o._nom == self._nom)
        else:
            return True

    @property
    def lexicalisationItem(self) -> LexicalisationItem:
        '''
        :return: une copie détachée du prédicat sous forme de lexicalisation
        '''
        return LexicalisationItem(LexicalisationItem.Type.PREDICAT, self._identifiant, self._nom)

class Error(Enum):
    '''
    Classe erreur : Permet de typer les erreurs rencontrées pendant le parsinf
    '''

    NO_ERROR = 0
    AUTO_REFERENCE = 1
    DISJONCTION = 2
    CYCLE = 3

    def __str__(self):
        '''
        Retourne une chaîne de caractère étiquetant l'erreur.
        :return: un libellé textuel pour l'erreur
        '''
        return '{0}'.format(self.name)

class XML:
    '''
    Classe gérant le parsing des données...
    '''

    class Info():
        """
                Classe gérant une information d'entrée-sortie
        """

        class LevelAnonymisation(Enum):
            """
            Classe LevelAnonymisation : Permet de décrire la transmission de l'information.
             Masquée elle est portée par la structure,
             Encodée elle est transmise sous forme relativement anonyme,
             Transmise non encodée :  les mots sont en anglais, séparés fussent-ils plusieurs pour une entité par des espaces.
             Transmise régulée :une entité = un mot fusse-t-il composé de parts reliées par un underscore,
            """
            MASQUAGE = 0
            ENCODAGE_SIMPLE = 1
            TRANSMISSION_NON_ENCODÉE = 2
            TRANSMISSION_RÉGULÉE = 3

        class Encodage(Enum):
            """
            Classe décrivant le type d'encodage
            """
            AUCUN = 0
            ACTEUR = 1
            RELATION = 2
            ACTEUR_RELATION = 3
            ACTEUR_À_RÔLE_PRÉFIXÉ = 4
            ACTEUR_À_RÔLE_PRÉFIXÉ_RELATION = 5
            FLUX = 6

        class Structure(Enum):
            """
            Classe décrivant la structure de l'information
            """
            LINÉAIRE = 0
            ARBORESCENCE_STRUCTURÉE_PARENTHÉSÉE = 1
            ARBORESCENTE_STRUCTURÉE_PARENTHÉSÉE_RELATIVE = 2  # non utilisée, rebaptisable et exploitable à souhait
            ARBORESCENTE_PARENTHÉSÉE = 3
            ARBORESCENTE_INFORMÉE_CONCISE = 4
            ARBORESCENTE_INFORMÉE_REDONDANTE = 5
            LINÉAIRE_MÉCANIQUEMENT_PARENTHÉSÉE = 6
            ARBORESCENTE_STRUCTURÉE_PARENTHÉSÉE_RELATIVE_COMPLEXE= 7

        class Parenthèses(Enum):
            """
            Classe décrivant le type de parenthèsage:
                - TYPES = différenciée par type ouvrante/fermante
                - niveaux = différenciée par niveaux (selon sa place dans une hiérarchie parent/enfant : ie +1 si première parenthèse, +2 si seconde)
            """
            TYPE = 0
            NIVEAUX = 1
            TYPE_ET_NIVEAUX = 2

        def __init__(self, value: str, input: bool, output: bool, anonymisation: LevelAnonymisation, encodage: Encodage,
                     structure: Structure, triplets_originels: str):
            """

            :type output: bool
            :type input: bool
            """
            self.__output = output
            self.__input = input
            self.__value = value
            self.__anonymisation = anonymisation
            self.__encodage = encodage
            self.__structure = structure
            self.__descriptionStructure = ""
            self.__triplets_linéarisés_originels = triplets_originels

        def __str__(self):
            if self.__descriptionStructure == "":
                return (
                            "Anonymisation " + self.__anonymisation.name.lower() + ", encodage " + self.__encodage.name.lower() \
                            + ", structure " + self.__structure.name.lower()).replace("_", " ")
            else:
                return (
                            "Anonymisation " + self.__anonymisation.name.lower() + ", encodage " + self.__encodage.name.lower() \
                            + ", structure " + self.__structure.name.lower() + " (" + self.__descriptionStructure.name + ")").replace(
                    "_", " ")

        def toDic(self):
            a = {'entrée': self.isInput,
                 'sortie': self.isOutput,
                 'exemple': self.__value[:],
                 'anonymisation': self.__anonymisation.name,
                 'encodage': self.__encodage.name,
                 'structure': self.__structure.name}
            if isinstance(self.__descriptionStructure, self.Parenthèses):
                a['parenthésage'] = self.__descriptionStructure.name
            else:
                a['parenthésage'] = ""
            return a

        @property
        def typeParenthèse(self):
            return self.__descriptionStructure

        @property
        def isInput(self):
            return self.__input

        @property
        def encodage(self):
            return self.__encodage

        @property
        def anonymisation(self):
            return self.__anonymisation

        @property
        def entrée_originelle(self):
            return self.__triplets_linéarisés_originels

        @property
        def isOutput(self):
            return self.__output

        @typeParenthèse.setter
        def typeParenthèse(self, parenthèse: Parenthèses):
            self.__descriptionStructure = parenthèse

        @property
        def value(self):
            return self.__value

        def __hash__(self) -> int:
            i = str(self.__anonymisation.value) + str(self.__encodage.value) + str(self.__structure.value)
            if isinstance(self.__descriptionStructure, self.Parenthèses):
                i += str(self.__descriptionStructure.value)
            i += str(int(self.isInput)) + str(int(self.isOutput))
            return int(i)

    class corpusBLEU():

        def __init__(self, saveObjet:SaveObject):
            self.__saveObjet = saveObjet
            self.__variations = self.__saveObjet.loadJSON('lexicalisations.json', dict)
            self.__last_item = ""

            for key in self.__variations.keys():
                for encodage in self.__variations[key].keys():
                    self.__variations[key][encodage] = set(self.__variations[key][encodage])

        def __close__(self):
            for key in self.__variations.keys():
                for encodage in self.__variations[key].keys():
                    self.__variations[key][encodage] = list(self.__variations[key][encodage])
            self.__saveObjet.writeJSON('lexicalisations.json', self.__variations)

        def sentences(self, triplet_linéraire: str, encodage_dans_la_sortie: str, filter: int, lex_initiale = ''):
            '''
            Récupération des lexicalisations liées à un triplets
            :param triplet_linéraire: set des lexicalisations
            :param encodage_dans_la_sortie : la valeurconvertie en string de l'encodage présent dans la sortie
            :param filter : le nombre de lexilications à fournir (-1 pour toutes)
            :param lex_initiale : s'il y a une lexicalisation qu'on sait vouloir prendre en compte...
            :return:
            '''
            lexicalisations = set()
            if lex_initiale != '':
                lexicalisations.add(lex_initiale)
            for encodage_saved, set_lex in self.__variations[triplet_linéraire].items():
                if encodage_saved == encodage_dans_la_sortie:
                    lexicalisations.update(set_lex)
            if filter <= 0:
                return list(lexicalisations)
            elif  len(lexicalisations) <= filter:
                liste = []
                liste.append(lex_initiale)
                lexicalisations.remove(lex_initiale)
                liste += list(lexicalisations)
                return (liste + [""]*filter)[0:filter]
            else:
                filtered_lexicalisations = []
                dico_diff = {}
                if lex_initiale != '':
                    filtered_lexicalisations.append(lex_initiale)
                    lexicalisations.remove(filtered_lexicalisations[-1])
                for lex in lexicalisations:
                    dico_diff[lex] = yule(lex)
                if len(dico_diff) > 0 and len(filtered_lexicalisations) < filter:
                    filtered_lexicalisations.append(sorted(dico_diff.items(), key=lambda x: x[1], reverse=True)[0][0])
                    lexicalisations.remove(filtered_lexicalisations[-1])

                while len(filtered_lexicalisations) < filter:
                    dico_diff = {}
                    for seq1 in filtered_lexicalisations:
                        for seq2 in lexicalisations:
                            if seq2 not in dico_diff.keys():
                                dico_diff[seq2] = []
                            dico_diff[seq2].append(difflib.SequenceMatcher(a=seq1, b=seq2).ratio())
                    for key in dico_diff.keys():
                        dico_diff[key] = statistics.median(dico_diff[key])

                    if len(dico_diff) > 0:
                        filtered_lexicalisations.append(sorted(dico_diff.items(), key=lambda x: x[1])[0][0])
                    lexicalisations.remove(filtered_lexicalisations[-1])
                    if len(lexicalisations)  == 0:
                        while len(filtered_lexicalisations) < filter:
                            filtered_lexicalisations.append("")


                return filtered_lexicalisations

        def createItem(self, triplet_linéaire: str):
            '''
            Crée une entrée au corpus BLEU
            :param triplet_linéaire: le triplet type dont on ajoute la gestion, le tableau des variations des entrées
            :return: le dic des lexicalisations associé au triplet
            '''
            if triplet_linéaire not in self.__variations.keys():
                self.__variations[triplet_linéaire] = {}
            self.__last_item = triplet_linéaire

        def add_lexicalisation(self, lexicalisation: str, encodage_dans_la_sortie):
            es = encodage_dans_la_sortie.value.__str__()
            if not es in self.__variations[self.__last_item].keys():
                self.__variations[self.__last_item][es] = set()
            self.__variations[self.__last_item][es].add(lexicalisation.replace("  ", " "))

    class Contrainte():
        """
        Contrainte qui peut être présente (positive=True) ou absente (negative=True)
        """

        def __init__(self, name: str, present: bool):
            self.__name = name
            self.__present = present

        def __eq__(self, o: object) -> bool:
            if isinstance(o, XML.Contrainte):
                if self.__present == o.__present:
                    if self.__name == o.__name:
                        return True
            return False

        def __ne__(self, o: object) -> bool:
            return not self.__eq__(o)

        def __str__(self) -> str:
            if self.__present:
                return self.__name.replace(' ', '_')
            else:
                return "not_" + self.__name.replace(' ', '_')

        @property
        def name(self):
            return self.__name

        @property
        def positive(self):
            return self.__present

        @property
        def negative(self):
            return not self.__present

    def __init__(self, path: str, save_dirname:str):
        self.path = path
        self.data = []
        from datetime import date
        self.log = date.today().__str__() + "\n"
        self.__count_errors = 0
        self.__count_ok = 0
        self.__type_errors = {}
        self.__corpus_BLEU = self.corpusBLEU(SaveObject(save_dirname))


    def getLinearValue(self, graph: pgv.AGraph, nbunch: {}):

        def treatedNbunch(nbunch: []):
            parents = {}
            output = ''
            for node in nbunch:
                parent = graph.predecessors(node)
                if len(parent) > 1:
                    return ""
                elif not parent[0] in parents.keys():
                    parents[parent[0]] = []
                parents[parent[0]].append(node)
            s = [(k, parents[k]) for k in sorted(parents, key=parents.get)]
            sep = ""
            keys = []
            for key, value in s:
                keys.append(key)
                output += sep + "".join(value)
                sep = "$"
            output += "#"
            return keys, output

        tab = []
        bunches = sorted(list(nbunch.keys()), reverse=True)
        while len(bunches) > 0:
            keys, output = treatedNbunch(nbunch[keys])
            tab.append(output)
            del bunches[0]
            sep = ""
            if len(bunches) > 0:
                for key in keys:
                    output += sep + key
                    sep = "$"

    def getData(self, triplets: []) -> (str, Error, pgv.Node, set, pgv.AGraph):

        LearningItems = set()
        lexicalisationItems = set()

        graph = pgv.AGraph(directed=True, strict=False)
        triplets = sorted(triplets, key=lambda contenu: contenu[1] + contenu[0] + contenu[2])

        relationSujets = {}
        relationObjets = {}
        name_form = ""
        gestionnaireActeurs = Gestionnaire()
        for triplet in triplets:
            subj = Acteur(gestionnaireActeurs, triplet[0], triplet[3])
            obj = Acteur(gestionnaireActeurs, triplet[2], triplet[4])
            relation = Prédicat(triplet[1])  # , subj, obj)

            lexicalisationItems.add(subj.lexicalisationItem)
            lexicalisationItems.add(obj.lexicalisationItem)
            lexicalisationItems.add(relation.lexicalisationItem)
            if subj not in relationSujets.keys():
                relationSujets[subj] = []
            relationSujets[subj].append(relation)
            if obj not in relationObjets.keys():
                relationObjets[obj] = []
                relationObjets[obj].append(relation)

            name_form += subj.code + relation.identifiant + obj.code
            if subj == obj:
                return "", Error.AUTO_REFERENCE, None, lexicalisationItems, relationSujets, relationObjets
            else:
                if not graph.has_node(subj.identifiant):
                    graph.add_node(subj.identifiant)
                if not graph.has_node(obj.identifiant):
                    graph.add_node(obj.identifiant)
                if not graph.has_node(relation.identifiant):
                    graph.add_node(relation.identifiant)
                if not graph.has_edge(subj.identifiant, relation.identifiant):
                    graph.add_edge(subj.identifiant, relation.identifiant)
                if not graph.has_edge(relation.identifiant, obj.identifiant):  # graphviz autorise deux mêmes edges
                    graph.add_edge(relation.identifiant, obj.identifiant)


        # on va regarder maintenant s'il y a plusieurs roots
        root = ""
        for node in graph.iternodes():
            if len(graph.predecessors(node)) == 0:
                if root != '':
                    return graph.to_string(), Error.DISJONCTION, None, lexicalisationItems, relationSujets, relationObjets
                root = node

        def marqueSuccessor(node: pgv.Node, nbunch={}, level=0, treatedNodes=[]):
            treatedNodes.append(node)
            level += 1
            if graph.has_node(node):
                for succ in graph.successors(node):
                    key = str(level).zfill(2)
                    if key not in nbunch.keys():
                        nbunch[key] = [succ]
                    else:
                        nbunch[key].append(succ)
                    if not succ in treatedNodes:
                        erreur = marqueSuccessor(succ, nbunch, level, treatedNodes[:])
                        if erreur == Error.CYCLE:
                            return erreur
                        else:
                            return
                    else:
                        return Error.CYCLE
            else:
                return Error.CYCLE

        # on marque les niveaux et créée un nouveau graphe (so
        nbunch = {}
        output = marqueSuccessor(root, nbunch)
        if output == Error.CYCLE:
            return graph.to_string(), output, None, lexicalisationItems, relationSujets, relationObjets

        # on ordonnance
        for bunch in sorted(list(nbunch.keys()), reverse=True):
            graph.add_subgraph(nbunch[bunch], bunch)
            parent = {}
            for node in sorted(nbunch[bunch]):
                predecessors = graph.predecessors(node)
                for predecessor in predecessors:
                    if predecessor not in parent:
                        parent[predecessor] = []
                    if node.__str__().startswith("a"):  # on ignore les acteurs
                        parent[predecessor] += sorted(list(graph.successors(node)), reverse=False)
                    else:  # on ajoute le nœud ent tant qu'enfant à son prédecesseur
                        parent[predecessor].append(node)

            for each in parent.keys():
                value = ""
                for item in parent[each]:
                    value += item.__str__().strip()
                each.attr['key'] = "$" + value + "#"

        # maintenant on peut les ordonner par key
        linear_graph = ""

        def getSuccessors(node: pgv.Node, icount=0, all=True, numberize=False, numberize2=False):
            if icount == 20:
                return Error.CYCLE
            if graph.has_node(node):
                nodes = graph.successors(node)
                if not node.__str__().startswith("a") or all:
                    output = node.__str__()
                else:
                    output = " "
                if len(nodes) > 0:
                    if numberize and not numberize2:
                        output += "-" + icount.__str__() + " "
                    elif numberize2:
                        output += "+" + icount.__str__() + " "
                    else:
                        output += "( "
                    succold = ""
                    for successeur in sorted(nodes, key=lambda x: x.__str__() + x.attr['key']):
                        if succold != "":
                            if successeur.attr['key'] == succold.attr['key']:
                                print("profondeur réordonnancement plus graphe nécessaire")
                                exit()
                        sortie = getSuccessors(successeur, icount + 1, all, numberize, numberize2)
                        if sortie != Error.CYCLE:
                            output += sortie + " "
                        else:
                            return sortie
                    if numberize or numberize2:
                        output += " -" + icount.__str__()  # + " "+ icount.__str__() + ")"
                    else:
                        output += ")"

                return output.replace("  ", " ")
            else:
                return Error.CYCLE

        # on marque les niveaux et créée un nouveau graohe (so

        linear_graph = getSuccessors(root)
        shorter_graph = getSuccessors(root, 0, False)
        shorter_graph_numb = getSuccessors(root, 0, False, True)
        linear_graph_numb = getSuccessors(root, numberize=True)
        shorter_graph_numb_inf = getSuccessors(root, 0, False, numberize2=True)

        if linear_graph == Error.CYCLE:
            return graph.to_string(), linear_graph, None, lexicalisationItems, relationSujets, relationObjets

        # petite mamaille pour shorted_encode: l'ordre des acteurs doit avoir été positionné dans l'ordre d'apparition
        acteurs_values = linear_graph.split()
        ia = len(acteurs_values)
        while ia > 0:
            ia -= 1
            if len(acteurs_values[ia]) == 3 and acteurs_values[ia][0] == "a" and acteurs_values[ia][1:].isdigit():
                if ia > 0 and acteurs_values[ia] in acteurs_values[
                                                    0:ia]:  # on cherche à vérifier le nommage par ordre d'apparition dans le triplet
                    acteurs_values.pop(ia)
            else:
                acteurs_values.pop(ia)

        # une vérification cycle: si je me suis baladée dans un graph sans rencontrer le 2ème
        codes = {}
        for lexicalisationItem in lexicalisationItems:
            if lexicalisationItem.type == LexicalisationItem.Type.ACTEUR and lexicalisationItem.id.strip() in acteurs_values:
                codes[lexicalisationItem.code] = lexicalisationItem.id
        for objet in relationObjets.keys():
            if objet.code not in codes.keys():
                return graph.to_string(), Error.CYCLE, None, lexicalisationItems, relationSujets, relationObjets
        for sujet in relationSujets.keys():
            if sujet.code not in codes.keys():
                return graph.to_string(), Error.CYCLE, None, lexicalisationItems, relationSujets, relationObjets

        new_values = sorted((acteurs_values))
        if new_values != acteurs_values:
            for variable in (linear_graph, linear_graph_numb):
                in_var = variable.split()
                out_var = []
                for v in in_var:
                    if v in acteurs_values:
                        v = new_values[acteurs_values.index(v)]
                    out_var.append(v)

                if " ".join(in_var) == linear_graph:
                    linear_graph = " ".join(out_var)
                elif " ".join(in_var) == linear_graph_numb:
                    linear_graph_numb = " ".join(out_var)
                else:
                    print("erreur variable getData")
                    exit(1)
            codes = {}
            for lexicalisationItem in lexicalisationItems:
                if lexicalisationItem.type == LexicalisationItem.Type.ACTEUR and lexicalisationItem.id.strip() in acteurs_values:
                    lexicalisationItem.force_id(
                        new_values[acteurs_values.index(lexicalisationItem.id.strip())].strip() + " ")
                    codes[lexicalisationItem.code] = lexicalisationItem.id

            # comme les lexicalisationItems ont été générées détâchées de l'objet d'origine, in faut aussi changer dans les clefs
            # on va bien associer à code égale pour éviter les croisements incertains quand un sujet fait aussi partie des objet
            for objet in relationObjets.keys():
                objet._identifiant = codes[objet.code]
            for sujet in relationSujets.keys():
                sujet._identifiant = codes[objet.code]

            # les graphes seraient à changer, ils sont sortis à ttre informatifs et non utilisés, on les laisse pour l'heure
        return ((linear_graph, shorter_graph, linear_graph_numb, shorter_graph_numb, shorter_graph_numb_inf),
                Error.NO_ERROR, root, lexicalisationItems, relationSujets, relationObjets)

    def parseData(self):
        tree = ElementTree.parse(self.path)
        root = tree.getroot()

        exemples = root.findall('exemple')

        for exemple in exemples:
            encoded = exemple.find('encoded')
            origin = exemple.find('origin').text.strip()
            triplets = []
            log_triplet = " "
            triplets_encodés = ""
            triplets_simples = ""
            triplets_simples_préfixés = ""
            sep = ""

            for entry in exemple.findall('relations'):

                for relation in entry.findall('relation'):
                    prédicat = relation.attrib['reference']
                    sujet = relation.find("subject")
                    subj = sujet.attrib['reference']
                    subj_id = sujet.attrib['id']
                    objet = relation.find("object")
                    obj = objet.attrib['reference']
                    obj_id = objet.attrib['id']
                    triplets.append([subj, prédicat, obj, subj_id, obj_id])
                    log_triplet += '(' + subj + " ; " + prédicat + " ; " + obj + ") "
                    triplets_simples += sep + subj.strip().replace(" ", "_") + " " + prédicat.strip().replace(" ",
                                                                                                              "_") + " " + obj.strip().replace(
                        " ", "_")
                    triplets_encodés += sep + subj + " " + prédicat.strip().replace(" ", "_") + " " + obj
                    triplets_simples_préfixés += sep + "s_" + subj.strip().replace(" ",
                                                                                   "_") + " " + prédicat.strip().replace(
                        " ",
                        "_") + " " + "o_" + obj.strip().replace(
                        " ", "_")
                    sep = " "
            # dictionnaire des contraintes toutes initialisées à la négative
            out_constrainst = {'relative object': '', 'relative adverb': '', 'coordinated full clauses': '', 'coordinated clauses': '', 'apposition': '', 'possessif': '', 'relative subject': '', 'direct object': '', 'passive voice': '', 'existential': '', 'juxtaposition': ''}
            if v2 or v3:
                del out_constrainst['juxtaposition']

            count_contrainte = 0

            for key in out_constrainst.keys():
                out_constrainst[key]= "not_"+key.replace(' ', '_')
            for contraintes in exemple.findall("constraints"):
                for contrainte in contraintes.findall("contrainte"):
                    # si la contrainte est présente, on remplace par un mot clé non négatif
                    if contrainte.attrib['name'] in out_constrainst.keys():
                        count_contrainte += 1
                        out_constrainst[contrainte.attrib['name']] = contrainte.attrib['name'].replace(" ", "_")

            retour, code_erreur, root, lexicalisationItems, relationSujets, relationObjets = self.getData(triplets)

            # traitement codes erreur
            if code_erreur != Error.NO_ERROR:
                self.log += code_erreur.__str__() + log_triplet + " " + encoded.text + "\n"
                self.__count_errors += 1
                if code_erreur not in self.__type_errors.keys():
                    self.__type_errors[code_erreur] = 0
                self.__type_errors[code_erreur] += 1
            else:
                self.__count_ok += 1
                sortie = encoded.text.strip()

                listValue = []
                for lexicalisationItem in lexicalisationItems:
                    listValue.append(lexicalisationItem.value.replace(" ", "_"))
                listValue = sorted(listValue, key=lambda x: len(x.__str__()), reverse=True)

                triplets_parenthèses_non_encodées = retour[0]

                # bon, ce n'est pas très propre, mais cela fonctionne...
                for value in listValue:
                    for lexicalisationItem in lexicalisationItems:
                        if lexicalisationItem.value.replace(" ", "_") == value:
                            if lexicalisationItem.type == LexicalisationItem.Type.ACTEUR:
                                # parfois traînent des ~xlid.... donc on met une espace
                                sortie = sortie.replace(lexicalisationItem.code, " " + lexicalisationItem.id).replace(
                                    "  ", " ").strip()
                                triplets_encodés = triplets_encodés.replace(lexicalisationItem.value,
                                                                            lexicalisationItem.id).replace("  ", " ")
                                triplets_parenthèses_non_encodées = triplets_parenthèses_non_encodées.replace(
                                    lexicalisationItem.id, value + " ").replace("  ", " ") + " "

                normal = sortie

                # bon, ce n'est pas très propre, mais cela fonctionne...
                for value in listValue:
                    for lexicalisationItem in lexicalisationItems:
                        if lexicalisationItem.value.replace(" ", "_") == value:
                            if lexicalisationItem.type == LexicalisationItem.Type.ACTEUR:
                                normal = normal.replace(lexicalisationItem.id.strip(),
                                                        " " + lexicalisationItem.value.replace(" ", "_") + " ").replace(
                                    "  ", " ").strip()

                relations_structured = ""
                for acteur in sorted(list((set(relationSujets.keys()) | set(relationObjets.keys()))),
                                     key=lambda x: x.identifiant):
                    relations_structured += " [ ( "
                    sep = ""

                    if acteur in relationSujets.keys():
                        for relation in sorted(relationSujets[acteur], key=lambda x: x.identifiant):
                            relations_structured += sep + relation.identifiant
                            sep = " "
                    relations_structured += " ) ( "
                    sep = ""
                    if acteur in relationObjets.keys():
                        for relation in sorted(relationObjets[acteur], key=lambda x: x.identifiant):
                            relations_structured += sep + relation.identifiant
                            sep = " "
                    relations_structured += " ) ] "
                relations_structured = relations_structured.strip().replace("  ", " ")

                relations_structured_id = ""
                for acteur in sorted(list((set(relationSujets.keys()) | set(relationObjets.keys()))),
                                     key=lambda x: x.identifiant):
                    relations_structured_id += acteur.identifiant + " ( "
                    sep = ""

                    if acteur in relationSujets.keys():
                        for relation in sorted(relationSujets[acteur], key=lambda x: x.identifiant):
                            relations_structured_id += sep + relation.identifiant
                            sep = " "
                    relations_structured_id += " ) ( "
                    sep = ""
                    if acteur in relationObjets.keys():
                        for relation in sorted(relationObjets[acteur], key=lambda x: x.identifiant):
                            relations_structured_id += sep + relation.identifiant
                            sep = " "
                    relations_structured_id += " ) "
                relations_structured_id = relations_structured_id.strip().replace("  ", " ")

                relations_structured_expressive = ""
                sep = " "
                for acteur in sorted(list((set(relationSujets.keys()) | set(relationObjets.keys()))),
                                     key=lambda x: x.identifiant):
                    if acteur in relationSujets.keys():
                        relations_structured_expressive += ' ' + acteur.identifiant + "0"
                        for relation in sorted(relationSujets[acteur], key=lambda x: x.identifiant):
                            relations_structured_expressive += sep + relation.identifiant
                    if acteur in relationObjets.keys():
                        relations_structured_expressive += sep + acteur.identifiant + "1"
                        for relation in sorted(relationObjets[acteur], key=lambda x: x.identifiant):
                            relations_structured_expressive += sep + relation.identifiant

                    relations_structured_expressive = relations_structured_expressive.strip().replace("  ", " ")

                relations_structured_expressive2 = ""
                sep = " "
                for acteur in sorted(list((set(relationSujets.keys()) | set(relationObjets.keys()))),
                                     key=lambda x: x.identifiant):
                    if acteur in relationSujets.keys():
                        for relation in sorted(relationSujets[acteur], key=lambda x: x.identifiant):
                            relations_structured_expressive2 += sep + acteur.identifiant + "0"
                            relations_structured_expressive2 += sep + relation.identifiant
                    if acteur in relationObjets.keys():
                        for relation in sorted(relationObjets[acteur], key=lambda x: x.identifiant):
                            relations_structured_expressive2 += sep + acteur.identifiant + "1"
                            relations_structured_expressive2 += sep + relation.identifiant

                    relations_structured_expressive2.strip().replace("  ", " ")

                # on renvoie une contrainte par tour
                contraintes = []
                for contrainte in sorted(list(out_constrainst.keys())):
                    contraintes.append(XML.Contrainte(contrainte.replace("not_", ""),
                                                      not out_constrainst[contrainte].startswith("not_")))

                autres = []
                infosEntrées = []
                infosSorties = []

                self.__corpus_BLEU.createItem(triplets_simples)
                if not v2:
                    infosEntrées.append(XML.Info(triplets_simples, True, False, XML.Info.LevelAnonymisation.TRANSMISSION_NON_ENCODÉE,
                                 XML.Info.Encodage.AUCUN, XML.Info.Structure.LINÉAIRE, triplets_simples))
                    autres.append(XML.Info(log_triplet, False, False, XML.Info.LevelAnonymisation.TRANSMISSION_NON_ENCODÉE,
                                           XML.Info.Encodage.AUCUN, XML.Info.Structure.LINÉAIRE_MÉCANIQUEMENT_PARENTHÉSÉE, triplets_simples))
                    autres[-1].typeParenthèse = XML.Info.Parenthèses.TYPE
                    infosEntrées.append(XML.Info(triplets_simples_préfixés, True, False,
                                                 XML.Info.LevelAnonymisation.TRANSMISSION_NON_ENCODÉE,
                                                 XML.Info.Encodage.ACTEUR_À_RÔLE_PRÉFIXÉ, XML.Info.Structure.LINÉAIRE, triplets_simples))
                    infosEntrées.append(XML.Info(triplets_encodés, True, False, XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE,
                                                 XML.Info.Encodage.ACTEUR, XML.Info.Structure.LINÉAIRE, triplets_simples))
                    infosEntrées.append(XML.Info(triplets_parenthèses_non_encodées.replace("  ", " "),
                                                 True, False, XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE,
                                                 XML.Info.Encodage.RELATION,
                                                 XML.Info.Structure.LINÉAIRE_MÉCANIQUEMENT_PARENTHÉSÉE, triplets_simples))
                    infosEntrées[-1].typeParenthèse = XML.Info.Parenthèses.TYPE
                # au moins le premier est tel que voulu par Claire (arbre))
                # structure_encodée_parenthésée avec acteur
                struct_act =  retour[0][:]
                for litem in lexicalisationItems:
                    struct_act = struct_act.replace(litem.code.strip().replace(" ", '_') if litem.id == None else litem.id.strip().replace(" ", '_'), litem.value.strip())
                if v2:
                    struct_act_2 = retour[0][:]
                    sortie_act_2 = sortie.replace("  ", " ")
                    acteurs = {}
                    décomposition_sujets = struct_act_2.strip().split(" ")

                    i_count_po = 0
                    i_count_pf = 0

                    for idec, compo_sujet in enumerate(décomposition_sujets):
                        if compo_sujet not in ('(', ')'):
                            if compo_sujet.startswith("a"):
                                if compo_sujet not in acteurs.keys():
                                    if i_count_po == i_count_pf:
                                        rel_kept = ''
                                        for relation_courante in décomposition_sujets[idec+1:]:
                                            if relation_courante.startswith("r"):
                                                rel_kept= relation_courante
                                                break
                                        acteurs[compo_sujet] = rel_kept + "_sujet"
                                    else:
                                        rel_kept = ''
                                        for relation_courante in décomposition_sujets[:idec]:
                                            if relation_courante.startswith("r"):
                                                rel_kept = relation_courante
                                        acteurs[compo_sujet] = rel_kept + "_objet"
                        elif compo_sujet == "(":
                            i_count_po += 1
                        elif compo_sujet == ")":
                            i_count_pf += 1

                    combinaisons_sem_subj = set()
                    for litem in lexicalisationItems:
                        if litem.type == LexicalisationItem.Type.PREDICAT:
                            combinaisons_sem_subj.add(litem)
                        else:
                            combinaisons_sem_subj.add(Acteur(None, litem._value, litem._code, litem._value, acteurs[litem._id.strip()] + " "))
                            struct_act_2 = struct_act_2.replace(litem._id.strip(), acteurs[litem._id.strip()].strip())
                            sortie_act_2 = sortie_act_2.replace(litem._id.strip(), acteurs[litem._id.strip()].strip())

                    exception = False
                    for item in acteurs.values():
                        if item not in sortie_act_2:
                            print("poup")
                            print(struct_act_2)
                            print(sortie_act_2)
                            print(triplets_simples)
                            print(normal)
                            print(retour[0])
                            print(sortie)
                            exception = True
                    if not exception:
                        infosEntrées.append(XML.Info(struct_act_2.replace("  ", " "),
                                                 True, False, XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE,
                                                 XML.Info.Encodage.ACTEUR_À_RÔLE_PRÉFIXÉ,
                                                 XML.Info.Structure.ARBORESCENTE_STRUCTURÉE_PARENTHÉSÉE_RELATIVE,
                                                 triplets_simples))

                        infosSorties.append(XML.Info(sortie_act_2.replace("  ", " "), False, True,
                                                 XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE,
                                                 XML.Info.Encodage.ACTEUR_À_RÔLE_PRÉFIXÉ,
                                                 XML.Info.Structure.LINÉAIRE, triplets_simples))
                        self.__corpus_BLEU.add_lexicalisation(infosSorties[-1].value, infosSorties[-1].encodage)
                        yield [infosEntrées, infosSorties, contraintes, combinaisons_sem_subj, count_contrainte]
                elif v4:
                    struct_act_2 = retour[0][:]
                    sortie_act_2 = sortie.replace("  ", " ")
                    acteurs = {}
                    décomposition_sujets = struct_act_2.strip().split(" ")
                    decom_copy = décomposition_sujets[:]
                    i_count_po = 0
                    i_count_pf = 0

                    for idec, compo_sujet in enumerate(décomposition_sujets):
                        if compo_sujet not in ('(', ')') and compo_sujet not in acteurs.keys():
                            if compo_sujet.startswith("a"):
                                if compo_sujet not in acteurs.keys():
                                    if i_count_po == i_count_pf:
                                        rel_kept = ''
                                        for relation_courante in decom_copy[idec+1:]:
                                            if relation_courante.startswith("r"):
                                                rel_kept= relation_courante
                                                break
                                        acteurs[compo_sujet] = "a" + rel_kept + "Sujet"
                                    else:
                                        sujet = ''
                                        for relation_courante in decom_copy[:idec]:
                                            if relation_courante.startswith("a"):
                                                sujet = relation_courante.replace("a", 'az')
                                        rel_kept = ''
                                        for relation_courante in decom_copy[:idec]:
                                            if relation_courante.startswith("r"):
                                                rel_kept = relation_courante
                                        acteurs[compo_sujet] = sujet + rel_kept + "Objet"
                        elif compo_sujet == "(":
                            i_count_po += 1
                        elif compo_sujet == ")":
                            i_count_pf += 1

                    combinaisons_sem_subj = set()
                    for litem in lexicalisationItems:
                        if litem.type == LexicalisationItem.Type.PREDICAT:
                            combinaisons_sem_subj.add(litem)
                        else:
                            combinaisons_sem_subj.add(Acteur(None, litem._value, litem._code, litem._value, acteurs[litem._id.strip()] + " "))
                            struct_act_2 = struct_act_2.replace(litem._id.strip(), acteurs[litem._id.strip()].strip())
                            sortie_act_2 = sortie_act_2.replace(litem._id.strip(), acteurs[litem._id.strip()].strip())
                             #
                    exception = False
                    for item in acteurs.values():
                        if item not in sortie_act_2:
                            print("poup")
                            print(struct_act_2)
                            print(sortie_act_2)
                            print(triplets_simples)
                            print(normal)
                            print(retour[0])
                            print(sortie)
                            exception = True
                    if not exception:
                        infosEntrées.append(XML.Info(struct_act_2.replace("  ", " "),
                                                     True, False, XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE,
                                                     XML.Info.Encodage.ACTEUR_À_RÔLE_PRÉFIXÉ,
                                                     XML.Info.Structure.ARBORESCENTE_STRUCTURÉE_PARENTHÉSÉE_RELATIVE_COMPLEXE,
                                                     triplets_simples))

                        infosSorties.append(XML.Info(sortie_act_2.replace("  ", " "), False, True,
                                                     XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE,
                                                     XML.Info.Encodage.ACTEUR_À_RÔLE_PRÉFIXÉ,
                                                     XML.Info.Structure.LINÉAIRE, triplets_simples))
                        self.__corpus_BLEU.add_lexicalisation(infosSorties[-1].value, infosSorties[-1].encodage)
                        yield [infosEntrées, infosSorties, contraintes, combinaisons_sem_subj, count_contrainte]
             #dans les meilleurs scores ->
                if v3:
                    infosEntrées.append(XML.Info(struct_act.replace("  ", " "),
                                                 True, False, XML.Info.LevelAnonymisation.TRANSMISSION_RÉGULÉE,
                                                 XML.Info.Encodage.AUCUN,
                                                 XML.Info.Structure.ARBORESCENCE_STRUCTURÉE_PARENTHÉSÉE, triplets_simples))
                    infosEntrées.append(XML.Info(struct_act.replace("  ", " ").replace("_", " "),
                                                 True, False, XML.Info.LevelAnonymisation.TRANSMISSION_NON_ENCODÉE,
                                                 XML.Info.Encodage.FLUX,
                                                 XML.Info.Structure.ARBORESCENCE_STRUCTURÉE_PARENTHÉSÉE, triplets_simples))
                    infosEntrées.append(XML.Info(retour[0].replace("  ", " "),
                                                 True, False, XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE,
                                                 XML.Info.Encodage.ACTEUR_RELATION,
                                                 XML.Info.Structure.ARBORESCENCE_STRUCTURÉE_PARENTHÉSÉE, triplets_simples))
                    infosEntrées.append(XML.Info(retour[0].replace("  ", " "),
                                                 True, False, XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE,
                                                 XML.Info.Encodage.ACTEUR_RELATION,
                                                 XML.Info.Structure.ARBORESCENCE_STRUCTURÉE_PARENTHÉSÉE, triplets_simples))
                    infosEntrées[-1].typeParenthèse = XML.Info.Parenthèses.TYPE
                    infosEntrées.append(XML.Info(retour[1].replace("  ", " "),
                                                 True, False, XML.Info.LevelAnonymisation.MASQUAGE,
                                                 XML.Info.Encodage.RELATION,
                                                 XML.Info.Structure.ARBORESCENCE_STRUCTURÉE_PARENTHÉSÉE, triplets_simples))
                    infosEntrées[-1].typeParenthèse = XML.Info.Parenthèses.TYPE
                    infosEntrées.append(XML.Info(retour[2].replace("  ", " "),
                                                 True, False, XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE,
                                                 XML.Info.Encodage.ACTEUR_RELATION,
                                                 XML.Info.Structure.ARBORESCENCE_STRUCTURÉE_PARENTHÉSÉE, triplets_simples))
                    infosEntrées[-1].typeParenthèse = XML.Info.Parenthèses.NIVEAUX
                    infosEntrées.append(XML.Info(retour[3].replace("  ", " "),
                                                 True, False, XML.Info.LevelAnonymisation.MASQUAGE,
                                                 XML.Info.Encodage.RELATION,
                                                 XML.Info.Structure.ARBORESCENCE_STRUCTURÉE_PARENTHÉSÉE, triplets_simples))
                    infosEntrées[-1].typeParenthèse = XML.Info.Parenthèses.NIVEAUX
                    infosEntrées.append(XML.Info(retour[4].replace("  ", " "),
                                                 True, False, XML.Info.LevelAnonymisation.MASQUAGE,
                                                 XML.Info.Encodage.RELATION,
                                                 XML.Info.Structure.ARBORESCENCE_STRUCTURÉE_PARENTHÉSÉE, triplets_simples))
                    infosEntrées[-1].typeParenthèse = XML.Info.Parenthèses.TYPE_ET_NIVEAUX
                    infosSorties.append(XML.Info(sortie.replace("  ", " "), False, True,
                                                 XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE, XML.Info.Encodage.ACTEUR,
                                                 XML.Info.Structure.LINÉAIRE, triplets_simples))
                    self.__corpus_BLEU.add_lexicalisation(infosSorties[-1].value, infosSorties[-1].encodage)
                    infosSorties.append(XML.Info(normal.replace("  ", " "), False, True,
                                                 XML.Info.LevelAnonymisation.TRANSMISSION_RÉGULÉE, XML.Info.Encodage.AUCUN,
                                                 XML.Info.Structure.LINÉAIRE, triplets_simples))
                    self.__corpus_BLEU.add_lexicalisation(infosSorties[-1].value, infosSorties[-1].encodage)
                    infosSorties.append(XML.Info(normal.replace("  ", " ").replace("_", " "), False, True,
                                                 XML.Info.LevelAnonymisation.TRANSMISSION_RÉGULÉE, XML.Info.Encodage.FLUX,
                                                 XML.Info.Structure.LINÉAIRE, triplets_simples))
                    self.__corpus_BLEU.add_lexicalisation(infosSorties[-1].value, infosSorties[-1].encodage)
                    infosEntrées.append(XML.Info(relations_structured, True, False,
                                                 XML.Info.LevelAnonymisation.MASQUAGE, XML.Info.Encodage.RELATION,
                                                 XML.Info.Structure.ARBORESCENTE_PARENTHÉSÉE, triplets_simples
                                                 ))
                    infosEntrées[-1].typeParenthèse = XML.Info.Parenthèses.TYPE
                    infosEntrées.append(XML.Info(relations_structured_id, True, False,
                                                 XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE,
                                                 XML.Info.Encodage.ACTEUR_RELATION,
                                                 XML.Info.Structure.ARBORESCENTE_PARENTHÉSÉE, triplets_simples
                                                 ))
                    infosEntrées[-1].typeParenthèse = XML.Info.Parenthèses.TYPE
                    infosEntrées.append(XML.Info(relations_structured_expressive, True, False,
                                                 XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE,
                                                 XML.Info.Encodage.ACTEUR_RELATION,
                                                 XML.Info.Structure.ARBORESCENTE_INFORMÉE_CONCISE, triplets_simples))
                    infosEntrées.append(XML.Info(relations_structured_expressive2, True, False,
                                                 XML.Info.LevelAnonymisation.ENCODAGE_SIMPLE,
                                                 XML.Info.Encodage.ACTEUR_À_RÔLE_PRÉFIXÉ_RELATION,
                                                 XML.Info.Structure.ARBORESCENTE_INFORMÉE_REDONDANTE, triplets_simples))

                test = {}
                for info in infosEntrées + infosSorties:
                    key = info.__str__() + info.isInput.__str__() + info.isOutput.__str__()
                    if key not in test.keys():
                        test[key] = ""
                    else:
                        raise Exception("doublon : " + key)
                if v3:
                    yield [infosEntrées, infosSorties, contraintes, lexicalisationItems, count_contrainte]

    def close(self):
        self.__corpus_BLEU.__close__()

    @property
    def corpusLexicalisation(self) -> corpusBLEU:
        return self.__corpus_BLEU

    @property
    def infos(self) -> (int, int, {}):
        '''
        Informations de traitement
        :return: un dictionnaire rendant compte des succès et erreurs rencontrées lors du parsins
        '''
        return ({'ok': self.__count_ok, 'erreur': self.__count_errors, 'details_erreurs': self.__type_errors})

    def writeLog(self, path: str):
        '''
        Sauvegarde du journal de parsing
        :param path: chemin de sauvegarde
        :return: void
        '''
        fjson = open(path, "w")
        fjson.write(self.log)
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True


if __name__ == '__main__':
    data = XML("../../prepareCorpus/webnlgparser_data/working_data.xml")
    generator = data.parseData()
    for a in generator:
        print(a)
