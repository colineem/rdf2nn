#il suffit d'effacer les contraintes_Mono pour recalculer.

import json
from pycorenlp import StanfordCoreNLP
import pygraphviz as pgv
from progress.bar import Bar
import shared
import getpass
import os

dirname = "cs1s8_diff_1_1"

import glob

repertoire = dirname
see_examples = False
if os.path.isdir('/home/' + getpass.getuser() + '/Dropbox/PycharmProjectsMaison/' + \
                 'SentencesGenerationWithConstraints/essaiReseaux/'):
    dataPath = '/home/' + getpass.getuser() + '/Dropbox/PycharmProjectsMaison/' + \
               'SentencesGenerationWithConstraints/essaiReseaux/'
else:
    print(getpass.getuser() + " : alter code, add your path")
    exit(5)

def loadJSON(filename: str, type: type) -> object:
    global dataPath
    if os.path.isfile(dataPath + repertoire + "/" + filename):
        with open(dataPath +  repertoire + "/" + filename, 'r') as f:
            json_data = json.load(f)
        return json_data
    else:
        if type == dict:
            return {}
        else:
            return []

def writeJSON(model:str, filename: str, data: []):
    with open(dirname + "/" + model + "/" + filename, "w") as fjson:
        json.dump(data, fjson, ensure_ascii=False, indent=2)

try:
    nlp = StanfordCoreNLP("http://zammih:5000")
    text = ('Alan Bean was born on the "1932-03-15" and his Alma Mater is "UT Austin, B.S. 1955".')
    tmp_output = nlp.annotate(text, properties = {
        'annotators': 'tokenize,pos,depparse,parse,ner',
        'outputFormat': 'json'
        })
except:
    SERVEUR_CORENLP = 'http://localhost:5000'
    nlp = StanfordCoreNLP('http://localhost:5000')
    tmp_output = nlp.annotate(text, properties = {'annotators': 'tokenize,pos,depparse,parse,ner','outputFormat': 'json'})
    print(("Serveur changé pour localhost"))

POS_TAGS = ["CC", "CD", "DT", "EX", "FW", "IN", "JJ", "JJR", "JJS", "LS", "MD", "NN", "NNS", "NNP", "NNPS", "PDT", "POS", "PRP", "PRP$", "RB", "RBR", "RBS", "RP", "SYM", "TO", "UH", "VB", "VBD", "VBG", "VBN", "VBP", "VBZ", "WDT", "WP", "WP$", "WRB"]

basic_system = ""
enhanced_system = ""

def make_enhanced_système():
    import rules
    global enhanced_system 
    enhanced_system = rules.System()
    # self.alternative_system=rules.System()
    # add link(départflèche, arriveflèche, étiquetteflèche)
    # on va créer nos règles

    # "Alfred Garth Jones died in London which is lead by the European Parliament."
    # "Aaron Bertram, who plays Ska punk, is an artist with the band Kids Imagine Nation."
    # pas de détection de la relative sujet dans le premier cas.
    # étant donné que je n'ai que des phrases affirmatives...

    rule_relativeSubject = enhanced_system.create_rule("relative_subject")
    x = rule_relativeSubject.add_node(pos="WP", inside_allow=False)
    y = rule_relativeSubject.add_node(pos="V.*", inside_allow=False)
    z = rule_relativeSubject.add_node()
    l1 = rule_relativeSubject.forbid_link(y, x, 'nsubj.*', "")
    l1 = rule_relativeSubject.add_link(z, x, 'nsubj.*')
    l1 = rule_relativeSubject.add_link(x, z, 'acl:relcl')
    l2 = rule_relativeSubject.add_link(x, y, 'ref')
    rule_relativeSubject.set_position(x, y)
    rule_relativeSubject.set_position(y, z)

    rule_relativeSubject = enhanced_system.create_rule("relative_subject_2")
    x = rule_relativeSubject.add_node(pos="W.*", inside_allow=False)
    y = rule_relativeSubject.add_node(pos="")
    z = rule_relativeSubject.add_node(pos="")

    rule_relativeSubject.add_link(z, y, "nsubj.*")
    rule_relativeSubject.add_link(y, z, "acl:relcl")
    rule_relativeSubject.add_link(y, x, "ref")

    rule_relativeSubject.set_position(y, x)
    rule_relativeSubject.set_position(x, z)



    # relative adverbs
    rule_relativeAdverbs = rules.System.Rule(name="relative_adverb")
    # soit x un noeud pos_taggué *
    x = rule_relativeAdverbs.add_node()
    # soit y un noeud quelconque
    y = rule_relativeAdverbs.add_node(pos='WRB')
    # soit l1 le lien unissant faisant x modifié par y, de type advmod
    l1 = rule_relativeAdverbs.add_link(x, y, 'advmod')

    # soit z un noeud quelconque
    # z un nœud qui sera dans une relation de relcl avec ce que l'adverbe modifie
    z = rule_relativeAdverbs.add_node()
    # soit l2 le lien unissant z à x, de nature 'acl:relcl'
    l2 = rule_relativeAdverbs.add_link(z, x, 'acl:relcl')

    # sachant que z n'est pas le sujet de x
    l3 = rule_relativeAdverbs.forbid_link(x, z, 'nsubj.*', "")

    enhanced_system.add_rule(rule_relativeAdverbs)

    rule_coord = rules.System.Rule(name="coordinated_full_clauses_1")
    # soit x et y deux verbes
    v1 = rule_coord.add_node(pos='')
    v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
    # soit s1 et s2 deux sujets
    s1 = rule_coord.add_node()
    s2 = rule_coord.add_node()
    rule_coord.add_link(v1, s1, "nsubj.*")
    rule_coord.add_link(v2, s2, "nsubj.*")
    rule_coord.forbid_link(v1, v2, "advcl", "")

    # il existe un nœud quelconque k
    k = rule_coord.add_node()
    # ce nœud est à droite du sujet 2
    rule_coord.set_position(s2, k)
    # il n'a pas le lien ref avec le sujet de v2
    # si c'était le cas, nous aurions une relative sujet
    rule_coord.forbid_link(s2, k, "ref", "")  # je ne peux pas dire qu'il est à gauche de v2
    rule_coord.forbid_link(s2, v2, "acl:relcl",
                           "")  # je ne peux pas dire qu'il est à gauche de v2, je vais bloquer l'acl:relcl

    # soit c une coordination
    c = rule_coord.add_node(pos="CC", inside_allow=False)
    rule_coord.set_position(v1, c)
    rule_coord.set_position(s1, c)
    rule_coord.set_position(c, v2)
    rule_coord.set_position(c, s2)
    x = rule_coord.add_node()
    rule_coord.forbid_link(x, s2, "cop", "")  # on exclut les cop [x -> verbe être]
    # gestion deux temps

    # j'ajoute la règle
    enhanced_system.add_rule(rule_coord)

    # on s'occupe des "is"
    rule_coord = rules.System.Rule(name="coordinated_full_clauses_2")
    # soit x et y deux verbes
    v1 = rule_coord.add_node(pos='')
    v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
    # soit s1 le sujet du verbe 1
    s1 = rule_coord.add_node()
    # soit s2 le sujet du verbe 2
    # soit x ce qui est
    x = rule_coord.add_node()
    rule_coord.add_link(v1, s1, "nsubj.*")
    # on relie x à v2 par un lien cop
    rule_coord.add_link(x, v2, "cop")
    # est relié à v1 dans le cadre de la coordination verbale
    rule_coord.add_link(v1, x, "conj.*")
    # et ce qui est cop est sujet
    sujet = rule_coord.add_node()
    rule_coord.add_link(x, sujet, ".subj.*")

    # soit c une coordination
    c = rule_coord.add_node(pos="CC", inside_allow=False)
    rule_coord.set_position(v1, c)
    rule_coord.set_position(s1, c)
    rule_coord.set_position(c, sujet)
    rule_coord.set_position(c, x)
    rule_coord.set_position(c, v2)
    # j'ajoute la règle
    enhanced_system.add_rule(rule_coord)

    rule_coord = rules.System.Rule(name="coordinated_clauses_1")
    # soit x et y deux verbes
    v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
    v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
    # soit s1 et s2 deux sujets
    s1 = rule_coord.add_node()
    s2 = rule_coord.add_node()
    rule_coord.add_link(v1, s1, "nsubj.*")
    rule_coord.forbid_link(v2, s2, "nsubj.*", "")
    x = rule_coord.add_node()
    rule_coord.forbid_link(x, s2, "cop",
                           "")  # on exclut les cop [x -> verbe être] aboutissant à ce que être n'ait pas de sujet.
    rule_coord.forbid_link(x, v1, "csubj.*",
                           "")  # on exclut les cop [x -> verbe être] aboutissant à ce que être n'ait pas de sujet.
    x2 = rule_coord.add_node()

    # soit c une coordination
    c = rule_coord.add_node(pos="CC", inside_allow=False)
    rule_coord.set_position(v1, c)
    rule_coord.set_position(s1, c)
    rule_coord.set_position(s2, c)  # s2 aussi est après cc
    rule_coord.set_position(c, v2)
    rule_coord.set_position(c, x2)
    rule_coord.set_position(c, s2)
    rule_coord.forbid_link(s1, x2, "acl:relcl", "")
    # j'ajoute la règle
    enhanced_system.add_rule(rule_coord)

    rule_coord = rules.System.Rule(name="coordinated_clauses_2")
    # soit x et y deux verbes
    v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
    v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
    # soit s1 le sujet commun
    s1 = rule_coord.add_node()
    s2 = rule_coord.add_node()
    rule_coord.add_link(v1, s1, "nsubj.*")
    rule_coord.add_link(v2, s1, "nsubj.*")

    # soit c une coordination
    c = rule_coord.add_node(pos="CC", inside_allow=False)
    rule_coord.set_position(v1, c)
    rule_coord.set_position(s1, c)
    rule_coord.set_position(c, v2)
    # j'ajoute la règle
    enhanced_system.add_rule(rule_coord)

    # on s'occupe des "is"
    rule_coord = rules.System.Rule(name="coordinated_clauses_3")
    # soit x et y deux verbes
    v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
    v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
    # soit s1 le sujet commun
    s1 = rule_coord.add_node()
    # soit x ce qui est
    x = rule_coord.add_node()
    rule_coord.add_link(v1, s1, "nsubj.*")
    # on relie x à v2 par un lien cop
    rule_coord.add_link(x, v2, "cop")
    # est relié à v1 dans le cadre de la coordination verbale
    rule_coord.add_link(v1, x, "conj.*")
    # et ce qui est cop n'est pas sujet
    # pas sujet
    nonsuj = rule_coord.add_node()
    rule_coord.forbid_link(x, nonsuj, ".subj.*", "")

    # on s'occupe des "is"
    rule_coord = rules.System.Rule(name="coordinated_clauses_4")
    # soit x et y deux verbes
    v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
    v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
    # soit s1 le sujet commun
    s1 = rule_coord.add_node()
    # soit x ce qui est
    x1 = rule_coord.add_node()
    rule_coord.add_link(x1, s1, "nsubj.*")
    rule_coord.add_link(v2, s1, "nsubj.*")

    # on relie x à v1 par un lien cop
    rule_coord.add_link(x1, v1, "cop")
    # est relié à v2 dans le cadre de la coordination verbale
    rule_coord.add_link(v2, x1, "conj.*")

    # soit c une coordination
    c = rule_coord.add_node(pos="CC", inside_allow=False)
    rule_coord.set_position(x1, c)
    rule_coord.set_position(v1, c)
    rule_coord.set_position(s1, c)
    rule_coord.set_position(c, v2)
    # j'ajoute la règle
    enhanced_system.add_rule(rule_coord)

    # on s'occupe des "is"
    rule_coord = rules.System.Rule(name="coordinated_clauses_5")
    # soit x et y deux verbes
    v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
    v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
    # soit s1 le sujet
    s1 = rule_coord.add_node()
    # soit x1 et x2 ce qui est
    x1 = rule_coord.add_node()
    x2 = rule_coord.add_node()
    # on relie x à v1 par un lien cop
    rule_coord.add_link(x1, v1, "cop")
    # et x1 est relié au sujet par nsubj
    rule_coord.add_link(x1, s1, "nsubj.*")
    # il n'y a qu'un seul sujet référencé dans ce cas.

    # on relie x2 à v2 par un lien cop
    rule_coord.add_link(x2, v2, "cop")
    x3 = rule_coord.add_node()

    rule_coord.forbid_link(x2, x3, "nsubj.*", "")

    # et un lien conj.* lie x1 et x2
    rule_coord.add_link(x1, x2, "conj.*")

    # par ailleurs nous avons une coordination au milieu

    # soit c une coordination
    c = rule_coord.add_node(pos="CC", inside_allow=False)
    rule_coord.set_position(x1, c)
    rule_coord.set_position(v1, c)
    rule_coord.set_position(s1, c)
    rule_coord.set_position(c, v2)
    rule_coord.set_position(c, x2)
    rule_coord.set_position(c, x3)  # et non, il n'y a pas de sujet pour ce cop !s
    # j'ajoute la règle
    enhanced_system.add_rule(rule_coord)

    rule_passive = rules.System.Rule(name="passive_voice")
    # soit deux nœuds quelconques
    x = rule_passive.add_node()
    y = rule_passive.add_node()
    # il existe un lien .subjpass unissant deux noeuds
    rule_passive.add_link(x, y, '.subjpass.*')
    # ajout de la règle au système
    enhanced_system.add_rule(rule_passive)

    # alors visiblement cela détecte très très mal les clausal subject. Une horreur.
    # CF English language is spoken in Great Britain and a Severed Wasp was written in this language.
    # That his theory was flawed soon became obvious.
    # What she said makes sense.
    clausal_works = False
    if clausal_works:
        rule_cs = rules.System.Rule(name="clausal_subject")
        # soit deux nœuds quelconques
        x = rule_cs.add_node('V.*', inside_allow=False)
        y = rule_cs.add_node()
        # il existe un lien csubj.* unissant deux noeuds
        rule_cs.add_link(x, y, 'csubj.*')
        # ajout de la règle au système
        enhanced_system.add_rule(rule_cs)
    else:

        rule_cs = rules.System.Rule(name="clausal_subject")
        # soit verbe_maitre qui aura un sujet
        verbe_maitre = rule_cs.add_node('VB.*', inside_allow=False)
        # le verbe de la clause
        verbe_clause = rule_cs.add_node('VB.*', inside_allow=False)
        # nous sommes dans des phrases simples
        rule_cs.set_position(verbe_clause, verbe_maitre)
    if clausal_works:
        # le verbe maître sera dépourvu e sujet
        x = rule_cs.add_node()
        rule_cs.forbid_link(verbe_maitre, x, ".subj.*", "")
        # et il n'est pas le verbe être
        rule_cs.forbid_link(x, verbe_maitre, "cop", "")
        rule_cs.add_link(verbe_clause, verbe_maitre, "ccomp")
        # cette règle vise à éviter l'intégration des relatives type
        # The ground of A.S. Gubbio 1910 is located in Italy, where the people who live there are called Italians.
        # sinon elles correspondraient
        rule_cs.forbid_link(rule_cs.add_node(), verbe_maitre, "acl:relcl", "")

        # ajout de la règle au système
        enhanced_system.add_rule(rule_cs)

        # nous pourrions aussi avoir le verbe être. c'est détectable avec clausal subject, plutôt avec des what kjiso wants is ksdj
        rule_cs = rules.System.Rule(name="clausal_subject_3")
        # soit verbe_maitre qui aura un sujet
        verbe_maitre = rule_cs.add_node('VB.*', inside_allow=False)
        # le verbe de la clause
        verbe_clause = rule_cs.add_node('VB.*', inside_allow=False)
        # nous sommes dans des phrases simples
        rule_cs.set_position(verbe_clause, verbe_maitre)

        # le verbe maître sera dépourvu e sujet
        x = rule_cs.add_node()
        # et il est le verbe être
        rule_cs.add_link(x, verbe_maitre, "cop")

        rule_cs.add_link(x, verbe_clause, "csubj*")

        # ajout de la règle au système
        enhanced_system.add_rule(rule_cs)

        # nous pourrions aussi avoir le verbe être. c'est détectable avec un ccomp quand pré-fixé

        rule_cs = rules.System.Rule(name="clausal_subject_2")
        # soit verbe_maitre qui aura un sujet
        verbe_maitre = rule_cs.add_node('VB.*', inside_allow=False)
        # le verbe de la clause
        verbe_clause = rule_cs.add_node('VB.*', inside_allow=False)
        # nous sommes dans des phrases simples
        rule_cs.set_position(verbe_clause, verbe_maitre)

        # le verbe maître sera dépourvu e sujet
        x = rule_cs.add_node()
        # et il est le verbe être
        rule_cs.add_link(x, verbe_maitre, "cop")
        rule_cs.forbid_link(x, rule_cs.add_node(), "nsubj.*", "")

        rule_cs.add_link(verbe_clause, x, "ccomp")

        # ajout de la règle au système
        enhanced_system.add_rule(rule_cs)

    rule_appos = rules.System.Rule(name="apposition")
    # soit deux nœuds quelconques
    x = rule_appos.add_node()
    y = rule_appos.add_node()
    # il existe un lien csubj.* unissant deux noeuds
    rule_appos.add_link(x, y, 'appos')
    # ajout de la règle au système
    enhanced_system.add_rule(rule_appos)

    # case_rule = rules.System.Rule(name="possessif")
    # #soit deux nœuds quelconques
    # x = case_rule.add_node()
    # y = case_rule.add_node()
    # #il existe un lien csubj.* unissant deux noeuds
    # case_rule.add_link( x, y, 'case')
    # #ajout de la règle au système
    # enhanced_system.add_rule(case_rule)

    case_rule = rules.System.Rule(name="possessif")
    # soit deux nœuds quelconques
    x = case_rule.add_node()
    y = case_rule.add_node()
    # il existe un lien csubj.* unissant deux noeuds
    case_rule.add_link(x, y, 'nmod:poss')
    # ajout de la règle au système
    enhanced_system.add_rule(case_rule)

    #
    # coordonnées
    rule_juxtaClauses = rules.System.Rule(name="juxtaposition")
    n1 = rule_juxtaClauses.add_node()
    # un point final
    n2 = rule_juxtaClauses.add_node()

    sujet1 = rule_juxtaClauses.add_node()
    sujet2 = rule_juxtaClauses.add_node()
    # on veut que ces noeuds aient des sujets
    rule_juxtaClauses.add_link(n1, sujet1, 'nsubj.*')
    rule_juxtaClauses.add_link(n2, sujet2, 'nsubj.*')

    rule_juxtaClauses.add_link(n1, n2, 'parataxis')

    enhanced_system.add_rule(rule_juxtaClauses)

    objectDirect = rules.System.Rule(name="direct_object")
    # soit x et y deux verbes
    x = objectDirect.add_node(pos='V.*', inside_allow=False)
    y = objectDirect.add_node(pos='')

    objectDirect.add_link(x, y, 'dobj')

    # j'ajoute la règle
    enhanced_system.add_rule(objectDirect)

    objectInDirect = rules.System.Rule(name="indirect_object")
    # soit x et y deux verbes
    x = objectInDirect.add_node(pos='V.*', inside_allow=False)
    y = objectInDirect.add_node(pos='')

    objectInDirect.add_link(x, y, 'iobj')

    # j'ajoute la règle
    enhanced_system.add_rule(objectInDirect)

    # two objects

    twiceRule = rules.System.Rule(name="twice_objects")
    # soit x et y deux verbes
    x = twiceRule.add_node(pos='V.*', inside_allow=False)
    y = twiceRule.add_node(pos='')
    z = twiceRule.add_node(pos='')

    twiceRule.add_link(x, y, 'iobj')
    twiceRule.add_link(x, z, 'dobj')

    # j'ajoute la règle
    enhanced_system.add_rule(twiceRule)

    existencielle = rules.System.Rule(name="existential")
    # soit x et y deux verbes
    y = existencielle.add_node(pos='')
    z = existencielle.add_node(pos='V.*', inside_allow=False)

    existencielle.add_link(y, z, 'cop')

    # j'ajoute la règle
    enhanced_system.add_rule(existencielle)

    ptc = rules.System.Rule(name="participle clause")
    # soit x, y et z
    x = ptc.add_node(pos='V.*')
    y = ptc.add_node(pos='V.*')
    z = ptc.add_node(pos='VBG')

    ptc.add_link(y, z, 'auxpass')
    ptc.add_link(x, y, 'advcl.*')
    ptc.forbid_link(y, ptc.add_node(), 'nsubj.*')
    enhanced_system.add_rule(ptc)

    ptc = rules.System.Rule(name="participle clause_bis_1")
    # soit x, y et z
    y = ptc.add_node(pos='V.*')
    z = ptc.add_node(pos='VBG')

    ptc.add_link(y, z, 'advcl')
    enhanced_system.add_rule(ptc)

    ptc = rules.System.Rule(name="participle clause_ter_1")
    # soit x, y et z
    y = ptc.add_node(pos='V.*')
    z = ptc.add_node(pos='VBN')

    ptc.add_link(y, z, 'advcl')
    ptc.forbid_link(ptc.add_node(), y, 'acl:relcl')
    enhanced_system.add_rule(ptc)

    ptc = rules.System.Rule(name="participle clause_bis_2")
    # soit x, y et z
    y = ptc.add_node(pos='V.*')
    z = ptc.add_node(pos='VBG')
    x = ptc.add_node()

    ptc.add_link(x, y, 'cop')
    ptc.add_link(x, z, 'advcl')
    ptc.forbid_link(ptc.add_node(), y, 'acl:relcl')
    enhanced_system.add_rule(ptc)

    ptc = rules.System.Rule(name="participle clause_3")
    # soit x, y
    x = ptc.add_node()
    y = ptc.add_node(pos='VBG')

    ptc.add_link(x, y, 'acl.*')
    ptc.forbid_link(y, ptc.add_node(), 'nsubj.*')
    enhanced_system.add_rule(ptc)

    ptc = rules.System.Rule(name="participle clause_3_ter_2")
    # soit x, y
    x = ptc.add_node()
    y = ptc.add_node(pos='VBN')
    z = ptc.add_node()

    ptc.add_link(x, y, 'amod')
    ptc.add_link(z, x, 'dep')

    enhanced_system.add_rule(ptc)

    ptc = rules.System.Rule(name="participle clause_subject_2_1")
    # soit x, y et z
    y = ptc.add_node(pos='V.*')
    z = ptc.add_node(pos='VBG')

    ptc.add_link(z, ptc.add_node(), 'nmod.*')
    ptc.add_link(y, z, 'csubj.*')
    enhanced_system.add_rule(ptc)

    ptc = rules.System.Rule(name="participle clause_subject_3_1")
    # soit x, y et z
    y = ptc.add_node(pos='V.*')
    z = ptc.add_node(pos='VBG')
    x = ptc.add_node()

    ptc.add_link(z, x, 'dobj')
    ptc.add_link(y, z, 'csubj.*')
    enhanced_system.add_rule(ptc)

    ptc = rules.System.Rule(name="participle clause_subject_2_2")
    # soit x, y et z
    y = ptc.add_node(pos='V.*')
    z = ptc.add_node(pos='VBN')

    ptc.add_link(z, ptc.add_node(), 'nmod.*')
    ptc.forbid_link(ptc.add_node(), z, 'acl:relcl')
    ptc.add_link(y, z, 'csubj.*')
    enhanced_system.add_rule(ptc)

    ptc = rules.System.Rule(name="participle clause_subject_3_2")
    # soit x, y et z
    y = ptc.add_node(pos='V.*')
    z = ptc.add_node(pos='VBN')

    ptc.add_link(z, ptc.add_node(), 'dobj')
    ptc.forbid_link(ptc.add_node(), z, 'acl:relcl')
    ptc.add_link(y, z, 'csubj.*')
    enhanced_system.add_rule(ptc)

def make_basic_système():
    import rules
    global basic_system
    basic_system = rules.System()
    # relative object
    rule_relativeObject = rules.System.Rule(name="relative_object_1")
    # soit x un noeud quelconque
    x = rule_relativeObject.add_node()
    # soit y un noeud quelconque qui sera un pronom relatif
    pronom_relatif = rule_relativeObject.add_node(pos="WDT")

    what_relative = rule_relativeObject.add_node()
    rule_relativeObject.add_link(x, what_relative, "acl:relcl")
    rule_relativeObject.add_link(what_relative, pronom_relatif, 'ref')

    basic_system.add_rule(rule_relativeObject)

    # relative object
    rule_relativeObject = rules.System.Rule(name="relative_object_2")
    # soit x un noeud quelconque qui sera sujet
    x = rule_relativeObject.add_node()
    # soit y un noeud quelconque qui sera un pronom relatif
    pronom_relatif = rule_relativeObject.add_node(pos="W.*")
    # verbe
    rule_relativeObject.add_link(x, pronom_relatif, 'nmod.*')

    basic_system.add_rule(rule_relativeObject)

make_enhanced_système()
make_basic_système()
preds_dones = loadJSON("analyses_référentes.json", dict)

def testFile(sources_path, prédictions_path, targets_paths, dictionnaire_path):
    global nlp, preds_dones
    dico_références = {}
    print(sources_path)
    dico_mono_contraintes = {}
    dico_duo_contraintes = {}
    présence_acteurs = []


    with open(sources_path, 'r') as f:
        sources = f.read().split('\n')
    f.closed
    with open(prédictions_path, 'r') as f:
        prédictions = f.read().split('\n')
    f.closed
    with open(targets_paths, 'r') as f:
        targets = f.read().split('\n')
    f.closed

    with open(dictionnaire_path, 'r') as f:
        dictionnaire_acteurs = json.load(f)

    sources = [((k.split(" ")[0], ), " ".join(k.split(" ")[1:])) for k in sources]

    bar = Bar('Processing', max=len(dictionnaire_acteurs))
    for i, pred in enumerate(prédictions):
        if i < len(dictionnaire_acteurs):
            bar.next()
            pred = pred.replace("_", " ")
            entrée_dico = dictionnaire_acteurs[i]
            count_acteurs = 0
            acteurs_inside = 0
            for each in entrée_dico.keys():
                if each.startswith("a") or "_objet" in each or "_sujet" in each:
                    count_acteurs += 1
                    pred = pred.replace(each.strip().replace("_", " "), entrée_dico[each])
                    if entrée_dico[each] in pred:
                        acteurs_inside += 1
            présence_acteurs.append(acteurs_inside/count_acteurs)

            pred_error = False
            if pred in preds_dones.keys():
                output_src_done = preds_dones[pred]
                if preds_dones[pred] == "error":
                    output_src_done = nlp.annotate(pred, properties={
                        "pipelineLanguage": "en",
                        'annotators': 'tokenize,pos,depparse,parse,ner,mention',
                        'outputFormat': 'json'
                    })
                    pred_error = True
                elif preds_dones[pred] == "error2":
                    output_src_done = nlp.annotate(pred, properties={
                        "pipelineLanguage": "en",
                        'annotators': 'tokenize,pos,depparse,parse,ner',
                        'outputFormat': 'json'
                    })
            else:
                output_src_done = nlp.annotate(pred, properties={
                    "pipelineLanguage": "en",
                    'annotators': "tokenize,ssplit,pos,ner,depparse,coref",
                    'outputFormat': 'json'
                })  # -outputFormat typedDependencies  -keepPunct

            if output_src_done.__str__() == 'None' or type(output_src_done) == str:
                # stanford se casse parfois le nez sur les corefs

                output_src_done = nlp.annotate(pred, properties={
                    "pipelineLanguage": "en",
                    'annotators': 'tokenize,pos,depparse,parse,ner,mention',
                    'outputFormat': 'json'
                })
                coref_error = True
            preds_dones[pred] = output_src_done

            constraints = {}

            for num_sent in range(len(output_src_done['sentences'])):
                graph = pgv.AGraph(directed=True, strict=False)
                basic_graph = pgv.AGraph(directed=True, strict=False)
                for each in output_src_done['sentences'][num_sent]['enhancedPlusPlusDependencies']:
                    try:
                        graph.add_edge(each['governor'], each['dependent'], str(each['dep']))
                    except Exception as exc:
                        print()
                        print("enhanced_graph")
                        print(type(exc))  # the exception instance
                        print(exc.args)
                        # problème, un seul edge possible concomitamment
                        print("ajout dépendance en échec")
                        print(each['governor'].__str__() + " " + each['dependent'].__str__() + " " + each['dep'].__str__())

                for each in output_src_done['sentences'][num_sent]['basicDependencies']:
                    try:
                        basic_graph.add_edge(each['governor'], each['dependent'], each['dep'])
                    except Exception as exc:
                        print()
                        print("basic_graph")
                        print(type(exc))  # the exception instance
                        print(exc.args)
                        # problème, un seul edge possible concomitamment
                        print("ajout dépendance en échec")
                        print(each['governor'].__str__() + " " + each['dependent'].__str__() + " " + each['dep'].__str__())

                num_token = 0
                while num_token < len(output_src_done['sentences'][num_sent]['tokens']):


                    token = output_src_done['sentences'][num_sent]['tokens'][num_token]
                    attribs = {}
                    attribs['idx'] = token['index'].__str__()
                    attribs['ner'] = token['ner'].__str__()

                    pos = token['pos']
                    if pos not in POS_TAGS:
                        pos = "OTHERS"
                    attribs['pos'] = pos

                    # on ajoute l'item de façon plus complète à "items's root"
                    attribs = {}
                    attribs['idx'] = token['index'].__str__()
                    attribs['lemma'] = token['lemma'].__str__()
                    attribs['ner'] = token['ner'].__str__()
                    pos = token['pos']
                    if pos not in POS_TAGS:
                        pos = "OTHERS"
                    attribs['pos'] = pos
                    node = graph.get_node(token['index'])
                    node.attr['word'] = token['originalText'].__str__()
                    node.attr['pos'] = pos

                    node = basic_graph.get_node(token['index'])
                    node.attr['word'] = token['originalText'].__str__()
                    node.attr['pos'] = pos

                    num_token += 1
                error = False
                try:
                    constraints.update(enhanced_system.apply_rules(graph.to_string(),  []))
                    constraints.update(basic_system.apply_rules(basic_graph.to_string(),  []))
                except:
                    error = True
                    constraints = {}
                    constraints_2 = {}
                    print(pred)
                    print(graph.to_string())

            contrainte_satisfaite = 0
            ctrlr = sources[i][0]
            if "baseline" in sources_path:
                ctrlr = []
            i_coun = 0
            for contrainte_recherchée in ctrlr:
                found = False
                if contrainte_recherchée not in dico_mono_contraintes.keys():
                    if "supp" in sources_path:
                        dico_mono_contraintes[contrainte_recherchée] = {"correctement absente":0, 'présente':0}
                    else:
                        dico_mono_contraintes[contrainte_recherchée] = {"correctement présente": 0, 'absente': 0}

                for contrainte in constraints.keys():
                    if shared.nettoie(contrainte_recherchée) == shared.nettoie(contrainte):
                        if "supp" in sources_path:
                            dico_mono_contraintes[contrainte_recherchée]['présente'] += 1
                        else:
                            dico_mono_contraintes[contrainte_recherchée]['correctement présente'] += 1

                        found = True
                        contrainte_satisfaite += 1
                        i_coun += 1
                        break
                if not found and not error:
                    if "supp" in sources_path:
                        dico_mono_contraintes[contrainte_recherchée]["correctement absente"] += 1
                    else:
                        dico_mono_contraintes[contrainte_recherchée]["absente"] += 1
                elif not found:
                    if "analyze_error" not in dico_mono_contraintes[contrainte_recherchée].keys():
                        dico_mono_contraintes[contrainte_recherchée]["analyze_error"] = []
                    dico_mono_contraintes[contrainte_recherchée]["analyze_error"].append(pred)
                    # S'il y a eu une erreur, on tentera une autre analyse.
                    if pred_error:
                        preds_dones[pred] = "error"
                    else:
                        preds_dones[pred] = "error2"
            if len(sources[i][0]) == 2:
                cle = " ".join(sorted([sources[i][0][0], sources[i][0][1]]))
                if cle not in dico_duo_contraintes.keys():
                    dico_duo_contraintes[cle] = {'analyze_error':0, 'contraintes_satisfaites':0, 'contraintes_non_satisfaites':0, 'contraintes_demi_satisfaites':0}
                if error:
                    dico_duo_contraintes[cle]['analyze_error'] += 1
                elif i_coun == 2:
                    dico_duo_contraintes[cle]['contraintes_satisfaites'] += 1
                elif i_coun == 0:
                    dico_duo_contraintes[cle]['contraintes_non_satisfaites'] += 1
                else:
                    dico_duo_contraintes[cle]['contraintes_demi_satisfaites'] += 1

            for contrainte in constraints.keys():
                contrainte = shared.nettoie(contrainte)
                if contrainte not in dico_références.keys():
                    dico_références[contrainte] = 0
                dico_références[contrainte] += 1
    bar.finish()
    return dico_mono_contraintes, dico_duo_contraintes,  dico_références, round(sum(présence_acteurs)/len(présence_acteurs), 2)

from pprint import pprint


import glob
files = glob.glob(repertoire+'/*/')
for file in files:
    if os.path.isfile(file + 'data semantic/descriptif.json'):
        if os.path.isfile(file + "training_results/data_test_predictions.txt"):
            model = file[file.index(repertoire)+3:file.rindex('/')]
            modeles[model] = loadJSON(model + '/data semantic/descriptif.json', dict)
        

modeles = ('supp_14210-14001', 'supp_14210-14001')

for modele in modeles:
    dire = '/home/' + getpass.getuser() + '/Dropbox/PycharmProjectsMaison/' + \
        'SentencesGenerationWithConstraints/essaiReseaux/' + repertoire + "/" + modele
    if os.path.isfile(dire+'/contraintes_Mono.json'):
        print('done ' + modele )
    else:

        sources = dire + "/test_src"
        targets = dire + "/test_tgt"
        prédictions = dire +  "/training_results/data_test_predictions.txt"
        dictionnaire_path = dire +  "/data semantic/test_src"

        bdo = True
        for path in [sources, targets, prédictions, dictionnaire_path]:
            if not os.path.isfile(path):
                bdo = False
        if bdo:
            d1, d2, d3, taux_présence_acteurs = testFile(sources, prédictions, targets, dictionnaire_path)
            writeJSON(modele, "contraintes_Mono.json", d1)
            writeJSON(modele, "contraintes_Duo.json", d2)
            writeJSON(modele, "contraintes_références.json", d3)
            writeJSON(modele, "taux_présence_acteurs.json", {'présence_acteurs':taux_présence_acteurs})
    writeJSON(modele, "../analyses_référentes.json", preds_dones)
