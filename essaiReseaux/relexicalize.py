
import json
from progress.bar import Bar
import shared
import getpass
import os

dirname = "cs1s8_diff_1_1"
repertoire = dirname
see_examples = False
if os.path.isdir('/home/' + getpass.getuser() + '/Dropbox/PycharmProjectsMaison/' + \
                 'SentencesGenerationWithConstraints/essaiReseaux/'):
    dataPath = '/home/' + getpass.getuser() + '/Dropbox/PycharmProjectsMaison/' + \
               'SentencesGenerationWithConstraints/essaiReseaux/'
else:
    print(getpass.getuser() + " : alter code, add your path")
    exit(5)

def loadJSON(filename: str, type: type) -> object:
    global dataPath
    if os.path.isfile(dataPath + repertoire + "/" + filename):
        with open(dataPath +  repertoire + "/" + filename, 'r') as f:
            json_data = json.load(f)
        return json_data
    else:
        if type == dict:
            return {}
        else:
            return []

def writeJSON(model:str, filename: str, data: []):
    with open(dirname + "/" + model + "/" + filename, "w") as fjson:
        json.dump(data, fjson, ensure_ascii=False, indent=2)

def testFile(prédictions_path,  dictionnaire_path):
    global nlp, preds_dones
    dico_références = {}
    dico_mono_contraintes = {}
    dico_duo_contraintes = {}
    présence_acteurs = []


    with open(prédictions_path, 'r') as f:
        prédictions = f.read().split('\n')
    f.closed

    with open(dictionnaire_path, 'r') as f:
        dictionnaire_acteurs = json.load(f)

    relex = []
    bar = Bar('Processing', max=len(dictionnaire_acteurs))
    for i, pred in enumerate(prédictions):
        if i < len(dictionnaire_acteurs):
            bar.next()
            pred = pred.replace("_", " ")
            entrée_dico = dictionnaire_acteurs[i]
            count_acteurs = 0
            acteurs_inside = 0
            for each in entrée_dico.keys():
                if each.startswith("a") or "_objet" in each or "_sujet" in each:
                    count_acteurs += 1
                    pred = pred.replace(each.strip().replace("_", " "), entrée_dico[each])
                    if entrée_dico[each] in pred:
                        acteurs_inside += 1
            présence_acteurs.append(acteurs_inside/count_acteurs)
        relex.append(pred)
    bar.finish()
    return relex

from pprint import pprint
dictionnaire_general = loadJSON('data semantic/dictionnaire.json', dict)

dictionnaire_general = {}

import glob
files = glob.glob(repertoire+'/*/')
for file in files:
    if os.path.isfile(file + 'data semantic/descriptif.json'):
        if os.path.isfile(file + "training_results/data_test_predictions.txt"):
            model = file[file.index(repertoire)+3:file.rindex('/')]
            dictionnaire_general[model] = loadJSON(model + '/data semantic/descriptif.json', dict)
if dictionnaire_general == {}:
    dictionnaire_general = {'supp_14210-14001':'', 'add_14210-14001':''}
            
for modele in dictionnaire_general.keys():
    dire = '/home/' + getpass.getuser() + '/Dropbox/PycharmProjectsMaison/' + \
        'SentencesGenerationWithConstraints/essaiReseaux/' + repertoire + "/" + modele
    if modele.startswith("baseline"):

        sources = dire + "/test_src"
        targets = dire + "/test_tgt"
        prédictions = dire +  "/training_results/data_test_predictions.txt"
        dictionnaire_path = dire +  "/data semantic/test_src"

        bdo = True
        for path in [prédictions, dictionnaire_path]:
            if not os.path.isfile(path):
                bdo = False
        if bdo:
            relex = testFile(prédictions, dictionnaire_path)

            f = open(prédictions[0:prédictions.rindex('.')] + "_relex"+prédictions[prédictions.rindex('.'):] , "w")
            f.write("\n".join(relex))
            f.flush()
            f.close()
            while not f.closed:
                True
            print()
    else:

        sources = dire + "/test_src"
        targets = dire + "/test_tgt"
        prédictions = dire +  "/training_results/data_test_predictions.txt"
        dictionnaire_path = dire +  "/data semantic/test_src"

        bdo = True
        for path in [prédictions, dictionnaire_path]:
            if not os.path.isfile(path):
                bdo = False
        if bdo:
            relex = testFile(prédictions, dictionnaire_path)

            f = open(prédictions[0:prédictions.rindex('.')] + "_relex"+prédictions[prédictions.rindex('.'):] , "w")
            f.write("\n".join(relex))
            f.flush()
            f.close()
            while not f.closed:
                True
            print()

dire = '/home/' + getpass.getuser() + '/Dropbox/PycharmProjectsMaison/' + \
    'SentencesGenerationWithConstraints/essaiReseaux/' + repertoire + "/origins"

for modele in ['duo', 'baseline', 'mono']:

    sources = dire + "/" + modele + "/test_src"
    targets = dire + "/" + modele + "/test_tgt"
    prédictions = dire + "/" + modele + "/test_tgt"
    dictionnaire_path = dire + "/" + modele + "/data semantic/test_src"

    bdo = True
    for path in [prédictions, dictionnaire_path]:
        if not os.path.isfile(path):
            bdo = False
    if bdo:
        relex = testFile(prédictions, dictionnaire_path)
        if "." in prédictions:
            f = open(prédictions[0:prédictions.rindex('.')] + "_relex" + prédictions[prédictions.rindex('.'):], "w")
        else:
            f = open(prédictions + "_relex", "w")
        f.write("\n".join(relex))
        f.flush()
        f.close()
        while not f.closed:
            True
        print()


