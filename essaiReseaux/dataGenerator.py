import shutil
from itertools import combinations
from rdf import objets
import json, os, random
import warnings

#les entrées seront en petite quantité si debug est à true
debug = False
verbose = True
dirname = "base_line"
sepTrainValid = "Sep" in dirname

metric_analysed_blue = True

inputs = {}
outputs = {}
decoding_material = {}
lexicalisations = {}


#les entrées vont être très proches, et on veut les mêmes données d'exemple pour chaque modèle testé.
random_lines = {}

if not os.path.isdir(dirname + "/data semantic"):
    os.makedirs(dirname + "/data semantic")

def write(filename: str, data, dir, semantic=''):
    '''
    Vise l'écriture d'un fichier d'apprentissage
    :param filename: nom souhaité
    :param data: tableau des lignes à enregistrer
    :param dir : répertoire des fichiers (sous-réperoire de dirname/)
    :param semantic: données permettant le décodage ligne à ligne des résulats
    :return:
    '''
    if not os.path.isdir(dirname + "/" + dir):
        os.makedirs(dirname + "/" + dir)
    # si jamais un fichier train existe on sort. On ne va pas remplacer du travail.
    if os.path.isfile(dirname + "/" + dir + "/training_results/data-train.t7"):
        print(dir + " in progress, not rewritten.")
    else:
        fjson = open(dirname + "/" + dir + '/' + filename, "w")
        fjson.write(("\n".join(data)).replace("  ", " "))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        if verbose:
            print('written '+ os.path.abspath(dirname + "/" + dir + '/' + filename))

        if semantic != '':
            if not os.path.isdir(dirname + "/" + dir + "/data semantic"):
                os.makedirs(dirname + "/" + dir + "/data semantic")
            with open(dirname + "/" + dir + "/data semantic/" + filename, "w") as fjson:
                json.dump(semantic, fjson, ensure_ascii=False, indent=2)
    return os.path.abspath(dirname + "/" + dir + '/' + filename)


if not os.path.isdir(dirname + "/data semantic"):
    os.makedirs(dirname + "/data semantic")

def writeJSON(filename: str, data: []):
    with open(dirname + "/data semantic/" + filename, "w") as fjson:
        json.dump(data, fjson, ensure_ascii=False, indent=2)

def loadJSON(filename: str, type: type):
    if os.path.isfile(dirname + "/data semantic/" + filename):
        with open(dirname + "/data semantic/" + filename, 'r') as f:
            json_data = json.load(f)
        return json_data
    else:
        if type == dict:
            return {}
        else:
            return []

# dictionnaire = loadJSON("dictionnaire.json", dict)
dictionnaire = {}

files =["valid", "test", "train"]
testBLEU = {}
testMonoBLEU = {}
testDuoBLEU = {}

# concevons maintenant nos fichiers par cible d'usage.

# pour stocker la longueur maximale des séquences
src_max_length = {}
tgt_max_length = {}

for file in files:
    if verbose:
        print('fichier ' + file + '[src/tgt] amorcé')

    xml = objets.XML("../prepareCorpus/webnlgparser_data/working_data_"+file+".xml", dirname)

    for package in xml.parseData():
        entrées_potentielles = package[0]
        sorties_potentielles = package[1]
        contraintes = package[2]
        dictionnairesLexicalisations = package[3]

        for info_logique in entrées_potentielles:
            tab_lex = []
            if "acteur" in info_logique.encodage.name.lower() or "MASQUAGE" in info_logique.anonymisation.name:
                for sortie in sorties_potentielles:
                    if "acteur" in sortie.encodage.name.lower():
                        tab_lex.append(sortie)
                    else:
                        cle = str(info_logique.__hash__()) + "-" + str(sortie.__hash__())
                        if os.path.isdir(dirname + "/" + cle):
                            print("del " + cle)
                            shutil.rmtree(dirname + "/" + cle)
            else:
                for sortie in sorties_potentielles:
                    tab_lex.append(sortie)

            for lexicalisation in tab_lex:

                for préfixe in ['duo_', 'mono_']:
                    cle = préfixe + str(info_logique.__hash__()) + "-" + str(lexicalisation.__hash__())

                    if cle not in dictionnaire:
                        value_cle = "entrée : " + info_logique.__str__().lower() + " - sortie : " + lexicalisation.__str__().lower()
                        dictionnaire[cle] = {"clé fichier":value_cle,
                                         'entrées':info_logique.toDic(),
                                         'count':0,
                                         'sorties':lexicalisation.toDic()}

                    if cle == "mono_14210-14001" or cle=="mono_20010-30001":
                        #baseline : aucun encodage
                        modele = "baseline-" + cle.replace("mono_","")
                        if modele not in dictionnaire:
                            value_cle = "entrée : " + info_logique.__str__().lower() + " - sortie : " + lexicalisation.__str__().lower()
                            dictionnaire[modele] = {"clé fichier":value_cle,
                                             'entrées':info_logique.toDic(),
                                             'count':0,
                                             'sorties':lexicalisation.toDic()}
                        if modele not in inputs.keys():
                            inputs[modele] = {}
                            outputs[modele] = {}
                            decoding_material[modele] = {}
                            lexicalisations[modele] = {}
                            random_lines[modele] = {}
                        if file not in inputs[modele].keys():
                            inputs[modele][file] = []
                            outputs[modele][file] = []
                            decoding_material[modele][file] = []
                            lexicalisations[modele][file] = []
                            random_lines[modele][file] = []

                        random_lines[modele][file].append(len(inputs[modele][file]))
                        input = info_logique.value
                        while "  " in input:
                            input = input.replace("  ", " ")
                        inputs[modele][file].append(input)
                        while "  " in lexicalisation.value:
                            lexicalisation.value = lexicalisation.value.replace("  ", " ")
                        outputs[modele][file].append(lexicalisation.value)
                        #le matériel de decodage devrait être non avenu.
                        decoding_material[modele][file].append({})
                        for entrée_dic in dictionnairesLexicalisations:
                            decoding_material[modele][file][-1][entrée_dic.code if entrée_dic.id is None else entrée_dic.id] = entrée_dic.value

                        lexicalisations[modele][file].append(list(xml.corpusLexicalisation.sentences(lexicalisation.entrée_originelle, lexicalisation.encodage, 3, lexicalisation.value.replace("  ", " "))))


                    data_lext = list(xml.corpusLexicalisation.sentences(lexicalisation.entrée_originelle, lexicalisation.encodage.value.__str__(),  3, lexicalisation.value.replace("  ", " ")))

                    if cle not in inputs.keys():
                        inputs[cle] = {}
                        outputs[cle] = {}
                        decoding_material[cle] = {}
                        random_lines[cle] = {}
                        lexicalisations[cle] = {}

                    if file not in inputs[cle].keys():
                        inputs[cle][file] = []
                        outputs[cle][file] = []
                        decoding_material[cle][file] = []
                        lexicalisations[cle][file] = []
                        random_lines[cle][file] = []

                    if préfixe == 'mono_':
                        tab_contrainte = []
                        for contrainte in contraintes:
                            if contrainte.positive:
                                tab_contrainte.append(contrainte)
                        #modèle mono-contraintes
                        if len(tab_contrainte) == 0:
                            dictionnaire[cle]['count'] += 1
                            # on stocke les index, on fera un shuffle de ce tableau pour que les exemples ne se suivent pas avec une forte proximité
                            random_lines[cle][file].append(len(inputs[cle][file]))
                            inputs[cle][file].append('sans_contrainte ' + info_logique.value)
                            outputs[cle][file].append(lexicalisation.value)
                            decoding_material[cle][file].append({})
                            for entrée_dic in dictionnairesLexicalisations:
                                decoding_material[cle][file][-1][entrée_dic.code if entrée_dic.id is None else entrée_dic.id] = entrée_dic.value
                            lexicalisations[cle][file].append(data_lext)
                        for contrainte in tab_contrainte:
                            dictionnaire[cle]['count'] += 1
                            # on stocke les index, on fera un shuffle de ce tableau pour que les exemples ne se suivent pas avec
                            # une forte proximité
                            random_lines[cle][file].append(len(inputs[cle][file]))
                            input = contrainte.__str__() + ' ' + info_logique.value

                            inputs[cle][file].append(input.replace("  ", " "))

                            outputs[cle][file].append(lexicalisation.value.replace("  ", " "))
                            decoding_material[cle][file].append({})
                            for entrée_dic in dictionnairesLexicalisations:
                                decoding_material[cle][file][-1][entrée_dic.code if entrée_dic.id is None else entrée_dic.id] = entrée_dic.value
                            lexicalisations[cle][file].append(data_lext)
                    else:
                        tmp_constraint = []
                        for contrainte in contraintes:
                            if contrainte.positive:
                                tmp_constraint.append(contrainte)
                        tab_contrainte = combinations(tmp_constraint, 2)

                        for contrainte in tab_contrainte:
                            if isinstance(contrainte, tuple):
                                dictionnaire[cle]['count'] += 1
                                # on stocke les index, on fera un shuffle de ce tableau pour que les exemples ne se suivent pas avec
                                # une forte proximité
                                random_lines[cle][file].append(len(inputs[cle][file]))
                                input = contrainte[0].__str__() + ' ' +  contrainte[1].__str__() + ' ' + info_logique.value

                                inputs[cle][file].append(input.replace("  ", " "))

                                outputs[cle][file].append(lexicalisation.value.replace("  ", " "))
                                decoding_material[cle][file].append({})
                                for entrée_dic in dictionnairesLexicalisations:
                                    decoding_material[cle][file][-1][entrée_dic.code if entrée_dic.id is None else entrée_dic.id] = entrée_dic.value
                                lexicalisations[cle][file].append(data_lext)
                            elif len(tmp_constraint) == 1:

                                dictionnaire[cle]['count'] += 1
                                # on stocke les index, on fera un shuffle de ce tableau pour que les exemples ne se suivent pas avec
                                # une forte proximité
                                random_lines[cle][file].append(len(inputs[cle][file]))
                                input = contraintes[0].__str__() + ' ' + 'sans_contrainte ' + info_logique.value

                                inputs[cle][file].append(input.replace("  ", " "))

                                outputs[cle][file].append(lexicalisation.value.replace("  ", " "))
                                decoding_material[cle][file].append({})
                                for entrée_dic in dictionnairesLexicalisations:
                                    decoding_material[cle][file][-1][entrée_dic.code if entrée_dic.id is None else entrée_dic.id] = entrée_dic.value
                                lexicalisations[cle][file].append(data_lext)
                        if len(tmp_constraint) == 0:

                            dictionnaire[cle]['count'] += 1
                            # on stocke les index, on fera un shuffle de ce tableau pour que les exemples ne se suivent pas avec
                            # une forte proximité
                            random_lines[cle][file].append(len(inputs[cle][file]))
                            input = 'sans_contrainte ' + 'sans_contrainte ' + info_logique.value

                            inputs[cle][file].append(input.replace("  ", " "))

                            outputs[cle][file].append(lexicalisation.value.replace("  ", " "))
                            decoding_material[cle][file].append({})
                            for entrée_dic in dictionnairesLexicalisations:
                                decoding_material[cle][file][-1][entrée_dic.code if entrée_dic.id is None else entrée_dic.id] = entrée_dic.value
                            lexicalisations[cle][file].append(data_lext)


        if debug and len(inputs[cle][file]) > 100:
            break
    ls_keys = list(inputs.keys())
    if dirname == "v3":
        for cle in ls_keys:
            if cle not in ('mono_30210-14001', 'duo_30210-14001', 'baseline-14210-14001', 'baseline-20010-30001', 'mono_13410-11001', 'duo_13410-11001', 'duo_15510-11001', 'mono_15510-11001', 'duo_23010-11001', 'mono_13110-11001', 'mono_26110-36001', 'mono_126010-36001', 'mono_20010-36001', 'mono_30110-36001', 'duo_20010-36001'):
                del inputs[cle]

    for cle in inputs.keys():
        if verbose:
            print('fichier ' + file + '[src/tgt] pré-traité pour étape '+cle+', génération fichier ' + cle)
        # on shuffle pour que les données ne se suivent pas
        # si et seulement si on est sur le train, c'est plus sympa de regarder l'impact des contraintes linéairement

        #on initialise nos longueurs de chaînes
        if cle not in src_max_length.keys():
            src_max_length[cle] = 0
            tgt_max_length[cle] = 0

        dataset = [[], [], []]
        BLEUset = [[], [], []]

        if file == 'train' and not sepTrainValid:
            # je validerai sur 10% de mon corpus d'apprentissage, et à la fin, évaluerai sur test_src pour évaluer le bleu sur corpus non vu
            random.shuffle(random_lines[cle]['valid'])

            for each in random_lines[cle]['valid']:
                dataset[0].append(inputs[cle]['valid'][each])
                dataset[1].append(outputs[cle]['valid'][each])
                dataset[2].append(decoding_material[cle]['valid'][each])
                BLEUset[0].append(lexicalisations[cle]['valid'][each][0])
                BLEUset[1].append(lexicalisations[cle]['valid'][each][1])
                BLEUset[2].append(lexicalisations[cle]['valid'][each][2])

                len_input = len(inputs[cle]['valid'][each].split(" "))
                len_output = len(outputs[cle]['valid'][each].split(" "))
                if len_input > src_max_length[cle]:
                    src_max_length[cle] = len_input
                if len_output > tgt_max_length[cle]:
                    tgt_max_length[cle] = len_output

        if file == "train":
            random.shuffle(random_lines[cle][file])

        for each in random_lines[cle][file]:
            if file=='train' and not sepTrainValid:
                position = random.randint(0, len(dataset[0])-1)
            else:
                position = -1
            dataset[0].insert(position, inputs[cle][file][each])
            dataset[1].insert(position, outputs[cle][file][each])
            dataset[2].insert(position, decoding_material[cle][file][each])
            BLEUset[0].insert(position, lexicalisations[cle][file][each][0])
            BLEUset[1].insert(position, lexicalisations[cle][file][each][1])
            BLEUset[2].insert(position, lexicalisations[cle][file][each][2])

            len_input = len(inputs[cle][file][each].split(" "))
            len_output = len(outputs[cle][file][each].split(" "))
            if len_input > src_max_length[cle]:
                src_max_length[cle] = len_input
            if len_output > tgt_max_length[cle]:
                tgt_max_length[cle] = len_output


        f0 = write(file + "_src", dataset[0], cle, dataset[2])
        f1 = write(file + "_tgt", dataset[1], cle)
        f2 = write(file + "_references_1.lex", BLEUset[0], cle)
        f3 = write(file + "_references_2.lex", BLEUset[1], cle)
        f4 = write(file + "_references_3.lex", BLEUset[2], cle)

        # réalisons en passant les scripts pour multiBLEU
        sep = ">"
        if file != 'valid':
            if cle not in testBLEU.keys():

                testBLEU[cle] = []
                testBLEU[cle].append("export TEST=\"training_results/data_test_predictions.txt\"")
                testBLEU[cle].append("export TRAIN=\"training_results/data_train_predictions.txt\"")
                testDuoBLEU[cle] = []
                testDuoBLEU[cle].append("export TEST=\"training_results/data_test_predictions.txt\"")
                testDuoBLEU[cle].append("export TRAIN=\"training_results/data_train_predictions.txt\"")
                testMonoBLEU[cle] = []
                testMonoBLEU[cle].append("export TEST=\"training_results/data_test_predictions.txt\"")
                testMonoBLEU[cle].append("export TRAIN=\"training_results/data_train_predictions.txt\"")

            testBLEU[cle].append("export HYPOTHESIS=\"" + f2 + '"')
            testMonoBLEU[cle].append("export HYPOTHESIS=\"" + f2 + '"')
            testDuoBLEU[cle].append("export HYPOTHESIS=\"" + f2 + '"')
            testMonoBLEU[cle].append("export " + file.upper() + "REF0=\"" + f1 + '"')
            testBLEU[cle].append("export " + file.upper() + "REF0=\"" + f2 + '"')
            testBLEU[cle].append("export " + file.upper() + "REF1=\"" + f3 + '"')
            testDuoBLEU[cle].append("export " + file.upper() + "REF0=\"" + f2 + '"')
            testDuoBLEU[cle].append("export " + file.upper() + "REF1=\"" + f3 + '"')
            testBLEU[cle].append("export " + file.upper() + "REF2=\"" + f4 + '"')
            testBLEU[cle].append('../multi-bleu.perl ${' + file.upper() + 'REF0} ${' + file.upper() + 'REF1} ${' + file.upper() + 'REF2}  < ${HYPOTHESIS} ' + sep + ' bleu_' + file + '_references.txt')
            testBLEU[cle].append('../multi-bleu.perl ${' + file.upper() + 'REF0} ${' + file.upper() + 'REF1} ${' + file.upper() + 'REF2}  < ${' + file.upper() + '} ' + sep + ' bleu_' + file + '.txt')
            testDuoBLEU[cle].append('../multi-bleu.perl ${' + file.upper() + 'REF0} ${' + file.upper() + 'REF1} < ${' + file.upper() + '} ' + sep + ' duo_bleu_' + file + '.txt')
            testMonoBLEU[cle].append('../multi-bleu.perl ${' + file.upper() + 'REF0} < ${' + file.upper() + '} ' + sep + ' mono_bleu_' + file + '.txt')



    # Nous allons maintenant générer les données d'évaluation

    # on clôt le xml, ce qui provoque la sauvegarde des lexicalisations possibles.
    xml.close()

if verbose:
    print('génération scripts d\'apprentissage')
# générons maintenant nos script d'apprentissage
big_gpu_script = []
big_cpu_script = []
big_poor_gpu_script = []
for key in inputs.keys():
    #on va mettre un descriptif du fichier dans chaque répertoire
    writeJSON("../"+key + "/data semantic/descriptif.json", dictionnaire[key])
    if not os.path.isfile(dirname + "/" + key + "/training_results/data-train.t7"):
        if not os.path.isdir(dirname + "/" + key + "/training_results"):
            os.makedirs(dirname + "/" + key + "/training_results")
        for value in range(2):
            script = []
            poor_script = []
            script.append('# prepare vocab ')
            script.append("cd ~/OpenNMT")
            script.append("th preprocess.lua " +
                          " -train_src " + os.path.abspath(dirname + "/" + key + '/train_src') +
                          " -train_tgt " + os.path.abspath(dirname + "/" + key + '/train_tgt') +
                          " -valid_src " + os.path.abspath(dirname + "/" + key + '/valid_src') +
                          " -valid_tgt " + os.path.abspath(dirname + "/" + key + '/valid_tgt') +
                          " -src_seq_length " + src_max_length[key].__str__() +
                          " -tgt_seq_length " + tgt_max_length[key].__str__() +
                          " -save_data " + os.path.abspath(dirname + "/" + key + "/training_results/data"))
            if value == 0:
                if metric_analysed_blue:
                    script.append("th train.lua -encoder_type brnn -global_attention general  -end_epoch 25 " +
                              " -data " + os.path.abspath(dirname + "/" + key) + "/training_results/data-train.t7" +
                              " -save_model " + os.path.abspath(dirname + "/" + key + "/training_results/data") +
                              " -log_level WARNING " +
                              " -validation_metric bleu " +
                              " -valid_src " + os.path.abspath(dirname + "/" + key + '/valid_src') +
                              " -valid_tgt " + os.path.abspath(dirname + "/" + key + '/valid_tgt') +
                              " -gpuid 0 ")
                else:
                    script.append("th train.lua -encoder_type brnn -global_attention general  -end_epoch 25 " +
                              " -data " + os.path.abspath(dirname + "/" + key) + "/training_results/data-train.t7" +
                              " -save_model " + os.path.abspath(dirname + "/" + key + "/training_results/data") +
                              " -log_level WARNING " +
                              " -valid_src " + os.path.abspath(dirname + "/" + key + '/valid_src') +
                              " -valid_tgt " + os.path.abspath(dirname + "/" + key + '/valid_tgt') +
                              " -gpuid 0 ")
            else:
                poor_script += script[:]
                if metric_analysed_blue:
                    script.append("THC_CACHING_ALLOCATOR=1 th train.lua -encoder_type brnn -global_attention general  -end_epoch 25 " +
                          " -data " + os.path.abspath(dirname + "/" + key) + "/training_results/data-train.t7" +
                          " -save_model " + os.path.abspath(dirname + "/" + key + "/training_results/data") +
                          " -log_level WARNING " +
                          " -validation_metric bleu " +
                          " -save_validation_translation_every 25 " +
                          " -valid_src " + os.path.abspath(dirname + "/" + key + '/valid_src') +
                          " -valid_tgt " + os.path.abspath(dirname + "/" + key + '/valid_tgt') +
                          " -gpuid 1  -fallback_to_cpu true ")
                    poor_script.append("THC_CACHING_ALLOCATOR=0 th train.lua -encoder_type brnn  -end_epoch 25 -global_attention general " +
                          " -data " + os.path.abspath(dirname + "/" + key) + "/training_results/data-train.t7" +
                          " -save_model " + os.path.abspath(dirname + "/" + key + "/training_results/data") +
                          " -log_level WARNING " +
                          " -max_batch_size 4 " +
                          " -validation_metric bleu " +
                          " -save_validation_translation_every 25 " +
                          " -valid_src " + os.path.abspath(dirname + "/" + key + '/valid_src') +
                          " -valid_tgt " + os.path.abspath(dirname + "/" + key + '/valid_tgt') +
                          " -gpuid 1  -fallback_to_cpu true ")
                else:
                    script.append("THC_CACHING_ALLOCATOR=1 th train.lua -encoder_type brnn -global_attention general  -end_epoch 25 " +
                          " -data " + os.path.abspath(dirname + "/" + key) + "/training_results/data-train.t7" +
                          " -save_model " + os.path.abspath(dirname + "/" + key + "/training_results/data") +
                          " -log_level WARNING " +
                          " -valid_src " + os.path.abspath(dirname + "/" + key + '/valid_src') +
                          " -valid_tgt " + os.path.abspath(dirname + "/" + key + '/valid_tgt') +
                          " -gpuid 1  -fallback_to_cpu true ")
                    poor_script.append(
                        "THC_CACHING_ALLOCATOR=0 th train.lua -encoder_type brnn  -end_epoch 25 -global_attention general " +
                        " -data " + os.path.abspath(dirname + "/" + key) + "/training_results/data-train.t7" +
                        " -save_model " + os.path.abspath(dirname + "/" + key + "/training_results/data") +
                        " -log_level WARNING " +
                        " -max_batch_size 4 " +
                        " -valid_src " + os.path.abspath(dirname + "/" + key + '/valid_src') +
                        " -valid_tgt " + os.path.abspath(dirname + "/" + key + '/valid_tgt') +
                        " -gpuid 1  -fallback_to_cpu true ")
            script.append("ls -R " + os.path.abspath(dirname + "/" + key) + "/training_results/ > " + os.path.abspath(dirname + "/" + key) + "/" + key + "-epochs.txt")
            poor_script.append(script[-1])
            for epoch in range(16):
                script.append("rm "+ os.path.abspath(dirname + "/" + key) + "/training_results/data_epoch" + (epoch+1).__str__() + "_*.t7")
                poor_script.append(script[-1])

            if value == 0:
                script.append("th translate.lua -time true "+
                              " -model " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_epoch25_*.t7 " +
                              " -src  " + os.path.abspath(dirname + "/" + key + '/train_src') +
                              " -output " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_train_predictions.txt " +
                              " -log_level WARNING  -gpuid 0")
                script.append("th translate.lua -time true "+
                              " -model " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_epoch25_*.t7 " +
                              " -src  " + os.path.abspath(dirname + "/" + key + '/test_src') +
                              " -output " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_test_predictions.txt " +
                              " -log_level WARNING  -gpuid 0")
                script.append('')
                write("OpenNMT-cpu-lua.bash", script, key)
                big_cpu_script.append("bash " + os.path.abspath(dirname + "/" + key + "/OpenNMT-cpu-lua.bash"))
            else:
                script.append("th translate.lua -time true "+
                              " -model " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_epoch25_*.t7 " +
                              " -src  " + os.path.abspath(dirname + "/" + key + '/train_src') +
                              " -output " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_train_predictions.txt " +
                              " -log_level WARNING  -gpuid 1  -fallback_to_cpu true")
                poor_script.append("THC_CACHING_ALLOCATOR=0 th translate.lua -time true "+
                              " -model " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_epoch25_*.t7 " +
                              " -src  " + os.path.abspath(dirname + "/" + key + '/train_src') +
                              " -output " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_train_predictions.txt " +
                              " -log_level WARNING  -gpuid 1  -fallback_to_cpu true")

                script.append("th translate.lua -time true " +
                              " -model " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_epoch25_*.t7 " +
                              " -src  " + os.path.abspath(dirname + "/" + key + '/test_src') +
                              " -output " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_test_predictions.txt " +
                              " -log_level WARNING  -gpuid 1  -fallback_to_cpu true")
                poor_script.append("THC_CACHING_ALLOCATOR=0 th translate.lua -time true " +
                                   " -model " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_epoch25_*.t7 " +
                                   " -src  " + os.path.abspath(dirname + "/" + key + '/test_src') +
                                   " -output " + os.path.abspath(dirname + "/" + key + "/training_results/data") + "_test_predictions.txt " +
                                   " -log_level WARNING  -gpuid 1  -fallback_to_cpu true")
                script.append("")
                poor_script.append("")
                for i in range(len(script)):
                    script[i] = script[i].replace("milizam", "emilie")
                write("OpenNMT-gpu-lua.bash", script, key)
                write("OpenNMT-poor-gpu-lua.bash", poor_script, key)
                big_gpu_script.append("bash " + os.path.abspath(dirname + "/" + key + "/" + "OpenNMT-gpu-lua.bash"))
                big_poor_gpu_script.append("bash " + os.path.abspath(dirname + "/" + key + "/" + "OpenNMT-poor-gpu-lua.bash"))

    elif os.path.isfile(dirname + "/" + key + "/training_results/data_train_predictions.txt"):
        print(key + " - train in progress or done.")
    elif os.path.isfile(dirname + "/" + key + "/training_results/data_epoch25_valid_translation.txt"):
        print(key + " - train in progress.")
    else:
        print(key + " in progress.")

big_gpu_script.append('')
big_poor_gpu_script.append('')
big_cpu_script.append('')
write("OpenNMT-poor-gpu-lua.sh", big_poor_gpu_script, '')
write("OpenNMT-gpu-lua.sh", big_gpu_script, '')
write("OpenNMT-cpu-lua.sh", big_cpu_script, '')

# sauvegardons les données générales...
writeJSON("dictionnaire.json", dictionnaire)

#Enregistrons les scripts pour les bleus:

if verbose:
    print('génération scripts des BLEU')

launchable = []
for key in testBLEU.keys():
    if launchable == []:
        launchable.append("#!/bin/bash")
        launchable.append(' cd ' + key)
    else:
        launchable.append(' cd ../' + key)
    launchable += testBLEU[key]
write("calculer_BLEUs.bash", launchable, '')


launchable = []
for key in testMonoBLEU.keys():
    if launchable == []:
        launchable.append("#!/bin/bash")
        launchable.append(' cd ' + key)
    else:
        launchable.append(' cd ../' + key)
    launchable += testMonoBLEU[key]
write("calculer_mono_BLEUs.bash", launchable, '')



launchable = []
for key in testDuoBLEU.keys():
    if launchable == []:
        launchable.append("#!/bin/bash")
        launchable.append(' cd ' + key)
    else:
        launchable.append(' cd ../' + key)
    launchable += testDuoBLEU[key]
write("calculer_duo_BLEUs.bash", launchable, '')

