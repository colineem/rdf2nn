from rdf import objets
from pycorenlp import StanfordCoreNLP
from pprint import pprint
nlp = StanfordCoreNLP('http://zammih:5000')

try:
    text = ('Alan Bean was born on the "1932-03-15" and his Alma Mater is "UT Austin, B.S. 1955".')
    tmp_output = nlp.annotate(text, properties = {
        'annotators': 'tokenize,pos,depparse,parse,ner',
        'outputFormat': 'json'
        })
      #t1 = Tree.fromstring(tmp_output['sentences'][0]['parse'])
      #t2 = Tree.fromstring(tmp_output['sentences'][1]['parse'])
except:
    SERVEUR_CORENLP = 'http://localhost:5000'
    nlp = StanfordCoreNLP('http://localhost:5000')
    tmp_output = nlp.annotate(text, properties = {
    'annotators': 'tokenize,pos,depparse,parse,ner',
    'outputFormat': 'json'
    })
    print(("Serveur changé pour localhost"))

class Sentence():

    def __init__(self, nlpmotor: StanfordCoreNLP, sentence:str, lexicalisationItems: set):

        self.short_sentence = sentence

        short_analyse = nlpmotor.annotate(sentence, properties={
            "pipelineLanguage": "en",
            'annotators': "tokenize,ssplit,pos,ner,depparse,coref",
            'outputFormat': 'json'
        })

        self.short_analyse = short_analyse

        self.lexications = {}
        for lexicalisationItem in lexicalisationItems:
            if lexicalisationItem.type == objets.LexicalisationItem.Type.ACTEUR:
                self.lexications[lexicalisationItem.value] = lexicalisationItem.id
                sentence = sentence.replace(lexicalisationItem.id, lexicalisationItem.value)

        self.long_sentence = sentence

        long_analyse = nlpmotor.annotate(sentence, properties={
            "pipelineLanguage": "en",
            'annotators': "tokenize,ssplit,pos,ner,depparse,coref",
            'outputFormat': 'json'
        })

        self.long_analyse = long_analyse


#struture test
if __name__ == '__main__':

    sentence = 'Born in a01 , in Malaysia, a00 , resides in a03 and is a member of the a02 party.'
    lexicalisationItems = set()
    lexicalisationItems.add(objets.LexicalisationItem(objets.LexicalisationItem.Type.ACTEUR, 'xlid933xrid', '"Barisan Ra\'ayat Jati Sarawak"', 'a02'))
    lexicalisationItems.add(objets.LexicalisationItem(objets.LexicalisationItem.Type.ACTEUR, 'xlid924xrid',
                                              'Abdul Taib Mahmud', 'a00'))
    lexicalisationItems.add(objets.LexicalisationItem(objets.LexicalisationItem.Type.ACTEUR, 'xlid926xrid',
                                              'Miri, Malaysia', 'a01'))
    lexicalisationItems.add(objets.LexicalisationItem(objets.LexicalisationItem.Type.ACTEUR, 'xlid932xrid',
                                              'Sarawak', 'a03'))

    sentence = Sentence(nlp, sentence, lexicalisationItems)

    print(sentence.short_sentence)
    pprint(sentence.short_analyse)

    print(sentence.long_sentence)
    pprint(sentence.long_analyse)