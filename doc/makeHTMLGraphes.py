import os

texte="""<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.container {
    display:flex;
    flex-wrap:wrap;
    flex-direction:row;
    justify-content:flex-start;
    align-items:stretch;
}

.left {order:1; flex-basis:100%; height:100%;}
.middle {order:3; flex-basis:100%; height:100%;}

@media screen and (min-width:600px) {
   .container {
       flex-wrap:nowrap;
   } 

    .left {
        flex-basis:200px;
        order:1;
    }
    .middle {
        flex-basis:1;
        order:2;
    }
}
</style>
</head>
<body>

xyz

</body>
</html> 
"""
onlyfiles = [f for f in os.listdir("../essaiReseaux/outed_png_graphes_form") if os.path.isfile(os.path.join("../essaiReseaux/outed_png_graphes_form", f))]
onlyfiles.sort(key=len)

xyz=[]
i = 0
for file in onlyfiles:
    xyz.append("<div class='container'>")
    i += 1
    xyz.append("""    <div class="left">
   modèle """+file.replace(".png", "")+"""
    </div>
""")
#    xyz.append("""    <div class="left">
#    <img src='"""+os.path.join("../essaiReseaux/outed_png_graphes_form", file)+"""' style='height:100%'>
#    </div>
#""")

    xyz.append("""    <div class="middle">
    <img src='"""+os.path.join("../essaiReseaux/outed_png_graphes_indexed_form", file)+"""' style='height:100%'>
    </div>
""")

    #xyz.append("""    <div class="right">
   # modèle """+file.replace(".png", "")+""" &nbsp;
    #</div>
#""")
    xyz.append("</div>")
    xyz.append('<hr/>')


texte = texte.replace("xyz", "\n".join(xyz))

with open("graphes_plain.html", 'w') as f:
    f.write(texte)
f.closed
