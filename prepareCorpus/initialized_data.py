# un serveur stanford corenlp équipé d'un modèle pour l'anglais est nécessaire --> recherche stanford
# supprimer /home/[user]/stanford.json pour oublier des phrases supprimées.

import sys; print('Python %s on %s' % (sys.version, sys.platform))

_error = 0
stanford_parser = True

import os
import sys
import glob
import unicodedata
import nltk
from nltk.corpus import stopwords
stop_set = set(stopwords.words('english'))

import time
from pprint import pprint
temps_base = 25

import string

from bs4 import BeautifulSoup

from pycorenlp import StanfordCoreNLP

from xml.etree import ElementTree

import pygraphviz as pgv
import shared

import json
import copy

import re
import itertools

xlridpattern = 'xlid[0-9]+xrid'
regexAZ = re.compile('[^a-zA-Z]')

import datefinder

SERVEUR_CORENLP=shared.SERVEUR_CORENLP
REP_WEBNLG=shared.REP_WEBNLG
HOME_ROOT = shared.HOME_ROOT
ERREURS_RECHERCHE="initialized_data/erreur recherche.json"
SUCCES_RECHERCHE="./initialized_data/succes recherche.json"
test_debug = False
test_length = 3
import random
ONLY_ERROR_FILE = False

#forcing permet de chercher à identifier plus de sujets/objets en jouant sur l'ordre des prédicats
#le premier traitement (forcing=0) les classe par taille, afin que "university of mendrisio" soit recherché avant "mendrisio"
#le premier traitement (forcing=1) les classe gentiment dans un ordre aléatoire, ce qu fait gagner pas mal de lexicalisations (cas des items trouvés raccourcis, type us pour united states)
#le premier traitement (forcing=2) les classe gentiment dans par ordre aléatoire, ce qui est super violent, mais limité à nb_combinaisons combinaisons shuffled. Il semblerait que le forcing 1 soit équivalent si on le lance une petite centaine de fois.
forcing = 2
nb_combinaisons = 24
z_fill_combi = 5
if nb_combinaisons < 10:
    z_fill_combi = 1
elif nb_combinaisons < 100:
    z_fill_combi = 2
elif nb_combinaisons < 1000:
    z_fill_combi = 3

erreurs_recherche = {}
erreurs_recherche_bis = {}

if os.path.isfile(ERREURS_RECHERCHE):
    file = open(ERREURS_RECHERCHE)
    erreurs_recherche = json.load(file)
    file.close()

    file = open(ERREURS_RECHERCHE)
    erreurs_recherche_bis = json.load(file)
    file.close()
#on rajoutera dans le fichier ce qui est soluble et non solutionné
keys=[key for key in  erreurs_recherche.keys()]

succes_recherche = {}
if os.path.isfile(SUCCES_RECHERCHE):
    file = open(SUCCES_RECHERCHE)
    succes_recherche = json.load(file)
    file.close()

outed = set()

ik = len(keys)
while ik > 0:
    key = keys[ik-1]
    ik-=1
    if len(erreurs_recherche[key])>0:
        i = len(erreurs_recherche[key])
        while i > 0:
            if erreurs_recherche[key][i-1][0]==key.replace("_", " "):
                del erreurs_recherche[key][i-1]
                del erreurs_recherche_bis[key][i-1]
            else:
                a=1
            i-=1
    if len(erreurs_recherche[key])==0:
        del erreurs_recherche[key]
        del erreurs_recherche_bis[key]
del keys
POS_TAGS = ["CC", "CD", "DT", "EX", "FW", "IN", "JJ", "JJR", "JJS", "LS", "MD", "NN", "NNS", "NNP", "NNPS", "PDT", "POS", "PRP", "PRP$", "RB", "RBR", "RBS", "RP", "SYM", "TO", "UH", "VB", "VBD", "VBG", "VBN", "VBP", "VBZ", "WDT", "WP", "WP$", "WRB"]


def compile(obj, ignore_case=False):

    obj=obj.replace('\\', '\\\\').replace('{', '\{').replace('}', '\}').replace('(', '\(').replace(')', '\)').replace('[', '\[').replace(']', '\]').replace('.', '\.')                                    .replace('+', '\+').replace('?', '\?').replace('-', '\-').replace('*', '\*')
    if ignore_case:
        re_searched = re.compile(r'(?!\B)' + obj+'(?!\B)', re.IGNORECASE)
    else:
        re_searched = re.compile(r'(?!\B)' + obj+'(?!\B)')
    return re_searched


class Data(object):
    def __init__(self, directory):
        self.directory=directory
        self.all_worked_sentences = []
        self.all_basics_sentences = []

    def strip_accents(self, s):
        return ''.join(c for c in unicodedata.normalize('NFD', s)
                       if unicodedata.category(c) != 'Mn')

    def couple_equive(self, object_subject_searched, lexicalisation):
        in_oust = []
        in_oust.append(["Populous_(company)", ["the company Populous"]])
        in_oust.append(["Twilight_(band)",  ["the band Twilight"]])
        in_oust.append(["Galicia_(Spain)",  ["Galicia, Spain", "Galicia in Spain"]])
        in_oust.append(["United_States_Army",  ["US Army"]])
        in_oust.append(["120 million (Australian dollars)",  ["120 million Australian dollars"]])
        in_oust.append(["\"April 2014\"",  ["April of 2014"]])
        in_oust.append(["Lotus_Eaters_(band)",  ["the Lotus Eaters band"]])
        in_oust.append(["Isis_(band)",  ["the band Isis"]])
        in_oust.append(["Twilight_(band)",  ["Twilight band"]])
        in_oust.append(["United_Kingdom",  ["UK"]])
        in_oust.append(["Ahmet_Ertegun",  ["Ahmet Ertegum"]])
        in_oust.append(["Fighter_pilot",  ["test pilot", "Test pilot"]])
        in_oust.append(['Carroll_County,_Maryland',  ["Carrol County Maryland"]])
        in_oust.append(['Felipe_VI_of_Spain',  ["Felipe VI", 'Felipe VI of Spain of Spain']])
        in_oust.append(['"non_performing_personnel"',  ["non-performing personnel", "non-performing", "no-performing", "non performing", '"non performing personnel" personnel']])

        for s, fs in in_oust:
            if s == object_subject_searched:
                for f in fs :
                    lexicalisation = lexicalisation.replace(f, s.replace("_", " "))

        return  lexicalisation

    #  http://stackoverflow.com/questions/517923/what-is-the-best-way-to-remove-accents-in-a-python-unicode-string
    def sentence_equive(self, sentence):
        if "of 1800.00, is located in Sao Jose dos Pinhais." in sentence:
            sentence = sentence.replace("1800.00", "1800.0")
        if "number76798317" in sentence:
            sentence = sentence.replace("number76798317", "number 76798317")
        if "AFIT" in sentence:
            sentence = sentence.replace("AFIT in 1962 with an M.S.", "AFIT, M.S. 1962") \
                .replace("AFIT with an M.S. in 1962", "AFIT, M.S. 1962") \
                .replace("with a M.S., from AFIT in 1962", "AFIT, M.S. 1962") \
                .replace("AFIT, MS in 1962", "AFIT, M.S. 1962") \
                .replace("AFIT, MS in 1962", "AFIT, M.S. 1962") \
                .replace("AFIT in 1962 with an MA", "AFIT, M.S. 1962") \
                .replace("AFIT with an MS in 1962", "AFIT, M.S. 1962") \
                .replace("an M.S. from AFIT in 1962", "AFIT, M.S. 1962") \
                .replace("AFIT, M.S. in 1962", "AFIT, M.S. 1962") \
                .replace("with an M.S., from AFIT in 1962", "AFIT, M.S. 1962") \
                .replace("AFIT in 1962 with a M.S.", "AFIT, M.S. 1962")
        if "Antooquia Department" in sentence:
            sentence = sentence.replace("Antooquia Department", "Antioquia Department")
        if "Colombian dish" in sentence or "noodle" in sentence or "Paisa region" in sentence:
            sentence = sentence.replace("Colombian dish", "Colombian cuisine")
            sentence = sentence.replace(
                "ground beef, tapioca, noodle, rice vermicelli, beef broth, kailan, celery, salted vegetables, and fried shallots.",
                "Ground beef, tapioca, noodle, rice vermicelli, beef broth, kailan, celery, salted vegetables, fried shallots").replace(
                "Ground beef, tapioca, noodle, rice vermicelli, beef broth, kailan, celery, salted vegetables, and fried shallots",
                "Ground beef, tapioca, noodle, rice vermicelli, beef broth, kailan, celery, salted vegetables, fried shallots")
            sentence = sentence.replace(
                "ground beef, tapioca, noodle, rice vermicelli, beef broth, kailan, celery, salted vegetables and fried shallots",
                "Ground beef, tapioca, noodle, rice vermicelli, beef broth, kailan, celery, salted vegetables, fried shallots").replace(
                "Ground beef, tapioca, noodle, rice vermicelli, beef broth, kailan, celery, salted vegetables, and fried shallots",
                "Ground beef, tapioca, noodle, rice vermicelli, beef broth, kailan, celery, salted vegetables, fried shallots")
            sentence = sentence.replace('Paisa region', "Paisa Region")
        if "Claude Bartalone" in sentence:
            sentence = sentence.replace("Claude Bartalone", "Claude Bartolone")
        if "Manual Valls" in sentence:
            sentence = sentence.replace('Manual Valls', "Manuel Valls")
        if "18 grams of carbohydrate" in sentence:
            sentence = sentence.replace('18 grams of carbohydrate', "18.0 g grams of carbohydrate") \
                .replace("4.8g", '4.8 g')
        if "ground almond, jam, butter and eggs" in sentence or "Bakwell tart" in sentence or "Bakewell pudding" in sentence:
            sentence = sentence.replace('ground almond, jam, butter and eggs', "Ground almond, jam, butter, eggs")
            sentence = sentence.replace('ground almonds, jam, butter and eggs', "Ground almond, jam, butter, eggs")
            sentence = sentence.replace('Bakwell tart', "Bakewell pudding")
            sentence = sentence.replace('Bakewell pudding', "Bakewell pudding") \
                .replace("warm (freshly baked) or cold", '"Warm  or cold"') \
                .replace("warm or cold", "Warm  or cold")
        if "U of T at Austin" in sentence:
            sentence = sentence.replace("U of T at Austin", "University of Texas at Austin")
        elif "University of Texas, Austin" in sentence:
            sentence = sentence.replace( "University of Texas, Austin", "University of Texas at Austin")

        if "Frank Aldrin" in sentence:
            sentence = sentence.replace("Frank Aldrin", 'Buzz Aldrin')
        if "1634: The Ram Rebelllion " in sentence:
            sentence = sentence.replace("1634: The Ram Rebelllion", '1634: The Ram Rebellion')
        if "U.S. is" in sentence:
            sentence = sentence.replace("U.S. is", "United States is")
        if "U.S. are" in sentence:
            sentence = sentence.replace("U.S. are", "United States are")
        if "U.S. where" in sentence:
            sentence = sentence.replace("U.S. where", "United States where")
        if "U.S. and" in sentence:
            sentence = sentence.replace("U.S. and", "United States and")
        if "U.S.," in sentence:
            sentence = sentence.replace("U.S.,", "United States,")
        if "U.S.)" in sentence:
            sentence = sentence.replace("U.S.)", "United States)")
        if "U.S. by" in sentence:
            sentence = sentence.replace("U.S. by", "United States by")
        if "U.S. resident" in sentence:
            sentence = sentence.replace("U.S. resident", "United States resident")
        if "U.S" in sentence:
            sentence = sentence.replace("U.S", "United States")
        if "U.S" in sentence:
            sentence = sentence.replace("U.S", "United States")
        if "USA" in sentence:
            sentence = sentence.replace("USA", "United States")
        if "US" in sentence:
            sentence = sentence.replace("US", "United States")
        if "T.S. Thakur" in sentence:
            sentence = sentence.replace("T.S. Thakur", "T. S. Thakur")
        if "Asam peda " in sentence:
            sentence = sentence.replace("Asam peda ", "Asam pedas ")
        if "Sumatra and the Malay Peninsula" in sentence:
            sentence = sentence.replace("Sumatra and the Malay Peninsula", "Sumatra and Malay Peninsula")
        if "UK" in sentence.upper():
            sentence = sentence.replace("UK", "United Kingdom").replace(" uk", " United Kingdom")
        if "Arem arem" in sentence:
            sentence = sentence.replace("Arem arem", "Arem-arem")
            sentence = sentence.replace("nationwide in Indonesia, but is more specific to Java",
                                        "Nationwide in Indonesia, but more specific to Java") \
                .replace("nationwide in Indonesia and more specifically in Java",
                         "Nationwide in Indonesia, but more specific to Java")
            sentence = sentence.replace(": rice cooked in a banana leaf and vegetables or minced meat",
                                        ": compressed rice cooked in banana leaf with vegetables or minced meat fillings")
        if "Parliament of xlid175xrid" in sentence:
            sentence = sentence.replace("Parliament of xlid175xrid", "Parliament of Catalonia")
        if "bacon butty, bacon sarnie, rasher sandwich, bacon sanger, piece 'n bacon, bacon cob, bacon barm, or bacon muffin" in sentence:
            sentence = sentence.replace(
                "bacon butty, bacon sarnie, rasher sandwich, bacon sanger, piece 'n bacon, bacon cob, bacon barm, or bacon muffin",
                "Bacon butty, bacon sarnie, rasher sandwich, bacon sanger, piece 'n bacon, bacon cob, bacon barm, bacon muffin")
        if '"squeezed" or "smashed" chicken served with sambal' in sentence:
            sentence = sentence.replace('"squezed" or "smashed" chicken served with sambal',
                                        '"Squeezed" or "smashed" fried chicken served with sambal')
        if "Eastern Province of Sri Lanka." in sentence or "Eastern Province state of Sri Lanka" in sentence:
            sentence = sentence.replace("Eastern Province of Sri Lanka", "Eastern Province, Sri Lanka")
            sentence = sentence.replace("Eastern Province state of Sri Lanka", "Eastern Province, Sri Lanka")
        if "MIT with a Sc. D in 1963" in sentence:
            sentence = sentence.replace("MIT with a Sc. D in 1963",
                                        "Massachusetts Institute of Technology, Sc.D. 1963")
        if "in 1963 from MIT with a Sc.D" in sentence:
            sentence = sentence.replace(" in 1963 from MIT with a Sc.D",
                                        "from Massachusetts Institute of Technology, Sc.D. 1963")
        if "Huseyin Butuner and Hilmi Guner" in sentence:
            sentence = sentence.replace("Huseyin Butuner and Hilmi Guner", '"Hüseyin Bütüner and Hilmi Güner"')
        if "National Assembly." in sentence and "Azerbaijan" in sentence:
            sentence = sentence.replace("National Assembly.", "National Assembly (Azerbaijan).")

        if "Williams Anders" in sentence:
            sentence = sentence.replace("Williams Anders", 'William Anders')
        if "St.Louis" in sentence:
            sentence = sentence.replace("St.Louis", 'St. Louis')
        if "St Louis" in sentence:
            sentence = sentence.replace("St Louis", 'St. Louis')
        if "university of texas system" in sentence:
            sentence = sentence.replace("university of texas system", 'University of Texas System at Austin')
        if "University of Texas in Austin" in sentence:
            sentence = sentence.replace("University of Texas in Austin", 'University of Texas at Austin')
        if "Hook'em," in sentence:
            sentence = sentence.replace("Hook'em,", "Hook 'em (mascot),")
        if " The Hook 'em." in sentence:
            sentence = sentence.replace("The Hook 'em", "Hook 'em (mascot)")
        if "Hook'em." in sentence:
            sentence = sentence.replace("Hook'em.", "Hook 'em (mascot).")
        if "Edwin E.Aldrin Jr" in sentence or "Edwin E. Aldrin Jr." in sentence:
            sentence = sentence.replace("Edwin E.Aldrin Jr", "Edwin E. Aldrin, Jr.").replace("Edwin E. Aldrin Jr.",
                                                                                             "Edwin E. Aldrin, Jr.")

        to_l_s = []

        to_l__out = "constructionof"
        to_l_s_in = ["construction of"]
        to_l_s.append((to_l_s_in, to_l__out))


        to_l__out = "Alan B. Miller Hall"
        to_l_s_in = ["Alan B Miller Hall"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Dr. G. P. Prabhukumar"
        to_l_s_in = ["Dr G. P. Prabhukumar", "Dr G P Prabhukumar",
                     "Dr G.P. Prabhukumar",
                     "Dr. G.P. Prabhukumar",
                     "Dr g P Prabhukumar"]
        to_l_s.append((to_l_s_in, to_l__out))


        to_l__out = "A.T. Charlie Johnson"
        to_l_s_in = ["A T Charlie Johnson", "A.T Charlie Johnson", "A. T. Charlie Johnson"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "AIDS (journal)"
        to_l_s_in = ["The AIDS journal", "Aids Journal"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Ayam penyet"
        to_l_s_in = ["ayam penyet"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "1 Decembrie 1918 University"
        to_l_s_in = ["1 Decembrie 1981 University"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "UT Austin, B.S. 1955"
        to_l_s_in = ["UT Austin B.S. 1955", "UT Austin BS, in 1955"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "1_Decembrie_1918_University"
        to_l_s_in = ["1 Decembrie 1981 University"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "School of Business and Social Sciences at the Aarhus University"
        to_l_s_in = ["School of Business and Social Sciences at Aarhus University"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "30.0 g"
        to_l_s_in = ["30g"]
        to_l_s.append((to_l_s_in, to_l__out))
        to_l__out = "18.0 g"
        to_l_s_in = ["18.0g"]
        to_l_s.append((to_l_s_in, to_l__out))
        to_l__out = "1.8 g"
        to_l_s_in = ["1.8g"]
        to_l_s.append((to_l_s_in, to_l__out))
        to_l__out = "4.8 g"
        to_l_s_in = ["4.8g"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "AC Hotel Bella Sky Copenhagen"
        to_l_s_in = ["AC Hotel Bella Sky in Copenhagen"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "the length is 2900.0."
        to_l_s_in = ["the length is 29000.0."]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "British Hong Kong"
        to_l_s_in = ["Brithish Hong Kong", "British, Hong Kong"]
        to_l_s.append((to_l_s_in, to_l__out))


        to_l__out = "British Hong Kong"
        to_l_s_in = ["Brithish Hong Kong", "British, Hong Kong"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "23rd Street (Manhattan)"
        to_l_s_in = ["23rd Street, Manhattan"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Avenue A (Manhattan)"
        to_l_s_in = ["Avenue A, Manhattan"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Conservative Party (UK)"
        to_l_s_in = ["Conservative Party (United Kingdom)"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Labour Party (UK)"
        to_l_s_in = ["Labour Party (United Kingdom)"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Erie County, New York"
        to_l_s_in = ["Erie County New York", "Erie County in New York"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Asilomar Conference Grounds"
        to_l_s_in = ["Asilomar Conference Grouns"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = '"Asilomar Blvd., Pacific Grove, California"'
        to_l_s_in = ["Asilomar Blvd, Pacific Grove, California"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "DuPage County, Illinois"
        to_l_s_in = ["DuPage County in Illinois", "DuPage County Illinois"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Buffalo, New York"
        to_l_s_in = ["Buffallo New York", "Buffallo, New York"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Cuyahoga County, Ohio"
        to_l_s_in = ["Cuyahoga County in Ohio"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Eastern Province, Sri Lanka"
        to_l_s_in = ["Eastern Province Sri Lanka", "Eastern Province in Sri Lanka"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "County Limerick is part of Munster."
        to_l_s_in = ["Limerick County is part of Munster."]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Mulatu Teshome"
        to_l_s_in = ["Malatu Teshome"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Gujarat"
        to_l_s_in = ["Gujurat"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Sri Lankan rupee"
        to_l_s_in = ["Ski Lankan rupee"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Sri Lanka"
        to_l_s_in = ["Ski Lanka"]
        to_l_s.append((to_l_s_in, to_l__out))
        to_l__out = "Elizabeth II"
        to_l_s_in = ["Elizabeth 11"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "United Kingdom"
        to_l_s_in = ["United Kindgom"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "John Madin"
        to_l_s_in = ["Joh Madin"]
        to_l_s.append((to_l_s_in, to_l__out))
        to_l__out = "Bethesda, Maryland"
        to_l_s_in = ["Bethesda Maryland", "Bethseda, Maryland"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Cyrus Vance, Jr."
        to_l_s_in = ["Cyrus Vance Jr."]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Riverside Art Museum"
        to_l_s_in = ["Riverside Art Musuem"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Julia Morgan"
        to_l_s_in = ["Julie Morgan"]
        to_l_s.append((to_l_s_in, to_l__out))
        to_l__out = "Los Angeles Herald-Examiner"
        to_l_s_in = ["Los Angeles Herald Examiner"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "College of William & Mary"
        to_l_s_in = ["College of William and Mary"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "John Clancy (Labour politician)"
        to_l_s_in = ["Labour politician, John Clancy"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Wheeler, Texas"
        to_l_s_in = ["Wheeler Texas", "Wheeler in Texas"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Atatürk Monument (İzmir)'
        to_l_s_in = ["Ataturk Monument (Izmir)", "Ataturk Monument in Izmir"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = '"Türk Şehitleri Anıtı"'
        to_l_s_in = ["Turk Sehitleri Aniti",
                     "Turk Sehitler Aniti",
                     "Turk Sehitleri Anıtı",
                     "Turk Sehitleri Anıtı"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "June 1981"
        to_l_s_in = ["June in 1981"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'U.S. Route 83'
        to_l_s_in = ["United States. Route 83"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Asherton, Texas'
        to_l_s_in = ["Asherton Texas", "Asherton , Texas"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "before being retired"
        to_l_s_in = ["before retiring"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = '"from NWC, M.A. 1957"'
        to_l_s_in = ["with an M.A. from NWC"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = '"NWC, M.A. 1957"'
        to_l_s_in = ["NWC with a M.A. in 1957",
                     "NWC with an MA in 1957",
                     "NWC in 1957 with a M.A.", "NWC with a M.A. in 1957", "NWC, M.A. in 1957"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = '1634: The Ram Rebellion'
        to_l_s_in = ["1634 The Ram Rebellion"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Distinguished Service Medal (United States Navy)"
        to_l_s_in = ["Distinguished Service Medal by the U.S. Navy", "distinguished Service Medal by the US Navy",
                     "United States Navy Distinguished Service Medal", "Distinguished Service Medal by the U.S. Navy",
                     "Distinguished Service Medal of United States Navy", '" Distinguished Service Medal"']
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'The Labour Party (UK) leads Birmingham.'
        to_l_s_in = ["The Labour Party leads Birmingham."]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Historic districts in the United States'
        to_l_s_in = ["United States historic district"]
        to_l_s.append((to_l_s_in, to_l__out))
        to_l__out = 'in a Historic districts in the United States and'
        to_l_s_in = ["in a historic district and"]
        to_l_s.append((to_l_s_in, to_l__out))
        to_l__out = 'as a Historic districts in the United States and'
        to_l_s_in = ["as a historic district and"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Visvesvaraya Technological University which is in Belgaum"
        to_l_s_in = ["Visvesvaraya Technological University which is in Belgium"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Alan Shepard"
        to_l_s_in = ["Alan Shephard"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Lars Løkke Rasmussen'
        to_l_s_in = ["Lars Lokke Rasmussen"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'the current leader'
        to_l_s_in = ["the current leaderr"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Retired Buzz Aldrin'
        to_l_s_in = ["Retiree Buzz Aldrin"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Collin County, Texas'
        to_l_s_in = ["Collin County in Texas"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Glen Ridge, New Jersey"
        to_l_s_in = ["Glen Ridge New Jersey", "Glen Ridge, NJ", "Glen Ridge in New Jersey",
                     "Glen Ridge , New Jersey", "Glen Ridge New Jersey"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Aiery Neave'
        to_l_s_in = ["Airey Neave"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Knightsbridge'
        to_l_s_in = ["Kightsbridge", "Kinightsbridge"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Desteapta-te, romane!'
        to_l_s_in = ["Deșteaptă-te, române"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Department of Commerce Gold Medal'
        to_l_s_in = ["Dept of Commerce Gold Medal"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Edwin E. Aldrin, Jr."
        to_l_s_in = ["Edwin E. Aldrin Junior"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = '"Massachusetts Institute of Technology, Sc.D. 1963"'
        to_l_s_in = ["Massachusetts Institute of Technology with a Sc.D in 1963",
                     "Massachusetts Institute of Technology in 1963",
                     "Massachusetts Institute of Technology, Sc.D. in 1963"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = '16000'
        to_l_s_in = ["16,000"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = '108 St Georges Terrace'
        to_l_s_in = ["108 St. Georges Terrace"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Redefine Properties Limited'
        to_l_s_in = ["Redefine Properties Ltd"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Asian South Africans '
        to_l_s_in = ["Asian South African "]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'John Madin'
        to_l_s_in = ["John Main"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Colmore Row, Birmingham, England'
        to_l_s_in = ["103 Colmore Row, in Birmingham, England"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = '103 Colmore Row'
        to_l_s_in = ["103 Colman Row"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Aarhus'
        to_l_s_in = ["Aarus"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'B. M. Reddy'
        to_l_s_in = ["B M Reddy"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'the University of Texas System'
        to_l_s_in = ["the U of Texas system"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "2003-09-27"
        to_l_s_in = ['27th September, 2003', "9/27/2003", 'Sept. 27, 2003', '27th September, 2003']
        to_l_s.append((to_l_s_in, to_l__out))


        to_l__out = "1996-06-04"
        to_l_s_in = ['4th of June, 1996', "4 June 1996"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'University of Texas at Austin'
        to_l_s_in = ["University of Texas at Ausitin"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'University of Texas at Austin'
        to_l_s_in = ["U of Texas at Austin", "U of Texas in Austin"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = 'Mendrisio'
        to_l_s_in = ["Mendriso"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Baku Turkish Martyrs' Memorial"
        to_l_s_in = ["Baku Turkish Martyrs Memorial",
                     "Baku Turkish Martyrs memorial", "Baku Turkish Maryrs' Memorial"]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Alan B. Miller Hall"
        to_l_s_in = ["Alan B.Millar", ]

        to_l_s.append((to_l_s_in, to_l__out))
        to_l__out = "Dead Man's Plack"
        to_l_s_in = ["Dead Mans Plack", ]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Carroll County, Maryland"
        to_l_s_in = ["Carrol County, Maryland", ]
        to_l_s.append((to_l_s_in, to_l__out))

        to_l__out = "Adams County, Pennsylvania"
        to_l_s_in = ["Adams County Pennsylvania", ]
        to_l_s.append((to_l_s_in, to_l__out))

        for to_l_s_in, to_l__out in to_l_s:
            for all in to_l_s_in:
                if all in sentence:
                    sentence = sentence.replace(all, to_l__out)
        #  Sandesh (confectionery)
        #  Sandesh (confectionery)
        return sentence

    def process(self, verbose=False):
        if not os.path.isdir(REP_WEBNLG + "/data2text/data/crowdflower/xml2nn/"):
            os.makedirs(REP_WEBNLG + "/data2text/data/crowdflower/xml2nn/")
        garbage_tree = ElementTree.Element("root")
        garbage_data = {}

        dictionnaire_refus = ['Connecticut', 'Wolfsburn', 'Abubaker', 'Prudnikov',
                      'Netherlands', 'United States', 'US', 'Philippine',
                      'Alabama', 'Engish', 'Martínez', "hip hop", 'Phillip',
                      "Adams County", "Atlantic City", "Bolf", "Klink",
                      "Canadian", "Coconut", 'McQuaid', 'Ferencvarosi',
                      'English', 'Malaysia', "Alan", "Martin", "Nigerian",
                      "pudding", "Hong Kong", "bajji", "Spanish", "Izmir",
                      "American", "Ayers", "Athens", "Polish", 'Agnes',
                      'Kent',
                      "sandwich", 'Kharaitiyat', 'Prydnikov', 'Aleksandr',
                      "Georgia", "United Kingdom", "reality TV judge",
                      'Aiery',
                      "Stephens", "Zahra", "Austin", "Buzz", "Caterpillar",
                      "' B '", 'Marti', 'Aleksey', 'Chirikov', 'Kovac',
                      'Mexico',
                      "ketchup", "milk", "Anaheim", "Antioch", 'Bavarian',
                      'Phillips',
                      "Vincent St . Mary", "Ribcoff", "NY", "KY", "Dothan",
                      "Antares", "Anders", 'DeSoto', 'Desoto', 'London',
                      'Osborne',
                      "California", "Madison", "Indiana", "Antonis",
                      'Heinlen', 'Carlos', 'Juan',
                      "Samaas", "U . K .", "U.K", "New York", 'Turkish',
                      'Johnson',
                      'Jersey', 'Valis', 'Teisaire', 'Phillip', 'chicharon',
                      'morcilla', 'Karnatka', 'mushrooms',
                      'Barny', 'Livorno', 'Calcio', 'Brasileiro',
                      'Campeonato', 'Abilene', 'Abubaker', 'Kaila',
                      'Afonsa', 'Zwamemburgbaan', 'chorizo', 'garlic',
                      'olive', 'sesame', 'guanciale', 'Sebastian', 'patacones', 'Greenville', 'Auburn', 'Steuben',
                      'Illinois', 'Albany', 'Amarillo', 'DeKalb', 'Attica', 'Williamson', 'Zwamemburgbaan',
                      'Copenhagen', 'Jacabia', 'Williamson', 'Albuquerque', 'Finalnd', 'German', 'Germanany',
                      'Caterpiller', 'Taib', 'Abduk', 'Mamud', 'Tesaire', 'Arlington', 'Argentina', 'Appleton',
                      'Kharaityat', 'Turks', 'Baku', 'Torpedo', 'Moscow', 'Chirikov', 'Petrotin', 'Hop', 'Callabero',
                      'Caballero', 'Lydia', 'Guruli', 'Greek', 'Sibel', 'Bavarian', 'Aiery', 'Agnes', 'Neave',
                      'Samaras', 'Maryland', 'Gurul', 'Washing', 'America', 'Aleksandre', 'Forrest', 'Prydnikov',
                      'Romagnoli', 'Hernandez', 'Abel', 'Ferencvarosi', 'Coupe', 'Adams', 'Kharaitiyat', 'Aleksandr',
                      'Ertegun', 'Aderson', 'formerItalian', 'Mihajolovic', 'Alison', 'Donnell', 'Pennsylvania',
                      'bodhran', 'India', 'Andra', 'peyney', 'Chicharron', 'Tarrant', 'Alfa', 'Romeo', "China",
                      "Russia", " WB", "Texas", "Alex", 'Spain',
                      'Callabero', "Aleksandra", "Zealand", "Andrew", "Cesana", "Oregon"]
        total_possibilities_seen = 0


        pattern=re.compile(r'[0-9]+')

        if not self.directory.endswith("/"):
            self.directory += "/"
        self.triplets_dictionary = []

        if os.path.isfile(shared.WORKING_DATA_DIR + "triplets dictionary.json"):
            file = open(shared.WORKING_DATA_DIR + "triplets dictionary.json")
            self.triplets_dictionary = json.load(file)
            file.close()

        self.prédicats_dictionary = []
        self.working_dictionary = []
        ok_count=  0
        non_ok_count = 0
        sentences_count = 0
        filecoun=0
        file_liste = []
        for file in glob.glob(self.directory + "*/*.xml"):# [-1:]:
            filecoun += 1
            file_liste.append(file)

        ift=0
        shared.init_progress()
        if test_debug:
            a_list = set
            while len(a_list) < test_length or len(a_list)!=len(file_liste):
                a_list.append(random.choice(file_liste))
            file_liste = list(a_list)

            del a_list
            filecoun = len(file_liste)
         #   file_liste=[HOME_ROOT + "/Dropbox/benchmark_verified_manually_corrected/4triples/4triples_Building_982689_BMK_cleaned.xml"]


        eid_count={}
        for file in file_liste:
            shared.progress(ift, filecoun)
            ift   += 1
            # print(file)
            eid_count[file] = 0
            try:
                tree = ElementTree.parse(file)
                root = tree.getroot()
            except Exception as inst:
                print()
                print(file)
                print(type(inst))    #  the exception instance
                print(inst.args)     #  arguments stored in .args
                import sys
                sys.exit(5)
            lot_sentences =root.findall('entries/entry')
            i_part_sent = 0
            for entry in lot_sentences:
                i_part_sent += 1
                mtriples = entry.findall('modifiedtripleset/mtriple')
                lexs = entry.findall("lex[@comment='good']")
                eid = entry.attrib['eid']
                eid_count[file] +=1
                triplets = []
                sentences = []
                stop=False
                for mtriple in mtriples:
                    triplets.append(mtriple.text.split(" | "))
                    if len(triplets[-1]) != 3:
                        if verbose:
                            print("*")
                            print(triplets)
                        stop=True
                triplets = sorted(triplets, key=lambda contenu: contenu[1]+contenu[0]+contenu[2])
                if not stop:
                    lids = []
                    for lex in lexs:
                        #des fois qu'il y ait des doublons non perçus...
                        if lex.text not in sentences :
                            sentences.append(lex.text)
                            lids.append(lex.attrib['lid'])
                    if len(sentences)>0:
                        final_sentences = []
                        sources_dones = []
                        triplets_parsed = []
                        final_lids = []
                        dates = []
                        for triplet in triplets:
                            if triplet[0] not in self.triplets_dictionary:
                                self.triplets_dictionary.append(triplet[0])
                            if triplet[2] not in self.triplets_dictionary:
                                self.triplets_dictionary.append(triplet[2])
                            if triplet[1] not in self.prédicats_dictionary:
                                self.prédicats_dictionary.append(triplet[1])
                            triplets_parsed.append(triplet[0])
                            triplets_parsed.append(triplet[2])
                            if "date" in triplet[1].lower() or \
                                    "shipOrdered".lower() in triplet[1].lower() or \
                                    'added to the National Register of Historic Places'.lower() in triplet[1].lower() or \
                                    "shipLaidDown".lower() in triplet[1].lower() or \
                                    "shipLaunch".lower() in triplet[1].lower() or \
                                    "discovered".lower() in triplet[1].lower() or \
                                    "firstAired".lower() in triplet[1].lower() or \
                                    "lastAired".lower() in triplet[1].lower() or \
                                    "maidenVoyage".lower() in triplet[1].lower() or \
                                    "christeningDate".lower() in triplet[1].lower() or \
                                    "activeYearsStartDate".lower() in triplet[1].lower() or \
                                    "epoch" in triplet[1].lower() or 'established' in triplet[1].lower() or\
                                            triplet[1] in ["maidenFlight", "finalFlight", "shipInService"]:
                                dates.append(triplet[2])
                            elif triplet[1] not in ('ISSN_number', 'transportAircraft', 'shipPower', 'leaderTitle', 'league', 'orbitalPeriod', 'formerName', 'engine', 'club', 'fullname', 'location', 'address', 'foundedBy', 'LibraryofCongressClassification', 'region', 'runwayName', 'office (workedAt , workedAs)') \
                                    and  len(list(datefinder.find_dates(triplet[2], source= True,  strict=False))) >0 :
                                print("***")
                                print("date détectée, triplets néanmoins traité sans recherche de date.")
                                print(triplet)
                                print(lex.text)
                                print("***")

                        triplets_parsed=list(set(triplets_parsed))
                        triplets_parsed.sort(key=len, reverse=True)
                        potential_garbage_node = []

                        i_tmp_sent = 0

                        for sentence, lid in zip(sentences, lids):
                            i_tmp_sent += 1
                            total_possibilities_seen += 1
                            initial_sentence = sentence
                            tmp_triplet = triplets

                            infoCourante = "lexic. "
                            infoCourante += lid.__str__().zfill(1)
                            infoCourante += "/"
                            infoCourante += len(sentences).__str__().zfill(1)
                            infoCourante += " lot "
                            infoCourante += i_part_sent.__str__().zfill(2)
                            infoCourante += "/"
                            infoCourante += len(lot_sentences).__str__().zfill(2)
                            infoCourante += " fichier "
                            infoCourante += ift.__str__().zfill(3)
                            infoCourante += "/"
                            infoCourante += len(file_liste).__str__().zfill(3)
                            infoCourante = len(succes_recherche.keys()).__str__().zfill(5) + "/" + total_possibilities_seen.__str__().zfill(5) + " " + infoCourante
                            tmps1 = time.time()

                            if initial_sentence+eid+lid in succes_recherche.keys() and "xlid" in succes_recherche[initial_sentence+eid+lid][0]:
                                replaced_non_ok = 0
                                final_sentence, sentence, copy_sentence, lid, tmp_triplet = succes_recherche[initial_sentence+eid+lid]

                                if sentence == "" or "xlid" not in sentence:
                                    sentence = final_sentence
                                if "xlid" in copy_sentence:
                                    copy_sentence = initial_sentence
                                shared.progress(ift, filecoun, text=infoCourante)
                            else :
                                test_all_orders = [triplets_parsed]
                                if forcing == 2:
                                    if len(triplets_parsed)<=5:
                                        test_all_orders = list(itertools.permutations(triplets_parsed))
                                        random.shuffle(test_all_orders)
                                    else:
                                        test_all_orders = []
                                        while len(test_all_orders) < nb_combinaisons:
                                            random.shuffle(triplets_parsed)
                                            test_all_orders.append(triplets_parsed[:])
                                elif forcing == 1:
                                    random.shuffle(test_all_orders[0])

                                tmp_test_all = test_all_orders[: ]
                                for i_all, tmp_test_ind in enumerate(tmp_test_all):
                                    removed = False
                                    for ia, a in enumerate(tmp_test_ind[0:-1]):
                                        a = a[:]
                                        a = a.replace("'", "")
                                        a = a.replace('"', "")
                                        for b in tmp_test_ind[ia+1:]:
                                            b = b.replace("'", "")
                                            b = b.replace('"', "")
                                            if "(" in b:
                                                b = b[0:b.index("(")]
                                            if a.lower() in b[0:].lower():
                                                test_all_orders.remove(tmp_test_ind)
                                                removed = True
                                                break
                                        if removed:
                                            break

                                i_triple_state = 0

                                replaced_non_ok = -1
                                trait_case = {}
                                #infoCourante = "bunch "
                                #infoCourante += i_triple_state.__str__().zfill(z_fill_combi) + "/" + (
                                #    len(test_all_orders[0:nb_combinaisons])).__str__().zfill(z_fill_combi)
                                #infoCourante += " "


                                shared.progress(ift, filecoun, text=infoCourante)

                                for i_parsed_triplets, triplets_parsed in enumerate(test_all_orders[0:nb_combinaisons]):
                                    if replaced_non_ok == 0 or time.time()-tmps1 > 25:
                                        if replaced_non_ok > 0:
                                            print("***")
                                            print("phrase difficile à parser.")
                                            print(triplet)
                                            print(initial_sentence)
                                            print("***")
                                        break

                                    i_triple_state += 1

                                    if "xlid" not in sentence:
                                        copy_sentence = sentence
                                    else :
                                        copy_sentence = initial_sentence
                                    change=False

                                    sentence=self.sentence_equive(sentence)
                                    dones = []
                                    replaced_non_ok=0
                                    out = False
                                    for item in triplets_parsed:
                                        if time.time()-tmps1 > 15:
                                            replaced_non_ok = 0
                                            for item in triplets_parsed:
                                                if "xlid" + self.triplets_dictionary.index(item).__str__() + "xrid" not in sentence:
                                                    replaced_non_ok += 1

                                            if replaced_non_ok ==0:
                                                sentence_block = nltk.wordpunct_tokenize(sentence)
                                                for slot in sentence_block:
                                                    if "xlid" in slot and (not slot.endswith("xrid") or not slot.startswith("xlid")):
                                                        replaced_non_ok += 1

                                            if replaced_non_ok ==0:
                                                out = True
                                            break
                                        #on n'a pas encore testé le raccourcissement de la chaîne (ex: si keyboard instrument non trouvé, test de keyboard)
                                        cut = False
                                        traited_case=item[:]
                                        if traited_case not in trait_case:
                                            trait_case[traited_case] = True

                                        erreurs_recherche_tmp=[]
                                        if item in erreurs_recherche.keys():
                                            erreurs_recherche_tmp=copy.deepcopy(erreurs_recherche[item])
                                            if len(erreurs_recherche_tmp) > 0:
                                                i = len(erreurs_recherche_tmp)
                                                while i > 0:
                                                    if erreurs_recherche_tmp[i - 1][0] == item.replace("_", " "):
                                                        del erreurs_recherche_tmp[i - 1]
                                                    elif erreurs_recherche_tmp[i - 1][1] != copy_sentence and not erreurs_recherche_tmp[i - 1][1]:
                                                        del erreurs_recherche_tmp[i - 1]
                                                    i -= 1
                                        pre_done = True
                                        if item in dates:
                                            if item in sentence:
                                                searched = item
                                                pre_done = False
                                            else:
                                                try:
                                                    matches = list(datefinder.find_dates(sentence, source= True,  strict=False))
                                                    date_search = list(datefinder.find_dates(item, strict=True))[0]
                                                    for date_1, date_str in matches:
                                                        if date_search == date_1:
                                                            searched = date_str
                                                            pre_done = False
                                                            break
                                                except:
                                                    pre_done = True
                                        if pre_done:
                                            searched=item.replace("_", " ")
                                            if item == "Conservative_Party_(UK)":
                                                if " (United Kingdom)" not in sentence:
                                                    searched="Conservative Party"
                                            searched = self.sentence_equive(searched)

                                            sentence = self.couple_equive(item, sentence)

                                            if "(ethnic group)" in item:
                                                searched = searched.replace(" (ethnic group)", "")

                                        continue_search=True
                                        idval = "xlid" + self.triplets_dictionary.index(item).__str__() + "xrid"

                                        evacuation=False

                                        last_procedure = False
                                        while continue_search and not out and not time.time()-tmps1 > 15:
                                            #on place dans dones quand on a trouvé, si on cherche encore
                                            #si le mot n'était pas tout à fait dans la phrase, la phrase a été modifiée en conséquence
                                            if searched not in dones:
                                                searched = searched.replace("United States.", "United States")
                                                # numbers=pattern.findall(sentence)
                                                # les soucis de replacement dans l'identifiant triplet ne peuvent toucher que des nombres, sauf des entités...
                                                # bon, par souci de simplicité, on ne fait que sur entité entières un re.
                                                searched_last = item.replace("_", " ")
                                                found_similar = False
                                                a_bit_found_similar = False
                                                if i_parsed_triplets < 3 or random.randint(1, 2) == 1:
                                                    start = 0
                                                    end = len(searched_last.split(' ')) + 4

                                                    # if searched_last.split(' ')[-2].endswith(","):
                                                    # "soupçon d'une liste que la lexicalisation reprendrait avec un and
                                                    # start=1
                                                    # end = 0
                                                    # on va plutôt prendre le plus similaire et puis basta
                                                    similarité = 0
                                                    séquence = ""
                                                    socle = sentence.split(" ")

                                                    taux_similarité = 0.88


                                                    while taux_similarité == 0.88 or found_similar:
                                                        for start in range(0, len(socle)):
                                                            for min_fenster in range(1, end):
                                                                taille_fenetre = min_fenster

                                                                comparé = " ".join(socle[start:start + taille_fenetre])

                                                                if len(re.findall(xlridpattern, comparé)) == 0:

                                                                    tmp_socle = socle[start:start + taille_fenetre]
                                                                    res_socle = []
                                                                    need_try = False
                                                                    for i_s, s_s in enumerate(tmp_socle):
                                                                        b_append = False
                                                                        if s_s.lower() in stop_set:
                                                                            if len(res_socle) == 0:
                                                                                b_append = True
                                                                            elif res_socle[-1] in stop_set:
                                                                                b_append = True
                                                                            if i_s == len(tmp_socle) -1:
                                                                                b_append = True
                                                                            elif tmp_socle[i_s + 1] in stop_set:
                                                                                b_append = True
                                                                        else:
                                                                            b_append = True

                                                                        if b_append:
                                                                            res_socle.append(s_s)
                                                                            need_try = True

                                                                    if need_try:
                                                                        tmp_sim = shared.similar(searched_last.rstrip('\'\"-,.:;!?').lower(), " ".join(res_socle).rstrip('\'\"-,.:;!?').lower())

                                                                        if tmp_sim > taux_similarité:  # +min_fenster/2: en testant toute les combinaisons, c'est négative de contraindre
                                                                            if tmp_sim > similarité :
                                                                                similarité = tmp_sim
                                                                                séquence = comparé
                                                                                found_similar = True


                                                        if found_similar:
                                                            do_break = False
                                                            # on veut éviter les remplacements au sein des xlid et au milieu de mots.
                                                            if séquence[-1] in string.punctuation:
                                                                if searched_last[-1] not in string.punctuation or searched_last[-1] != séquence[-1]:
                                                                    while séquence[-1] in string.punctuation:
                                                                        séquence=séquence[0:-1]
                                                                        if searched_last[-1] == séquence[-1]:
                                                                            break
                                                            if séquence[0] in string.punctuation:
                                                                if searched_last[0] not in string.punctuation or searched_last[0] != séquence[0]:
                                                                    while séquence[0] in string.punctuation:
                                                                        séquence=séquence[1:]
                                                                        if searched_last[0] == séquence[0]:
                                                                            break

                                                            if séquence.endswith('\'') or séquence.endswith('\'s'):
                                                                if not searched_last.endswith('\'') and not searched_last.endswith('\'s'):
                                                                    if séquence.endswith('\'s'):
                                                                        séquence = séquence[0:-2]
                                                                    elif séquence.endswith('\''):
                                                                        séquence = séquence[0:-1]

                                                            for short_stop in stop_set:
                                                                # First parameter is the replacement, second parameter is your input string
                                                                tmp_search = searched_last[:]
                                                                tmp_search = regexAZ.sub('', tmp_search)
                                                                if len(short_stop) > 1:
                                                                    if séquence.startswith(short_stop + " "):
                                                                        if not tmp_search.startswith(short_stop):
                                                                            séquence = séquence[len(short_stop)+1:].strip()
                                                                    if séquence.endswith(" " + short_stop):
                                                                        if not tmp_search.endswith(short_stop):
                                                                            séquence = séquence[:-len(short_stop)].strip()

                                                            similarité = 0
                                                            pattern = re.compile(r'(?<![d0-9])' + re.escape(séquence),
                                                                                 re.IGNORECASE)
                                                            results_pattern = re.subn(pattern, idval, sentence)
                                                            if results_pattern[1] > 0:
                                                                sentence = results_pattern[0]
                                                                if not a_bit_found_similar and not (("(" not in searched_last and ")" in searched_last ) or (")" not in searched_last and "(" in searched_last )) :
                                                                    searched = searched_last
                                                                    dones.append(searched_last)
                                                                a_bit_found_similar = True
                                                                break
                                                            else:
                                                                found_similar = False
                                                        if taux_similarité <= 0.88:
                                                            taux_similarité = 0.9
                                                        elif taux_similarité < 0.92:
                                                            taux_similarité = 0.95

                                                if not a_bit_found_similar:
                                                    if re.search(compile(searched), sentence):
                                                        sentence = re.sub(compile(searched), idval, sentence)
                                                        dones.append(searched)
                                                    elif searched in sentence:
                                                        sentence = sentence.replace(searched, idval)
                                                        dones.append(searched)
                                                    elif not evacuation and '"' in searched and (
                                                            searched.replace('"', "") in sentence or searched.replace('"',
                                                                                                                      "").lower() in sentence.lower()):
                                                        if " " + searched.replace('"', "") in sentence:
                                                            sentence = sentence.replace(" " + searched.replace('"', ""),
                                                                                        " " + idval)  # re.sub(compile(searched.replace('"', "")), idval, sentence)
                                                            dones.append(searched)
                                                        elif "." + searched.replace('"', "") in sentence:
                                                            sentence = sentence.replace("." + searched.replace('"', ""),
                                                                                        ". " + idval)  # re.sub(compile(searched.replace('"', "")), idval, sentence)
                                                            dones.append(searched)
                                                        elif "," + searched.replace('"', "") in sentence:
                                                            sentence = sentence.replace("," + searched.replace('"', ""),
                                                                                        ", " + idval)  # re.sub(compile(searched.replace('"', "")), idval, sentence)
                                                            dones.append(searched)
                                                        elif "-" + searched.replace('"', "") in sentence:
                                                            sentence = sentence.replace("-" + searched.replace('"', ""),
                                                                                        " - " + idval)  # re.sub(compile(searched.replace('"', "")), idval, sentence)
                                                            dones.append(searched)
                                                        else:
                                                            results_pattern = compile(searched.replace('"', "")).findall(sentence)
                                                            if len(results_pattern) == 0:
                                                                results_pattern = compile(searched.replace('"', ""),
                                                                                          re.IGNORECASE).findall(sentence)
                                                            if len(results_pattern) > 0:
                                                                for bla in results_pattern:
                                                                    sentence = sentence.replace(bla,
                                                                                                "xlid" + self.triplets_dictionary.index(item).__str__() + "xrid")
                                                                dones.append(searched)
                                                            else:
                                                                # ce qui suit ne me plaît pas du tout, donc je commente. C'est pas mal pour les termes omis, mais souvent (ex: (UK)) ils se retrouvent dans le texte sous une autre forme.
                                                                #  if searched.endswith(")") and searched.index('(')>1:
                                                                #      if searched.split("(")[0].strip() in sentence:
                                                                #          sentence = sentence.replace(searched.split("(")[0].strip(),
                                                                #                              "xlid" + self.triplets_dictionary.index(
                                                                #                                  item).__str__() + "xrid")
                                                                #          dones.append(searched)
                                                                #  else:
                                                                # le mot n'existe pas sous forme entière, cela est au cahier des charges de gérer par entité, et non au caractère.
                                                                # on évacue.
                                                                evacuation = True

                                                    elif re.search(compile(searched.replace('.', "")), sentence):
                                                        sentence = re.sub(compile(searched.replace('.', "")), idval, sentence)
                                                        dones.append(searched)
                                                    elif re.search(compile(searched.replace('Kingdom of France', "France")),
                                                                   sentence):
                                                        sentence = re.sub(compile(searched.replace('Kingdom of France', "France")),
                                                                          idval, sentence)
                                                        dones.append(searched)
                                                    elif "  " in searched:
                                                        searched = searched[0:searched.index("  ")]
                                                        if re.search(compile(searched.replace('.', "")), sentence):
                                                            sentence = re.sub(compile(searched.replace('.', "")), idval, sentence)
                                                            dones.append(searched)
                                                    else:
                                                        searched = searched.replace(" language", "")
                                                        pattern = re.compile(r'(?!\B)' + re.escape(searched) + '(?!\B)',
                                                                             re.IGNORECASE)  # re.compile(r'\b'+re.escape(searched)+"$", re.IGNORECASE)
                                                        results_pattern = pattern.findall(sentence)
                                                        if len(results_pattern) == 0:
                                                            searched = data.strip_accents(searched)
                                                            pattern = re.compile(r'(?!\B)' + re.escape(searched) + '(?!\B)',
                                                                                 re.IGNORECASE)  # re.compile(r'\b'+re.escape(searched)+"$", re.IGNORECASE)
                                                            results_pattern = pattern.findall(sentence)

                                                        if len(results_pattern) > 0:
                                                            for bla in results_pattern:
                                                                sentence = sentence.replace(bla,
                                                                                            "xlid" + self.triplets_dictionary.index(item).__str__() + "xrid")
                                                            dones.append(searched)
                                                        elif "-" in searched and searched.replace("-", " ") in sentence:
                                                            sentence = sentence.replace(searched.replace("-", " "), idval)
                                                            dones.append(searched)
                                                        elif "-" in searched and searched.replace("-", "") in sentence:
                                                            sentence = sentence.replace(searched.replace("-", ""), idval)
                                                            dones.append(searched)
                                                        else:
                                                            # bon ça c'est optimisable...
                                                            if searched.endswith('.00'):
                                                                searched = searched.replace(".00", ".0")
                                                            elif "0 " in searched and ".0" not in searched and ".0" in sentence:
                                                                searched = searched.replace("0 ", "0.0 ")
                                                            elif searched.endswith('.0') and ".0" not in sentence:
                                                                searched = searched.replace(".0", "")
                                                            elif ".0" in searched and ".0" not in sentence:
                                                                searched = searched.replace(".0", "")
                                                            elif "(" in searched and ")" in searched and ('the '+ (searched[searched.index("(")+1:searched.index(")")]+ ' ' + searched[0:searched.index("(")].strip()).lower() in sentence.lower()):
                                                                searched = 'the '+ (searched[searched.index("(")+1:searched.index(")")]+ ' ' + searched[0:searched.index("(")].strip()).lower()
                                                                if searched in sentence:
                                                                    sentence = sentence.replace(searched, "xlid" + self.triplets_dictionary.index(item).__str__() + "xrid")
                                                                    dones.append(searched)

                                                            elif "(" in searched and ")" in searched and "(" not in sentence and ")" not in sentence:
                                                                test_where = searched.split(" ")[-1].replace("(", "").replace(")", "").strip()
                                                                what =  (" ".join(searched.split(" ")[0:-1])).replace("(", "").replace(")", "").strip()
                                                                if (what + " in " + test_where).lower() in sentence.lower():
                                                                    searched = what + " in " + test_where
                                                                elif (what + ", " + test_where).lower() in sentence.lower():
                                                                    searched = what + ", " + test_where
                                                                elif (what + "," + test_where).lower() in sentence.lower():
                                                                    searched = what + "," + test_where
                                                                elif (test_where + " " + what).lower() in sentence.lower():
                                                                    searched = test_where + " " + what
                                                                elif "music" in searched and len(searched.replace("music", '').strip()) >= 3 and searched.replace("music", '').strip().lower() in sentence.lower():
                                                                    searched = searched.replace("music", '').strip()
                                                                else:
                                                                    old_s = searched
                                                                    searched = searched.replace("(", "").replace(")", "").replace("  ", " ").strip()
                                                                    if searched.lower() not in sentence.lower():
                                                                        searched = old_s.replace("(", " ").replace(")", "").strip()
                                                                    if searched.lower() not in sentence.lower():
                                                                        searched = old_s[0:old_s.index("(")].strip()
                                                                    # if searched.lower() in sentence.lower():
                                                                    #     first = old_s[0:old_s.index("(")].strip()
                                                                    #     nextfirst = old_s[old_s.index("(")+1:].strip()
                                                                    #     if nextfirst[1:].lower() != nextfirst[1:]:
                                                                    #         #le mot contient une majuscule
                                                                    #         print()
                                                                    #         print((first, nextfirst, sentence))

                                                                if " " + searched in sentence:
                                                                    sentence = sentence.replace(" " + searched, " xlid" + self.triplets_dictionary.index(item).__str__() + "xrid")
                                                                    dones.append(searched)
                                                            elif searched.endswith(","):
                                                                searched = searched[0:-1].strip()
                                                            elif ", " in searched:
                                                                searched=searched.replace(", ", ",")
                                                            elif "," in searched:
                                                                if searched.replace(",", " ").replace("  ", " ") in sentence:
                                                                    searched = searched.replace(",", " ").replace("  ", " ")
                                                                    sentence = sentence.replace(searched, " xlid" + self.triplets_dictionary.index(item).__str__() + "xrid")
                                                                    dones.append(searched)
                                                                else:
                                                                    searched=searched.replace(",", " ")
                                                            elif searched.count(" ")>0 and cut==False:
                                                                subelement=searched.split(" ")[-1]
                                                                if subelement.replace("(", "").replace(")", "").lower() not in sentence.lower():
                                                                    searched = " ".join(searched.split(" ")[0:-1])
                                                                cut = True
                                                            else:
                                                                if searched not in dones:
                                                                    replaced_non_ok += 1
                                                                    unable_to_treat = True

                                                                    if unable_to_treat:
                                                                        if not os.path.basename(file) in garbage_data.keys():
                                                                            garbage_data[os.path.basename(file)] = []
                                                                            garbage_data[os.path.basename(file)].append(
                                                                                ElementTree.SubElement(garbage_tree, "file", path=file))
                                                                            garbage_data[os.path.basename(file)].append({})
                                                                            garbage_data[os.path.basename(file)].append([])
                                                                    if item not in erreurs_recherche_bis.keys():
                                                                        erreurs_recherche_bis[item] = []

                                                                    if trait_case[traited_case]:
                                                                        trait_case[traited_case] = False
                                                                        erreurs_recherche_bis[item].append((item.replace("_", " "),
                                                                                                        copy_sentence, False,
                                                                                                        {'fichier': file,
                                                                                                         'eid': eid, 'lid': lid,
                                                                                                         'so': triplets_parsed}))

                                                                    continue_search = False
                                                else:
                                                    continue_search = False
                                            else:
                                                continue_search = False

                                        replaced_non_ok = 0
                                        for item in triplets_parsed:
                                            if "xlid" + self.triplets_dictionary.index(item).__str__() + "xrid" not in sentence:
                                                replaced_non_ok += 1

                                        if replaced_non_ok ==0:
                                            sentence_block = nltk.wordpunct_tokenize(sentence)
                                            for slot in sentence_block:
                                                if "xlid" in slot and (not slot.endswith("xrid") or not slot.startswith("xlid")):
                                                    replaced_non_ok += 1

                                        if replaced_non_ok ==0:
                                            out = True

                                    if replaced_non_ok == 0:
                                        for word in dictionnaire_refus:
                                            if word in sentence:
                                                replaced_non_ok += 1
                                                if replaced_non_ok == 1  and initial_sentence not in outed:
                                                    print("***")
                                                    pprint(triplets_parsed)
                                                    print("phrase non propre --> " + word)
                                                    print(sentence)
                                                    if '"' not in initial_sentence:
                                                        print('grep -rin "' + initial_sentence + '" | cut -d : -f 1 | xargs gedit '  )
                                                    print("***")
                                                    outed.add(initial_sentence)
                                                    
                                        if sentence.count('(') != sentence.count(')'):
                                            replaced_non_ok += 1

                            if replaced_non_ok == 0 and "xlid" in sentence:
                                sentence = " ".join(nltk.wordpunct_tokenize(sentence.replace("  ", " ")))
                                copy_sentence = copy_sentence.replace("  ", " ")
                                final_sentence = sentence.replace(", and", " and ")
                                final_sentence = final_sentence.replace("  ", " ")
                                if initial_sentence+eid+lid not in succes_recherche.keys() :
                                    if final_sentence == sentence:
                                        sentence = ""
                                    succes_recherche[initial_sentence+eid+lid] = (final_sentence, sentence, copy_sentence, lid, triplets)
                                    if sentence == "":
                                        sentence = final_sentence
                                # si je n'ai pas la phrase pour un même lot de triplets
                                if final_sentence not in final_sentences and sorted(tmp_triplet, key=lambda contenu: contenu[1]+contenu[0]+contenu[2])==sorted(triplets, key=lambda contenu: contenu[1]+contenu[0]+contenu[2]):
                                    final_sentences.append(final_sentence)
                                    sources_dones.append(copy_sentence)
                                    final_lids.append(lid)
                                elif final_sentence not in final_sentences:
                                   self.working_dictionary.append((tmp_triplet, [copy_sentence], [final_sentence], [lid],
                                                                   os.path.basename(file), eid))
                            else:
                                if os.path.basename(file) in garbage_data.keys():
                                    fichier = garbage_data[os.path.basename(file)][0]

                                    if copy_sentence not in garbage_data[os.path.basename(file)][2]:
                                        garbage_data[os.path.basename(file)][2].append(copy_sentence)
                                    if not entry.attrib['eid'].__str__() + entry.attrib['category'].__str__() in \
                                            garbage_data[os.path.basename(file)][1].keys():
                                        entrée = \
                                            garbage_data[os.path.basename(file)][1][
                                                entry.attrib['eid'].__str__() + entry.attrib['category'].__str__()] = \
                                            ElementTree.SubElement(fichier, "entry", eid=entry.attrib['eid'],
                                                                   category=entry.attrib['category'],
                                                                   size=entry.attrib['size'])
                                        triples = ElementTree.SubElement(entrée, "tripleset")
                                        for t in triplets:
                                            ElementTree.SubElement(triples, "triple").text = " | ".join(t)

                                    entrée = garbage_data[os.path.basename(file)][1][entry.attrib['eid'].__str__() + entry.attrib['category'].__str__()]

                                    b = ElementTree.SubElement(entrée, "lex", lid=lid.__str__())
                                    ElementTree.SubElement(b, "valeur").text = copy_sentence
                                    garbage_data[os.path.basename(file)][2].append(copy_sentence)

                                    not_found = ElementTree.SubElement(b, "missing")
                                    found = ElementTree.SubElement(b, "found")
                                    for item in triplets_parsed:
                                        if "xlid" + self.triplets_dictionary.index(item).__str__() + "xrid" not in sentence:
                                            attribs = {}
                                            attribs['id'] = "xlid" + self.triplets_dictionary.index(item).__str__() + "xrid"
                                            attribs['value'] = item
                                            ElementTree.SubElement(not_found, "element", attribs)
                                        else:
                                            attribs = {}
                                            attribs['id'] = "xlid" + self.triplets_dictionary.index(item).__str__() + "xrid"
                                            attribs['value'] = item
                                            ElementTree.SubElement(found, "element", attribs)


                                    entrée = garbage_data[os.path.basename(file)][1][entry.attrib['eid'].__str__() + entry.attrib['category'].__str__()]
                                    tmp_lex = entrée.find("lex[@lid='"+lid.__str__()+"']")

                                    ElementTree.SubElement(tmp_lex, "valeur").text = sentence.replace(", and", ",").replace(",and", ",")


                        if len(final_sentences)>0:
                            self.working_dictionary.append((triplets, sources_dones, final_sentences, final_lids, os.path.basename(file), eid))
                        non_ok_count += len(sentences)-len(final_sentences)
                        ok_count += len(final_sentences)
                        sentences_count += len(sentences)
                        self.all_worked_sentences += final_sentences
                        self.all_basics_sentences += sources_dones


                else:
                    triplets.pop(-1)

        print()
        print("détails eid: ", end = '')

        shared.progress(1, 1)
        shared.init_progress()
        print()
        print("exclus: ", end='')
        print(non_ok_count)
        print("inclus: ", end='')
        print(ok_count)
        print("lexicalisations: ", end='')
        print(sentences_count)


        if False:
            fjson = open(ERREURS_RECHERCHE, "w")
            fjson.write(json.dumps(erreurs_recherche_bis, indent=1, ensure_ascii=False))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True

        fjson = open(SUCCES_RECHERCHE, "w")
        fjson.write(json.dumps(succes_recherche, indent=1, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        if not stanford_parser:
            exit()


        bs = BeautifulSoup(ElementTree.tostring(garbage_tree, method="xml"), 'xml')
        text_re = re.compile('>\n\s+([^<>\s].*?)\n\s+</', re.DOTALL)
        prettyXml = text_re.sub('>\g<1></', bs.prettify())

        f = open(shared.WORKING_DATA_DIR + "entities_not_found.xml", 'w')

        f.write(prettyXml)
        f.close()

        print("erreurs recherches sauvegardés")
        print("ok")
        if ONLY_ERROR_FILE:
            import  sys
            sys.exit()

        del garbage_tree
        del garbage_data

#  stanford_parser=True
try:
    nlp = StanfordCoreNLP(SERVEUR_CORENLP)
    text = ('Alan Bean was born on the "1932-03-15" and his Alma Mater is "UT Austin, B.S. 1955".')
    text = (
        'Alan Bean was born on the 15th of March 1932 and his Alma Mater is UT Austin, B.S. 1955.'
        'Alan Bean was born on the 15th of March 1932. His Alma Mater is UT Austin, B.S. 1955.'
        "Alan Bean was born on the 15th of March 1932 and his Alma Mater is UT Austin, B.S. 1955.")
    tmp_output = nlp.annotate(text, properties = {
        'annotators': 'tokenize,pos,depparse,parse,ner',
        'outputFormat': 'json'
        })
      #t1 = Tree.fromstring(tmp_output['sentences'][0]['parse'])
      #t2 = Tree.fromstring(tmp_output['sentences'][1]['parse'])
except:
    try:
        SERVEUR_CORENLP = 'http://localhost:5000'
        nlp = StanfordCoreNLP('http://localhost:5000')
        tmp_output = nlp.annotate(text,
                                  properties={'annotators': 'tokenize,pos,depparse,parse,ner', 'outputFormat': 'json'})
        print(("Serveur changé pour localhost"))
    except:
        print("Vérifier serveur stanford")
        exit()

# pprint(t1.productions())
# pprint(t2.productions())
analyzed_sources_done = {}
analyzed_sources_done_dupl = {}

JSON_DATA_FILE = shared.HOME_ROOT + "stanford.json"
print("chargement json")
try:
    if os.path.isfile(JSON_DATA_FILE):
        file = open(JSON_DATA_FILE)
        analyzed_sources_done = json.load(file)
        file.close()
except:
    print("no stanford json file openable.")



# on a gardé les strcutures correctement parsées. Mais on fait l'appairage, en prenant en plus les coreference.
if __name__ == '__main__':


    #data = Data(REP_WEBNLG + "/data2text/data/crowdflower/benchmark_verified_manually_corrected/")
    data = Data(shared.HOME_ROOT + "Dropbox/benchmark_verified_manually_corrected/")
    print(shared.HOME_ROOT + "Dropbox/benchmark_verified_manually_corrected/")

    data.process()

    fjson = open(shared.WORKING_DATA_DIR + "triplets dictionary.json", "w")
    fjson.write(json.dumps(data.triplets_dictionary, indent=2, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True
    print("sauvegarde dictionnaire réalisée")
    print("ok")

    fjson = open(shared.WORKING_DATA_DIR + "sentences.json", "w")
    fjson.write(json.dumps(data.working_dictionary, indent=2, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True
    print("sauvegarde phrases parsées réalisée")
    print("ok")

    # on va stocker les phrases présentant des soucis de parsing.
    #if not os.path.isdir(REP_WEBNLG + "/data2text/data/crowdflower/xml2nn/"):
    #    os.makedirs(REP_WEBNLG + "/data2text/data/crowdflower/xml2nn/")
    garbage_tree = ElementTree.Element("root")

    keeped_tree =  ElementTree.Element("root")

    tree= ElementTree.ElementTree()
    tree._setroot(keeped_tree)
    # un même nom de fichier va nous être ramené successivement dans la boucle. La phrase précédente pourra être réécrite,
    # mais on va garder les index de fichier comme précédemment, où inexpérimentée j'avais géré ainsi plutôt que créer les
    # noeuds aux niveaux qui vont bien, ce qui donnerait un code plus lisible
    garbage_data = {}
    keeped_data = {}


    def verify_tokens(value, tokens, xlid, transford_sentences, based_words):
        value_strim =  value.replace(" ", "").lower()
        based_words_strim =  based_words.replace(" ", "").lower()

        words=""

        for token in tokens:
            words += token["originalText"]# .replace("")# '' (140519528467568)

        if words.lower()==value_strim.lower():
            return "equal"
        elif words.replace('\'', '`').replace("``", '').replace('"', '')==value_strim.replace('\'', '`').replace("``", '').replace('"', ''):
            return "equal"
        else:
            if len(tokens)==1 and tokens[0]['pos'] == "PRP":
                return 'pronominalization'
            elif len(tokens)==1 and tokens[0]['pos'] == "PRP$":
                return 'possessive pronominalization'
            elif based_words_strim in words.lower():
                return "different"
            else:
                ref_tmp=transford_sentences[transford_sentences.index(xlid):].replace(xlid, "")
                if len(ref_tmp)==0:
                    return 'reference'
                elif ref_tmp[0] not in [" ", ".", ":", "!", "?", ","]:
                    return "different"
                else:
                    return 'reference'

                print()
                print(value_strim)
                print(words)
                print(tokens)
                print()
        return "different"

    i_w=0
    eid_vides = {}
    count_no_error=0
    shared.init_progress()

    properties = {"pipelineLanguage"	: "en",
                        'annotators': 'tokenize,pos,ner,depparse,parse,coref',
                        'outputFormat': 'json'}
    for triplets, sources_dones, final_sentences, final_lids, fichier, eid in data.working_dictionary:
        shared.progress(i_w, len(data.working_dictionary))
        i_w += 1
        why_bad = {}
        bads_lids = []# identifiants des phrases qu'on aura eu du mal à parser et qui sont abandonnées.
        bads_items = []
        subjects_objects = {}
        encoded_triplets = []
        for item in triplets:
            val1= "xlid"+data.triplets_dictionary.index(item[0]).__str__()+"xrid"
            val2= "xlid"+data.triplets_dictionary.index(item[2]).__str__()+"xrid"

            if val1 not in subjects_objects.keys():
                subjects_objects[val1]=item[0].replace("_"," ")
            if val2 not in subjects_objects.keys():
                subjects_objects[val2] = item[2].replace("_"," ")
            # graph.add_node(item[0])
            # graph.add_node(item[2])
            encoded_triplets.append([val1, item[1], val2])

        for final_sentence, final_lid in zip(final_sentences, final_lids):

            source_done = final_sentence.replace("/", " / ")

            items = {}

            searchedItems=re.findall(xlridpattern, source_done, re.IGNORECASE)

            # on traite les clés par ordre d'apparition, sinon cela va nous fausser les index.
            for key in searchedItems:
                if key not in subjects_objects.keys():
                    print((source_done, key, subjects_objects))
                    print((triplets, sources_dones, final_sentences, final_lids, fichier, eid))
                value= subjects_objects[key]

                l_charsetOffset=source_done.index(key)
                r_charsetOffset=l_charsetOffset+len(value.strip())
                source_done=source_done[:l_charsetOffset] + value + source_done[l_charsetOffset+len(key):]
                if key not in items:
                    items[key] = []
                items[key].append((l_charsetOffset, r_charsetOffset))

            coref_error = False# on retente le parse si coref error
            if source_done in analyzed_sources_done.keys():
                if "corefs_error" in analyzed_sources_done[source_done].keys():
                    coref_error = True
                else:
                    coref_error = False

            if source_done in analyzed_sources_done.keys() and not coref_error:
                output_src_done = analyzed_sources_done[source_done]
            else:
                output_src_done = nlp.annotate(source_done, properties={
                    "pipelineLanguage": "en",
                    'annotators': "tokenize,ssplit,pos,ner,depparse,coref",
                    'outputFormat': 'json'
                })  # -outputFormat typedDependencies  -keepPunct

                if output_src_done.__str__() == 'None' or type(
                        output_src_done) == str:  # stanford se casse parfois le nez sur les corefs

                    print(source_done)
                    output_src_done = nlp.annotate(source_done, properties={
                        "pipelineLanguage": "en",
                        'annotators': 'tokenize,pos,ner,depparse,coref',
                        'outputFormat': 'json'
                    })  #

                    if output_src_done.__str__() == 'None' or type(
                            output_src_done) == str:  # stanford se casse parfois le nez sur les corefs
                        print("\nerreur coref ref non rectifiée")
                        print(source_done)
                        print()
                        output_src_done = nlp.annotate(source_done, properties={
                            "pipelineLanguage": "en",
                            'annotators': 'tokenize,pos,depparse,parse,ner,mention',
                            'outputFormat': 'json'
                        })
                        coref_error = True
                        output_src_done['corefs_error'] = "True"
                        output_src_done['corefs'] = {}
                        # bon, on va en profiter pour une tite sauvegarde

                        print(" ...", end = '')

                        # try:
                        #     analyzed_sources_done[source_done] = output_src_done
                        #
                        #     fjson = open(JSON_DATA_FILE, "w")
                        #     fjson.write(json.dumps(analyzed_sources_done, properties = properties, ensure_ascii=False))
                        #     fjson.flush()
                        #     fjson.close()
                        #     while not fjson.closed:
                        #         True
                        #
                        # except KeyboardInterrupt:
                        #     print()
                        #     print("Attendez SVP, enregistrement données en cours")
                        #
                        #     if not fjson.closed:
                        #         fjson.close()
                        #         while not fjson.closed:
                        #             True
                        #     fjson = open(JSON_DATA_FILE, "w")
                        #     fjson.write(json.dumps(analyzed_sources_done, ensure_ascii=False))
                        #     fjson.flush()
                        #     fjson.close()
                        #     while not fjson.closed:
                        #         True
                        #     print("Enregistrement effectué")
                        #     sys.exit(1)

                    else:
                        coref_error = False
                        if 'corefs_error' in output_src_done.keys():
                            print("coref_error rectifiée")
                            del output_src_done['corefs_error']
                analyzed_sources_done[source_done]=output_src_done
                # on va rechercher la présence de nos items sous formes déguisée [coref sur item non listé]


            analyzed_sources_done_dupl[source_done] = output_src_done
            for key, corefs in output_src_done['corefs'].items():
                iCorefs = []
                ref_corefs=[]
                x_coref=0
                for coref in corefs:
                    # les index core commencent à 1, les index offset à 0
                    num_sent=coref['sentNum']
                    core_startIndex=coref['startIndex']
                    core_endIndex=coref['endIndex']-1# endIndex est l'index du token suivant :/
                    l_charsetOffset=output_src_done['sentences'][num_sent-1]['tokens'][core_startIndex-1]['characterOffsetBegin']
                    r_charsetOffset=output_src_done['sentences'][num_sent-1]['tokens'][core_endIndex-1]['characterOffsetEnd']
                    for ref, paire_item in items.items():
                        for item in paire_item:
                            if l_charsetOffset>=item[0] and (r_charsetOffset<=item[1] or (l_charsetOffset<=item[1] and r_charsetOffset >item[1] )):
                                ref_corefs.append((ref, num_sent, core_startIndex, core_endIndex))
                                iCorefs.append(x_coref)
                    x_coref += 1

                if len(ref_corefs)>0:
                    for ref_coref, num_sent, core_startIndex, core_endIndex in ref_corefs:
                        x_coref=0
                        refs_used=[]
                        for coref in corefs:
                            if x_coref not in iCorefs:
                                num_sent=coref['sentNum']
                                core_startIndex=coref['startIndex']
                                core_endIndex=coref['endIndex']-1# endIndex est l'index du token suivant dans la liste des tokens : tokens[x-1]['index']=x :/
                                #quand les réfs sont utilisés plusieurs fois, on garde par défaut la première croisée.
                                if num_sent.__str__() + core_startIndex.__str__() + num_sent.__str__() + core_endIndex.__str__() not in refs_used:
                                    items[ref_coref].append((num_sent-1, core_startIndex-1, num_sent-1, core_endIndex-1))
                                    refs_used.append(num_sent.__str__() + core_startIndex.__str__() + num_sent.__str__() + core_endIndex.__str__())
                            x_coref   += 1

            new_items = {}

            # on va tester nos phrases, on en profite pour prélever le plus grand nombre de tokens
            # on charge les index des tokens, pas la position dans la phrases d'origine
            nb_max_tokens=0
            # on cherche nos items, leur index de début
            for key in items.keys():
                new_items[key] = []
                for item in items[key]:
                    if len(item)==2:
                        num_sent=0

                        while num_sent < len(output_src_done['sentences']):
                            # plus grand nombre de tokens évalué et ramassé si...
                            if len(output_src_done['sentences'][num_sent]['tokens'])>nb_max_tokens:
                                nb_max_tokens=len(output_src_done['sentences'][num_sent]['tokens'])
                            # recherche de l'index de début et de fin de notre item sur la phrase parsée
                            for token in output_src_done['sentences'][num_sent]['tokens']:
                                index_token=token['index']-1
                                l_charsetOffset=token['characterOffsetBegin']
                                r_charsetOffset=token['characterOffsetEnd']
                                if l_charsetOffset >= item[0] and r_charsetOffset<=item[1] :
                                    new_items[key].append((num_sent, index_token, item))# on a le début de l'expression, la phrase
                                    # reste à trouver la fin, cera la prochaine opération
                                    num_sent = len(output_src_done['sentences'])
                                    break
                            num_sent += 1

            # mis à jour de l'index de fin pour chaque item trouvé, on le fait en deux passes pour éviter les recouvrements.
            for key in new_items.keys():
                for item in new_items[key]:
                    found=False
                    l_refOffset=item[2][0]
                    r_refOffset=item[2][1]
                    new_item = [item[0], item[1], item[0], item[1]]
                    items[key].remove(item[2])
                    num_sent=item[0]
                    start_token=item[1]
                    while num_sent < len(output_src_done['sentences']):
                        start_sent=num_sent
                        for token in output_src_done['sentences'][start_sent]['tokens'][start_token:]:
                            index_token=token['index']-1
                            l_charsetOffset=token['characterOffsetBegin']
                            r_charsetOffset=token['characterOffsetEnd']
                            if l_charsetOffset >= l_refOffset and r_charsetOffset <= r_refOffset:
                                new_item[2]=num_sent
                                new_item[3]=index_token
                                # on modifie jusqu'au dernier dans les clous
                                found=True
                                num_sent=start_sent
                            else:
                                num_sent=len(output_src_done['sentences'])
                                break
                        start_token=0
                        num_sent += 1
                    if found:
                        items[key].append(new_item)
            del new_items

            # # On va classifier nos valeurs objets/sujets par ordre d'apparition
            no_error = True
            for key in items.keys():
                for item in items[key]:
                    if len(item)==2:
                        if final_lid not in bads_lids:
                            bads_lids.append(final_lid)
                            bads_items.append(item)
                        if final_lid not in why_bad.keys():
                            why_bad[final_lid]=[]
                        why_bad[final_lid].append({'cause':"not precisly found", 'position':item.__str__(), 'key' : key })
                        no_error = False
                    elif len(item)==4 :
                        # si un item est sur deux lignes, on va considérer cela comme une erreur de parsing
                        # item[2] ne sera pas traité ensuite ! ne rien changer ici sans aller adapter le traitement des items
                        #  dans --> # on ordonne nos items
                        if (item[0]!=item[2]):
                            bads_lids.append(final_lid)
                            bads_items.append(item)
                            if final_lid not in why_bad.keys():
                                why_bad[final_lid]=[]
                            why_bad[final_lid].append({'cause': "split on several sentences", 'position': item.__str__(), 'key' : key})
                            no_error = False

            chasing_error = False
            # on va stocker les informations résultant de l'analyse
            if no_error:

                if not os.path.basename(fichier) in keeped_data.keys():
                    keeped_data[os.path.basename(fichier)] = []
                    keeped_data[os.path.basename(fichier)].append(ElementTree.SubElement(keeped_tree, "file", name=fichier))
                    keeped_data[os.path.basename(fichier)].append({})
                noeud_fichier = keeped_data[os.path.basename(fichier)][0]

                # éléments propres à l'EID
                if not eid in keeped_data[os.path.basename(fichier)][1].keys():
                    keeped_data[os.path.basename(fichier)][1][eid] = ElementTree.SubElement(noeud_fichier, "eid", name=eid)
                    noeud_eid = keeped_data[os.path.basename(fichier)][1][eid]
                    triples = ElementTree.SubElement(noeud_eid, "tripleset")
                    it=0
                    for t in triplets:
                        ElementTree.SubElement(triples, "triple", tid=it.__str__()).text = " | ".join(t)
                        it += 1
                    triples = ElementTree.SubElement(noeud_eid, "tripleset_encoded")
                    it=0
                    for t in encoded_triplets:
                        ElementTree.SubElement(triples, "triple", tid=it.__str__()).text = " | ".join(t)
                        it += 1
                else:
                    noeud_eid = keeped_data[os.path.basename(fichier)][1][eid]

                # on ordonne nos items
                références = {}
                data_triplets = {}
                for id in items.keys():
                    for item in items[id]:
                        if item[0] not in références.keys():
                            références[item[0]] = {}
                            data_triplets[item[0]] = []
                        if item[1] not in références[item[0]].keys():
                            références[item[0]][item[1]]=""
                        elif références[item[0]][item[1]][0] == item[3] and \
                                        références[item[0]][item[1]][1] == id and \
                                        références[item[0]][item[1]][1] == key and \
                                        id in  data_triplets[item[0]]:
                            pass
                            #ElementTree.dump(noeud_sentence)
                        else:
                            # des clés pour ne pas gâcher d'espace, ce qui n'est pas très très utile
                            # l'idée en tout cas est d'ordonnancer.
                            # cetet erreur ne fait que garantir qu'un item ne commence pas exactement au même endroit qu'un autre.
                            # print(source_done)
                            # print("erreur : référence croisée, débugguer")

                            chasing_error=True
                            bads_lids.append(final_lid)
                            bads_items.append(item)
                            if final_lid not in why_bad.keys():
                                why_bad[final_lid]=[]
                            why_bad[final_lid].append({'cause': "chasing analyze", 'position': item.__str__(), 'key' : key})
                        if not id in data_triplets[item[0]]:
                            data_triplets[item[0]].append(id)

                        références[item[0]][item[1]]=(item[3], id)

                if not chasing_error:
                    num_sent = 0
                    # on va faire un premier tour pour chasser des soucis de parsing.
                    while num_sent < len(output_src_done['sentences']):
                        for t in encoded_triplets:
                            if num_sent in data_triplets.keys() and not chasing_error:
                                if t[0] in data_triplets[num_sent] and t[2] in data_triplets[num_sent] :
                                    for i_verified in [0, 2]:
                                        copy_items=copy.deepcopy(items[t[i_verified]])
                                        for tab in copy_items:
                                            if tab[0]==num_sent and not chasing_error:
                                                pos_deb=tab[1]
                                                pos_deb=tab[1]
                                                pos_fin=tab[3]+1 # inclusion du token fin
                                                verified=verify_tokens(subjects_objects[t[i_verified]], output_src_done['sentences'][num_sent]["tokens"][pos_deb:pos_fin], t[i_verified], final_sentence, data.triplets_dictionary[int(t[i_verified].replace("xlid", "").replace("xrid", ""))].replace("_"," "))
                                                if verified=="different":
                                                    if len(items[t[i_verified]])> 1:
                                                        items[t[i_verified]].remove(tab)
                                                    else:
                                                        bads_lids.append(final_lid)
                                                        bads_items.append(item)
                                                        if final_lid not in why_bad.keys():
                                                            why_bad[final_lid]=[]
                                                        why_bad[final_lid].append({'cause': "chasing error", 'position': item.__str__(), 'key' : key})
                                                        chasing_error = True

                        num_sent   += 1

                if not chasing_error:
                    attribs = {}
                    attribs['lid'] = final_lid
                    if coref_error:
                        attribs['coref'] = "error"
                    else:
                        attribs['coref'] = "analyzed"
                    noeud_lexicalisation = ElementTree.SubElement(noeud_eid, "lexicalisation", attribs)
                    noeud_lexicalisation_encoded = ElementTree.SubElement(noeud_lexicalisation, "encoded")
                    noeud_lexicalisation_encoded.text = final_sentence
                    noeud_lexicalisation_encoded = ElementTree.SubElement(noeud_lexicalisation, "analysed")
                    noeud_lexicalisation_encoded.text = source_done
                    noeud_sentences = ElementTree.SubElement(noeud_lexicalisation, "sentences", nombre=(len(data_triplets)).__str__())

                    triplets_done = []

                    # on garde les références des noeuds de triplets pour chaque phrase
                    embedded_triplets_inside_sentence = {}  # 
                    embedded_triplets_inside_sentence_apart = {}  # 
                    embedded_triplets_inside_sentence_items = {}  # détail des triples par sentence --> l'élément est-il déjà géré par un triplet complet ?
                    triples_references = {} # le noeuds des infos générales triplets: un par sentence

                    # graphe originel
                    num_sent = 0

                    while num_sent < len(output_src_done['sentences']):

                        noeud_sentence = ElementTree.SubElement(noeud_sentences, "sentence", sid=num_sent.__str__())
                        triples_references[num_sent] = ElementTree.SubElement(noeud_sentence, "triplets")
                        noeud_triplets = triples_references[num_sent]
                        it=0
                        embedded_triplets_inside_sentence_apart[num_sent] = {}
                        embedded_triplets_inside_sentence[num_sent] = {}
                        for t in encoded_triplets:
                            if num_sent in data_triplets.keys():
                                if t[0] in data_triplets[num_sent] and t[2] in data_triplets[num_sent] :
                                    attribs = {}
                                    attribs["tid"]=it.__str__()
                                    attribs["partial"]="full"
                                    attribs["idSubject"] = t[0]
                                    attribs["idObject"] = t[2]
                                    embedded_triplets_inside_sentence_apart[num_sent][it] = ElementTree.SubElement(noeud_triplets, "triplet", attribs)

                                    solutions = ElementTree.SubElement(embedded_triplets_inside_sentence_apart[num_sent][it], "solutions")

                                    # on va regarder si l'item apparaît tel quel ou modifié
                                    for i_verified in [0, 2]:
                                        for tab in items[t[i_verified]]:
                                            if tab[0]==num_sent:
                                                pos_deb=tab[1]
                                                pos_fin=tab[3]+1 # inclusion du token fin
                                                if i_verified==0:
                                                    attribs = {}
                                                    attribs["caracteristique"] = verify_tokens(subjects_objects[t[i_verified]], output_src_done['sentences'][num_sent]["tokens"][pos_deb:pos_fin], t[i_verified], final_sentence, data.triplets_dictionary[int(t[i_verified].replace("xlid", "").replace("xrid", ""))].replace("_"," "))
                                                    attribs["indexDeb"] = (pos_deb + 1).__str__()
                                                    attribs["indexFin"] = pos_fin.__str__()
                                                    sujet = ElementTree.SubElement(solutions, "sujet", attribs)

                                                else:
                                                    attribs = {}
                                                    attribs["caracteristique"]  = verify_tokens(subjects_objects[t[i_verified]], output_src_done['sentences'][num_sent]["tokens"][pos_deb:pos_fin], t[i_verified], final_sentence, data.triplets_dictionary[int(t[i_verified].replace("xlid", "").replace("xrid", ""))].replace("_"," "))
                                                    attribs["indexDeb"] = (pos_deb + 1).__str__()
                                                    attribs["indexFin"] = pos_fin.__str__()
                                                    objet = ElementTree.SubElement(solutions, "objet", attribs)

                                    if it not in triplets_done:
                                        triplets_done.append(it)
                            it += 1

                        graph = pgv.AGraph(directed=True, strict=False)

                        for each in output_src_done['sentences'][num_sent]['enhancedPlusPlusDependencies']:
                            try:
                                graph.add_edge(each['governor'], each['dependent'], str(each['dep']))
                            except Exception as exc:
                                print()
                                print("enhanced_graph")
                                print(type(exc))  # the exception instance
                                print(exc.args)
                                #problème, un seul edge possible concomitamment
                                print("ajout dépendance en échec")
                                print(each['governor'].__str__()+" "+each['dependent'].__str__()+ " "+ each['dep'].__str__())
                                print(source_done)

                        basic_graph = pgv.AGraph(directed=True, strict=False)

                        for each in output_src_done['sentences'][num_sent]['basicDependencies']:
                            try:
                                basic_graph.add_edge(each['governor'], each['dependent'], each['dep'])
                            except Exception as exc:
                                print()
                                print("basic_graph")
                                print(type(exc))  # the exception instance
                                print(exc.args)
                                #problème, un seul edge possible concomitamment
                                print("ajout dépendance en échec")
                                print(each['governor'].__str__()+" "+each['dependent'].__str__()+ " "+ each['dep'].__str__())
                                print(source_done)

                        root_sentence = ElementTree.SubElement(noeud_sentence, "ROOT", idx="0")
                        embedded_triplets_inside_sentence[num_sent] = ElementTree.SubElement(root_sentence, "embeddedTriples")
                        # on va stocker les identifiant déjà géré
                        embedded_triplets_inside_sentence_items[embedded_triplets_inside_sentence[num_sent].__str__()]=set()
                        triplets_position = embedded_triplets_inside_sentence[num_sent]

                        items_position = ElementTree.SubElement(root_sentence, "rootItems")
                        num_token = 0
                        while num_token < len(output_src_done['sentences'][num_sent]['tokens']):

                            if True:

                                to_do = True
                                if num_sent in références.keys():
                                    if num_token in références[num_sent].keys():
                                        to_do = False
                                        ref_tok=num_token
                                        attribs = {}
                                        attribs['id']=références[num_sent][ref_tok][1]
                                        a = ElementTree.SubElement(triplets_position, "triple", attribs)
                                        embedded_triplets_inside_sentence_items[embedded_triplets_inside_sentence[num_sent].__str__()].add(attribs['id'])
                                        while num_token <= références[num_sent][ref_tok][0]:
                                            token = output_src_done['sentences'][num_sent]['tokens'][num_token]
                                            attribs = {}
                                            attribs['idx'] = token['index'].__str__()
                                            attribs['ner'] = token['ner'].__str__()

                                            pos=token['pos']
                                            if pos not in POS_TAGS:
                                                pos="OTHERS"
                                            attribs['pos'] = pos
                                            b=ElementTree.SubElement(a, "item", attribs)

                                            # on ajoute l'item de façon plus complète à "items's root"
                                            attribs = {}
                                            attribs['idx'] = token['index'].__str__()
                                            attribs['lemma'] = token['lemma'].__str__()
                                            attribs['ner'] = token['ner'].__str__()
                                            pos=token['pos']
                                            if pos not in POS_TAGS:
                                                pos = "OTHERS"
                                            attribs['pos'] = pos
                                            b=ElementTree.SubElement(items_position , "item", attribs)
                                            b.text=token['originalText'].__str__()

                                            node = graph.get_node(token['index'])
                                            node.attr['word'] = token['originalText'].__str__()
                                            node.attr['pos'] = pos

                                            node = basic_graph.get_node(token['index'])
                                            node.attr['word'] = token['originalText'].__str__()
                                            node.attr['pos'] = pos

                                            num_token   += 1

                                if to_do:
                                    token = output_src_done['sentences'][num_sent]['tokens'][num_token]
                                    attribs = {}
                                    attribs['idx'] = token['index'].__str__()
                                    attribs['lemma'] = token['lemma'].__str__()
                                    attribs['ner'] = token['ner'].__str__()
                                    pos=token['pos']
                                    if pos not in POS_TAGS:
                                        pos="OTHERS"
                                    attribs['pos'] = pos
                                    b=ElementTree.SubElement(items_position , "item", attribs)
                                    b.text=token['originalText'].__str__()

                                    node = graph.get_node(token['index'])
                                    node.attr['word'] = token['originalText'].__str__()
                                    node.attr['pos'] = pos

                                    node = basic_graph.get_node(token['index'])
                                    node.attr['word'] = token['originalText'].__str__()
                                    node.attr['pos'] = pos
                                    num_token  += 1
                            else:
                                num_token += 1

                        # ON ATTRIBUE le graphe AU NOEUD QUI VA BIEN
                        ElementTree.SubElement(noeud_sentence, "graphe").text=graph.string()
                        ElementTree.SubElement(noeud_sentence, "basic_graphe").text=basic_graph.string()
                        num_sent += 1

                    output_src_done_encoded = nlp.annotate(final_sentence, properties={
                        "pipelineLanguage": "en",
                        'annotators': "tokenize,ssplit,pos,depparse",
                        'outputFormat': 'json'
                    })  # -outputFormat typedDependencies  -keepPunct
                    analyzed_sources_done_dupl[final_sentence] = output_src_done_encoded

                    if len(analyzed_sources_done_dupl.keys()) % 100 == 0:

                        try:
                            fjson = open(JSON_DATA_FILE, "w")
                            fjson.write(json.dumps(analyzed_sources_done_dupl, indent=1, ensure_ascii=False))
                            fjson.flush()
                            fjson.close()
                            while not fjson.closed:
                                True
                        except KeyboardInterrupt:
                            if not fjson.closed:
                                fjson.close()
                                while not fjson.closed:
                                    True

                            print("Minute, sauvegarde en cours")
                            fjson = open(JSON_DATA_FILE, "w")
                            fjson.write(json.dumps(analyzed_sources_done_dupl, ensure_ascii=False))
                            fjson.flush()
                            fjson.close()
                            while not fjson.closed:
                                True
                            sys.exit(1)

                    # graphe encoded
                    num_sent = 0

                    while num_sent < len(output_src_done_encoded['sentences']):

                        graph_encoded = pgv.AGraph(directed=True, strict=False)
                        for each in output_src_done_encoded['sentences'][num_sent]['enhancedPlusPlusDependencies']:
                            try:
                                graph_encoded.add_edge(each['governor'], each['dependent'], each['dep'])
                            except:
                                print(source_done)

                        num_token = 0
                        while num_token < len(output_src_done_encoded['sentences'][num_sent]['tokens']):
                            token = output_src_done_encoded['sentences'][num_sent]['tokens'][num_token]
                            pos = token['pos']
                            if pos not in POS_TAGS:
                                pos = "OTHERS"
                            node = graph_encoded.get_node(token['index'])
                            node.attr['word'] = token['originalText'].__str__()
                            node.attr['pos'] = pos

                            num_token   += 1

                        #  ON ATTRIBUE le graphe AU NOEUD QUI VA BIEN
                        ElementTree.SubElement(noeud_sentence, "grapheOnEncodedString").text = graph_encoded.string()
                        num_sent   += 1



                    # on recherche maintenant les triplets splittés
                    if len(triplets_done) < len(triplets):
                        it = 0
                        for t in encoded_triplets:
                            if it not in triplets_done:
                                noeud_triplet = ElementTree.SubElement(noeud_lexicalisation, "partialTriplet", tid=it.__str__())

                                t_found = [False, False]
                                num_sent = 0
                                while num_sent < len(output_src_done['sentences']):
                                    # on enregistre à l'endroit attendu
                                    triplets_position = embedded_triplets_inside_sentence[num_sent]


                                    # on garde une mémoire spécifique
                                    if num_sent in data_triplets.keys():
                                        if t[0] in data_triplets[num_sent]:
                                            if t[0] in items.keys():
                                                for coord in items[t[0]]:
                                                    if coord[0]==num_sent:
                                                        sent_deb=coord[0]
                                                        sent_end=coord[2]
                                                        tok_deb=coord[1]
                                                        tok_end=coord[3]
                                                        attribs = {}
                                                        attribs['idSubject'] = t[0]
                                                        attribs['tid'] = it.__str__()
                                                        attribs['partial'] = 'subject'# on va regarder si l'item apparaît tel quel ou modifiéattribs["solutions"] = {}

                                                        if num_sent not in triples_references.keys():
                                                            noeud_sentence = ElementTree.SubElement(noeud_sentences, "sentence",  sid=num_sent.__str__())
                                                            triples_references[num_sent] = ElementTree.SubElement(noeud_sentence, "triplets")
                                                        ref_sol = ElementTree.SubElement(triples_references[num_sent], "triplet", attribs)
                                                        solutions = ElementTree.SubElement(ref_sol, "solutions")

                                                        i_verified = 0
                                                        for tab in items[t[i_verified]]:
                                                            if tab[0]==num_sent:
                                                                pos_deb=tab[1]
                                                                pos_fin=tab[3]+1 # inclusion du token fin
                                                                attribs = {}
                                                                attribs["caracteristique"] = verify_tokens(subjects_objects[t[i_verified]], output_src_done['sentences'][num_sent]["tokens"][pos_deb:pos_fin], t[i_verified], final_sentence, data.triplets_dictionary[int(t[i_verified].replace("xlid", "").replace("xrid", ""))].replace("_"," "))
                                                                attribs["indexDeb"] = (pos_deb + 1).__str__()
                                                                attribs["indexFin"] = pos_fin.__str__()
                                                                sujet = ElementTree.SubElement(solutions, "sujet", attribs)

                                                        t_found[0] = True


                                                        if t[0] not in embedded_triplets_inside_sentence_items[triplets_position.__str__()]:

                                                            attribs = {}
                                                            attribs['id'] = t[0]
                                                            a = ElementTree.SubElement(triplets_position, "triple", attribs)
                                                            while tok_deb<=tok_end:
                                                                token=output_src_done['sentences'][num_sent]['tokens'][tok_deb]
                                                                attribs = {}
                                                                attribs['idx'] = token['index'].__str__()
                                                                attribs['ner'] = token['ner'].__str__()

                                                                pos=token['pos']
                                                                if pos not in POS_TAGS:
                                                                    pos="OTHERS"
                                                                attribs['pos'] = pos
                                                                b=ElementTree.SubElement(a, "item", attribs)
                                                                tok_deb += 1

                                            ElementTree.SubElement(noeud_triplet, "sujet", sentence=num_sent.__str__())
                                        if t[2] in data_triplets[num_sent] :
                                            if t[2] in items.keys():
                                                for coord in items[t[2]]:
                                                    if coord[0]==num_sent:
                                                        sent_deb=coord[0]
                                                        sent_end=coord[2]
                                                        tok_deb=coord[1]
                                                        tok_end=coord[3]

                                                        attribs = {}
                                                        attribs['idObject'] = t[2]
                                                        attribs['tid']= it.__str__()
                                                        attribs['partial'] = 'object'
                                                        if num_sent not in triples_references.keys():
                                                            noeud_sentence = ElementTree.SubElement(noeud_sentences, "sentence",  sid=num_sent.__str__())
                                                            triples_references[num_sent] = ElementTree.SubElement(noeud_sentence, "triplets")
                                                        ref_sol = ElementTree.SubElement(triples_references[num_sent],
                                                                                         "triplet", attribs)
                                                        solutions = ElementTree.SubElement(ref_sol, "solutions")

                                                        t_found[1] = True# on va regarder si l'item apparaît tel quel ou modifié

                                                        i_verified = 2
                                                        for tab in items[t[i_verified]]:
                                                            if tab[0]==num_sent:
                                                                pos_deb=tab[1]
                                                                pos_fin=tab[3]+1 # inclusion du token fin
                                                                attribs = {}
                                                                attribs["caracteristique"] = verify_tokens(subjects_objects[t[i_verified]], output_src_done['sentences'][num_sent]["tokens"][pos_deb:pos_fin], t[i_verified], final_sentence, data.triplets_dictionary[int(t[i_verified].replace("xlid", "").replace("xrid", ""))].replace("_"," "))
                                                                attribs["indexDeb"] = (pos_deb + 1).__str__()
                                                                attribs["indexFin"] = pos_fin.__str__()
                                                                sujet = ElementTree.SubElement(solutions, "objet",
                                                                                               attribs)


                                                        if t[2] not in embedded_triplets_inside_sentence_items[triplets_position.__str__()]:

                                                            attribs = {}
                                                            attribs['id'] = t[2]
                                                            a = ElementTree.SubElement(triplets_position, "triple", attribs)
                                                            while tok_deb<=tok_end:
                                                                token=output_src_done['sentences'][num_sent]['tokens'][tok_deb]
                                                                attribs = {}
                                                                attribs['idx'] = token['index'].__str__()
                                                                attribs['ner'] = token['ner'].__str__()

                                                                pos=token['pos']
                                                                if pos not in POS_TAGS:
                                                                    pos="OTHERS"
                                                                attribs['pos'] = pos
                                                                b=ElementTree.SubElement(a, "item", attribs)
                                                                tok_deb += 1
                                            ElementTree.SubElement(noeud_triplet, "objet", sentence=num_sent.__str__())
                                    num_sent += 1
                            it += 1


        # si on a eu du mal à parser certaines phrases
        if len(bads_lids)>0:
            if not os.path.basename(fichier) in garbage_data.keys():
                garbage_data[os.path.basename(fichier)] = []
                garbage_data[os.path.basename(fichier)].append(ElementTree.SubElement(garbage_tree, "file", name=fichier))
                garbage_data[os.path.basename(fichier)].append({})

            noeud_fichier = garbage_data[os.path.basename(fichier)][0]

            if not eid in garbage_data[os.path.basename(fichier)][1].keys():
                garbage_data[os.path.basename(fichier)][1][eid] = [ElementTree.SubElement(noeud_fichier, "eid", name=eid)]

            noeud_id = garbage_data[os.path.basename(fichier)][1][eid][0]
            triples = ElementTree.SubElement(noeud_id, "tripleset")
            for t in triplets:
                ElementTree.SubElement(triples, "triple").text = " | ".join(t)
            triples = ElementTree.SubElement(noeud_id, "tripleset_encoded")
            for t in encoded_triplets:
                ElementTree.SubElement(triples, "triple").text = " | ".join(t)

            for bad_lid, item in zip(bads_lids, bads_items):

                for final_sentence, source_done, final_lid in zip(final_sentences, sources_dones, final_lids):
                    if final_lid==bad_lid:
                        attribs_error = {}
                        attribs_error['lid'] = final_lid
                        noeud_sentence = ElementTree.SubElement(noeud_id, "lex", attribs_error )
                        for errot in why_bad[final_lid]:
                            attribs_error = {}
                            attribs_error['cause'] = errot['cause']
                            attribs_error['position'] = errot['position']
                            attribs_error['key'] = errot['key']
                            ElementTree.SubElement(noeud_sentence, "error", attribs_error)
                        noeud_encoded = ElementTree.SubElement(noeud_sentence, "encoded")
                        noeud_encoded.text= final_sentence
                        noeud_encoded = ElementTree.SubElement(noeud_sentence, "analysed")
                        noeud_encoded.text= source_done
                bad_lid_continue=False
                while bad_lid_continue:
                    bad_lid_continue=False
                    for final_sentence, source_done, final_lid in zip(final_sentences, sources_dones, final_lids):
                        if final_lid==bad_lid:
                            final_sentences.remove(final_sentence)
                            final_lids.remove(final_lid)
                            if len(final_sentences)==0:
                                if fichier not in eid_vides.keys():
                                    eid_vides[fichier] = []
                                eid_vides[fichier].append(eid)
                            bad_lid_continue=True
                            break


    shared.progress(1, 1)
    shared.init_progress()

    # on a des triplets qui n'ont plus de traductions, on les supprime
    ilen=len(data.working_dictionary)
    while ilen>0:
        ilen -= 1
        triplets=data.working_dictionary[ilen][0]
        sources_dones=data.working_dictionary[ilen][1]
        final_sentences=data.working_dictionary[ilen][2]
        final_lids=data.working_dictionary[ilen][3]
        fichier=data.working_dictionary[ilen][4]
        eid=data.working_dictionary[ilen][5]
        if fichier in eid_vides.keys():
            if eid in eid_vides[fichier]:
                del data.working_dictionary[ilen]

    print()

    print("Sauvegarde des données de travail")

    try:
        fjson = open(JSON_DATA_FILE, "w")
        fjson.write(json.dumps(analyzed_sources_done_dupl, indent=1, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
    except KeyboardInterrupt:
        if not fjson.closed:
            fjson.close()
            while not fjson.closed:
                True

        print("Minute, sauvegarde en cours")
        fjson = open(JSON_DATA_FILE, "w")
        fjson.write(json.dumps(analyzed_sources_done_dupl, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        sys.exit(1)


    print("sauvegarde données stanford réalisée")
    print("ok")

    print("")
    print("Sauvegarde des données erreur")
    bs = BeautifulSoup(ElementTree.tostring(garbage_tree, method="xml"), 'xml')
    text_re = re.compile('>\n\s+([^<>\s].*?)\n\s+</', re.DOTALL)
    prettyXml = text_re.sub('>\g<1></', bs.prettify())

    f = open(shared.WORKING_DATA_DIR + "entities_not_correctly_parsed.xml", 'w')

    f.write(prettyXml)
    f.close()

    del garbage_tree
    del garbage_data

    print("Sauvegarde des données valides")

    tree.write(shared.WORKING_DATA_DIR + "output.xml")


    bs = BeautifulSoup(ElementTree.tostring(keeped_tree, method="xml"), 'xml')
    text_re = re.compile('>\n\s+([^<>\s].*?)\n\s+</', re.DOTALL)
    prettyXml = text_re.sub('>\g<1></', bs.prettify())

    f = open(shared.WORKING_DATA_DIR + "entities_parsed.xml", 'w')

    f.write(bs.prettify())
    f.close()

    print("Sauvegardes effectuées")



    del keeped_tree
    del keeped_data
