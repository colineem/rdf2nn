
import shared
import json
import sys
import os
from shutil import copyfile
from nltk.tree import *
#from nltk.draw.tree import TreeView
from nltk import Tree
from pycorenlp import StanfordCoreNLP
from xml.etree import ElementTree

#from PIL import Image, ImageTk
from tkinter import *
import webbrowser

STATISTIQUES="statistiques_data/"
STATS_FILE="statistiques.json"
REFERENCE_FILE="statistiques_reference.json"
GENERAL_FILE="statistiques_all.json"
CONSTRAINTS = sorted(['apposition', 'coordinated clauses', 'coordinated full clauses', 'direct object', 'existential', 'indirect object', 'juxtaposition', 'participle clause', 'participle clause subject', 'passive voice', 'possessif', 'relative adverb', 'relative object', 'relative subject', 'twice objects']
)
WHAT = {'apposition' :"deux nœuds sont liés par une relation de type 'appos'. La relation considérée n'est pas interne à une entité xlidrlid.",
'coordinated clauses' :'une relation coordonnée simple est une coordination verbale réalisée par une conjonction, de type "Julien trouve et mange une pomme".',
'coordinated full clauses' :'une relation coordonnée pleine est une coordination réalisée par une conjonction, de type "Julien trouve une pomme et il la mange".',
'direct object' :'un objet direct  (COD) complète un verbe : "David bat Goliath" --> Goliath est ici le COD. À ne pas confondre avec un complément ciconstanciel x ou y.',
'existential' :"Définit une relation d'identité, peut être trouble (ex: 'Elle est la fille du pasteur', ou encore 'Aarhus Airport is 25 metres above sea level')",
'indirect object' :'un objet indirect ("She gave me a raise" --> "a raise" est l\'objet indirect de "give")',
'juxtaposition' :'deux clauses coordonnées par un simple signe de ponctuation, prosaïquement la virgule.',
'participle clause' : '"After being recruited by NASA in 1963, Alan Bean spent 100305 minutes in space." est une clause participiale type.\n "advcl" est alors relation portant sur des verbes ou ',
'participle clause subject' :'À cocher quand la clause participiale est sujette. Un lien csubj unit alors une participle clause à une autre clause.',
'passive voice' :'Voix passive... ',
'possessif' :'Une relation "nmod:poss" unit deux mots.',
'relative adverb' :'"advmod" lié à une relative clause.',
'relative object' :'Le pronom relatif est objet (ex: "It’s the same cooker that my mother has.")',
'relative subject' :'Le pronom relatif est sujet (ex: "Aaron Bertram, who plays Ska punk, is an artist.")',
'twice objects' :'et un objet direct, et un objet indirect',
}
#on charge
global nlp
try :
    nlp = StanfordCoreNLP(shared.SERVEUR_CORENLP)
except:
    pass

if not os.path.isfile(STATISTIQUES+REFERENCE_FILE) :
    if not os.path.isfile(STATISTIQUES+STATS_FILE) :
        print("le fichier de référence n'est pas créable, pas de données statistiques")
        sys.exit(5)
    else:
        copyfile(STATISTIQUES+STATS_FILE, STATISTIQUES+REFERENCE_FILE)


file = open(STATISTIQUES+REFERENCE_FILE)
silver_file = json.load(file)
file.close()



file = open(STATISTIQUES+GENERAL_FILE)
data_calc = json.load(file)
file.close()

working_data = {}
working_data_tmp = ElementTree.parse(shared.OUTPUT_DATA_DIR + "working_data.xml")

#on précharge les données des règles pour donner à comprendre pourquoi x a été décidé
keys_tmp = list(silver_file.keys())

#nombre maximal de contrainte
nb_max_constraint = 0
nb_max_token = 0

root = working_data_tmp.getroot()
for exemple in root.findall('exemple'):
    encoded = exemple.find('encoded')
    if encoded.text.strip() in keys_tmp:
        encoded.text = encoded.text.strip()
        keys_tmp.remove(encoded.text)
        working_data[encoded.text] = {}
        constraints = exemple.find("constraints")
        #nb_constraint = constraints.attrib['count']
        #nb_constraint_uniq = constraints.attrib['count']
        icount = 0
        for contrainte in constraints.findall("contrainte"):
            name = contrainte.attrib['name']
            if name not in working_data[encoded.text].keys():
                working_data[encoded.text][name] = []
                icount += 1
            working_data[encoded.text][name].append([])
            #je crée un groupe pour chaque lot, indifférenciant la cause.
            for node in contrainte.findall("socle/node"):
                working_data[encoded.text][name][-1].append([])
                for token in node.findall('token'):
                    working_data[encoded.text][name][-1][-1] = token.attrib["index"]
                    if int(token.attrib['index'])> nb_max_token:
                        nb_max_token = int(token.attrib['index'])
        tokens = exemple.find("tokens")
        working_data[encoded.text]['tokens']=[]
        for token in tokens.findall('token'):
            working_data[encoded.text]['tokens'].append(token.text.strip())

        if icount > nb_max_constraint:
            nb_max_constraint = icount



if len(keys_tmp) > 0:
    print("Reste " + (len(keys_tmp)).__str__() + " non trouvée !")
    exit()


del root
del working_data_tmp

constraintsValues={}
constraintButton={}
constraintsRefValues={}
constraintRefButton={}

fenetre = Tk()

label = Label(fenetre, text="Gestion silver")
label.pack()
p = PanedWindow(fenetre, orient=VERTICAL)
p.pack(side=TOP, expand=Y, fill=BOTH, pady=2, padx=2)
lPhraseOrigine = Text(p,  background='yellow', width=150, height=5)
lPhraseTransformée = Text(p,  background='white',  height=5)
p.add(lPhraseOrigine)
p.add(lPhraseTransformée)
pgrid = PanedWindow(p, orient=VERTICAL)
labelGrid = Label(p, text="Annotation automatique : noeuds d'appui")
labelsGrids = []
global orig_color
for r in range(nb_max_constraint):
    labelsGrids.append([])
    labelsGrids[r].append(Label(pgrid, text='-', borderwidth=1))
    labelsGrids[r][-1].grid(row=r, column=0)
    orig_color = labelsGrids[r][-1].cget("background")
for r in range(nb_max_constraint):
    for c in range(nb_max_token):
        labelsGrids[r].append(Label(pgrid, text='.', borderwidth=1 ))
        labelsGrids[r][-1].grid(row=r,column=c+1) #'R%s/C%s'%(r,c)
p.add(labelGrid)
p.add(pgrid)

def callback(event):
    webbrowser.open_new(r"http://zammih:5000")


link = Label(p, text="Ouvrir le serveur Stanford CoreNLP", fg="blue", cursor="hand2")
p.add(link)
link.bind("<Button-1>", callback)



pcomp = PanedWindow(p, orient=HORIZONTAL)
pdata = PanedWindow(pcomp, orient=VERTICAL)
pref = PanedWindow(pcomp, orient=VERTICAL)
pfilter = PanedWindow(pcomp, orient=VERTICAL)
pinformations = PanedWindow(pcomp, orient=VERTICAL)

indexKeys=0
keys=sorted(silver_file.keys())


l1 = Label(p, text='Référence', justify=LEFT)
l2 = Label(p, text='Calculé', justify=LEFT)
pdata.add(l1)
pref.add(l2)
l1 = Label(p, text='', justify=LEFT)
l2 = Label(p, text='', justify=LEFT)
pdata.add(l1)
pref.add(l2)

for constraint in CONSTRAINTS:
    constraintsValues[constraint] = IntVar()
    constraintButton[constraint]=Checkbutton(pdata, text=constraint, variable=constraintsValues[constraint], anchor=W)
    constraintButton[constraint].VAR = constraintsValues[constraint]
    pdata.add(constraintButton[constraint])
    constraintsRefValues[constraint] = IntVar()
    constraintRefButton[constraint]=Checkbutton(pref, text=constraint, variable=constraintsRefValues[constraint], anchor=W, state=DISABLED, indicatoron=FALSE, disabledforeground='black')
    constraintRefButton[constraint].VAR = constraintsRefValues[constraint]

    pref.add(constraintRefButton[constraint])

explications = StringVar()
tadam = Label(pinformations, textvariable=explications, wraplength=375, height=10, justify=LEFT)
pinformations.add(tadam)
pcomp.add(pdata)
pcomp.add(pref)
pcomp.add(pfilter)
pcomp.add(pinformations)
p.add(pcomp)
skipValue = IntVar()
skipButton = Checkbutton(p, text="Passer les déjà traités", variable=skipValue, anchor=W)

p.add(skipButton)

explications.set('Choisir un filtre pour réduire la liste à ce qui est affecté par lui.')

def save():

    # on sauvegarde
    fjson = open(STATISTIQUES + REFERENCE_FILE, "w")
    fjson.write(json.dumps(silver_file, indent=2, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True

#c = Canvas(p, width=300, height=300)
vGoto = StringVar()

def next():
    global keys
    global indexKeys
    global lPhraseOrigine
    global lPhraseTransformée
    global silver_file
    global constraintButton
    global constraintsValues
    global skipButton
    global skipValue
    global orig_color
    #global c

    if len(keys)==0:
        print("Pas de données à traiter")

    if indexKeys>0 :
        silver_file[keys[indexKeys - 1]]["structures silver"] = list(
            set(silver_file[keys[indexKeys - 1]]["structures silver"]))
        silver_file[keys[indexKeys - 1]]["structures"] = list(
            set(data_calc[keys[indexKeys - 1]]["structures"]))
        for constraint in constraintButton.keys():
            if constraintsValues[constraint].get() == 1:
                if constraint not in silver_file[keys[indexKeys-1]]["structures silver"]:
                    silver_file[keys[indexKeys - 1]]["structures silver"].append(constraint)
            else:
                if "structures silver" not in constraint in silver_file[keys[indexKeys-1]]:
                    silver_file[keys[indexKeys - 1]]["structures silver"] = []
                if constraint in silver_file[keys[indexKeys-1]]["structures silver"]:
                    silver_file[keys[indexKeys - 1]]["structures silver"].remove(constraint)

        save()

        if indexKeys == len(keys):
            print("Exemples tous traités.")
            print("sauvegarde effectuée")
            indexKeys = 0


    key = keys[indexKeys]
    consigne =  "".join(silver_file[key]["structures silver"]) != "".join(data_calc[key]["structures"])
    if consigne:
        consigne = ("".join(silver_file[key]["structures silver"]) + "".join(data_calc[key]["structures"])).count("coordinated full clauses")
    consigne = True
    if ((skipValue.get() == 1 and silver_file[key]["structures silver"] == []) or skipValue.get() == 0) and consigne:
        label.config(text="Gestion silver, " + (indexKeys+1).__str__() + "/" + len(keys).__str__())
        #lPhraseOrigine.config(text=silver_file[key]["phrase origine"])
        lPhraseOrigine.delete(1.0, END)
        lPhraseOrigine.insert(1.0, silver_file[key]["phrase origine"])
        try:
            a=nlp.annotate(silver_file[key]["phrase origine"], properties={
                "pipelineLanguage": "en",
                'annotators': 'tokenize,pos,depparse,parse',
                'outputFormat': 'json'
            })#'java.lang.IllegalArgumentException: annotator "parse" requires annotation "PartOfSpeechAnnotation". The usual requirements for this annotator are: '

            for s in a['sentences']:
                Tree.fromstring(s['parse']).pretty_print()
        except:
            pass
            # TreeView(Tree.fromstring(s['parse']))._cframe.print_to_file('output.ps')
            #
            # import PIL
            # original = PIL.Image.open("output.ps")
            # original.show()
            # picture = ImageTk.PhotoImage(original)
            # myimg = c.create_image((0,0),image=picture, anchor="nw")#
            # c.itemconfigure(myimg, image=picture)  #

        lPhraseTransformée.delete(1.0, END)
        lPhraseTransformée.insert(1.0, key)
        if "structures silver" not in silver_file[key].keys():
            silver_file[key]["structures silver"] = []
        if silver_file[key]["structures silver"] == []:
            silver_file[key]["structures silver"] = data_calc[key]["structures"]

        for constraint in constraintButton.keys():
            if constraint in silver_file[key]["structures silver"]:
                constraintButton[constraint].select()
            else:
                constraintButton[constraint].deselect()
            if constraint in data_calc[key]["structures"]:
                constraintRefButton[constraint].select()
            else:
                constraintRefButton[constraint].deselect()

        phraseEtudiée = working_data[key]
        cles_contraintes = sorted(list(phraseEtudiée.keys()))
        cles_contraintes.remove("tokens")
        tokens = phraseEtudiée['tokens']

        for r in range(nb_max_constraint):
            sep = ""
            if r < len(cles_contraintes):
                sep = "."
            labelsGrids[r][0].config(text=sep, background=orig_color)
            i_tmp = 1
            while i_tmp < len(labelsGrids[r]):
                if i_tmp > len(tokens):
                    sep = ""
                labelsGrids[r][i_tmp].config(text=sep, background=orig_color)
                i_tmp += 1
            if r < len(cles_contraintes):
                labelsGrids[r][0].config(text=cles_contraintes[r] + "   ")
                colors = ["white", "green", "blue", "cyan", "yellow", "magenta", "red"]

                for contrainte in phraseEtudiée[cles_contraintes[r]]:
                    if len(colors) == 0:
                        colors = ["white", "green", "blue", "cyan", "yellow", "magenta", "red"]
                    color = colors.pop(0)
                    for noeud in contrainte:
                        labelsGrids[r][int(noeud)].config(text=tokens[int(noeud)-1], background=color)





        indexKeys += 1

    elif skipValue.get() == 1:
        print(indexKeys)
        print("#1")
        indexKeys+=1
        next()
    else:
        print("but...")
        indexKeys += 1
    vGoto.set(indexKeys)

filtre = StringVar()
filtre.set("Tous")

def filtrer():
    global keys
    global filtre
    global filtre
    global indexKeys
    indexKeys = 0
    réponse = filtre.get()
    if réponse == "Tous":
        keys = sorted(silver_file.keys())
        explications.set('Choisir un filtre pour réduire la liste à ce qui est affecté par lui.')
    else:
        keys=[]
        tmp_keys=[key for key in silver_file.keys()]
        for key in tmp_keys:
            if key not in data_calc.keys():
                del silver_file[key]
                print("Exemple non trouvé")
            else :
                if réponse in silver_file[key]["structures silver"] or réponse in data_calc[key]["structures"]:
                        keys.append(key)
            if réponse in WHAT.keys():
                explications.set(réponse + " : \n" + WHAT[réponse])
            else:
                explications.set(réponse + " : explication non disponible.")
    next()


pfilter.add(Label(p, text='Filtres', justify=LEFT))
b= Radiobutton(pfilter, text="Tous (affectés ou pas d'un côté comme de l'autre)", variable=filtre, value="Tous", indicatoron=0, command=filtrer)
pfilter.add(b)
for constraint in CONSTRAINTS:
    b = Radiobutton(pfilter, text=constraint + " affecté", variable=filtre, value=constraint, indicatoron=0, command=filtrer)
    pfilter.add(b)

p2 = PanedWindow(p, orient=HORIZONTAL)

boutonSauver=Button(p2, text="Sauver", command=save)
p2.add(boutonSauver)

boutonFermer=Button(p2, text="Fermer", command=fenetre.quit)
p2.add(boutonFermer)

p.add(p2)


#p.add(c)

p.pack()

p1 = PanedWindow(p, orient=HORIZONTAL)



def goto():
    global keys
    global indexKeys
    global vGoto

    s=vGoto.get()
    if s.isdigit():
        s=int(s)
        if s<len(keys) and s >= 0:
            indexKeys = s-1
            next()



tGoTo = Entry(p1, textvariable=vGoto)
p1.add(tGoTo)
boutonGoTo=Button(p, text="Aller à", command=goto)
p1.add(boutonGoTo)
p.add(p1)




boutonSuivant=Button(p, text="Suivant", command=next)
p.add(boutonSuivant)

next()
fenetre.mainloop()

exit()
