import json


REFERENCE_FILE = "statistiques_reference.json"
GENERAL_FILE = "statistiques_all.json"

file = open(REFERENCE_FILE)
silver_file = json.load(file)
file.close()

dictionnaire = {}
références = {}
for key in silver_file.keys():
    encoded = key.replace('""', '"').replace('"', '""')
    sentence = silver_file[key]["phrase origine"].replace('""', '"').replace('"', '""')
    structures = silver_file[key]["structures"]
    for structure in structures:
        if structure not in dictionnaire.keys():
            dictionnaire[structure] = set()
        dictionnaire[structure].add(sentence)
    références[sentence] = encoded

csv = ["sentence,encoded,"]
csv[-1] += ",".join(sorted(dictionnaire.keys()))
print(csv)
for sentence in références.keys():
    csv.append('"'+sentence+'","'+références[sentence]+'"')

    for structure in sorted(dictionnaire.keys()):
        if sentence in dictionnaire[structure]:
            csv[-1] += ",yes"
        else:
            csv[-1] += ","


fWfile = open("CF structures.csv", "w")

fWfile.write( "\n".join(csv))
fWfile.flush()
fWfile.close()
