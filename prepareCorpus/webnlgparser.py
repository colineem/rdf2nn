import sys
import shared
from xml.etree import ElementTree
import pygraphviz as pgv
import json
import re
import shutil, os
import math

VERSION = 0.99998
print(VERSION)
debug = False

print("debug " + debug.__str__())

REP_WEBNLG = shared.REP_WEBNLG
DATA="webnlgparser_data/"
JSON_LEARNING_BASE = DATA+"logs/learning_base.json"
JSON_LEARNING_REFS = DATA+"logs/learning_refs.json"
JSON_CONSTRAINTS_DIR = DATA + "detailled_informations/" #+constraint+".json"

JSON_INDEX_MOTS = DATA + "index_mots.json"

XML_WORKING_DATA_FILE = DATA+"working_data.xml"

ERRORS_DIR = DATA + "logs/"

data_errors = {}
base_verbale=r = re.compile("V.*")
data_per_types={}

#nouveaux identifiants pour les xlidxxxxlrid:

if os.path.isfile('similarite_data/identifiants.json'):
    file = open('similarite_data/identifiants.json')
    ids_xlirds = json.load(file)
    file.close()
else:
    ids_xlirds={}

if os.path.isfile('similarite_data/identifiants.json'):
    file = open('similarite_data/logs/id--valeurs.json')
    ids_xlirds_vals = json.load(file)
    file.close()
else:
    ids_xlirds_vals = {}


class Tree(object):

    def manageError(self,  exception, sentence, analyzed_sentence):

        if str(type(exception)) not in data_errors.keys():
            data_errors[str(type(exception))] = []
        data_errors[str(type(exception))].append(sentence.text)
        data_errors[str(type(exception))].append(analyzed_sentence.text)

        del self.learning_base_dictionnary[sentence.text]
        return False

    #initialisation et chargement des données
    def __init__(self):
        self.anonymised_dictionnary={}
        self.learning_base_dictionnary={}

        self.learning_base={}
        self.tree = ElementTree.parse(shared.WORKING_DATA_DIR + "output.xml")
        print("arbre chargé")
        self.make_enhanced_système()
        self.make_basic_système()
        self.parse()
        print("\nparsing effectué")
        self.save()

    def make_enhanced_système(self):
        import rules
        self.enhanced_system=rules.System()
        #self.alternative_system=rules.System()
        # add link(départflèche, arriveflèche, étiquetteflèche)
        # on va créer nos règles


        # "Alfred Garth Jones died in London which is lead by the European Parliament."
        # "Aaron Bertram, who plays Ska punk, is an artist with the band Kids Imagine Nation."
        # pas de détection de la relative sujet dans le premier cas.
        # étant donné que je n'ai que des phrases affirmatives...
        b_phrases_affirmatives = True

        if b_phrases_affirmatives:
            rule_relativeSubject = self.enhanced_system.create_rule("relative_subject")
            x = rule_relativeSubject.add_node(pos="WP", inside_allow=False)
            y = rule_relativeSubject.add_node(pos="V.*", inside_allow=False)
            z = rule_relativeSubject.add_node()
            l1 = rule_relativeSubject.forbid_link(y, x, 'nsubj.*', "")
            l1 = rule_relativeSubject.add_link(z, x, 'nsubj.*')
            l1 = rule_relativeSubject.add_link(x, z, 'acl:relcl')
            l2 = rule_relativeSubject.add_link(x, y, 'ref')
            rule_relativeSubject.set_position(x, y)
            rule_relativeSubject.set_position(y, z)

            rule_relativeSubject = self.enhanced_system.create_rule("relative_subject_2")
            x = rule_relativeSubject.add_node(pos="W.*", inside_allow=False)
            y = rule_relativeSubject.add_node(pos="")
            z = rule_relativeSubject.add_node(pos="")

            rule_relativeSubject.add_link(z, y, "nsubj.*")
            rule_relativeSubject.add_link(y, z, "acl:relcl")
            rule_relativeSubject.add_link(y, x, "ref")

            rule_relativeSubject.set_position(y, x)
            rule_relativeSubject.set_position(x, z)



        else:

            #relative subject
            rule_relativeSubject = rules.System.Rule(name="relative_subject")
            #soit x un noeud pos_taggué V.*
            x = rule_relativeSubject.add_node()
            #soit y un noeud quelconque
            y = rule_relativeSubject.add_node()
            #soit l1 le lien unissant y à x, de nature 'acl:relcl' : y est uni à x pour une relation de type relative clause
            #y a une relative clause dont l'évènement est x
            l1 = rule_relativeSubject.add_link(y, x, 'acl:relcl')
            #soit l2 le lien unissant x à y, de nature 'nsubj' : x a pour sujet y
            #x a pour sujet y
            l2 = rule_relativeSubject.add_link( x, y, 'nsubj.*')
            #et on ajoute la règle
            self.enhanced_system.add_rule(rule_relativeSubject)


        #relative adverbs
        rule_relativeAdverbs = rules.System.Rule(name="relative_adverb")
        #soit x un noeud pos_taggué *
        x = rule_relativeAdverbs.add_node()
        #soit y un noeud quelconque
        y = rule_relativeAdverbs.add_node(pos='WRB')
        #soit l1 le lien unissant faisant x modifié par y, de type advmod
        l1 = rule_relativeAdverbs.add_link(x, y, 'advmod')

        #soit z un noeud quelconque
        #z un nœud qui sera dans une relation de relcl avec ce que l'adverbe modifie
        z = rule_relativeAdverbs.add_node()
        # soit l2 le lien unissant z à x, de nature 'acl:relcl'
        l2 = rule_relativeAdverbs.add_link(z, x, 'acl:relcl')

        #sachant que z n'est pas le sujet de x
        l3 = rule_relativeAdverbs.forbid_link(x, z, 'nsubj.*', "")

        self.enhanced_system.add_rule(rule_relativeAdverbs)


        rule_coord = rules.System.Rule(name="coordinated_full_clauses_1")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='')
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 et s2 deux sujets
        s1 = rule_coord.add_node()
        s2 = rule_coord.add_node()
        rule_coord.add_link(v1, s1, "nsubj.*")
        rule_coord.add_link(v2, s2, "nsubj.*")
        rule_coord.forbid_link(v1, v2, "advcl", "")

        #il existe un nœud quelconque k
        k=rule_coord.add_node()
        #ce nœud est à droite du sujet 2
        rule_coord.set_position(s2, k)
        #il n'a pas le lien ref avec le sujet de v2
        #si c'était le cas, nous aurions une relative sujet
        rule_coord.forbid_link(s2, k, "ref", "")#je ne peux pas dire qu'il est à gauche de v2
        rule_coord.forbid_link(s2, v2, "acl:relcl", "")#je ne peux pas dire qu'il est à gauche de v2, je vais bloquer l'acl:relcl


        #soit c une coordination
        c = rule_coord.add_node(pos="CC", inside_allow=False)
        rule_coord.set_position(v1, c)
        rule_coord.set_position(s1, c)
        rule_coord.set_position(c, v2)
        rule_coord.set_position(c, s2)
        x = rule_coord.add_node()
        rule_coord.forbid_link(x, s2, "cop", "")  # on exclut les cop [x -> verbe être]
                                                #gestion deux temps

        #j'ajoute la règle
        self.enhanced_system.add_rule(rule_coord)

        #on s'occupe des "is"
        rule_coord = rules.System.Rule(name="coordinated_full_clauses_2")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='')
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 le sujet du verbe 1
        s1 = rule_coord.add_node()
        #soit s2 le sujet du verbe 2
        #soit x ce qui est
        x = rule_coord.add_node()
        rule_coord.add_link(v1, s1, "nsubj.*")
        #on relie x à v2 par un lien cop
        rule_coord.add_link(x, v2, "cop")
        #est relié à v1 dans le cadre de la coordination verbale
        rule_coord.add_link(v1, x, "conj.*")
        #et ce qui est cop est sujet
        sujet= rule_coord.add_node()
        rule_coord.add_link(x, sujet, ".subj.*")


        #soit c une coordination
        c = rule_coord.add_node(pos="CC", inside_allow=False)
        rule_coord.set_position(v1, c)
        rule_coord.set_position(s1, c)
        rule_coord.set_position(c, sujet)
        rule_coord.set_position(c, x)
        rule_coord.set_position(c, v2)
        #j'ajoute la règle
        self.enhanced_system.add_rule(rule_coord)

        rule_coord = rules.System.Rule(name="coordinated_clauses_1")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 et s2 deux sujets
        s1 = rule_coord.add_node()
        s2 = rule_coord.add_node()
        rule_coord.add_link(v1, s1, "nsubj.*")
        rule_coord.forbid_link(v2, s2, "nsubj.*", "")
        x = rule_coord.add_node()
        rule_coord.forbid_link(x, s2, "cop", "")#on exclut les cop [x -> verbe être] aboutissant à ce que être n'ait pas de sujet.
        rule_coord.forbid_link(x, v1, "csubj.*", "")#on exclut les cop [x -> verbe être] aboutissant à ce que être n'ait pas de sujet.
        x2 = rule_coord.add_node()


        #soit c une coordination
        c = rule_coord.add_node(pos="CC", inside_allow=False)
        rule_coord.set_position(v1, c)
        rule_coord.set_position(s1, c)
        rule_coord.set_position(s2, c)#s2 aussi est après cc
        rule_coord.set_position(c, v2)
        rule_coord.set_position(c, x2)
        rule_coord.set_position(c, s2)
        rule_coord.forbid_link(s1, x2, "acl:relcl", "")
        #j'ajoute la règle
        self.enhanced_system.add_rule(rule_coord)

        rule_coord = rules.System.Rule(name="coordinated_clauses_2")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 le sujet commun
        s1 = rule_coord.add_node()
        s2 = rule_coord.add_node()
        rule_coord.add_link(v1, s1, "nsubj.*")
        rule_coord.add_link(v2, s1, "nsubj.*")

        #soit c une coordination
        c = rule_coord.add_node(pos="CC", inside_allow=False)
        rule_coord.set_position(v1, c)
        rule_coord.set_position(s1, c)
        rule_coord.set_position(c, v2)
        #j'ajoute la règle
        self.enhanced_system.add_rule(rule_coord)

        #on s'occupe des "is"
        rule_coord = rules.System.Rule(name="coordinated_clauses_3")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 le sujet commun
        s1 = rule_coord.add_node()
        #soit x ce qui est
        x = rule_coord.add_node()
        rule_coord.add_link(v1, s1, "nsubj.*")
        #on relie x à v2 par un lien cop
        rule_coord.add_link(x, v2, "cop")
        #est relié à v1 dans le cadre de la coordination verbale
        rule_coord.add_link(v1, x, "conj.*")
        #et ce qui est cop n'est pas sujet
        #pas sujet
        nonsuj= rule_coord.add_node()
        rule_coord.forbid_link(x, nonsuj, ".subj.*", "")


        #on s'occupe des "is"
        rule_coord = rules.System.Rule(name="coordinated_clauses_4")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 le sujet commun
        s1 = rule_coord.add_node()
        #soit x ce qui est
        x1 = rule_coord.add_node()
        rule_coord.add_link(x1, s1, "nsubj.*")
        rule_coord.add_link(v2, s1, "nsubj.*")


        #on relie x à v1 par un lien cop
        rule_coord.add_link(x1, v1, "cop")
        #est relié à v2 dans le cadre de la coordination verbale
        rule_coord.add_link(v2, x1, "conj.*")



        #soit c une coordination
        c = rule_coord.add_node(pos="CC", inside_allow=False)
        rule_coord.set_position(x1, c)
        rule_coord.set_position(v1, c)
        rule_coord.set_position(s1, c)
        rule_coord.set_position(c, v2)
        #j'ajoute la règle
        self.enhanced_system.add_rule(rule_coord)

        #on s'occupe des "is"
        rule_coord = rules.System.Rule(name="coordinated_clauses_5")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 le sujet
        s1 = rule_coord.add_node()
        #soit x1 et x2 ce qui est
        x1 = rule_coord.add_node()
        x2 = rule_coord.add_node()
        #on relie x à v1 par un lien cop
        rule_coord.add_link(x1, v1, "cop")
        #et x1 est relié au sujet par nsubj
        rule_coord.add_link(x1, s1, "nsubj.*")
        #il n'y a qu'un seul sujet référencé dans ce cas.

        #on relie x2 à v2 par un lien cop
        rule_coord.add_link(x2, v2, "cop")
        x3 = rule_coord.add_node()

        rule_coord.forbid_link(x2, x3, "nsubj.*", "")

        #et un lien conj.* lie x1 et x2
        rule_coord.add_link(x1, x2, "conj.*")

        #par ailleurs nous avons une coordination au milieu

        #soit c une coordination
        c = rule_coord.add_node(pos="CC", inside_allow=False)
        rule_coord.set_position(x1, c)
        rule_coord.set_position(v1, c)
        rule_coord.set_position(s1, c)
        rule_coord.set_position(c, v2)
        rule_coord.set_position(c, x2)
        rule_coord.set_position(c, x3)#et non, il n'y a pas de sujet pour ce cop !s
        #j'ajoute la règle
        self.enhanced_system.add_rule(rule_coord)



        rule_passive = rules.System.Rule(name="passive_voice")
        #soit deux nœuds quelconques
        x = rule_passive.add_node()
        y = rule_passive.add_node()
        #il existe un lien .subjpass unissant deux noeuds
        rule_passive.add_link( x, y, '.subjpass.*')
        #ajout de la règle au système
        self.enhanced_system.add_rule(rule_passive)

        #alors visiblement cela détecte très très mal les clausal subject. Une horreur.
        #CF English language is spoken in Great Britain and a Severed Wasp was written in this language.
        #That his theory was flawed soon became obvious.
        #What she said makes sense.
        clausal_works=False
        if clausal_works:
            rule_cs = rules.System.Rule(name="clausal_subject")
            #soit deux nœuds quelconques
            x = rule_cs.add_node('V.*', inside_allow=False)
            y = rule_cs.add_node()
            #il existe un lien csubj.* unissant deux noeuds
            rule_cs.add_link(x, y, 'csubj.*')
            #ajout de la règle au système
            self.enhanced_system.add_rule(rule_cs)
        else:

            rule_cs = rules.System.Rule(name="clausal_subject")
            #soit verbe_maitre qui aura un sujet
            verbe_maitre =rule_cs.add_node('VB.*', inside_allow=False)
            #le verbe de la clause
            verbe_clause =rule_cs.add_node('VB.*', inside_allow=False)
            #nous sommes dans des phrases simples
            rule_cs.set_position(verbe_clause, verbe_maitre)
        if clausal_works:
            #le verbe maître sera dépourvu e sujet
            x = rule_cs.add_node()
            rule_cs.forbid_link(verbe_maitre, x, ".subj.*", "")
            #et il n'est pas le verbe être
            rule_cs.forbid_link(x, verbe_maitre, "cop", "")
            rule_cs.add_link(verbe_clause, verbe_maitre, "ccomp")
            #cette règle vise à éviter l'intégration des relatives type
            #The ground of A.S. Gubbio 1910 is located in Italy, where the people who live there are called Italians.
            #sinon elles correspondraient
            rule_cs.forbid_link(rule_cs.add_node(), verbe_maitre, "acl:relcl", "")

            #ajout de la règle au système
            self.enhanced_system.add_rule(rule_cs)

            #nous pourrions aussi avoir le verbe être. c'est détectable avec clausal subject, plutôt avec des what kjiso wants is ksdj
            rule_cs = rules.System.Rule(name="clausal_subject_3")
            #soit verbe_maitre qui aura un sujet
            verbe_maitre =rule_cs.add_node('VB.*', inside_allow=False)
            #le verbe de la clause
            verbe_clause =rule_cs.add_node('VB.*', inside_allow=False)
            #nous sommes dans des phrases simples
            rule_cs.set_position(verbe_clause, verbe_maitre)

            #le verbe maître sera dépourvu e sujet
            x = rule_cs.add_node()
            #et il est le verbe être
            rule_cs.add_link(x, verbe_maitre, "cop")

            rule_cs.add_link(x, verbe_clause, "csubj*")

            #ajout de la règle au système
            self.enhanced_system.add_rule(rule_cs)

            #nous pourrions aussi avoir le verbe être. c'est détectable avec un ccomp quand pré-fixé

            rule_cs = rules.System.Rule(name="clausal_subject_2")
            #soit verbe_maitre qui aura un sujet
            verbe_maitre =rule_cs.add_node('VB.*', inside_allow=False)
            #le verbe de la clause
            verbe_clause =rule_cs.add_node('VB.*', inside_allow=False)
            #nous sommes dans des phrases simples
            rule_cs.set_position(verbe_clause, verbe_maitre)

            #le verbe maître sera dépourvu e sujet
            x = rule_cs.add_node()
            #et il est le verbe être
            rule_cs.add_link(x, verbe_maitre, "cop")
            rule_cs.forbid_link(x, rule_cs.add_node(), "nsubj.*", "")

            rule_cs.add_link(verbe_clause, x, "ccomp")

            #ajout de la règle au système
            self.enhanced_system.add_rule(rule_cs)



        rule_appos = rules.System.Rule(name="apposition")
        #soit deux nœuds quelconques
        x = rule_appos.add_node()
        y = rule_appos.add_node()
        #il existe un lien csubj.* unissant deux noeuds
        rule_appos.add_link( x, y, 'appos')
        #ajout de la règle au système
        self.enhanced_system.add_rule(rule_appos)

        # case_rule = rules.System.Rule(name="possessif")
        # #soit deux nœuds quelconques
        # x = case_rule.add_node()
        # y = case_rule.add_node()
        # #il existe un lien csubj.* unissant deux noeuds
        # case_rule.add_link( x, y, 'case')
        # #ajout de la règle au système
        # self.enhanced_system.add_rule(case_rule)

        case_rule = rules.System.Rule(name="possessif")
        #soit deux nœuds quelconques
        x = case_rule.add_node()
        y = case_rule.add_node()
        #il existe un lien csubj.* unissant deux noeuds
        case_rule.add_link( x, y, 'nmod:poss')
        #ajout de la règle au système
        self.enhanced_system.add_rule(case_rule)



        #
        #coordonnées
        rule_juxtaClauses = rules.System.Rule(name="juxtaposition")
        n1 = rule_juxtaClauses.add_node()
        # un point final
        n2 = rule_juxtaClauses.add_node()

        sujet1 = rule_juxtaClauses.add_node()
        sujet2 = rule_juxtaClauses.add_node()
        #on veut que ces noeuds aient des sujets
        rule_juxtaClauses.add_link(n1, sujet1, 'nsubj.*')
        rule_juxtaClauses.add_link(n2, sujet2, 'nsubj.*')

        rule_juxtaClauses.add_link(n1, n2, 'parataxis')
        if False:
            #soit x et y deux verbes
            verbe1 = rule_juxtaClauses.add_node(pos='V.*', word=".*(?!:ing)$", inside_allow=False)
            verbe2 = rule_juxtaClauses.add_node(pos='V.*', word=".*(?!:ing)$", inside_allow=False)
            # une virgule
            virgule = rule_juxtaClauses.add_node(word=',')
            # un point final
            point = rule_juxtaClauses.add_node(word='.')
            #un wrb potentiel
            wrbpos = rule_juxtaClauses.add_node(inside_allow=False)

            #on interdit un lien typé relative clause sur le dernier verbe avec un object entre les deux verbes.
            rule_juxtaClauses.forbid_link(verbe2, wrbpos, "acl:relcl", "")
            rule_juxtaClauses.set_position(verbe1, wrbpos)
            rule_juxtaClauses.set_position(wrbpos, verbe2)
            #et la seconde clause n'est pas une clause adverbiale de la première
            rule_juxtaClauses.forbid_link(verbe1, verbe2, "a.*cl.*", "")

            #deux sujets
            sujet1= rule_juxtaClauses.add_node(pos="^(?!W).*$")
            sujet2= rule_juxtaClauses.add_node(pos="^(?!W).*$")
            #le sujet 2 n'est pas lié à un noeaud quelconque par une relation advmod si ce noeud est de type "WRB"

            noeud_quelconque = rule_juxtaClauses.add_node()
            rule_juxtaClauses.forbid_link(verbe2, noeud_quelconque, "advmod", "WRB")

            rule_juxtaClauses.add_link(verbe1, sujet1, '.subj.*')
            rule_juxtaClauses.add_link(verbe2, sujet2, '.subj.*')
            rule_juxtaClauses.forbid_link(sujet2, noeud_quelconque, "ref", "WRB")

            #verbe1 est relié à deux puncts --> clôture virgule et clôture point final
            rule_juxtaClauses.add_link(verbe1, virgule, "punct")
            rule_juxtaClauses.add_link(verbe1, point, "punct")

            rule_juxtaClauses.set_position(verbe1, virgule)
            rule_juxtaClauses.set_position(sujet1, virgule)
            rule_juxtaClauses.set_position(virgule, verbe2)
            rule_juxtaClauses.set_position(virgule, sujet2)
            rule_juxtaClauses.set_position(sujet1, sujet2)
            rule_juxtaClauses.set_position(verbe1, verbe2)


        self.enhanced_system.add_rule(rule_juxtaClauses)


        objectDirect = rules.System.Rule(name="direct_object")
        #soit x et y deux verbes
        x = objectDirect.add_node(pos='V.*', inside_allow=False)
        y = objectDirect.add_node(pos='')

        objectDirect.add_link(x, y, 'dobj')

        #j'ajoute la règle
        self.enhanced_system.add_rule(objectDirect)

        objectInDirect = rules.System.Rule(name="indirect_object")
        #soit x et y deux verbes
        x = objectInDirect.add_node(pos='V.*', inside_allow=False)
        y = objectInDirect.add_node(pos='')

        objectInDirect.add_link(x, y, 'iobj')

        #j'ajoute la règle
        self.enhanced_system.add_rule(objectInDirect)

        #two objects


        twiceRule = rules.System.Rule(name="twice_objects")
        #soit x et y deux verbes
        x = twiceRule.add_node(pos='V.*', inside_allow=False)
        y = twiceRule.add_node(pos='')
        z = twiceRule.add_node(pos='')

        twiceRule.add_link(x, y, 'iobj')
        twiceRule.add_link(x, z, 'dobj')

        #j'ajoute la règle
        self.enhanced_system.add_rule(twiceRule)

        existencielle = rules.System.Rule(name="existential")
        #soit x et y deux verbes
        y = existencielle.add_node(pos='')
        z = existencielle.add_node(pos='V.*', inside_allow=False)

        existencielle.add_link(y, z, 'cop')

        #j'ajoute la règle
        self.enhanced_system.add_rule(existencielle)

        ptc = rules.System.Rule(name="participle clause")
        # soit x, y et z
        x = ptc.add_node(pos='V.*')
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBG')

        ptc.add_link(y, z, 'auxpass')
        ptc.add_link(x, y, 'advcl.*')
        ptc.forbid_link(y, ptc.add_node(), 'nsubj.*')
        self.enhanced_system.add_rule(ptc)

        ptc = rules.System.Rule(name="participle clause_bis_1")
        #soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBG')

        ptc.add_link(y, z, 'advcl')
        self.enhanced_system.add_rule(ptc)

        ptc = rules.System.Rule(name="participle clause_ter_1")
        #soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBN')

        ptc.add_link(y, z, 'advcl')
        ptc.forbid_link(ptc.add_node(), y, 'acl:relcl')
        self.enhanced_system.add_rule(ptc)

        ptc = rules.System.Rule(name="participle clause_bis_2")
        #soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBG')
        x = ptc.add_node()

        ptc.add_link(x, y, 'cop')
        ptc.add_link(x, z, 'advcl')
        ptc.forbid_link(ptc.add_node(), y, 'acl:relcl')
        self.enhanced_system.add_rule(ptc)

        ptc = rules.System.Rule(name="participle clause_3")
        #soit x, y
        x = ptc.add_node()
        y = ptc.add_node(pos='VBG')

        ptc.add_link(x, y, 'acl.*')
        ptc.forbid_link(y, ptc.add_node(), 'nsubj.*')
        self.enhanced_system.add_rule(ptc)

        ptc = rules.System.Rule(name="participle clause_3_ter_2")
        #soit x, y
        x = ptc.add_node()
        y = ptc.add_node(pos='VBN')
        z = ptc.add_node()

        ptc.add_link(x, y, 'amod')
        ptc.add_link(z, x, 'dep')

        self.enhanced_system.add_rule(ptc)


        ptc = rules.System.Rule(name="participle clause_subject_2_1")
        #soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBG')

        ptc.add_link(z, ptc.add_node(), 'nmod.*')
        ptc.add_link(y, z, 'csubj.*')
        self.enhanced_system.add_rule(ptc)



        ptc = rules.System.Rule(name="participle clause_subject_3_1")
        #soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBG')
        x = ptc.add_node()

        ptc.add_link(z, x, 'dobj')
        ptc.add_link(y, z, 'csubj.*')
        self.enhanced_system.add_rule(ptc)


        ptc = rules.System.Rule(name="participle clause_subject_2_2")
        #soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBN')
        
        ptc.add_link(z, ptc.add_node(), 'nmod.*')
        ptc.forbid_link(ptc.add_node(), z, 'acl:relcl')
        ptc.add_link(y, z, 'csubj.*')
        self.enhanced_system.add_rule(ptc)

        ptc = rules.System.Rule(name="participle clause_subject_3_2")
        # soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBN')

        ptc.add_link(z, ptc.add_node(), 'dobj')
        ptc.forbid_link(ptc.add_node(), z, 'acl:relcl')
        ptc.add_link(y, z, 'csubj.*')
        self.enhanced_system.add_rule(ptc)

    def make_basic_système(self):
        import rules
        self.basic_system=rules.System()
        #relative object
        rule_relativeObject = rules.System.Rule(name="relative_object_1")
        #soit x un noeud quelconque
        x = rule_relativeObject.add_node()
        #soit y un noeud quelconque qui sera un pronom relatif
        pronom_relatif = rule_relativeObject.add_node(pos="WDT")

        what_relative = rule_relativeObject.add_node()
        rule_relativeObject.add_link(x, what_relative, "acl:relcl" )
        rule_relativeObject.add_link(what_relative, pronom_relatif, 'ref')

        self.basic_system.add_rule(rule_relativeObject)

        #relative object
        rule_relativeObject = rules.System.Rule(name="relative_object_2")
        #soit x un noeud quelconque qui sera sujet
        x = rule_relativeObject.add_node()
        #soit y un noeud quelconque qui sera un pronom relatif
        pronom_relatif = rule_relativeObject.add_node(pos="W.*")
        #verbe
        rule_relativeObject.add_link(x, pronom_relatif, 'nmod.*' )

        self.basic_system.add_rule(rule_relativeObject)

        if False:

            #relative object

            # suspendu à cause analyse type : An affiliate of the European University Association, the School of Business and Social Sciences at the Aarhus University in Aarhus, Denmark has 16000 students enrolled.
            # ou encore The 1 Decembrie 1918 University of Alba Iulia, Romania has the nickname Uab and the rector is called \"Breaz Valer Daniel\".


            rule_relativeObject = rules.System.Rule(name="relative_object_3")
            #soit x un noeud quelconque qui sera sujet
            x = rule_relativeObject.add_node()

            verbe_théorique = rule_relativeObject.add_node()
            rule_relativeObject.add_link(verbe_théorique, x, 'nsubj.*' )

            verbe_à_dépendance = rule_relativeObject.add_node()
            rule_relativeObject.add_link(verbe_à_dépendance, verbe_théorique,  'ccomp' )

            self.basic_system.add_rule(rule_relativeObject)


    def parse(self):
        root = self.tree.getroot()
        count=0
        shared.init_progress()

        if debug:
            data_to_parse = root.findall('file')[0:1]
        else:
            data_to_parse = root.findall('file')

        for file in data_to_parse:
            for eid in file.findall('eid'):
                count+=1

        i_step=0
        for file in data_to_parse:
            fichier_origine = file.attrib['name']
            if True: # "allSolutions" in fichier_origine and i_step<2:
                for eid in file.findall('eid'):
                    shared.progress(i_step, count)
                    i_step+=1
                    eidname = eid.attrib['name']
                    triples={}
                    for tripleset, tripleset_encoded in zip(eid.findall('tripleset/triple'), eid.findall('tripleset_encoded/triple') ):
                        t1_id= tripleset.attrib['tid']
                        t2_id= tripleset_encoded.attrib['tid']
                        if t1_id!=t2_id:
                            print("erreur d'appariement")
                            print("revoir logique")
                            sys.exit(5)
                        triplet = tripleset.text.split(" | ")
                        triplet_encoded = tripleset_encoded.text.split(" | ")
                        triples[t1_id]=triplet_encoded
                        for text, encoded_text in zip([triplet[0], triplet[2]], [triplet_encoded[0], triplet_encoded[2]]) :

                            if encoded_text not in self.anonymised_dictionnary.keys():
                                self.anonymised_dictionnary[encoded_text] = text.replace("_", " ")

                    for lex in eid.findall('lexicalisation'):

                        graphes=lex.findall('sentences/sentence/graphe')
                        basic_graphes=lex.findall('sentences/sentence/basic_graphe')
                        alt_graphes=lex.findall('sentences/sentence/grapheOnEncodedString')
                        e_triples = lex.findall('sentences/sentence/ROOT/embeddedTriples')
                        i_sent=0

                        if (len(graphes)>0 ) and (len(graphes) == len(alt_graphes)  or (len(graphes) != len(alt_graphes)  and len(graphes)== lex.find("encoded").text.count("."))):
                            lid = lex.attrib['lid']
                            coref_analyzed = lex.attrib['coref']
                            sentence = lex.find("encoded")
                            self.learning_base_dictionnary[sentence.text]=[triples]
                            analyzed_sentence = lex.find("analysed")
                            self.learning_base_dictionnary[sentence.text].append(analyzed_sentence.text)
                            #print(analyzed_sentence.text)

                            self.learning_base_dictionnary[sentence.text].append({})
                            for graphe, basic_graphe, alt_graphe, embedded_triples in zip(graphes, basic_graphes, alt_graphes,  e_triples):

                                #on étudie la réalisation de la relation : les structures grammaticales à l'intérieur
                                # de notre triplet n'ont pas d'importance
                                # on les exclut
                                internal_relation_triple_so_excluded=[]
                                for embedded_triple in embedded_triples.findall('triple'):
                                    internal_relation_triple_so_excluded.append([])
                                    for item in embedded_triple.findall('item'):
                                        internal_relation_triple_so_excluded[-1].append(item.attrib['idx'])

                                #constraints = self.alternative_system.apply_rules(alt_graphe.text, internal_relation_triple_so_excluded)
                                #constraints = self.enhanced_system.apply_rules(alt_graphe.text, internal_relation_triple_so_excluded[:])
                                constraints = self.enhanced_system.apply_rules(graphe.text, internal_relation_triple_so_excluded[:])
                                constraints_2 = self.basic_system.apply_rules(basic_graphe.text, internal_relation_triple_so_excluded[:])
                                for constraint_2_name, constraint_2_data in constraints_2.items():
                                    if constraint_2_name not in constraints.keys():
                                        constraints[constraint_2_name] = []
                                        for lot in constraint_2_data:
                                            constraints[constraint_2_name].append(lot)

                                self.learning_base_dictionnary[sentence.text][-1][i_sent] = {}
                                self.learning_base_dictionnary[sentence.text][-1][i_sent]["analyse"] = {}
                                self.learning_base_dictionnary[sentence.text][-1][i_sent]["analyse"]["constraints"] = constraints
                                self.learning_base_dictionnary[sentence.text][-1][i_sent]["analyse"]["initial_tokens"] = {}

                                for alpha in constraints:
                                    alpha=shared.nettoie(alpha)
                                    if alpha not in data_per_types.keys():
                                        data_per_types[alpha]=[]
                                    data_per_types[alpha].append((sentence.text, analyzed_sentence.text))

                                i_sent+=1

                            sentences = lex.findall('sentences/sentence')
                            do_it = True

                            for xml_sentence in sentences:
                                if do_it:
                                    sid=int(xml_sentence.attrib['sid'])
                                    try:
                                        for s_token in xml_sentence.findall('ROOT/rootItems/item'):
                                            attribs = s_token.attrib
                                            self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["initial_tokens"][attribs['idx']] = s_token.text


                                        tokenized_results={}
                                        tokenized_results_final={}
                                        for s_triple in xml_sentence.findall('triplets/triplet'):

                                            triplet = s_triple
                                            a = {}

                                            if s_triple.attrib['partial'] == "full":
                                                if "triplets_full" not in self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"].keys():
                                                    self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["triplets_full"] = {}
                                                self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["triplets_full"][triplet.attrib["tid"]] = a
                                                a["subject"] = {triplet.attrib["idSubject"]:[]}
                                                a["object"] = {triplet.attrib["idObject"]:[]}

                                            elif s_triple.attrib['partial'] == "subject":

                                                if "triplets_subject" not in self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"].keys():
                                                    self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["triplets_subject"] = {}

                                                a["subject"] = {triplet.attrib["idSubject"]:[]}
                                                self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["triplets_subject"][triplet.attrib["tid"]]  = a

                                            elif s_triple.attrib['partial'] == "object":

                                                if "triplets_object" not in self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"].keys():
                                                    self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["triplets_object"] = {}

                                                a["object"] =  {triplet.attrib["idObject"]:[]}
                                                self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["triplets_object"][triplet.attrib["tid"]]  = a

                                            for solution_objet in s_triple.findall('solutions/sujet'):
                                                a["subject"][triplet.attrib["idSubject"]].append(solution_objet.attrib)

                                            for solution_sujet in s_triple.findall('solutions/objet'):
                                                a["object"][triplet.attrib["idObject"]].append(solution_sujet.attrib)

                                            for level in a.keys():
                                                for id_triple_param in a[level].keys():
                                                    for refence in a[level][id_triple_param]:
                                                        deb = int(refence["indexDeb"])
                                                        fin = int(refence["indexFin"])
                                                        while deb <= fin:
                                                            tokenized_results[deb.__str__()]=id_triple_param
                                                            deb+=1


                                        table_des_index={}
                                        for s_token in xml_sentence.findall('ROOT/rootItems/item'):
                                            index_initial = s_token.attrib['idx']
                                            table_des_index[int(index_initial)]= s_token

                                        table_inversée_des_index={}
                                        i_count=1
                                        #on renumérote
                                        for s_token_key in sorted(table_des_index.keys()):
                                            s_token = table_des_index[s_token_key]
                                            index_initial = s_token.attrib['idx']

                                            #on en profite pour construire une utile table inversée
                                            table_inversée_des_index[index_initial] = i_count.__str__()

                                            #tokenized_results contient les index des subjects/objects
                                            if i_count.__str__() not in tokenized_results_final.keys():
                                                if index_initial in tokenized_results.keys() :
                                                    tokenized_results_final[i_count.__str__()] = {"text":[s_token.text], "valeur": tokenized_results[index_initial], "index":[index_initial]}
                                                else:
                                                    tokenized_results_final[i_count.__str__()] = {"text":[s_token.text], "index": [index_initial]}

                                            else:
                                                tokenized_results_final[i_count.__str__()]["text"].append(s_token.text)
                                                tokenized_results_final[i_count.__str__()]["index"].append(index_initial)
                                            tokenized_results_final[i_count.__str__()]['pos'] =  s_token.attrib['pos']
                                            tokenized_results_final[i_count.__str__()]['lemma'] =  s_token.attrib['lemma']

                                            #si i_count est une entité object/subjet on n'avance pas: je reste sur le même index
                                            if (s_token_key + 1).__str__() not in tokenized_results.keys() or s_token_key.__str__() not in tokenized_results.keys():

                                                i_count += 1
                                            elif tokenized_results[(s_token_key + 1).__str__()] != tokenized_results[index_initial]:
                                                i_count += 1

                                        self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["restructured_tokens"] = tokenized_results_final
                                        self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["restructured_constraints"] = {}
                                        self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["table_inversée_des_index"]=table_inversée_des_index


                                        for constraint in self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["constraints"].keys():
                                            simple_contrainte=shared.nettoie(constraint)
                                            #ex: contrainte=direct objet
                                            self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["restructured_constraints"][simple_contrainte]=[]
                                            #il y a potentiellement plusieurs lots de direct object, par exemple
                                            for lot in self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["constraints"][constraint]:
                                                self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["restructured_constraints"][simple_contrainte].append({})
                                                if len(self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["restructured_constraints"][simple_contrainte]) > 1:
                                                    print("")
                                                    print("plusieurs lots de contraintes")
                                                    print(sentence.text)
                                                    print()
                                                for node in lot.keys():
                                                    node_name = node+"_"+constraint
                                                    tab_contrainte = self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["restructured_constraints"][simple_contrainte][-1]
                                                    tab_contrainte[node_name]=[]
                                                    old_refs = lot[node].split(", ")
                                                    for old_ref in old_refs:
                                                        #souci, parfois un noeud ROOT
                                                        if table_inversée_des_index[old_ref] not in tab_contrainte[node_name]:
                                                            tab_contrainte[node_name].append(table_inversée_des_index[old_ref])

                                    except KeyError as inst:
                                        print()
                                        print(sentence.text)
                                        print(analyzed_sentence.text)
                                        exc_type, exc_obj, exc_tb = sys.exc_info()
                                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                                        print("Key Error")
                                        print((exc_type, fname, exc_tb.tb_lineno))
                                        do_it = self.manageError(inst, sentence, analyzed_sentence)

                                    except AttributeError as inst:
                                        print()
                                        print(sentence.text)
                                        print(analyzed_sentence.text)
                                        exc_type, exc_obj, exc_tb = sys.exc_info()
                                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                                        print("Attribute Error")
                                        print((exc_type, fname, exc_tb.tb_lineno))
                                        print()

                                        do_it = self.manageError(inst, sentence, analyzed_sentence)
                                    except Exception as inst:
                                        print()
                                        print(sentence.text)
                                        print(analyzed_sentence.text)
                                        exc_type, exc_obj, exc_tb = sys.exc_info()
                                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                                        print("Exception")
                                        print((exc_type, fname, exc_tb.tb_lineno))
                                        print()
                                        do_it = self.manageError(inst, sentence, analyzed_sentence)

                            if sentence.text in self.learning_base_dictionnary.keys():
                                self.learning_base_dictionnary[sentence.text].append(fichier_origine)

                        else:
                            # print()
                            # print(len(self.learning_base_dictionnary.keys()).__str__()+ " lexicalisations gérées.")
                            # print("Les graphes des lexicalisations suivantes ne font pas la même taille, mauvais parsing.")
                            # print(lex.find("encoded").text)
                            # print(lex.find("analysed").text)
                            # print()
                            if "taille graphe" not in data_errors.keys():
                                data_errors["taille graphe"]=[]
                            data_errors["taille graphe"].append(sentence.text)
                            data_errors["taille graphe"].append(analyzed_sentence.text)
                            # print(analyzed_sentence.text)
                            #if do_it: ---> l'update donne lieu au remplacement du contenu, donc à retenir : ne pas faire
                            #    data_per_types.update(tmp_data_per_types)

        shared.progress(1,1)
        shared.init_progress()

    # def getContenu(selfself, graphe, items_idx):#, items_values):
    #
    #     G = pgv.AGraph(graphe.text)
    #     t_verbes = list(filter(r.match, items_idx.values()))
    #     constrs=[]
    #     if len(t_verbes)>0:
    #         verbes=[]
    #         for a in items_idx.keys():
    #             if items_idx[a] in t_verbes:
    #                 verbes.append(a)
    #         for verbe in verbes:
    #             node_verb=G.get_node(verbe)
    #             for parent in G.predecessors(node_verb):
    #                 if G.get_edge( parent, node_verb).key=="acl:relcl":
    #                     print(graphe.text)
    #                     if G.has_edge(verbe, parent, key='nsubj') or G.has_edge(verbe, parent, key='nsubjpass') :
    #                         print(graphe.text)
    #                         constrs.append('rel_subj')
    #
    #     return constrs

    def save(self):
        try:
            for key in data_errors.keys():
                fjson = open(ERRORS_DIR + key + ".json", "w")
                fjson.write(json.dumps(data_errors[key], indent=2, ensure_ascii=False))
                fjson.flush()
                fjson.close()
                print("erreurs sauvegardées pour "+key+".")
                while not fjson.closed:
                    True
        except KeyboardInterrupt:
            if not fjson.closed:
                fjson.close()
                while not fjson.closed:
                    True

        i_count= 0
        for key in data_errors.keys():
            i_count+=len(data_errors[key])

        print(i_count.__str__() + " entrées")
        print()

        try:
            fjson = open(JSON_LEARNING_BASE, "w")
            fjson.write(json.dumps(self.anonymised_dictionnary, indent=2,ensure_ascii=False))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
        except KeyboardInterrupt:
            if not fjson.closed:
                fjson.close()
                while not fjson.closed:
                    True
        print("learning base (correspondance clés/subject object des triplets) assurée.")

        print((len(self.anonymised_dictionnary.keys())).__str__() + " entrées")
        print()

        shutil.rmtree(JSON_CONSTRAINTS_DIR, ignore_errors=True)
        os.makedirs(JSON_CONSTRAINTS_DIR)
        for constraint in data_per_types.keys():
            try:
                file= JSON_CONSTRAINTS_DIR + constraint+".json"
                fjson = open(file, "w")
                fjson.write(json.dumps(data_per_types[constraint],  indent=2,ensure_ascii=False))
                fjson.flush()
                fjson.close()
                while not fjson.closed:
                    True
            except KeyboardInterrupt:
                if not fjson.closed:
                    fjson.close()
                    while not fjson.closed:
                        True
        print("sauvegarde effectuée des listes log des contraintes repérées")

        print("travail sur les logs")

        #on va d'abord indexer nos identifiants subjects/objects
        index_id_so=[]
        for key in self.anonymised_dictionnary.keys():
            index_id_so.append(key)

        #il va nous falloir un index des relations
        index_relations=[]
        #et un index des mots
        index_mots=[]
        #qu'on a potentiel enregistrés triés par similité
        if os.path.isfile(JSON_INDEX_MOTS) :
            file = open(JSON_INDEX_MOTS)
            index_mots = json.load(file)
            file.close()

        print("Construction du xml")

        shared.init_progress()
        xml_restructured_data = ElementTree.Element("root")
        i_count=0
        lenk = len(self.learning_base_dictionnary.keys())
        for lexicalisation_traitée_key in self.learning_base_dictionnary.keys():
            shared.progress(i_count, lenk)

            attribs={'index':i_count.__str__(), 'sentences':(len(self.learning_base_dictionnary[lexicalisation_traitée_key][2].keys())).__str__(), 'triplets':(len(self.learning_base_dictionnary[lexicalisation_traitée_key][0].keys())).__str__()}
            attribs['source'] = self.learning_base_dictionnary[lexicalisation_traitée_key][3]
            if attribs['source'].split("_")[1] == "allSolutions":
                attribs['theme'] = attribs['source'].split("_")[2]
            else:
                attribs['theme'] = attribs['source'].split("_")[1]

            #on traite l'exemple n°i_count
            if attribs['theme'] == "allSolutions":
                print(attribs['source'])
                exit(5)

            noeud_lexicalisation = ElementTree.SubElement(xml_restructured_data, "exemple", attribs)
            #les triplets sont en 0, la phrase d'origine en 1, les données d'analyse en 2

            noeud_encoded = ElementTree.SubElement(noeud_lexicalisation, "encoded")
            noeud_encoded.text = lexicalisation_traitée_key

            noeud_phrase_origine = ElementTree.SubElement(noeud_lexicalisation, "origin")
            noeud_phrase_origine.text = self.learning_base_dictionnary[lexicalisation_traitée_key][1]

            noeud_relations = ElementTree.SubElement(noeud_lexicalisation, "relations")

            #on va récupérer les données de traitements
            tables_inversées_des_index={}
            for i_sentence in self.learning_base_dictionnary[lexicalisation_traitée_key][2].keys():
                tables_inversées_des_index[i_sentence]=self.learning_base_dictionnary[lexicalisation_traitée_key][2][i_sentence]["analyse"]['table_inversée_des_index']
            del self.learning_base_dictionnary[lexicalisation_traitée_key][2][i_sentence]["analyse"]['table_inversée_des_index']
            liste_relations={}
            liste_sujet_objet={}
            for relation_key in self.learning_base_dictionnary[lexicalisation_traitée_key][0].keys():
                relation = self.learning_base_dictionnary[lexicalisation_traitée_key][0][relation_key]
                if relation[1] not in index_relations:
                    index_relations.append(relation[1])

                attribs_relation={"index":index_relations.index(relation[1]).__str__(), "reference":relation[1]}
                liste_relations[relation_key]=ElementTree.SubElement(noeud_relations, "relation", attribs_relation)

                if self.anonymised_dictionnary[relation[0]] in ids_xlirds.keys():
                    id0_recalculée = ids_xlirds[self.anonymised_dictionnary[relation[0]]]
                else:
                    id0_recalculée = None
                if self.anonymised_dictionnary[relation[2]] in ids_xlirds.keys():
                    id2_recalculée = ids_xlirds[self.anonymised_dictionnary[relation[2]]]
                else:
                    id2_recalculée = None

                str0_origine = self.anonymised_dictionnary[relation[0]]
                str2_origine = self.anonymised_dictionnary[relation[2]]

                if id0_recalculée.__str__() in ids_xlirds_vals.keys():
                    text0normalisé = ids_xlirds_vals[id0_recalculée.__str__()][0]
                else:
                    text0normalisé = ""
                if id2_recalculée.__str__() in ids_xlirds_vals.keys():
                    text2normalisé = ids_xlirds_vals[id2_recalculée.__str__()][0]
                else:
                    text2normalisé = ""

                attribs_sujet={"id":relation[0], "reference": str0_origine, "id_calc": id0_recalculée.__str__(), "reference_calc" : text0normalisé}
                attribs_objet={"id":relation[2], "reference": str2_origine, "id_calc": id2_recalculée.__str__(), "reference_calc" : text2normalisé}
                liste_sujet_objet[relation_key]={}
                liste_sujet_objet[relation_key]["sujet"] = ElementTree.SubElement(liste_relations[relation_key], "subject", attribs_sujet)
                liste_sujet_objet[relation_key]["objet"] = ElementTree.SubElement(liste_relations[relation_key], "object", attribs_objet)

            #attribs_contraintes={"count":(len(self.learning_base_dictionnary[lexicalisation_traitée_key][2][i_sentence]["analyse"]["restructured_constraints"])).__str__()}
            noeud_contraintes = ElementTree.SubElement(noeud_lexicalisation, "constraints")

            noeud_tokens = ElementTree.SubElement(noeud_lexicalisation, "tokens")
            i_token = 0

            #phrase length nous servira de référence pour recalculer les index
            phrases_length=[0]
            attribs_tokens={'begin':"1", 'end':"1"}

            constraints_count_strict=0
            constraints_count=0
            attribs_contraintes={'uniq':'0', 'count':'0'}
            diff_ref = {}
            for sentence_key in sorted(self.learning_base_dictionnary[lexicalisation_traitée_key][2].keys()):
                analyse = self.learning_base_dictionnary[lexicalisation_traitée_key][2][sentence_key]["analyse"]
                current_count=1

                #on s'occupe des tokens
                while current_count <= len(analyse["restructured_tokens"]):
                    token=analyse["restructured_tokens"][current_count.__str__()]
                    attribs_token={}
                    attribs_token["index"]=(current_count+i_token).__str__()
                    attribs_tokens['end']=(current_count+i_token).__str__()
                    output_id = -1
                    if "valeur" in token.keys():
                        #nous avons là un subject ou object
                        texte_token = " ".join(token["text"])#
                        if sentence_key not in diff_ref.keys():
                            diff_ref[sentence_key]={}
                        if attribs_token["index"] not in diff_ref[sentence_key].keys():
                            diff_ref[sentence_key][attribs_token["index"]]=texte_token
                        diff_ref[sentence_key][attribs_token["index"]]=texte_token
                        attribs_token["reference"] = self.anonymised_dictionnary[token["valeur"]]
                        attribs_token["id_so"] = token["valeur"]
                        attribs_token["pos"] = 'ENT'

                        val_identifiante=texte_token
                        #si le mot est différent (pronominalisation par ex.) on va attribuer un identifiant proche mais différent.

                        val = [texte_token, attribs_token['reference']]
                        # optimisations et limitations du nombre de calcul
                        if not val[0].isdigit() or not val[1].isdigit():#deux nombre on va les comparer tel quel, sinon on compare sans chiffre
                            if val[0].isdigit() == val[1].isdigit():
                                val[0] = val[0].strip("\"0129456789.").strip()
                                val[1] = val[1].strip("\"0129456789.").strip()
                                if len(val[0]) <=2  or len(val[1]) <=2 :
                                    val = [texte_token, attribs_token['reference']]

                        minuscule1 = False
                        minuscule2 = False
                        majuscule1 = False
                        majuscule2 = False

                        if val[0].lower() != val[0]:
                            majuscule1 = True
                            "La variable contient des majuscules"
                        elif val[0].upper() != val[0]:
                            minuscule1 = True #et des minuscules

                        if val[1].lower() != val[1]:
                            majuscule2 = True
                            "La variable contient des majuscules"
                        elif val[1].upper() != val[1]:
                            minuscule2 = True #et des minuscules

                        dist = 0
                        if (val[0].endswith('s') and val[1].endswith('s')) or (not val[0].endswith('s') and not val[1].endswith('s')):
                            if (minuscule1 == minuscule2 and majuscule2 == majuscule1) :
                                dist = shared.similar(val[0], val[1])
                            #sinon
                                #on est pas dans la même catégorie a priori

                        if dist>0.9 and val[0]:
                            if dist > 0.9 and len(val[0])>3:
                                val_identifiante = attribs_token['reference']
                            else:
                                #on ajoutera un nombre compris entre 0 et 9
                                val_identifiante = attribs_token['reference']+(round(dist*10)).__str__()
                            if dist<0.7:
                                texte_token = " ".join(token["text"])
                                if texte_token in index_mots: #si c'est un mot courant au deuxième passage il est dans index_mots
                                    output_id=(index_mots.index(texte_token) + 50).__str__()
                        else:
                            val_identifiante = texte_token #+ "9"  # l'identifiant de texte_token sera au plus proche des kifkif
                        if val_identifiante not in index_mots:
                            index_mots.append(val_identifiante)
                        identifiant_mot=index_mots.index(val_identifiante) + 50
                    else:
                        texte_token = " ".join(token["text"])
                        if texte_token not in index_mots:
                            index_mots.append(texte_token)
                        identifiant_mot=index_mots.index(texte_token) + 50
                        attribs_token["pos"] = token["pos"]
                        attribs_token["lemma"]  = token["lemma"]

                    attribs_token["word_id"] = identifiant_mot.__str__()
                    if output_id == -1:
                        output_id=identifiant_mot.__str__()

                    attribs_token["word_id"] = identifiant_mot.__str__()
                    attribs_token["output_id"] = output_id

                    noeud_token = ElementTree.SubElement(noeud_tokens, "token", attribs_token)
                    noeud_token.text = texte_token

                    current_count+=1

                phrases_length.append(current_count+i_token-1)
                i_token+=current_count-1

                for constraint_key in analyse['restructured_constraints'].keys():

                    constraint = analyse["restructured_constraints"][constraint_key]

                    attribs_contrainte = {"name":constraint_key}
                    attribs_contraintes['uniq'] = (int(attribs_contraintes['uniq'])+1).__str__()
                    noeud_contrainte = ElementTree.SubElement(noeud_contraintes, "contrainte", attribs_contrainte)
                    i_socle = 0
                    for contrainte_détails in constraint:
                        socle = ElementTree.SubElement(noeud_contrainte, "socle", id=i_socle.__str__())
                        attribs_contraintes['count'] = (int(attribs_contraintes['count'])+1).__str__()
                        i_socle += 1
                        for noeud_key in sorted(contrainte_détails.keys(), key=lambda clé: int(clé[0:clé.index("(")])):
                            index_node_rule=int(noeud_key[0:noeud_key.index("(")])
                            node_socle_attribs = {'index_node_rule':index_node_rule.__str__()}
                            if "(/" not in noeud_key:
                                node_socle_attribs["pos"]=noeud_key[noeud_key.index("(")+1:noeud_key.index("/")]
                            if "/)" not in noeud_key:
                                node_socle_attribs["word"]=noeud_key[noeud_key.index("/")+1:noeud_key.index(")")]
                            if ")_" in noeud_key:
                                node_socle_attribs["rule"]=noeud_key[noeud_key.index(")")+2:]

                            node_socle = ElementTree.SubElement(socle, "node", node_socle_attribs)
                            for token_node_socle in contrainte_détails[noeud_key]:
                                ElementTree.SubElement(node_socle, "token", index=(int(token_node_socle)+phrases_length[sentence_key]).__str__())


                if "triplets_full" in analyse.keys():
                    analyse_triplet = analyse["triplets_full"]
                    for relation_key in analyse_triplet.keys():
                        noeud_relation = liste_relations[relation_key]

                        if "tokens" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens']= "0"

                        if "tokens_object" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens_object']= "0"

                        if "tokens_subject" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens_subject']= "0"

                        noeud_relation.attrib['identification'] = "full"

                        noeud_relation_sujet = liste_sujet_objet[relation_key]["sujet"]
                        noeud_relation_objet = liste_sujet_objet[relation_key]["objet"]

                        analyse_sujet_items = analyse_triplet[relation_key]["subject"][noeud_relation_sujet.attrib["id"]]
                        analyse_object_items = analyse_triplet[relation_key]["object"][noeud_relation_objet.attrib["id"]]

                        for analyse_sujet in analyse_sujet_items:
                            position_dans_sentence = int(tables_inversées_des_index[sentence_key][analyse_sujet['indexFin']])
                            attribs_item = {"index":(position_dans_sentence+ phrases_length[sentence_key]).__str__(),
                                                                                   'identification': analyse_sujet["caracteristique"]}
                            if analyse_sujet["caracteristique"] != "equal":
                                attribs_item['text']= diff_ref[sentence_key][(position_dans_sentence+ phrases_length[sentence_key]).__str__()]

                            ElementTree.SubElement(noeud_relation_sujet, "token", attribs_item)

                            noeud_relation.attrib['tokens'] = (int(noeud_relation.attrib['tokens']) + 1).__str__()
                            noeud_relation.attrib['tokens_subject'] = (int(noeud_relation.attrib['tokens_subject']) + 1).__str__()


                        for analyse_object in analyse_object_items:
                            position_dans_sentence = int(tables_inversées_des_index[sentence_key][analyse_object['indexFin']])
                            attribs_item = {
                                "index": (position_dans_sentence + phrases_length[sentence_key]).__str__(),
                                'identification': analyse_object["caracteristique"]}
                            if analyse_object["caracteristique"] != "equal":
                                attribs_item['text'] = diff_ref[sentence_key][
                                    (position_dans_sentence + phrases_length[sentence_key]).__str__()]

                            ElementTree.SubElement(noeud_relation_objet, "token", attribs_item)
                            noeud_relation.attrib['tokens'] = (int(noeud_relation.attrib['tokens']) + 1).__str__()
                            noeud_relation.attrib['tokens_object'] = (int(noeud_relation.attrib['tokens_object']) + 1).__str__()

                if "triplets_subject" in analyse.keys():
                    analyse_triplet = analyse["triplets_subject"]
                    for relation_key in analyse_triplet.keys():
                        noeud_relation = liste_relations[relation_key]

                        if "tokens" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens']= "0"

                        if "tokens_object" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens_object']= "0"

                        if "tokens_subject" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens_subject']= "0"

                        if 'identification' not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['identification'] = "subject"
                        elif noeud_relation.attrib['identification'] == "object":
                            noeud_relation.attrib['identification'] = "full"

                        noeud_relation_sujet = liste_sujet_objet[relation_key]["sujet"]

                        analyse_sujet_items = analyse_triplet[relation_key]["subject"][noeud_relation_sujet.attrib["id"]]

                        for analyse_sujet in analyse_sujet_items:
                            position_dans_sentence = int(
                                tables_inversées_des_index[sentence_key][analyse_sujet['indexFin']])
                            attribs_item = {"index": (position_dans_sentence + phrases_length[sentence_key]).__str__(),
                                            'identification': analyse_sujet["caracteristique"]}
                            if analyse_sujet["caracteristique"] != "equal":
                                attribs_item['text'] = diff_ref[sentence_key][
                                    (position_dans_sentence + phrases_length[sentence_key]).__str__()]

                            ElementTree.SubElement(noeud_relation_sujet, "token", attribs_item)
                            noeud_relation.attrib['tokens'] = (int(noeud_relation.attrib['tokens']) + 1).__str__()
                            noeud_relation.attrib['tokens_subject'] = (
                            int(noeud_relation.attrib['tokens_subject']) + 1).__str__()

                if "triplets_object" in analyse.keys():
                    analyse_triplet = analyse["triplets_object"]
                    for relation_key in analyse_triplet.keys():
                        noeud_relation = liste_relations[relation_key]

                        if 'identification' not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['identification'] = "object"
                        elif noeud_relation.attrib['identification'] == "subject":
                            noeud_relation.attrib['identification'] = "full"

                        if "tokens" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens']= "0"

                        if "tokens_object" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens_object']= "0"

                        if "tokens_subject" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens_subject']= "0"



                        noeud_relation_objet = liste_sujet_objet[relation_key]["objet"]

                        analyse_object_items = analyse_triplet[relation_key]["object"][noeud_relation_objet.attrib["id"]]
                        for analyse_object in analyse_object_items:
                            position_dans_sentence = int(tables_inversées_des_index[sentence_key][analyse_object['indexFin']])
                            attribs_item = {
                                "index": (position_dans_sentence + phrases_length[sentence_key]).__str__(),
                                'identification': analyse_object["caracteristique"]}
                            if analyse_object["caracteristique"] != "equal":
                                attribs_item['text'] = diff_ref[sentence_key][
                                    (position_dans_sentence + phrases_length[sentence_key]).__str__()]
                            ElementTree.SubElement(noeud_relation_objet, "token", attribs_item)
                            noeud_relation.attrib['tokens'] = (int(noeud_relation.attrib['tokens']) + 1).__str__()
                            noeud_relation.attrib['tokens_object'] = (int(noeud_relation.attrib['tokens_object']) + 1).__str__()



            i_count+=1
            noeud_tokens.attrib=attribs_tokens
            noeud_contraintes.attrib=attribs_contraintes

        shared.progress(lenk, lenk)
        print()

        print("On sauvegarde")
        try:
            fjson = open(JSON_LEARNING_REFS, "w")
            fjson.write(json.dumps(self.learning_base_dictionnary,  indent=2,ensure_ascii=False))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
        except KeyboardInterrupt:
            if not fjson.closed:
                fjson.close()
                while not fjson.closed:
                    True

        print("sauvegarde effectuée base de référence")
        print((len(self.learning_base_dictionnary.keys())).__str__() + " entrées")
        print()
        del self.learning_base_dictionnary

        from bs4 import BeautifulSoup
        bs = BeautifulSoup(ElementTree.tostring(xml_restructured_data, method="xml"), 'xml')
        text_re = re.compile('>\n\s+([^<>\s].*?)\n\s+</', re.DOTALL)
        prettyXml = text_re.sub('>\g<1></', bs.prettify())

        f = open(XML_WORKING_DATA_FILE, 'w')

        f.write(bs.prettify())
        f.close()

        del  xml_restructured_data
        #nos index de mots:


        sav_mots=[index_mots.pop(-1)]
        print("Construction des index de mots rapprochés, utilisé par similarite.py à lancer après un premier tour ici. Relancer webnlgparser.py ensuite")
        shared.init_progress()
        i_count=0
        for yop in index_mots:
            shared.progress(i_count, len(index_mots))
            i_count+=1
            save=0
            sav_mot=''
            found=False
            if not len(sav_mots)==len(index_mots)-1:
                for mot in sav_mots:
                    val = [yop, mot]
                    # optimisations et limitations du nombre de calcul
                    if not val[0].isdigit() or not val[1].isdigit():
                        if val[0].isdigit() == val[1].isdigit():
                            val[0] = val[0].strip("\"0129456789.").strip()
                            val[1] = val[1].strip("\"0129456789.").strip()
                        if len(val[0]) <=2  or len(val[1]) <=2 :
                            val = [yop, mot]

                    dist = shared.similar(val[0], val[1])
                    if dist==1:
                        sav_mots.insert(sav_mots.index(mot), yop)
                        found = True
                        break
                    elif dist>save or sav_mot=='':
                        save=dist
                        sav_mot=mot
                if not found:
                    if sav_mot.isdigit() and yop.isdigit():
                        if float(sav_mot)<float(yop):
                            sav_mots.insert(sav_mots.index(sav_mot)+1, yop)
                        else:
                            sav_mots.insert(sav_mots.index(sav_mot), yop)
                    else:
                        k=sorted([sav_mot, yop])
                        if k[0]==sav_mot:
                            sav_mots.insert(sav_mots.index(sav_mot) + 1, yop)
                        else:
                            sav_mots.insert(sav_mots.index(sav_mot), yop)

        shared.progress(len(index_mots), len(index_mots))
        # on sauvegarde
        print("Sauvegarde mots")
        fjson = open(JSON_INDEX_MOTS, "w")
        fjson.write(json.dumps(sav_mots, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        print("Terminé")

#struture test
if __name__ == '__main__':
    tree=Tree()
import sys
import shared
from xml.etree import ElementTree
import pygraphviz as pgv
import json
import re
import shutil, os
import math

VERSION = 0.99997
print(VERSION)
debug = False

print("debug " + debug.__str__())

REP_WEBNLG = shared.REP_WEBNLG
DATA="webnlgparser_data/"
JSON_LEARNING_BASE = DATA+"logs/learning_base.json"
JSON_LEARNING_REFS = DATA+"logs/learning_refs.json"
JSON_CONSTRAINTS_DIR = DATA + "detailled_informations/" #+constraint+".json"

JSON_INDEX_MOTS = DATA + "index_mots.json"

XML_WORKING_DATA_FILE = DATA+"working_data.xml"

ERRORS_DIR = DATA + "logs/"

data_errors = {}
base_verbale=r = re.compile("V.*")
data_per_types={}

#nouveaux identifiants pour les xlidxxxxlrid:

if os.path.isfile('similarite_data/identifiants.json'):
    file = open('similarite_data/identifiants.json')
    ids_xlirds = json.load(file)
    file.close()
else:
    ids_xlirds={}

if os.path.isfile('similarite_data/identifiants.json'):
    file = open('similarite_data/logs/id--valeurs.json')
    ids_xlirds_vals = json.load(file)
    file.close()
else:
    ids_xlirds_vals = {}


class Tree(object):

    def manageError(self,  exception, sentence, analyzed_sentence):

        if str(type(exception)) not in data_errors.keys():
            data_errors[str(type(exception))] = []
        data_errors[str(type(exception))].append(sentence.text)
        data_errors[str(type(exception))].append(analyzed_sentence.text)

        del self.learning_base_dictionnary[sentence.text]
        return  False

    #initialisation et chargement des données
    def __init__(self):
        self.anonymised_dictionnary={}
        self.learning_base_dictionnary={}

        self.learning_base={}
        self.tree = ElementTree.parse(shared.WORKING_DATA_DIR + "output.xml")
        print("arbre chargé")
        self.make_enhanced_système()
        self.make_basic_système()
        self.parse()
        print("\nparsing effectué")
        self.save()

    def make_enhanced_système(self):
        import rules
        self.enhanced_system=rules.System()
        #self.alternative_system=rules.System()
        # add link(départflèche, arriveflèche, étiquetteflèche)
        # on va créer nos règles


        # "Alfred Garth Jones died in London which is lead by the European Parliament."
        # "Aaron Bertram, who plays Ska punk, is an artist with the band Kids Imagine Nation."
        # pas de détection de la relative sujet dans le premier cas.
        # étant donné que je n'ai que des phrases affirmatives...
        b_phrases_affirmatives = True

        if b_phrases_affirmatives:
            rule_relativeSubject = self.enhanced_system.create_rule("relative_subject")
            x = rule_relativeSubject.add_node(pos="WP", inside_allow=False)
            y = rule_relativeSubject.add_node(pos="V.*", inside_allow=False)
            z = rule_relativeSubject.add_node()
            l1 = rule_relativeSubject.forbid_link(y, x, 'nsubj.*', "")
            l1 = rule_relativeSubject.add_link(z, x, 'nsubj.*')
            l1 = rule_relativeSubject.add_link(x, z, 'acl:relcl')
            l2 = rule_relativeSubject.add_link(x, y, 'ref')
            rule_relativeSubject.set_position(x, y)
            rule_relativeSubject.set_position(y, z)

            rule_relativeSubject = self.enhanced_system.create_rule("relative_subject_2")
            x = rule_relativeSubject.add_node(pos="W.*", inside_allow=False)
            y = rule_relativeSubject.add_node(pos="")
            z = rule_relativeSubject.add_node(pos="")

            rule_relativeSubject.add_link(z, y, "nsubj.*")
            rule_relativeSubject.add_link(y, z, "acl:relcl")
            rule_relativeSubject.add_link(y, x, "ref")

            rule_relativeSubject.set_position(y, x)
            rule_relativeSubject.set_position(x, z)



        else:

            #relative subject
            rule_relativeSubject = rules.System.Rule(name="relative_subject")
            #soit x un noeud pos_taggué V.*
            x = rule_relativeSubject.add_node()
            #soit y un noeud quelconque
            y = rule_relativeSubject.add_node()
            #soit l1 le lien unissant y à x, de nature 'acl:relcl' : y est uni à x pour une relation de type relative clause
            #y a une relative clause dont l'évènement est x
            l1 = rule_relativeSubject.add_link(y, x, 'acl:relcl')
            #soit l2 le lien unissant x à y, de nature 'nsubj' : x a pour sujet y
            #x a pour sujet y
            l2 = rule_relativeSubject.add_link( x, y, 'nsubj.*')
            #et on ajoute la règle
            self.enhanced_system.add_rule(rule_relativeSubject)


        #relative adverbs
        rule_relativeAdverbs = rules.System.Rule(name="relative_adverb")
        #soit x un noeud pos_taggué *
        x = rule_relativeAdverbs.add_node()
        #soit y un noeud quelconque
        y = rule_relativeAdverbs.add_node(pos='WRB')
        #soit l1 le lien unissant faisant x modifié par y, de type advmod
        l1 = rule_relativeAdverbs.add_link(x, y, 'advmod')

        #soit z un noeud quelconque
        #z un nœud qui sera dans une relation de relcl avec ce que l'adverbe modifie
        z = rule_relativeAdverbs.add_node()
        # soit l2 le lien unissant z à x, de nature 'acl:relcl'
        l2 = rule_relativeAdverbs.add_link(z, x, 'acl:relcl')

        #sachant que z n'est pas le sujet de x
        l3 = rule_relativeAdverbs.forbid_link(x, z, 'nsubj.*', "")

        self.enhanced_system.add_rule(rule_relativeAdverbs)


        rule_coord = rules.System.Rule(name="coordinated_full_clauses_1")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='')
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 et s2 deux sujets
        s1 = rule_coord.add_node()
        s2 = rule_coord.add_node()
        rule_coord.add_link(v1, s1, "nsubj.*")
        rule_coord.add_link(v2, s2, "nsubj.*")
        rule_coord.forbid_link(v1, v2, "advcl", "")

        #il existe un nœud quelconque k
        k=rule_coord.add_node()
        #ce nœud est à droite du sujet 2
        rule_coord.set_position(s2, k)
        #il n'a pas le lien ref avec le sujet de v2
        #si c'était le cas, nous aurions une relative sujet
        rule_coord.forbid_link(s2, k, "ref", "")#je ne peux pas dire qu'il est à gauche de v2
        rule_coord.forbid_link(s2, v2, "acl:relcl", "")#je ne peux pas dire qu'il est à gauche de v2, je vais bloquer l'acl:relcl


        #soit c une coordination
        c = rule_coord.add_node(pos="CC", inside_allow=False)
        rule_coord.set_position(v1, c)
        rule_coord.set_position(s1, c)
        rule_coord.set_position(c, v2)
        rule_coord.set_position(c, s2)
        x = rule_coord.add_node()
        rule_coord.forbid_link(x, s2, "cop", "")  # on exclut les cop [x -> verbe être]
                                                #gestion deux temps

        #j'ajoute la règle
        self.enhanced_system.add_rule(rule_coord)

        #on s'occupe des "is"
        rule_coord = rules.System.Rule(name="coordinated_full_clauses_2")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='')
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 le sujet du verbe 1
        s1 = rule_coord.add_node()
        #soit s2 le sujet du verbe 2
        #soit x ce qui est
        x = rule_coord.add_node()
        rule_coord.add_link(v1, s1, "nsubj.*")
        #on relie x à v2 par un lien cop
        rule_coord.add_link(x, v2, "cop")
        #est relié à v1 dans le cadre de la coordination verbale
        rule_coord.add_link(v1, x, "conj.*")
        #et ce qui est cop est sujet
        sujet= rule_coord.add_node()
        rule_coord.add_link(x, sujet, ".subj.*")


        #soit c une coordination
        c = rule_coord.add_node(pos="CC", inside_allow=False)
        rule_coord.set_position(v1, c)
        rule_coord.set_position(s1, c)
        rule_coord.set_position(c, sujet)
        rule_coord.set_position(c, x)
        rule_coord.set_position(c, v2)
        #j'ajoute la règle
        self.enhanced_system.add_rule(rule_coord)

        rule_coord = rules.System.Rule(name="coordinated_clauses_1")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 et s2 deux sujets
        s1 = rule_coord.add_node()
        s2 = rule_coord.add_node()
        rule_coord.add_link(v1, s1, "nsubj.*")
        rule_coord.forbid_link(v2, s2, "nsubj.*", "")
        x = rule_coord.add_node()
        rule_coord.forbid_link(x, s2, "cop", "")#on exclut les cop [x -> verbe être] aboutissant à ce que être n'ait pas de sujet.
        rule_coord.forbid_link(x, v1, "csubj.*", "")#on exclut les cop [x -> verbe être] aboutissant à ce que être n'ait pas de sujet.
        x2 = rule_coord.add_node()


        #soit c une coordination
        c = rule_coord.add_node(pos="CC", inside_allow=False)
        rule_coord.set_position(v1, c)
        rule_coord.set_position(s1, c)
        rule_coord.set_position(s2, c)#s2 aussi est après cc
        rule_coord.set_position(c, v2)
        rule_coord.set_position(c, x2)
        rule_coord.set_position(c, s2)
        rule_coord.forbid_link(s1, x2, "acl:relcl", "")
        #j'ajoute la règle
        self.enhanced_system.add_rule(rule_coord)

        rule_coord = rules.System.Rule(name="coordinated_clauses_2")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 le sujet commun
        s1 = rule_coord.add_node()
        s2 = rule_coord.add_node()
        rule_coord.add_link(v1, s1, "nsubj.*")
        rule_coord.add_link(v2, s1, "nsubj.*")

        #soit c une coordination
        c = rule_coord.add_node(pos="CC", inside_allow=False)
        rule_coord.set_position(v1, c)
        rule_coord.set_position(s1, c)
        rule_coord.set_position(c, v2)
        #j'ajoute la règle
        self.enhanced_system.add_rule(rule_coord)

        #on s'occupe des "is"
        rule_coord = rules.System.Rule(name="coordinated_clauses_3")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 le sujet commun
        s1 = rule_coord.add_node()
        #soit x ce qui est
        x = rule_coord.add_node()
        rule_coord.add_link(v1, s1, "nsubj.*")
        #on relie x à v2 par un lien cop
        rule_coord.add_link(x, v2, "cop")
        #est relié à v1 dans le cadre de la coordination verbale
        rule_coord.add_link(v1, x, "conj.*")
        #et ce qui est cop n'est pas sujet
        #pas sujet
        nonsuj= rule_coord.add_node()
        rule_coord.forbid_link(x, nonsuj, ".subj.*", "")


        #on s'occupe des "is"
        rule_coord = rules.System.Rule(name="coordinated_clauses_4")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 le sujet commun
        s1 = rule_coord.add_node()
        #soit x ce qui est
        x1 = rule_coord.add_node()
        rule_coord.add_link(x1, s1, "nsubj.*")
        rule_coord.add_link(v2, s1, "nsubj.*")


        #on relie x à v1 par un lien cop
        rule_coord.add_link(x1, v1, "cop")
        #est relié à v2 dans le cadre de la coordination verbale
        rule_coord.add_link(v2, x1, "conj.*")



        #soit c une coordination
        c = rule_coord.add_node(pos="CC", inside_allow=False)
        rule_coord.set_position(x1, c)
        rule_coord.set_position(v1, c)
        rule_coord.set_position(s1, c)
        rule_coord.set_position(c, v2)
        #j'ajoute la règle
        self.enhanced_system.add_rule(rule_coord)

        #on s'occupe des "is"
        rule_coord = rules.System.Rule(name="coordinated_clauses_5")
        #soit x et y deux verbes
        v1 = rule_coord.add_node(pos='V.*', inside_allow=False)
        v2 = rule_coord.add_node(pos='V.*', inside_allow=False)
        #soit s1 le sujet
        s1 = rule_coord.add_node()
        #soit x1 et x2 ce qui est
        x1 = rule_coord.add_node()
        x2 = rule_coord.add_node()
        #on relie x à v1 par un lien cop
        rule_coord.add_link(x1, v1, "cop")
        #et x1 est relié au sujet par nsubj
        rule_coord.add_link(x1, s1, "nsubj.*")
        #il n'y a qu'un seul sujet référencé dans ce cas.

        #on relie x2 à v2 par un lien cop
        rule_coord.add_link(x2, v2, "cop")
        x3 = rule_coord.add_node()

        rule_coord.forbid_link(x2, x3, "nsubj.*", "")

        #et un lien conj.* lie x1 et x2
        rule_coord.add_link(x1, x2, "conj.*")

        #par ailleurs nous avons une coordination au milieu

        #soit c une coordination
        c = rule_coord.add_node(pos="CC", inside_allow=False)
        rule_coord.set_position(x1, c)
        rule_coord.set_position(v1, c)
        rule_coord.set_position(s1, c)
        rule_coord.set_position(c, v2)
        rule_coord.set_position(c, x2)
        rule_coord.set_position(c, x3)#et non, il n'y a pas de sujet pour ce cop !s
        #j'ajoute la règle
        self.enhanced_system.add_rule(rule_coord)



        rule_passive = rules.System.Rule(name="passive_voice")
        #soit deux nœuds quelconques
        x = rule_passive.add_node()
        y = rule_passive.add_node()
        #il existe un lien .subjpass unissant deux noeuds
        rule_passive.add_link( x, y, '.subjpass.*')
        #ajout de la règle au système
        self.enhanced_system.add_rule(rule_passive)

        #alors visiblement cela détecte très très mal les clausal subject. Une horreur.
        #CF English language is spoken in Great Britain and a Severed Wasp was written in this language.
        #That his theory was flawed soon became obvious.
        #What she said makes sense.
        clausal_works=False
        if clausal_works:
            rule_cs = rules.System.Rule(name="clausal_subject")
            #soit deux nœuds quelconques
            x = rule_cs.add_node('V.*', inside_allow=False)
            y = rule_cs.add_node()
            #il existe un lien csubj.* unissant deux noeuds
            rule_cs.add_link(x, y, 'csubj.*')
            #ajout de la règle au système
            self.enhanced_system.add_rule(rule_cs)
        else:

            rule_cs = rules.System.Rule(name="clausal_subject")
            #soit verbe_maitre qui aura un sujet
            verbe_maitre =rule_cs.add_node('VB.*', inside_allow=False)
            #le verbe de la clause
            verbe_clause =rule_cs.add_node('VB.*', inside_allow=False)
            #nous sommes dans des phrases simples
            rule_cs.set_position(verbe_clause, verbe_maitre)
        if clausal_works:
            #le verbe maître sera dépourvu e sujet
            x = rule_cs.add_node()
            rule_cs.forbid_link(verbe_maitre, x, ".subj.*", "")
            #et il n'est pas le verbe être
            rule_cs.forbid_link(x, verbe_maitre, "cop", "")
            rule_cs.add_link(verbe_clause, verbe_maitre, "ccomp")
            #cette règle vise à éviter l'intégration des relatives type
            #The ground of A.S. Gubbio 1910 is located in Italy, where the people who live there are called Italians.
            #sinon elles correspondraient
            rule_cs.forbid_link(rule_cs.add_node(), verbe_maitre, "acl:relcl", "")

            #ajout de la règle au système
            self.enhanced_system.add_rule(rule_cs)

            #nous pourrions aussi avoir le verbe être. c'est détectable avec clausal subject, plutôt avec des what kjiso wants is ksdj
            rule_cs = rules.System.Rule(name="clausal_subject_3")
            #soit verbe_maitre qui aura un sujet
            verbe_maitre =rule_cs.add_node('VB.*', inside_allow=False)
            #le verbe de la clause
            verbe_clause =rule_cs.add_node('VB.*', inside_allow=False)
            #nous sommes dans des phrases simples
            rule_cs.set_position(verbe_clause, verbe_maitre)

            #le verbe maître sera dépourvu e sujet
            x = rule_cs.add_node()
            #et il est le verbe être
            rule_cs.add_link(x, verbe_maitre, "cop")

            rule_cs.add_link(x, verbe_clause, "csubj*")

            #ajout de la règle au système
            self.enhanced_system.add_rule(rule_cs)

            #nous pourrions aussi avoir le verbe être. c'est détectable avec un ccomp quand pré-fixé

            rule_cs = rules.System.Rule(name="clausal_subject_2")
            #soit verbe_maitre qui aura un sujet
            verbe_maitre =rule_cs.add_node('VB.*', inside_allow=False)
            #le verbe de la clause
            verbe_clause =rule_cs.add_node('VB.*', inside_allow=False)
            #nous sommes dans des phrases simples
            rule_cs.set_position(verbe_clause, verbe_maitre)

            #le verbe maître sera dépourvu e sujet
            x = rule_cs.add_node()
            #et il est le verbe être
            rule_cs.add_link(x, verbe_maitre, "cop")
            rule_cs.forbid_link(x, rule_cs.add_node(), "nsubj.*", "")

            rule_cs.add_link(verbe_clause, x, "ccomp")

            #ajout de la règle au système
            self.enhanced_system.add_rule(rule_cs)



        rule_appos = rules.System.Rule(name="apposition")
        #soit deux nœuds quelconques
        x = rule_appos.add_node()
        y = rule_appos.add_node()
        #il existe un lien csubj.* unissant deux noeuds
        rule_appos.add_link( x, y, 'appos')
        #ajout de la règle au système
        self.enhanced_system.add_rule(rule_appos)

        # case_rule = rules.System.Rule(name="possessif")
        # #soit deux nœuds quelconques
        # x = case_rule.add_node()
        # y = case_rule.add_node()
        # #il existe un lien csubj.* unissant deux noeuds
        # case_rule.add_link( x, y, 'case')
        # #ajout de la règle au système
        # self.enhanced_system.add_rule(case_rule)

        case_rule = rules.System.Rule(name="possessif")
        #soit deux nœuds quelconques
        x = case_rule.add_node()
        y = case_rule.add_node()
        #il existe un lien csubj.* unissant deux noeuds
        case_rule.add_link( x, y, 'nmod:poss')
        #ajout de la règle au système
        self.enhanced_system.add_rule(case_rule)



        #
        #coordonnées
        rule_juxtaClauses = rules.System.Rule(name="juxtaposition")
        n1 = rule_juxtaClauses.add_node()
        # un point final
        n2 = rule_juxtaClauses.add_node()

        sujet1 = rule_juxtaClauses.add_node()
        sujet2 = rule_juxtaClauses.add_node()
        #on veut que ces noeuds aient des sujets
        rule_juxtaClauses.add_link(n1, sujet1, 'nsubj.*')
        rule_juxtaClauses.add_link(n2, sujet2, 'nsubj.*')

        rule_juxtaClauses.add_link(n1, n2, 'parataxis')
        if False:
            #soit x et y deux verbes
            verbe1 = rule_juxtaClauses.add_node(pos='V.*', word=".*(?!:ing)$", inside_allow=False)
            verbe2 = rule_juxtaClauses.add_node(pos='V.*', word=".*(?!:ing)$", inside_allow=False)
            # une virgule
            virgule = rule_juxtaClauses.add_node(word=',')
            # un point final
            point = rule_juxtaClauses.add_node(word='.')
            #un wrb potentiel
            wrbpos = rule_juxtaClauses.add_node(inside_allow=False)

            #on interdit un lien typé relative clause sur le dernier verbe avec un object entre les deux verbes.
            rule_juxtaClauses.forbid_link(verbe2, wrbpos, "acl:relcl", "")
            rule_juxtaClauses.set_position(verbe1, wrbpos)
            rule_juxtaClauses.set_position(wrbpos, verbe2)
            #et la seconde clause n'est pas une clause adverbiale de la première
            rule_juxtaClauses.forbid_link(verbe1, verbe2, "a.*cl.*", "")

            #deux sujets
            sujet1= rule_juxtaClauses.add_node(pos="^(?!W).*$")
            sujet2= rule_juxtaClauses.add_node(pos="^(?!W).*$")
            #le sujet 2 n'est pas lié à un noeaud quelconque par une relation advmod si ce noeud est de type "WRB"

            noeud_quelconque = rule_juxtaClauses.add_node()
            rule_juxtaClauses.forbid_link(verbe2, noeud_quelconque, "advmod", "WRB")

            rule_juxtaClauses.add_link(verbe1, sujet1, '.subj.*')
            rule_juxtaClauses.add_link(verbe2, sujet2, '.subj.*')
            rule_juxtaClauses.forbid_link(sujet2, noeud_quelconque, "ref", "WRB")

            #verbe1 est relié à deux puncts --> clôture virgule et clôture point final
            rule_juxtaClauses.add_link(verbe1, virgule, "punct")
            rule_juxtaClauses.add_link(verbe1, point, "punct")

            rule_juxtaClauses.set_position(verbe1, virgule)
            rule_juxtaClauses.set_position(sujet1, virgule)
            rule_juxtaClauses.set_position(virgule, verbe2)
            rule_juxtaClauses.set_position(virgule, sujet2)
            rule_juxtaClauses.set_position(sujet1, sujet2)
            rule_juxtaClauses.set_position(verbe1, verbe2)


        self.enhanced_system.add_rule(rule_juxtaClauses)


        objectDirect = rules.System.Rule(name="direct_object")
        #soit x et y deux verbes
        x = objectDirect.add_node(pos='V.*', inside_allow=False)
        y = objectDirect.add_node(pos='')

        objectDirect.add_link(x, y, 'dobj')

        #j'ajoute la règle
        self.enhanced_system.add_rule(objectDirect)

        objectInDirect = rules.System.Rule(name="indirect_object")
        #soit x et y deux verbes
        x = objectInDirect.add_node(pos='V.*', inside_allow=False)
        y = objectInDirect.add_node(pos='')

        objectInDirect.add_link(x, y, 'iobj')

        #j'ajoute la règle
        self.enhanced_system.add_rule(objectInDirect)

        #two objects


        twiceRule = rules.System.Rule(name="twice_objects")
        #soit x et y deux verbes
        x = twiceRule.add_node(pos='V.*', inside_allow=False)
        y = twiceRule.add_node(pos='')
        z = twiceRule.add_node(pos='')

        twiceRule.add_link(x, y, 'iobj')
        twiceRule.add_link(x, z, 'dobj')

        #j'ajoute la règle
        self.enhanced_system.add_rule(twiceRule)

        existencielle = rules.System.Rule(name="existential")
        #soit x et y deux verbes
        y = existencielle.add_node(pos='')
        z = existencielle.add_node(pos='V.*', inside_allow=False)

        existencielle.add_link(y, z, 'cop')

        #j'ajoute la règle
        self.enhanced_system.add_rule(existencielle)

        ptc = rules.System.Rule(name="participle clause")
        # soit x, y et z
        x = ptc.add_node(pos='V.*')
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBG')

        ptc.add_link(y, z, 'auxpass')
        ptc.add_link(x, y, 'advcl.*')
        ptc.forbid_link(y, ptc.add_node(), 'nsubj.*')
        self.enhanced_system.add_rule(ptc)

        ptc = rules.System.Rule(name="participle clause_bis_1")
        #soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBG')

        ptc.add_link(y, z, 'advcl')
        self.enhanced_system.add_rule(ptc)

        ptc = rules.System.Rule(name="participle clause_ter_1")
        #soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBN')

        ptc.add_link(y, z, 'advcl')
        ptc.forbid_link(ptc.add_node(), y, 'acl:relcl')
        self.enhanced_system.add_rule(ptc)

        ptc = rules.System.Rule(name="participle clause_bis_2")
        #soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBG')
        x = ptc.add_node()

        ptc.add_link(x, y, 'cop')
        ptc.add_link(x, z, 'advcl')
        ptc.forbid_link(ptc.add_node(), y, 'acl:relcl')
        self.enhanced_system.add_rule(ptc)

        ptc = rules.System.Rule(name="participle clause_3")
        #soit x, y
        x = ptc.add_node()
        y = ptc.add_node(pos='VBG')

        ptc.add_link(x, y, 'acl.*')
        ptc.forbid_link(y, ptc.add_node(), 'nsubj.*')
        self.enhanced_system.add_rule(ptc)

        ptc = rules.System.Rule(name="participle clause_3_ter_2")
        #soit x, y
        x = ptc.add_node()
        y = ptc.add_node(pos='VBN')
        z = ptc.add_node()

        ptc.add_link(x, y, 'amod')
        ptc.add_link(z, x, 'dep')

        self.enhanced_system.add_rule(ptc)


        ptc = rules.System.Rule(name="participle clause_subject_2_1")
        #soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBG')

        ptc.add_link(z, ptc.add_node(), 'nmod.*')
        ptc.add_link(y, z, 'csubj.*')
        self.enhanced_system.add_rule(ptc)



        ptc = rules.System.Rule(name="participle clause_subject_3_1")
        #soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBG')
        x = ptc.add_node()

        ptc.add_link(z, x, 'dobj')
        ptc.add_link(y, z, 'csubj.*')
        self.enhanced_system.add_rule(ptc)


        ptc = rules.System.Rule(name="participle clause_subject_2_2")
        #soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBN')

        ptc.add_link(z, ptc.add_node(), 'nmod.*')
        ptc.forbid_link(ptc.add_node(), z, 'acl:relcl')
        ptc.add_link(y, z, 'csubj.*')
        self.enhanced_system.add_rule(ptc)

        ptc = rules.System.Rule(name="participle clause_subject_3_2")
        # soit x, y et z
        y = ptc.add_node(pos='V.*')
        z = ptc.add_node(pos='VBN')

        ptc.add_link(z, ptc.add_node(), 'dobj')
        ptc.forbid_link(ptc.add_node(), z, 'acl:relcl')
        ptc.add_link(y, z, 'csubj.*')
        self.enhanced_system.add_rule(ptc)

    def make_basic_système(self):
        import rules
        self.basic_system=rules.System()
        #relative object
        rule_relativeObject = rules.System.Rule(name="relative_object_1")
        #soit x un noeud quelconque
        x = rule_relativeObject.add_node()
        #soit y un noeud quelconque qui sera un pronom relatif
        pronom_relatif = rule_relativeObject.add_node(pos="WDT")

        what_relative = rule_relativeObject.add_node()
        rule_relativeObject.add_link(x, what_relative, "acl:relcl" )
        rule_relativeObject.add_link(what_relative, pronom_relatif, 'ref')

        self.basic_system.add_rule(rule_relativeObject)

        #relative object
        rule_relativeObject = rules.System.Rule(name="relative_object_2")
        #soit x un noeud quelconque qui sera sujet
        x = rule_relativeObject.add_node()
        #soit y un noeud quelconque qui sera un pronom relatif
        pronom_relatif = rule_relativeObject.add_node(pos="W.*")
        #verbe
        rule_relativeObject.add_link(x, pronom_relatif, 'nmod.*' )

        self.basic_system.add_rule(rule_relativeObject)

        if False:

            #relative object

            # suspendu à cause analyse type : An affiliate of the European University Association, the School of Business and Social Sciences at the Aarhus University in Aarhus, Denmark has 16000 students enrolled.
            # ou encore The 1 Decembrie 1918 University of Alba Iulia, Romania has the nickname Uab and the rector is called \"Breaz Valer Daniel\".


            rule_relativeObject = rules.System.Rule(name="relative_object_3")
            #soit x un noeud quelconque qui sera sujet
            x = rule_relativeObject.add_node()

            verbe_théorique = rule_relativeObject.add_node()
            rule_relativeObject.add_link(verbe_théorique, x, 'nsubj.*' )

            verbe_à_dépendance = rule_relativeObject.add_node()
            rule_relativeObject.add_link(verbe_à_dépendance, verbe_théorique,  'ccomp' )

            self.basic_system.add_rule(rule_relativeObject)


    def parse(self):
        root = self.tree.getroot()
        count=0
        shared.init_progress()

        if debug:
            data_to_parse = root.findall('file')[0:1]
        else:
            data_to_parse = root.findall('file')

        for file in data_to_parse:
            for eid in file.findall('eid'):
                count+=1

        i_step=0
        for file in data_to_parse:
            fichier_origine = file.attrib['name']
            for eid in file.findall('eid'):
                shared.progress(i_step, count)
                i_step+=1
                eidname = eid.attrib['name']
                triples={}
                for tripleset, tripleset_encoded in zip(eid.findall('tripleset/triple'), eid.findall('tripleset_encoded/triple') ):
                    t1_id= tripleset.attrib['tid']
                    t2_id= tripleset_encoded.attrib['tid']
                    if t1_id!=t2_id:
                        print("erreur d'appariement")
                        print("revoir logique")
                        sys.exit(5)
                    triplet = tripleset.text.split(" | ")
                    triplet_encoded = tripleset_encoded.text.split(" | ")
                    triples[t1_id]=triplet_encoded
                    for text, encoded_text in zip([triplet[0], triplet[2]], [triplet_encoded[0], triplet_encoded[2]]) :

                        if encoded_text not in self.anonymised_dictionnary.keys():
                            self.anonymised_dictionnary[encoded_text] = text.replace("_", " ")


                for lex in eid.findall('lexicalisation'):

                    graphes=lex.findall('sentences/sentence/graphe')
                    basic_graphes=lex.findall('sentences/sentence/basic_graphe')
                    alt_graphes=lex.findall('sentences/sentence/grapheOnEncodedString')
                    e_triples = lex.findall('sentences/sentence/ROOT/embeddedTriples')
                    i_sent=0

                    if (len(graphes)>0 ) and (len(graphes) == len(alt_graphes)  or (len(graphes) != len(alt_graphes)  and len(graphes)== lex.find("encoded").text.count("."))):
                        lid = lex.attrib['lid']
                        coref_analyzed = lex.attrib['coref']
                        sentence = lex.find("encoded")
                        self.learning_base_dictionnary[sentence.text]=[triples]
                        analyzed_sentence = lex.find("analysed")
                        self.learning_base_dictionnary[sentence.text].append(analyzed_sentence.text)
                        #print(analyzed_sentence.text)

                        self.learning_base_dictionnary[sentence.text].append({})
                        for graphe, basic_graphe, alt_graphe, embedded_triples in zip(graphes, basic_graphes, alt_graphes,  e_triples):

                            #on étudie la réalisation de la relation : les structures grammaticales à l'intérieur
                            # de notre triplet n'ont pas d'importance
                            # on les exclut
                            internal_relation_triple_so_excluded=[]
                            for embedded_triple in embedded_triples.findall('triple'):
                                internal_relation_triple_so_excluded.append([])
                                for item in embedded_triple.findall('item'):
                                    internal_relation_triple_so_excluded[-1].append(item.attrib['idx'])

                            #constraints = self.alternative_system.apply_rules(alt_graphe.text, internal_relation_triple_so_excluded)
                            #constraints = self.enhanced_system.apply_rules(alt_graphe.text, internal_relation_triple_so_excluded[:])
                            constraints = self.enhanced_system.apply_rules(graphe.text, internal_relation_triple_so_excluded[:])
                            constraints_2 = self.basic_system.apply_rules(basic_graphe.text, internal_relation_triple_so_excluded[:])
                            for constraint_2_name, constraint_2_data in constraints_2.items():
                                if constraint_2_name not in constraints.keys():
                                    constraints[constraint_2_name] = []
                                    for lot in constraint_2_data:
                                        constraints[constraint_2_name].append(lot)

                            self.learning_base_dictionnary[sentence.text][-1][i_sent] = {}
                            self.learning_base_dictionnary[sentence.text][-1][i_sent]["analyse"] = {}
                            self.learning_base_dictionnary[sentence.text][-1][i_sent]["analyse"]["constraints"] = constraints
                            self.learning_base_dictionnary[sentence.text][-1][i_sent]["analyse"]["initial_tokens"] = {}

                            for alpha in constraints:
                                alpha=shared.nettoie(alpha)
                                if alpha not in data_per_types.keys():
                                    data_per_types[alpha]=[]
                                data_per_types[alpha].append((sentence.text, analyzed_sentence.text))

                            i_sent+=1

                        sentences = lex.findall('sentences/sentence')
                        do_it = True

                        for xml_sentence in sentences:
                            if do_it:
                                sid=int(xml_sentence.attrib['sid'])
                                try:
                                    for s_token in xml_sentence.findall('ROOT/rootItems/item'):
                                        attribs = s_token.attrib
                                        self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["initial_tokens"][attribs['idx']] = s_token.text


                                    tokenized_results={}
                                    tokenized_results_final={}
                                    for s_triple in xml_sentence.findall('triplets/triplet'):

                                        triplet = s_triple
                                        a = {}

                                        if s_triple.attrib['partial'] == "full":
                                            if "triplets_full" not in self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"].keys():
                                                self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["triplets_full"] = {}
                                            self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["triplets_full"][triplet.attrib["tid"]] = a
                                            a["subject"] = {triplet.attrib["idSubject"]:[]}
                                            a["object"] = {triplet.attrib["idObject"]:[]}

                                        elif s_triple.attrib['partial'] == "subject":

                                            if "triplets_subject" not in self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"].keys():
                                                self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["triplets_subject"] = {}

                                            a["subject"] = {triplet.attrib["idSubject"]:[]}
                                            self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["triplets_subject"][triplet.attrib["tid"]]  = a

                                        elif s_triple.attrib['partial'] == "object":

                                            if "triplets_object" not in self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"].keys():
                                                self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["triplets_object"] = {}

                                            a["object"] =  {triplet.attrib["idObject"]:[]}
                                            self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["triplets_object"][triplet.attrib["tid"]]  = a

                                        for solution_objet in s_triple.findall('solutions/sujet'):
                                            a["subject"][triplet.attrib["idSubject"]].append(solution_objet.attrib)

                                        for solution_sujet in s_triple.findall('solutions/objet'):
                                            a["object"][triplet.attrib["idObject"]].append(solution_sujet.attrib)

                                        for level in a.keys():
                                            for id_triple_param in a[level].keys():
                                                for refence in a[level][id_triple_param]:
                                                    deb = int(refence["indexDeb"])
                                                    fin = int(refence["indexFin"])
                                                    while deb <= fin:
                                                        tokenized_results[deb.__str__()]=id_triple_param
                                                        deb+=1


                                    table_des_index={}
                                    for s_token in xml_sentence.findall('ROOT/rootItems/item'):
                                        index_initial = s_token.attrib['idx']
                                        table_des_index[int(index_initial)]= s_token

                                    table_inversée_des_index={}
                                    i_count=1
                                    #on renumérote
                                    for s_token_key in sorted(table_des_index.keys()):
                                        s_token = table_des_index[s_token_key]
                                        index_initial = s_token.attrib['idx']

                                        #on en profite pour construire une utile table inversée
                                        table_inversée_des_index[index_initial] = i_count.__str__()

                                        #tokenized_results contient les index des subjects/objects
                                        if i_count.__str__() not in tokenized_results_final.keys():
                                            if index_initial in tokenized_results.keys() :
                                                tokenized_results_final[i_count.__str__()] = {"text":[s_token.text], "valeur": tokenized_results[index_initial], "index":[index_initial]}
                                            else:
                                                tokenized_results_final[i_count.__str__()] = {"text":[s_token.text], "index": [index_initial]}

                                        else:
                                            tokenized_results_final[i_count.__str__()]["text"].append(s_token.text)
                                            tokenized_results_final[i_count.__str__()]["index"].append(index_initial)
                                        tokenized_results_final[i_count.__str__()]['pos'] =  s_token.attrib['pos']
                                        tokenized_results_final[i_count.__str__()]['lemma'] =  s_token.attrib['lemma']

                                        #si i_count est une entité object/subjet on n'avance pas: je reste sur le même index
                                        if (s_token_key + 1).__str__() not in tokenized_results.keys() or s_token_key.__str__() not in tokenized_results.keys():

                                            i_count += 1
                                        elif tokenized_results[(s_token_key + 1).__str__()] != tokenized_results[index_initial]:
                                            i_count += 1

                                    self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["restructured_tokens"] = tokenized_results_final
                                    self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["restructured_constraints"] = {}
                                    self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["table_inversée_des_index"]=table_inversée_des_index


                                    for constraint in self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["constraints"].keys():
                                        simple_contrainte=shared.nettoie(constraint)
                                        #ex: contrainte=direct objet
                                        self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["restructured_constraints"][simple_contrainte]=[]
                                        #il y a potentiellement plusieurs lots de direct object, par exemple
                                        for lot in self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["constraints"][constraint]:
                                            self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["restructured_constraints"][simple_contrainte].append({})
                                            if len(self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["restructured_constraints"][simple_contrainte]) > 1:
                                                print("")
                                                print("plusieurs lots de contraintes")
                                                print(sentence.text)
                                                print()
                                            for node in lot.keys():
                                                node_name = node+"_"+constraint
                                                tab_contrainte = self.learning_base_dictionnary[sentence.text][-1][sid]["analyse"]["restructured_constraints"][simple_contrainte][-1]
                                                tab_contrainte[node_name]=[]
                                                old_refs = lot[node].split(", ")
                                                for old_ref in old_refs:
                                                    #souci, parfois un noeud ROOT
                                                    if table_inversée_des_index[old_ref] not in tab_contrainte[node_name]:
                                                        tab_contrainte[node_name].append(table_inversée_des_index[old_ref])




                                except KeyError as inst:
                                    print()
                                    print(sentence.text)
                                    print(analyzed_sentence.text)
                                    exc_type, exc_obj, exc_tb = sys.exc_info()
                                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

                                    print(exc_type, fname, exc_tb.tb_lineno)
                                    do_it = self.manageError(inst, sentence, analyzed_sentence)

                                except AttributeError as inst:
                                    print()
                                    print(sentence.text)
                                    print(analyzed_sentence.text)
                                    exc_type, exc_obj, exc_tb = sys.exc_info()
                                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                                    print()

                                    do_it = self.manageError(inst, sentence, analyzed_sentence)
                                except Exception as inst:
                                    print()
                                    print(sentence.text)
                                    print(analyzed_sentence.text)
                                    exc_type, exc_obj, exc_tb = sys.exc_info()
                                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                                    print()
                                    do_it = self.manageError(inst, sentence, analyzed_sentence)



                        if sentence.text in self.learning_base_dictionnary.keys():
                            self.learning_base_dictionnary[sentence.text].append(fichier_origine)
                    else:
                        # print()
                        # print(len(self.learning_base_dictionnary.keys()).__str__()+ " lexicalisations gérées.")
                        # print("Les graphes des lexicalisations suivantes ne font pas la même taille, mauvais parsing.")
                        # print(lex.find("encoded").text)
                        # print(lex.find("analysed").text)
                        # print()
                        if "taille graphe" not in data_errors.keys():
                            data_errors["taille graphe"]=[]
                        data_errors["taille graphe"].append(sentence.text)
                        data_errors["taille graphe"].append(analyzed_sentence.text)
                        # print(analyzed_sentence.text)
                        #if do_it: ---> l'update donne lieu au remplacement du contenu, donc à retenir : ne pas faire
                        #    data_per_types.update(tmp_data_per_types)
        shared.progress(1,1)
        shared.init_progress()

    # def getContenu(selfself, graphe, items_idx):#, items_values):
    #
    #     G = pgv.AGraph(graphe.text)
    #     t_verbes = list(filter(r.match, items_idx.values()))
    #     constrs=[]
    #     if len(t_verbes)>0:
    #         verbes=[]
    #         for a in items_idx.keys():
    #             if items_idx[a] in t_verbes:
    #                 verbes.append(a)
    #         for verbe in verbes:
    #             node_verb=G.get_node(verbe)
    #             for parent in G.predecessors(node_verb):
    #                 if G.get_edge( parent, node_verb).key=="acl:relcl":
    #                     print(graphe.text)
    #                     if G.has_edge(verbe, parent, key='nsubj') or G.has_edge(verbe, parent, key='nsubjpass') :
    #                         print(graphe.text)
    #                         constrs.append('rel_subj')
    #
    #     return constrs

    def save(self):
        try:
            for key in data_errors.keys():
                fjson = open(ERRORS_DIR + key + ".json", "w")
                fjson.write(json.dumps(data_errors[key], indent=2, ensure_ascii=False))
                fjson.flush()
                fjson.close()
                print("erreurs sauvegardées pour "+key+".")
                while not fjson.closed:
                    True
        except KeyboardInterrupt:
            if not fjson.closed:
                fjson.close()
                while not fjson.closed:
                    True

        i_count= 0
        for key in data_errors.keys():
            i_count+=len(data_errors[key])

        print(i_count.__str__() + " entrées")
        print()

        try:
            fjson = open(JSON_LEARNING_BASE, "w")
            fjson.write(json.dumps(self.anonymised_dictionnary, indent=2,ensure_ascii=False))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
        except KeyboardInterrupt:
            if not fjson.closed:
                fjson.close()
                while not fjson.closed:
                    True
        print("learning base (correspondance clés/subject object des triplets) assurée.")

        print((len(self.anonymised_dictionnary.keys())).__str__() + " entrées")
        print()

        shutil.rmtree(JSON_CONSTRAINTS_DIR, ignore_errors=True)
        os.makedirs(JSON_CONSTRAINTS_DIR)
        for constraint in data_per_types.keys():
            try:
                file= JSON_CONSTRAINTS_DIR + constraint+".json"
                fjson = open(file, "w")
                fjson.write(json.dumps(data_per_types[constraint],  indent=2,ensure_ascii=False))
                fjson.flush()
                fjson.close()
                while not fjson.closed:
                    True
            except KeyboardInterrupt:
                if not fjson.closed:
                    fjson.close()
                    while not fjson.closed:
                        True
        print("sauvegarde effectuée des listes log des contraintes repérées")

        print("travail sur les logs")

        #on va d'abord indexer nos identifiants subjects/objects
        index_id_so=[]
        for key in self.anonymised_dictionnary.keys():
            index_id_so.append(key)

        #il va nous falloir un index des relations
        index_relations=[]
        #et un index des mots
        index_mots=[]
        #qu'on a potentiel enregistrés triés par similité
        if os.path.isfile(JSON_INDEX_MOTS) :
            file = open(JSON_INDEX_MOTS)
            index_mots = json.load(file)
            file.close()

        print("Construction du xml")

        shared.init_progress()
        xml_restructured_data = ElementTree.Element("root")
        i_count=0
        lenk = len(self.learning_base_dictionnary.keys())
        for lexicalisation_traitée_key in self.learning_base_dictionnary.keys():
            shared.progress(i_count, lenk)

            attribs={'index':i_count.__str__(), 'sentences':(len(self.learning_base_dictionnary[lexicalisation_traitée_key][2].keys())).__str__(), 'triplets':(len(self.learning_base_dictionnary[lexicalisation_traitée_key][0].keys())).__str__()}

            if self.learning_base_dictionnary[lexicalisation_traitée_key][3].split("_")[1] == "allSolutions":
                attribs['theme'] = self.learning_base_dictionnary[lexicalisation_traitée_key][3].split("_")[2]
            else:
                attribs['theme'] = self.learning_base_dictionnary[lexicalisation_traitée_key][3].split("_")[1]
            attribs['source'] = self.learning_base_dictionnary[lexicalisation_traitée_key][3]

            if attribs['theme'] == "allSolutions":
                print(attribs['source'])
                exit(5)

            # on traite l'exemple n°i_count
            noeud_lexicalisation = ElementTree.SubElement(xml_restructured_data, "exemple", attribs)
            # les triplets sont en 0, la phrase d'origine en 1, les données d'analyse en 2

            noeud_encoded = ElementTree.SubElement(noeud_lexicalisation, "encoded")
            noeud_encoded.text = lexicalisation_traitée_key

            noeud_phrase_origine = ElementTree.SubElement(noeud_lexicalisation, "origin")
            noeud_phrase_origine.text = self.learning_base_dictionnary[lexicalisation_traitée_key][1]

            noeud_relations = ElementTree.SubElement(noeud_lexicalisation, "relations")

            # on va récupérer les données de traitements
            tables_inversées_des_index={}
            for i_sentence in self.learning_base_dictionnary[lexicalisation_traitée_key][2].keys():
                tables_inversées_des_index[i_sentence]=self.learning_base_dictionnary[lexicalisation_traitée_key][2][i_sentence]["analyse"]['table_inversée_des_index']
            del self.learning_base_dictionnary[lexicalisation_traitée_key][2][i_sentence]["analyse"]['table_inversée_des_index']
            liste_relations={}
            liste_sujet_objet={}
            for relation_key in self.learning_base_dictionnary[lexicalisation_traitée_key][0].keys():
                relation = self.learning_base_dictionnary[lexicalisation_traitée_key][0][relation_key]
                if relation[1] not in index_relations:
                    index_relations.append(relation[1])

                attribs_relation={"index":index_relations.index(relation[1]).__str__(), "reference":relation[1]}
                liste_relations[relation_key]=ElementTree.SubElement(noeud_relations, "relation", attribs_relation)

                if self.anonymised_dictionnary[relation[0]] in ids_xlirds.keys():
                    id0_recalculée = ids_xlirds[self.anonymised_dictionnary[relation[0]]]
                else:
                    id0_recalculée = None
                if self.anonymised_dictionnary[relation[2]] in ids_xlirds.keys():
                    id2_recalculée = ids_xlirds[self.anonymised_dictionnary[relation[2]]]
                else:
                    id2_recalculée = None

                str0_origine = self.anonymised_dictionnary[relation[0]]
                str2_origine = self.anonymised_dictionnary[relation[2]]

                if id0_recalculée.__str__() in ids_xlirds_vals.keys():
                    text0normalisé = ids_xlirds_vals[id0_recalculée.__str__()][0]
                else:
                    text0normalisé = ""
                if id2_recalculée.__str__() in ids_xlirds_vals.keys():
                    text2normalisé = ids_xlirds_vals[id2_recalculée.__str__()][0]
                else:
                    text2normalisé = ""

                attribs_sujet={"id":relation[0], "reference": str0_origine, "id_calc": id0_recalculée.__str__(), "reference_calc" : text0normalisé}
                attribs_objet={"id":relation[2], "reference": str2_origine, "id_calc": id2_recalculée.__str__(), "reference_calc" : text2normalisé}
                liste_sujet_objet[relation_key]={}
                liste_sujet_objet[relation_key]["sujet"] = ElementTree.SubElement(liste_relations[relation_key], "subject", attribs_sujet)
                liste_sujet_objet[relation_key]["objet"] = ElementTree.SubElement(liste_relations[relation_key], "object", attribs_objet)

            #attribs_contraintes={"count":(len(self.learning_base_dictionnary[lexicalisation_traitée_key][2][i_sentence]["analyse"]["restructured_constraints"])).__str__()}
            noeud_contraintes = ElementTree.SubElement(noeud_lexicalisation, "constraints")

            noeud_tokens = ElementTree.SubElement(noeud_lexicalisation, "tokens")
            i_token = 0

            #phrase length nous servira de référence pour recalculer les index
            phrases_length=[0]
            attribs_tokens={'begin':"1", 'end':"1"}

            constraints_count_strict=0
            constraints_count=0
            attribs_contraintes={'uniq':'0', 'count':'0'}
            diff_ref = {}
            for sentence_key in sorted(self.learning_base_dictionnary[lexicalisation_traitée_key][2].keys()):
                analyse = self.learning_base_dictionnary[lexicalisation_traitée_key][2][sentence_key]["analyse"]
                current_count=1

                #on s'occupe des tokens
                while current_count <= len(analyse["restructured_tokens"]):
                    token=analyse["restructured_tokens"][current_count.__str__()]
                    attribs_token={}
                    attribs_token["index"]=(current_count+i_token).__str__()
                    attribs_tokens['end']=(current_count+i_token).__str__()
                    output_id = -1
                    if "valeur" in token.keys():
                        #nous avons là un subject ou object
                        texte_token = " ".join(token["text"])#
                        if sentence_key not in diff_ref.keys():
                            diff_ref[sentence_key]={}
                        if attribs_token["index"] not in diff_ref[sentence_key].keys():
                            diff_ref[sentence_key][attribs_token["index"]]=texte_token
                        diff_ref[sentence_key][attribs_token["index"]]=texte_token
                        attribs_token["reference"] = self.anonymised_dictionnary[token["valeur"]]
                        attribs_token["id_so"] = token["valeur"]
                        attribs_token["pos"] = 'ENT'

                        val_identifiante=texte_token
                        #si le mot est différent (pronominalisation par ex.) on va attribuer un identifiant proche mais différent.

                        val = [texte_token, attribs_token['reference']]
                        # optimisations et limitations du nombre de calcul
                        if not val[0].isdigit() or not val[1].isdigit():#deux nombre on va les comparer tel quel, sinon on compare sans chiffre
                            if val[0].isdigit() == val[1].isdigit():
                                val[0] = val[0].strip("\"0129456789.").strip()
                                val[1] = val[1].strip("\"0129456789.").strip()
                                if len(val[0]) <=2  or len(val[1]) <=2 :
                                    val = [texte_token, attribs_token['reference']]

                        minuscule1 = False
                        minuscule2 = False
                        majuscule1 = False
                        majuscule2 = False

                        if val[0].lower() != val[0]:
                            majuscule1 = True
                            "La variable contient des majuscules"
                        elif val[0].upper() != val[0]:
                            minuscule1 = True #et des minuscules

                        if val[1].lower() != val[1]:
                            majuscule2 = True
                            "La variable contient des majuscules"
                        elif val[1].upper() != val[1]:
                            minuscule2 = True #et des minuscules

                        dist = 0
                        if (val[0].endswith('s') and val[1].endswith('s')) or (not val[0].endswith('s') and not val[1].endswith('s')):
                            if (minuscule1 == minuscule2 and majuscule2 == majuscule1) :
                                dist = shared.similar(val[0], val[1])
                            #sinon
                                #on est pas dans la même catégorie a priori

                        if dist>0.9 and val[0]:
                            if dist > 0.9 and len(val[0])>3:
                                val_identifiante = attribs_token['reference']
                            else:
                                #on ajoutera un nombre compris entre 0 et 9
                                val_identifiante = attribs_token['reference']+(round(dist*10)).__str__()
                            if dist<0.7:
                                texte_token = " ".join(token["text"])
                                if texte_token in index_mots: #si c'est un mot courant au deuxième passage il est dans index_mots
                                    output_id=(index_mots.index(texte_token) + 50).__str__()
                        else:
                            val_identifiante = texte_token #+ "9"  # l'identifiant de texte_token sera au plus proche des kifkif
                        if val_identifiante not in index_mots:
                            index_mots.append(val_identifiante)
                        identifiant_mot=index_mots.index(val_identifiante) + 50
                    else:
                        texte_token = " ".join(token["text"])
                        if texte_token not in index_mots:
                            index_mots.append(texte_token)
                        identifiant_mot=index_mots.index(texte_token) + 50
                        attribs_token["pos"] = token["pos"]
                        attribs_token["lemma"]  = token["lemma"]

                    attribs_token["word_id"] = identifiant_mot.__str__()
                    if output_id == -1:
                        output_id=identifiant_mot.__str__()

                    attribs_token["word_id"] = identifiant_mot.__str__()
                    attribs_token["output_id"] = output_id

                    noeud_token = ElementTree.SubElement(noeud_tokens, "token", attribs_token)
                    noeud_token.text = texte_token

                    current_count+=1

                phrases_length.append(current_count+i_token-1)
                i_token+=current_count-1

                for constraint_key in analyse['restructured_constraints'].keys():

                    constraint = analyse["restructured_constraints"][constraint_key]

                    attribs_contrainte = {"name":constraint_key}
                    attribs_contraintes['uniq'] = (int(attribs_contraintes['uniq'])+1).__str__()
                    noeud_contrainte = ElementTree.SubElement(noeud_contraintes, "contrainte", attribs_contrainte)
                    i_socle = 0
                    for contrainte_détails in constraint:
                        socle = ElementTree.SubElement(noeud_contrainte, "socle", id=i_socle.__str__())
                        attribs_contraintes['count'] = (int(attribs_contraintes['count'])+1).__str__()
                        i_socle += 1
                        for noeud_key in sorted(contrainte_détails.keys(), key=lambda clé: int(clé[0:clé.index("(")])):
                            index_node_rule=int(noeud_key[0:noeud_key.index("(")])
                            node_socle_attribs = {'index_node_rule':index_node_rule.__str__()}
                            if "(/" not in noeud_key:
                                node_socle_attribs["pos"]=noeud_key[noeud_key.index("(")+1:noeud_key.index("/")]
                            if "/)" not in noeud_key:
                                node_socle_attribs["word"]=noeud_key[noeud_key.index("/")+1:noeud_key.index(")")]
                            if ")_" in noeud_key:
                                node_socle_attribs["rule"]=noeud_key[noeud_key.index(")")+2:]

                            node_socle = ElementTree.SubElement(socle, "node", node_socle_attribs)
                            for token_node_socle in contrainte_détails[noeud_key]:
                                ElementTree.SubElement(node_socle, "token", index=(int(token_node_socle)+phrases_length[sentence_key]).__str__())


                if "triplets_full" in analyse.keys():
                    analyse_triplet = analyse["triplets_full"]
                    for relation_key in analyse_triplet.keys():
                        noeud_relation = liste_relations[relation_key]

                        if "tokens" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens']= "0"

                        if "tokens_object" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens_object']= "0"

                        if "tokens_subject" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens_subject']= "0"

                        noeud_relation.attrib['identification'] = "full"

                        noeud_relation_sujet = liste_sujet_objet[relation_key]["sujet"]
                        noeud_relation_objet = liste_sujet_objet[relation_key]["objet"]

                        analyse_sujet_items = analyse_triplet[relation_key]["subject"][noeud_relation_sujet.attrib["id"]]
                        analyse_object_items = analyse_triplet[relation_key]["object"][noeud_relation_objet.attrib["id"]]

                        for analyse_sujet in analyse_sujet_items:
                            position_dans_sentence = int(tables_inversées_des_index[sentence_key][analyse_sujet['indexFin']])
                            attribs_item = {"index":(position_dans_sentence+ phrases_length[sentence_key]).__str__(),
                                                                                   'identification': analyse_sujet["caracteristique"]}
                            if analyse_sujet["caracteristique"] != "equal":
                                attribs_item['text']= diff_ref[sentence_key][(position_dans_sentence+ phrases_length[sentence_key]).__str__()]

                            ElementTree.SubElement(noeud_relation_sujet, "token", attribs_item)

                            noeud_relation.attrib['tokens'] = (int(noeud_relation.attrib['tokens']) + 1).__str__()
                            noeud_relation.attrib['tokens_subject'] = (int(noeud_relation.attrib['tokens_subject']) + 1).__str__()


                        for analyse_object in analyse_object_items:
                            position_dans_sentence = int(tables_inversées_des_index[sentence_key][analyse_object['indexFin']])
                            attribs_item = {
                                "index": (position_dans_sentence + phrases_length[sentence_key]).__str__(),
                                'identification': analyse_object["caracteristique"]}
                            if analyse_object["caracteristique"] != "equal":
                                attribs_item['text'] = diff_ref[sentence_key][
                                    (position_dans_sentence + phrases_length[sentence_key]).__str__()]

                            ElementTree.SubElement(noeud_relation_objet, "token", attribs_item)
                            noeud_relation.attrib['tokens'] = (int(noeud_relation.attrib['tokens']) + 1).__str__()
                            noeud_relation.attrib['tokens_object'] = (int(noeud_relation.attrib['tokens_object']) + 1).__str__()

                if "triplets_subject" in analyse.keys():
                    analyse_triplet = analyse["triplets_subject"]
                    for relation_key in analyse_triplet.keys():
                        noeud_relation = liste_relations[relation_key]

                        if "tokens" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens']= "0"

                        if "tokens_object" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens_object']= "0"

                        if "tokens_subject" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens_subject']= "0"

                        if 'identification' not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['identification'] = "subject"
                        elif noeud_relation.attrib['identification'] == "object":
                            noeud_relation.attrib['identification'] = "full"

                        noeud_relation_sujet = liste_sujet_objet[relation_key]["sujet"]

                        analyse_sujet_items = analyse_triplet[relation_key]["subject"][noeud_relation_sujet.attrib["id"]]

                        for analyse_sujet in analyse_sujet_items:
                            position_dans_sentence = int(
                                tables_inversées_des_index[sentence_key][analyse_sujet['indexFin']])
                            attribs_item = {"index": (position_dans_sentence + phrases_length[sentence_key]).__str__(),
                                            'identification': analyse_sujet["caracteristique"]}
                            if analyse_sujet["caracteristique"] != "equal":
                                attribs_item['text'] = diff_ref[sentence_key][
                                    (position_dans_sentence + phrases_length[sentence_key]).__str__()]

                            ElementTree.SubElement(noeud_relation_sujet, "token", attribs_item)
                            noeud_relation.attrib['tokens'] = (int(noeud_relation.attrib['tokens']) + 1).__str__()
                            noeud_relation.attrib['tokens_subject'] = (
                            int(noeud_relation.attrib['tokens_subject']) + 1).__str__()

                if "triplets_object" in analyse.keys():
                    analyse_triplet = analyse["triplets_object"]
                    for relation_key in analyse_triplet.keys():
                        noeud_relation = liste_relations[relation_key]

                        if 'identification' not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['identification'] = "object"
                        elif noeud_relation.attrib['identification'] == "subject":
                            noeud_relation.attrib['identification'] = "full"

                        if "tokens" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens']= "0"

                        if "tokens_object" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens_object']= "0"

                        if "tokens_subject" not in noeud_relation.attrib.keys():
                            noeud_relation.attrib['tokens_subject']= "0"



                        noeud_relation_objet = liste_sujet_objet[relation_key]["objet"]

                        analyse_object_items = analyse_triplet[relation_key]["object"][noeud_relation_objet.attrib["id"]]
                        for analyse_object in analyse_object_items:
                            position_dans_sentence = int(tables_inversées_des_index[sentence_key][analyse_object['indexFin']])
                            attribs_item = {
                                "index": (position_dans_sentence + phrases_length[sentence_key]).__str__(),
                                'identification': analyse_object["caracteristique"]}
                            if analyse_object["caracteristique"] != "equal":
                                attribs_item['text'] = diff_ref[sentence_key][
                                    (position_dans_sentence + phrases_length[sentence_key]).__str__()]
                            ElementTree.SubElement(noeud_relation_objet, "token", attribs_item)
                            noeud_relation.attrib['tokens'] = (int(noeud_relation.attrib['tokens']) + 1).__str__()
                            noeud_relation.attrib['tokens_object'] = (int(noeud_relation.attrib['tokens_object']) + 1).__str__()



            i_count+=1
            noeud_tokens.attrib=attribs_tokens
            noeud_contraintes.attrib=attribs_contraintes

        shared.progress(lenk, lenk)
        print()

        print("On sauvegarde")
        try:
            fjson = open(JSON_LEARNING_REFS, "w")
            fjson.write(json.dumps(self.learning_base_dictionnary,  indent=2,ensure_ascii=False))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
        except KeyboardInterrupt:
            if not fjson.closed:
                fjson.close()
                while not fjson.closed:
                    True

        print("sauvegarde effectuée base de référence")
        print((len(self.learning_base_dictionnary.keys())).__str__() + " entrées")
        print()
        del self.learning_base_dictionnary

        from bs4 import BeautifulSoup
        bs = BeautifulSoup(ElementTree.tostring(xml_restructured_data, method="xml"), 'xml')
        text_re = re.compile('>\n\s+([^<>\s].*?)\n\s+</', re.DOTALL)
        prettyXml = text_re.sub('>\g<1></', bs.prettify())

        f = open(XML_WORKING_DATA_FILE, 'w')

        f.write(bs.prettify())
        f.close()

        del  xml_restructured_data
        #nos index de mots:


        sav_mots=[index_mots.pop(-1)]
        print("Construction des index de mots rapprochés, utilisé par similarite.py à lancer après un premier tour ici. Relancer webnlgparser.py ensuite")
        shared.init_progress()
        i_count=0
        for yop in index_mots:
            shared.progress(i_count, len(index_mots))
            i_count+=1
            save=0
            sav_mot=''
            found=False
            if not len(sav_mots)==len(index_mots)-1:
                for mot in sav_mots:
                    val = [yop, mot]
                    # optimisations et limitations du nombre de calcul
                    if not val[0].isdigit() or not val[1].isdigit():
                        if val[0].isdigit() == val[1].isdigit():
                            val[0] = val[0].strip("\"0129456789.").strip()
                            val[1] = val[1].strip("\"0129456789.").strip()
                        if len(val[0]) <=2  or len(val[1]) <=2 :
                            val = [yop, mot]

                    dist = shared.similar(val[0], val[1])
                    if dist==1:
                        sav_mots.insert(sav_mots.index(mot), yop)
                        found = True
                        break
                    elif dist>save or sav_mot=='':
                        save=dist
                        sav_mot=mot
                if not found:
                    if sav_mot.isdigit() and yop.isdigit():
                        if float(sav_mot)<float(yop):
                            sav_mots.insert(sav_mots.index(sav_mot)+1, yop)
                        else:
                            sav_mots.insert(sav_mots.index(sav_mot), yop)
                    else:
                        k=sorted([sav_mot, yop])
                        if k[0]==sav_mot:
                            sav_mots.insert(sav_mots.index(sav_mot) + 1, yop)
                        else:
                            sav_mots.insert(sav_mots.index(sav_mot), yop)

        shared.progress(len(index_mots), len(index_mots))
        # on sauvegarde
        print("Sauvegarde mots")
        fjson = open(JSON_INDEX_MOTS, "w")
        fjson.write(json.dumps(sav_mots, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        print("Terminé")

#struture test
if __name__ == '__main__':
    tree=Tree()
