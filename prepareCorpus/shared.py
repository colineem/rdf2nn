import sys, getpass

import difflib
getpass.getuser()

SERVEUR_CORENLP='http://zammih:5000'


HOME_ROOT="/home/"+getpass.getuser()+"/"
REP_WEBNLG=HOME_ROOT+"/data/webnlgChallenge"
WORKING_DATA_DIR="initialized_data/"
OUTPUT_DATA_DIR="webnlgparser_data/"

def similar(seq1, seq2):
    return difflib.SequenceMatcher(a=seq1, b=seq2).ratio()


global old_progress
old_progress=-1
def init_progress():
    global old_progress
    old_progress=0

def progress(i, end_val, bar_length=50, text =""):
    percent = float(i) / end_val
    hashes = u"\u2612" * int(round(percent * bar_length))
    spaces = u"\u2610" * (bar_length - len(hashes))
    rounded = int(round(percent * 100))
    global old_progress
    if percent and old_progress!=rounded:
        old_progress=rounded
        sys.stdout.write("\r\t\t\tPercent: [{0}] {1}%".format(hashes + spaces, rounded ))
        sys.stdout.flush()
    elif text != "":
        sys.stdout.write("\r\t\t"+ text + " - Percent: [{0}] {1}%".format(hashes + spaces, rounded )+ "           ")
        sys.stdout.flush()


class Error(Exception):
    """Base class for exceptions in this module."""
    pass

def nettoie(name):
    return name.replace("_", " ").replace("1", "").replace("bis", "").replace(" ter", "").replace("2", "").replace("5", "").replace("3", "").replace("4", "").strip()
