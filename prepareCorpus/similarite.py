

import json
import itertools
import difflib
import shared
from math import *


file = open('webnlgparser_data/logs/learning_base.json')
silver_file = json.load(file)
file.close()

output_id={}
items_list = {}

#on va mettre dans un tableau les identifiants auquels une valeur est égale
#chacun est tout d'abors égal à lui-même
for k in silver_file.values():
    output_id[k]=[k]

sorted_refs = [k for k in silver_file.values()]
taille_fenêtre = 1000
print(taille_fenêtre.__str__()+ ": taille de la fenêtre. ")
nombre_de_fenêtres = ceil(float(len(sorted_refs))/taille_fenêtre)

nombre_de_combinaisons=len(sorted_refs)*(len(sorted_refs)-1)

shared.init_progress()
print()
print("pour référence, combinaisons max =" + nombre_de_combinaisons.__str__() + ".")

print((nombre_de_fenêtres * 2).__str__() + " tours à faire.")


fenêtre_courante = 0
deb = 0

while fenêtre_courante < nombre_de_fenêtres * 2:

    old_end = deb + taille_fenêtre

    #print((deb,  deb + taille_fenêtre))

    for yop in itertools.combinations(sorted_refs[deb: deb + taille_fenêtre], 2):
        val = [yop[0], yop[1]]
        do_dist = True

        #optimisations et limitations du nombre de calcul
        if not val[0].isdigit() or not val[1].isdigit():
            if val[0].isdigit() == val[1].isdigit():
                val[0]=val[0].strip("\"0129456789.").strip()
                val[1]=val[1].strip("\"0129456789.").strip()
                if len(val[0]) <=2  or len(val[1]) <=2 :
                    val = [yop[0], yop[1]]
        elif val[0].isdigit() == val[1].isdigit():#isdigit semble plus long que levenstein...
            do_dist = False

        if do_dist:
            dist=shared.similar(val[0], val[1])
            if dist>0.85:#pour éviter ça : ('929', '1929') 0.85
                output_id[yop[0]].append(yop[1])
                output_id[yop[1]].append(yop[0])

    deb = old_end - floor(taille_fenêtre / 2)

    fenêtre_courante += 1
    shared.progress(fenêtre_courante, nombre_de_fenêtres*2)



shared.progress(1, 1)
print()
print("Tour terminé en valeur max " + old_end.__str__() + ".")
print()
print(len(sorted_refs))

#les combinaisons ont été faites sans doublons
for key, values in output_id.items():
    #chacun des items contenus a pour identifiant celui de la tête:
    for value in values:
        if key not in items_list.keys():
            items_list[key] = len(items_list)
            items_list[value] = items_list[key]
        else:
            items_list[value] = items_list[key]

print("Tous les " +  (len(sorted_refs)).__str__() + " items doivent être présents. ")

print("Il y en a " + (len(set([k for k in items_list.values()]))).__str__() +".")

result = len(set([k for k in items_list.values()]))
print("Soit une réduction de " + (len(sorted_refs)-result).__str__() + ".")

clusterisation = True

while clusterisation:
    clusterisation = False
    #les combinaisons ont été faites sans doublons
    for key, values in output_id.items():
        results = []
        #Les identifiants doivent être partagés par tout le groupe
        for value in values:
            if items_list[value] not in results:
                results.append(items_list[value])
        if len(results)>1:
            #on regroupe les concepts et on refera un tour
            clusterisation = True

            keeped = results.pop()
            for result in results:
                for key_item in items_list.keys():
                    if items_list[key_item] == result:
                        items_list[key_item] = keeped

#on va s'amuse à renuméroter
ids = sorted(list(set([k for k in items_list.values()])))

for key_item in items_list.keys():
    items_list[key_item] = ids.index(items_list[key_item])

print("Nombres d'identifiants après clusterisation :  ", end='')
result = len(set([k for k in items_list.values()]))
print(result)
print("--> soit une réduction de " + (len(sorted_refs)-result).__str__() + " après clusterisation.")


#on sort les identifiants
fjson = open("similarite_data/identifiants.json", "w")
fjson.write(json.dumps(items_list, indent=2, ensure_ascii=False))
fjson.flush()
fjson.close()
while not fjson.closed:
    True

#on sort les rapprochements effectués
rapprochements = {}
for key, value in  items_list.items():
    if value not in rapprochements.keys():
        rapprochements[value]=[]
    rapprochements[value].append(key)
#si une clé n'est pas dans les valeurs..
for key in  items_list.keys():
    if key not in rapprochements.keys():
        rapprochements[key]=[]
    rapprochements[key].append(key)
fjson = open("similarite_data/logs/id--valeurs.json", "w")
fjson.write(json.dumps(rapprochements, indent=2, ensure_ascii=False))
fjson.flush()
fjson.close()
while not fjson.closed:
    True
