import sys, os
import shared
import json, glob

import random

ALL = True
REFRESH = False
if len(sys.argv)>1:
    if sys.argv[1].upper()=="REFERENCE":
        ALL = False
        print("Fichier de référence en constitution")
    elif sys.argv[1].upper()=="REFRESH":
        ALL = False
        REFRESH = True
        print("Fichier de référence en mode refresh")
    else:
        ALL = True
if ALL:
    print("Socle statistique toutes données")


STATS_ON = 150

REP_WEBNLG = shared.REP_WEBNLG
DATA_REFS="webnlgparser_data/detailled_informations/"
STATISTIQUES="statistiques_data/"
STATS_FILE="statistiques.json"

DATA="webnlgparser_data/"
EXCLUDED = [DATA_REFS+"learning_base.json",  DATA_REFS+"learning_refs.json"]
JSON_LEARNING_REFS = DATA+"logs/learning_refs.json"


#6253044
#12506087
filecount = 0
filelist={}
for fichier in glob.glob(DATA_REFS + "*.json"):  # [-1:]:
    filecount += 1
    if fichier not in EXCLUDED:
        file = open(fichier)
        filelist[fichier] = json.load(file)

        file.close()

#réf data permet d'aller chercher des informations complémentaires.
tmp_ref_file= open(JSON_LEARNING_REFS)
print(JSON_LEARNING_REFS)
ref_data = json.load(tmp_ref_file)
tmp_ref_file.close()


data_stats={}

def get_name(name):
    return (os.path.splitext(name)[0].replace(DATA_REFS, '').replace("bis", "").replace("1", "").replace("2", "").replace("5", "").replace("3", "").replace("4", "").replace ("_", " ")).strip()

constraints_used = set()

if ALL:
    for k in ref_data.keys():
        data_stats[k] = {}
        data_stats[k]["structures"]=[]
        for constraints in [ref_data[k][2][sentence]["analyse"]["constraints"].keys() for sentence in ref_data[k][2].keys()]:
            data_stats[k]["structures"] += list(set([shared.nettoie(contrainte) for contrainte in constraints]))
            constraints_used.update([shared.nettoie(contrainte) for contrainte in constraints])
        #contrainte unique par phrase, mais non par lexicalisation
        data_stats[k]["structures lexicalisation"] = list(set(data_stats[k]["structures"]))
else:
    CONSTRAINTS_TABLE = {}
    if REFRESH:
        REFERENCE_FILE = "statistiques_reference.json"
        GENERAL_FILE="statistiques_all.json"

        file = open(STATISTIQUES + REFERENCE_FILE)#.replace(".json", "_reference.json"))
        silver_file = json.load(file)
        file.close()

        file = open(STATISTIQUES + GENERAL_FILE)
        data_calc = json.load(file)
        file.close()

        to_del = []
        for key in silver_file.keys():
            if key in data_calc.keys():
                silver_file[key]["structures"] = data_calc[key]["structures"]
                for structure in data_calc[key]["structures"]:
                    if structure not in CONSTRAINTS_TABLE:
                        CONSTRAINTS_TABLE[structure]=0
                    CONSTRAINTS_TABLE[structure]+=1
                    CONSTRAINTS_TABLE[structure]=0
            else:
                to_del.append(key)
                print("suppression de "+ key)
        for key in to_del:
            del  silver_file[key]
        data_stats = silver_file





    else:
        for fichier in filelist.keys():
            if get_name(fichier) not in CONSTRAINTS_TABLE:
                CONSTRAINTS_TABLE[get_name(fichier)]=0

            #s'il y a moins de contraintes on prend tout
            if len(filelist[fichier])< (STATS_ON/(len(filelist.keys())+1)) + CONSTRAINTS_TABLE[get_name(fichier)] :
                for item in filelist[fichier]:
                    if item[0] not in data_stats.keys():
                        data_stats[item[0]]={}
                        data_stats[item[0]]["structures"] = []
                    if get_name(fichier) not in data_stats[item[0]]["structures"]:
                        data_stats[item[0]]["structures"] .append(get_name(fichier))
                        constraints_used.add(get_name(fichier))
            else:
                #on prélève à hauteur...
                icount=0
                while icount < STATS_ON/((len(filelist.keys()))+1) + CONSTRAINTS_TABLE[get_name(fichier)]:
                    icount+=1
                    item = random.choice(filelist[fichier])
                    if item[0] not in data_stats.keys():
                        data_stats[item[0]]={}
                        data_stats[item[0]]["structures"] = []
                    if get_name(fichier) not in data_stats[item[0]]["structures"]:
                        data_stats[item[0]]["structures"] .append(get_name(fichier))
                        constraints_used.add(get_name(fichier))


    #on complète et remplit
    if len(data_stats)< STATS_ON:
        k=[k for k in ref_data.keys()]
        icount=0
        while icount < STATS_ON/((len(filelist.keys()))+1) and len(k)>0:
            item=random.choice(k)
            k.remove(item)
            if sum(len(ref_data[item][2][i_sent]['analyse']['constraints']) for i_sent in ref_data[item][2].keys()) == 0:
                data_stats[item] = {}
                data_stats[item]["structures"] = []
                icount+=1

    #on comlète les listes à hauteur de x en piochant n'importe où
    poudoum = [k for k in filelist.keys()]
    if len(data_stats.keys()) < STATS_ON:
        while len(data_stats.keys())< min(len(ref_data.keys()), STATS_ON):
            fichier=random.choice(poudoum)

            item = random.choice(filelist[fichier])
            if item[0] not in data_stats.keys():
                data_stats[item[0]] = {}
                data_stats[item[0]]["structures"] = []
            if get_name(fichier) not in data_stats[item[0]]["structures"]:
                data_stats[item[0]]["structures"] .append(get_name(fichier))
                constraints_used.add(get_name(fichier))

R_data=[]
global_infos_acteurs = []#au sein d'un lot de triplets, on va repérer quels sont les prédicats pour lesquels l'acteur est sujet, les prédicats pour lesquels l'acteur est objet, on gardera aussi le nombre de tokens en sortie, le nombre de contraintes et la taille du lot
diversité_usages_sujet_objet = {}
constraints_used = sorted(constraints_used)
columns = ['triplets', 'relations', 'acteurs', 'phrase', 'pronominalisation', 'référence pronominale', 'nombre tokens']
columns += constraints_used
columns.append('contraintes')
columns.append('contraintes uniques')
print("contraintes : ")
print(constraints_used)
R_full_data=[]

shared.init_progress()
count=len(data_stats.keys())
step=0
for item in data_stats.keys():

    shared.progress(step, count)
    step+=1
    if not REFRESH:
        data_stats[item]["structures silver"] = []
    else:
        if "structures silver" not in data_stats[item]:
            data_stats[item]["structures silver"] = []
    R_data.append([])
    for fichier in filelist.keys():
        for values in filelist[fichier]:
            if values[0]==item and get_name(fichier) not in data_stats[item]["structures"]:
                data_stats[item]["structures"].append(get_name(fichier))
    data_stats[item]["phrase origine"] = ref_data[item][1]
    data_stats[item]["informations triplets"] = {}
    data_stats[item]["informations triplets"]["phrases"]=0
    data_stats[item]["informations triplets"]["pronominalisation"]=0
    data_stats[item]["informations triplets"]["reference pronominale"]=0
    data_stats[item]["informations triplets"]["nombre tokens"]=0
    relations_contenues = set()

    for sentence in ref_data[item][2].keys():
        data_stats[item]["informations triplets"]["phrases"] += 1
        data_stats[item]["informations triplets"]["nombre tokens"] += len(ref_data[item][2][sentence]['analyse']['restructured_tokens'])
        #chaque information ne doit compter qu'une fois
        triplet_elt_used = []

        sa = ref_data[item][2][sentence]['analyse']
        str_type_triplets = ["triplets_full", "triplets_subject", "triplets_object"]
        for str_type in str_type_triplets:
            if str_type in sa.keys():
                for tid in sa[str_type].keys():
                    for triplet in sa[str_type][tid].keys():
                        for clé_triple in sa[str_type][tid][triplet]:
                            for triplet_elt in sa[str_type][tid][triplet][clé_triple]:
                                elt_ref = "".join(triplet_elt.values())
                                if triplet_elt["caracteristique"] == "pronominalization":
                                    if elt_ref not in triplet_elt_used:
                                        triplet_elt_used.append(elt_ref)
                                        data_stats[item]["informations triplets"]["pronominalisation"] += 1
                                elif triplet_elt["caracteristique"] == "possessive pronominalization":
                                    if elt_ref not in triplet_elt_used:
                                        triplet_elt_used.append(elt_ref)
                                        data_stats[item]["informations triplets"]["reference pronominale"] += 1


    acteurs=[]
    infos_acteurs = {}
    for triple_key in ref_data[item][0].keys():
        sujet = ref_data[item][0][triple_key][0]
        prédicat = ref_data[item][0][triple_key][1]
        objet = ref_data[item][0][triple_key][2]
        relations_contenues.add(prédicat)
        acteurs.append(sujet)
        acteurs.append(objet)
        if sujet not in infos_acteurs.keys():
            infos_acteurs[sujet] = {'sujet des prédicats':[], 'objet des prédicats':[]}
        infos_acteurs[sujet]['sujet des prédicats'].append(prédicat)

        if objet not in infos_acteurs.keys():
            infos_acteurs[objet] =  {'sujet des prédicats':[], 'objet des prédicats':[]}
        infos_acteurs[objet]['objet des prédicats'].append  (prédicat)
        if sujet not in diversité_usages_sujet_objet.keys():
            diversité_usages_sujet_objet[sujet]={'sujet des prédicats': set(), 'objet des prédicats': set(), 'informant les prédicats': set()}

        if objet not in diversité_usages_sujet_objet.keys():
            diversité_usages_sujet_objet[objet] = {'sujet des prédicats': set(), 'objet des prédicats': set(), 'informant les prédicats': set()}

        diversité_usages_sujet_objet[sujet]['sujet des prédicats'].add(prédicat)
        diversité_usages_sujet_objet[objet]['objet des prédicats'].add(prédicat)

        diversité_usages_sujet_objet[sujet]['informant les prédicats'].add(prédicat)
        diversité_usages_sujet_objet[objet]['informant les prédicats'].add(prédicat)



    for acteur_key in infos_acteurs.keys():
        #INFORMATIONS DE NIVEAU SUPÉRIEUR POUR FILTRER
        infos_acteurs[acteur_key]["acteur"] = acteur_key
        infos_acteurs[acteur_key]["taille lot triplets"] = len(ref_data[item][0].keys())
        infos_acteurs[acteur_key]["tokens"] = data_stats[item]["informations triplets"]["nombre tokens"]

        global_infos_acteurs.append(infos_acteurs[acteur_key])



    data_stats[item]["informations triplets"]["acteurs uniques"]=len(set(acteurs))
    data_stats[item]["informations triplets"]["acteurs"]=len(acteurs)

    #cel1 1 de la ligne 'triplets'
    R_data[-1].append(len(ref_data[item][0].keys()))
    #cel1 1 de la ligne 'relations' : les relations disctinctes contenues
    R_data[-1].append(len(relations_contenues))
    #cel1 1 de la ligne 'acteurs'
    R_data[-1].append(data_stats[item]["informations triplets"]["acteurs uniques"])
    #cel1 1 de la ligne 'phrases':
    R_data[-1].append(data_stats[item]["informations triplets"]["phrases"])
    #cel1 1 de la ligne 'pronominalisation'
    R_data[-1].append(data_stats[item]["informations triplets"]["pronominalisation"])
    #cel1 1 de la ligne 'reference_pronomoninale':
    R_data[-1].append(data_stats[item]["informations triplets"]["reference pronominale"])
    #nombre de tckens
    R_data[-1].append(data_stats[item]["informations triplets"]["nombre tokens"])

    count_unique_contrainte = 0
    for constraint in constraints_used:
        #R_data[-1].append(True if data_stats[item]["structures"].count(constraint)> 0 else False)
        R_data[-1].append(data_stats[item]["structures"].count(constraint))
        if data_stats[item]["structures"].count(constraint) > 0:
            count_unique_contrainte +=1
    R_data[-1].append(count_unique_contrainte)
    R_data[-1].append(len(data_stats[item]["structures"]))

    for triple_key in ref_data[item][0].keys():
        R_full_data.append([ref_data[item][0][triple_key][1].replace(",", " ").replace("  ", " ")]+R_data[-1])

shared.progress(1,1)
print()
str_name = "data_acteurs.json"
if ALL:
    str_name = "data_acteurs_all.json"
fjson = open(STATISTIQUES+str_name, "w")
fjson.write(json.dumps(global_infos_acteurs, indent=2, ensure_ascii=False))
fjson.flush()
fjson.close()
while not fjson.closed:
    True

print("sauvegarde infos acteurs json effectuée")

if ALL:
    str_name = "informations diversité objets.sujets"

    fWfile = open(STATISTIQUES + "R/" + str_name + '.csv', "w")
    row = ["acteur", 'sujet pour prédicats', 'objet pour prédicats', 'informant prédicats']
    fWfile.write(" , ".join(row) + "\n")
    for acteur in diversité_usages_sujet_objet.keys():
        row = []
        row.append(acteur)
        row.append(len(diversité_usages_sujet_objet[acteur]['sujet des prédicats']))
        row.append(len(diversité_usages_sujet_objet[acteur]['objet des prédicats']))
        row.append(len(diversité_usages_sujet_objet[acteur]['informant les prédicats']))
        fWfile.write(" , ".join([valeur.__str__() for valeur in row]) + "\n")
        fWfile.flush()
    fWfile.close()
     #set not serializable
    #fjson = open(STATISTIQUES+str_name+'.json', "w")
    #fjson.write(json.dumps(diversité_usages_sujet_objet, indent=2, ensure_ascii=False))
    #fjson.flush()
    #fjson.close()
    #while not fjson.closed:
    #    True

shared.progress(1, 1)
print()
str_name = "R/data_acteurs.csv"
if ALL:
    str_name = "R/data_acteurs_all.csv"

shared.init_progress()
count = len(global_infos_acteurs)
step = 0
fWfile = open(STATISTIQUES + str_name, "w")
row = ['acteur', 'sujetPrédicats', 'objetPrédicats', 'triplets', 'tokens']
fWfile.write(" , ".join(row) + "\n")
for infos_acteurs in global_infos_acteurs:
    row = []
    row.append(infos_acteurs["acteur"])
    #colonnes = :'sujet des prédicats'[], 'objet des prédicats', 'taille lot triplets' et 'tokens'
    row.append(len(infos_acteurs['sujet des prédicats']))
    row.append(len(infos_acteurs['objet des prédicats']))
    row.append(infos_acteurs['taille lot triplets'])
    row.append(infos_acteurs['tokens'])
    shared.progress(step, count)
    step += 1
    fWfile.write(" , ".join([valeur.__str__() for valeur in row]) + "\n")
    fWfile.flush()
fWfile.close()
shared.progress(1, 1)
print()
print("sauvegarde infos acteurs R (csv) effectuée")


shared.progress(1,1)
print()
str_name = "R/data.csv"
if ALL:
    str_name = "R/data_all.csv"

shared.init_progress()
count=len(R_data)
step=0
fWfile = open(STATISTIQUES + str_name, "w")
fWfile.write( (" , ".join(columns) + "\n").replace(" ", "_").replace("é", "e").replace("_,_", " , ") )
for row in R_data:
    shared.progress(step, count)
    step+=1
    fWfile.write(" , ".join([valeur.__str__() for valeur in row]) + "\n")
    fWfile.flush()
fWfile.close()
shared.progress(1,1)
print()
print("sauvegarde contraintes effectuée")


str_name = "R/data_relations.csv"
if ALL:
    str_name = "R/data_all_relations.csv"

fWfile = open(STATISTIQUES + str_name, "w")
columns.insert( 0 , "relation")
#columns.insert( 2 , "relations")


fWfile.write( (" , ".join(columns) + "\n").replace(" ", "_").replace("é", "e").replace("_,_", " , ") )

shared.init_progress()
count=len(R_full_data)
step=0
for row in R_full_data:
    shared.progress(step, count)
    step+=1
    fWfile.write(" , ".join([valeur.__str__() for valeur in row]) + "\n")
    fWfile.flush()
fWfile.close()

shared.progress(1,1)
print()
print("sauvegarde contrainte par relation effectuée")


print(len(data_stats))

if ALL:
    STATS_FILE=STATS_FILE.replace(".json", "_all.json")
else:
    STATS_FILE = STATS_FILE.replace(".json", "_reference.json")
fjson = open(STATISTIQUES+STATS_FILE, "w")
fjson.write(json.dumps(data_stats, indent=2, ensure_ascii=False))
fjson.flush()
fjson.close()
while not fjson.closed:
    True

print(STATISTIQUES+STATS_FILE + " créé.")
