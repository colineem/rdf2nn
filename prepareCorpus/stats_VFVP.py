
import json



STATISTIQUES="statistiques_data/"
STATS_FILE="statistiques.json"
GENERAL_FILE="statistiques_all.json"
REFERENCE_FILE="statistiques_reference.json"
CONSTRAINTS = {'apposition' : {}, 'coordinated clauses' : {}, 'coordinated full clauses' : {}, 'direct object' : {}, 'existential' : {}, 'indirect object' : {}, 'juxtaposition' : {}, 'participle clause' : {}, 'participle clause subject' : {}, 'passive voice' : {}, 'possessif' : {}, 'relative adverb' : {}, 'relative object' : {}, 'relative subject' : {}, 'twice objects': {}}

file = open(STATISTIQUES+REFERENCE_FILE)
silver_file = json.load(file)
file.close()


file = open(STATISTIQUES+GENERAL_FILE)
data_calc = json.load(file)
file.close()

classes = ["vrai négatif", "vrai positif", "faux négatif", "faux positif"]
for key in CONSTRAINTS:
    for classe in classes:
        CONSTRAINTS[key][classe] = 0

del classes
for key in silver_file.keys():
    for constraint in CONSTRAINTS.keys():
        classe="vrai négatif"
        if constraint in silver_file[key]["structures silver"]:
            if constraint in data_calc[key]["structures"]:
                classe = "vrai positif"
            else:
                classe = "faux négatif"
        else:
            if constraint in data_calc[key]["structures"]:
                classe = "faux positif"
        if classe not in CONSTRAINTS[constraint].keys():
            CONSTRAINTS[constraint][classe] = 0
        CONSTRAINTS[constraint][classe] += 1

import pprint
pprint.pprint(CONSTRAINTS)
print()

rappel = {}
précision = {}
fscore = {}
for constraint in sorted(CONSTRAINTS.keys()):

    try:
        for classe in ["vrai négatif", "vrai positif","faux négatif", "faux positif"]:
            if classe not in CONSTRAINTS[constraint].keys():
                CONSTRAINTS[constraint][classe] = 0
        try:
            rappel[constraint] = float(CONSTRAINTS[constraint]["vrai positif"])/(CONSTRAINTS[constraint]["vrai positif"]+CONSTRAINTS[constraint]["faux négatif"])
        except Exception as inst:
            rappel[constraint] = "NaN"
        try:
            précision[constraint] = float(CONSTRAINTS[constraint]["vrai positif"])/(CONSTRAINTS[constraint]["vrai positif"]+CONSTRAINTS[constraint]["faux positif"])
        except Exception as inst:
            précision[constraint] = "NaN"
        try:
            fscore[constraint] = (2*((précision[constraint]*rappel[constraint])/(précision[constraint]+rappel[constraint])))
        except Exception as inst:
            fscore[constraint] = "NaN"
        print("rappel " + constraint + " : "+ rappel[constraint].__str__())
        print("précision " + constraint + " : "+ précision[constraint].__str__())
        print("F-score " + constraint + " : " + (fscore[constraint]).__str__())
        print()
    except Exception as inst:
        fscore[constraint] = "NaN"
print("#"*10)
import glob, json, os, pprint


#6253044
#12506087
filecount = 0
data={}
decompte={}
for fichier in glob.glob("webnlgparser_data/detailled_informations/*.json"):  # [-1:]:
    filecount += 1
    file = open(fichier)
    name = os.path.basename(fichier)
    data_tmp = json.load(file)
    file.close()
    for list in data_tmp:
        if list[0] not in data.keys():
            data[list[0]] = [name[0:name.rindex(".")]]
        else:
            data[list[0]].append(name[0:name.rindex(".")])

    decompte[name[0:name.rindex(".")]] = len(data_tmp)

decompte["sans contrainte"] = 1301

import math

print(math.fsum(decompte.values()))


print("contrainte,rappel,précision,F-score")
for constraint in sorted(CONSTRAINTS.keys()):
    print(constraint, end = ",")
    print(rappel[constraint].__str__(), end = ",")
    print(précision[constraint], end = ",")
    print(fscore[constraint])
CONSTRAINTS['sans contrainte'] = {}
CONSTRAINTS['sans contrainte']["vrai positif"] = 10
CONSTRAINTS['sans contrainte']["faux positif"] = 0
rappel['sans contrainte'] = 1.
précision['sans contrainte'] = 1.
fscore['sans contrainte'] = 1.
print("contrainte & \# compte corpus & \# compte éval. & rappel & précision & F-score")
for constraint in sorted(CONSTRAINTS.keys()):
    print("\\textit{" + constraint + "}", end = " & " + decompte[constraint].__str__() + " & ")
    print((CONSTRAINTS[constraint]["faux positif"]  + CONSTRAINTS[constraint]["vrai positif"]).__str__(), end = " & ")
    print(round(rappel[constraint], 2).__str__(), end = " & ")
    print(round(précision[constraint], 2), end = " & ")
    print(round(fscore[constraint], 2), end = "\\\\ \hline ")
    print()

print("Le rappel est défini par le nombre de documents pertinents retrouvés au regard du nombre de documents pertinents.")
print("La précision est définie par le nombre de documents pertinents retrouvés au regard du nombre de documents total.")





