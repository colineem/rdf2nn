import glob, json, os, pprint


#6253044
#12506087
filecount = 0
data={}
decompte={}
for fichier in glob.glob("detailled_informations/*.json"):  # [-1:]:
    filecount += 1
    file = open(fichier)
    name = os.path.basename(fichier)
    data_tmp = json.load(file)
    file.close()
    for list in data_tmp:
        if list[0] not in data.keys():
            data[list[0]] = [name[0:name.rindex(".")]]
        else:
            data[list[0]].append(name[0:name.rindex(".")])

    decompte[name[0:name.rindex(".")]] = len(data_tmp)

file = open("logs/learning_refs.json")
data_tmp = json.load(file)
file.close()
decompte["sans contrainte"] = 0

for key in data_tmp.keys():
    if key not in data.keys():
        data[key] = []
        decompte["sans contrainte"] += 1


pprint.pprint(decompte)
print("nombre de lexicalisations : ", end = '')
print(len(data.keys()))