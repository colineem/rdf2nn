import argparse
import os
from progress.bar import Bar
import time
from multiprocessing import Pool, Semaphore, Queue, Process
import multiprocessing as mp
import math

managerProd = mp.Manager()
managerRef = mp.Manager()
# Define an output queue
outputProximiteProd = managerProd.Queue()
outputProximiteRef = managerRef.Queue()

atime=time.time()


import sys

# TODO système de rangement plus simple des données

parser = argparse.ArgumentParser(description='Evalue la diversité d\'un corpus généré.')
parser.add_argument('--input', metavar='-i', type=str, nargs='+',  dest="file_input", default='data_test_predictions.txt',
                   help='input file')
parser.add_argument('--output', metavar='-o', type=str, nargs='+', dest="file_output",  default='data_test_predictions',
                   help='output file, ne pas préciser d\'extension, sera relex pour relexicalisation, tsv pour tsv')
parser.add_argument('--lex', metavar='-l', dest="file_lexicalisation",
                   nargs = '+', default='_test.relex', type=str,
                   help='lexicalisations')
parser.add_argument('--ref_src', metavar='-c', dest="ref_src",
                   nargs = '?', default='inputs_test.data', type=str,
                   help='fichier des demandes originelles.')
parser.add_argument('--simplification', action='store_true',
                   help='Travail sur simplifications plutôt qu\'expansion.')
parser.add_argument('--log', action='store_true',
                   help='Correspond à verbose off : bascule les sorties dans un log (training results) et ne touche pas aux stats enregistrées.',)
parser.add_argument('--multi_sent', action='store_true',
                   help='Option activant le traitement des lexicalisations comportant plusieurs phrases.',)
parser.add_argument('--fusion', action='store_true',
                   help='Option activant la recherche de contrainte sur l\'élément partagé et sur l\'élément ajouté.',)
parser.add_argument('--invert_constraint', action='store_true',
                   help='Option faisant porter la recherche de contrainte sur l\'élément du triplet ajouté, et non support.',)
parser.add_argument('--theme', action='store_true',
                   help='Organise par thème d\'apprentissage ("city"), non implémenté')
parser.add_argument('--theme_split', action='store_true',
                   help='Organise par thème d\'apprentissage ("city") en arrière plan')
parser.add_argument('--shorted_function', action='store_true',
                   help='Raccourci la function appliquée à la sentence.',)
parser.add_argument('--constraint_in_output_or_in_input', action='store_true',
                   help='Avec cette option, on n\'exclut pas que la contrainte soit et dans l\'entrée, et dans la sortie.',)
parser.add_argument('--constraint_somewhere_in_output', action='store_true',
                   help='La contrainte peut ne pas etre liée au triplet en gestion, mais porter sur un autre élément dans la lexicalisation',)
parser.add_argument('--text_2_text', action='store_true',
                   help='Similaire à triple_2_sentence mais produit un corpus pour opennmt',)
parser.add_argument('--data_2_text', action='store_true',
                   help='Similaire à triple_2_sentence, mais produit un corpus pour opennmt avec en entrée les descriptions logiques',)
parser.add_argument('--remove_constrainst', action='store_true',
                   help='retrait contrainte on text_2_text',)
parser.add_argument('--text_2_text_restricted', action='store_true',
                   help='Nécessite text_2_text, restreint les paires à un différentiel d\'un sur les contraintes. ',)
parser.add_argument('--key_1', action='store_true',
                   help='Restreint la clé : phrase jamais vue quelque soit la contrainte',)
parser.add_argument('--key_2', action='store_true',
                   help='Restreint la clé : phrase associée à un triplet logique jamais vue quelque soit la contrainte',)
parser.add_argument('--unfilter_words', action='store_true',
                  help='Désactive le filtre sur les mots, non valide pour data_2_text',)
parser.add_argument("-lt", '--little', default=0, type=int,
                  help='Sort du programme si données test > 1800 lignes',)
parser.add_argument('--only_retreat', action='store_true',
                  help='Prévu pour réaliser le compte de la diversité des demandes quand cela n\'a pas été fait.',)
parser.add_argument('--base_line', action='store_true',
                   help='Option sans contrainte pour d2t et t2t',)
parser.add_argument('--nbest5', action='store_true',
                   help='Option sans contrainte pour d2t et t2t, sur retour n-best 5',)
parser.add_argument('--all', action='store_true',
                   help='Calculs effectués pour les données mises de côté pour all (cf makeAll.py)',)

parser.add_argument('--debug', action='store_true',
                   help='Active le stockage dans un répertoire debug.',)
parser.add_argument('--mode_flux_corrected_v3', action='store_true',
                   help='Active le stockage dans un répertoire debug.',)

args = parser.parse_args()
if args.all:
        conteneur_apprentissage = "all"
        conteneur_construction_data = "all/json"
        conteneur_training_data = "all/training_results"
else:
    if args.unfilter_words:
        if args.data_2_text:
            exit(5)
    # il me faut des dossiers json/training_results/data
    if args.text_2_text:
        if args.multi_sent:
            conteneur_apprentissage = "text_2_text/multi_sent"
            conteneur_construction_data = "text_2_text/multi_sent/json"
            conteneur_training_data = "text_2_text/multi_sent/training_results"
        else:
            conteneur_apprentissage = "text_2_text/simple"
            conteneur_construction_data = "text_2_text/simple/json"
            conteneur_training_data = "text_2_text/simple/training_results"
        if args.remove_constrainst:
            conteneur_apprentissage = conteneur_apprentissage.replace('text_2_text', "text_2_text/remove_constrainst")
            conteneur_construction_data = conteneur_construction_data.replace('text_2_text', "text_2_text/remove_constrainst")
            conteneur_training_data = conteneur_training_data.replace('text_2_text', "text_2_text/remove_constrainst")
        if args.fusion or args.invert_constraint:
            exit(0)
    elif args.data_2_text:
        if args.multi_sent:
            conteneur_apprentissage = "data_2_text/multi_sent"
            conteneur_construction_data = "data_2_text/multi_sent/json"
            conteneur_training_data = "data_2_text/multi_sent/training_results"
        else:
            conteneur_apprentissage = "data_2_text/simple"
            conteneur_construction_data = "data_2_text/simple/json"
            conteneur_training_data = "data_2_text/simple/training_results"
        if args.fusion or args.invert_constraint:
            exit(0)
    else:

        if not args.multi_sent and not args.invert_constraint and not args.fusion:
            # conteneur des données d'apprentissage
            conteneur_apprentissage = "expansion/simple"
            conteneur_construction_data = "expansion/simple/json"
            conteneur_training_data = "expansion/simple/training_results"
        elif args.multi_sent and not args.invert_constraint and not args.fusion:
            # conteneur des données d'apprentissage
            conteneur_apprentissage = "expansion/multi_sent"
            conteneur_construction_data = "expansion/multi_sent/json"
            conteneur_training_data = "expansion/multi_sent/training_results"
        elif not args.multi_sent and args.invert_constraint and not args.fusion:
            # conteneur des données d'apprentissage
            conteneur_apprentissage = "expansion/simple_invert_constraint"
            conteneur_construction_data = "expansion/simple_invert_constraint/json"
            conteneur_training_data = "expansion/simple_invert_constraint/training_results"
        elif args.multi_sent and args.invert_constraint and not args.fusion:
            # conteneur des données d'apprentissage
            conteneur_apprentissage = "expansion/multi_invert_constraint"
            conteneur_construction_data = "expansion/multi_invert_constraint/json"
            conteneur_training_data = "expansion/multi_invert_constraint/training_results"
        elif args.multi_sent and args.fusion:
            # conteneur des données d'apprentissage
            conteneur_apprentissage = "expansion/multi_sent_fusion"
            conteneur_construction_data = "expansion/multi_sent_fusion/json"
            conteneur_training_data = "expansion/multi_sent_fusion/training_results"
        elif not args.multi_sent and args.fusion:
            # conteneur des données d'apprentissage
            conteneur_apprentissage = "expansion/simple_fusion"
            conteneur_construction_data = "expansion/simple_fusion/json"
            conteneur_training_data = "expansion/simple_fusion/training_results"
        else:
            print(parser.parse_args())
            print("Situation non prévue, l'inversion du support des contraintes n'étant pas compatible avec une fusion... fusion toute contrainte porteuse est prise en compte.")
            exit(1)

        if args.simplification:
            conteneur_apprentissage = conteneur_apprentissage.replace("expansion/", "simplification/")
            conteneur_construction_data = conteneur_construction_data.replace("expansion/", "simplification/")
            conteneur_training_data = conteneur_training_data.replace("expansion/", "simplification/")

    if args.text_2_text or args.data_2_text:
        if args.invert_constraint and not args.fusion:
            conteneur_apprentissage = conteneur_apprentissage.replace("2_text", "2_text/invert_constraint")
            conteneur_construction_data = conteneur_construction_data.replace("2_text", "2_text/invert_constraint")
            conteneur_training_data = conteneur_training_data.replace("2_text", "2_text/invert_constraint")
        elif args.fusion:
            conteneur_apprentissage = conteneur_apprentissage.replace("2_text", "2_text/fusion")
            conteneur_construction_data = conteneur_construction_data.replace("2_text", "2_text/fusion")
            conteneur_training_data = conteneur_training_data.replace("2_text", "2_text/fusion")
        if args.invert_constraint and  args.fusion:
            exit(5)
        elif args.constraint_in_output_or_in_input:
            # TODO
            exit(6)
    if args.theme or args.theme_split:
        conteneur_apprentissage = "themes/" + conteneur_apprentissage
        conteneur_construction_data = "themes/" + conteneur_construction_data
        conteneur_training_data = "themes/" + conteneur_training_data

        if args.theme_split:
            conteneur_apprentissage = conteneur_apprentissage.replace("themes", "themes/theme_split")
            conteneur_construction_data = conteneur_construction_data.replace("themes", "themes/theme_split")
            conteneur_training_data = conteneur_training_data.replace("themes", "themes/theme_split")
        else:
            conteneur_apprentissage = conteneur_apprentissage.replace("themes", "themes/theme_non_split")
            conteneur_construction_data = conteneur_construction_data.replace("themes", "themes/theme_non_split")
            conteneur_training_data = conteneur_training_data.replace("themes", "themes/theme_non_split")

        if args.text_2_text or args.data_2_text:
            # todo implémenter
            exit(0)


    if args.shorted_function:
        conteneur_apprentissage = "shorted/" + conteneur_apprentissage
        conteneur_construction_data = "shorted/" + conteneur_construction_data
        conteneur_training_data = "shorted/" + conteneur_training_data

    if args.constraint_somewhere_in_output:
        conteneur_apprentissage = "diff_sentences/" + conteneur_apprentissage
        conteneur_construction_data = "diff_sentences/" + conteneur_construction_data
        conteneur_training_data = "diff_sentences/" + conteneur_training_data
        args.constraint_in_output_or_in_input
    elif args.constraint_in_output_or_in_input:
        conteneur_apprentissage = "constraint/input_output/" + conteneur_apprentissage
        conteneur_construction_data = "constraint/input_output/" + conteneur_construction_data
        conteneur_training_data = "constraint/input_output/" + conteneur_training_data
    else:
        conteneur_apprentissage = "constraint/in_output/" + conteneur_apprentissage
        conteneur_construction_data = "constraint/in_output/" + conteneur_construction_data
        conteneur_training_data = "constraint/in_output/" + conteneur_training_data

    if args.text_2_text_restricted:
        conteneur_apprentissage = "text_2_text_restricted/" + conteneur_apprentissage
        conteneur_construction_data = "text_2_text_restricted/" + conteneur_construction_data
        conteneur_training_data = "text_2_text_restricted/" + conteneur_training_data

    if args.key_1:
        conteneur_apprentissage = "contrainte_never_seen/" + conteneur_apprentissage
        conteneur_construction_data = "contrainte_never_seen/" + conteneur_construction_data
        conteneur_training_data = "contrainte_never_seen/" + conteneur_training_data

    if args.key_2:
        conteneur_apprentissage = "contrainte_never_seen_for_triplet_and_sentence/" + conteneur_apprentissage
        conteneur_construction_data = "contrainte_never_seen_for_triplet_and_sentence/" + conteneur_construction_data
        conteneur_training_data = "contrainte_never_seen_for_triplet_and_sentence/" + conteneur_training_data
        if args.key_1:
            exit(3)

    if args.unfilter_words:
        conteneur_apprentissage = "no_filter/" + conteneur_apprentissage
        conteneur_construction_data = "no_filter/" + conteneur_construction_data
        conteneur_training_data = "no_filter/" + conteneur_training_data


    if args.base_line:
        if not (args.text_2_text or args.data_2_text):
            exit(1)
        if args.key_1 or args.key_2:
            exit(1)
        if not args.multi_sent:
            exit(1)
        conteneur_apprentissage = "base_line/" + conteneur_apprentissage
        conteneur_construction_data = "base_line/" + conteneur_construction_data
        conteneur_training_data = "base_line/" + conteneur_training_data


if args.debug:
    conteneur_apprentissage = "debug/" + conteneur_apprentissage
    conteneur_construction_data = "debug/" + conteneur_construction_data
    conteneur_training_data = "debug/" + conteneur_training_data
if args.mode_flux_corrected_v3:
    conteneur_apprentissage = "mode_flux_corrected_v3/" + conteneur_apprentissage
    conteneur_construction_data = "mode_flux_corrected_v3/" + conteneur_construction_data
    conteneur_training_data = "mode_flux_corrected_v3/" + conteneur_training_data

if args.nbest5:
    if not args.base_line:
        exit(2)
    if not os.path.isfile(conteneur_training_data + "/data_test_predictions_nbest5.txt"):
        # print(conteneur_training_data + " inexistant.")
        exit(0)
else:
    if not os.path.isfile(conteneur_training_data + "/data_test_predictions.txt"):
        # print(conteneur_training_data + " inexistant.")
        exit(0)

if not os.path.isfile(conteneur_training_data + "/../outputs_test.data"):
    print(conteneur_training_data + " : données de références inexistantes.")
    exit(1)

old_stdout = sys.stdout

file_prefix = "diversiteMeaning"
if args.nbest5:
    file_prefix = "diversiteMeaning_nbest5"

str_entete = "fichier,similariteProd,similariteRef,ratio,masse,nbDemande"
if os.path.isfile("diversiteMeaning.csv"):
    entete = False
else:
    entete = True

if os.path.isfile(conteneur_training_data + "/" + file_prefix + '.info'):

    with open(conteneur_training_data + "/" + file_prefix + '.info') as f:
        data = f.readlines()
        f.close()

    retreatAllDones = (data[1].endswith(",0") or data[1].endswith(",0\n"))

    if retreatAllDones:

        with open(conteneur_training_data + "/../inputs_test.data") as f:
            inputs_test_data = f.readlines()

        already_dones = set()

        for input in inputs_test_data:
            already_dones.add(input)

        retreated = data[1].split(",")
        retreated[0] = len(already_dones).__str__()

        retreated = ",".join(retreated)

        data[1] = retreated

        f = open(conteneur_training_data + "/" + file_prefix + '.info', 'w')
        f.write("".join(data))
        f.flush()
        f.close()
        while not f.closed:
            True

    fjson = open("diversiteMeaning.csv", "a")
    if args.log:
        if entete:
            fjson.write(str_entete)
        for line in data:
            fjson.write(line)
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        # print("\ndiversiteMeaning.csv écrit pour " + conteneur_training_data)
    else:
        print(data)
else:
    if args.only_retreat:
        exit()

    if args.log:
        f = open(conteneur_training_data + "/" + file_prefix + '.log', 'w')
        # We redirect the 'sys.stdout' command towards the descriptor file
        sys.stdout = f
    else:
        print(conteneur_apprentissage + " en traitement.")

    if args.nbest5:
        data_test = "data_test_predictions_nbest5"
    else:
        data_test = "data_test_predictions"
    with open(conteneur_training_data + "/" + data_test + ".txt") as f:
        producted_data = f.readlines()

        if args.mode_flux_corrected_v3:
            for i, line in enumerate(producted_data):
                producted_data[i] = " ".join(producted_data[i].strip().split(" ")[1:])


    with open(conteneur_training_data + "/../outputs_test.data") as f:
        referenced_data = f.readlines()
        if args.mode_flux_corrected_v3:
            for i, line in enumerate(referenced_data):
                referenced_data[i] = " ".join(referenced_data[i].strip().split(" ")[1:])

    with open(conteneur_training_data + "/../inputs_test.data") as f:
        inputs_test_data = f.readlines()
        for i, line in enumerate(inputs_test_data):
            inputs_test_data[i] = inputs_test_data[i]


    with open(conteneur_training_data + "/../outputs_test.meaning") as f:
        outputs_test_meaning = f.readlines()

    if args.little > 0:
        if len(producted_data) > args.little:
            print(conteneur_training_data + " remis aux calendes grecques.")
            exit(5)
    
    print(conteneur_training_data + ":" + len(producted_data).__str__() + " lignes à traiter.")

    import shared

    already_dones = set()

    for input in inputs_test_data:
        already_dones.add(input)

    distinct_meaning = {}
    for meaning in outputs_test_meaning:
        if meaning.strip() not in distinct_meaning.keys():
            distinct_meaning[meaning.strip()] = 0
        distinct_meaning[meaning.strip()] += 1

    localcal1 ={}
    localcal2 ={}

    processes = []

    def operation(args):
        corpus, test_meaning, input_data, queue = args
        bar = Bar('Processing ', max=len(corpus))
        meaningBank = {}
        setInput = set()
        for item, meaning, input in zip(corpus, test_meaning, input_data):
            meaning = meaning.strip()
            item = item.strip()
            input = input.strip()
            if input+item not in setInput:
                setInput.add(input+item)
                if meaning not in meaningBank.keys():
                    meaningBank[meaning] = []
                meaningBank[meaning].append(item)

        bar = Bar('Processing ', max=len(meaningBank.values()))
        for key in meaningBank.keys():
            proxiAB = {}
            corpus_meaningBank = meaningBank[key]
            if len(corpus_meaningBank) > 1:

                for i in range(len(corpus_meaningBank)):
                    corpus_meaningBank[i] = corpus_meaningBank[i].strip()

                for item in corpus_meaningBank:
                    if not item in proxiAB.keys():
                        print(item)
                        proximiteProd = 0
                        produced_corpus_without_current = corpus_meaningBank[:]
                        produced_corpus_without_current.remove(item)
                        for compar in produced_corpus_without_current:
                            if not compar+item in localcal1.keys():
                                localcal1[compar+item] = shared.similar(compar, item)
                                localcal1[item+compar] = localcal1[compar+item]
                            proximiteProd += localcal1[compar + item]
                        proxiAB[item] = proximiteProd / (len(corpus_meaningBank) - 1)
                    queue.put(proxiAB[item])
            bar.next()
        bar.finish()
        queue.put('Done')

    pool = mp.Pool()
    if args.nbest5:
        t = []
        for corpus in (outputs_test_meaning, inputs_test_data):
            tmp_corpus = []
            for line in corpus:
                for i in range(5):
                    tmp_corpus.append(line)
            t.append(tmp_corpus)
        a = (producted_data, t[0], t[1], outputProximiteProd )
    else:

        for i, produced in enumerate(producted_data):
            producted_data[i] = producted_data[i].strip()
            if i > len(referenced_data) -1 :
                producted_data.pop(i)
            else:
                referenced_data[i] = referenced_data[i].strip()

        print("traitement " + len(producted_data).__str__())

        a = (producted_data, outputs_test_meaning, inputs_test_data, outputProximiteProd )
    b = (referenced_data, outputs_test_meaning, inputs_test_data, outputProximiteRef)
    pool.map(operation, [a, b])

    pool.close()
    pool.join()

    proximitésProd = []
    proximitésRef = []
    msg1 = ""
    msg2 = ""
    while True:
        if msg1 != "Done":
            msg1 = outputProximiteProd.get()
            if msg1 != "Done":
                proximitésProd.append(msg1)
        if msg2 != "Done":
            msg2 = outputProximiteRef.get()
            if msg2 != "Done":
                proximitésRef.append(msg2)
        if msg1 == 'Done' and  msg2 == 'Done':
            break

    if len(proximitésProd) > 0:
        proximiteProd = sum(proximitésProd)/len(proximitésProd)
    else:
        proximiteProd = 0.
    if len(proximitésRef) > 0:
        proximiteRef = sum(proximitésRef)/len(proximitésRef)
    else:
        proximiteRef = 0.

    if proximiteProd>0:
        proximitéRatio = proximiteRef/proximiteProd
    else:
        proximitéRatio = 0

    print(proximiteRef).__str__()
    print(proximiteProd).__str__()
    print(proximitéRatio).__str__()
    if args.log:
        fjson = open("diversiteMeaning.csv", "a")
        if entete:
            fjson.write(str_entete)
        data = '\n'+ ",".join([conteneur_training_data,
                                    (proximiteRef).__str__(),
                                    (proximiteProd).__str__(),
                                    (proximitéRatio).__str__(),
                                    (len(producted_data)).__str__(),
                                    (len(already_dones)).__str__(),
                                    (len(distinct_meaning.keys())).__str__(),
                                    (math.fsum(distinct_meaning.values())/len(distinct_meaning.keys())).__str__(),
                                    ])
        fjson.write(data)
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        print("diversiteMeaning.csv écrit pour " + conteneur_training_data)
        
    btime=time.time()
    difftime = btime - atime # display the difference in seconds
    difftuple = time.gmtime(difftime)
    print("\n")
    print(len(producted_data).__str__() + " lignes traitées.")
    print("Temps d'exécution : %i heures %i minutes %i secondes" % ( difftuple.tm_hour, difftuple.tm_min, difftuple.tm_sec))


    f = open(conteneur_training_data + "/" +  file_prefix + '.info', 'w')
    f.write(data)
    f.flush()
    f.close()
    while not f.closed:
        True
    
    sys.stdout = old_stdout
    print("\n")
    print(conteneur_training_data)
    print(len(producted_data).__str__() + " lignes traitées.")
    print("proximiteRef", end = " : ")
    print(proximiteRef).__str__()
    print("proximiteProd", end = " : ")
    print(proximiteProd).__str__()
    print("proximiteRatio", end = " : ")
    print(proximitéRatio).__str__()
    print("distinct_meaning count", end = " : ")
    print(len(distinct_meaning.keys()))
    print("textes par meaning", end = " : ")
    print(math.fsum(distinct_meaning.values())/len(distinct_meaning.keys()))
    print( "Temps d'exécution : %i heures %i minutes %i secondes" % ( difftuple.tm_hour, difftuple.tm_min, difftuple.tm_sec) )
    print()

