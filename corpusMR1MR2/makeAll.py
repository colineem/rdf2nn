import os
import math
import json
import random
from pprint import pprint
from nltk.tokenize.moses import MosesDetokenizer
import argparse

import string

translator = str.maketrans('', '', string.punctuation)

import testContraintes, shared


import color

parser = argparse.ArgumentParser(description='Génération d\'un corpus d\'apprentissage, lancer avec --only_cmd pour le pré-traitement permettant de calculer la taille des corpus de test et de validation.')
parser.add_argument('--baselined2t', action='store_true',
                  help='Sorties pour data_2_text',)
parser.add_argument('--baselinet2t', action='store_true',
                  help='Sorties pour text_2_text',)
parser.add_argument('--t2t', action='store_true',
                  help='Sorties pour text_2_text',)
parser.add_argument('--d2t', action='store_true',
                  help='Sorties pour text_2_text',)
parser.add_argument('--ext', action='store_true',
                  help='Sorties pour text_2_text',)
parser.add_argument('--simp', action='store_true',
                  help='Sorties pour text_2_text',)
parser.add_argument('--d2t_t2t_all', action='store_true',
                  help='Sorties pour base_line + all',)
parser.add_argument('--all', action='store_true',
                  help='all',)

args=parser.parse_args()
baselined2t = args.baselined2t
baselinet2t = args.baselinet2t
if args.baselined2t and args.baselinet2t:
    exit(5)
if args.baselined2t and args.d2t_t2t_all:
    exit(5)
if args.baselinet2t and args.d2t_t2t_all:
    exit(5)

detokenizer = MosesDetokenizer()

directories = ["no_filter/text_2_text_restricted/constraint/in_output/text_2_text/multi_sent/training_results", 
"no_filter/diff_sentences/expansion/multi_sent/training_results",
"constraint/in_output/data_2_text/multi_sent/training_results",
"no_filter/diff_sentences/simplification/multi_sent/training_results",
#"base_line/constraint/in_output/data_2_text/multi_sent/training_results",
#"base_line/constraint/in_output/text_2_text/multi_sent/training_results",
               ]
if args.d2t_t2t_all:
    directories+=["base_line/constraint/in_output/data_2_text/multi_sent/training_results",
"base_line/constraint/in_output/text_2_text/multi_sent/training_results"]



modèles = ["text_2_text",
"expansion",
"data_2_text",
"simplification",
#"baseline data_2_text",
#"baseline text 2 text",
           ]

if args.d2t_t2t_all:
    modèles += ["baseline data_2_text",
"baseline text 2 text"]

sets = []
annotations_corpus = {}
count_not_take_source = {}
count_not_take = 0

if baselined2t :
    directories = ["base_line/constraint/in_output/data_2_text/multi_sent/training_results"]
    modèles = ["baseline data_2_text"]
elif baselinet2t :
    directories = ["base_line/constraint/in_output/text_2_text/multi_sent/training_results"]
    modèles = ["baseline text 2 text"]
elif args.d2t:
    directories = ["constraint/in_output/data_2_text/multi_sent/training_results",                   ]
    modèles = ["data_2_text"]
elif args.t2t:
    directories = ["no_filter/text_2_text_restricted/constraint/in_output/text_2_text/multi_sent/training_results"]
    modèles = ["text_2_text"]
elif args.ext:
    directories = ["no_filter/diff_sentences/expansion/multi_sent/training_results"]
    modèles = ["expansion"]
elif args.simp:
    directories = ["no_filter/diff_sentences/simplification/multi_sent/training_results"]
    modèles = ["simplification"]


if os.path.isfile("store_constraints.json"):
    with open("store_constraints.json", 'r') as f:
        store = json.load(f)
        f.close()
else:
    store = {}

modeles_2_csv = {}
meaningsKeep = []

for directory in directories:
    meaningRef = directory.replace("training_results", "outputs_test.meaning")

    if os.path.isfile(meaningRef):
        with open(meaningRef) as f:
            read_data_output = f.readlines()
            f.close()
        sets.append(set())
        sets[-1]=set([k.strip() for k in read_data_output]) 
    else:
        print(meaningRef + " inexistant.")



setCommun = set.intersection(*map(set,sets))
meaningsKeep = list(setCommun)
random.shuffle(meaningsKeep)
meaningsKeep = meaningsKeep[0:50]

print()
print("taille du set commun : ", end = '')
print(len(setCommun))

if len(sets) != len(directories):
    exit(1)
    
del(sets)

print()
print()
print("Détail des sets")
print()

directory_count = {}
selected_by_meaning = {}

if not os.path.isdir("all/training_results"):
    os.makedirs("all/json")
    os.makedirs("all/training_results")

virtual_meanings = []
virtual_inputs = []
virtual_outputs = []
virtual_relex = []
virtual_prod = []    
virtual_source = []

len_store = len(store.keys())

popi = []
for i, dir in enumerate(directories[:]):
    if os.path.isdir(dir):
        fichier =dir+"/data_test_predictions.txt"

        if "base_line" in dir:
            fichier =dir+"/data_test_predictions_nbest5.txt"

        if not os.path.isfile(fichier):
            directories.remove(dir)
            print(dir + " sans prédiction.")
        else:
            print(dir + " ok.")
    else:
        popi.append(i)

popi.reverse()
for i in popi:
    modèles.pop(i)

if len(modèles)<2 and not (args.baselined2t or args.baselinet2t or args.d2t or args.t2t or args.simp or args.ext):
    print(directories)
    print("pas assez de modèles")
    exit()

for i_dir, directory in enumerate(directories):

    relex_meaning = {}

    meaningRef = directory.replace("training_results", "outputs_test.meaning")

    inputRef = directory.replace("training_results", "inputs_test.data")
    outputRef = directory.replace("training_results", "outputs_test.data")
    relexRef = directory.replace("training_results", "_test.relex")
    if "base_line" in directory:
        outputProd = directory + "/data_test_predictions_nbest5.txt"
    else:
        outputProd = directory + "/data_test_predictions.txt"

    def duplicate(corpus:[]):
        tmp_corpus = []
        for line in corpus:
            for i in range(5):
                tmp_corpus.append(line)
        return tmp_corpus

    with open(meaningRef) as f:
        read_data_ref = f.readlines()
        f.close()
        if "base_line" in directory:
            read_data_ref = duplicate(read_data_ref)
    
    with open(outputProd) as f:
        read_data_output = f.readlines()
        f.close()
    
    with open(inputRef) as f:
        read_data_input = f.readlines()
        f.close()
        if "base_line" in directory:
            read_data_input = duplicate(read_data_input)

    with open(outputRef) as f:
        read_data_output_ref = f.readlines()
        f.close()
        if "base_line" in directory:
            read_data_output_ref = duplicate(read_data_output_ref)
     
    with open(relexRef) as f:
        read_data_relex = json.load(f)
        f.close()
        if "base_line" in directory:
            read_data_relex = duplicate(read_data_relex)
        
    count = {}

    localCount = 0
    for meaningRef, inputRef, relexRef, outputProd, outputRef in zip(read_data_ref, read_data_input, read_data_relex, read_data_output, read_data_output_ref):
        meaningRef = meaningRef.strip()
        if meaningRef in setCommun:

            inputRef = inputRef.strip()
            outputProd = outputProd.strip()
            outputRef = outputRef.strip()
            if meaningRef not in count.keys():
                count[meaningRef] = 0
            count[meaningRef] += 1
            localCount += 1
            if directory not in directory_count.keys():
                directory_count[directory] = 0
            directory_count[directory] += 1
                        
            virtual_meanings.append(meaningRef)
            virtual_inputs.append(inputRef)
            virtual_outputs.append(outputRef)
            virtual_relex.append(relexRef)
            virtual_prod.append(outputProd)   
            virtual_source.append(directory)

            relex_output = outputProd
            waited_outpout = outputRef


            # ça c'est pour éviter que des écarts d'encodage objets/sujets séparent des phrases identiques
            output_key = []
            for elt in outputProd.split():
                if "objet" not in elt.lower() and "sujet" not in elt.lower():
                    output_key.append(elt)
            output_key = " ".join(output_key)

            if meaningRef not in relex_meaning.keys():
                relex_meaning[meaningRef] = relexRef
            if meaningRef not in annotations_corpus.keys():
                annotations_corpus[meaningRef] = {}

            if outputRef not in annotations_corpus[meaningRef].keys():
                annotations_corpus[meaningRef][output_key] = {}

            relex_output = outputProd[:]
            waited_outpout = outputRef[:]
            init_relex_output = outputProd[:]
            init_waited_outpout = outputRef[:]

            referenceLex = relex_meaning[meaningRef]

            original = relex_output[:]
            original_waited_outpout = waited_outpout[:]
            inputRefRelex = inputRef.split()[1:]
            init_relex_input = inputRef.split()[1:]
            for data_mod in ("simplification", "expansion"):
                if data_mod in directory:
                    inputRefRelex[1] = "<b> " + inputRefRelex[1] + " </b>"
                    inputRefRelex[2] = inputRefRelex[2] + " <br/><b>"
                    inputRefRelex[-1] = inputRefRelex[-1] + " </b>"

            inputRefRelex = " ".join(inputRefRelex)

            for data_mod in ("text_2_text", "data_2_text"):
                if data_mod in directory:
                    inputRefRelex = "<b> " + inputRefRelex + "</b> "
            meaningRefRelex = meaningRef[:]
            for lex_key in referenceLex.keys():
                relex_output = relex_output.replace(lex_key, referenceLex[lex_key])
                inputRefRelex = inputRefRelex.replace(lex_key, referenceLex[lex_key])
                meaningRefRelex = meaningRefRelex.replace(lex_key, referenceLex[lex_key])
                waited_outpout = waited_outpout.replace(lex_key, referenceLex[lex_key])

            for lex_key in relexRef.keys():
                init_relex_output = init_relex_output.replace(lex_key, relexRef[lex_key])
                init_waited_outpout = init_waited_outpout.replace(lex_key, relexRef[lex_key])
                init_relex_input = init_relex_output.replace(lex_key, relexRef[lex_key])

            take = True
            #on ne prend dans le corpus d'annotation que les phrases qui passent un premier niveau de fouille d'erreur:
            if "objet" in relex_output.lower() or "sujet" in relex_output.lower():
                if "objet" in init_relex_output.lower() or "sujet" in init_relex_output.lower():
                    take = False
            if "objet" in init_relex_output.lower() or "sujet" in init_relex_output.lower() :
                take = False

            if take:

                relex_output = relex_output.replace("_", " ")
                inputRefRelex = inputRefRelex.replace("_", " ")
                waited_outpout = waited_outpout.replace("_", " ")

                init_relex_output = init_relex_output.replace("_", " ")
                init_waited_outpout = init_waited_outpout.replace("_", " ")

                source = ""

                if "text_2_text" in directory:
                    source = "text_2_text"
                elif "data_2_text" in directory:
                    source = "data_2_text"
                elif "simplification" in directory:
                    source = "simplification"
                else:
                    source = "extension"
                if not "base_line" in directory:
                    if relex_output not in store.keys():
                        contraintes = testContraintes.getConstraintes(relex_output)
                        if type(contraintes) == str:
                            testContraintes.connect()
                            contraintes = testContraintes.getConstraintes(relex_output)
                            if type(contraintes) == str:
                                print("error nlp")
                                exit(5)
                        contraintes2 = testContraintes.getConstraintes(original)
                        if type(contraintes2) == str:
                            testContraintes.connect()
                            contraintes2 = testContraintes.getConstraintes(original)
                            if type(contraintes2) == str:
                                print("error nlp")
                                exit(5)

                        # pas très normal, mais une erreur mémoire coté stanford pourrait se produire
                        if not (contraintes2 == {} or type(contraintes2) == str):
                            for contrainte in contraintes2.keys():
                                if contrainte not in contraintes.keys():
                                    contraintes[contrainte] = contraintes2[contrainte]
                            # on ajoute pas d'informations sur les contraintes déjà présentes, d'abord on l'a déjà référencée, peut-être pour autre chose, mais les tokens seraient à recalculer...
                        store[relex_output] = contraintes

                    contraintes = store[relex_output]
                    solt_c = []
                    for ic, cont in enumerate(contraintes):
                        solt_c.append(shared.nettoie(cont))
                    contraintes = solt_c
                    del solt_c

                    contrainte = shared.nettoie(inputRef.split(" ")[0])


                inputRefRelex = " ".join(detokenizer.detokenize(inputRefRelex.split(" ")))
                relex_output = " ".join(detokenizer.detokenize(relex_output.split(" ")))
                waited_outpout = " ".join(detokenizer.detokenize(waited_outpout.split(" ")))

                init_relex_output = " ".join(detokenizer.detokenize(init_relex_output.split(" ")))
                init_waited_outpout = " ".join(detokenizer.detokenize(init_waited_outpout.split(" ")))


                if relex_output not in annotations_corpus[meaningRef][output_key].keys():
                    annotations_corpus[meaningRef][output_key][relex_output] = {"asked":"", 'meaningRefRelex':meaningRefRelex,"waited": [], "original_waited_outpout": [],"original": [], 'annotators': {'Claire': "", 'Emilie': ""}, "source": [], "contrainte": [], "alter":[], "alter_waited":[]}
                    if meaningRef not in selected_by_meaning.keys():
                        selected_by_meaning[meaningRef] = 0
                    selected_by_meaning[meaningRef] += 1
                if init_relex_output not in annotations_corpus[meaningRef][output_key][relex_output]['alter']:
                    annotations_corpus[meaningRef][output_key][relex_output]['alter'].append(init_relex_output)
                    annotations_corpus[meaningRef][output_key][relex_output]['alter_waited'].append(init_waited_outpout)
                if original not in annotations_corpus[meaningRef][output_key][relex_output]['original']:
                    annotations_corpus[meaningRef][output_key][relex_output]['original'].append(original)
                    annotations_corpus[meaningRef][output_key][relex_output]['original_waited_outpout'].append(original_waited_outpout)

                if source not in annotations_corpus[meaningRef][output_key][relex_output]["source"]:
                    annotations_corpus[meaningRef][output_key][relex_output]["source"].append(source)

                if not "base_line" in directory:
                    if contrainte not in annotations_corpus[meaningRef][output_key][relex_output]["contrainte"]:
                        annotations_corpus[meaningRef][output_key][relex_output]["contrainte"].append([contrainte, contraintes])

                if waited_outpout not in annotations_corpus[meaningRef][output_key][relex_output]["waited"]:
                    annotations_corpus[meaningRef][output_key][relex_output]["waited"].append(waited_outpout)
                annotations_corpus[meaningRef][output_key][relex_output]["asked"] = inputRefRelex

                if args.all:
                    clé_modèle = "all"
                else:
                    clé_modèle = modèles[i_dir]
                if clé_modèle not in modeles_2_csv.keys():
                    modeles_2_csv[clé_modèle] = {}
                if meaningRef.strip() not in modeles_2_csv[clé_modèle].keys():
                    if (meaningRef.count("/")) > 2 and  (meaningRef.count("/")) <= 5 * 2: # à cinq triplets
                        if args.all:
                            clé_modèle = "all"
                        else:
                            clé_modèle = modèles[i_dir][:]
                        modeles_2_csv[clé_modèle][meaningRef.strip()] = []

                if meaningRef.strip() in modeles_2_csv[clé_modèle].keys():
#                    modeles_2_csv[modèles[i_dir]][meaningRef.count("/")] += 1

                    if "base_line" in directory:
                        modeles_2_csv[modèles[i_dir][:]][meaningRef.strip()].append(relex_output[:])
                    else:
                        if args.all:
                            clé_modèle = "all"
                        else:
                            clé_modèle = modèles[i_dir][:]
                        if contrainte[:]  + "<br/>" + relex_output[:] not in modeles_2_csv[clé_modèle][meaningRef.strip()]:
                            modeles_2_csv[clé_modèle][meaningRef.strip()].append(contrainte[:]  + "<br/>" + relex_output[:])



    print(directory)
    print("Nombre d'exemples pour le modèle :", end=" ")
    print(localCount)
#    print("Nombre d'exemples pour le modèle par meaning du set commun :")
#    pprint(count)
    print()
print("Nombre d'exemples pour le modèle pour le set commun :")            
pprint(directory_count)

total = 0    
total = math.fsum(k for k in directory_count.values())

print(total)
    
for k in directory_count.keys():
    print(k, end = " : ")
    print(directory_count[k]/total)
    
with open("all/outputs_test.lex", "w") as fjson:
    json.dump(virtual_relex, fjson, ensure_ascii=False, indent=2)

with open("all/annotations_corpus.json", "w") as fjson:
    json.dump(annotations_corpus, fjson, ensure_ascii=False, indent=2)

sets = []
for modèle in modeles_2_csv.keys():
    sets.append(set())
    for meaningRef in modeles_2_csv[modèle].keys():
        sets[-1].add(meaningRef)
setCommun = set.intersection(*map(set,sets))
meaningsKeep = list(setCommun)
random.shuffle(meaningsKeep)
meaningsKeep = meaningsKeep[0:50]


for modèle in modeles_2_csv.keys():
    file_annotation = modèle + ".csv"
    if args.baselined2t :
        file_annotation = "d2t_only_"+file_annotation
    elif args.baselinet2t:
        file_annotation = "t2t_only_"+file_annotation
    elif args.d2t_t2t_all:
        file_annotation = "all_"+file_annotation
    elif args.d2t or args.t2t or args.ext or args.simp or args.all:
        file_annotation = "alone_syn_"+file_annotation
    else:
        file_annotation = "all_syn_" + file_annotation

    with open("all/csv_selection/" + file_annotation, "w") as fjson:
        lines = []
        for meaningRef in modeles_2_csv[modèle].keys():
            if meaningRef in meaningsKeep and not isinstance(meaningRef, int):
                line = []
                line.append('"' + meaningRef.replace('"', '""') + "<br/>" + '"' )
                if not "baseline" in modèle:
                    lexticalisations = list(set(modeles_2_csv[modèle][meaningRef]))
                    random.shuffle(lexticalisations)
                else:
                    bck = modeles_2_csv[modèle][meaningRef][:]
                    cpy = bck[:]
                    lexticalisations = []
                    if len(bck) <= 10:
                        lexticalisations = [k for k in bck if k not  in lexticalisations[:]]
                    else:
                        while len(lexticalisations) < 10:
                            cpy = bck[0:5]
                            bck = bck[5:]
                            for item in cpy:
                                if item not in lexticalisations:
                                    lexticalisations.append(item)
                            if len(bck) == 0:
                                break
                line.append('"' + "<br/><br/>".join(lexticalisations[0:10]).replace('"', '""') + '"')
                line.append(len(set(lexticalisations)).__str__())
                lines.append(",".join(line))
        lines.insert(0, '"meaning","texts","count_distincts_without_constraint"')
        fjson.write(("\n".join(lines)))


if len_store != len(store.keys()):
    with open("store_constraints.json", "w") as fjson:
        json.dump(store, fjson, ensure_ascii=False, indent=2)
        f.close()
    print(color.green("modification magasin d'analyse : sauvegarde effectuée"))


if args.baselined2t or args.baselinet2t or args.d2t_t2t_all:
    exit(0)

fjson = open("all/training_results/" + "data_test_predictions.source", "w")
fjson.write(("\n".join(virtual_source)))
fjson.flush()
fjson.close()
while not fjson.closed:
    True
    
fjson = open("all/training_results/" + "data_test_predictions.txt", "w")
fjson.write(("\n".join(virtual_prod)))
fjson.flush()
fjson.close()
while not fjson.closed:
    True


fjson = open("all/" + "outputs_test.data", "w")
fjson.write(("\n".join(virtual_outputs)))
fjson.flush()
fjson.close()
while not fjson.closed:
    True

fjson = open("all/" + "outputs_test.meaning", "w")
fjson.write(("\n".join(virtual_meanings)))
fjson.flush()
fjson.close()
while not fjson.closed:
    True
    
fjson = open("all/" + "inputs_test.data", "w")
fjson.write(("\n".join(virtual_inputs)))
fjson.flush()
fjson.close()
while not fjson.closed:
    True

print()
print(count_not_take, end=" textes non sélectionnés.\n")

totalSelected = math.fsum(selected_by_meaning.values())

print(int(total - count_not_take), end=" textes sélectionnés.\n")
print(totalSelected, end=" textes uniques sélectionnés.\n")
print(totalSelected/len(selected_by_meaning.keys()), end=" textes par meaning sélectionnés.\n")
print(len(selected_by_meaning.keys()), " meaning avec textes sélectionnés.")
import numpy as np
values = np.array(list(selected_by_meaning.values()))
print(np.median(values), end=" médiane nombre de texte uniques par meaning sélectionnés.\n")

print("centiles :")
sep = ""
for i in range(10):
    if (i+1) %10 == 0:
        sep = ""
        print("\n")

    print(sep , end="")
    print(i+1 , end=": ")
    print(np.percentile(values, (i+1)*10), end="")
    sep= ", "

print()
print(max(values))
pprint(count_not_take_source)
print(min(values))