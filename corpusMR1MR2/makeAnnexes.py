import os
import math
import json
import random
from pprint import pprint
from nltk.tokenize.moses import MosesDetokenizer
import unidecode
import argparse

import string

translator = str.maketrans('', '', string.punctuation)

import testContraintes, shared

parser = argparse.ArgumentParser(description='Génération d\'un corpus d\'apprentissage, lancer avec --only_cmd pour le pré-traitement permettant de calculer la taille des corpus de test et de validation.')

parser.add_argument("-t", '--taille', default=50, type=int,
                    help='Nombre de meanings', )
parser.add_argument("-d", '--debug', action='store_true',
                    help='Active le debug', )
parser.add_argument('--only_one', action='store_true',
                    help='Active le debug', )

args=parser.parse_args()

print("% taille max de la sélection :", end= " ")
print(args.taille)
import color
def getAnnexe(baselinet2t:bool=False, baselined2t:bool=False, shuffle:bool=False, t2t:bool=False, d2t:bool=False, ext:bool=False, simp:bool=False, all:bool=False):
    oldRef = ""
    detokenizer = MosesDetokenizer()
    seens = set()
    directories = ["no_filter/text_2_text_restricted/constraint/in_output/text_2_text/multi_sent/training_results",
                   "no_filter/diff_sentences/expansion/multi_sent/training_results",
                   "constraint/in_output/data_2_text/multi_sent/training_results",
                   "no_filter/diff_sentences/simplification/multi_sent/training_results",
                   # "base_line/constraint/in_output/data_2_text/multi_sent/training_results",
                   # "base_line/constraint/in_output/text_2_text/multi_sent/training_results",
                   ]

    modèles = ["T2T",
               "TX",
               "D2T",
               "TR",
               # "baseline data_2_text",
               # "baseline text 2 text",
               ]

    sets = []
    count_not_take_source = {}
    count_not_take = 0

    if shuffle:
        directories = ["mode_flux_corrected_v3/base_line/constraint/in_output/text_2_text/multi_sent/training_results"]
        modèles = ["SHUF"]
    if baselined2t:
        directories = ["base_line/constraint/in_output/data_2_text/multi_sent/training_results"]
        modèles = ["D2T"]
    elif baselinet2t:
        directories = ["base_line/constraint/in_output/text_2_text/multi_sent/training_results"]
        modèles = ["TXR2T"]
    elif d2t:
        directories = ["constraint/in_output/data_2_text/multi_sent/training_results", ]
        modèles = ["D2T"]
    elif t2t:
        directories = ["no_filter/text_2_text_restricted/constraint/in_output/text_2_text/multi_sent/training_results"]
        modèles = ["T2T"]
    elif ext:
        directories = ["no_filter/diff_sentences/expansion/multi_sent/training_results"]
        modèles = ["TX"]
    elif simp:
        directories = ["no_filter/diff_sentences/simplification/multi_sent/training_results"]
        modèles = ["TR"]

    if os.path.isfile("store_constraints.json"):
        with open("store_constraints.json", 'r') as f:
            store = json.load(f)
            f.close()
    else:
        store = {}

    modeles_2_csv = {}
    modeles_2_annexes = {}
    meaningsKeep = []

    for directory in directories:
        meaningRef = directory.replace("training_results", "outputs_test.meaning")

        if os.path.isfile(meaningRef):
            with open(meaningRef) as f:
                read_data_output = f.readlines()
                f.close()
            sets.append(set())
            sets[-1] = set([k.strip() for k in read_data_output])
        else:
            print(meaningRef + " inexistant.")

    setCommun = set.intersection(*map(set, sets))
    meaningsKeep = list(setCommun)
    random.shuffle(meaningsKeep)
    meaningsKeep = meaningsKeep[0:50]

    print()
    print("taille du set commun : ", end='')
    print(len(setCommun))

    if len(sets) != len(directories):
        exit(1)

    del (sets)

    print()
    print()
    print("Détail des sets")
    print()

    directory_count = {}
    selected_by_meaning = {}

    if not os.path.isdir("all/training_results"):
        os.makedirs("all/json")
        os.makedirs("all/training_results")

    virtual_meanings = []
    virtual_inputs = []
    virtual_outputs = []
    virtual_relex = []
    virtual_prod = []
    virtual_source = []

    len_store = len(store.keys())

    popi = []
    for i, dir in enumerate(directories[:]):
        if os.path.isdir(dir):
            fichier = dir + "/data_test_predictions.txt"

            if "base_line" in dir and not "mode_flux_corrected" in directory:
                fichier = dir + "/data_test_predictions_nbest5.txt"

            if not os.path.isfile(fichier):
                directories.remove(dir)
                print(dir + " sans prédiction.")
            else:
                print(dir + " ok.")
        else:
            popi.append(i) # sinon je retire le répertoire et aucune donnée ne sera sorti pour lui

    popi.reverse()
    for i in popi:
        modèles.pop(i)

    if len(modèles) < 2 and not (baselined2t or baselinet2t or d2t or t2t or simp or ext or shuffle):
        print(directories)
        print("pas assez de modèles")
        exit()

    relex_meaning = {}

    for i_dir, directory in enumerate(directories):

        print(directory + " en traitement.")

        meaningRef = directory.replace("training_results", "outputs_test.meaning")
        inputMeaningRef = directory.replace("training_results", "inputs_test.meaning")


        inputRef = directory.replace("training_results", "inputs_test.data")
        outputRef = directory.replace("training_results", "outputs_test.data")
        relexRef = directory.replace("training_results", "_test.relex")
        if "base_line" in directory and not "mode_flux_corrected" in directory:
            outputProd = directory + "/data_test_predictions_nbest5.txt"
        else:
            outputProd = directory + "/data_test_predictions.txt"

        def duplicate(corpus: []):
            tmp_corpus = []
            for line in corpus:
                for i in range(5):
                    tmp_corpus.append(line)
            return tmp_corpus


        with open(meaningRef) as f:
            read_data_ref = f.readlines()
            f.close()
            if "base_line" in directory and not "mode_flux_corrected" in directory:
                read_data_ref = duplicate(read_data_ref)

        with open(inputMeaningRef) as f:
            inputMeaningRef = f.readlines()
            f.close()
            if "base_line" in directory and not "mode_flux_corrected" in directory:
                inputMeaningRef = duplicate(inputMeaningRef)

        with open(outputProd) as f:
            read_data_output = f.readlines()
            f.close()

        with open(inputRef) as f:
            read_data_input = f.readlines()
            f.close()
            if "base_line" in directory and not "mode_flux_corrected" in directory:
                read_data_input = duplicate(read_data_input)

        with open(outputRef) as f:
            read_data_output_ref = f.readlines()
            f.close()
            if "base_line" in directory and not "mode_flux_corrected" in directory:
                read_data_output_ref = duplicate(read_data_output_ref)

        with open(relexRef) as f:
            read_data_relex = json.load(f)
            f.close()
            if "base_line" in directory and not "mode_flux_corrected" in directory:
                read_data_relex = duplicate(read_data_relex)

        count = {}

        localCount = 0
        for meaningRef, inputRef, relexRef, outputProd, outputRef, inputMeaning in zip(read_data_ref, read_data_input, read_data_relex,
                                                                         read_data_output, read_data_output_ref, inputMeaningRef):
            meaningRef = meaningRef.strip()
            meaningRefCpy = inputMeaning.strip()

            if "mode_flux_corrected" in directory:
                contrainte = outputProd.split(" ")[0]
                outputProd = " " .join(outputProd.split(" ")[1:])
                outputRef = " " .join(outputRef.split(" ")[1:])


            if meaningRef in setCommun:

                inputRef = inputRef.strip()
                outputProd = outputProd.strip()
                outputRef = outputRef.strip()
                if meaningRef not in count.keys():
                    count[meaningRef] = 0
                count[meaningRef] += 1
                localCount += 1
                if directory not in directory_count.keys():
                    directory_count[directory] = 0
                directory_count[directory] += 1

                virtual_meanings.append(meaningRef)
                virtual_inputs.append(inputRef)
                virtual_outputs.append(outputRef)
                virtual_relex.append(relexRef)
                virtual_prod.append(outputProd)
                virtual_source.append(directory)

                relex_output = outputProd
                waited_outpout = outputRef

                # ça c'est pour éviter que des écarts d'encodage objets/sujets séparent des phrases identiques
                output_key = []
                for elt in outputProd.split():
                    if "objet" not in elt.lower() and "sujet" not in elt.lower():
                        output_key.append(elt)
                output_key = " ".join(output_key)

                if meaningRef not in relex_meaning.keys():
                    relex_meaning[meaningRef] = relexRef

                relex_output = outputProd[:]
                waited_outpout = outputRef[:]
                init_relex_output = outputProd[:]
                init_waited_outpout = outputRef[:]

                referenceLex = relex_meaning[meaningRef]

                original = relex_output[:]
                original_waited_outpout = waited_outpout[:]

                if not "base_line" in directory:
                    inputRefRelex = inputRef.split()[1:]
                    init_relex_input = inputRef.split()[1:]
                else:
                    inputRefRelex = inputRef.split()
                    init_relex_input = inputRef.split()

                triplet = ""
                for data_mod in ("simplification", "expansion"):
                    if data_mod in directory:
                        triplet = "/".join(inputRefRelex[0:3])
                        inputRefRelex = inputRefRelex[3:]

                if "data_2_text" in directory:
                    inputRefRelex = "/".join(inputRefRelex)
                else:
                    inputRefRelex = " ".join(inputRefRelex)

                meaningRefRelex = '\large{\\begin{tabbing}$M$ : \{\= (' + "),\\\\\n\\>(".join(
                    unidecode.unidecode(meaningRef.replace("/", " , ")).replace("&", "\&").split(".")) + \
                                  ") \}\end{tabbing}}"

                init_triplet = triplet[:]
                meaningRef1 = meaningRef[:]
                for lex_key in referenceLex.keys():
                    meaningRef1 = meaningRef1.replace(lex_key, referenceLex[lex_key])
                    triplet = triplet.replace(lex_key, referenceLex[lex_key])
                    relex_output = relex_output.replace(lex_key, referenceLex[lex_key])
                    inputRefRelex = inputRefRelex.replace(lex_key, referenceLex[lex_key])
                    meaningRefRelex = meaningRefRelex.replace(lex_key, referenceLex[lex_key])
                    waited_outpout = waited_outpout.replace(lex_key, referenceLex[lex_key])

                init_relex_input = " ".join(init_relex_input[3:])
                for lex_key in relexRef.keys():
                    init_triplet = init_triplet.replace(lex_key, relexRef[lex_key])
                    init_relex_output = init_relex_output.replace(lex_key, relexRef[lex_key])
                    init_waited_outpout = init_waited_outpout.replace(lex_key, relexRef[lex_key])
                    init_relex_input = init_relex_input.replace(lex_key, relexRef[lex_key])

                pref = "1"
                for each_larg_ref in meaningRef1.replace("/", " , ").split("."):
                    if len(each_larg_ref) > 62 or "located" in relex_output or meaningRef1.count("/") == 2:
                        pref = "0"
                        break
                meaningRefRelex = pref + meaningRefRelex

                take = True
                # on ne prend dans le corpus d'annotation que les phrases qui passent un premier niveau de fouille d'erreur:
                if "objet" in relex_output.lower() or "sujet" in relex_output.lower():
                    if "objet" in init_relex_output.lower() or "sujet" in init_relex_output.lower():
                        take = False

                if "objet" in init_relex_output.lower() or "sujet" in init_relex_output.lower():
                    take = False

                if take:

                    relex_output = relex_output.replace("_", " ")

                    if "mode_flux_corrected" in directory:
                        k_input = inputRefRelex.split(" ")[0]
                        inputRefRelex = " ".join(inputRefRelex.split(" ")[1:])
                        inputRefRelex = inputRefRelex.replace("_", "\_")
                    else:
                        inputRefRelex = inputRefRelex.replace("_", " ")
                    waited_outpout = waited_outpout.replace("_", " ")

                    init_relex_output = init_relex_output.replace("_", " ")
                    init_waited_outpout = init_waited_outpout.replace("_", " ")

                    source = ""

                    if "text_2_text" in directory:
                        source = "text_2_text"
                    elif "data_2_text" in directory:
                        source = "data_2_text"
                    elif "simplification" in directory:
                        source = "simplification"
                    else:
                        source = "extension"

                    if all:
                        clé_modèle = "all"
                    else:
                        clé_modèle = modèles[i_dir]
                    if clé_modèle not in modeles_2_csv.keys():
                        modeles_2_csv[clé_modèle] = {}
                    meaningRefRelex = meaningRefRelex.replace("_", " ")
                    if not meaningRefRelex in modeles_2_csv[clé_modèle].keys():
                        modeles_2_csv[clé_modèle][meaningRefRelex] = {'k':[], 'M':[], 'm':[], 'T\'M_{self relex}':[], 'T':[], 'T\'':[]}
                        if "mode_flux_corrected" in directory:
                            modeles_2_csv[clé_modèle][meaningRefRelex]['W'] = []
                            modeles_2_csv[clé_modèle][meaningRefRelex]['k\''] = []

                    if "mode_flux_corrected" in directory:
                        contrainte = shared.nettoie(contrainte)
                    elif not "base_line" in directory:
                        contrainte = shared.nettoie(inputRef.split(" ")[0])

                    else:
                        contrainte = ""

                    cle_unicisation = clé_modèle+meaningRefRelex+contrainte+relex_output

                    if "mode_flux_corrected" in directory:
                        key_mean = " ".join(sorted(inputRefRelex.split(" ")))
                        cle_unicisation = clé_modèle + key_mean + contrainte + relex_output
                        if "objet" in relex_output.lower() or "sujet" in relex_output.lower() or  "objet" in inputRefRelex.lower() or "sujet" in inputRefRelex.lower():
                            seens.add(cle_unicisation)


                    if not cle_unicisation in seens:
                        seens.add(cle_unicisation)
                        modeles_2_csv[clé_modèle][meaningRefRelex]['k'].append("")
                        modeles_2_csv[clé_modèle][meaningRefRelex]['M'].append("")
                        modeles_2_csv[clé_modèle][meaningRefRelex]['m'].append("")
                        modeles_2_csv[clé_modèle][meaningRefRelex]['T\'M_{self relex}'].append("")
                        modeles_2_csv[clé_modèle][meaningRefRelex]['T'].append("")
                        modeles_2_csv[clé_modèle][meaningRefRelex]['T\''].append("")
                        if "mode_flux_corrected" in directory:
                            modeles_2_csv[clé_modèle][meaningRefRelex]['W'].append("")
                            modeles_2_csv[clé_modèle][meaningRefRelex]['k\''].append("")

                        if baselined2t or baselinet2t:
                            if oldRef != inputRefRelex:
                                inputRefCount = 1
                                oldRef = inputRefRelex
                            else:
                                inputRefCount += 1
                                inputRefRelex += " \\textit{(" + inputRefCount.__str__() + ")}"

                        modeles_2_csv[clé_modèle][meaningRefRelex]['k'][-1] = contrainte
                        if not "data_2_text" in directory:
                            modeles_2_csv[clé_modèle][meaningRefRelex]['T\''][-1] = " ".join(detokenizer.detokenize(inputRefRelex.split(" ")))

                        modeles_2_csv[clé_modèle][meaningRefRelex]['T'][-1] = " ".join(detokenizer.detokenize(relex_output.split(" ")))
                        if "mode_flux_corrected" in directory:
                            modeles_2_csv[clé_modèle][meaningRefRelex]['W'][-1] = " ".join(detokenizer.detokenize(waited_outpout.split(" ")))
                            modeles_2_csv[clé_modèle][meaningRefRelex]['k\''][-1]  = shared.nettoie(contrainte)
                            modeles_2_csv[clé_modèle][meaningRefRelex]['k'][-1]  = shared.nettoie(k_input)
                        if "objet" in relex_output.lower() or "sujet" in relex_output.lower() or  "objet" in inputRefRelex.lower() or "sujet" in inputRefRelex.lower():
                            makeInitial = False
                            if "objet" in relex_output.lower() or "sujet" in relex_output.lower():
                                add = ""
                                if ("objet" in inputRefRelex.lower() or "sujet" in inputRefRelex.lower()):
                                    if modeles_2_csv[clé_modèle][meaningRefRelex]['T\''] != "":
                                        add +=  "~\\\\~\n$T\'_{self relex}$ : "+ " ".join(detokenizer.detokenize(init_relex_input.replace("_", " ").split(" ")))
                                add += "~\\\\~\n$T_{self relex}$ : " + " ".join(detokenizer.detokenize(init_relex_output.split(" ")))


                            else:
                                if modeles_2_csv[clé_modèle][meaningRefRelex]['T\''] != "":
                                    add =  "~\\\\~\n$T\'_{self relex}$ : "+ " ".join(detokenizer.detokenize(init_relex_input.replace("_", " ").split(" ")))
                            if "mode_flux_corrected" in directory:
                                modeles_2_csv[clé_modèle][meaningRefRelex]['W'][-1] =  " ".join(detokenizer.detokenize(init_waited_outpout.split(" ")))
                            if not "data_2_text" in directory:
                                makeInitial = True
                            elif "objet" in relex_output.lower() or "sujet" in relex_output.lower():
                                makeInitial = True
                            if makeInitial:
                                modeles_2_csv[clé_modèle][meaningRefRelex]['T'][-1] += add
                                modeles_2_csv[clé_modèle][meaningRefRelex]['T\'M_{self relex}'][-1] = \
                                    "& " + meaningRefCpy.replace("&", "\&").replace(".", " \\\\ & ").replace("_", "\_")

                        modeles_2_csv[clé_modèle][meaningRefRelex]["m"][-1] = triplet.replace("_", "\_")

        print(directory)
        print("Nombre d'exemples pour le modèle :", end=" ")
        print(localCount)
        #    print("Nombre d'exemples pour le modèle par meaning du set commun :")
        #    pprint(count)
        print()

    print("Nombre d'exemples pour le modèle pour le set commun :")
    pprint(directory_count)
    total = 0
    total = math.fsum(k for k in directory_count.values())

    print(total)

    for k in directory_count.keys():
        print(k, end=" : ")
        print(directory_count[k] / total)

    sets = []

    for modèle in modeles_2_csv.keys():
        sets.append(set())
        for meaningRef in modeles_2_csv[modèle].keys():
            sets[-1].add(meaningRef)
    setCommun = set.intersection(*map(set, sets))
    meaningsKeep = list(setCommun)
    countLittle = 0
    for meaningKeep in meaningsKeep:
        if meaningKeep[0] == "1":
            countLittle += 1
    print("count little", end=" : ")
    print(countLittle)
    if countLittle >= args.taille:
        for meaningKeep in meaningsKeep[:]:
            if meaningKeep[0] == "0":
                meaningsKeep.remove(meaningKeep)
    meaningsKeep = random.sample(meaningsKeep, args.taille)


    modele_sample_data = {}
    for modèle in modeles_2_csv.keys():
        print(modèle + " : construction des données.")
        file_annotation = modèle
        if baselined2t or baselinet2t:
            file_annotation = file_annotation + "$_{5best}$"
        elif d2t or t2t or ext or simp or all:
            file_annotation = file_annotation + "$_{syn}$"
        else:
            file_annotation = file_annotation + "$_{syn}$"
        modele_sample_data[file_annotation] = {}

        for meaningRef in modeles_2_csv[modèle].keys():
            if meaningRef in meaningsKeep and not isinstance(meaningRef, int):
                modele_sample_data[file_annotation][meaningRef] = modeles_2_csv[modèle][meaningRef]

    return modele_sample_data

annexes = []

if not args.debug and os.path.isfile("all/annexes" + args.taille.__str__() + ".json"):
    with open("all/annexes" + args.taille.__str__() + ".json", 'r') as f:
        annexes = json.load(f)
        f.close()
else:
    if not args.debug:
        annexes.append(getAnnexe(baselinet2t=True))
        annexes.append(getAnnexe(baselined2t=True))
    annexes.append(getAnnexe(shuffle=True))
    if not args.debug:
        annexes.append(getAnnexe(simp=True))
        annexes.append(getAnnexe(ext=True))
        annexes.append(getAnnexe(t2t=True))
        annexes.append(getAnnexe(d2t=True))
        annexes.append(getAnnexe(all=True))
    if not args.debug:
        with open("all/annexes" + args.taille.__str__() + ".json", "w") as fjson:
            json.dump(annexes, fjson, ensure_ascii=False, indent=2)

sep = ""
print("""
\documentclass[10pt,a4paper]{article}
\\usepackage[fleqn]{amsmath}
\\usepackage{amsfonts}
\\usepackage{amssymb}
\\usepackage[utf8x]{inputenc}
\\usepackage[left=1cm,right=1cm,top=1.5cm,bottom=1.5cm]{geometry}
\\usepackage[bookmarks={true},bookmarksopen={true}]{hyperref}
\setlength{\mathindent}{0cm}
\\begin{document}
""")

for annexe in annexes:
    for clé_modèle in annexe.keys():
        print(sep + "\section{TASK : "+ clé_modèle + "}")
        sep ="\\newpage"
        sep ="\n"
        for meaningRefRelex in annexe[clé_modèle].keys():
            print("\\rule{3cm}{1pt}\\textbf{" + meaningRefRelex[1:].replace("&", "\&") + "}\\noindent")
            if args.debug and not args.only_one:
                print(len(annexe[clé_modèle][meaningRefRelex]['k']), end=" generated relexicalisable text(s).~\\\\~")

            if 'W' in annexe[clé_modèle][meaningRefRelex].keys():
                print("Waited :")
                print("\\begin{itemize}")
                for value in set(annexe[clé_modèle][meaningRefRelex]['W']):
                    print("\item ", end=value.replace("&", "\&").replace("$", "\$"))
                print("\end{itemize}")

            for i in range(len(annexe[clé_modèle][meaningRefRelex]['k'])):
                if 'k\'' in annexe[clé_modèle][meaningRefRelex].keys():
                    if annexe[clé_modèle][meaningRefRelex]['k\''] == annexe[clé_modèle][meaningRefRelex]['k']:
                        del annexe[clé_modèle][meaningRefRelex]['k\'']
                        annexe[clé_modèle][meaningRefRelex]['k$ and $k\''] = annexe[clé_modèle][meaningRefRelex]['k']
                        del annexe[clé_modèle][meaningRefRelex]['k']
                for key in ['m', 'k$ and $k\'', 'k','k\'',  'T\'', 'M', 'T', 'T\'M_{self relex}', ]:
                    if key in annexe[clé_modèle][meaningRefRelex].keys():
                        out1 = key
                        out2 = unidecode.unidecode(annexe[clé_modèle][meaningRefRelex][key][i])
                        if key in ("k",'k\'', 'k$ and $k\''):
                            """
              "juxtaposition",
              "possessif",
                            """

                            # possessive relative, pied piping​ (eg "of which")​
                            # prepositional object, ditransitive clause, predicative clause​ ( = existentiel)​
                            out2 = out2.replace("coordinated full clauses", "Sentence Coordination")\
                                .replace("coordinated clauses","VP coordination")\
                                .replace("direct object","Transitive clause")\
                                .replace("existential","predicative clause​")\
                                .replace("relative object","object relative")\
                                .replace("relative adverb","Adverbe Relative")\
                                .replace("relative subject","subject relative")
                            out2 = out2.split()
                            for i_piece, piece in enumerate(out2):
                                out2[i_piece] = out2[i_piece][0].upper() +out2[i_piece][1:]
                            out2 = " ".join(out2)

                        if out2 != "":
                            if clé_modèle != "all" and out1 != meaningRefRelex:
                                if key == 'T\'M_{self relex}':
                                    print("\\vspace{-30pt}\\begin{alignat*}{1}")
                                    print('\t\t' + out1.replace("&", "\&").replace("#", "\#"), end=" : ")
                                else:
                                    print('\t\t$' + out1.replace("&", "\&").replace("#", "\#") , end= "$ : ")


                            if key == 'T\'M_{self relex}':
                                print(out2  + "\end{alignat*}")
                            else:
                                print(out2.replace("&", "\&").replace("#", "\#") +"~\\\\~" , )
                        if key == "T":
                            print("~\\\\~")

            if args.only_one:
                break
print("""
\end{document}
""")