import argparse
import os
import json
import pprint
import re
from progress.bar import Bar
import sacrebleu
import sys
# import testContraintes, shared chargé plus bas

parser = argparse.ArgumentParser(description='Relexicalise un corpus généré.')
parser.add_argument('--input', metavar='-i', type=str, nargs='+',  dest="file_input", default='data_test_predictions.txt',
                   help='input file')
parser.add_argument('--output', metavar='-o', type=str, nargs='+', dest="file_output",  default='data_test_predictions',
                   help='output file, ne pas préciser d\'extension, sera relex pour relexicalisation, tsv pour tsv')
parser.add_argument('--lex', metavar='-l', dest="file_lexicalisation",
                   nargs = '+', default='_test.relex', type=str,
                   help='lexicalisations')
parser.add_argument('--ref_src', metavar='-c', dest="ref_src",
                   nargs = '?', default='inputs_test.data', type=str,
                   help='fichier des demandes originelles.')
parser.add_argument('--sacrebleu', action='store_true',
                   help='Calcul du bleu (sacrebleu)',)
parser.add_argument('--simplification', action='store_true',
                   help='Travail sur simplifications plutôt qu\'expansion.')
parser.add_argument('--log', action='store_true',
                   help='Correspond à verbose off : bascule les sorties dans un log (training results) et ne touche pas aux stats enregistrées.',)

parser.add_argument('--multi_sent', action='store_true',
                   help='Option activant le traitement des lexicalisations comportant plusieurs phrases.',)
parser.add_argument('--fusion', action='store_true',
                   help='Option activant la recherche de contrainte sur l\'élément partagé et sur l\'élément ajouté.',)
parser.add_argument('--invert_constraint', action='store_true',
                   help='Option faisant porter la recherche de contrainte sur l\'élément du triplet ajouté, et non support.',)
parser.add_argument('--theme', action='store_true',
                   help='Organise par thème d\'apprentissage ("city"), non implémenté')
parser.add_argument('--theme_split', action='store_true',
                   help='Organise par thème d\'apprentissage ("city") en arrière plan')
parser.add_argument('--shorted_function', action='store_true',
                   help='Raccourci la function appliquée à la sentence.',)
parser.add_argument('--constraint_in_output_or_in_input', action='store_true',
                   help='Avec cette option, on n\'exclut pas que la contrainte soit et dans l\'entrée, et dans la sortie.',)
parser.add_argument('--constraint_somewhere_in_output', action='store_true',
                   help='La contrainte peut ne pas etre liée au triplet en gestion, mais porter sur un autre élément dans la lexicalisation',)
parser.add_argument('--text_2_text', action='store_true',
                   help='Similaire à triple_2_sentence mais produit un corpus pour opennmt',)
parser.add_argument('--data_2_text', action='store_true',
                   help='Similaire à triple_2_sentence, mais produit un corpus pour opennmt avec en entrée les descriptions logiques',)
parser.add_argument('--remove_constrainst', action='store_true',
                   help='retrait contrainte on text_2_text',)
parser.add_argument('--text_2_text_restricted', action='store_true',
                   help='Nécessite text_2_text, restreint les paires à un différentiel d\'un sur les contrainte. ',)

args = parser.parse_args()

modData1 = False #premier système de colonnade pour le csv sorti

if args.text_2_text_restricted and not args.text_2_text:
    exit()

# il me faut des dossiers json/training_results/data
if args.text_2_text:
    if args.multi_sent:
        conteneur_apprentissage = "text_2_text/multi_sent"
        conteneur_construction_data = "text_2_text/multi_sent/json"
        conteneur_training_data = "text_2_text/multi_sent/training_results"
    else:
        conteneur_apprentissage = "text_2_text/simple"
        conteneur_construction_data = "text_2_text/simple/json"
        conteneur_training_data = "text_2_text/simple/training_results"
    if args.remove_constrainst:
        conteneur_apprentissage = conteneur_apprentissage.replace('text_2_text', "text_2_text/remove_constrainst")
        conteneur_construction_data = conteneur_construction_data.replace('text_2_text', "text_2_text/remove_constrainst")
        conteneur_training_data = conteneur_training_data.replace('text_2_text', "text_2_text/remove_constrainst")
    if args.fusion or args.invert_constraint:
        print("args.text_2_text and (args.fusion or args.invert_constraint)")
        exit(1)
elif args.data_2_text:
    if args.multi_sent:
        conteneur_apprentissage = "data_2_text/multi_sent"
        conteneur_construction_data = "data_2_text/multi_sent/json"
        conteneur_training_data = "data_2_text/multi_sent/training_results"
    else:
        conteneur_apprentissage = "data_2_text/simple"
        conteneur_construction_data = "data_2_text/simple/json"
        conteneur_training_data = "data_2_text/simple/training_results"
    if args.fusion or args.invert_constraint:
        print("args.data_2_text and (args.fusion or args.invert_constraint)")
        exit(1)
else:

    if not args.multi_sent and not args.invert_constraint and not args.fusion:
        # conteneur des données d'apprentissage
        conteneur_apprentissage = "expansion/simple"
        conteneur_construction_data = "expansion/simple/json"
        conteneur_training_data = "expansion/simple/training_results"
    elif args.multi_sent and not args.invert_constraint and not args.fusion:
        # conteneur des données d'apprentissage
        conteneur_apprentissage = "expansion/multi_sent"
        conteneur_construction_data = "expansion/multi_sent/json"
        conteneur_training_data = "expansion/multi_sent/training_results"
    elif not args.multi_sent and args.invert_constraint and not args.fusion:
        # conteneur des données d'apprentissage
        conteneur_apprentissage = "expansion/simple_invert_constraint"
        conteneur_construction_data = "expansion/simple_invert_constraint/json"
        conteneur_training_data = "expansion/simple_invert_constraint/training_results"
    elif args.multi_sent and args.invert_constraint and not args.fusion:
        # conteneur des données d'apprentissage
        conteneur_apprentissage = "expansion/multi_invert_constraint"
        conteneur_construction_data = "expansion/multi_invert_constraint/json"
        conteneur_training_data = "expansion/multi_invert_constraint/training_results"
    elif args.multi_sent and args.fusion:
        # conteneur des données d'apprentissage
        conteneur_apprentissage = "expansion/multi_sent_fusion"
        conteneur_construction_data = "expansion/multi_sent_fusion/json"
        conteneur_training_data = "expansion/multi_sent_fusion/training_results"
    elif not args.multi_sent and args.fusion:
        # conteneur des données d'apprentissage
        conteneur_apprentissage = "expansion/simple_fusion"
        conteneur_construction_data = "expansion/simple_fusion/json"
        conteneur_training_data = "expansion/simple_fusion/training_results"
    else:
        print(parser.parse_args())
        print("Situation non prévue, l'inversion du support des contraintes n'étant pas compatible avec une fusion... fusion toute contrainte porteuse est prise en compte.")
        exit(1)

    if args.simplification:
        conteneur_apprentissage = conteneur_apprentissage.replace("expansion/", "simplification/")
        conteneur_construction_data = conteneur_construction_data.replace("expansion/", "simplification/")
        conteneur_training_data = conteneur_training_data.replace("expansion/", "simplification/")

if args.text_2_text or args.data_2_text:
    if args.invert_constraint and not args.fusion:
        conteneur_apprentissage = conteneur_apprentissage.replace("2_text", "2_text/invert_constraint")
        conteneur_construction_data = conteneur_construction_data.replace("2_text", "2_text/invert_constraint")
        conteneur_training_data = conteneur_training_data.replace("2_text", "2_text/invert_constraint")
    elif args.fusion:
        conteneur_apprentissage = conteneur_apprentissage.replace("2_text", "2_text/fusion")
        conteneur_construction_data = conteneur_construction_data.replace("2_text", "2_text/fusion")
        conteneur_training_data = conteneur_training_data.replace("2_text", "2_text/fusion")
    if args.invert_constraint and  args.fusion:
        print("(args.text_2_text or args.data_2_text) and (args.fusion and args.invert_constraint)")
        exit(5)

if args.theme or args.theme_split:
    conteneur_apprentissage = "themes/" + conteneur_apprentissage
    conteneur_construction_data = "themes/" + conteneur_construction_data
    conteneur_training_data = "themes/" + conteneur_training_data

    if args.theme_split:
        conteneur_apprentissage = conteneur_apprentissage.replace("themes", "themes/theme_split")
        conteneur_construction_data = conteneur_construction_data.replace("themes", "themes/theme_split")
        conteneur_training_data = conteneur_training_data.replace("themes", "themes/theme_split")
    else:
        conteneur_apprentissage = conteneur_apprentissage.replace("themes", "themes/theme_non_split")
        conteneur_construction_data = conteneur_construction_data.replace("themes", "themes/theme_non_split")
        conteneur_training_data = conteneur_training_data.replace("themes", "themes/theme_non_split")
    if args.text_2_text or args.data_2_text:
        # todo implémenter
        print("(args.text_2_text or args.data_2_text) and (args.theme_split)")
        exit(1)

if args.shorted_function:
    conteneur_apprentissage = "shorted/" + conteneur_apprentissage
    conteneur_construction_data = "shorted/" + conteneur_construction_data
    conteneur_training_data = "shorted/" + conteneur_training_data

if args.constraint_somewhere_in_output:
    conteneur_apprentissage = "diff_sentences/" + conteneur_apprentissage
    conteneur_construction_data = "diff_sentences/" + conteneur_construction_data
    conteneur_training_data = "diff_sentences/" + conteneur_training_data
    args.constraint_in_output_or_in_input
elif args.constraint_in_output_or_in_input:
    conteneur_apprentissage = "constraint/input_output/" + conteneur_apprentissage
    conteneur_construction_data = "constraint/input_output/" + conteneur_construction_data
    conteneur_training_data = "constraint/input_output/" + conteneur_training_data
else:
    conteneur_apprentissage = "constraint/in_output/" + conteneur_apprentissage
    conteneur_construction_data = "constraint/in_output/" + conteneur_construction_data
    conteneur_training_data = "constraint/in_output/" + conteneur_training_data


if args.text_2_text_restricted:
    conteneur_apprentissage = "text_2_text_restricted/" + conteneur_apprentissage
    conteneur_construction_data = "text_2_text_restricted/" + conteneur_construction_data
    conteneur_training_data = "text_2_text_restricted/" + conteneur_training_data

if conteneur_apprentissage.startswith('constraint/in_output/data_2_text/fusion'):
    exit(1) # traité par le everywhere

if conteneur_apprentissage.startswith('constraint/in_output/data_2_text/invert_constraint'):
    exit(1)  # traité par le everywhere

if conteneur_apprentissage.startswith('constraint/in_output/text_2_text/fusion'):
    exit(1) # traité par le everywhere

if conteneur_apprentissage.startswith('constraint/in_output/text_2_text/invert_constraint'):
    exit(1)  # traité par le everywhere

if conteneur_apprentissage.startswith('diff_sentences/data_2_text/'):
    exit(1)  # traité par le everywhere

if conteneur_apprentissage.startswith('diff_sentences/text_2_text/'):
    exit(1) # traité par le everywhere

if not os.path.isfile(conteneur_training_data + "/data_test_predictions.txt"):
    print(conteneur_construction_data)
    print(conteneur_training_data + " inexistant.")
    exit(1)

if not os.path.isfile(conteneur_training_data + "/../outputs_test.data"):
    print(conteneur_training_data + " : données de références inexistantes.")
    exit(1)

b_nature = True
if "/" not in args.ref_src:
    args.ref_src = conteneur_apprentissage + "/" + args.ref_src
else:
    b_nature = False
if "/" not in args.file_lexicalisation:
    args.file_lexicalisation =  conteneur_apprentissage + "/" + args.file_lexicalisation
if "/" not in args.file_input:
    args.file_input = conteneur_training_data + "/" +  args.file_input
if "/" not in args.file_output:
    args.file_output = conteneur_training_data + "/" +   args.file_output

if os.path.isfile(conteneur_training_data + "/" +'analyses.log'):
    os.remove(conteneur_training_data + "/" +'analyses.log')

if not os.path.isfile(args.file_input):
    print(args.file_input + " inexistant.")
    exit(1)

if not os.path.isfile(args.file_lexicalisation):
    print(args.file_lexicalisation + " inexistant.")
    exit(1)

if not os.path.isfile(args.ref_src):
    print(args.ref_src + " inexistant.")
    exit(1)

with open(args.file_input) as f:
    read_data = f.readlines()

with open(args.ref_src) as f:
    read_data_input = f.readlines()

if os.path.isfile(args.ref_src.replace("inputs", "outputs")):
    with open(args.ref_src.replace("inputs", "outputs")) as f:
        read_data_output = f.readlines()
else:
    read_data_output = [] * len(read_data_input)

with open(args.file_lexicalisation, 'r') as f:
    json_data = json.load(f)

if os.path.isfile("store_constraints.json"):
    with open("store_constraints.json", 'r') as f:
        store = json.load(f)
else:
    store = {}
len_store = len(store.keys())

if len(json_data)<len(read_data):
    print("Fichier de lexicalisations moins long que données générées. Ceci est considéré comme une erreur.")
    print(parser.parse_args())
    exit(2)

if len(json_data)<len(read_data_input):
    print("Fichier de lexicalisations moins long que données d'entrées. Ceci est considéré comme une erreur.")
    print(parser.parse_args())
    exit(2)

if len(read_data_input)<len(read_data):
    print("Fichier de données d'entrée moins long que données générées. Ceci est considéré comme une erreur.")
    print(parser.parse_args())
    exit(2)

import testContraintes, shared

outputs = []
inputs_src = []

i = 0
g_objetsTrouvés = 0
g_objetsNonTrouvés = 0

g_contraintesRéalisées = 0
g_non_réalisée = 0
g_contrainte_parent = 0
g_contrainte_élément = 0

taille_corpus = ""
taille_train = ""
taille_test = ""
taille_dev = ""

g_matrice_confusion = {}

print("Taille des corpus : ")
import subprocess
p = subprocess.Popen([" wc -l " + conteneur_apprentissage + "/i*.data"], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
for line in p.stdout.readlines():
    print('\t- ', end='')
    tmp = line.decode("utf-8").strip()
    print(tmp)
    if "total" in tmp :
        taille_corpus = re.findall("\d+", tmp)[0]
    elif "_train" in tmp :
        taille_train = re.findall("\d+", tmp)[0]
    elif "_test" in tmp :
        taille_test = re.findall("\d+", tmp)[0]
    elif "_valid" in tmp :
        taille_dev = re.findall("\d+", tmp)[0]

retval = p.wait()

if modData1 :

    for_matrice_correlation = [
        "conteneur_training_data,theme,diff_sent,multi_sent,ajout_contrainte,input,action,contrainte,shuffled,tailleCorpus,tailleTrain,tailleDev,tailleTest,quartileCorpus,constrainst_only_linked_to_triple,constrainst_never_in_input," +
        "contrainteSentence,contrainteFind,objetFind,bleu,tauxContrainteTrouve,tauxObjetTrouve,tauxObjEtContrainte"]

    basic_values = ""

    if "shorted" in conteneur_construction_data:
        basic_values += "shorted,"
    elif "data_2_text"in conteneur_construction_data or "text_2_text"in conteneur_construction_data:
        basic_values += "na,"
    else:
        basic_values += "long,"

    remove_contrainte = False

    if "expansion" in conteneur_construction_data:
        basic_values += "expansion,"
    elif "data_2_text"in conteneur_construction_data:
        basic_values += "data_2_text,"
    elif "text_2_text"in conteneur_construction_data:
        basic_values += "text_2_text,"
    else:
        basic_values += "simplification,"
        remove_contrainte = True
    if args.remove_constrainst:
        remove_contrainte = True

    if remove_contrainte:
        basic_values = "no," + basic_values
    else:
        basic_values = "yes," + basic_values

    if args.multi_sent:
        basic_values = "yes," + basic_values
    else:
        basic_values = "no," + basic_values

    if args.constraint_in_output_or_in_input:
        basic_values += "restricted-"
    if args.constraint_somewhere_in_output:
        basic_values += "everywhere,"
    elif "fusion" in conteneur_construction_data:
        basic_values += "fusion,"
    elif "invert_constraint" in conteneur_construction_data:
        basic_values += "indirecte,"
    else:
        basic_values += "direct,"

    if "themes" in conteneur_construction_data:
        basic_values += "yes,"
    else:
        basic_values += "no,"

    basic_values += taille_corpus + ","
    basic_values += taille_train + ","
    basic_values += taille_dev + ","
    basic_values += taille_test + ","

    if  int(taille_corpus) <= 14607:
        basic_values += "first,"
    elif int(taille_corpus) <= 27770 :
        basic_values += "second,"
    elif int(taille_corpus) <= 49602.25:
        basic_values += "third,"
    else:
        basic_values += "fourth,"

    if args.constraint_somewhere_in_output:
        # constrainst_only_linked_to_triple
        basic_values += "no,"
    else:
        basic_values += "yes,"

    if args.constraint_in_output_or_in_input:
        # constrainst_never_in_input
        basic_values += "no,"
    else:
        basic_values += "yes,"

else:


    for_matrice_correlation = [
        "modele,declinaison_modele,declinaison_recherche_contrainte,restriction_contrainte_input,multi_sent,entree_simplifiee,repartition_data_theme,taille_corpus,train,dev,test,classementArbitraireTaille," +
        "contrainteSentence,contrainteFind,objetFind,bleu,tauxContrainteTrouve,tauxObjetTrouve,tauxObjEtContrainte"]


    modèle = ""
    déclinaison_modèle = "NA"
    if args.text_2_text:
        modèle = "text_2_text"
        if args.data_2_text or args.simplification:
            print('modèle in ("simplification") and args.text_2_text  ')
            exit(1)
        if args.remove_constrainst:
            déclinaison_modèle = "ajout"
        else:
            déclinaison_modèle = "retrait"

    elif args.data_2_text:
        modèle = "data_2_text"
        if args.simplification:
            print('modèle in ("simplification") and args.data_2_text  ')
            exit(2)
        if args.remove_constrainst:
            déclinaison_modèle = "ajout"
        else:
            déclinaison_modèle = "retrait"
    else:
        modèle = "modification"
        if args.simplification:
            déclinaison_modèle = "simplification"
        else:
            déclinaison_modèle = "expansion"




    déclinaison_recherche_contrainte = "NA"
    if déclinaison_modèle in ("simplification", "expansion"):
        if args.invert_constraint:
            déclinaison_recherche_contrainte = "support"
            if args.fusion:
                print('args.invert_constraint and args.fusion and modèle in ("simplification", "expansion")')
                exit(3)
        elif args.fusion:
            déclinaison_recherche_contrainte = "support_ou_objet"
        elif args.constraint_somewhere_in_output:
            déclinaison_recherche_contrainte = "everywhere"
        else:
            déclinaison_recherche_contrainte = "objet"
    if déclinaison_recherche_contrainte == "NA":
        if args.invert_constraint or args.fusion:
            print('(args.invert_constraint or args.fusion ) and not modèle in ("simplification", "expansion")')
            exit(4)
        déclinaison_recherche_contrainte = "everywhere"
        if args.constraint_somewhere_in_output:
            print('modèle not in ("simplification", "expansion"), contrainte everywhere, nul besoin de préciser')
            exit(5)
    elif args.constraint_somewhere_in_output and (not déclinaison_modèle in ("simplification", "expansion") or (déclinaison_modèle in ("simplification", "expansion") and déclinaison_recherche_contrainte != "everywhere") ):
        print('mais que fait-on ici... ?')
        exit(5)

    restriction_contrainte_input = "no"
    if args.constraint_in_output_or_in_input:
        restriction_contrainte_input = "yes"

    multi_sent = "no"
    if args.multi_sent:
        multi_sent = "yes"

    entrée_simplifiée = "no"
    if args.shorted_function:
        entrée_simplifiée = "yes"
        if déclinaison_modèle not in ("simplification", "expansion"):
            print('args.shorted_function and modèle not in ("simplification", "expansion")')
            exit(6)

    repartition_data_theme = "no"
    if args.theme_split:
        repartition_data_theme = "yes"
        if déclinaison_modèle not in ("simplification", "expansion"):
            print('args.theme_split and modèle not in ("simplification", "expansion")')
            exit(7) #non implémenté, inutile de chercher/..

    basic_values = ",".join((modèle, déclinaison_modèle, déclinaison_recherche_contrainte,restriction_contrainte_input,multi_sent,entrée_simplifiée,repartition_data_theme)) + ","


    basic_values += taille_corpus + ","
    basic_values += taille_train + ","
    basic_values += taille_dev + ","
    basic_values += taille_test + ","

    if  int(taille_corpus) <= 14607:
        basic_values += "first,"
    elif int(taille_corpus) <= 27770 :
        basic_values += "second,"
    elif int(taille_corpus) <= 49602.25:
        basic_values += "third,"
    else:
        basic_values += "fourth,"




if args.log:
    f= open(conteneur_training_data + "/" +'analyses.log', 'w')
    # We redirect the 'sys.stdout' command towards the descriptor file
    sys.stdout = f
else:
    print(args.file_input + " en traitement.")


print("dossiers de travail :")
print('\tconteneur apprentissage : ' + conteneur_apprentissage)
print('\tconteneur training_data : ' + conteneur_training_data)
print('fichiers générés : ' )
print('\tdonnées relexicalisées : ' + args.file_output + ".relex")
print('\tchiffres issus des traitements : ' + args.file_output + ".tsv")
 #print('\tdetails ' + for_matrice_correlation[0] + " : " + args.file_output + "_full.tsv")
if not  args.log:
    print("Activer --log pour sortir un log des analyses.")
print()

print("Taille des corpus : ")
import subprocess
p = subprocess.Popen([" wc -l " + conteneur_apprentissage + "/i*.data"], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
for line in p.stdout.readlines():
    print('\t- ', end='')
    tmp = line.decode("utf-8").strip()
    print(tmp)
    if "total" in tmp :
        taille_corpus = re.findall("\d+", tmp)[0]
    elif "_train" in tmp :
        taille_train = re.findall("\d+", tmp)[0]
    elif "_test" in tmp :
        taille_test = re.findall("\d+", tmp)[0]
    elif "_valid" in tmp :
        taille_dev = re.findall("\d+", tmp)[0]

bar = Bar('Processing', max=len(read_data))
#dones_entries = set()
for traduction_encodée, lexicalisation, demande, reference in zip(read_data, json_data[0:len(read_data)], read_data_input[0:len(read_data)], read_data_output[0:len(read_data)]):
    #if traduction_encodée + demande not in dones_entries:
        # on va analyser les entrées distinctes
        # dones_entries.add(traduction_encodée + demande) --> non, il faudrait en garder le meilleur BLEU
    if traduction_encodée.strip() != "":
        traduction_encodée = traduction_encodée.strip()
        outputs.append(traduction_encodée)
        demande  = demande.strip().split(" ")
        forme_syntaxique = demande.pop(0)

        triplet_input = demande[0:3]
        objetsRecherchés = triplet_input[:]
        objetsRecherchés.pop(1)

        objetsTrouvés = 0
        objetsNonTrouvés = 0


        #on va aller regarder dans l'analyse stanford (indexée de 1 à n) si on a la contrainte attendue au bon endroit.
        if "data_2_text" in conteneur_construction_data or "text_2_text" in conteneur_construction_data:
            triplet_input = ""
            initial_sentence = demande[:]
            demande = " ".join(demande)
            found_objet = False
        else:
            initial_sentence = demande[3:]

        objet_recherché  = ""
        objet_non_recherché  = ""

        if triplet_input != "" :
            for objet in objetsRecherchés:
                if objet in outputs[-1].split(" ") and objet not in initial_sentence:
                    objetsTrouvés += 1
                    for iObj, obj in enumerate(outputs[-1].split(" ")):
                        if obj == objet:
                            objet_recherché = objet
                elif objet not in initial_sentence:
                    objetsNonTrouvés += 1
                else:
                    for iObj, obj in enumerate(outputs[-1].split(" ")):
                        if obj == objet:
                            objet_non_recherché = objet

            found_objet = False
            if objet_recherché != "":
                g_objetsTrouvés += 1
                found_objet = True
            else:
                g_objetsNonTrouvés += 1

            demande = " ".join(demande[3:])
        triplet_input = " ".join(triplet_input)

        hyp = traduction_encodée.split(" ")
        reference_lex = reference[:].strip()
        ref = reference.strip().split(" ")
        for lex in lexicalisation.keys():
            if lex == objet_recherché:
                objet_recherché = lexicalisation[lex].replace("_", " ")
            if lex == objet_non_recherché:
                objet_non_recherché = lexicalisation[lex].replace("_", " ")
            outputs[-1] = outputs[-1].replace(lex, lexicalisation[lex])
            demande = demande.replace(lex, lexicalisation[lex])
            reference_lex = reference_lex.replace(lex, lexicalisation[lex])
            triplet_input = triplet_input.replace(lex, lexicalisation[lex])
        demande = demande.split(" ")

        contrainteRéalisée = 0
        contrainte_élément = 0
        contrainte_parent = 0

        indexes_token_objet = []
        tmpS = outputs[-1].replace("_", " ").split()

        objet = objet_recherché.split(' ')
        for iS in range(len(tmpS)):
            index_token_objet = []
            for iO in range(len(objet)):
                if tmpS[iS] == objet[iO]:
                    # le stanford parser nous rend les index en string, on convertit pour rendre comparable
                    index_token_objet.append((iS + 1).__str__())
                    iS += 1
                else:
                    index_token_objet.clear()
                    break
            if len(index_token_objet) == len(objet):
                indexes_token_objet += index_token_objet

        indexes_token_objet_non_recherché = []
        tmpS = outputs[-1].replace("_", " ").split()

        objet = objet_non_recherché.split(' ')
        for iS in range(len(tmpS)):
            index_token_objet = []
            for iO in range(len(objet)):
                if tmpS[iS] == objet[iO]:
                    # le stanford parser nous rend les index en string, on convertit pour rendre comparable
                    index_token_objet.append((iS + 1).__str__())
                    iS += 1
                else:
                    index_token_objet.clear()
                    break
            if len(index_token_objet) == len(objet):
                indexes_token_objet_non_recherché += index_token_objet


        phrase_analysée = outputs[-1].replace("_", " ")
        if phrase_analysée in store.keys():
            contraintes = store[phrase_analysée]
        else:
            contraintes = testContraintes.getConstraintes(outputs[-1].replace("_", " "))
            contraintes2  = testContraintes.getConstraintes(" ".join(hyp))
            for contrainte in contraintes2.keys():
                if contrainte not in contraintes.keys():
                    contraintes[contrainte] = contraintes2[contrainte]
                # on ajoute pas d'informations sur les contraintes déjà présentes, d'abord on l'a déjà référencée, peut-être pour autre chose, mais les tokens seraient à recalculer...

            store[phrase_analysée] = contraintes
        for contrainte in contraintes.keys():
            if forme_syntaxique == shared.nettoie(contrainte).replace(" ", "_"):
                contrainteRéalisée = 1
                if len(indexes_token_objet) > 0:
                    indexes_tokens_contraintes = []
                    for elt in contraintes[contrainte]:
                        indexes_tokens_contraintes += elt.values()
                    if not set(indexes_tokens_contraintes).isdisjoint(set(indexes_token_objet)):
                        contrainte_élément += 1
                    if not set(indexes_tokens_contraintes).isdisjoint(set(indexes_token_objet_non_recherché)):
                        contrainte_parent += 1

        if contrainteRéalisée == 0:
            g_non_réalisée += 1

        if forme_syntaxique not in g_matrice_confusion.keys():
            g_matrice_confusion[forme_syntaxique] = {'identifié':0, 'non identifié':0, 'objet trouvé':0, 'objet non trouvé':0, 'objet et contrainte':0,
                                                     'bleu':[], 'objet et contrainte bleu':[], 'lost bleu':[]}

        g_matrice_confusion[forme_syntaxique]['identifié'] += 1 if contrainteRéalisée > 0 else 0
        g_matrice_confusion[forme_syntaxique]['non identifié'] += 0 if contrainteRéalisée > 0 else 1
        g_matrice_confusion[forme_syntaxique]['objet trouvé'] += 1 if found_objet else 0
        g_matrice_confusion[forme_syntaxique]['objet non trouvé'] += 0 if found_objet else 1

        score = sacrebleu.corpus_bleu(" ".join(hyp), " ".join(ref)).score
        g_matrice_confusion[forme_syntaxique]['bleu'].append(score)
        specific_values = forme_syntaxique + ","
        specific_values +=  contrainteRéalisée.__str__() + ","
        specific_values += "1," if found_objet else "0,"
        specific_values += score.__str__()

        if contrainteRéalisée > 0 and found_objet:
            g_matrice_confusion[forme_syntaxique]['objet et contrainte'] += 1
            g_matrice_confusion[forme_syntaxique]['objet et contrainte bleu'].append(score)
        else:
            g_matrice_confusion[forme_syntaxique]['lost bleu'].append(score)

        g_contraintesRéalisées += contrainteRéalisée
        g_contrainte_élément += contrainte_élément
        g_contrainte_parent += contrainte_parent

        inputs_src.append(score.__str__() + "\t" + contrainteRéalisée.__str__()   + "\t" + contrainte_parent.__str__()   + "\t" + contrainte_élément.__str__()   + "\t" + objetsTrouvés.__str__()  + "\t" + forme_syntaxique.replace('"', "'") + "\t" + triplet_input.replace('"', "'") + "\t" + " ".join(demande).replace('"', "'") + "\t" + outputs[-1].replace('"', "'") + "\t" + reference_lex.replace('"', "'"))
        i += 1
        for_matrice_correlation.append(basic_values + specific_values)
        bar.next()

bar.finish()
if len_store != len(store.keys()):
    with open("store_constraints.json", "w") as fjson:
        json.dump(store, fjson, ensure_ascii=False, indent=2)
    print("modification magasin d'analyse : sauvegarde effectuée")
if args.simplification:
    inputs_src.insert(0, "bleu\tcontrainte ajoutée\tcontrainte sur élément ajouté\tcontrainte sur élément support à l'élément ajouté\tinformation présente\tdemande\tinformation à ajouter\tlexicalisation à modifier\tlexicalisation obtenue\tlexicalisation attendue")
else:
    inputs_src.insert(0, "bleu\tcontrainte présente\tcontrainte sur élément\tcontrainte sur élément support à l'élément ajouté\tinformation supprimée\tdemande\tinformation à enlever\tlexicalisation à modifier\tlexicalisation obtenue\tlexicalisation attendue")
print(len(outputs), end = " traductions.\n")

fjson = open(args.file_output + ".relex", "w")
fjson.write(("\n".join(outputs)))
fjson.flush()
fjson.close()
while not fjson.closed:
    True
print(args.file_output + ".relex écrit.")

fjson = open(args.file_output + ".tsv", "w")
fjson.write(("\n".join(inputs_src)))
fjson.flush()
fjson.close()
while not fjson.closed:
    True
print(args.file_output + ".tsv écrit.")

print("Objets trouvés : ", end="")
print(g_objetsTrouvés)
print("Objets non trouvés : ", end="")
print(g_objetsNonTrouvés)

print("contraintes réalisées", end = " : ")
print(g_contraintesRéalisées)
print("contraintes non réalisées", end = " : ")
print(g_non_réalisée)
print("contraintes sur parent élément ajouté", end = " : ")
print(g_contrainte_parent)
print("contraintes sur élément ajouté", end = " : ")
print(g_contrainte_élément)

if args.sacrebleu:
    print("calcul du BLEU (sacrebleu) : ")
    if args.log:
        import subprocess
        p = subprocess.Popen(['sacrebleu --input ' + args.file_input + " " + args.ref_src.replace("inputs", "outputs") + " --force"], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in p.stdout.readlines():
            print(line.decode("utf-8").strip())
        retval = p.wait()
    else:
        os.system('sacrebleu --input ' + args.file_input + " " + args.ref_src.replace("inputs", "outputs") + " --force")

# table de confusion
print()
print(" *", end="*"*150)
print()
print(" * ", end = "")
print("forme syntaxique".ljust(25, " "), end=" * ")
print("contrainte".ljust(23, " "), end=" * ")
print("objet ajouté".ljust(23, " "), end=" * ")
print('objet et contrainte'.ljust(20, " "), end=" * ")
print("total".ljust(11, " "), end=" * ")
print("BLEU".ljust(30, " "), end=" * ")
print()
print(" * ", end = "")
print("".ljust(25, " "), end=" * ")
print("trouvée".ljust(9, " "), end=" * ")
print("non trouvée".ljust(9, " "), end=" * ")
print("trouvé".ljust(9, " "), end=" * ")
print("non trouvé".ljust(11, " "), end=" * ")
print("".ljust(20, " "), end=" * ")
print("".ljust(11, " "), end=" * ")
print("général".ljust(8, " "), end=" * ")
print("obj&c".ljust(8, " "), end=" * ")
print("non ok".ljust(8, " "), end=" * ")
print()
print(" *", end="*"*150)
print()
total_col = {}
total = 0

if args.simplification:
    print()
    print("Les données de contraintes sont données à titre purement informatif : la contrainte précisait quelle règle syntaxique liait l'élément à supprimer à son parent dans la lexicalisation originelle. Rien n'interdit à la contrainte de s'exprimer à nouveau ailleurs en sortie.")
    print()

for forme_syntaxique in sorted(list(g_matrice_confusion.keys())):
    print(" * ", end = "")
    print(forme_syntaxique.ljust(25, " "), end=" * ")
    g_matrice_confusion[forme_syntaxique]['total'] = 0
    for item in  ['identifié', 'non identifié', 'objet trouvé', 'objet non trouvé', 'objet et contrainte']:
        if "non" in item :
            endtab = 11
        else:
            endtab = 9
        if " et " in item:
            endtab = 20
        if item in ['identifié', 'non identifié' ]:
            g_matrice_confusion[forme_syntaxique]['total'] += g_matrice_confusion[forme_syntaxique][item]
            total += g_matrice_confusion[forme_syntaxique][item]
        if item not in total_col.keys():
            total_col[item] = 0
        total_col[item] += g_matrice_confusion[forme_syntaxique][item]
        print(g_matrice_confusion[forme_syntaxique][item].__str__().ljust(endtab, " "), end=" * ")
    print(g_matrice_confusion[forme_syntaxique]['total'].__str__().ljust(11, " "), end=" * ")
    if len(g_matrice_confusion[forme_syntaxique]['bleu']) > 0:
        print(round(sum(g_matrice_confusion[forme_syntaxique]['bleu']) / len(g_matrice_confusion[forme_syntaxique]['bleu']), 2).__str__().ljust(8, " "), end=" * ")
    else:
        print("0".ljust(8, " "), end=" * ")
    if len(g_matrice_confusion[forme_syntaxique]['objet et contrainte bleu'])> 0:
        print(round(sum(g_matrice_confusion[forme_syntaxique]['objet et contrainte bleu']) / len(g_matrice_confusion[forme_syntaxique]['objet et contrainte bleu']), 2).__str__().ljust(8, " "), end=" * ")
    else:
        print("0".ljust(8, " "), end=" * ")
    if len(g_matrice_confusion[forme_syntaxique]['lost bleu']) > 0:
        print(round(sum(g_matrice_confusion[forme_syntaxique]['lost bleu']) / len(g_matrice_confusion[forme_syntaxique]['lost bleu']), 2).__str__().ljust(8, " "), end=" * ")
    else:
        print("0".ljust(8, " "), end=" * ")
    print()
print(" *", end="*"*150)
print()
print(" * ", end = "")
print("Total".ljust(25, " "), end=" * ")
for item in  ['identifié', 'non identifié', 'objet trouvé', 'objet non trouvé', 'objet et contrainte']:
    if "non" in item :
        endtab = 11
    else:
        endtab = 9
    if " et " in item:
        endtab = 20
    print(total_col[item].__str__().ljust(endtab, " "), end=" * ")
print(total.__str__().ljust(11, " "), end=" * ")
print(" " * 30, end=" * ")
print()
print(" *", end="*" * 150)
print()

print()
print()
print(" *", end="*"*117)
print()
print(" * ", end = "")
print("forme syntaxique".ljust(25, " "), end=" * ")
print("contrainte".ljust(23, " "), end=" * ")
print("objet ajouté".ljust(23, " "), end=" * ")
print('objet et contrainte'.ljust(20, " "), end=" * ")
print("total".ljust(11, " "), end=" * ")
print()
print(" * ", end = "")
print("".ljust(25, " "), end=" * ")
print("trouvée".ljust(9, " "), end=" * ")
print("non trouvée".ljust(9, " "), end=" * ")
print("trouvé".ljust(9, " "), end=" * ")
print("non trouvé".ljust(11, " "), end=" * ")
print("".ljust(20, " "), end=" * ")
print("".ljust(11, " "), end=" * ")
print()
print(" *", end="*"*117)
print()
for forme_syntaxique in sorted(list(g_matrice_confusion.keys())):
    print(" * ", end = "")
    print(forme_syntaxique.ljust(25, " "), end=" * ")
    for item in  ['identifié', 'non identifié', 'objet trouvé', 'objet non trouvé', 'objet et contrainte']:
        if "non" in item :
            endtab = 11
        else:
            endtab = 9
        if " et " in item:
            endtab = 20
        print(round(g_matrice_confusion[forme_syntaxique][item]/g_matrice_confusion[forme_syntaxique]['total'], 2).__str__().ljust(endtab, " "), end=" * ")
    print(round(g_matrice_confusion[forme_syntaxique]['total']/total, 2).__str__().ljust(11, " "), end=" * ")
    print()
print(" *", end="*"*117)
print()
print(" * ", end = "")
print("Total".ljust(25, " "), end=" * ")
for item in  ['identifié', 'non identifié', 'objet trouvé', 'objet non trouvé', 'objet et contrainte']:
    if "non" in item :
        endtab = 11
    else:
        endtab = 9
    if " et " in item:
        endtab = 20
    print(round(total_col[item]/total, 2).__str__().ljust(endtab, " "), end=" * ")
print("1".ljust(11, " "), end=" * ")
print()
print(" *", end="*" * 117)
print()
print(" * ", end = "")

print()
if args.log:
    for i in range(len(for_matrice_correlation)-1):
        if modData1:
            for_matrice_correlation[i + 1] = conteneur_training_data + "," + ("theme" in conteneur_training_data).__str__() +","+ ("diff_sent" in conteneur_training_data).__str__() +","+ for_matrice_correlation[i + 1]

        for item in ['identifié',  'objet trouvé', 'objet et contrainte']:
            for_matrice_correlation[i+1] += "," + (total_col[item] / total).__str__()

    if os.path.isfile("statistiques_full.csv"):
       for_matrice_correlation[0] = ""

    fjson = open("statistiques_full.csv", "a")
    fjson.write(("\n".join(for_matrice_correlation)))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True

    print("statistiques_full.csv écrit")
