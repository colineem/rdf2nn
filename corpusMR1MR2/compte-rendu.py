from pprint import pprint
import color
import argparse
import os
import json
import re
from progress.bar import Bar
import sacrebleu
import sys
import testContraintes, shared
from itertools import product
from mosestokenizer import *
import math

dones = set()

debug = False # debug permet de skipper un gros fichier qui a été en cours de traitement longtemps, et de sortir si les données existent.

parser = argparse.ArgumentParser(description='Rend compte de la diversité des corpus générés.')
parser.add_argument('--log', action='store_true',
                   help='Correspond à verbose off : bascule les sorties dans un log (training results) et ne touche pas aux stats enregistrées.',)

args = parser.parse_args()

if args.log:
    flog = open( 'compte-rendu.log', 'w')
    # We redirect the 'sys.stdout' command towards the descriptor file
    old_stout = sys.stdout
    sys.stdout = flog

modulo = 150
print("Traitement des clés affiché par lot de " + modulo.__str__() + ".")


import string

translator = str.maketrans('', '', string.punctuation)

if os.path.isfile("store_constraints.json"):
    with open("store_constraints.json", 'r') as f:
        store = json.load(f)
        f.close()
else:
    store = {}


def getData(text_2_text:bool, data_2_text:bool, simplification:bool, multi_sent:bool,
            fusion: bool, invert_constraint:bool, theme_split:bool, shorted_function:bool,
            constraint_in_output_or_in_input:bool, constraint_somewhere_in_output:bool,
            remove_constrainst:bool, text_2_text_restricted:bool, key_1:bool, key_2:bool,unfilter_words:bool,
            base_line:bool, all_test:bool, nbest5:bool, debug:bool, mode_flux_corrected_v3:bool):
    global store
    global dones
    global only_all

    local_parser = argparse.ArgumentParser(description='Relexicalise un corpus généré.')
    local_parser.add_argument('--input', metavar='-i', type=str, nargs='+', dest="file_input",
                        default='data_test_predictions.txt',
                        help='input file')
    local_parser.add_argument('--output', metavar='-o', type=str, nargs='+', dest="file_output",
                        default='data_test_predictions',
                        help='output file, ne pas préciser d\'extension, sera relex pour relexicalisation, tsv pour tsv')
    local_parser.add_argument('--lex', metavar='-l', dest="file_lexicalisation",
                        nargs='+', default='_test.relex', type=str,
                        help='lexicalisations')
    local_parser.add_argument('--ref_src', metavar='-c', dest="ref_src",
                        nargs='?', default='inputs_test.data', type=str,
                        help='fichier des demandes originelles.')
    local_parser.add_argument('--sacrebleu', action='store_true',
                        help='Calcul du bleu (sacrebleu)', )
    local_parser.add_argument('--simplification', action='store_true',
                    help='Travail sur simplifications plutôt qu\'expansion.')
    local_parser.add_argument('--log', action='store_true',
                        help='Correspond à verbose off : bascule les sorties dans un log (training results) et ne touche pas aux stats enregistrées.', )
    local_parser.add_argument('--multi_sent', action='store_true',
                        help='Option activant le traitement des lexicalisations comportant plusieurs phrases.', )
    local_parser.add_argument('--fusion', action='store_true',
                        help='Option activant la recherche de contrainte sur l\'élément partagé et sur l\'élément ajouté.', )
    local_parser.add_argument('--invert_constraint', action='store_true',
                    help='Option faisant porter la recherche de contrainte sur l\'élément du triplet ajouté, et non support.', )
    local_parser.add_argument('--theme', action='store_true',
                    help='Organise par thème d\'apprentissage ("city"), non implémenté')
    local_parser.add_argument('--theme_split', action='store_true',
                       help='Organise par thème d\'apprentissage ("city") en arrière plan')
    local_parser.add_argument('--shorted_function', action='store_true',
                        help='Raccourci la function appliquée à la sentence.', )
    local_parser.add_argument('--constraint_in_output_or_in_input', action='store_true',
                        help='Avec cette option, on n\'exclut pas que la contrainte soit et dans l\'entrée, et dans la sortie.', )
    local_parser.add_argument('--constraint_somewhere_in_output', action='store_true',
                        help='La contrainte peut ne pas etre liée au triplet en gestion, mais porter sur un autre élément dans la lexicalisation', )
    local_parser.add_argument('--text_2_text', action='store_true',
                    help='Similaire à triple_2_sentence mais produit un corpus pour opennmt', )
    local_parser.add_argument('--text_2_text_restricted', action='store_true',
               help='Nécessite text_2_text, restreint les paires à un différentiel d\'un sur les contrainte. ',)
    local_parser.add_argument('--data_2_text', action='store_true',
                        help='Similaire à triple_2_sentence, mais produit un corpus pour opennmt avec en entrée les descriptions logiques', )
    local_parser.add_argument('--remove_constrainst', action='store_true',
                    help='retrait contrainte on text_2_text', )
    local_parser.add_argument('--key_1', action='store_true',
                   help='Restreint la clé : phrase jamais vue quelque soit la contrainte',)
    local_parser.add_argument('--key_2', action='store_true',
                   help='Restreint la clé : phrase associée à un triplet logique jamais vue quelque soit la contrainte',)
    local_parser.add_argument('--unfilter_words', action='store_true',
                              help='Désactive le filtre sur les mots, non valide pour data_2_text', )
    local_parser.add_argument('--base_line', action='store_true',
                   help='Option sans contrainte pour d2t et t2t',)
    local_parser.add_argument('--all', action='store_true',
                   help='Calculs effectués pour les données mises de côté pour all (cf makeAll.py)',)
    local_parser.add_argument('--nbest5', action='store_true',
                       help='Option sans contrainte pour d2t et t2t, sur retour n-best 5',)
    local_parser.add_argument('--debug', action='store_true',
                       help='Active le stockage dans un répertoire debug.',)
    local_parser.add_argument('--mode_flux_corrected_v3', action='store_true',
                       help='Active le stockage dans un répertoire debug.',)

    args = local_parser.parse_args()

    args.text_2_text = text_2_text
    args.data_2_text = data_2_text
    args.simplification = simplification
    args.multi_sent = multi_sent
    args.fusion = fusion
    args.invert_constraint = invert_constraint
    args.theme_split = theme_split
    args.shorted_function = shorted_function
    args.constraint_in_output_or_in_input = constraint_in_output_or_in_input
    args.constraint_somewhere_in_output = constraint_somewhere_in_output
    args.text_2_text_restricted = text_2_text_restricted
    args.remove_constrainst = remove_constrainst
    args.key_1 = key_1
    args.key_2 = key_2
    args.unfilter_words = unfilter_words
    args.base_line = base_line
    args.all = all_test
    args.nbest5 = nbest5
    args.debug = debug
    args.mode_flux_corrected_v3 = mode_flux_corrected_v3

    args.sacrebleu = False
    if args.all:
        if (text_2_text == True or  data_2_text == True or  simplification == True or invert_constraint == True or theme_split == True or  shorted_function == True or constraint_in_output_or_in_input == True or  key_1 == True or  key_2 == True or base_line == True):
            return {}

    
    if args.unfilter_words:
        if args.data_2_text:
            return {}

    if args.text_2_text_restricted and not args.text_2_text:
        return {}

    # il me faut des dossiers json/training_results/data
    if args.text_2_text:
        if args.multi_sent:
            conteneur_apprentissage = "text_2_text/multi_sent"
            conteneur_construction_data = "text_2_text/multi_sent/json"
            conteneur_training_data = "text_2_text/multi_sent/training_results"
        else:
            conteneur_apprentissage = "text_2_text/simple"
            conteneur_construction_data = "text_2_text/simple/json"
            conteneur_training_data = "text_2_text/simple/training_results"
        if args.remove_constrainst:
            conteneur_apprentissage = conteneur_apprentissage.replace('text_2_text', "text_2_text/remove_constrainst")
            conteneur_construction_data = conteneur_construction_data.replace('text_2_text',
                                                                              "text_2_text/remove_constrainst")
            conteneur_training_data = conteneur_training_data.replace('text_2_text', "text_2_text/remove_constrainst")
        if args.fusion or args.invert_constraint:
            # print("args.text_2_text and (args.fusion or args.invert_constraint)")
            return {}
    elif args.data_2_text:
        if args.multi_sent:
            conteneur_apprentissage = "data_2_text/multi_sent"
            conteneur_construction_data = "data_2_text/multi_sent/json"
            conteneur_training_data = "data_2_text/multi_sent/training_results"
        else:
            conteneur_apprentissage = "data_2_text/simple"
            conteneur_construction_data = "data_2_text/simple/json"
            conteneur_training_data = "data_2_text/simple/training_results"
        if args.fusion or args.invert_constraint:
            # print("args.data_2_text and (args.fusion or args.invert_constraint)")
            return {}
    else:

        if not args.multi_sent and not args.invert_constraint and not args.fusion:
            # conteneur des données d'apprentissage
            conteneur_apprentissage = "expansion/simple"
            conteneur_construction_data = "expansion/simple/json"
            conteneur_training_data = "expansion/simple/training_results"
        elif args.multi_sent and not args.invert_constraint and not args.fusion:
            # conteneur des données d'apprentissage
            conteneur_apprentissage = "expansion/multi_sent"
            conteneur_construction_data = "expansion/multi_sent/json"
            conteneur_training_data = "expansion/multi_sent/training_results"
        elif not args.multi_sent and args.invert_constraint and not args.fusion:
            # conteneur des données d'apprentissage
            conteneur_apprentissage = "expansion/simple_invert_constraint"
            conteneur_construction_data = "expansion/simple_invert_constraint/json"
            conteneur_training_data = "expansion/simple_invert_constraint/training_results"
        elif args.multi_sent and args.invert_constraint and not args.fusion:
            # conteneur des données d'apprentissage
            conteneur_apprentissage = "expansion/multi_invert_constraint"
            conteneur_construction_data = "expansion/multi_invert_constraint/json"
            conteneur_training_data = "expansion/multi_invert_constraint/training_results"
        elif args.multi_sent and args.fusion:
            # conteneur des données d'apprentissage
            conteneur_apprentissage = "expansion/multi_sent_fusion"
            conteneur_construction_data = "expansion/multi_sent_fusion/json"
            conteneur_training_data = "expansion/multi_sent_fusion/training_results"
        elif not args.multi_sent and args.fusion:
            # conteneur des données d'apprentissage
            conteneur_apprentissage = "expansion/simple_fusion"
            conteneur_construction_data = "expansion/simple_fusion/json"
            conteneur_training_data = "expansion/simple_fusion/training_results"
        else:
            # print(local_parser.parse_args())
            # print( "Situation non prévue, l'inversion du support des contraintes n'étant pas compatible avec une fusion... fusion toute contrainte porteuse est prise en compte.")
            return {}

        if args.simplification:
            conteneur_apprentissage = conteneur_apprentissage.replace("expansion/", "simplification/")
            conteneur_construction_data = conteneur_construction_data.replace("expansion/", "simplification/")
            conteneur_training_data = conteneur_training_data.replace("expansion/", "simplification/")

    if args.text_2_text or args.data_2_text:
        if args.invert_constraint and not args.fusion:
            conteneur_apprentissage = conteneur_apprentissage.replace("2_text", "2_text/invert_constraint")
            conteneur_construction_data = conteneur_construction_data.replace("2_text", "2_text/invert_constraint")
            conteneur_training_data = conteneur_training_data.replace("2_text", "2_text/invert_constraint")
        elif args.fusion:
            conteneur_apprentissage = conteneur_apprentissage.replace("2_text", "2_text/fusion")
            conteneur_construction_data = conteneur_construction_data.replace("2_text", "2_text/fusion")
            conteneur_training_data = conteneur_training_data.replace("2_text", "2_text/fusion")
        if args.invert_constraint and args.fusion:
            # print("(args.text_2_text or args.data_2_text) and (args.fusion and args.invert_constraint)")
            return {}

    if args.theme or args.theme_split:
        conteneur_apprentissage = "themes/" + conteneur_apprentissage
        conteneur_construction_data = "themes/" + conteneur_construction_data
        conteneur_training_data = "themes/" + conteneur_training_data

        if args.theme_split:
            conteneur_apprentissage = conteneur_apprentissage.replace("themes", "themes/theme_split")
            conteneur_construction_data = conteneur_construction_data.replace("themes", "themes/theme_split")
            conteneur_training_data = conteneur_training_data.replace("themes", "themes/theme_split")
        else:
            conteneur_apprentissage = conteneur_apprentissage.replace("themes", "themes/theme_non_split")
            conteneur_construction_data = conteneur_construction_data.replace("themes", "themes/theme_non_split")
            conteneur_training_data = conteneur_training_data.replace("themes", "themes/theme_non_split")
        if args.text_2_text or args.data_2_text:
            # todo implémenter
            # print("(args.text_2_text or args.data_2_text) and (args.theme_split)")
            return {}

    if args.shorted_function:
        conteneur_apprentissage = "shorted/" + conteneur_apprentissage
        conteneur_construction_data = "shorted/" + conteneur_construction_data
        conteneur_training_data = "shorted/" + conteneur_training_data

    if args.constraint_somewhere_in_output:
        conteneur_apprentissage = "diff_sentences/" + conteneur_apprentissage
        conteneur_construction_data = "diff_sentences/" + conteneur_construction_data
        conteneur_training_data = "diff_sentences/" + conteneur_training_data
    elif args.constraint_in_output_or_in_input:
        conteneur_apprentissage = "constraint/input_output/" + conteneur_apprentissage
        conteneur_construction_data = "constraint/input_output/" + conteneur_construction_data
        conteneur_training_data = "constraint/input_output/" + conteneur_training_data
    else:
        conteneur_apprentissage = "constraint/in_output/" + conteneur_apprentissage
        conteneur_construction_data = "constraint/in_output/" + conteneur_construction_data
        conteneur_training_data = "constraint/in_output/" + conteneur_training_data

    if args.text_2_text_restricted:
        conteneur_apprentissage = "text_2_text_restricted/" + conteneur_apprentissage
        conteneur_construction_data = "text_2_text_restricted/" + conteneur_construction_data
        conteneur_training_data = "text_2_text_restricted/" + conteneur_training_data

    if args.key_1:
        conteneur_apprentissage = "contrainte_never_seen/" + conteneur_apprentissage
        conteneur_construction_data = "contrainte_never_seen/" + conteneur_construction_data
        conteneur_training_data = "contrainte_never_seen/" + conteneur_training_data

    if args.key_2:
        conteneur_apprentissage = "contrainte_never_seen_for_triplet_and_sentence/" + conteneur_apprentissage
        conteneur_construction_data = "contrainte_never_seen_for_triplet_and_sentence/" + conteneur_construction_data
        conteneur_training_data = "contrainte_never_seen_for_triplet_and_sentence/" + conteneur_training_data
        if args.key_1:
            return {}

    if args.unfilter_words:
        conteneur_apprentissage = "no_filter/" + conteneur_apprentissage
        conteneur_construction_data = "no_filter/" + conteneur_construction_data
        conteneur_training_data = "no_filter/" + conteneur_training_data
    if args.base_line and not args.mode_flux_corrected_v3:
        if not (args.text_2_text or args.data_2_text):
            return {}
        if args.key_1 or args.key_2:
            return {}
        if not args.multi_sent:
            return {}
        if args.text_2_text_restricted:
            return {}
        if args.theme:
            return {}
        if args.theme_split:
            return {}
        if args.remove_constrainst:
            return {}
        if args.shorted_function:
            return {}
        if args.simplification:
            return {}
        if args.constraint_somewhere_in_output or args.constraint_in_output_or_in_input:
            return {}

        conteneur_apprentissage = "base_line/" + conteneur_apprentissage
        conteneur_construction_data = "base_line/" + conteneur_construction_data
        conteneur_training_data = "base_line/" + conteneur_training_data


    if args.all:
        conteneur_apprentissage = "all"
        conteneur_construction_data = "all/json"
        conteneur_training_data = "all/training_results"
        args.file_lexicalisation = "all/outputs_test.lex"
        if args.debug or args.mode_flux_corrected_v3:
            return {}
    elif args.debug:
        conteneur_apprentissage = "debug/" + conteneur_apprentissage
        conteneur_construction_data = "debug/" + conteneur_construction_data
        conteneur_training_data = "debug/" + conteneur_training_data
        if args.mode_flux_corrected_v3:
            conteneur_apprentissage = "mode_flux_corrected_v3/" + conteneur_apprentissage
            conteneur_construction_data = "mode_flux_corrected_v3/" + conteneur_construction_data
            conteneur_training_data = "mode_flux_corrected_v3/" + conteneur_training_data
        else:
            return {}
    if args.mode_flux_corrected_v3:
        if not args.nbest5:
            return{}

    if args.mode_flux_corrected_v3:
        multi_sent = True
        args.multi_sent = True
        text_2_text = True
        args.text_2_text = text_2_text
        data_2_text = False
        args.data_2_text = data_2_text
        args.multi_sent = multi_sent
        fusion = False
        args.fusion = fusion
        invert_constraint = False
        args.invert_constraint = invert_constraint
        theme_split = False
        args.theme_split = theme_split
        shorted_function = False
        args.shorted_function = shorted_function
        constraint_in_output_or_in_input = False
        args.constraint_in_output_or_in_input = constraint_in_output_or_in_input
        constraint_somewhere_in_output = True
        args.constraint_somewhere_in_output = constraint_somewhere_in_output
        text_2_text_restricted = False
        args.text_2_text_restricted = text_2_text_restricted
        remove_constrainst = False
        args.remove_constrainst = remove_constrainst
        key_1 = False
        key_2 = False
        args.key_1 = key_1
        args.key_2 = key_2
        unfilter_words = False
        args.unfilter_words = unfilter_words
        base_line = True
        args.base_line = base_line
        all_test = False
        args.all = all_test
        nbest5 = True
        args.nbest5 = nbest5
        debug = True
        args.debug = debug
        mode_flux_corrected_v3 = True
        args.mode_flux_corrected_v3 = mode_flux_corrected_v3

        conteneur_apprentissage = "mode_flux_corrected_v3/base_line/constraint/in_output/text_2_text/multi_sent/"
        conteneur_construction_data = "mode_flux_corrected_v3/base_line/constraint/in_output/text_2_text/multi_sent/json"
        conteneur_training_data = "mode_flux_corrected_v3/base_line/constraint/in_output/text_2_text/multi_sent/training_results"

    if conteneur_apprentissage in dones:
        return {}

    if args.base_line:
        if args.all:
            return {}

    if not os.path.isfile(conteneur_training_data + "/data_test_predictions.txt"):
        # print(conteneur_construction_data)
        # print(conteneur_training_data + " inexistant.")
        return {}
    if args.nbest5:
        if not args.base_line:
            return {}
        if not os.path.isfile(conteneur_training_data + "/data_test_predictions_nbest5.txt"):
            # print(conteneur_training_data + " inexistant.")
            return {}

    if not os.path.isfile(conteneur_training_data + "/../outputs_test.data"):
        # print(conteneur_training_data + " : données de références inexistantes.")
        return {}

    if "/" not in args.ref_src:
        args.ref_src = conteneur_apprentissage + "/" + args.ref_src

    if "/" not in args.file_lexicalisation:
        args.file_lexicalisation = conteneur_apprentissage + "/" + args.file_lexicalisation
    if "/" not in args.file_input:
        args.file_input = conteneur_training_data + "/" + args.file_input
    if "/" not in args.file_output:
        args.file_output = conteneur_training_data + "/" + args.file_output

    if os.path.isfile(conteneur_training_data + "/" + 'compte-rendu.log'):
        os.remove(conteneur_training_data + "/" + 'compte-rendu.log')

    if not os.path.isfile(args.file_input):
        # print(args.file_input + " inexistant.")
        return {}

    if not os.path.isfile(args.file_lexicalisation):
        # print(args.file_lexicalisation + " inexistant.")
        return {}

    if not os.path.isfile(args.ref_src):
        # print(args.ref_src + " inexistant.")
        return {}

    with open(args.file_input) as f:
        read_data = f.readlines()
        f.close()

    with open(args.ref_src) as f:
        read_data_input = f.readlines()
        f.close()

    if os.path.isfile(args.ref_src.replace("inputs", "outputs")):
        with open(args.ref_src.replace("inputs", "outputs")) as f:
            read_data_output = f.readlines()
            f.close()
    else:
        read_data_output = [] * len(read_data_input)

    with open(args.file_lexicalisation, 'r') as f:
        json_data = json.load(f)
        f.close()


    len_store = len(store.keys())

    if len(json_data) < len(read_data):
        # print("Fichier de lexicalisations moins long que données générées. Ceci est considéré comme une erreur.")
        # print(local_parser.parse_args())
        return {}

    if len(json_data) < len(read_data_input):
        # print("Fichier de lexicalisations moins long que données d'entrées. Ceci est considéré comme une erreur.")
        # print(local_parser.parse_args())
        return {}

    if len(read_data_input) < len(read_data):
        # print("Fichier de données d'entrée moins long que données générées. Ceci est considéré comme une erreur.")
        # print(local_parser.parse_args())
        return {}

    outputs = []
    inputs_src = []

    i = 0
    g_objetsTrouvés = 0
    g_objetsNonTrouvés = 0

    g_contraintesRéalisées = 0
    g_non_réalisée = 0

    taille_corpus = ""
    taille_train = ""
    taille_test = ""
    taille_dev = ""


    import subprocess
    p = subprocess.Popen([" wc -l " + conteneur_apprentissage + "/i*.data"], shell=True, stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
    for line in p.stdout.readlines():
        tmp = line.decode("utf-8").strip()
        if "total" in tmp:
            taille_corpus = re.findall("\d+", tmp)[0]
        elif "_train" in tmp:
            taille_train = re.findall("\d+", tmp)[0]
        elif "_test" in tmp:
            taille_test = re.findall("\d+", tmp)[0]
        elif "_valid" in tmp:
            taille_dev = re.findall("\d+", tmp)[0]

    retval = p.wait()
    p.kill()

    modèle = ""
    déclinaison_modèle = "no"
    if args.text_2_text:
        modèle = "text_2_text"
        if args.data_2_text or args.simplification:
            # print('(args.data_2_text or args.simplification) and text_2_text ')
            return {}
        if args.remove_constrainst:
            déclinaison_modèle = "retrait"
        else:
            déclinaison_modèle = "ajout"

    elif args.data_2_text:
        modèle = "data_2_text"
        if args.simplification:
            # print('modèle in ("simplification") and args.data_2_text  ')
            return {}
        déclinaison_modèle = "ajout"
    else:
        modèle = "modification"
        if args.simplification:
            déclinaison_modèle = "simplification"
        else:
            déclinaison_modèle = "expansion"

    déclinaison_recherche_contrainte = "no"
    if déclinaison_modèle in ("simplification", "expansion"):
        if args.invert_constraint:
            déclinaison_recherche_contrainte = "support"
            if args.fusion:
                # print('args.invert_constraint and args.fusion and modèle in ("simplification", "expansion")')
                return {}
        elif args.fusion:
            déclinaison_recherche_contrainte = "support_ou_objet"
        elif args.constraint_somewhere_in_output:
            déclinaison_recherche_contrainte = "everywhere"
        else:
            déclinaison_recherche_contrainte = "objet"
    if déclinaison_recherche_contrainte == "no":
        if args.invert_constraint or args.fusion:
            # print('(args.invert_constraint or args.fusion ) and not modèle in ("simplification", "expansion")')
            return {}
        déclinaison_recherche_contrainte = "everywhere"
        if args.constraint_somewhere_in_output and not args.mode_flux_corrected_v3:
            # print('modèle not in ("simplification", "expansion"), contrainte everywhere, nul besoin de préciser')
            return {}
    elif args.constraint_somewhere_in_output and (not déclinaison_modèle in ("simplification", "expansion") or (déclinaison_modèle in ("simplification", "expansion") and déclinaison_recherche_contrainte != "everywhere") ):
        # print('mais que fait-on ici... ?')
        return {}
    if text_2_text_restricted and not modèle == "text_2_text":
        # print("text_2_text_restricted reervé à text_2_text" )
        return {}
    restriction_contrainte_input = "no"
    if args.constraint_in_output_or_in_input:
        restriction_contrainte_input = "yes"
        if args.text_2_text or args.data_2_text:
            return {}

    multi_sent = "no"
    if args.multi_sent:
        multi_sent = "yes"

    mots_filtrés = "no"
    if not args.data_2_text:
        if not args.unfilter_words:
            mots_filtrés = "yes"

    entrée_simplifiée = "no"
    if args.shorted_function:
        entrée_simplifiée = "yes"
        if déclinaison_modèle not in ("simplification", "expansion"):
            # print('args.shorted_function and modèle not in ("simplification", "expansion")')
            return {}
    elif not modèle == "modification":
        entrée_simplifiée = "no"


    repartition_data_theme = "no"
    if args.theme_split:
        repartition_data_theme = "yes"
        if déclinaison_modèle not in ("simplification", "expansion"):
            # print('args.theme_split and modèle not in ("simplification", "expansion")')
            return {}  # non implémenté, inutile de chercher/..

    if entrée_simplifiée == "yes":
        entrée_simplifiée =  "simplified input"
    elif  text_2_text_restricted:
        entrée_simplifiée = "différentiel contraintes <= 1"
    elif modèle == "modification":
        entrée_simplifiée =  "complex input"

    cle_corpus = "full" # le trio contrainte, triplet et lexicalisation appartient à un corpus test/train/dev
    if args.key_1:
        if args.data_2_text:
            cle_corpus = "meaning" # le triplet et la lexicalisation appartiennent à un corpus test/train/dev
        elif args.text_2_text:
            cle_corpus = "sentence" # le triplet et la lexicalisation appartiennent à un corpus test/train/dev
        else:
            cle_corpus = "triplet + sentence" # le triplet et la lexicalisation appartiennent à un corpus test/train/dev
    if args.key_2:
        cle_corpus = "only sentence" # la lexicalisation appartient à un corpus test/train/dev

    lines = []
    if args.base_line:
        entrée_simplifiée = "no"
        déclinaison_recherche_contrainte = "NA"
        déclinaison_modèle = "baseline"
        if args.nbest5:
            déclinaison_modèle = "baseline_nbest5"
        if args.mode_flux_corrected_v3:
            modèle = "shuffle"
    elif args.all:
        entrée_simplifiée = "no"
        déclinaison_recherche_contrainte = "NA"
        déclinaison_modèle = "ALL"
        modèle = "ALL"

    file = args.ref_src

    if not args.all:
        for corpus_prefix in ('inputs_', 'outputs_'):
            for corpus_suffix in ('test', 'valid', 'train'):
                if os.path.isfile(file.replace("test", corpus_suffix).replace("inputs_", corpus_prefix)):
                    with open(file.replace("test", corpus_suffix)) as f:
                        lines += f.readlines()
                        f.close()
                else:
                    print(args.ref_src)
                    print(args.file_input)
                    print(file.replace("test", corpus_suffix).replace("input", corpus_prefix))
                    print("error")
                    exit()
    else:
        with open("all/inputs_test.data") as f:
            lines += f.readlines()
            f.close()
        with open("all/outputs_test.data") as f:
            lines += f.readlines()
            f.close()
        with open("all/training_results/data_test_predictions.txt") as f:
            lines += f.readlines()
            f.close()

    words = set()
    for i in range(len(lines)):
        words.update(set(lines[i].strip().split(" ")))

    vocab_size = len(words)




    import subprocess
    p = subprocess.Popen([" wc -l " + conteneur_apprentissage + "/i*.data"], shell=True, stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
    for line in p.stdout.readlines():
       # # print('\t- ', end='')
        tmp = line.decode("utf-8").strip()
       # # print(tmp)
        if "total" in tmp:
            taille_corpus = re.findall("\d+", tmp)[0]
        elif "_train" in tmp:
            taille_train = re.findall("\d+", tmp)[0]
        elif "_test" in tmp:
            taille_test = re.findall("\d+", tmp)[0]
        elif "_valid" in tmp:
            taille_dev = re.findall("\d+", tmp)[0]
    p.kill
#
    bar = Bar(conteneur_apprentissage.replace('text_2_text', 't2t').replace('for_triplet_and_sentence', '4_t&S').replace('themes/theme_split', 'themes').replace('data_2_data', 'd2d').replace('never', 'not').replace('remove', 'rm'), max=max(1, math.floor(len(read_data)/150)))

    score_moyen = []
    bleu_contrainte_realisée = []
    bleu_objet_trouvé = []
    bleu_non_contrainte_realisée = []
    bleu_non_objet_trouvé = []

    satisf = []
    non_satisf = []
    debug_satisf = [[], [], []]
    debug_non_satisf = [[], [], []]

    contrainte_ok = 0
    objets_ok = 0

    data = ["todo"] * 6

    if os.path.isfile(conteneur_training_data + "/" + 'diversite.info'):

        with open(conteneur_training_data + "/" + 'diversite.info') as f:
            data = f.readlines()
            f.close()
        try:
            data = data[1].split(",")
        except:
            print(color.red("erreur"))
            print("Effacement " + conteneur_training_data + "/" + 'diversite.info')
            os.remove(conteneur_training_data + "/" + 'diversite.info')
            print(data)
            retour = []

            if args.text_2_text:
                retour.append("text_2_text")

            if args.data_2_text:
                retour.append("data_2_text")

            if args.simplification:
                retour.append("simplification")

            if args.multi_sent:
                retour.append("multi_sent")

            if args.fusion:
                retour.append("fusion")

            if args.invert_constraint:
                retour.append("invert_constraint")

            if args.theme_split:
                retour.append("theme_split")

            if args.shorted_function:
                retour.append("shorted_function")

            if args.constraint_in_output_or_in_input:
                retour.append("constraint_in_output_or_in_input")

            if args.constraint_somewhere_in_output:
                retour.append("constraint_somewhere_in_output")

            if args.remove_constrainst:
                retour.append("remove_constrainst")

            if args.text_2_text_restricted:
                retour.append("text_2_text_restricted")

            if args.key_1:
                retour.append("key_1")

            if args.key_2:
                retour.append("key_2")

            if args.unfilter_words:
                retour.append('unfilter_words')

            if args.base_line:
                retour.append('base_line')

            if args.debug:
                retour.append('debug')

            if args.mode_flux_corrected_v3:
                retour.append('mode_flux_corrected_v3')

            retour.append("log")

            param = " --".join(retour)

            if param != "":
                param = "--" + param
            print(color.red('python3 evaluationDiversite.py ' + param))
            data = ["todo"] * 6

    data_meaning = ["todo"] * 8

    file_prefix = "diversiteMeaning"
    if args.nbest5:
        file_prefix = "diversiteMeaning_nbest5"

    if os.path.isfile(conteneur_training_data + "/" + file_prefix + '.info'):
        with open(conteneur_training_data + "/" + file_prefix + '.info') as f:
            data_meaning = f.readlines()
            f.close()
        data_meaning = data_meaning[1].split(",")

    prox_moy_meaning_ref = data_meaning[1]
    prox_moy_meaning_prod = data_meaning[2]
    prox_moy_meaning_ratio = data_meaning[3]
    nb_meaning = data_meaning[6]
    nb_lex_meaning = data_meaning[7]
    if isinstance(nb_lex_meaning, int) and float(nb_lex_meaning) == 1. :
        if prox_moy_meaning_prod == prox_moy_meaning_ref and float(prox_moy_meaning_prod) == 0:
            prox_moy_meaning_ref = "1."
            prox_moy_meaning_prod = "1."
            prox_moy_meaning_ratio = "1."
        else:
            prox_moy_meaning_ref = "todo"
            prox_moy_meaning_prod = "todo"
            prox_moy_meaning_ratio = "todo"

    # dones_entries = set()

    if args.nbest5:

        tmp_corpus = []
        for line in json_data:
            for i in range(5):
                tmp_corpus.append(line)
        json_data = tmp_corpus

        tmp_corpus = []
        for line in read_data_input:
            for i in range(5):
                tmp_corpus.append(line)
        read_data_input = tmp_corpus
        tmp_corpus = []

        for line in read_data_output:
            for i in range(5):
                tmp_corpus.append(line)
        read_data_output = tmp_corpus

    for traduction_encodée, lexicalisation, demande, reference in zip(read_data, json_data[0:len(read_data)],
                                                                      read_data_input[0:len(read_data)],
                                                                      read_data_output[0:len(read_data)]):
        global i_count
        # if traduction_encodée + demande not in dones_entries:
        # on va analyser les entrées distinctes
        # dones_entries.add(traduction_encodée + demande) --> non, il faudrait en garder le meilleur BLEU
        if traduction_encodée.strip() != "":
            traduction_encodée = traduction_encodée.strip()
            if args.mode_flux_corrected_v3 :
                forme_syntaxique = traduction_encodée.strip().split(" ")[0]
                traduction_encodée = " ".join(traduction_encodée.strip().split(" ")[1:])
            outputs.append(traduction_encodée)
            demande = demande.strip().split(" ")

            if not args.mode_flux_corrected_v3 :
                forme_syntaxique = demande.pop(0)
                triplet_input = demande[0:3]
                objetsRecherchés = triplet_input[:]
                objetsRecherchés.pop(1)
            else:
                objetsRecherchés= demande
                triplet_input = ""

            objetsTrouvés = 0
            objetsNonTrouvés = 0

            # on va aller regarder dans l'analyse stanford (indexée de 1 à n) si on a la contrainte attendue au bon endroit.
            if ("data_2_text" in conteneur_construction_data or "text_2_text" in conteneur_construction_data) and not args.mode_flux_corrected_v3:
                triplet_input = ""
                initial_sentence = demande[:]
                demande = " ".join(demande)
                found_objet = False
            else:
                initial_sentence = demande[3:]
                if args.mode_flux_corrected_v3:
                    initial_sentence = demande[:]
                else:
                    initial_sentence = demande[3:]

            objet_recherché = ""
            objet_non_recherché = ""
            setObjetInitiaux = set(reference[:].split(" "))
            setObjetTrouvés = set(traduction_encodée[:].split(" "))
            for a in (setObjetInitiaux, setObjetTrouvés):
                for elt in a.copy():
                    if "sujet_" not in elt:
                        if "objet_" not in elt:
                            if "Sujet" not in elt:
                                if "Objet" not in elt:
                                    a.remove(elt)

            différenceObjets = (setObjetInitiaux ^ setObjetTrouvés)

            if triplet_input != "" or args.mode_flux_corrected_v3:
                for objet in objetsRecherchés:
                    if objet in outputs[-1].split(" ") and objet not in initial_sentence:
                        objetsTrouvés += 1
                        for iObj, obj in enumerate(outputs[-1].split(" ")):
                            if obj == objet:
                                objet_recherché = objet
                    elif objet not in initial_sentence:
                        objetsNonTrouvés += 1
                    else:
                        for iObj, obj in enumerate(outputs[-1].split(" ")):
                            if obj == objet:
                                objet_non_recherché = objet

                found_objet = False
                if objet_recherché != "":
                    g_objetsTrouvés += 1
                    found_objet = True
                else:
                    g_objetsNonTrouvés += 1
                if not  args.mode_flux_corrected_v3:
                    demande = " ".join(demande[3:])
                else:
                    demande = " ".join(demande)

            if not args.mode_flux_corrected_v3:
                triplet_input = " ".join(triplet_input)
            else:
                triplet = ""

            hyp = traduction_encodée.split(" ")
            reference_lex = reference[:].strip()
            hypothèse_lex = traduction_encodée[:].strip()
            ref = reference.strip().split(" ")
            for lex in lexicalisation.keys():
                if lex == objet_recherché:
                    objet_recherché = lexicalisation[lex].replace("_", " ")
                if lex == objet_non_recherché:
                    objet_non_recherché = lexicalisation[lex].replace("_", " ")
                outputs[-1] = outputs[-1].replace(lex, lexicalisation[lex])
                demande = demande.replace(lex, lexicalisation[lex])
                reference_lex = reference_lex.replace(lex, lexicalisation[lex])
                hypothèse_lex = hypothèse_lex.replace(lex, lexicalisation[lex])
                triplet_input = triplet_input.replace(lex, lexicalisation[lex])
            demande = demande.split(" ")

            contrainteRéalisée = 0

            phrase_analysée = outputs[-1].replace("_", " ")

            #if phrase_analysée in store.keys():
            #    if store[phrase_analysée] == {} or type(store[phrase_analysée] ) == str :
            #        del store[phrase_analysée]
            if not args.base_line:
                if phrase_analysée in store.keys():
                    contraintes = store[phrase_analysée]
                else:
                    contraintes = testContraintes.getConstraintes(phrase_analysée)
                    if type(contraintes) == str:
                        connect()
                        contraintes = testContraintes.getConstraintes(phrase_analysée)
                        if type(contraintes) == str:
                            print("error nlp")
                            exit(5)
                    contraintes2 = testContraintes.getConstraintes(" ".join(hyp))
                    if type(contraintes2) == str:
                        connect()
                        contraintes2 = testContraintes.getConstraintes(" ".join(hyp))
                        if type(contraintes2) == str:
                            print("error nlp")
                            exit(5)

                    # pas très normal, mais une erreur mémoire coté stanford pourrait se produire
                    if not (contraintes2 == {} or type(contraintes2) == str):
                        for contrainte in contraintes2.keys():
                            if contrainte not in contraintes.keys():
                                contraintes[contrainte] = contraintes2[contrainte]
                        # on ajoute pas d'informations sur les contraintes déjà présentes, d'abord on l'a déjà référencée, peut-être pour autre chose, mais les tokens seraient à recalculer...
                    store[phrase_analysée] = contraintes

                for contrainte in contraintes.keys():
                    if shared.nettoie(forme_syntaxique).replace(" ", "_") == shared.nettoie(contrainte).replace(" ", "_"):
                        contrainteRéalisée = 1

            if contrainteRéalisée == 0:
                g_non_réalisée += 1

            #on va calculer les bleu sur retokinisé

            with MosesDetokenizer('en') as detokenize:
                hypothèse_lex_retok = detokenize(hypothèse_lex.split(" ")).replace("_", " ")
                reference_lex_retok = detokenize(reference_lex.split(" ")).replace("_", " ")

            score = sacrebleu.corpus_bleu(hypothèse_lex_retok, reference_lex_retok).score
            score_moyen.append(score)

            specific_values = forme_syntaxique + ","
            specific_values += contrainteRéalisée.__str__() + ","
            specific_values += "1," if found_objet else "0,"
            specific_values += score.__str__()

            satisfaction_objets = (len(différenceObjets) == 0)

            satisfaction_contrainte = False
            if déclinaison_modèle == "retrait":
                if contrainteRéalisée == 0:
                    satisfaction_contrainte = True
            elif contrainteRéalisée > 0:
                satisfaction_contrainte = True

            if satisfaction_contrainte:
                bleu_contrainte_realisée.append(score)
            else:
                bleu_non_contrainte_realisée.append(score)

            if satisfaction_objets:
                bleu_objet_trouvé.append(score)
            else:
                bleu_non_objet_trouvé.append(score)

            if satisfaction_objets and satisfaction_contrainte:
                satisf.append(score)
                debug_satisf[0].append(différenceObjets)
                debug_satisf[1].append(contrainteRéalisée)
                debug_satisf[2].append((satisfaction_objets, satisfaction_contrainte))
            else:
                non_satisf.append(score)
                debug_non_satisf[0].append(différenceObjets)
                debug_non_satisf[1].append(contrainteRéalisée)
                debug_non_satisf[2].append((satisfaction_objets, satisfaction_contrainte))

            if satisfaction_objets:
                objets_ok += 1

            if satisfaction_contrainte:
                contrainte_ok += 1

            g_contraintesRéalisées += contrainteRéalisée

            i += 1

            if i % modulo == 0:
                if bar.index  < math.floor(len(read_data)/modulo) :
                    bar.next()

    bar.finish()
    if len_store != len(store.keys()):
        with open("store_constraints.json", "w") as fjson:
            json.dump(store, fjson, ensure_ascii=False, indent=2)
            f.close()
        print(color.green("modification magasin d'analyse : sauvegarde effectuée"))

    if len(score_moyen)>0:
        score_moyen = sum(score_moyen) / len(score_moyen)
    else:
        score_moyen = 0.

    if len(satisf) > 0:
        best_respect = sum(satisf) / len(satisf)
        len_best = len(satisf)
    else:
        best_respect = 0.
        len_best = 0

    if data[4].strip() != (int(taille_test)+1).__str__().strip():
        print(color.blue(conteneur_construction_data), end = ", ")
        if os.path.isfile(conteneur_training_data + "/" + 'diversite.info'):
            print(color.red('diversite.info calculé sur données incomplètes'), end = " : ")
            os.remove(conteneur_training_data + "/" + 'diversite.info')
        print(color.yellow('exécuter execBattery.py --evaluation'))

        retour = []

        if args.text_2_text:
            retour.append("text_2_text")

        if args.data_2_text:
            retour.append("data_2_text")

        if args.simplification:
            retour.append("simplification")

        if args.multi_sent:
            retour.append("multi_sent")

        if args.fusion:
            retour.append("fusion")

        if args.invert_constraint:
            retour.append("invert_constraint")

        if args.theme_split:
            retour.append("theme_split")

        if args.shorted_function:
            retour.append("shorted_function")

        if args.constraint_in_output_or_in_input:
            retour.append("constraint_in_output_or_in_input")

        if args.constraint_somewhere_in_output:
            retour.append("constraint_somewhere_in_output")

        if args.remove_constrainst:
            retour.append("remove_constrainst")

        if args.text_2_text_restricted:
            retour.append("text_2_text_restricted")

        if args.key_1:
            retour.append("key_1")

        if args.key_2:
            retour.append("key_2")

        if args.unfilter_words:
            retour.append('unfilter_words')

        retour.append("log")

        param = " --".join(retour)

        if param != "":
            param = "--"+param
        print(color.red('python3 evaluationDiversite.py ' + param))
    if data[0].strip() in ["no_filter/text_2_text_restricted/constraint/in_output/text_2_text/multi_sent/training_results",
 "base_line/constraint/in_output/text_2_text/multi_sent/training_results",
 "constraint/in_output/data_2_text/multi_sent/training_results",
 "base_line/constraint/in_output/data_2_text/multi_sent/training_results",
 "no_filter/diff_sentences/simplification/multi_sent/training_results",
 "no_filter/diff_sentences/expansion/multi_sent/training_results",
 "all/training_results"]:
        kept = "True"
    else:
        if modèle == "ALL":
            kept = "True"
        elif déclinaison_modèle == "baseline_nbest5":
            kept = "True"
        else:
            kept = "False"
    dones.add(conteneur_apprentissage)
    taille_corpus_str = "NA";
    if not args.all:
        taille_corpus_str = int(taille_corpus)+1
    return {'score bleu général': score_moyen,
            'bleu_contrainte_satisfaite': sum(bleu_contrainte_realisée)/len(bleu_contrainte_realisée) if len(bleu_contrainte_realisée)> 0 else 0,
            'bleu_objets_satisfaits':sum(bleu_objet_trouvé)/len(bleu_objet_trouvé) if len(bleu_objet_trouvé)> 0 else 0,
            'bleu_non_contrainte_satisfaite':sum(bleu_non_contrainte_realisée)/len(bleu_non_contrainte_realisée) if len(bleu_non_contrainte_realisée)> 0 else 0,
            'bleu_non_objets_satisfaits':sum(bleu_non_objet_trouvé)/len(bleu_non_objet_trouvé) if len(bleu_non_objet_trouvé)> 0 else 0,
            '# corpus':taille_corpus_str, 'keep':kept,
            '# corpus test':int(taille_test)+1, "filtre_sur_mots": mots_filtrés,
            '# plénitude': len_best, "bleu plénitude": best_respect, 'couverture plénitude':len_best/(int(taille_test)+1),
            "fichier":data[0],"similariteProd":data[2],"similariteRef":data[1],"ratio diversité":data[3],
            "masse":data[4],"# ask diversity":data[5], 'modèle':modèle, 'déclinaison_modèle':déclinaison_modèle,
            'déclinaison_recherche_contrainte':déclinaison_recherche_contrainte,
            "restriction_contrainte_input":restriction_contrainte_input,
            "multi_sent":multi_sent, 'contrainte_entrée_sortie':entrée_simplifiée,
            "repartition_data_theme":repartition_data_theme, 'cle_corpus':cle_corpus, '# vocab':vocab_size,
            '# objets ok':objets_ok, '# contrainte ok': contrainte_ok,
            'prox_moy_meaning_ref': prox_moy_meaning_ref, 'prox_moy_meaning_prod': prox_moy_meaning_prod,
            'prox_moy_meaning_ratio': prox_moy_meaning_ratio, '# meaning': nb_meaning, '# lex_meaning': nb_lex_meaning}

key_sys = ['keep', 'cle_corpus', 'modèle', 'déclinaison_modèle',
            'déclinaison_recherche_contrainte',
            "restriction_contrainte_input",
            "multi_sent", "filtre_sur_mots", 'contrainte_entrée_sortie',
            "repartition_data_theme"]
keys = ['# vocab','# corpus',
        '# corpus test','score bleu général',
        '# plénitude', "bleu plénitude", 'couverture plénitude',
        'prox_moy_meaning_ref', 'prox_moy_meaning_prod',
        'prox_moy_meaning_ratio', '# meaning', '# lex_meaning',
        "fichier", "ratio diversité",
        "similariteProd","similariteRef", "# ask diversity",'bleu_contrainte_satisfaite',
        'bleu_objets_satisfaits', 'bleu_non_contrainte_satisfaite',
        'bleu_non_objets_satisfaits', '# objets ok', '# contrainte ok'] # masse

csv =",".join(key_sys) + "," + ",".join(keys) + "\n"
restricted_csv =",".join(key_sys) + "," + ",".join(keys) + "\n"

result = ''
i_count = 0
i_count_todo = 0

for a in product([True, False], repeat=20):
    if args.log:
        flog.flush()
    del result
    result = getData(*a)

    #if True:
     #   results = getData(False, False, False, False, False, False, False, False, False, False, False, False, False,
      #                    False, False, True, False, True, True, True)

    if result != {}:
        line = ""
        sep = ""
        for key in key_sys:
            line += sep + result[key].__str__()
            sep = ","
        for key in keys:
            line += sep + result[key].__str__()
        if line not in csv:
            i_count +=1
            if "todo" in line:
                i_count_todo += 1
            csv += line
            csv += "\n"

fjson = open("statistiques.csv", "w")
fjson.write(csv)
fjson.flush()
fjson.close()
while not fjson.closed:
    True

print(i_count, end = " ")
print('bases traitées, dont ', end = "")
print(i_count_todo, end = " ")
print("en attente de traitements supplémentaires (", end = "")
print(i_count - i_count_todo, end = "")
print(" sont complètement traités).")


if args.log:
    flog.close()
    # We redirect the 'sys.stdout' command towards the descriptor file
    sys.stdout = old_stout
