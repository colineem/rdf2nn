from xml.etree import ElementTree
from pprint import pprint
from nltk.corpus import stopwords
import os, json
import sys
import string, re
from nltk.corpus import wordnet as wn
import random
import argparse
import nltk
from progress.bar import Bar
import copy
import itertools
import string

translator = str.maketrans('', '', string.punctuation)
table = str.maketrans('/.', "  ")

debug = False

parser = argparse.ArgumentParser(description='Génération d\'un corpus d\'apprentissage, lancer avec --only_cmd pour le pré-traitement permettant de calculer la taille des corpus de test et de validation.')
parser.add_argument('--f', action='store_true',
                   help='Option activant le traitement des lexicalisations comportant plusieurs phrases.',)
parser.add_argument('--fusion', action='store_true',
                   help='Option activant la recherche de contrainte sur l\'élément partagé et sur l\'élément ajouté.',)
parser.add_argument('--invert_constraint', action='store_true',
                   help='Option faisant porter la recherche de contrainte sur l\'élément du triplet ajouté, et non support.',)
parser.add_argument('--log', action='store_true',
                   help='Correspond à verbose off : bascule les sorties dans un log (conteneur construction data)',)
parser.add_argument('--only_cmd', action='store_true',
                   help='Sort juste le bash et sort. Enregistre tout de même la taille du corpus.',)
parser.add_argument('--simplification', action='store_true',
                   help='Travail sur simplifications plutôt qu\'expansion.')
parser.add_argument('--multi_taches', action='store_true',
                   help='Travail sur corpus multi-tâche, options invert_constraint et fusion inopérantes sur ce choix.')
parser.add_argument('--theme', action='store_true',
                   help='Organise par thème d\'apprentissage ("city"), non implémenté')
parser.add_argument('--theme_split', action='store_true',
                   help='Organise par thème d\'apprentissage ("city") en arrière plan')
parser.add_argument('--triple_2_sentence', action='store_true',
                   help='Triplets deléxicalisés vers lexicalisations.',)
parser.add_argument('--shorted_function', action='store_true',
                   help='Raccourci la function appliquée à la sentence.',)
parser.add_argument('--constraint_in_output_or_in_input', action='store_true',
                   help='Avec cette option, on n\'exclut pas que la contrainte soit et dans l\'entrée, et dans la sortie.',)
parser.add_argument('--constraint_somewhere_in_output', action='store_true',
                   help='La contrainte peut ne pas etre liée au triplet en gestion, mais porter sur un autre élément dans la lexicalisation',)
parser.add_argument('--text_2_text', action='store_true',
                   help='Similaire à triple_2_sentence mais produit un corpus pour opennmt',)
parser.add_argument('--text_2_text_restricted', action='store_true',
                   help='Nécessite text_2_text, restreint les paires à un différentiel d\'un sur les contrainte. ',)
parser.add_argument('--remove_constrainst', action='store_true',
                   help='retire la contrainte pour text_2_text',)
parser.add_argument('--data_2_text', action='store_true',
                   help='Similaire à triple_2_sentence, mais produit un corpus pour opennmt avec en entrée les descriptions logiques',)
parser.add_argument('--key_1', action='store_true',
                   help='Restreint la clé : phrase jamais vue quelque soit la contrainte',)
parser.add_argument('--key_2', action='store_true',
                   help='Restreint la clé : phrase associée à un triplet logique jamais vue quelque soit la contrainte',)
parser.add_argument('--unfilter_words', action='store_true',
                  help='Désactive le filtre sur les mots, non valide pour data_2_text',)
parser.add_argument('--multi_sent', action='store_true',
                   help='Option activant le traitement des lexicalisations comportant plusieurs phrases.',)
parser.add_argument('--base_line', action='store_true',
                   help='Option sans contrainte pour d2t et t2t',)
parser.add_argument('--debug', action='store_true',
                   help='Active le stockage dans un répertoire debug.',)
parser.add_argument('--mode_flux_corrected_v3', action='store_true',
                   help='Active le stockage dans un répertoire debug.',)

args=parser.parse_args()


liste_char=string.ascii_letters+string.digits
regex = re.compile('[%s]' % re.escape(string.punctuation))

if debug:
    args.multi_sent = True

if args.text_2_text_restricted and not args.text_2_text:
    exit(4)

if args.unfilter_words:
    if args.data_2_text:
        exit(5)



#les vocaublaires...
if args.multi_taches or args.triple_2_sentence or args.data_2_text or args.text_2_text:  # passe la simplification, on ne va faire qu'un corpus
    vocab = [["<unk>", "<s>", "</s>", "<pad>"], ["<unk>", "<s>", "</s>", "<pad>"], ["<unk>", "<s>", "</s>", "<pad>"]]

# il me faut des dossiers json/training_results/data
if args.triple_2_sentence:
    if args.multi_sent:
        conteneur_apprentissage = "triple_2_sentence/multi_sent"
        conteneur_construction_data = "triple_2_sentence/multi_sent/json"
        conteneur_training_data = "triple_2_sentence/multi_sent/training_results"
    else:
        conteneur_apprentissage = "triple_2_sentence/simple"
        conteneur_construction_data = "triple_2_sentence/simple/json"
        conteneur_training_data = "triple_2_sentence/simple/training_results"
    if args.fusion or args.invert_constraint:
        exit(1)
elif args.text_2_text:
    if args.multi_sent:
        conteneur_apprentissage = "text_2_text/multi_sent"
        conteneur_construction_data = "text_2_text/multi_sent/json"
        conteneur_training_data = "text_2_text/multi_sent/training_results"
    else:
        conteneur_apprentissage = "text_2_text/simple"
        conteneur_construction_data = "text_2_text/simple/json"
        conteneur_training_data = "text_2_text/simple/training_results"
    if args.remove_constrainst:
        conteneur_apprentissage = conteneur_apprentissage.replace('text_2_text', "text_2_text/remove_constrainst")
        conteneur_construction_data = conteneur_construction_data.replace('text_2_text', "text_2_text/remove_constrainst")
        conteneur_training_data = conteneur_training_data.replace('text_2_text', "text_2_text/remove_constrainst")
    if args.fusion or args.invert_constraint:
        exit(1)
elif args.data_2_text:
    if args.multi_sent:
        conteneur_apprentissage = "data_2_text/multi_sent"
        conteneur_construction_data = "data_2_text/multi_sent/json"
        conteneur_training_data = "data_2_text/multi_sent/training_results"
    else:
        conteneur_apprentissage = "data_2_text/simple"
        conteneur_construction_data = "data_2_text/simple/json"
        conteneur_training_data = "data_2_text/simple/training_results"
    if args.fusion or args.invert_constraint:
        exit(1)
else:

    if not args.multi_sent and not args.invert_constraint and not args.fusion:
        # conteneur des données d'apprentissage
        conteneur_apprentissage = "expansion/simple"
        conteneur_construction_data = "expansion/simple/json"
        conteneur_training_data = "expansion/simple/training_results"
    elif args.multi_sent and not args.invert_constraint and not args.fusion:
        # conteneur des données d'apprentissage
        conteneur_apprentissage = "expansion/multi_sent"
        conteneur_construction_data = "expansion/multi_sent/json"
        conteneur_training_data = "expansion/multi_sent/training_results"
    elif not args.multi_sent and args.invert_constraint and not args.fusion:
        # conteneur des données d'apprentissage
        conteneur_apprentissage = "expansion/simple_invert_constraint"
        conteneur_construction_data = "expansion/simple_invert_constraint/json"
        conteneur_training_data = "expansion/simple_invert_constraint/training_results"
    elif args.multi_sent and args.invert_constraint and not args.fusion:
        # conteneur des données d'apprentissage
        conteneur_apprentissage = "expansion/multi_invert_constraint"
        conteneur_construction_data = "expansion/multi_invert_constraint/json"
        conteneur_training_data = "expansion/multi_invert_constraint/training_results"
    elif args.multi_sent and args.fusion:
        # conteneur des données d'apprentissage
        conteneur_apprentissage = "expansion/multi_sent_fusion"
        conteneur_construction_data = "expansion/multi_sent_fusion/json"
        conteneur_training_data = "expansion/multi_sent_fusion/training_results"
    elif not args.multi_sent and args.fusion:
        # conteneur des données d'apprentissage
        conteneur_apprentissage = "expansion/simple_fusion"
        conteneur_construction_data = "expansion/simple_fusion/json"
        conteneur_training_data = "expansion/simple_fusion/training_results"
    else:
        print(parser.parse_args())
        print("Situation non prévue, l'inversion du support des contraintes n'étant pas compatible avec une fusion... fusion toute contrainte porteuse est prise en compte.")
        exit(1)

    if args.multi_taches: # passe la simplification, on ne va faire qu'un corpus

        conteneur_apprentissage = conteneur_apprentissage.replace("expansion/", "multi_taches/")
        conteneur_construction_data = conteneur_construction_data.replace("expansion/", "multi_taches/")
        conteneur_training_data = conteneur_training_data.replace("expansion/", "multi_taches/")
        if args.fusion or args.invert_constraint:
            exit(1)

    elif args.simplification:
        conteneur_apprentissage = conteneur_apprentissage.replace("expansion/", "simplification/")
        conteneur_construction_data = conteneur_construction_data.replace("expansion/", "simplification/")
        conteneur_training_data = conteneur_training_data.replace("expansion/", "simplification/")

if args.text_2_text or args.data_2_text:
    if args.invert_constraint and not args.fusion:
        conteneur_apprentissage = conteneur_apprentissage.replace("2_text", "2_text/invert_constraint")
        conteneur_construction_data = conteneur_construction_data.replace("2_text", "2_text/invert_constraint")
        conteneur_training_data = conteneur_training_data.replace("2_text", "2_text/invert_constraint")
    elif args.fusion:
        conteneur_apprentissage = conteneur_apprentissage.replace("2_text", "2_text/fusion")
        conteneur_construction_data = conteneur_construction_data.replace("2_text", "2_text/fusion")
        conteneur_training_data = conteneur_training_data.replace("2_text", "2_text/fusion")
    if args.invert_constraint and  args.fusion:
        exit(5)


if args.theme or args.theme_split:
    conteneur_apprentissage = "themes/" + conteneur_apprentissage
    conteneur_construction_data = "themes/" + conteneur_construction_data
    conteneur_training_data = "themes/" + conteneur_training_data

    if args.theme_split:
        conteneur_apprentissage = conteneur_apprentissage.replace("themes", "themes/theme_split")
        conteneur_construction_data = conteneur_construction_data.replace("themes", "themes/theme_split")
        conteneur_training_data = conteneur_training_data.replace("themes", "themes/theme_split")
    else:
        conteneur_apprentissage = conteneur_apprentissage.replace("themes", "themes/theme_non_split")
        conteneur_construction_data = conteneur_construction_data.replace("themes", "themes/theme_non_split")
        conteneur_training_data = conteneur_training_data.replace("themes", "themes/theme_non_split")

    if args.text_2_text or args.data_2_text:
        # todo implémenter
        exit(2)

# petit support d'exit

if args.shorted_function:
    conteneur_apprentissage = "shorted/" + conteneur_apprentissage
    conteneur_construction_data = "shorted/" + conteneur_construction_data
    conteneur_training_data = "shorted/" + conteneur_training_data

if args.constraint_somewhere_in_output:
    conteneur_apprentissage = "diff_sentences/" + conteneur_apprentissage
    conteneur_construction_data = "diff_sentences/" + conteneur_construction_data
    conteneur_training_data = "diff_sentences/" + conteneur_training_data

elif args.constraint_in_output_or_in_input:
    conteneur_apprentissage = "constraint/input_output/" + conteneur_apprentissage
    conteneur_construction_data = "constraint/input_output/" + conteneur_construction_data
    conteneur_training_data = "constraint/input_output/" + conteneur_training_data
else:
    conteneur_apprentissage = "constraint/in_output/" + conteneur_apprentissage
    conteneur_construction_data = "constraint/in_output/" + conteneur_construction_data
    conteneur_training_data = "constraint/in_output/" + conteneur_training_data


if args.text_2_text_restricted:
    conteneur_apprentissage = "text_2_text_restricted/" + conteneur_apprentissage
    conteneur_construction_data = "text_2_text_restricted/" + conteneur_construction_data
    conteneur_training_data = "text_2_text_restricted/" + conteneur_training_data

if args.key_1:
    conteneur_apprentissage = "contrainte_never_seen/" + conteneur_apprentissage
    conteneur_construction_data = "contrainte_never_seen/" + conteneur_construction_data
    conteneur_training_data = "contrainte_never_seen/" + conteneur_training_data

if args.key_2:
    conteneur_apprentissage = "contrainte_never_seen_for_triplet_and_sentence/" + conteneur_apprentissage
    conteneur_construction_data = "contrainte_never_seen_for_triplet_and_sentence/" + conteneur_construction_data
    conteneur_training_data = "contrainte_never_seen_for_triplet_and_sentence/" + conteneur_training_data
    if args.key_1:
        exit(3)

if args.unfilter_words:
    conteneur_apprentissage = "no_filter/" + conteneur_apprentissage
    conteneur_construction_data = "no_filter/" + conteneur_construction_data
    conteneur_training_data = "no_filter/" + conteneur_training_data

if args.base_line:
    if not (args.text_2_text or args.data_2_text):
        exit(1)
    if args.key_1 or args.key_2:
        exit(1)
    if not args.multi_sent:
        exit(1)
    conteneur_apprentissage = "base_line/" + conteneur_apprentissage
    conteneur_construction_data = "base_line/" + conteneur_construction_data
    conteneur_training_data = "base_line/" + conteneur_training_data

if args.debug:
    conteneur_apprentissage = "debug/" + conteneur_apprentissage
    conteneur_construction_data = "debug/" + conteneur_construction_data
    conteneur_training_data = "debug/" + conteneur_training_data
if args.mode_flux_corrected_v3:
    conteneur_apprentissage = "mode_flux_corrected_v3/" + conteneur_apprentissage
    conteneur_construction_data = "mode_flux_corrected_v3/" + conteneur_construction_data
    conteneur_training_data = "mode_flux_corrected_v3/" + conteneur_training_data

modèle = ""
déclinaison_modèle = "NA"
if args.text_2_text:
    modèle = "text_2_text"
    if args.data_2_text or args.simplification or args.key_2 or args.multi_taches:
        print('modèle in ("simplification") and args.text_2_text  ')
        exit(1)
    if args.remove_constrainst:
        déclinaison_modèle = "ajout"
    else:
        déclinaison_modèle = "retrait"

elif args.data_2_text:
    modèle = "data_2_text"
    if args.simplification or args.key_2 or args.multi_taches:
        print('modèle in ("simplification") and args.data_2_text  ')
        exit(2)
    if args.remove_constrainst:
        déclinaison_modèle = "ajout"
    else:
        déclinaison_modèle = "retrait"
else:
    modèle = "modification"
    if args.simplification:
        déclinaison_modèle = "simplification"
    else:
        déclinaison_modèle = "expansion"

déclinaison_recherche_contrainte = "NA"
if déclinaison_modèle in ("simplification", "expansion"):
    if args.invert_constraint:
        déclinaison_recherche_contrainte = "support"
        if args.fusion:
            print('args.invert_constraint and args.fusion and modèle in ("simplification", "expansion")')
            exit(3)
    elif args.fusion:
        déclinaison_recherche_contrainte = "support_ou_objet"
    elif args.constraint_somewhere_in_output:
        déclinaison_recherche_contrainte = "everywhere"
    else:
        déclinaison_recherche_contrainte = "objet"
elif args.constraint_in_output_or_in_input:
    # TODO
    exit(5)

if déclinaison_recherche_contrainte == "NA":
    if args.invert_constraint or args.fusion:
        print('(args.invert_constraint or args.fusion ) and not modèle in ("simplification", "expansion")')
        exit(4)
    déclinaison_recherche_contrainte = "everywhere"
    if args.constraint_somewhere_in_output:
        print('modèle not in ("simplification", "expansion"), contrainte everywhere, nul besoin de préciser')
        exit(5)
elif args.constraint_somewhere_in_output and (not déclinaison_modèle in ("simplification", "expansion") or (
        déclinaison_modèle in (
"simplification", "expansion") and déclinaison_recherche_contrainte != "everywhere")):
    print('mais que fait-on ici... ?')
    exit(5)

restriction_contrainte_input = "no"
if args.constraint_in_output_or_in_input:
    restriction_contrainte_input = "yes"

multi_sent = "no"
if args.multi_sent:
    multi_sent = "yes"

entrée_simplifiée = "no"
if args.shorted_function:
    entrée_simplifiée = "yes"
    if déclinaison_modèle not in ("simplification", "expansion"):
        print('args.shorted_function and modèle not in ("simplification", "expansion")')
        exit(6)

repartition_data_theme = "no"
if args.theme_split:
    repartition_data_theme = "yes"
    if déclinaison_modèle not in ("simplification", "expansion"):
        print('args.theme_split and modèle not in ("simplification", "expansion")')
        exit(7)  # non implémenté, inutile de chercher/..

if not os.path.isdir(conteneur_apprentissage):
    os.makedirs(conteneur_apprentissage)

if not os.path.isdir(conteneur_construction_data):
    os.makedirs(conteneur_construction_data)

if not os.path.isdir(conteneur_training_data):
    os.makedirs(conteneur_training_data)

if args.multi_taches:
    instruction_gpu = "launchers_nmt/" + conteneur_apprentissage.replace('-', "_").replace("/", "_") + ".bash"
    if not os.path.isdir("launchers_nmt/"):
        os.makedirs("launchers_nmt")
    if args.key_2 or args.key_1 or args.shorted_function or déclinaison_modèle in ("simplification", "expansion") or args.constraint_in_output_or_in_input or args.invert_constraint or args.fusion:
        exit(2) 

else:
    instruction_gpu = "launchers/" + conteneur_apprentissage.replace('-', "_").replace("/", "_") + ".bash"

    if not os.path.isdir("launchers/"):
        os.makedirs("launchers")

def writeJSON(data:[], filename:str, dirname:str = conteneur_construction_data):
    if dirname == "":
        with open(filename, "w") as fjson:
            json.dump(data, fjson, ensure_ascii=False, indent=2)
    else:
        with open(dirname + "/" + filename, "w") as fjson:
            json.dump(data, fjson, ensure_ascii=False, indent=2)


def loadJSON(filename: str, type_données: type, dirname:str= conteneur_construction_data):
    if os.path.isfile(dirname + "/" + filename):
        with open(dirname + "/" + filename, 'r') as f:
            json_data = json.load(f)
        return json_data
    else:
        if type_données == dict:
            return {}
        else:
            return []

if len(os.listdir(conteneur_training_data)) > 0:
    print(conteneur_training_data + " non vide")
    print(instruction_gpu)
    if not debug :
        exit()
elif os.path.isfile(conteneur_apprentissage+"/inputs_test.data"):
    print(conteneur_apprentissage+"/inputs_test.data déjà créé.")
    print(instruction_gpu)
    if not debug:
        exit()
else:
    print("*******")
    print(conteneur_construction_data)
    try:
        if not args.only_cmd and len(loadJSON("corpus.json", dict))==0:
            print(conteneur_training_data + " vide et corpus.json non créé." )
            print("lancement première passe demandé (--only_cmd).")
            args.only_cmd = True
            exit()
        elif args.only_cmd:
            if os.path.isfile(conteneur_construction_data+"/corpus.json"):
                os.remove(conteneur_construction_data+"/corpus.json")
    except:
        print(os.listdir(conteneur_training_data))
        print("erreur lecture json, lancement première passe demandé (--only_cmd).")
        args.only_cmd = True
        exit()

import sys
sys.stderr
if args.log and not debug:
    f= open(conteneur_construction_data + "/" +'built.log', 'w')
    # We redirect the 'sys.stdout' command towards the descriptor file
    sys.stdout = f

print("dossiers de travail :")
print('\tconteneur apprentissage : ' + conteneur_apprentissage)
print('\tconteneur construction_data : ' + conteneur_construction_data)
print('\tconteneur training_data : ' + conteneur_training_data)
print('\tinstruction gpu : ' + instruction_gpu)
print()

if args.theme_split:
    MR1_MR2_test = loadJSON("base_theme_test.json", [])
    MR1_MR2_valid = loadJSON("base_theme_valid.json", [])
    MR1_MR2_train = loadJSON("base_theme_train.json", [])
else:
    MR1_MR2_test = loadJSON("base_test.json", [])
    MR1_MR2_valid = loadJSON("base_valid.json", [])
    MR1_MR2_train = loadJSON("base_train.json", [])

print("Préparation des données de traitements")

file = "../prepareCorpus/webnlgparser_data/working_data.xml"

# chargement des données
tree = ElementTree.parse(file)
root = tree.getroot()
items_syntaxiques = {}

MR = {}
usedPrédicats = {}

exemples = root.findall('exemple')

used_data = set()

src_seq_length = 0
tgt_seq_length = 0
print()
print("Chargement des données")
bar = Bar('Processing', max=len(exemples))

encoded_seen = set()
for exemple in exemples:
    sentences_count = int(exemple.attrib['sentences'])

    encoded = exemple.find('encoded').text.strip()
    encoded = " ".join(nltk.wordpunct_tokenize(encoded))

    if (sentences_count == 1 or args.multi_sent) and encoded not in encoded_seen:
        encoded_seen.add(encoded)
        origin = exemple.find('origin').text.strip()
        thème = exemple.attrib['theme']
        nbTriplets = exemple.attrib['triplets']
        predicats = set()
        elements = {}

        for entry in exemple.findall('relations'):

            for relation in entry.findall('relation'):
                prédicat = relation.attrib['reference'].strip().replace(" ", "_")
                predicats.add(prédicat)

        sorted_predicats_MR2 = sorted(list(predicats))

        MR2 = False
        MR1 = []
        for tmpKeySet in usedPrédicats.keys():
            tmpSet = set(tmpKeySet.split("##"))
            if len(tmpSet) + 1 == len(predicats):
                if predicats.issuperset(tmpSet):
                    MR2 = True
                    MR1.append(tmpSet)
            elif len(tmpSet) == len(predicats) and tmpSet.issubset(predicats):
                predicats = tmpSet

        if "##".join(sorted(list(predicats))) not in usedPrédicats.keys():
            usedPrédicats["##".join(sorted(list(predicats)))] = []

        if MR2:
            tmpsMR1 = []
            MR2 = False
            for tmpMR1 in MR1:
                sorted_predicats_MR1 = sorted(list(tmpMR1))
                subj_objs = {}
                linear_mr = []
                for entry in exemple.findall('relations'):
                    for relation in entry.findall('relation'):
                        prédicat = relation.attrib['reference'].strip().replace(" ", "_")
                        if prédicat in tmpMR1:
                            sujet = relation.find("subject")
                            subj = sujet.attrib['reference']
                            subj_id = sujet.attrib['id']
                            if subj_id not in subj_objs.keys():
                                tmpName = prédicat.translate(translator) + "Sujet"
                                i = 1
                                while tmpName + i.__str__() in subj_objs.values():
                                    i += 1
                                subj_objs[subj_id] = tmpName + i.__str__()

                            encoded = encoded.replace(subj_id, subj_objs[subj_id])

                            objet = relation.find("object")
                            obj = objet.attrib['reference']
                            obj_id = objet.attrib['id']
                            if obj_id not in subj_objs.keys():
                                tmpName = prédicat.translate(translator) + "Objet"
                                i = 1
                                while tmpName + i.__str__() in subj_objs.values():
                                    i += 1
                                subj_objs[obj_id] = tmpName + i.__str__()

                            encoded = encoded.replace(obj_id, subj_objs[obj_id])

                            linear_mr.append([subj_objs[subj_id], prédicat.strip().replace(" ", "_"), subj_objs[obj_id]])

                initial_key = ".".join(["/".join(k) for k in sorted(linear_mr, key=lambda x: (x[1], x[0], x[2]))])
                if initial_key in usedPrédicats["##".join(sorted_predicats_MR1)]:
                    tmpsMR1.append([initial_key, tmpMR1, linear_mr])
                    MR2 = True
            MR1 = tmpsMR1

        subj_objs = {}
        linear_mr = []
        data_relation = []

        encoded = exemple.find('encoded').text.strip()
        encoded = " ".join(nltk.wordpunct_tokenize(encoded))

        for entry in exemple.findall('relations'):

            for relation in entry.findall('relation'):
                prédicat = relation.attrib['reference'].strip().replace(" ", "_")

                sujet = relation.find("subject")
                subj = sujet.attrib['reference']
                subj_id = sujet.attrib['id']

                if subj_id not in subj_objs.keys():
                    tmpName = prédicat.translate(translator) + "Sujet"
                    i = 1
                    while tmpName + i.__str__() in subj_objs.values():
                        i += 1
                    subj_objs[subj_id] = tmpName + i.__str__()

                encoded = encoded.replace(subj_id, subj_objs[subj_id])

                objet = relation.find("object")
                obj = objet.attrib['reference']
                obj_id = objet.attrib['id']
                if obj_id not in subj_objs.keys():
                    tmpName = prédicat.translate(translator) + "Objet"
                    i = 1
                    while tmpName + i.__str__() in subj_objs.values():
                        i += 1
                    subj_objs[obj_id] = tmpName + i.__str__()

                encoded = encoded.replace(obj_id, subj_objs[obj_id])

                data_relation.append(((subj, subj_id, subj_objs[subj_id]), prédicat, (obj, obj_id, subj_objs[obj_id]), relation))

                linear_mr.append([subj_objs[subj_id], prédicat, subj_objs[obj_id]])

        key = ".".join(["/".join(k) for k in sorted(linear_mr, key=lambda x: (x[1], x[0], x[2]))])

        if key not in usedPrédicats["##".join(sorted(list(predicats)))]:
            usedPrédicats["##".join(sorted(list(predicats)))].append(key)
        if MR2:
            for initial_key, tmpMR1, infoLogiques in MR1:
                encodedMR1 = encoded[:]
                originMR1 = origin[:]
                MR2_2_MR1 = {}
                for item in sorted(linear_mr, key=lambda x: (x[1], x[0], x[2])):
                    predicat = item[1]
                    sujet = item[0]
                    objet = item[2]
                    if predicat in MR2_2_MR1.keys():
                        MR2_2_MR1[predicat].append((sujet, objet))
                    else:
                        MR2_2_MR1[predicat] = [(sujet, objet)]
                for item in sorted(infoLogiques, key=lambda x: (x[1], x[0], x[2])):
                    predicat = item[1]
                    sujet = item[0]
                    objet = item[2]
                    i = 0
                    while isinstance(MR2_2_MR1[predicat][i], dict):
                        i += 1
                    # MR2 : MR1
                    MR2_2_MR1[predicat][i] = ({MR2_2_MR1[predicat][i][0]:sujet, MR2_2_MR1[predicat][i][1]:objet})

                lexicalisation = {}
                for i_item, item in enumerate(data_relation):
                    lexicalisation[item[2][2]] = item[2][0]
                    lexicalisation[item[0][2]] = item[0][0]

                    for triplet in MR2_2_MR1[item[1]]:
                        if not isinstance(triplet, dict):

                            # c'est notre prédicat ajouté

                            prédicat = item[1].strip().replace(" ", "_")
                            relation = item[-1]

                            sujet_found = False
                            objet_found = False

                            sujet = item[0][0]
                            objet = item[2][0]

                            for i_item_lk, item_lk in enumerate(data_relation):
                                if i_item != i_item_lk:
                                    sujet_lk = item_lk[0][0]
                                    objet_lk = item_lk[2][0]
                                    # si le sujet de mon item est sujet ou objet pour un autre triplet
                                    if sujet in(sujet_lk, objet_lk):
                                        sujet_found = True
                                    # si l'objet de mon item est sujet ou objet pour un autre triplet
                                    if objet in(objet_lk, objet_lk):
                                        objet_found = True

                            if sujet_found and objet_found:
                                MR2 = False
                            if args.fusion:
                                values_tested = ['subject', 'object']
                            else:
                                if args.invert_constraint:
                                    # l'objet support de l'ajout est impliqué dans la contrainte
                                    value = 'subject' if not objet_found else 'object'
                                    valeur_concernée = sujet if objet_found else objet
                                else:
                                    # l'objet ajouté est impliqué dans la contrainte
                                    value = 'object' if not objet_found else 'subject'
                                    valeur_concernée = objet if objet_found else sujet
                                values_tested = [value]

                            added_data = []
                            for value in values_tested:

                                for added_element in relation.findall(value):
                                    for token in added_element.findall('token'):
                                        identification = token.attrib['identification']
                                        index = token.attrib['index']
                                        added_data.append((item[0:3], identification, index, []))

                                for constraint in exemple.findall('constraints/contrainte'):
                                    name = constraint.attrib['name']

                                    if name not in ('participle clause subject', 'participle clause'):
                                        for socle in constraint.findall('socle'):
                                            for token in socle.findall('node/token'):
                                                index = token.attrib['index']
                                                for data in added_data:
                                                    if args.constraint_somewhere_in_output:
                                                        data[-1].append(name)
                                                    elif data[2] == index:
                                                        data[-1].append(name)

                            keeped_values = []
                            for data in added_data:
                                if len(data[-1]) > 0:
                                    keeped_values.append(data)
                                    for constr in data[-1]:
                                        if constr not in items_syntaxiques.keys():
                                            items_syntaxiques[constr] = 0
                                        items_syntaxiques[constr] += 1

                            if len(keeped_values) == 0 :
                                MR2 = False

                if MR2:
                    MR[initial_key][-1].append((key, linear_mr, encoded, origin, subj_objs, data_relation, tmpMR1, MR2_2_MR1, lexicalisation, keeped_values))
        names = []
        for constraint in exemple.findall('constraints/contrainte'):
            name = constraint.attrib['name']
            if name not in ('participle clause subject'):#, 'participle clause'):
                names.append(name)
        if key in MR.keys():
            MR[key][0].append((linear_mr, encoded, origin, subj_objs, data_relation, names))
        else:
            MR[key] = [[(linear_mr, encoded, origin, subj_objs, data_relation, names, thème, nbTriplets)], []]


    bar.next()

bar.finish()
nb_with = 0
nb_without = 0
nb_with_simple_MR1 = 0
nb_with_simple_MR2 = 0

max_lexicalisations = 0
nb_lexicalisations = []
for each in MR.keys():
    if len(MR[each][0]) > max_lexicalisations:
        max_lexicalisations = len(MR[each][0])
    nb_lexicalisations.append(len(MR[each][0]))
    if len(MR[each][0]) == 0:
        nb_without += len(MR[each][0])
    else:
        nb_with += len(MR[each][0]) * len(MR[each][0])
        nb_with_simple_MR1 += len(MR[each][0])
        nb_with_simple_MR2 += len(MR[each][1])

import math
print(len(nb_lexicalisations))
print(math.fsum(nb_lexicalisations)/len(nb_lexicalisations))
print(max_lexicalisations)

if debug:
    exit()
print("sur-évaluations (évaluations avant filtrage)")
print("with (couplages MR1/MR2 envisageables sous réserve contrainte impliquée dans la transformation):", end = ' ')
print(nb_with)
print("nb_MR1_with_simple :", end = ' ')
print(nb_with_simple_MR1)
print("nb_MR2_with_simple :", end = ' ')
print(nb_with_simple_MR2)
print("without :", end = ' ')
print(nb_without)

corpus = loadJSON("corpus.json", dict)

if "taille" in corpus.keys():
    taille_max_base_non_train = int(corpus['taille']) / 10 # 20% de taille totale évaluée pour test et valid
elif args.multi_sent:
    taille_max_base_non_train = 564868 / 10 # 20% de taille totale évaluée pour test et valid
else:
    taille_max_base_non_train = 530182 / 10  # 20% de taille totale évaluée pour test et valid

pprint(items_syntaxiques)

nb_subset = 0
thèmes_références = copy.deepcopy(corpus)
for each_cat in corpus.keys():
    if each_cat not in ['MR1_MR2_test', 'MR1_MR2_train', 'MR1_MR2_valid']:
        corpus[each_cat] = 0
    else:
        for thème in corpus[each_cat]:
            corpus[each_cat][thème] = 0
if "taille" not in corpus.keys():
    corpus['taille'] = 0
elif args.theme_split and not os.path.isfile(conteneur_construction_data + "/base_theme_test.json") and 'MR1_MR2_valid' in corpus.keys():
    del corpus['MR1_MR2_valid']
    del corpus['MR1_MR2_test']
    del corpus['MR1_MR2_train']

inputs = [[], [], []]
outputs = [[], [], []]
outputs_relex = [[], [], []]
outputs2 = [[], [], []]
inputs_lex = [[], [], []]
inputs_meaning = [[], [], []]
outputs_lex = [[], [], []]
outputs_meaning = [[], [], []]
outputs2_lex = [[], [], []]
contraintes_retenues = {}

print()
print("Traitement des données")

if args.multi_taches:
    bar = Bar('Processing', max=len(MR.keys()))
    variations = []
    variations_lex = {}
    variations_keys = {}
else:
    bar = Bar('Processing', max=nb_with)
    variations_input = {}

count_keep = 0
count_anomaly = 0
count_constraint = {}

keys_string = list(MR.keys())
random.shuffle(keys_string)
for each in keys_string:
    # le thème est propre à chaque ligne. on va considérer que l'assemblage prédicatif est propre au thème donc commun à l'ensemble de lignes.
    # TODO split sur thème
    thème = MR[each][0][0][6]
    if args.triple_2_sentence or args.data_2_text or args.text_2_text:
        bar.next()

        key_input = each.replace('.', " ").replace('/', " ")

        if args.text_2_text:
            for MR1_piece in MR[each][0]:
                count_constraint = {}
                contraintesInput, inputData, elementsInput = MR1_piece[5], MR1_piece[1], MR1_piece[4]

                if args.mode_flux_corrected_v3:
                    for MR2_piece in MR[each][0][:]:
                        contraintesOutput = MR2_piece[5]
                        contraintes = set(contraintesOutput)
                        for contrainte in contraintes:
                            if contrainte not in count_constraint.keys():
                                count_constraint[contrainte] = 0

                            count_constraint[contrainte] += 1

                for MR2_piece in MR[each][0]:

                    input = " ".join(nltk.wordpunct_tokenize(inputData[:]))
                    contraintesInput = set(contraintesInput)

                    contraintesOutput, outputData, elements = MR2_piece[5], MR2_piece[1], MR2_piece[4]

                    output = " ".join(nltk.wordpunct_tokenize(outputData[:]))

                    if not args.mode_flux_corrected_v3:
                        if args.text_2_text_restricted:
                            setCO = set(contraintesOutput)
                            contraintesOutput = setCO - contraintesInput
                            contraintesInput =  contraintesInput - setCO
                            if len(contraintesOutput) == 0:
                                input = output
                            elif not (len(contraintesOutput) == 1 and len(contraintesInput)<=1):
                                input = output
                    # contraintesOutput contient les contraintes de la sortie qui ne sont pas dans l'entrée
                    # contraintesInput contient les contraintes de l'entrée qui ne sont pas dans la sortie
                    if "xlid" in input or "xlid" in output:
                        input = "get out"

                    if input != "get out" and (input != output or args.mode_flux_corrected_v3):

                        if not args.base_line and not args.mode_flux_corrected_v3:
                            stop = set(stopwords.words('english'))
                            stop.add('')

                            if args.unfilter_words:
                                phrase_a = set()
                                phrase_b = set()
                            else:
                                phrase_a = set(nltk.wordpunct_tokenize(regex.sub(' ', output.lower()).strip().replace('  ', ' ')))
                                phrase_b = set(nltk.wordpunct_tokenize(regex.sub(' ', input.lower()).strip().replace('  ', ' ')))

                                pop_set = set()
                                for a in phrase_a.union(phrase_b):
                                    if not wn.synsets(a, pos=wn.NOUN):
                                        pop_set.add(a)

                                phrase_a = phrase_a - stop
                                phrase_a = phrase_a - pop_set
                                phrase_b = phrase_b - stop
                                phrase_b = phrase_b - pop_set
                        else:
                            phrase_a = set()
                            phrase_b = set()

                        if len(phrase_b.symmetric_difference(phrase_a)) == 0:

                            if args.base_line and not args.mode_flux_corrected_v3:
                                contraintes = ["false"]
                            elif not args.mode_flux_corrected_v3:
                                if args.remove_constrainst:
                                    contraintes = set(contraintesInput).difference(contraintesOutput)
                                else:
                                    contraintes = set(contraintesOutput).difference(contraintesInput)
                            else:
                                contraintes = set(contraintesOutput)
                            input_copy = input[:].strip()

                            if args.mode_flux_corrected_v3 and len(contraintes)>0 and len(contraintesInput)>0:
                                k1 = count_constraint[min(count_constraint, key=count_constraint.get)]
                                k2 = count_constraint[max(count_constraint, key=count_constraint.get)]
                                for _ in range(min(len(contraintes), k2 - k1)):
                                    contraintes = list(contraintes)
                                    random.shuffle(contraintes)
                                    tmp_contrainte= [x[0] for x in sorted(count_constraint.items(), key=lambda x: x[1]) if x[0] in contraintes] # clé du premier tuple

                                    if len(tmp_contrainte) > 1:
                                        all = [x[0] for x in sorted(count_constraint.items(), key=lambda x: x[1])][:len(count_constraint.keys())-2]
                                        for tmp_cont in all:
                                            if tmp_cont in tmp_contrainte[0:3]:
                                                contraintes.append(tmp_cont)
                                                count_constraint[tmp_cont] += 1
                                                break
                                        else:
                                            break
                                    else:
                                        break

                            if args.mode_flux_corrected_v3 and len(contraintesInput)==0:
                                contraintes = []

                            for contrainte in contraintes:

                                if args.base_line or args.mode_flux_corrected_v3:
                                    input = input_copy
                                else:
                                    input = contrainte.replace(' ', '_') + " " + input_copy

                                if args.mode_flux_corrected_v3:
                                    output_copy = contrainte.replace(' ', '_') + " " + output[:]
                                else:
                                    output_copy = output[:]

                                if input + output_copy not in used_data:
                                    add_data = True

                                    used_data.add(input + output_copy)

                                    lexicalisations = {}

                                    for lex in elements:
                                        lexicalisations[lex[0][2]] = lex[0][0].replace(" ", "_")
                                        lexicalisations[lex[2][2]] = lex[2][0].replace(" ", "_")

                                    lexMR1 = input[:]
                                    lexMR2 = output_copy[:]

                                    seen_in_input = input[:]

                                    if args.key_1:
                                        seen_in_input = input_copy

                                    for lex_key in lexicalisations.keys():
                                        lexMR1 = lexMR1.replace(lex_key, lexicalisations[lex_key])
                                        lexMR2 = lexMR2.replace(lex_key, lexicalisations[lex_key])
                                        seen_in_input = seen_in_input.replace(lex_key, "")
                                    while "  " in seen_in_input:
                                        seen_in_input = seen_in_input.replace("  ", " ")

                                    src_seq_ = len(input.split(" "))
                                    if src_seq_ > src_seq_length:
                                        src_seq_length = src_seq_

                                    tgt_seq_ = len(output_copy.split(" "))
                                    if tgt_seq_ > tgt_seq_length:
                                        tgt_seq_length = tgt_seq_

                                    if args.mode_flux_corrected_v3:
                                        input_tab = input.split()

                                        input = [k for k in input_tab if "sujet" in k.lower() or "objet" in k.lower()]

                                        # V2
                                        seen_in_input = contrainte.replace(' ', '_') + " " +  " ".join(sorted(set(input)))
                                        # V3
                                        seen_in_input = " ".join(sorted(list(set(input))))

                                        random.shuffle(input)
                                        input = contrainte.replace(' ', '_') + " " + " ".join(input)

                                    index_train_test_valid = -1

                                    if seen_in_input in MR1_MR2_valid:
                                        index_train_test_valid = 2
                                    elif seen_in_input in MR1_MR2_test:
                                        index_train_test_valid = 1
                                    elif seen_in_input in MR1_MR2_train:
                                        index_train_test_valid = 0

                                    # on s'assure que chaque élément est bien enregistré dans les bases, sinon on enregistre
                                    if index_train_test_valid == 0:
                                        slot = MR1_MR2_train
                                    elif index_train_test_valid == 1:
                                        slot = MR1_MR2_test
                                    elif index_train_test_valid == 2:
                                        slot = MR1_MR2_valid
                                    else:
                                        if args.theme_split:
                                            if 'MR1_MR2_test' not in corpus.keys():
                                                corpus['MR1_MR2_test'] = {}
                                            if 'MR1_MR2_valid' not in corpus.keys():
                                                corpus['MR1_MR2_valid'] = {}
                                            if 'MR1_MR2_train' not in corpus.keys():
                                                corpus['MR1_MR2_train'] = {}
                                            if thème not in corpus['MR1_MR2_test'].keys():
                                                corpus['MR1_MR2_test'][thème] = 0
                                            if thème not in corpus['MR1_MR2_valid'].keys():
                                                corpus['MR1_MR2_valid'][thème] = 0
                                            if thème not in corpus['MR1_MR2_train'].keys():
                                                corpus['MR1_MR2_train'][thème] = 0
                                            if thème not in thèmes_références.keys():
                                                thèmes_références[thème] = taille_max_base_non_train * 2
                                            if corpus['MR1_MR2_valid'][thème] + corpus['MR1_MR2_test'][thème] < thèmes_références[
                                                thème] / 5:
                                                if random.randint(1, 5) == 5:
                                                    if corpus['MR1_MR2_test'][thème] < corpus['MR1_MR2_valid'][thème]:
                                                        corpus['MR1_MR2_test'][thème] += 1
                                                        slot = MR1_MR2_test
                                                        index_train_test_valid = 1
                                                    else:
                                                        corpus['MR1_MR2_valid'][thème] += 1
                                                        slot = MR1_MR2_valid
                                                        index_train_test_valid = 2
                                                else:
                                                    corpus['MR1_MR2_train'][thème] += 1
                                                    slot = MR1_MR2_train
                                                    index_train_test_valid = 0
                                            else:
                                                corpus['MR1_MR2_train'][thème] += 1
                                                slot = MR1_MR2_train
                                                index_train_test_valid = 0
                                        else:
                                            index_train_test_valid = 0
                                            slot = MR1_MR2_train
                                            if len(MR1_MR2_test) + len(MR1_MR2_valid) < taille_max_base_non_train * 2:
                                                if random.randint(1, 5) == 5:
                                                    if len(MR1_MR2_test) < len(MR1_MR2_valid):
                                                        slot = MR1_MR2_test
                                                        index_train_test_valid = 1
                                                    else:
                                                        slot = MR1_MR2_valid
                                                        index_train_test_valid = 2
                                    if seen_in_input not in slot:
                                        slot.append(seen_in_input)

                                    inputs[index_train_test_valid].append(input)
                                    outputs[index_train_test_valid].append(output_copy)

                                    inputs_meaning[index_train_test_valid].append(each)
                                    outputs_meaning[index_train_test_valid].append(each)

                                    inputs_lex[index_train_test_valid].append(lexMR1)
                                    outputs_lex[index_train_test_valid].append(lexMR2)

                                    outputs_relex[index_train_test_valid].append(lexicalisations)
                                    vocab[0] += list(set([k for k in nltk.wordpunct_tokenize(input) if k not in vocab[0]]))
                                    vocab[1] += list(set([k for k in nltk.wordpunct_tokenize(output_copy) if k not in vocab[1]]))

                                    corpus['taille'] += 1
                                    if thème not in corpus.keys():
                                        corpus[thème] = 0
                                    corpus[thème] += 1


        else:
            for MR1_piece in MR[each][0]:

                contraintes, output, elements = MR1_piece[5], MR1_piece[1], MR1_piece[4]

                output = " ".join(nltk.wordpunct_tokenize(output))

                if args.base_line:
                   contraintes = [False]

                D2TM = []
                tmpD2TMs = each.translate(table)
                while "  " in tmpD2TMs:
                    tmpD2TMs = tmpD2TMs.replace("  ", " ")
                tmpD2TMs = tmpD2TMs.split(" ")

                try:
                    for tmpD2TM in tmpD2TMs:
                        if tmpD2TM != "":
                            if "sujet" in tmpD2TM.lower() or "objet" in tmpD2TM.lower():
                                D2TM.append(tmpD2TM)
                                D2TM.append("")
                            else:
                                D2TM[-1] += tmpD2TM + " "
                    for iElt in range(len(D2TM)):
                        D2TM[iElt] = D2TM[iElt].strip()
                    while '' in D2TM:
                        D2TM.remove('')

                    if len(D2TM) % 3 != 0:
                        count_anomaly += 1
                        input = "xlid"

                    for iElt in range(len(D2TM)):
                        D2TM[iElt] = "_".join(D2TM[iElt].split(" "))
                    input = " ".join(D2TM)

                except Exception as exci:
                    count_anomaly += 1
                    input = "xlid"


                if len(contraintes) == 0:
                    contraintes.append('sans contrainte')

                for contrainte in set(contraintes):
                    if args.base_line:
                        input = input.replace('.', "_").replace('/', "_")
                    else:
                        input = contrainte.replace(' ', '_') + " " + input.replace('.', "_").replace('/', "_")

                    if "xlid" in input or "xlid" in output:
                        count_anomaly += 1
                        input = output
                    else :
                        count_keep += 1

                    if input + output not in used_data and input != output:
                        add_data = True

                        used_data.add(input + output)

                        lexicalisations = {}

                        for lex in elements:
                            lexicalisations[lex[0][2]] = lex[0][0].replace(" ", "_")
                            lexicalisations[lex[2][2]] = lex[2][0].replace(" ", "_")

                        lexMR1 = input[:]
                        lexMR2 = output[:]

                        seen_in_input = input[:]


                        if args.key_1:
                            seen_in_input = each.replace('.', " ").replace('/', " ")

                        for lex_key in lexicalisations.keys():
                            lexMR1 = lexMR1.replace(lex_key, lexicalisations[lex_key])
                            lexMR2 = lexMR2.replace(lex_key, lexicalisations[lex_key])
                            seen_in_input = seen_in_input.replace(lex_key, "")

                        while "  " in seen_in_input:
                            seen_in_input = seen_in_input.replace("  ", " ")

                        src_seq_ = len(input.split(" "))
                        if src_seq_ > src_seq_length:
                            src_seq_length = src_seq_

                        tgt_seq_ = len(output.split(" "))
                        if tgt_seq_ > tgt_seq_length:
                            tgt_seq_length = tgt_seq_

                        index_train_test_valid = -1


                        if seen_in_input in MR1_MR2_valid:
                            index_train_test_valid = 2
                        elif seen_in_input in MR1_MR2_test:
                            index_train_test_valid = 1
                        elif seen_in_input in MR1_MR2_train:
                            index_train_test_valid = 0

                        # on s'assure que chaque élément est bien enregistré dans les bases, sinon on enregistre
                        if index_train_test_valid == 0:
                            slot = MR1_MR2_train
                        elif index_train_test_valid == 1:
                            slot = MR1_MR2_test
                        elif index_train_test_valid == 2:
                            slot = MR1_MR2_valid
                        else:
                            if args.theme_split:
                                if 'MR1_MR2_test' not in corpus.keys():
                                    corpus['MR1_MR2_test'] = {}
                                if 'MR1_MR2_valid' not in corpus.keys():
                                    corpus['MR1_MR2_valid'] = {}
                                if 'MR1_MR2_train' not in corpus.keys():
                                    corpus['MR1_MR2_train'] = {}
                                if thème not in corpus['MR1_MR2_test'].keys():
                                    corpus['MR1_MR2_test'][thème] = 0
                                if thème not in corpus['MR1_MR2_valid'].keys():
                                    corpus['MR1_MR2_valid'][thème] = 0
                                if thème not in corpus['MR1_MR2_train'].keys():
                                    corpus['MR1_MR2_train'][thème] = 0
                                if thème not in thèmes_références.keys():
                                    thèmes_références[thème] = taille_max_base_non_train * 2
                                if corpus['MR1_MR2_valid'][thème] + corpus['MR1_MR2_test'][thème] < thèmes_références[thème] / 5:
                                    if random.randint(1, 5) == 5:
                                        if corpus['MR1_MR2_test'][thème] < corpus['MR1_MR2_valid'][thème]:
                                            corpus['MR1_MR2_test'][thème] += 1
                                            slot = MR1_MR2_test
                                            index_train_test_valid = 1
                                        else:
                                            corpus['MR1_MR2_valid'][thème] += 1
                                            slot = MR1_MR2_valid
                                            index_train_test_valid = 2
                                    else:
                                        corpus['MR1_MR2_train'][thème] += 1
                                        slot = MR1_MR2_train
                                        index_train_test_valid = 0
                                else:
                                    corpus['MR1_MR2_train'][thème] += 1
                                    slot = MR1_MR2_train
                                    index_train_test_valid = 0
                            else:
                                index_train_test_valid = 0
                                slot = MR1_MR2_train
                                if len(MR1_MR2_test) + len(MR1_MR2_valid) < taille_max_base_non_train * 2:
                                    if random.randint(1, 5) == 5:
                                        if len(MR1_MR2_test) < len(MR1_MR2_valid):
                                            slot = MR1_MR2_test
                                            index_train_test_valid = 1
                                        else:
                                            slot = MR1_MR2_valid
                                            index_train_test_valid = 2
                        if seen_in_input not in slot:
                            slot.append(seen_in_input)


                        inputs[index_train_test_valid].append(input)
                        outputs[index_train_test_valid].append(output)

                        inputs_meaning[index_train_test_valid].append(each)
                        outputs_meaning[index_train_test_valid].append(each)

                        inputs_lex[index_train_test_valid].append(lexMR1)
                        outputs_lex[index_train_test_valid].append(lexMR2)

                        outputs_relex[index_train_test_valid].append(lexicalisations)
                        vocab[0] += list(set([k for k in nltk.wordpunct_tokenize(input) if k not in vocab[0]]))
                        vocab[1] += list(set([k for k in nltk.wordpunct_tokenize(output) if k not in vocab[1]]))

                        corpus['taille'] += 1
                        if thème not in corpus.keys():
                            corpus[thème] = 0
                        corpus[thème] += 1
    elif args.multi_taches:
        bar.next()
        variations_lex[each] = set()
        for infosLexMR in MR[each][0]:
            variations_lex[each].add(" ".join(nltk.wordpunct_tokenize(infosLexMR[1])))
        if len(variations_lex[each]) > 0:
            variations.append(len(variations_lex[each]))
            for p, r in itertools.permutations(MR[each][0], 2):
                # p/r[5] : les contraintes.
                set_p5 = set(p[5])
                set_r5 = set(r[5])

                if len(set_p5) in (len(set_r5), len(set_r5)-1):#échange avec sans_contrainte ou autre contrainte
                    for contrainte in set_p5:
                        if contrainte not in set_r5 and (set_p5 - {contrainte}).issubset(set_r5):
                            key_input = each.replace('.', " ").replace('/', " ")
                            input = contrainte.replace(' ', '_') + " " + each.replace('.', " ").replace('/', " ")
                            output1 = " ".join(nltk.wordpunct_tokenize(p[1]))
                            output2 = " ".join(nltk.wordpunct_tokenize(r[1]))

                            if "xlid" in input or "xlid" in output1 or "xlid" in output2:
                                output1 = output2

                            if input + output1 + output2 not in used_data and not output1 == outputs2:
                                add_data = True

                                if not args.unfilter_words:

                                    stop = set(stopwords.words('english'))
                                    stop.add('')

                                    phrase_a = set(regex.sub(' ', output1.lower().strip()).replace('  ', ' ').split(" "))
                                    phrase_b = set(regex.sub(' ', output2.lower().strip()).replace('  ', ' ').split(" "))

                                    phrase_a = phrase_a - stop
                                    phrase_b = phrase_b - stop

                                    if not phrase_a.issubset(phrase_b):
                                        add_data = False

                                if add_data:
                                    used_data.add(input + output1 + output2)

                                    lexicalisations = {}

                                    for lex in p[4]:
                                        lexicalisations[lex[0][2]] = lex[0][0].replace(" ", "_")
                                        lexicalisations[lex[2][2]] = lex[2][0].replace(" ", "_")

                                    lexMR1 = input[:]
                                    lexMR2 = output1[:]
                                    lexMR3 = output2[:]
                                    seen_in_input = input[:]

                                    if args.key_1:
                                        seen_in_input = each.replace('.', " ").replace('/', " ")

                                    for lex_key in lexicalisations.keys():
                                        lexMR1 = lexMR1.replace(lex_key, lexicalisations[lex_key])
                                        lexMR2 = lexMR2.replace(lex_key, lexicalisations[lex_key])
                                        lexMR3 = lexMR3.replace(lex_key, lexicalisations[lex_key])
                                        seen_in_input = seen_in_input.replace(lex_key, "")
                                    while "  " in seen_in_input:
                                        seen_in_input = seen_in_input.replace("  ", " ")

                                    src_seq_ = len(input.split(" "))
                                    if src_seq_ > src_seq_length:
                                        src_seq_length = src_seq_

                                    # pour le multi-tâche nmt, on a besoin plutôt du ratio (cf nmt.py)
                                    tgt_seq_ = max(len(output1.split(" ")), len(output2.split(" ")))
                                    if src_seq_/tgt_seq_ > tgt_seq_length:
                                        tgt_seq_length = src_seq_/tgt_seq_

                                    index_train_test_valid = -1

                                    if seen_in_input in MR1_MR2_valid:
                                        index_train_test_valid = 2
                                    elif seen_in_input in MR1_MR2_test:
                                        index_train_test_valid = 1
                                    elif seen_in_input in MR1_MR2_train:
                                        index_train_test_valid = 0

                                    # on s'assure que chaque élément est bien enregistré dans les bases, sinon on enregistre
                                    if index_train_test_valid == 0:
                                        slot = MR1_MR2_train
                                    elif index_train_test_valid == 1:
                                        slot = MR1_MR2_test
                                    elif index_train_test_valid == 2:
                                        slot = MR1_MR2_valid
                                    else:
                                        if args.theme_split:
                                            if 'MR1_MR2_test' not in corpus.keys():
                                                corpus['MR1_MR2_test'] = {}
                                            if 'MR1_MR2_valid' not in corpus.keys():
                                                corpus['MR1_MR2_valid'] = {}
                                            if 'MR1_MR2_train' not in corpus.keys():
                                                corpus['MR1_MR2_train'] = {}
                                            if thème not in corpus['MR1_MR2_test'].keys():
                                                corpus['MR1_MR2_test'][thème] = 0
                                            if thème not in corpus['MR1_MR2_valid'].keys():
                                                corpus['MR1_MR2_valid'][thème] = 0
                                            if thème not in corpus['MR1_MR2_train'].keys():
                                                corpus['MR1_MR2_train'][thème] = 0
                                            if thème not in thèmes_références.keys():
                                                thèmes_références[thème] = taille_max_base_non_train * 2
                                            if corpus['MR1_MR2_valid'][thème] + corpus['MR1_MR2_test'][thème] < thèmes_références[thème] / 5:
                                                if random.randint(1, 5) == 5:
                                                    if corpus['MR1_MR2_test'][thème] < corpus['MR1_MR2_valid'][thème] :
                                                        corpus['MR1_MR2_test'][thème] += 1
                                                        slot = MR1_MR2_test
                                                        index_train_test_valid = 1
                                                    else:
                                                        corpus['MR1_MR2_valid'][thème] += 1
                                                        slot = MR1_MR2_valid
                                                        index_train_test_valid = 2
                                                else:
                                                    corpus['MR1_MR2_train'][thème] += 1
                                                    slot = MR1_MR2_train
                                                    index_train_test_valid = 0
                                            else:
                                                corpus['MR1_MR2_train'][thème] += 1
                                                slot = MR1_MR2_train
                                                index_train_test_valid = 0
                                        else:
                                            index_train_test_valid = 0
                                            slot = MR1_MR2_train
                                            if len(MR1_MR2_test) + len(MR1_MR2_valid) < taille_max_base_non_train * 2:
                                                if random.randint(1, 5) == 5:
                                                    if len(MR1_MR2_test) < len(MR1_MR2_valid):
                                                        slot = MR1_MR2_test
                                                        index_train_test_valid = 1
                                                    else:
                                                        slot = MR1_MR2_valid
                                                        index_train_test_valid = 2
                                    if seen_in_input not in slot:
                                        slot.append(seen_in_input)

                                    inputs[index_train_test_valid].append(input)
                                    outputs[index_train_test_valid].append(output1)
                                    outputs2[index_train_test_valid].append(output2)


                                    inputs_meaning[index_train_test_valid].append(each)
                                    outputs_meaning[index_train_test_valid].append(each)

                                    inputs_lex[index_train_test_valid].append(lexMR1)
                                    outputs_lex[index_train_test_valid].append(lexMR2)
                                    outputs2_lex[index_train_test_valid].append(lexMR3)
                                    outputs_relex[index_train_test_valid].append(lexicalisations)

                                    # vocab[0] contient les input, 1 et 2 les outputs
                                    vocab[0] += list(set([k for k in nltk.wordpunct_tokenize(input) if k not in vocab[0]]))
                                    vocab[1] += list(set([k for k in nltk.wordpunct_tokenize(output1) if k not in vocab[1]]))
                                    vocab[2] += list(set([k for k in nltk.wordpunct_tokenize(output2) if k not in vocab[2]]))
                                    corpus['taille'] += 1
                                    if thème not in corpus.keys():
                                        corpus[thème]=0
                                    corpus[thème] += 1
    elif len(MR[each][0]) > 0:
        for MR1tmp in MR[each][0]:
            for MR2tmp in MR[each][1]:
                MR2 = copy.deepcopy(MR2tmp)
                MR1 = copy.deepcopy(MR1tmp)
                constraintes_MR1 =  MR1[5]
                sentence_MR1 = MR1[2]
                sentence_encoded_MR1 = MR1[1]
                contraintes_MR1 = MR1[5]

                values_MR1 = MR1[3]
                inv_values_MR1 = {}
                for oldKey in values_MR1.keys():
                    sentence_encoded_MR1 = sentence_encoded_MR1.replace(values_MR1[oldKey], oldKey)
                    inv_values_MR1[values_MR1[oldKey]] = oldKey
                sentence_MR2 = MR2[3]
                sentence_encoded_MR2 = MR2[2]
                MR2_2_MR1 = MR2[-3]
                lexicalisations = MR2[-2]
                keeped_values = MR2[-1]
                # il va falloir recomposer l'encoded
                rebuiltItems = {}
                for itemMR2_key in MR2_2_MR1.keys():
                    itemMR2 = MR2_2_MR1[itemMR2_key]
                    for elt in itemMR2:
                        if isinstance(elt, dict):
                            for key in elt.keys():
                                #keys = new values, coming from MR2, value = from MR1
                                rebuiltItems[key] = elt[key]

                for newValue in rebuiltItems.keys():
                    oldValue = rebuiltItems[newValue]
                    sentence_encoded_MR1 = sentence_encoded_MR1.replace(inv_values_MR1[oldValue], newValue)

                sentence_MR1 = sentence_encoded_MR1[:]

                for trad in lexicalisations.keys():
                    sentence_MR1 = sentence_MR1.replace(trad, lexicalisations[trad])

                # évaluation keep or not

                if args.unfilter_words:
                    phrase_a = set()
                    phrase_b = set()
                else:
                    stop = set(stopwords.words('english'))
                    stop.add('')


                    if args.simplification:
                        phrase_b = set(regex.sub(' ', sentence_encoded_MR1.lower().strip()).replace('  ', ' ').split(" "))
                        phrase_a = set(regex.sub(' ', sentence_encoded_MR2.lower().strip()).replace('  ', ' ').split(" "))
                    else:
                        phrase_a = set(regex.sub(' ', sentence_encoded_MR1.lower().strip()).replace('  ', ' ').split(" "))
                        phrase_b = set(regex.sub(' ', sentence_encoded_MR2.lower().strip()).replace('  ', ' ').split(" "))

                    pop_set = set()
                    for a in phrase_a:
                        if not wn.synsets(a, pos=wn.NOUN):
                            pop_set.add(a)

                    phrase_a = phrase_a - stop
                    phrase_a = phrase_a - pop_set
                    phrase_b = phrase_b - stop

                if phrase_a.issubset(phrase_b):
                    # on garde a priori
                    nb_subset += 1
                    for keeped_value in keeped_values:
                        i_count_contraintes = 0
                        contraintes_MR2 = list(set(keeped_value[3]))

                        if args.simplification:
                            tmp = set(contraintes_MR2)
                            contraintes_MR2 = contraintes_MR1
                            contraintes_MR1 = tmp

                        # application d'un différentiel
                        if True:#conteneur_training_data in ("no_filter/diff_sentences/simplification/multi_sent/training_results", "no_filter/diff_sentences/expansion/multi_sent/training_results"):
                            setCO = set(contraintes_MR2)
                            contraintesOutput = setCO - set(contraintes_MR1)
                            contraintesInput = set(contraintes_MR1) - setCO
                            if not (len(contraintesOutput) <= 1 and len(contraintesInput) <= 1):
                                contraintes_MR2 = []

                        for contrainte_MR2 in contraintes_MR2:

                            # TODO décider
                            if contrainte_MR2 not in contraintes_MR1 or \
                                    (args.constraint_in_output_or_in_input and not args.constraint_somewhere_in_output): #test....
                                i_count_contraintes += 1
                                if contrainte_MR2 not in contraintes_retenues.keys():
                                    contraintes_retenues[contrainte_MR2] = 0
                                contraintes_retenues[contrainte_MR2] += 1

                                key_input = keeped_value[0][0][2]
                                key_input += " "
                                key_input += keeped_value[0][1].replace(" ", "_")
                                key_input += " "
                                key_input += keeped_value[0][2][2]
                                key_input += " "

                                input = contrainte_MR2.replace(" ", "_") + " " + key_input

                                if args.simplification:
                                    key_input_2 = sentence_encoded_MR2
                                    input += sentence_encoded_MR2
                                    output = sentence_encoded_MR1
                                else:
                                    key_input_2 = sentence_encoded_MR1
                                    input += sentence_encoded_MR1
                                    output = sentence_encoded_MR2

                                if "xlid" in input or "xlid" in output:
                                    input = output

                                if input + output not in used_data and not input == output:

                                    used_data.add(input + output)

                                    local_lexicalisations = {}
                                    for lex_key in lexicalisations.keys():
                                        local_lexicalisations[lex_key] = lexicalisations[lex_key].replace(" ", "_")

                                    tmpInput = input.split()[0:4]
                                    input = " ".join(input.split()[4:])
                                    input = " ".join(nltk.wordpunct_tokenize(input))

                                    tt = input.replace(' _ ', '_').split(" ")

                                    if args.shorted_function:
                                        a = tmpInput[0]
                                        sujet_traité = tmpInput[1]
                                        prédicat_traité = tmpInput[2]
                                        objet_traité = tmpInput[3]

                                        valeurs_to_short = {}

                                        if sujet_traité in input and sujet_traité in output:
                                            variableCommune = "sujet" + "_" + prédicat_traité
                                            valeurs_to_short[sujet_traité] = variableCommune
                                            variable_différente = "objet" + "_" + prédicat_traité
                                            valeurs_to_short[objet_traité] = variable_différente
                                            local_lexicalisations[variableCommune] = local_lexicalisations[sujet_traité]
                                            local_lexicalisations[variable_différente] = local_lexicalisations[objet_traité]
                                        else:
                                            variableCommune = "objet" + "_" + prédicat_traité
                                            valeurs_to_short[objet_traité] = variableCommune
                                            variable_différente = "sujet" + "_" + prédicat_traité
                                            valeurs_to_short[sujet_traité] = variable_différente
                                            local_lexicalisations[variableCommune] = local_lexicalisations[objet_traité]
                                            local_lexicalisations[variable_différente] = local_lexicalisations[sujet_traité]

                                        del local_lexicalisations[objet_traité]
                                        del local_lexicalisations[sujet_traité]

                                        input = " ".join(tt)

                                        for old_valeur in valeurs_to_short.keys():
                                            output = output.replace(old_valeur, valeurs_to_short[old_valeur])
                                            input = input.replace(old_valeur, valeurs_to_short[old_valeur])

                                        tmpInput = [a, variable_différente]
                                        tt = input.split(" ")

                                    tt = tmpInput + tt
                                    input = " ".join(tt)

                                    if len(tt) > src_seq_length:
                                        src_seq_length = len(tt)

                                    output = " ".join(nltk.wordpunct_tokenize(output))
                                    tt = output.replace(' _ ', '_').split(" ")
                                    if len(tt) > tgt_seq_length:
                                        tgt_seq_length = len(tt)
                                    output = " ".join(tt)

                                    lexMR1 = input[:]
                                    lexMR2 = output[:]
                                    seen_in_input = input[:]

                                    if args.key_1:
                                        seen_in_input = key_input

                                    if args.key_2:
                                        seen_in_input = key_input_2

                                    for lex_key in local_lexicalisations.keys():
                                        lexMR1 = lexMR1.replace(lex_key, local_lexicalisations[lex_key])
                                        lexMR2 = lexMR2.replace(lex_key, local_lexicalisations[lex_key])
                                        seen_in_input = seen_in_input.replace(lex_key, "")
                                    while "  " in seen_in_input:
                                        seen_in_input = een_in_input = seen_in_input.replace("  ", " ")

                                    #" ".join(tt)

                                    index_train_test_valid = -1
                                    #si la clé est connue
                                    if seen_in_input in MR1_MR2_valid:
                                        index_train_test_valid = 2
                                    elif seen_in_input in MR1_MR2_test:
                                        index_train_test_valid = 1
                                    elif seen_in_input in MR1_MR2_train:
                                        index_train_test_valid = 0

                                    # on s'assure que chaque élément est bien enregistré dans les bases, sinon on enregistre
                                    if index_train_test_valid == 0:
                                        slot = MR1_MR2_train
                                    elif index_train_test_valid == 1:
                                        slot = MR1_MR2_test
                                    elif index_train_test_valid == 2:
                                        slot = MR1_MR2_valid
                                    else:
                                        if args.theme_split:
                                            if 'MR1_MR2_test' not in corpus.keys():
                                                corpus['MR1_MR2_test'] = {}
                                            if 'MR1_MR2_valid' not in corpus.keys():
                                                corpus['MR1_MR2_valid'] = {}
                                            if 'MR1_MR2_train' not in corpus.keys():
                                                corpus['MR1_MR2_train'] = {}
                                            if thème not in corpus['MR1_MR2_test'].keys():
                                                corpus['MR1_MR2_test'][thème] = 0
                                            if thème not in corpus['MR1_MR2_valid'].keys():
                                                corpus['MR1_MR2_valid'][thème] = 0
                                            if thème not in corpus['MR1_MR2_train'].keys():
                                                corpus['MR1_MR2_train'][thème] = 0
                                            if thème not in thèmes_références.keys():
                                                thèmes_références[thème] = taille_max_base_non_train * 2
                                            if corpus['MR1_MR2_valid'][thème] + corpus['MR1_MR2_test'][thème] < thèmes_références[thème] / 5:
                                                if random.randint(1, 5) == 5:
                                                    if corpus['MR1_MR2_test'][thème] < corpus['MR1_MR2_valid'][thème]:
                                                        corpus['MR1_MR2_test'][thème] += 1
                                                        slot = MR1_MR2_test
                                                        index_train_test_valid = 1
                                                    else:
                                                        corpus['MR1_MR2_valid'][thème] += 1
                                                        slot = MR1_MR2_valid
                                                        index_train_test_valid = 2
                                                else:
                                                    corpus['MR1_MR2_train'][thème] += 1
                                                    slot = MR1_MR2_train
                                                    index_train_test_valid = 0
                                            else:
                                                corpus['MR1_MR2_train'][thème] += 1
                                                slot = MR1_MR2_train
                                                index_train_test_valid = 0
                                        else:
                                            index_train_test_valid = 0
                                            slot = MR1_MR2_train
                                            # voyons sir les bases sont suffisamment charnues
                                            if len(MR1_MR2_test) + len(MR1_MR2_valid) < taille_max_base_non_train * 2:
                                                if random.randint(1, 5) == 5:
                                                    if len(MR1_MR2_test) < len(MR1_MR2_valid):
                                                        slot = MR1_MR2_test
                                                        index_train_test_valid = 1
                                                    else:
                                                        slot = MR1_MR2_valid
                                                        index_train_test_valid = 2

                                        if seen_in_input not in slot:
                                            slot.append(seen_in_input)

                                        inputs[index_train_test_valid].append(input)
                                        outputs[index_train_test_valid].append(output)
                                        inputs_lex[index_train_test_valid].append(lexMR1)

                                        if args.simplification:
                                            outputs_meaning[index_train_test_valid].append(each)
                                            inputs_meaning[index_train_test_valid].append(MR2[0])
                                        else:
                                            inputs_meaning[index_train_test_valid].append(each)
                                            outputs_meaning[index_train_test_valid].append(MR2[0])

                                        outputs_lex[index_train_test_valid].append(lexMR2)
                                        outputs_relex[index_train_test_valid].append(local_lexicalisations)
                                        corpus['taille'] += 1

                                        if thème not in corpus.keys():
                                            corpus[thème] = 0
                                        corpus[thème] += 1
                bar.next()
bar.finish()

print()
print("Anomalies : " + count_anomaly.__str__(), end="/")
print(count_keep)
print()

print('subset ok', end = ' : ')
print(nb_subset)
print('corpus ok', end = ' : ')
print(corpus['taille'])

pprint(corpus)

writeJSON(corpus, "corpus.json")


if not args.only_cmd:

    if args.multi_taches or args.triple_2_sentence:
        prefix_train = conteneur_apprentissage + "/train"
        prefix_dev = conteneur_apprentissage + "/dev"
        prefix_test = conteneur_apprentissage + "/test"
        prefix_vocab = conteneur_apprentissage + "/vocab"

        extension_src = ".src"
        extension_tgt1 = ".tgt"
        extension_tgt2 = ".shadow"

        prefix = [prefix_train, prefix_test, prefix_dev]
        suffixe = [extension_src, extension_tgt1, extension_tgt2]

        fjson = open(prefix_vocab + extension_src, "w")
        fjson.write(("\n".join(vocab[0])))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        print(prefix_vocab + extension_src + " écrit")

        fjson = open(prefix_vocab + extension_tgt1, "w")
        fjson.write(("\n".join(vocab[1])))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        print(prefix_vocab + extension_tgt1 + " écrit")

        if args.multi_taches:
            fjson = open(prefix_vocab + extension_tgt2, "w")
            fjson.write(("\n".join(vocab[2])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
            print(prefix_vocab + extension_tgt2 + " écrit")


    for i in range(3):

        if args.multi_taches or args.triple_2_sentence:
            fjson = open(prefix[i] + extension_src, "w")
            fjson.write(("\n".join(inputs[i])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
            print(prefix[i] + extension_src + " écrit")

            fjson = open(prefix[i]  + extension_src + ".lex", "w")
            fjson.write(("\n".join(inputs_lex[i])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
            print(prefix_train + ".lex écrit")

            fjson = open(prefix[i]  + extension_src + ".meaning", "w")
            fjson.write(("\n".join(inputs_meaning[i])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
            print(prefix_train + ".meaning écrit")

            fjson = open(prefix[i]  + extension_tgt1 + ".meaning", "w")
            fjson.write(("\n".join(outputs_meaning[i])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
            print(prefix_train + ".meaning écrit")

            fjson = open(prefix[i] + extension_tgt1, "w")
            fjson.write(("\n".join(outputs[i])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
            print(prefix[i] + extension_tgt1 + " écrit")

            fjson = open(prefix[i] + extension_tgt1 + ".lex", "w")
            fjson.write(("\n".join(outputs_lex[i])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
            print(prefix[i] + extension_tgt1 + ".lex écrit")

            writeJSON(outputs_relex[i], prefix[i] + extension_tgt1 + ".relex", "")
            print(prefix[i] + extension_tgt1 + ".relex écrit")

            if args.multi_taches:
                fjson = open(prefix[i] + extension_tgt2, "w")
                fjson.write(("\n".join(outputs2[i])))
                fjson.flush()
                fjson.close()
                while not fjson.closed:
                    True
                print(prefix[i] + extension_tgt2 + " écrit")

                fjson = open(prefix[i] + extension_tgt2 + ".lex", "w")
                fjson.write(("\n".join(outputs2_lex[i])))
                fjson.flush()
                fjson.close()
                while not fjson.closed:
                    True
                print(prefix[i] + extension_tgt2 + ".lex écrit")

        else:
            file_end = "_train"
            if i == 1:
                file_end = "_test"
            elif i == 2:
                file_end = "_valid"

            fjson = open(conteneur_apprentissage + "/inputs" + file_end + ".data", "w")
            fjson.write(("\n".join(inputs[i])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
            print(conteneur_apprentissage + "/inputs" + file_end + ".data écrit")

            fjson = open(conteneur_apprentissage + "/inputs" + file_end + ".lex", "w")
            fjson.write(("\n".join(inputs_lex[i])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
            print(conteneur_apprentissage + "/inputs" + file_end + ".lex")

            fjson = open(conteneur_apprentissage + "/inputs" + file_end + ".meaning", "w")
            fjson.write(("\n".join(inputs_meaning[i])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
                print(conteneur_apprentissage + "/inputs" + file_end + ".meaning écrit")

            fjson = open(conteneur_apprentissage + "/outputs" + file_end + ".meaning", "w")
            fjson.write(("\n".join(outputs_meaning[i])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
                print(conteneur_apprentissage + "/outputs" + file_end + ".meaning écrit")

            fjson = open(conteneur_apprentissage + "/outputs" + file_end + ".data", "w")
            fjson.write(("\n".join(outputs[i])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
            print(conteneur_apprentissage + "/outputs" + file_end + ".data écrit")

            fjson = open(conteneur_apprentissage + "/outputs" + file_end + ".lex", "w")
            fjson.write(("\n".join(outputs_lex[i])))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
            print(conteneur_apprentissage + "/outputs" + file_end + ".lex écrit")

            writeJSON(outputs_relex[i], file_end + ".relex", conteneur_apprentissage)
            print(file_end + ".relex écrit")

if args.multi_taches:
    for key in variations_lex.keys():
        variations_lex[key] = {"size":len(variations_lex[key]), 'data':list(variations_lex[key])}
    writeJSON(variations_lex,  "variations_lex.json", conteneur_apprentissage)

if not args.only_cmd:
    if args.theme_split:
        writeJSON(MR1_MR2_test, "base_theme_test.json")
        writeJSON(MR1_MR2_valid, "base_theme_valid.json")
        writeJSON(MR1_MR2_train, "base_theme_train.json")
    else:
        writeJSON(MR1_MR2_test, "base_test.json")
        writeJSON(MR1_MR2_valid, "base_valid.json")
        writeJSON(MR1_MR2_train, "base_train.json")
    print("base_train, test.json et base_valid.json écrits." )


pprint(contraintes_retenues)

print("src_seq_length", end = " : ")
pprint(src_seq_length)
print("tgt_seq_length", end = " : ")
pprint(tgt_seq_length)

if args.multi_taches or args.triple_2_sentence:
    if args.multi_taches:
        opennmt = """
cd ~/nmt

python3 -m nmt.nmt  --multitask=True --multitask_type="join" --attention=scaled_luong --tasks_weights="0.6,0.4"   --src=src --tgt=tgt,shadow --vocab_prefix=/home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/vocab --train_prefix=/home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/train --dev_prefix=/home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/dev --test_prefix=/home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/test --out_dir=/home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/training_results --num_train_steps=1000     --steps_per_stats=100 --num_layers=3     --num_units=128     --dropout=0.3 --metrics=bleu
      
    """
    else:
        opennmt = """
cd ~/nmt

python3 -m nmt.nmt  --multitask=False --attention=scaled_luong --src=src --tgt=tgt --vocab_prefix=/home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/vocab --train_prefix=/home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/train --dev_prefix=/home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/dev --test_prefix=/home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/test --out_dir=/home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/training_results --num_train_steps=1000     --steps_per_stats=100 --num_layers=3     --num_units=128     --dropout=0.3 --metrics=bleu
      
    """

else:
    opennmt = """
cd /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_training_data + """/

varbash=$(find ./  -maxdepth 1 -name '*e13.pt' |cut -d ":" -f 2)
if [ "${varbash}" == "" ];then
    varbash2=$(find ./../  -maxdepth 1 -name '*.meaning' |cut -d ":" -f 2)
    if [ "${varbash2}" != "" ];then
        # prepare vocab 
        cd ~/OpenNMT-py
        
        python3 preprocess.py -train_src /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/inputs_train.data -train_tgt /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/outputs_train.data -valid_src /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/inputs_valid.data -valid_tgt /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/outputs_valid.data -src_seq_length """ + src_seq_length.__str__() + """ -tgt_seq_length """ + tgt_seq_length.__str__() + """ -save_data /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_training_data + """/data
        
        THC_CACHING_ALLOCATOR=1 python3 train.py -encoder_type brnn -global_attention general -epochs 13 -data /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_training_data + """/data -save_model /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_training_data + """/data -gpuid 0
        
        ls -R /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_training_data + """/ > /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_training_data + """/epochs.txt
        
        python3 translate.py  -model /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_training_data + """/data_acc_*_e13.pt -src /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/inputs_test.data -output /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_training_data + """/data_test_predictions.txt  -gpu 0 
        
        python3 translate.py  -n_best 5 -model /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_training_data + """/data_acc_*_e13.pt -src /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/inputs_test.data -output /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_training_data + """/data_test_predictions_nbest.txt  -gpu 0 

        python3 translate.py  -model /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_training_data + """/data_acc_*_e13.pt -src /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_apprentissage + """/inputs_train.data -output /home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/""" + conteneur_training_data + """/data_train_predictions.txt  -gpu 0 
    fi
fi

"""

if not args.only_cmd:
    fjson = open(instruction_gpu, "w")
    fjson.write(opennmt)
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True

    print(instruction_gpu + " écrit.")
exit(0)

if args.multi_taches:
    def gestErr():
        erreur = sys.exc_info()
        typerr = u"%s" % (erreur[0])
        typerr = typerr[typerr.find("'") + 1:typerr.rfind("'")]
        print(typerr)
        msgerr = u"%s" % (erreur[1])
        print(msgerr)

    from scipy import stats

    import numpy as np
    import matplotlib.pyplot as plt
    import math

    # '---------- Read Data ----------'

    Taille = np.sort(variations)

    # '---------- Print Descriptive statistics: Continuous Case ----------'

    print(('Taille Dim: ', Taille.shape))
    print(('mean', np.mean(Taille)))
    print(('std', np.std(Taille)))
    print(('std (unbiased): ', np.std(Taille, ddof=1)))
    print(('Median: ', np.median(Taille)))
    print(('Max and Min Value: ', max(Taille), min(Taille)))
    print(('Range: ', max(Taille) - min(Taille)))
    print(('Mode: ', stats.mode(Taille, axis=0)))
    print(('First quartile: ', stats.scoreatpercentile(Taille, 25)))
    print(('Second quartile: ', stats.scoreatpercentile(Taille, 50)))
    print(('Third quartile: ', stats.scoreatpercentile(Taille, 75)))

    # '---------- Discrete Case ----------'

    NbData = Taille.shape[0]
    NbClass = 4  # int( math.log(NbData,2) ) + 1
    Range = max(Taille) - min(Taille)
    ClassRange = float(Range) / NbClass

    print(('NbData: ', NbData))
    print(('NbClass: ', NbClass))
    print(('ClassRange: ', ClassRange))

    X = np.arange(NbClass)
    AbsoluteFrequency = np.zeros(NbClass)
    for i in np.arange(NbData - 1):
        c = int((Taille[i] - min(Taille)) / ClassRange)
        AbsoluteFrequency[c] = AbsoluteFrequency[c] + 1
    AbsoluteFrequency[NbClass - 1] = AbsoluteFrequency[NbClass - 1] + 1

    ClassLabel = []
    j = round(min(Taille), 2)
    for i in np.arange(NbClass + 1):
        ClassLabel.append(j)
        j = round(j + ClassRange, 2)
    LabelList = (ClassLabel)
    x_pos = np.arange(len(LabelList))

    # '---------- Plot Absolute Frequency Histogram ----------'

    try:
        try:
            fig = plt.figure()
        except:
            gestErr()
        plt.xticks(x_pos, LabelList, rotation=45)
        plt.ylabel(r'Absolute Frequency $n_i$')
        bar1 = plt.bar(X, AbsoluteFrequency, \
                       width=1.0, bottom=0, color='Green', alpha=0.65, label='Legend')
        plt.savefig(conteneur_apprentissage + '/Histogram.png', bbox_inches='tight')

        RelativeFrequency = np.zeros(NbClass)
        RelativeFrequency = AbsoluteFrequency / NbData

        # '---------- Plot Cumulative distribution function ----------'

        CumulativeFrequency = np.zeros(NbClass)
        CumulativeFrequency_xStart = np.zeros(NbClass)
        CumulativeFrequency_xEnd = np.zeros(NbClass)
        j = 0
        k = 0
        for i in np.arange(NbClass):
            CumulativeFrequency[i] = j + RelativeFrequency[i]
            j = j + RelativeFrequency[i]
            CumulativeFrequency_xStart[i] = k
            CumulativeFrequency_xEnd[i] = k + 1
            k += 1

        try:
            fig = plt.figure()
        except:
            gestErr()
        for i in np.arange(NbClass):
            plt.plot([CumulativeFrequency_xStart[i], CumulativeFrequency_xEnd[i]], \
                     [CumulativeFrequency[i], CumulativeFrequency[i]], 'k--')
            if i < NbClass - 1:
                plt.scatter(CumulativeFrequency_xEnd[i], CumulativeFrequency[i], \
                            s=80, facecolors='none', edgecolors='r')
            if i == 0:
                plt.plot([CumulativeFrequency_xStart[i], CumulativeFrequency_xEnd[i]], \
                         [0, CumulativeFrequency[i]], 'r--')
            else:
                plt.plot([CumulativeFrequency_xStart[i], CumulativeFrequency_xEnd[i]], \
                         [CumulativeFrequency[i - 1], CumulativeFrequency[i]], 'r--')
        plt.xlim(0, NbClass)
        plt.ylim(0, 1)
        plt.xticks(x_pos, LabelList, rotation=45)
        plt.title("Cumulative Distribution Function")
        plt.savefig(conteneur_apprentissage + '/CumulativeDistributionFunction.png', bbox_inches='tight')


        # '---------- Plot Box Plot ----------'

        try:
            fig = plt.figure()
        except:
            gestErr()
        plt.xticks([0], ['Taille'])
        plt.boxplot(Taille)
        plt.savefig(conteneur_apprentissage + '/BoxPlot.png', bbox_inches='tight')

    except:
        gestErr()
