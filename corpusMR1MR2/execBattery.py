import argparse
import os, subprocess
import multiprocessing as mp
from multiprocessing import Pool
from itertools import  product
import math

parser = argparse.ArgumentParser(description='Batch pour bâtir les corpus.')
parser.add_argument('--built', action='store_true', default=True,
                    help='Lance builtCorpus', )
parser.add_argument('--evaluation', action='store_true',
                    help='Lance evaluationDiversite', )
parser.add_argument('--diversiteMeaning', action='store_true',
                    help='Lance evaluationMeaningDiversite', )
parser.add_argument("-lt", '--little', default=0, type=int,
                    help='Traitements petits fichiers seulement (evaluations uniquement)', )
parser.add_argument("-be", '--big', default=0, type=int,
                    help='Traitements gros fichiers seulement (evaluation uniquement)', )
parser.add_argument('--reverse', action='store_true',
                    help='Invers l\'ordonnancement des demandes', )
parser.add_argument('--only_retreat', action='store_true',
                  help='Prévu pour réaliser le compte de la diversité des demandes quand cela n\'a pas été fait (evaluation uniquement).',)
args = parser.parse_args()



def getData(a):
    text_2_text, data_2_text, simplification, multi_sent, fusion, invert_constraint, theme_split, shorted_function, constraint_in_output_or_in_input, constraint_somewhere_in_output, remove_constrainst, text_2_text_restricted, key_1, key_2, unfilter_words, base_line = a

    retour = []

    if text_2_text:
        retour .append("text_2_text")

    if data_2_text:
        retour.append("data_2_text")

    if simplification:
        retour.append("simplification")

    if multi_sent:
        retour.append("multi_sent")

    if fusion:
        retour.append("fusion")

    if invert_constraint:
        retour.append("invert_constraint")

    if theme_split:
        retour.append("theme_split")

    if shorted_function:
        retour.append("shorted_function")

    if constraint_in_output_or_in_input:
        retour.append("constraint_in_output_or_in_input")

    if constraint_somewhere_in_output:
        retour.append("constraint_somewhere_in_output")

    if remove_constrainst:
        retour.append("remove_constrainst")

    if text_2_text_restricted:
        retour.append("text_2_text_restricted")

    if key_1:
        retour.append("key_1")

    if key_2:
        retour.append("key_2")

    if unfilter_words:
        retour.append('unfilter_words')

    if base_line:
        retour.append('base_line')

    if args.only_retreat:
        retour.append('only_retreat')

    param = " --".join(retour)

    # on va mettre un petit filtre sur des impossibilités...

    count = 0
    if "text_2_text" in param:
        count += 1

    if "data_2_text" in param:
        count += 1
        if "text_2_text_restricted" in param:
            count += 1

    if "text_2_text" in param or"data_2_text" in param:
        if "shorted_function" in param:
            count += 1
        if "theme_split" in param:
            count += 1
        if "invert_constraint" in param:
            count += 1
        if "fusion" in param:
            count += 1

    if "simplification" in param:
        count += 1

    if not "text_2_text" in param and not "data_2_text" in param:
        if "remove_constrainst" in param:
            count+= 1
        if "text_2_text_restricted" in param:
            count += 1

    commande = ''
    if count < 2:
        if len(retour ) > 0:
            param = " --"+param
        if args.evaluation:
            if args.little > 0:
                commande = 'python3 evaluationDiversite.py ' + param + " --little " + args.little.__str__() + " --log"
                p = subprocess.Popen(['python3 evaluationDiversite.py ' + param + " --little " + args.little.__str__() + " --log"], shell=True)
            elif args.big > 0 :
                commande = 'python3 evaluationDiversite.py ' + param + " --big " + args.big.__str__() + " --log"
                print(commande)
                p = subprocess.Popen(['python3 evaluationDiversite.py ' + param + " --big " + args.big.__str__() + " --log"], shell=True)
            else:
                commande = 'python3 evaluationDiversite.py ' + param + " --log"
                p = subprocess.Popen(['python3 evaluationDiversite.py ' + param + " --log"], shell=True)
            retval = p.wait()
        elif args.diversiteMeaning:
            if args.little > 0:
                commande = 'python3 evaluationMeaningDiversite.py ' + param + " --little " + args.little.__str__() + " --log"
                p = subprocess.Popen(['python3 evaluationMeaningDiversite.py ' + param + " --little --log"], shell=True)
            else:
                commande = 'python3 evaluationMeaningDiversite.py ' + param + " --log"
                p = subprocess.Popen(['python3 evaluationMeaningDiversite.py ' + param + " --log"], shell=True)
            retval = p.wait()
        else:
            commande = 'python3 builtCorpus.py ' + param + " --only_cmd"
            p = subprocess.Popen(['python3 builtCorpus.py ' + param + " --only_cmd"], shell=True , stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
            retval = p.wait()
            print(retval.__str__() +" : " + param + " --only_cmd")
            try:
                p.kill
            except:
                pass
            if retval == 0:
                commande += '\npython3 builtCorpus.py ' + param
                p = subprocess.Popen(['python3 builtCorpus.py ' + param ],shell=True , stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
                for line in p.stdout.readlines():
                    print(line.decode("utf-8").strip())

if args.evaluation:

    if os.path.isfile(
            '/home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/statistiques_full.csv'):
        os.remove(
            "/home/colineem/Dropbox/PycharmProjectsMaison/SentencesGenerationWithConstraints/corpusMR1MR2/statistiques_full.csv")



if __name__ == '__main__':

    start = True
    end = False

    if args.reverse:
        start = False
        end = True

    cpus = mp.cpu_count()

    if args.evaluation or args.diversiteMeaning:
        cpus = max(1, cpus / 2)
        if args.little > 0:
            cpus = min(min(2, cpus / 2)+1, cpus)
    else:
        cpus = max(3, cpus / 2)

    pool = Pool(math.floor(cpus))
    pool.map(getData, [a for a in product([start, end], repeat=16)])
