import pygraphviz
import shared
import copy
import re
import itertools

debug=True
class System(object):

    class __node__(object):

        def __init__(self, pos='', word='', inside_allow=True):
            self.pos = pos
            self.word = word
            self.inside_allow = inside_allow
            self.__leftNodes__ = []
            self.__rightNodes__ = []

        def __str__(self):
            label = "node " + str(self.__hash__())
            if self.pos != '' and self.word !='':
                label+=" (POS: " + self.pos +", LBL: " + self.word + ")"
            elif self.pos != '':
                label+= " (POS: " + self.pos + ")"
            elif self.word != '':
                label += " (LBL: " + self.word + ")"
            return label

    class __link__(object):

        def __init__(self, i_node_a, i_node_b, link_value):
            self.i_node_a = i_node_a
            self.i_node_b = i_node_b
            self.link_value = link_value

    class Rule(object):

        def __init__(self, name=''):
            self.name = name
            self.__nodes__ = []
            self.__links__ = []
            self.__forbidden_links__ = []

        def add_node(self, pos='', word='', inside_allow=True):
            node=System.__node__(pos, word, inside_allow)
            self.__nodes__.append(node)
            return node

        def add_link(self, node_a, node_b, nature_link):
            if not isinstance(node_a, System.__node__):
                raise NodeError(node_a.__str__() + " is not a node. Rule " + self.name + ".")
            if not isinstance(node_b, System.__node__):
                raise NodeError(node_b.__str__() + " is not a node. Rule " + self.name + ".")
            if node_a not in self.__nodes__:
                raise NodeError(node_a.__str__() + " not in bunch. Rule " +self.name +".")
            if node_b not in self.__nodes__:
                raise NodeError(node_b.__str__() + " not in bunch. Rule " +self.name +".")
            link=System.__link__(self.__nodes__.index(node_a), self.__nodes__.index(node_b), nature_link)
            self.__links__.append(link)
            return link

        def forbid_link(self, node_a, node_b, nature_link, contrainte_pos_arrivée_flèche=''):
            if not isinstance(node_a, System.__node__):
                raise NodeError(node_a.__str__() + " is not a node. Rule" + self.name + ".")
            if not isinstance(node_b, System.__node__):
                raise NodeError(node_b.__str__() + " is not a node. Rule" + self.name + ".")
            if node_a not in self.__nodes__:
                raise NodeError(node_a.__str__() + " not in bunch. Rule" +self.name +".")
            if node_b not in self.__nodes__:
                raise NodeError(node_b.__str__() + " not in bunch. Rule" +self.name +".")
            link=System.__link__(self.__nodes__.index(node_a), self.__nodes__.index(node_b), nature_link)
            self.__forbidden_links__.append((link, contrainte_pos_arrivée_flèche))
            return link

        def set_position(self, left_node, right_node):

            if isinstance(left_node, System.__node__):
                if left_node in self.__nodes__:
                    left_node.__rightNodes__.append(right_node)
                else:
                    raise NodeError(left_node.__str__() + " not in bunch. Rule " + self.name + ".")
            else:
                raise NodeError(left_node.__str__() + " not a node. Rule " + self.name + ".")

            if isinstance(right_node, System.__node__):
                if right_node in self.__nodes__:
                    right_node.__leftNodes__.append(left_node)
                else:
                    raise right_node(node.__str__() + " not in bunch. Rule " + self.name + ".")
            else:
                raise right_node(node.__str__() + " not a node. Rule " + self.name + ".")


    def __init__(self):
        self.rules=[]

    def add_rule(self, rule):
        if not isinstance(rule, System.Rule):
            raise RuleError(rule.__str__() + ' is not a Rule')

        self.rules.append(rule)

    def create_rule(self, rule_name):
        rule = System.Rule(name=rule_name)
        self.rules.append(rule)
        return rule


    def apply_rules(self, graphe, forbidden_relations):

        def intersect(tab1, tab2):
            return list(set(tab1) & set(tab2))

        graph=pygraphviz.AGraph(graphe.replace("\n", "").replace("\t", "").strip())
        rules_applied={}

        forbidden_links = set()

        rules_done = []

        inside_items=[]
        for each in forbidden_relations:
            inside_items+=each
            for yop in itertools.permutations(each, 2):
                forbidden_links.add(yop[0]+"-"+yop[1])
        inside_items=[int(k) for k in set(inside_items)]

        #des fois on a un lien qui part de zéro, hors ROOT ne fait pas partie des tokens de l'analyse
        if '0' in graph.nodes():
            #for k in graph.iterneighbors('0'):
            #    graph.remove_edge("0", k)
            graph.remove_node("0")

        for rule in self.rules:
            global nodes_candidats_pw
            global all_nodes
            nodes_candidats_pw = {}
            all_nodes = True
            nothing_forbidden = False
            #A: on constitue les groupes de nœuds potentiels.
            for index, node in enumerate(rule.__nodes__):
                nodes_candidats_pw[index]=[]
                if node.pos!='' and node.word !='':
                    if node.pos[0]=="-" and node.pos!="-":
                        a=node.pos[1:]
                        for k in graph.nodes():
                            if not re.match(a, k.attr['pos'])and re.match(node.word, k.attr['word']):
                                if int(k) not in inside_items or (int(k) in inside_items and node.inside_allow):
                                    nodes_candidats_pw[index].append(k)
                    else:
                        for k in graph.nodes():
                            if re.match(node.pos, k.attr['pos'])and re.match(node.word, k.attr['word']):
                                if int(int(k)) not in inside_items or (int(k) in inside_items and node.inside_allow):
                                    nodes_candidats_pw[index].append(k)
                elif node.pos != '':
                    if node.pos[0]=="-" and node.pos!="-":
                        a = node.pos[1:]
                        for k in graph.nodes():
                            if not re.match(a, k.attr['pos']):
                                if int(k) not in inside_items or (int(k) in inside_items and node.inside_allow):
                                    nodes_candidats_pw[index].append(k)
                    else:
                        for k in graph.nodes():
                            if re.match(node.pos, k.attr['pos']):
                                if int(k) not in inside_items or (int(k) in inside_items and node.inside_allow):
                                    nodes_candidats_pw[index].append(k)
                elif node.word != '':
                    for k in graph.nodes():
                        if re.match(node.word, k.attr['word']):
                            if int(k) not in inside_items or (int(k) in inside_items and node.inside_allow):
                                nodes_candidats_pw[index].append(k)
                else:
                    #pas de pos ou word imposés
                    if node.inside_allow:
                        nodes_candidats_pw[index]+=graph.nodes()
                    else:
                        for k in graph.nodes():
                            if int(k) not in inside_items or (int(k) in inside_items and node.inside_allow):
                                nodes_candidats_pw[index].append(k)
                #il faut ce noeud dans le graph, il n'y est pas... on sort.
                if len(nodes_candidats_pw[index])==0:
                    all_nodes = False

            if all_nodes:
                #maintenant qu'on a tout nos noeuds, on va vérifier qu'ils respectent les notions de gauche et droite
                #on traite que les liens gauche vu que les deux sont systématiquement renseignés.

                global links
                links=[]
                for index, node in enumerate(rule.__nodes__):
                    if all_nodes:
                        if len(node.__leftNodes__) > 0:
                            for left_node in node.__leftNodes__:
                                index_du_tas_contenu_gauche = rule.__nodes__.index(left_node)
                                links.append(System.__link__(index, index_du_tas_contenu_gauche, "gauche"))

                def verifie_position():
                    global links
                    global nodes_candidats_pw
                    global all_nodes

                    if len(links)>0:


                        available_relations = set()
                        for index, link in enumerate(links):
                            nature_lien = link.link_value
                            for node_a in nodes_candidats_pw[int(link.i_node_a)]:
                                for node_b in nodes_candidats_pw[int(link.i_node_b)]:
                                    available_relations.add((node_a, node_b))

                        compare=set()
                        while len(available_relations - compare) != 0 and all_nodes:
                            available_relations = compare
                            compare=set()
                            wanted_relations = {}

                            for index, link in enumerate(links):
                                # pour une nature de lien donné
                                nature_lien = link.link_value
                                wanted_relations[index] = []
                                for node_a in nodes_candidats_pw[int(link.i_node_a)]:
                                    for node_b in nodes_candidats_pw[int(link.i_node_b)]:
                                        if nature_lien == "gauche":
                                            if int(node_b) < int(node_a) and node_a.__str__() + "-" + node_b.__str__() not in forbidden_links:
                                                wanted_relations[index].append((node_a, node_b))
                                                compare.add((node_a, node_b))

                            if 0 in [len(tab) for tab in wanted_relations.values()]:
                                all_nodes = False
                            else:
                                # on ne touche pas à ce qui n'est pas contraint par une règle de lien obligatoire
                                for index, link in enumerate(links):
                                    nodes_candidats_pw[link.i_node_a] = intersect(nodes_candidats_pw[link.i_node_a], [node_a for node_a, node_b in wanted_relations[index]])
                                    nodes_candidats_pw[link.i_node_b] = intersect(nodes_candidats_pw[link.i_node_b], [node_b for node_a, node_b in wanted_relations[index]])
                                if 0 in [len(tab) for tab in nodes_candidats_pw.values()]:
                                    all_nodes = False

                verifie_position()

                if len(rule.__links__)>0 and all_nodes:
                    wanted_relations={}
                    for index, link in enumerate(rule.__links__):
                        # pour une nature de lien donné
                        nature_lien = link.link_value
                        wanted_relations[index]=[]
                        for node_a in nodes_candidats_pw[int(link.i_node_a)]:
                            for node_b in nodes_candidats_pw[int(link.i_node_b)]:
                                left = True
                                right = True
                                # si l'indice du lot de noeud b est parmi les lots qui doivent être à gauche de notre noeud a
                                #sinon la relation n'est pas valide
                                if link.i_node_b in rule.__nodes__[link.i_node_a].__leftNodes__:
                                    if int(node_b.__str__()) >= int(node_a.__str__()):
                                        left = False
                                if link.i_node_b in rule.__nodes__[link.i_node_a].__rightNodes__:
                                    if int(node_b.__str__()) <= int(node_a.__str__()):
                                        right = False
                                if right and left:
                                    if graph.has_edge(node_a, node_b) and node_a.__str__() + "-" + node_b.__str__() not in forbidden_links:
                                        if nature_lien == "" or nature_lien == "*":
                                            wanted_relations[index].append((node_a, node_b))
                                        elif graph.has_edge(node_a, node_b, key=nature_lien):
                                            wanted_relations[index].append((node_a, node_b))
                                        elif nature_lien.endswith(".*"):
                                            edge = graph.get_edge(node_a, node_b)
                                            if re.match(nature_lien, edge.key):
                                                wanted_relations[index].append((node_a, node_b))




                    if 0 in [len(tab) for tab in wanted_relations.values()]:
                        all_nodes=False
                    else:
                        #on ne touche pas à ce qui n'est pas contraint par une règle de lien obligatoire
                        for index, link in enumerate(rule.__links__):
                            nodes_candidats_pw[link.i_node_a] = intersect(nodes_candidats_pw[link.i_node_a] , [node_a for node_a, node_b in wanted_relations[index]])
                            nodes_candidats_pw[link.i_node_b] = intersect(nodes_candidats_pw[link.i_node_b] , [node_b for node_a, node_b in wanted_relations[index]])
                        if 0 in [len(tab) for tab in nodes_candidats_pw.values()]:
                            all_nodes = False

                        #on sait que les relations ne sont prise en compte qu'entre nœuds correctement postionnés.


                if all_nodes:
                    nothing_forbidden = True
                    for link, contrainte_arrivée_flèche in rule.__forbidden_links__:
                        if nothing_forbidden:
                            indice_noeud_a = link.i_node_a
                            indice_noeud_b = link.i_node_b
                            nature_lien = link.link_value
                            pop_from_a = []
                            for node_a in nodes_candidats_pw[indice_noeud_a]:
                                forbidden_found = False
                                for node_b in nodes_candidats_pw[indice_noeud_b]:
                                    if contrainte_arrivée_flèche == "" or contrainte_arrivée_flèche == graph.get_node(node_b).attr['pos']:
                                        if graph.has_edge(node_a, node_b) and node_a.__str__() + "-" + node_b.__str__() not in forbidden_links:
                                            if (nature_lien == "" or nature_lien == "*") :
                                                forbidden_found = True
                                            elif graph.has_edge(node_a, node_b, key=nature_lien):
                                                forbidden_found = True
                                            elif nature_lien.endswith(".*"):
                                                edge = graph.get_edge(node_a, node_b)
                                                if re.match(nature_lien, edge.key):
                                                    forbidden_found = True

                                # du moment que le lien existe, c'est que a on n'en veut pas...
                                if forbidden_found:
                                    pop_from_a.append(node_a)

                            if len(pop_from_a) == len(nodes_candidats_pw[indice_noeud_a]):
                                nodes_candidats_pw[indice_noeud_a].clear()
                                nothing_forbidden = False
                                break
                            else:
                                for each in pop_from_a:
                                    nodes_candidats_pw[indice_noeud_a].remove(each)
                    verifie_position()

                if 0 in [len(tab) for tab in nodes_candidats_pw.values()]:
                    all_nodes = False
                elif all_nodes:

                    if not debug:
                        rule_name=shared.nettoie(rule.name)
                    else:
                        rule_name = rule.name
                    if rule_name not in rules_applied.keys():
                        rules_applied[rule_name]=[]

                    str_rule_results=rule_name
                    for index, each in enumerate(rule.__nodes__):
                        str_rule_results += "("+each.pos + "/" + each.word +")"+ ", ".join(nodes_candidats_pw[index])

                    if str_rule_results not in rules_done:
                        rules_applied[rule_name].append({})

                        for index, each in enumerate(rule.__nodes__):
                            rules_applied[rule_name][-1][index.__str__() + "("+each.pos + "/" + each.word +")"]=", ".join(nodes_candidats_pw[index])


                        rules_done.append(str_rule_results)


        return rules_applied









class NodeError(shared.Error):
    """Exception raised for errors in the input.

    Attributes:
        expr -- input expression in which the error occurred
        msg  -- explanation of the error
    """

    def __init__(self,  msg):
        self.msg = msg

class RuleError(shared.Error):
    """Exception raised for errors in the input.

    Attributes:
        expr -- input expression in which the error occurred
        msg  -- explanation of the error
    """

    def __init__(self, msg):
        self.msg = msg

#struture test
if __name__ == '__main__':
    a = System.__node__(pos='V.*')

    b = System.__node__(pos='V.*')
    print(b in[a])
    print(a in [a])

    #les ID de nos deux noeuds sont bien différents
    print(a==b)
    print(a)
