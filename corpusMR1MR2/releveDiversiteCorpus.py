from xml.etree import ElementTree
import nltk
from progress.bar import Bar
import argparse

parser = argparse.ArgumentParser(description='Produit un relevé des contraintes propres au corpus.')

args=parser.parse_args()

file = "../prepareCorpus/webnlgparser_data/working_data.xml"

# chargement des données
tree = ElementTree.parse(file)
root = tree.getroot()
items_syntaxiques = {}

MR = {}
usedPrédicats = {}

exemples = root.findall('exemple')

used_data = set()

src_seq_length = 0
tgt_seq_length = 0
print()
print("Chargement des données")
bar = Bar('Processing', max=len(exemples))

data = ["individu,theme,nbSentences,nbtriplets,"]
data_contraintes = ["individu,theme,contrainte,presence"]
contraintes = ['twice objects', 'indirect object', 'juxtaposition', 'relative object', 'coordinated full clauses', 'relative adverb', 'coordinated clauses', 'apposition', 'possessif', 'relative subject', 'direct object', 'passive voice', 'existential', 'contraintes']

dat_chi = {}


data[-1] += ",".join(contraintes)
data_relations = [data[-1][:]+ ",relation,"]

contraintes_subjet = contraintes[:]
contraintes_objet = contraintes[:]
for i in range(len(contraintes)):
    contraintes_subjet[i] = "sujet " + contraintes_subjet[i]
    contraintes_objet[i] = "objet " + contraintes_objet[i]
data_relations[-1] += ",".join(contraintes_subjet) + ","
data_relations[-1] += ",".join(contraintes_objet)


encoded_seen = set()
for exemple in exemples:
    sentences_count = int(exemple.attrib['sentences'])
    inde = int(exemple.attrib['sentences'])

    encoded = exemple.find('encoded').text.strip()
    encoded = " ".join(nltk.wordpunct_tokenize(encoded))

    if encoded not in encoded_seen:
        # <exemple index="0" sentences="1" source="2triples_MeanOfTransportation_1020950_BMK_cleaned.xml" theme="MeanOfTransportation" triplets="2">
        encoded_seen.add(encoded)

        socle_data = "ind " + (len(data)-1).__str__() + ","
        socle_data += exemple.attrib['theme'] + ","
        if exemple.attrib['theme'] not in dat_chi.keys():
            dat_chi[exemple.attrib['theme']] = {}
            for contrainte in contraintes:
                dat_chi[exemple.attrib['theme']][contrainte] = 0
            dat_chi[exemple.attrib['theme']]["sans contrainte"] = 0

        socle_data_contraintes = socle_data[:]
        socle_data += exemple.attrib['sentences'] + ","
        socle_data += exemple.attrib['triplets'] + ","

        base_data = socle_data[:]
        origin = exemple.find('origin').text.strip()
        thème = exemple.attrib['theme']

        nbTriplets = exemple.attrib['triplets']
        predicats = set()
        elements = {}

        for entry in exemple.findall('relations'):

            for relation in entry.findall('relation'):
                prédicat = relation.attrib['reference']
                predicats.add(prédicat)

        subj_objs = {}
        linear_mr = []
        data_relation = []

        encoded = exemple.find('encoded').text.strip()
        encoded = " ".join(nltk.wordpunct_tokenize(encoded))

        names = set()

        tokens_touches = {}
        for constraint in exemple.findall('constraints/contrainte'):
            name = constraint.attrib['name']
            if name not in ('participle clause subject', 'participle clause'):
                names.add(name)
                if name not in tokens_touches:
                    tokens_touches[name] = set()
                for socle in constraint.findall('socle'):
                    for token in socle.findall('node/token'):
                        index = token.attrib['index']
                        tokens_touches[name].add(token)

        for constraint in contraintes[:-1]:
            if constraint in names:
                base_data += '1,'
                data_contraintes.append(socle_data_contraintes + constraint + ",1")
                dat_chi[exemple.attrib['theme']][contrainte] += 1
            else:
                base_data += '0,'
                data_contraintes.append(socle_data_contraintes + constraint + ",0")
        base_data += len(names).__str__()
        if len(names) == 0:
            dat_chi[exemple.attrib['theme']]["sans contrainte"] += 1

        for relation in exemple.findall('relations/relation'):
            data_relation = base_data[:] + ","
            data_relation += '"' + relation.attrib['reference'] + '",'
            tokens_sujet = set()
            for token_sujet in relation.findall('subject/token'):
                identification = token_sujet.attrib['identification']
                index = token_sujet.attrib['index']
                tokens_sujet.add(index)
            tokens_objet = set()
            for token_objet in relation.findall('object/token'):
                identification = token_objet.attrib['identification']
                index = token_objet.attrib['index']
                tokens_objet.add(index)
            count_touche = 0
            for constraint in contraintes[:-1]:
                if constraint in names:
                    if len(tokens_sujet.intersection(tokens_touches))>0:
                        data_relation += '1,'
                        count_touche += 1
                    else:
                        data_relation += '0,'
                else:
                    data_relation += '0,'
            data_relation += count_touche.__str__() + ","
            count_touche = 0
            for constraint in contraintes[:-1]:
                if constraint in names:
                    if len(tokens_objet.intersection(tokens_touches))>0:
                        data_relation += '1,'
                        count_touche += 1
                    else:
                        data_relation += '0,'
                else:
                    data_relation += '0,'
            data_relation += count_touche.__str__()
            data_relations.append(data_relation)

        data.append(base_data)

    bar.next()
bar.finish()

for i in range(len(data)):
    data[i] = data[i].replace(" ", "_")

for i in range(len(data_relations)):
    data_relations[i] = data_relations[i].replace(" ", "_")

fjson = open("data_corpus.csv", "w")
fjson.write(("\n".join(data)))
fjson.flush()
fjson.close()
while not fjson.closed:
    True
print("données corpus écrites")

fjson = open("data_relations.csv", "w")
fjson.write(("\n".join(data_relations)))
fjson.flush()
fjson.close()
while not fjson.closed:
    True
print("données relations écrites")

fjson = open("data_contraintes.csv", "w")
fjson.write(("\n".join(data_contraintes)))
fjson.flush()
fjson.close()
while not fjson.closed:
    True
print("données contraintes écrites")

"""
 # Créations des vecteurs correspondant aux 2 catégories :
hommes = c(50,70,110,60)
femmes = c(80,75,100,30)
# Création d'une matrice comparative :
tableau = matrix(c(hommes, femmes),2,4,byrow=T) # (2 : nombre de lignes et 4 nombres de colonnes (tranches salariales))
# Réalisation du test khi-deux - les résultats sont sauvegardés dans "khi_test"
khi_test = chisq.test(tableau)
khi_test # affiche le résultat du test

"""