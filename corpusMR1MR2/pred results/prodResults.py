import os
import json
from nltk.tokenize.moses import MosesDetokenizer
from tqdm import tqdm
from pprint import pprint

common_meaning = {}

detokenizer = MosesDetokenizer()

directories = {}

for dossier, sous_dossiers, fichiers in os.walk('.'):
    if "_test.relex" in fichiers:
        directories[dossier] = {
            'relex':'_test.relex',
            'output': 'training_results/data_test_predictions.txt',
            'input': 'inputs_test.data',
            'meaning': 'outputs_test.meaning',
        }
        if os.path.isfile(os.path.join(dossier, "training_results/data_test_predictions_nbest5.txt")):
            directories[dossier]['output'] =  "training_results/data_test_predictions_nbest5.txt"

# construction d'un meaning commun

keys = directories.keys()
values_keys_dic = {"in":{},"out":{}}

#traitement des fichiers
for key in keys:

    état = key[key.rindex('/') + 1:]

    meaning = os.path.join(key, directories[key]['meaning'])


    if état != "ALL":

        with open(meaning) as f:
            meanings = [k.strip() for k in f.readlines()]
        f.close()

        #on va reconstruire, c'est trop pénible d'en perdre
        with open(état + "/outputs_test.lex") as f:
            outputs_relex = [k.strip() for k in f.readlines()]
        f.close()
        with open(état + "/outputs_test.data") as f:
            outputs_data = [k.strip() for k in f.readlines()]
        f.close()

        #on va reconstruire, c'est trop pénible d'en perdre
        with open(état + "/inputs_test.lex") as f:
            inputs_relex = [k.strip() for k in f.readlines()]
        f.close()
        with open(état + "/inputs_test.data") as f:
            inputs_data = [k.strip() for k in f.readlines()]
        f.close()

        relexs_output = []


        for output_relex, output_data, meaning in zip(outputs_relex, outputs_data, meanings):
            output_relex = output_relex.split()
            output_data = output_data.split()

            relexs_output.append({})
            output_meaning_key = set()
            values_keys = set()

            for a, b in zip(output_relex, output_data):
                if "Sujet" in b or "Objet" in b:
                    relexs_output[-1][b] = a
                    output_meaning_key.add(b)
                    values_keys.add(a)
                    if a not in values_keys_dic['out'].keys():
                        values_keys_dic['out'][a] = 0
                    values_keys_dic['out'][a] += 1

            if meaning not in common_meaning.keys():
                common_meaning[meaning] = {}

            output_meaning_key = "".join(sorted(output_meaning_key))

            if output_meaning_key not in common_meaning[meaning].keys():
                common_meaning[meaning][output_meaning_key] = []

            if relexs_output[-1] not in common_meaning[meaning][output_meaning_key]:
                common_meaning[meaning][output_meaning_key].append(relexs_output[-1])

        relexs_input = []

        for input_relex, input_data, meaning in zip(inputs_relex, inputs_data,  meanings):
            i = input_relex[:]
            j = input_data[:]
            input_relex = input_relex.split()
            input_data = input_data.split()

            relexs_input.append({})

            input_meaning_key = set()
            values_keys = set()

            for a, b in zip(input_relex, input_data):
                if "Sujet" in b or "Objet" in b:
                    relexs_input[-1][b] = a
                    input_meaning_key.add(b)
                    values_keys.add(a)
                    if a not in values_keys_dic['in'].keys():
                        values_keys_dic['in'][a] = 0
                    values_keys_dic['in'][a] += 1

            input_meaning_key = "".join(sorted(input_meaning_key))

            if input_meaning_key not in common_meaning[meaning].keys():
                common_meaning[meaning][input_meaning_key] = []

            if relexs_input[-1] not in common_meaning[meaning][input_meaning_key]:
                common_meaning[meaning][input_meaning_key].append(relexs_input[-1])

values_keys_dic["in"] = sorted(values_keys_dic["in"].items(), key=lambda x:x[1])
values_keys_dic["out"] = sorted(values_keys_dic["out"].items(), key=lambda x:x[1])
# values_keys_dic_copy = dict((x, y) for x, y in values_keys_dic)
values_keys_dic["in"] = dict((x, y) for x, y in values_keys_dic["in"])
values_keys_dic["out"] = dict((x, y) for x, y in values_keys_dic["out"])


#
# tmp = {}
# selected = []
# for meaning in common_meaning.keys():
#     tmp[meaning] = {}
#     for meaning_key_in, meaning_key_out in zip(common_meaning[meaning]["in"].keys(), common_meaning[meaning]["out"].keys()):
#         if meaning_key_in not in tmp[meaning].keys():
#             tmp[meaning][meaning_key_in] = []
#         if meaning_key_out not in tmp[meaning].keys():
#             tmp[meaning][meaning_key_out] = []
#         value_selec_in = common_meaning[meaning]["in"][meaning_key_in][0]
#         value_selec_out = common_meaning[meaning]["out"][meaning_key_out][0]
#         value_count = -1
#         found = False
#         for dico_in in common_meaning[meaning]["in"][meaning_key_in]:
#             for dico_out in common_meaning[meaning]["out"][meaning_key_out]:
#                 tmp_count = len(set(dico_in.values()).intersection(dico_out.values()))
#                 if tmp_count > value_count and (dico_in in selected and dico_out in selected):
#                     found = True
#                     value_selec_in = dico_in
#                     value_selec_out = dico_out
#                     value_count = tmp_count
#                 elif not found:
#                     if tmp_count > value_count :
#                         value_selec_in = dico_in
#                         value_selec_out = dico_out
#                         value_count = tmp_count
#         selected.append(dico_in)
#         selected.append(dico_out)
#         tmp[meaning][meaning_key_in].append(value_selec_in)
#         tmp[meaning][meaning_key_out].append(value_selec_out)
#
# common_meaning = tmp

sorties = {}

#traitement des fichiers
for key in keys:

    état = key[key.rindex('/') + 1:]
    sorties[état] = {}

    relex = os.path.join(key, directories[key]['relex'])
    output = os.path.join(key, directories[key]['output'])
    input = os.path.join(key, directories[key]['input'])
    meaning = os.path.join(key, directories[key]['meaning'])


    with open(input) as f:
        inputs =  [k.strip() for k in f.readlines()]
    f.close()
    with open(meaning) as f:
        meanings =  [k.strip() for k in f.readlines()]
    f.close()

    with open(relex, 'r') as f:
        relexs = json.load(f)

    if "nbest5" in output:
        t = []
        m = []
        r = []
        for line, meaning, relex in zip(inputs, meanings, relexs):
            for _ in range(5):
                t.append(line.strip())
                m.append(meaning.strip())
                r.append(relex)
        inputs = t
        meanings = m
        relexs = r
        del t, m

    with open(output) as f:
        outputs = [k.strip() for k in f.readlines()]

    meaning_shared = {}
    already_dones = []
    for input, output, meaning, i_tQ in zip(inputs, outputs, meanings, tqdm(range(len(meanings)))):
        if input + output not in already_dones:
            already_dones.append(input+output)
            if meaning not in sorties[état]:
                sorties[état][meaning] = [[], [], [], [], []]

            input_relex = input[:]
            output_relex = output[:]

            take = True

            input_meaning_key = set()
            output_meaning_key = set()

            for b in input_relex[:].split():
                if "Sujet" in b or "Objet" in b:
                    input_meaning_key.add(b)
            input_meaning_key = "".join(sorted(input_meaning_key))

            for b in output_relex[:].split():
                if "Sujet" in b or "Objet" in b:
                    output_meaning_key.add(b)
            output_meaning_key = "".join(sorted(output_meaning_key))

            max_inter = -1
            sel_i = ""
            sel_o = ""
            max_freq = 0
            subset_active = False
            if output_meaning_key in common_meaning[meaning].keys() and input_meaning_key in common_meaning[meaning].keys():
                for input_m in common_meaning[meaning][input_meaning_key]:
                    for output_m in common_meaning[meaning][output_meaning_key]:

                        set_i = set(input_m.values())
                        set_o = set(output_m.values())

                        value_freq = 0
                        for item_to_freq in set_i:
                            try:
                                value_freq += values_keys_dic["in"][item_to_freq]
                            except:
                                value_freq += values_keys_dic["out"][item_to_freq]
                        for item_to_freq in set_o:
                            try:
                                value_freq += values_keys_dic["out"][item_to_freq]
                            except:
                                value_freq += values_keys_dic["in"][item_to_freq]

                        if set_i.issubset(set_o) or set_o.issubset(set_i):
                            if not subset_active:
                                sel_i = input_m
                                sel_o = output_m
                                subset_active = True
                                max_freq = value_freq
                            else:
                                if max_freq < value_freq:
                                    max_freq = value_freq
                                    sel_i = input_m
                                    sel_o = output_m


                        l_int = len(set_o.intersection(set_i))
                        if not subset_active:
                            if l_int > max_inter:
                                max_inter = l_int
                                sel_i = input_m
                                sel_o = output_m
                            elif l_int == max_inter and value_freq> max_freq:
                                max_freq = value_freq
                                sel_i = input_m
                                sel_o = output_m

            else:
                take = False

            if take:
                if input_meaning_key in common_meaning[meaning].keys():
                    for anonym in sel_i:
                        if anonym in input_relex:
                            input_relex = input_relex.replace(anonym, sel_i[anonym]).replace("_", " ")
                        else:
                            take = False
                else:
                    take = False
                meaning_relex = meaning[:]
                if output_meaning_key in common_meaning[meaning].keys():
                    for anonym in sel_o.keys():
                        if anonym not in output_relex:
                            take = False
                            break
                        else:
                            output_relex = output_relex.replace(anonym, sel_o[anonym]).replace("_", " ")
                            meaning_relex = meaning_relex.replace(anonym, sel_o[anonym]).replace("_", " ")
                else:
                    take = False

                if "Sujet" in output_relex or "Objet" in output_relex:
                    take = False

            if take:
                sorties[état][meaning][0].append(input)
                sorties[état][meaning][1].append(detokenizer.detokenize(input_relex.split(' '), return_str=True))
                sorties[état][meaning][2].append(detokenizer.detokenize(output_relex.split(' '), return_str=True))
                sorties[état][meaning][3].append(meaning)
                sorties[état][meaning][4].append(meaning_relex)




for état in sorties.keys():
    os.makedirs('./sorties_exploitables_papier/' + état , exist_ok=True)
    inputs = open('./sorties_exploitables_papier/' + état + '/inputs.data', 'w')
    inputs_relex = open('./sorties_exploitables_papier/' + état + '/inputs.relex', 'w')
    outputs_relex = open('./sorties_exploitables_papier/' + état + '/outputs.relex', 'w')
    meanings_data = open('./sorties_exploitables_papier/' + état + '/meaning.data', 'w')
    meanings_relex = open('./sorties_exploitables_papier/' + état + '/meaning.relex', 'w')
    store = []
    for meaning in sorted(sorties[état].keys()):
        for i in range(len(sorties[état][meaning][0])):
            if meaning + sorties[état][meaning][2][i] not in store:
                store.append(meaning + sorties[état][meaning][3][i])
                inputs.write(sorties[état][meaning][0][i])
                inputs.write('\n')
                inputs_relex.write(sorties[état][meaning][1][i])
                inputs_relex.write('\n')
                outputs_relex.write(sorties[état][meaning][2][i])
                outputs_relex.write('\n')
                meanings_data.write(sorties[état][meaning][3][i])
                meanings_data.write('\n')
                meanings_relex.write(sorties[état][meaning][4][i])
                meanings_relex.write('\n')

    inputs.close()
    inputs_relex.close()
    outputs_relex.close()
    meanings_data.close()
    meanings_relex.close()

