existential 1955_Dodge alternativeName "Dodge_Coronet" The 1955_Dodge , also known as the "Dodge_Coronet" , has a Hardtop and a V8_engine .
possessif Audi_A1 assembly Audi_Brussels The Audi_A1 is a Hatchback assembled by Audi_Brussels and has a 1.2_litres engine .
coordinated_full_clauses Audi_A1 assembly Audi_Brussels The Audi_A1 is a Hatchback assembled by Audi_Brussels and has a 1.2_litres engine .
relative_subject Abarth_1000_GT_Coupé wheelbase 2160.0_millimetres The "Two_door_coupé" Abarth_1000_GT_Coupé , with a Straight-four_engine , has a 2160.0_millimetres wheelbase .
relative_subject Audi_A1 assembly "Brussels_,_Belgium" The Hatchback Audi_A1 , assembled in "Brussels_,_Belgium" , has a 1.2_litres liter engine .
apposition 1955_Dodge transmission "3-speed_automatic" The 1955_Dodge has a V8_engine , a "3-speed_automatic" transmission and a Station_wagon style body .
apposition Abarth_1000_GT_Coupé productionEndYear 1958 The Abarth_1000_GT_Coupé has a Straight-four_engine and a Coupé body style . The last of this model , rolled off the production line in 1958 .
relative_subject Abarth_1000_GT_Coupé wheelbase 2160.0_millimetres The Abarth_1000_GT_Coupé is a "Two_door_coupé" model with a Straight-four_engine and a 2160.0_millimetres wheelbase .
direct_object Alfa_Romeo_164 class Executive_car The Alfa_Romeo_164 , or "Alfa_Romeo_168" . is an Executive_car with a Straight-four_engine .
relative_subject Aston_Martin_V8 successor Aston_Martin_Virage Aston_Martin_V8 , succeeded by Aston_Martin_Virage , is manufactured by Aston_Martin . Both Aston_Martin_V8 and the Aston_Martin_RHAM/1 are vehicles .
relative_subject DeSoto_Custom manufacturer DeSoto_(automobile) The DeSoto_Custom was manufactured at DeSoto_(automobile) and is a related means of transport to the 1955_Dodge and the Chrysler_Newport .
passive_voice DeSoto_Custom manufacturer DeSoto_(automobile) DeSoto_(automobile) are the manufacturers of the DeSoto_Custom which is a related means of transportation to the 1955_Dodge and Chrysler_Newport .
relative_subject Alfa_Romeo_164 assembly Milan The Alfa_Romeo_164 ( manufactured in Milan ) , Lancia_Thema and the Fiat_Croma are related means of transportation .
direct_object American_Locomotive_Company foundationPlace Schenectady_,_New_York The ALCO_RS-3 was built by the United_States American_Locomotive_Company which was founded in Schenectady_,_New_York .
existential Neptun_Werft locationCity Rostock The A-Rosa_Luna was built by Neptun_Werft which is headquartered in Rostock , Germany .
apposition A-Rosa_Luna shipDisplacement 1850.0_tonnes The A-Rosa_Luna is 125800.0_millimetres in length and weighs 1850.0_tonnes . It was completed 2005-04-06 .
existential ALCO_RS-3 builder American_Locomotive_Company The ALCO_RS-3 with a Diesel-electric_transmission and 17068.8_millimetres long , is made by the American_Locomotive_Company .
existential Humphrys_,_Tennant_and_Dykes location Deptford The Alhambra , which had the length of 63800.0_millimetres , has a Humphrys_,_Tennant_and_Dykes power type in Deptford .
coordinated_clauses A-Rosa_Luna length 125800.0_millimetres With a top speed of 24.0 , the ship A-Rosa_Luna is 125800.0_millimetres long and weighs 1850.0_tonnes .
apposition AIDAstella owner Costa_Crociere The AIDAstella is owned by Costa_Crociere and it ' s operator is Germany based AIDA_Cruises .
existential AIDAstella builder Meyer_Werft AIDA_Cruises , based in Germany , operates the ship AIDAstella which was built by Meyer_Werft .
existential Alhambra powerType Humphrys_,_Tennant_and_Dykes The Alhambra , located in London and owned by P&O_(company) , is powered by a Humphrys_,_Tennant_and_Dykes engine .
direct_object AIDAluna powerType Caterpillar_Inc AIDA_Cruises ( located in Germany ) is the owner of the AIDAluna which is powered by Caterpillar_Inc .
passive_voice AIDAluna powerType Caterpillar_Inc Rostock based AIDA_Cruises owns the AIDAluna which has a Caterpillar_Inc engine .
coordinated_clauses American_submarine_NR-1 shipBeam 3.8_m The American_submarine_NR-1 is 45000.0_millimetres long , has a top speed of 8.334 and a 3.8_m ship beam .
relative_subject AIDAstella christeningDate 2013-03-16 The christening date of AIDAstella is 2013-03-16 , its service began 2013-03-17 . It measures 253260.0_millimetres in length .
coordinated_clauses ALCO_RS-3 buildDate "May_1950_-_August_1956" The ALCO_RS-3 , which has a V12_engine , was produced between "May_1950_-_August_1956" by the American_Locomotive_Company .
coordinated_clauses ALCO_RS-3 length 17068.8_millimetres The ALCO_RS-3 was built by the Montreal_Locomotive_Works . It has a Four-stroke_engine and is 17068.8_millimetres in length .
relative_subject ALCO_RS-3 buildDate "May_1950_-_August_1956" The ALCO_RS-3 was built by the Montreal_Locomotive_Works between "May_1950_-_August_1956" . It has a Four-stroke_engine .
passive_voice AIDAstella shipBeam 32.2 Built by Meyer_Werft , the AIDAstella is 253260.0_millimetres long and has a beam of 32.2 m .
coordinated_full_clauses A-Rosa_Luna shipDisplacement 1850.0_tonnes The A-Rosa_Luna was built by Neptun_Werft_,_Warnemünde , weighs 1850.0_tonnes and is 125800.0_millimetres long .
coordinated_full_clauses ALCO_RS-3 engine Four-stroke_engine The ALCO_RS-3 was built by the Montreal_Locomotive_Works . It has a Four-stroke_engine and is 17068.8_millimetres in length .
passive_voice AIDAstella shipBeam 32.2 Meyer_Werft built the AIDAstella which is 253260.0_millimetres long with a beam of 32.2 .
relative_subject ALCO_RS-3 engine V12_engine The ALCO_RS-3 has a V12_engine and is 17068.8_millimetres in length . It was built by Montreal_Locomotive_Works .
relative_subject ALCO_RS-3 cylinderCount 12 The ALCO_RS-3 was built by the Montreal_Locomotive_Works , has a cylinder count of 12 and is 17068.8_millimetres in length .
relative_subject Humphrys_,_Tennant_and_Dykes location London Built by the Samuda_Brothers , the Alhambra has a Humphrys_,_Tennant_and_Dykes power type . Humphrys_,_Tennant_and_Dykes is located in London .
passive_voice ALCO_RS-3 length 17068.8_millimetres The American_Locomotive_Company , founded in Schenectady_,_New_York , were the builders of the ALCO_RS-3 , which has a length of 17068.8_millimetres .
direct_object ALCO_RS-3 length 17068.8_millimetres The ALCO_RS-3 was built by the United_States founded American_Locomotive_Company and is 17068.8_millimetres long .
relative_adverb United_States ethnicGroup Asian_Americans The United_States , with its capital of Washington , is home to Asian_Americans and The Atlas_II .
apposition United_States ethnicGroup African_Americans The ALV_X-1 comes from the United_States where African_Americans are an ethnic group and Washington is the capital .
relative_adverb United_States ethnicGroup Native_Americans The United_States is home to Native_Americans and Americans as well as the origin of the ALV_X-1 .
passive_voice United_States ethnicGroup Asian_Americans ALV_X-1 comes from the United_States where Americans live and where Asian_Americans are one of the ethnic groups .
relative_adverb United_States demonym Americans The country of origin of the ALV_X-1 is the United_States , home to Americans and where African_Americans are an ethnic group .
relative_adverb United_States anthem The_Star-Spangled_Banner ALV_X-1 originated in the United_States , where White_Americans are one of the ethnic groups . It is also where the The_Star-Spangled_Banner is the national anthem .
coordinated_full_clauses United_States demonym Americans The country of origin of the ALV_X-1 is the United_States , home to Americans and where African_Americans are an ethnic group .
coordinated_full_clauses United_States demonym Americans ALV_X-1 is from the United_States where Native_Americans are an ethnic group and Americans live .
existential United_States leaderTitle President_of_the_United_States The Atlas_II originated from the United_States , where Native_Americans are an ethnic group . The leader of the United_States has the title President_of_the_United_States .
apposition United_States capital Washington The ALV_X-1 comes from the United_States where African_Americans are an ethnic group and Washington is the capital .
apposition United_States capital Washington Atlas_II originates from the United_States where the capital is Washington and African_Americans are among the ethnic groups .
apposition United_States demonym Americans The Atlas_II came from the United_States where Asian_Americans are an ethnic group and where Americans live .
existential United_States capital Washington The ALV_X-1 originates from the United_States which has the capital city of Washington and the White_Americans as an ethnic group .
relative_object United_States demonym Americans The United_States is home to Native_Americans and Americans as well as the origin of the ALV_X-1 .
relative_object United_States capital Washington The United_States , with its capital of Washington , is home to Asian_Americans and The Atlas_II .
relative_object United_States anthem The_Star-Spangled_Banner ALV_X-1 originated in the United_States , where White_Americans are one of the ethnic groups . It is also where the The_Star-Spangled_Banner is the national anthem .
possessif United_States anthem The_Star-Spangled_Banner The ALV_X-1 came from the United_States where African_Americans are an ethnic group and the anthem is the The_Star-Spangled_Banner .
possessif United_States anthem The_Star-Spangled_Banner The country of origin of the ALV_X-1 is the United_States where Native_Americans are an ethnic group and the The_Star-Spangled_Banner is the national anthem .
possessif United_States capital Washington Atlas_II comes from the United_States where the capital is Washington and Native_Americans are an ethnic group .
possessif United_States demonym Americans The Atlas_II comes from the United_States where Americans live and where Native_Americans are an ethnic group .
possessif United_States demonym Americans The Atlas_II is from the United_States where the people are called Americans . Asian_Americans are part of the ethnic groups in that country .
passive_voice ALV_X-1 countryOrigin United_States The ALV_X-1 originated in the United_States , has a diameter is 1.524_metres and launched from the Mid-Atlantic_Regional_Spaceport .
relative_subject AMC_Matador relatedMeanOfTransportation AMC_Ambassador The AMC_Matador and the AMC_Ambassador are relative means of transportation , while the AMC_Matador is assembled in Port_Melbourne_,_Victoria and available in a 1974 model .
coordinated_full_clauses AMC_Matador relatedMeanOfTransportation AMC_Ambassador AMC_Matador , related to the AMC_Ambassador , is made in Thames_,_New_Zealand . One of its model years is 1971 .
coordinated_full_clauses AMC_Matador relatedMeanOfTransportation AMC_Ambassador The AMC_Matador , available in a 1974 model , is made in Thames_,_New_Zealand . That car is similar to the AMC_Ambassador .
coordinated_clauses AMC_Matador relatedMeanOfTransportation AMC_Ambassador The AMC_Matador , including the 1971 model is assembled in the "USA" and is related to the AMC_Ambassador .
coordinated_clauses AMC_Matador relatedMeanOfTransportation AMC_Ambassador The AMC_Matador is assembled by the Australian_Motor_Industries including the year 1971 . It is a relative means of transportation to the AMC_Ambassador .
relative_adverb 1955_Dodge relatedMeanOfTransportation Plymouth_Plaza Sergio_Marchionne was a key person at Dodge , the manufacturer of the 1955_Dodge . This car , along with the DeSoto_Custom and Plymouth_Plaza are related means of car transportation .
relative_subject AIDAstella owner Costa_Crociere The AIDAstella , owned by Costa_Crociere , is 253260.0_millimetres long and has a 32.2 meter beam .
relative_subject American_submarine_NR-1 topSpeed 8.334 The American_submarine_NR-1 is 45000.0_millimetres long , has a top speed of 8.334 and a 3.8_m ship beam .
direct_object ARA_Veinticinco_de_Mayo_(V-2) country Argentina The ARA_Veinticinco_de_Mayo_(V-2) ship ' s beam measures 24.4 and it is 192000.0_millimetres long . It derives from Argentina .
direct_object Abarth_1000_GT_Coupé designer Gruppo_Bertone The Abarth_1000_GT_Coupé was designed by Gruppo_Bertone and built by Abarth which was founded in Italy .
coordinated_clauses Alvis_Speed_25 engine 4387.0_cubic_cm Alvis_Speed_25 which has a 4387.0_cubic_cm engine , was manufactured by the Alvis_Car_and_Engineering_Company which was founded in Coventry .
existential Alvis_Speed_25 engine Straight-six_engine Alvis_Speed_25 , with a Straight-six_engine , was manufactured by the Alvis_Car_and_Engineering_Company in Coventry .
coordinated_clauses Gruppo_Bertone foundedBy Giovanni_Bertone Gruppo_Bertone was founded in Italy by Giovanni_Bertone who designed the Abarth_1000_GT_Coupé .
coordinated_full_clauses Abarth_1000_GT_Coupé wheelbase 2160.0_millimetres The Abarth_1000_GT_Coupé has a Coupé body style , a wheelbase of 2160.0_millimetres and rolled off the production line in 1958 .
coordinated_clauses Abarth_1000_GT_Coupé width 1.55 The Abarth_1000_GT_Coupé is 1.55 metres wide , has a wheelbase of 2160.0_millimetres and the body style is "Two_door_coupé" .
coordinated_full_clauses Abarth_1000_GT_Coupé width 1.55 The Abarth_1000_GT_Coupé is 1.55 metres wide , has a wheelbase of 2160.0_millimetres and the body style is "Two_door_coupé" .
possessif Abarth_1000_GT_Coupé productionEndYear 1958 The Abarth_1000_GT_Coupé is a "Two_door_coupé" model , has a wheelbase of 2160.0_millimetres and stopped being produced in 1958 .
direct_object Abarth_1000_GT_Coupé bodyStyle "Two_door_coupé" The Abarth_1000_GT_Coupé is a "Two_door_coupé" model with a Straight-four_engine and a 2160.0_millimetres wheelbase .
relative_subject Abarth_1000_GT_Coupé bodyStyle "Two_door_coupé" The Abarth_1000_GT_Coupé has the Straight-four_engine , a wheel base of 2160.0_millimetres and a "Two_door_coupé" body style .
direct_object AMC_Matador alternativeName "VAM_Classic" With AMC_V8_engine , the AMC_Matador , sometimes known as the "VAM_Classic" , was assembled in Mexico_City .
passive_voice Alfa_Romeo_164 relatedMeanOfTransportation Saab_9000 The Alfa_Romeo_164 , assembled in Italy , has a Straight-four_engine and is relative to the Saab_9000 and Lancia_Thema .
possessif Alfa_Romeo_164 relatedMeanOfTransportation Saab_9000 The Alfa_Romeo_164 , assembled in Italy , has a Straight-four_engine and is relative to the Saab_9000 and Lancia_Thema .
passive_voice 1955_Dodge wheelbase 120.0_inches The 1955_Dodge , manufactured by Dodge , has a wheelbase of 120.0_inches and a V8_engine .
relative_subject 1955_Dodge wheelbase 120.0_inches The 1955_Dodge ( manufactured by Dodge ) has a V8_engine and a wheelbase of 120.0_inches .
coordinated_clauses Alvis_Car_and_Engineering_Company foundationPlace Coventry Alvis_Speed_25 which has a 4387.0_cubic_cm engine , was manufactured by the Alvis_Car_and_Engineering_Company which was founded in Coventry .
passive_voice Honda division Acura Honda makes the Acura_TLX which possesses a Honda_J_engine . Acura is a division of Honda .
passive_voice Honda division Acura Acura is a division of Honda which makes the Acura_TLX which has an Inline-four_engine .
existential Audi foundedBy August_Horch Audi , founded by August_Horch , manufactures the Audi_A1 and includes Audi_e-tron .
passive_voice Acura_TLX engine Honda_J_engine Honda makes the Acura_TLX which holds a Honda_J_engine . A division of Honda is Acura .
passive_voice Acura_TLX relatedMeanOfTransportation Honda_Accord Acura is a divsion of Honda which makes the Acura_TLX which is related to the Honda_Accord .
apposition Finland demonym Finns The icebreaker ship Aleksey_Chirikov_(icebreaker) was built in Finland where the natives are known as Finns by Arctech_Helsinki_Shipyard .
coordinated_clauses AIDAstella length 253260.0_millimetres Built by Meyer_Werft , the AIDAstella is 253260.0_millimetres and has a beam of 32.2 m .
passive_voice Alfa_Romeo_164 engine Straight-four_engine The Alfa_Romeo_164 , also known as the "Alfa_Romeo_168" , is a Luxury_vehicle with a Straight-four_engine .
passive_voice Aston_Martin_V8 engine 5.3_litres The Aston_Martin_V8 , assembled at Newport_Pagnell , has a 5.3_litres liter engine and is relative to the Aston_Martin_RHAM/1 .
relative_subject AMC_Matador modelYears 1971 The AMC_Matador is assembled by the Australian_Motor_Industries including the year 1971 . It is a relative means of transportation to the AMC_Ambassador .
direct_object AMC_Matador modelYears 1971 The AMC_Matador , including the 1971 model is assembled in the "USA" and is related to the AMC_Ambassador .
existential Aston_Martin_V8 engine 5.3_litres The Aston_Martin_V8 , related to the Aston_Martin_RHAM/1 , is assembled in "Newport_Pagnell_,_Buckinghamshire_,_England_,_United_Kingdom" and has a 5.3_litres engine .
direct_object AMC_Matador modelYears 1971 1971 is one of the model years of the AMC_Matador which was assembled in "Australia" and is related to the AMC_Ambassador .
relative_object Aston_Martin_V8 engine 5.3_litres The Aston_Martin_V8 , assembled at Newport_Pagnell , has a 5.3_litres liter engine and is relative to the Aston_Martin_RHAM/1 .
existential Alhambra builder Samuda_Brothers The Samuda_Brothers built the Alhambra which has a Humphrys_,_Tennant_and_Dykes power type from Deptford .
relative_subject Caterpillar_Inc foundationPlace United_States The power type of the AIDAluna is the Caterpillar_Inc engine . Caterpillar_Inc . was founded in California in the United_States and is located in Illinois .
direct_object Alhambra owner P&O_(company) Alhambra is owned by P&O_(company) and is powered by the Deptford - located Humphrys_,_Tennant_and_Dykes engine .
relative_subject Alhambra owner P&O_(company) The Alhambra is owned by P&O_(company) and is powered by an engine from the London company Humphrys_,_Tennant_and_Dykes .
possessif AIDAluna owner AIDA_Cruises AIDAluna is owned by AIDA_Cruises and powered by the Caterpillar_Inc engine from the United_States .
passive_voice Alhambra shipLaunch 1855-05-31 The Alhambra , with its 8.3_m ship beam , launched 1855-05-31 , was "Wrecked" .
relative_subject Aleksey_Chirikov_(icebreaker) builder Helsinki Aleksey_Chirikov_(icebreaker) was built in Helsinki , in Finland . It is in "In_service" and has a ship beam of 21.2 m .
coordinated_full_clauses Alhambra status "Wrecked" The Alhambra has a top speed of 18.52 and a ship beam of 8.3_m . It was once "Wrecked" .
relative_subject 1955_Dodge alternativeName "Dodge_Suburban" The 1955_Dodge , or "Dodge_Suburban" , has a "3-speed_automatic" transmission with a V8_engine .
relative_subject 1955_Dodge bodyStyle Station_wagon The 1955_Dodge has a V8_engine , a "3-speed_automatic" transmission and a Station_wagon style body .
relative_subject Audi_A1 assembly Audi_Brussels The Audi_A1 , assembled in Audi_Brussels , has a "5-speed_manual" transmission and a 1.2_litres liter engine .
coordinated_clauses 1955_Dodge alternativeName "Dodge_Suburban" The 1955_Dodge , also called the "Dodge_Suburban" , has a V8_engine and a "3-speed_automatic" auto transmission .
coordinated_clauses 1955_Dodge bodyStyle Hardtop The 1955_Dodge has a V8_engine , a "3-speed_automatic" transmission and a Hardtop .
relative_object Audi foundedBy August_Horch The Audi_A1 was manufactured by , Audi , a company which was founded by August_Horch . Ducati is a subsidiary of Audi .
direct_object Audi foundedBy August_Horch The Audi_A1 was manufactured by , Audi , a company which was founded by August_Horch . Ducati is a subsidiary of Audi .
relative_subject Antares_(rocket) launchSite Mid-Atlantic_Regional_Spaceport_Launch_Pad_0 The Antares_(rocket) is similar to the Delta_II ( from the United_States ) and was launched at the Mid-Atlantic_Regional_Spaceport_Launch_Pad_0 .
relative_subject Antares_(rocket) manufacturer Yuzhnoye_Design_Office The Antares_(rocket) , manufactured at the Yuzhnoye_Design_Office , is relative to the United_States based , Delta_II .
relative_subject Delta_II launchSite Vandenberg_AFB_Space_Launch_Complex_2 Launched at Cape_Canaveral_Air_Force_Station and the Vandenberg_AFB_Space_Launch_Complex_2 was the Delta_II United_States rocket . The Antares_(rocket) is similar to the Delta .
possessif Ariane_5 manufacturer Arianespace Arianespace is the manufacturer of Ariane_5 , 2004-12-18 was Ariane_5 ' s final flight and it was launched from the Guiana_Space_Centre .
possessif Ariane_5 manufacturer European_Space_Agency The European_Space_Agency manufactured the Ariane_5 , which was launched from the Guiana_Space_Centre and had a final flight on the 2009-12-18 .
possessif Antares_(rocket) manufacturer Yuzhnoye_Design_Office The Antares_(rocket) , produced by Yuzhnoye_Design_Office , was first launched 2013-04-21 from Mid-Atlantic_Regional_Spaceport .
possessif United_States ethnicGroup African_Americans Auburn_,_Alabama is in the United_States , the capital of which is Washington . It is also the country where there are many ethnic groups , one of which is , African_Americans .
relative_adverb Abilene_,_Texas isPartOf Texas Abilene_,_Texas is a part of Texas in the United_States . The capital of the United_States is Washington .
relative_adverb Auburn_,_Washington isPartOf Pierce_County_,_Washington Auburn_,_Washington is part of Pierce_County_,_Washington in the United_States . Washington is also the capital of the United_States .
relative_adverb United_States ethnicGroup African_Americans The United_States includes the African_Americans ethnic group , has the capital of Washington and is the location of Albany_,_Oregon .
relative_adverb United_States ethnicGroup Native_Americans Akron_,_Ohio is located within the United_States ( capital city : Washington ) where Native_Americans are one of the ethnic groups .
relative_adverb United_States ethnicGroup African_Americans The capitol of the United_States is Washington . One of the ethnic groups are African_Americans and it is also home to the Bacon_Explosion .
relative_adverb United_States ethnicGroup African_Americans In the United_States one of the ethnic groups is African_Americans and the capital is Washington . A_Fortress_of_Grey_Ice is from the country .
relative_adverb ARA_Veinticinco_de_Mayo_(V-2) builder Cammell_Laird The ARA_Veinticinco_de_Mayo_(V-2) , built by Cammell_Laird , is from Argentina ( capital : Buenos_Aires ).
relative_adverb Italy leaderName Matteo_Renzi The capital of Italy is Rome . Two leaders in Italy are Matteo_Renzi and Laura_Boldrini . Amatriciana_sauce is a traditional sauce there .
relative_adverb Italy leaderName Pietro_Grasso Italy is the country Amatriciana_sauce comes from and the capital is Rome . The leaders are Pietro_Grasso and Laura_Boldrini .
apposition United_States ethnicGroup Native_Americans Atlanta is in the United_States whose capital is Washington . Native_Americans are an ethnic group in the United_States .
relative_object Attica_,_Indiana isPartOf Fountain_County_,_Indiana Attica_,_Indiana , Fountain_County_,_Indiana is in the United_States : where Washington is the capital .
passive_voice United_States ethnicGroup Native_Americans Atlanta is in the United_States whose capital is Washington . Native_Americans are an ethnic group in the United_States .
passive_voice United_States ethnicGroup Native_Americans The book , 1634:_The_Ram_Rebellion , comes from the United_States , whose capital is Washington and has Native_Americans as one of its ethnic groups .
passive_voice Albany_,_Oregon isPartOf Oregon Albany_,_Oregon is part of Oregon in the United_States where the capital city is Washington .
passive_voice United_States ethnicGroup Asian_Americans Amarillo_,_Texas is in the United_States , where Washington is the capital and where Asian_Americans are an ethnic group .
passive_voice United_States leaderName Barack_Obama The Bacon_Explosion originates from the United_States where the leader is Barack_Obama and the capital is Washington .
passive_voice United_States leaderName Joe_Biden A Bacon_Explosion is a dish from the United_States where Joe_Biden is a political leader and the capital city is Washington .
passive_voice Indonesia leaderName Jusuf_Kalla The dish Bakso comes from Indonesia where Jusuf_Kalla is the leader and Jakarta is the capitol city .
coordinated_full_clauses Abilene_,_Texas isPartOf Texas Abilene_,_Texas is a part of Texas , in the United_States , of which Washington is the capital .
coordinated_full_clauses Atlantic_City isPartOf Atlantic_County_,_New_Jersey Atlantic_City is in Atlantic_County_,_New_Jersey in the United_States . Washington is the capital of the United_States .
coordinated_full_clauses Albany_,_Oregon isPartOf Linn_County_,_Oregon Albany_,_Oregon falls under Linn_County_,_Oregon , in the United_States , the capital of which is , Washington .
coordinated_full_clauses Auburn_,_Washington isPartOf King_County_,_Washington Auburn_,_Washington is part of King_County_,_Washington in the United_States The capital of the United_States is Washington .
coordinated_full_clauses United_States ethnicGroup Native_Americans Auburn_,_Alabama is in the United_States ; where Native_Americans are one of the ethnic groups and where the capital is Washington .
coordinated_full_clauses Malaysia leaderName Abu_Zahar_Ujang Asam_pedas is a found in Malaysia , where the capital is Putrajaya and Abu_Zahar_Ujang is the name of the leader .
coordinated_full_clauses United_States leaderName Joe_Biden A Bacon_Explosion is a dish from the United_States where Joe_Biden is a political leader and the capital city is Washington .
coordinated_full_clauses United_States ethnicGroup Native_Americans The United_States is home to Native_Americans and is the origin place of A_Fortress_of_Grey_Ice . Washington is the capital .
coordinated_full_clauses United_States ethnicGroup Asian_Americans A_Severed_Wasp is from the United_States , where there is an ethnic group called Asian_Americans and where the capital is Washington .
coordinated_full_clauses 1_Decembrie_1918_University city Alba_Iulia 1_Decembrie_1918_University is in Alba_Iulia , Romania . Bucharest is the capital of Romania .
coordinated_full_clauses Italy leaderName Pietro_Grasso Italy is the country Amatriciana_sauce comes from and the capital is Rome . The leaders are Pietro_Grasso and Laura_Boldrini .
relative_object Malaysia leaderName Arifin_Zakaria Asam_pedas is a food found in Malaysia , where the capital is Kuala_Lumpur and Arifin_Zakaria is the leader .
relative_object AIDS_(journal) publisher Lippincott_Williams_&_Wilkins AIDS_(journal) is published by Lippincott_Williams_&_Wilkins . It is is published in the United_Kingdom , which capital is London .
relative_object United_States ethnicGroup Asian_Americans A_Fortress_of_Grey_Ice is from the United_States , where Washington is the capital and Asian_Americans are one of the ethnic groups .
relative_object ARA_Veinticinco_de_Mayo_(V-2) builder Cammell_Laird The ARA_Veinticinco_de_Mayo_(V-2) , built by Cammell_Laird , is from Argentina ( capital : Buenos_Aires ).
juxtaposition United_States ethnicGroup Native_Americans Atlanta is in the United_States whose capital is Washington . Native_Americans are one of the ethnic groups in the United_States .
juxtaposition United_States ethnicGroup African_Americans Albany_,_Oregon is located in the United_States whose capital of Washington . One ethnic group in the United_States are African_Americans .
direct_object Abilene_,_Texas isPartOf Jones_County_,_Texas Abilene_,_Texas is in Jones_County_,_Texas ; which is in the United_States . Washington is the capital .
direct_object Auburn_,_Washington isPartOf King_County_,_Washington The capital of the United_States is Washington and King_County_,_Washington which is in Washington is the location of Auburn_,_Washington .
relative_subject ARA_Veinticinco_de_Mayo_(V-2) builder Cammell_Laird Cammell_Laird in Argentina made the ARA_Veinticinco_de_Mayo_(V-2) . The capital of Argentina is Buenos_Aires .
direct_object United_States ethnicGroup African_Americans Alpharetta_,_Georgia is in the United_States . Washington is the capital of the United_States and African_Americans are an ethnic group there .
direct_object United_States ethnicGroup Asian_Americans The capital of the United_States , where the Asian_Americans are an ethnic group , is Washington . Auburn_,_Alabama is located in the country .
direct_object Auburn_,_Washington isPartOf Pierce_County_,_Washington Auburn_,_Washington is part of Pierce_County_,_Washington in the United_States . Washington is also the capital of the United_States .
direct_object Albany_,_Oregon isPartOf Linn_County_,_Oregon Albany_,_Oregon is part of Linn_County_,_Oregon , in the United_States , where the capital is Washington .
direct_object Attica_,_Indiana isPartOf Fountain_County_,_Indiana Attica_,_Indiana is in Fountain_County_,_Indiana in the United_States . Washington is the country ' s capital .
direct_object United_States ethnicGroup Asian_Americans Akron_,_Ohio , is in the United_States . The capital of the country is Washington and Asian_Americans are one of the ethnic groups in that country .
direct_object United_States ethnicGroup White_Americans The capital of the United_States is Washington and White_Americans are one of the ethnic groups . It is home to the Bacon_Explosion .
direct_object United_States leaderName Joe_Biden A Bacon_Explosion is a dish from the United_States where Joe_Biden is a political leader and the capital city is Washington .
direct_object United_States ethnicGroup African_Americans In the United_States one of the ethnic groups is African_Americans and the capital is Washington . A_Fortress_of_Grey_Ice is from the country .
direct_object Italy leaderName Sergio_Mattarella Pietro_Grasso and Sergio_Mattarella are the leaders of Italy . The capital is Rome and Amatriciana_sauce is a traditional Italian sauce .
possessif Albany_,_Oregon isPartOf Linn_County_,_Oregon Albany_,_Oregon is part of Linn_County_,_Oregon , in the United_States , where the capital is Washington .
possessif Albany_,_Oregon isPartOf Oregon Albany_,_Oregon is part of Oregon in the United_States where the capital city is Washington .
possessif Malaysia ethnicGroup Malaysian_Chinese Asam_pedas is a food found in Malaysia where the Malaysian_Chinese are an ethnic group and the capital city is Kuala_Lumpur .
relative_adverb Indonesia leaderName Joko_Widodo Jakarta is the capital of Indonesia whose leader is Joko_Widodo . Bakso is a dish from that country .
possessif Sri_Lanka language Tamil_language Tamil_language is the language of Sri_Lanka where Adisham_Hall is located and the capital city is Sri_Jayawardenepura_Kotte .
possessif United_States ethnicGroup Native_Americans 1634:_The_Ram_Rebellion was written in the United_States where the capital is Washington and the Native_Americans are an ethnic group .
possessif United_States ethnicGroup Native_Americans A_Wizard_of_Mars originates from the United_States where the Native_Americans are an ethnic group and the capital city is Washington .
relative_subject United_States ethnicGroup African_Americans Alpharetta_,_Georgia is in the United_States . Washington is the capital of the United_States and African_Americans are an ethnic group there .
relative_subject United_States ethnicGroup Asian_Americans The Asian_Americans are an ethnic group within the United_States where the capital is Washington and Auburn_,_Alabama is located .
relative_subject Albany_,_Oregon isPartOf Linn_County_,_Oregon Albany_,_Oregon is part of Linn_County_,_Oregon , in the United_States , where the capital is Washington .
relative_subject United_States ethnicGroup Asian_Americans Akron_,_Ohio , is in the United_States . The capital of the country is Washington and Asian_Americans are one of the ethnic groups in that country .
relative_subject United_States ethnicGroup Native_Americans Auburn_,_Alabama is in the United_States ; where Native_Americans are one of the ethnic groups and where the capital is Washington .
relative_subject Italy leaderName Sergio_Mattarella Arrabbiata_sauce can be found in Italy , which leader is Sergio_Mattarella and the capital is Rome .
relative_subject United_States ethnicGroup White_Americans The capital of the United_States is Washington and White_Americans are one of the ethnic groups . It is home to the Bacon_Explosion .
relative_subject Italy leaderName Sergio_Mattarella Italy is the country Amatriciana_sauce comes from , its leader is Sergio_Mattarella and its capital is Rome .
relative_subject United_States ethnicGroup Native_Americans A_Wizard_of_Mars comes from the United_States where Native_Americans are one of the ethnic groups and the capital city is Washington .
relative_subject Italy leaderName Pietro_Grasso Italy is the country Amatriciana_sauce comes from and the capital is Rome . The leaders are Pietro_Grasso and Laura_Boldrini .
relative_subject United_States ethnicGroup African_Americans Atlanta is in the United_States where Washington is the capital and where African_Americans are an ethnic group .
relative_subject United_States ethnicGroup Native_Americans Washington is the capital of the United_States , the country where Amarillo_,_Texas is located and where Native_Americans are one of the ethnic groups .
relative_subject Malaysia leaderName Abdul_Halim_of_Kedah Asam_pedas is a dish from Malaysia where the leader is Abdul_Halim_of_Kedah and the capital city is Kuala_Lumpur .
relative_adverb Italy leaderName Sergio_Mattarella Amatriciana_sauce is a traditional sauce from Italy which has the capital city of Rome and Sergio_Mattarella as leader .
relative_adverb United_States ethnicGroup Asian_Americans Washington is the capital of the United_States which includes many Asian_Americans . 1634:_The_Ram_Rebellion comes from the country .
relative_adverb Bacon_sandwich dishVariation BLT London is the capital of the United_Kingdom which is the country that the Bacon_sandwich comes . A variation of a Bacon_sandwich is the BLT which itself is a variation of a Club_sandwich .
passive_voice Atlantic_City isPartOf Atlantic_County_,_New_Jersey Atlantic_City is part of Atlantic_County_,_New_Jersey , United_States . The capital of which is Washington .
passive_voice Atlantic_City_,_New_Jersey isPartOf Atlantic_County_,_New_Jersey Atlantic_City_,_New_Jersey , part of Atlantic_County_,_New_Jersey , in the United_States , the capital of which is Washington .
relative_object United_States ethnicGroup Asian_Americans 1634:_The_Ram_Rebellion was written in the United_States , where there are many Asian_Americans and the capital city is Washington .
possessif United_States ethnicGroup African_Americans Akron_,_Ohio is located in United_States . A country where African_Americans are one of the ethnic groups and the capital is Washington .
passive_voice Malaysia leaderName Abu_Zahar_Ujang Asam_pedas is a food found in Malaysia , which leader is Abu_Zahar_Ujang and its capital is Kuala_Lumpur .
apposition Italy language Italian_language Arrabbiata_sauce is from Italy , where the capital is Rome and Italian_language is the language .
apposition United_States ethnicGroup Asian_Americans Washington is the capital of the United_States , where Akron_,_Ohio is located and Asian_Americans are one of the ethnic groups .
apposition Italy leaderName Pietro_Grasso Arrabbiata_sauce comes from Italy where the leader is Pietro_Grasso and Rome is the capital city .
apposition United_States ethnicGroup Native_Americans One of the ethnic groups in the United_States are Native_Americans , the country is also the home of the dish Bacon_Explosion and has Washington as it ' s capitol city .
apposition Indonesia leaderName Jusuf_Kalla The dish Bakso comes from Indonesia where Jusuf_Kalla is the leader and Jakarta is the capitol city .
apposition United_States ethnicGroup Asian_Americans The capital city of the United_States is Washington . Some Asian_Americans live in the United_States and A_Wizard_of_Mars was Published there .
apposition Argentina leaderName Gabriela_Michetti The ARA_Veinticinco_de_Mayo_(V-2) derives from Argentina where Gabriela_Michetti is the leader and Buenos_Aires the capital .
relative_adverb Alfredo_Zitarrosa deathPlace Montevideo Alfredo_Zitarrosa died in Montevideo , in Uruguay , the leader of which , is Raúl_Fernando_Sendic_Rodríguez .
relative_adverb Indonesia currency Indonesian_rupiah Arem-arem is a dish served in Indonesia . The leader of this country is Joko_Widodo and the currency used is the Indonesian_rupiah .
relative_adverb Arròs_negre region Valencian_Community Arròs_negre comes from the region of the Valencian_Community in Spain . The leader of Spain is Felipe_VI_of_Spain .
relative_adverb United_States ethnicGroup Native_Americans Barack_Obama leads the United_States . which is the country of the Bacon_Explosion . Native_Americans are an ethnic group in that country .
relative_adverb Singapore language English_language Beef_kway_teow is a popular dish in Singapore , which leader is Halimah_Yacob and English_language is the spoken language .
relative_adverb Sri_Lanka currency Sri_Lankan_rupee Adisham_Hall is located in the country of Sri_Lanka , the leader is Ranil_Wickremesinghe and the currency is the Sri_Lankan_rupee .
relative_adverb AIDS_(journal) publisher Lippincott_Williams_&_Wilkins The AIDS_(journal) is published by Lippincott_Williams_&_Wilkins . It was published in the United_Kingdom , which leader is David_Cameron .
relative_adverb Bacon_sandwich dishVariation BLT David_Cameron is leader of the United_Kingdom , which is home to the Bacon_sandwich . A variation of the Bacon_sandwich is the BLT , which is itself a variation of a Club_sandwich .
relative_subject Alfredo_Zitarrosa birthPlace Montevideo Uruguay is led by Tabaré_Vázquez . Montevideo in that country is the birth place of Alfredo_Zitarrosa .
relative_subject Amdavad_ni_Gufa location Ahmedabad Amdavad_ni_Gufa is located in Ahmedabad , India and the name of the leader in that country is Sumitra_Mahajan .
relative_subject United_States ethnicGroup Asian_Americans The United_States is home to Barack_Obama and Asian_Americans . Alcatraz_Versus_the_Evil_Librarians was also written here .
relative_object India currency Indian_rupee Bhajji comes from the country India , which leader is Sumitra_Mahajan and its currency is the Indian_rupee .
relative_object United_States ethnicGroup Asian_Americans The book Alcatraz_Versus_the_Evil_Librarians comes from the United_States , where Joe_Biden is a leader and Asian_Americans are one of its ethnic groups .
coordinated_full_clauses Antwerp_International_Airport cityServed Antwerp Antwerp , Belgium , is led by Philippe_of_Belgium and served by Antwerp_International_Airport .
relative_object Aarhus_Airport location Tirstrup Denmark is lead by Lars_Løkke_Rasmussen and is the location of Aarhus_Airport , Tirstrup .
relative_object Alfredo_Zitarrosa deathPlace Montevideo Alfredo_Zitarrosa passed away in Montevideo , Uruguay where Raúl_Fernando_Sendic_Rodríguez is the leader .
apposition Indonesia capital Jakarta Jakarta is the capital of Indonesia whose leader is Joko_Widodo . Bakso is a dish from that country .
relative_object Ampara_Hospital region Ampara_District Ampara_Hospital is located in Ampara_District , Sri_Lanka . The leader of the country is Ranil_Wickremesinghe .
passive_voice Athens_International_Airport cityServed Athens The Athens_International_Airport serves the city of Athens , in Greece where Alexis_Tsipras is the leader .
relative_adverb Allama_Iqbal_International_Airport cityServed Lahore Allama_Iqbal_International_Airport is located in Lahore , Pakistan . The leader of the country is Anwar_Zaheer_Jamali .
passive_voice Indonesia currency Indonesian_rupiah Arem-arem is a food found in Indonesia , where the currecncy is Indonesian_rupiah and Jusuf_Kalla is a leader .
relative_adverb Spain demonym Spaniards The people of Spain are known as Spaniards and are lead by Felipe_VI_of_Spain . Arròs_negre is from the country .
passive_voice United_States capital Washington The Bacon_Explosion comes from the United_States where Barack_Obama is a leader and the capital city is Washington .
passive_voice United_States ethnicGroup Asian_Americans The Bacon_Explosion comes from the United_States where Barack_Obama is the leader and Asian_Americans live .
passive_voice Indonesia capital Jakarta Joko_Widodo is the leader of Indonesia , where the capital is Jakarta and where the dish Bakso is from .
passive_voice Singapore language English_language Beef_kway_teow is a dish from the country of Singapore , where the leader is Halimah_Yacob and one of the languages spoken is English_language .
relative_adverb Spain language Spanish_language The leader of Spain is Felipe_VI_of_Spain and it is also where Spanish_language is spoken and the dish Arròs_negre is from .
passive_voice Ajoblanco region Andalusia Ajoblanco is from the Andalusia region of Spain where Felipe_VI_of_Spain is the leader .
relative_adverb Sri_Lanka capital Sri_Jayawardenepura_Kotte Adisham_Hall is located in the country of Sri_Lanka which is lead by Ranil_Wickremesinghe . The capital of Sri_Lanka is Sri_Jayawardenepura_Kotte .
relative_adverb Amdavad_ni_Gufa location Gujarat Amdavad_ni_Gufa is located in Gujarat , India . T._S._Thakur is a leader of India .
relative_adverb United_States ethnicGroup African_Americans Barack_Obama , president of the United_States is an African_Americans man , which is one of the ethnic groups of the United_States . A_Wizard_of_Mars was published in the United_States .
passive_voice United_States ethnicGroup African_Americans 1634:_The_Ram_Rebellion comes from the United_States , where Barack_Obama is the president . African_Americans are one of the ethnic groups there .
passive_voice United_States ethnicGroup Asian_Americans 1634:_The_Ram_Rebellion comes from the United_States where the Asian_Americans are an ethnic group and Barack_Obama leads the country .
passive_voice United_States ethnicGroup African_Americans A_Severed_Wasp is from the United_States where Barack_Obama is the leader and African_Americans are an ethnic group .
relative_subject Arem-arem region Javanese_cuisine The Javanese_cuisine dish Arem-arem is found in Indonesia where Jusuf_Kalla is a leader .
existential Amdavad_ni_Gufa location Gujarat Amdavad_ni_Gufa is located in Gujarat , India , which is lead by Narendra_Modi .
relative_subject Amdavad_ni_Gufa location Gujarat Amdavad_ni_Gufa is located in Gujarat , India . T._S._Thakur is a leader of India .
coordinated_full_clauses Allama_Iqbal_International_Airport cityServed Lahore Allama_Iqbal_International_Airport is located in Lahore , Pakistan . The leader of the country is Anwar_Zaheer_Jamali .
coordinated_full_clauses Italy capital Rome Arrabbiata_sauce can be found in Italy , which leader is Sergio_Mattarella and the capital is Rome .
coordinated_full_clauses Mexico demonym Mexicans Bionico is a food from Mexico where the people are known as Mexicans and the leader is Silvano_Aureoles_Conejo .
coordinated_full_clauses Addis_Ababa_City_Hall location Addis_Ababa The leader of Ethiopia is Hailemariam_Desalegn and Addis_Ababa_City_Hall is located in Addis_Ababa located in Ethiopia .
possessif Italy language Italian_language Amatriciana_sauce is a traditional Italian_language sauce , from Italy , where the language is Italian_language and the leader is Pietro_Grasso .
coordinated_full_clauses Malaysia capital Kuala_Lumpur Asam_pedas is a food found in Malaysia , which leader is Abu_Zahar_Ujang and its capital is Kuala_Lumpur .
possessif France language French_language Baked_Alaska is from France where French_language is the spoken language and Gérard_Larcher is the leader .
possessif Indonesia capital Jakarta Bakso is a dish from the country of Indonesia , where the capital is Jakarta and one of the leaders is Jusuf_Kalla .
possessif Italy capital Rome Amatriciana_sauce is a traditional sauce of Italy , where the capital is Rome and a key leader is Laura_Boldrini .
possessif Republic_of_Ireland language Irish_language Adare_Manor is located in the Republic_of_Ireland , where one of the official languages is Irish_language and the leader is Enda_Kenny .
possessif United_States ethnicGroup Native_Americans Native_Americans are an ethnic group in the United_States where Barack_Obama is president and where A_Severed_Wasp is from .
existential Al-Taqaddum_Air_Base cityServed Fallujah The Al-Taqaddum_Air_Base serves the city of Fallujah in Iraq . The country is led by Fuad_Masum .
passive_voice Malaysia capital Kuala_Lumpur Asam_pedas is a food found in Malaysia , where the capital is Kuala_Lumpur and Arifin_Zakaria is the leader .
passive_voice Amdavad_ni_Gufa location Ahmedabad T._S._Thakur is a leader of India where Amdavad_ni_Gufa is located in Ahmedabad .
passive_voice United_States ethnicGroup Native_Americans Native_Americans and Alcatraz_Versus_the_Evil_Librarians are part of the United_States ; which Barack_Obama is the president of .
possessif Alfredo_Zitarrosa birthPlace Montevideo Alfredo_Zitarrosa was born in Montevideo , in the country of Uruguay , the leader of which , is Raúl_Fernando_Sendic_Rodríguez .
relative_object Romania capital Bucharest 1_Decembrie_1918_University is located in Romania . The capital of the country is Bucharest and the leader ' s name is Klaus_Iohannis .
direct_object A_Loyal_Character_Dancer publisher Soho_Press A_Loyal_Character_Dancer was published by Soho_Press in the United_States which is lead by Barack_Obama .
relative_subject Al-Taqaddum_Air_Base cityServed Fallujah The Al-Taqaddum_Air_Base serves the city of Fallujah in Iraq . Haider_al-Abadi is the leader of Iraq .
relative_subject Alfredo_Zitarrosa deathPlace Montevideo Alfredo_Zitarrosa died in Montevideo , in Uruguay , the leader of which , is Raúl_Fernando_Sendic_Rodríguez .
relative_subject United_States ethnicGroup Native_Americans Barack_Obama leads the United_States . which is the country of the Bacon_Explosion . Native_Americans are an ethnic group in that country .
relative_subject United_States ethnicGroup Asian_Americans The Bacon_Explosion originated in the United_States where John_Roberts is the leader and the Asian_Americans are an ethnic group .
relative_subject France currency Euro The currency of France is the Euro , it is also where Manuel_Valls is a leader and Baked_Alaska is a popular dish .
relative_subject Italy capital Rome Italy is the country Amatriciana_sauce comes from , its leader is Sergio_Mattarella and its capital is Rome .
relative_subject Republic_of_Ireland language Irish_language Adare_Manor is in the Irish_language speaking Republic_of_Ireland , where the leader is Enda_Kenny .
relative_subject United_States ethnicGroup Native_Americans Barack_Obama is the leader of the United_States . A_Fortress_of_Grey_Ice is from the United_States and Native_Americans are an ethnic group in the United_States .
relative_subject United_States ethnicGroup Native_Americans A_Severed_Wasp originates from the United_States where the leader is Barack_Obama and the Native_Americans are an ethnic group .
direct_object Bacon_sandwich dishVariation BLT David_Cameron is leader of the United_Kingdom , which is home to the Bacon_sandwich . A variation of the Bacon_sandwich is the BLT , which is itself a variation of a Club_sandwich .
coordinated_full_clauses Indonesia language Indonesian_language Arem-arem is a food found in Indonesia , which leader is Joko_Widodo and the language is Indonesian_language .
coordinated_full_clauses Indonesia language Indonesian_language In Indonesia ; Jusuf_Kalla is the leader , Indonesian_language is the language and Arem-arem is commonly served .
coordinated_full_clauses United_States capital Washington The Bacon_Explosion comes from the United_States where Barack_Obama is a leader and the capital city is Washington .
coordinated_full_clauses United_States ethnicGroup White_Americans Bacon_Explosion come from the United_States where Barack_Obama is the leader and White_Americans are an ethnic group .
coordinated_full_clauses France language French_language Baked_Alaska is from the country of France , which leader is Manuel_Valls and the spoken language is French_language .
coordinated_full_clauses Mexico demonym Mexicans Bionico is a food found in Mexico , where Enrique_Peña_Nieto is the leader and Mexicans are the inhabitants .
coordinated_full_clauses Spain demonym Spaniards Ajoblanco originates from the country of Spain , where the Spaniards are from and also where the leader is Felipe_VI_of_Spain .
coordinated_full_clauses Republic_of_Ireland language English_language Adare_Manor is located in the Republic_of_Ireland , the leader of this country is Enda_Kenny and one of the languages used is English_language .
coordinated_full_clauses United_States ethnicGroup Native_Americans 1634:_The_Ram_Rebellion comes from the United_States where Barack_Obama is president and Native_Americans are one of the ethnic groups .
coordinated_full_clauses United_States ethnicGroup African_Americans A_Severed_Wasp is from the United_States where Barack_Obama is the leader and African_Americans are an ethnic group .
coordinated_full_clauses Argentina capital Buenos_Aires The ARA_Veinticinco_de_Mayo_(V-2) derives from Argentina where Gabriela_Michetti is the leader and Buenos_Aires the capital .
apposition Indonesia language Indonesian_language Arem-arem is a food found in Indonesia , which leader is Joko_Widodo and the language is Indonesian_language .
apposition Indonesia currency Indonesian_rupiah Arem-arem is a food found in Indonesia , where Jusuf_Kalla is the leader and the currency used is the Indonesian_rupiah .
apposition Malaysia capital Kuala_Lumpur Asam_pedas is a food found in Malaysia , which leader is Abu_Zahar_Ujang and its capital is Kuala_Lumpur .
apposition India demonym Indian_people India , led by Sumitra_Mahajan and inhabited by the Indian_people , is the home to Bhajji .
apposition Singapore language Standard_Chinese Beef_kway_teow is a dish from the country of Singapore . Standard_Chinese is their language spoken , this Country leader is Tony_Tan .
apposition United_States ethnicGroup African_Americans Barack_Obama is both president of the United_States and African_Americans , which is an ethnic group of the United_States . A_Fortress_of_Grey_Ice originates from the United_States .
apposition United_States ethnicGroup Native_Americans 1634:_The_Ram_Rebellion comes from the United_States where the leader is Barack_Obama and the Native_Americans are an ethnic group .
apposition United_States ethnicGroup African_Americans The book Alcatraz_Versus_the_Evil_Librarians comes from the United_States where Joe_Biden is a leader and one ethnic group is the African_Americans .
direct_object Aarhus_Airport location Tirstrup Aarhus_Airport is located in Tirstrup , in Denmark , where the leader is Margrethe_II_of_Denmark .
direct_object Indonesia language Indonesian_language Arem-arem is a dish commonly served in Indonesia where the spoken language is Indonesian_language and Jusuf_Kalla is a leader .
direct_object United_States ethnicGroup Native_Americans Bacon_Explosion comes from the United_States , where Barack_Obama is a leader and where Native_Americans are an ethnic group .
direct_object United_States ethnicGroup African_Americans The Bacon_Explosion originated in the United_States , where African_Americans are an ethnic group and John_Roberts is a leader .
direct_object United_States capital Washington Bacon_Explosion originates from the United_States where the capital city is Washington and the leader is Paul_Ryan .
direct_object France language French_language Baked_Alaska comes from France where French_language is the national language and the leader is Gérard_Larcher .
direct_object France language French_language Barny_Cakes originate in France , where French_language is the national language and where Claude_Bartolone is the leader .
direct_object Beef_kway_teow region Indonesia Beef_kway_teow is a dish served in Indonesia and is from Singapore , where Tony_Tan is a leader .
direct_object Singapore currency Singapore_dollar Beef_kway_teow is a dish from the country of Singapore where the currency is the Singapore_dollar and the leader is Tony_Tan .
direct_object United_States ethnicGroup Asian_Americans A_Fortress_of_Grey_Ice is from the United_States where there are many Asian_Americans and the leader is Barack_Obama .
direct_object United_States ethnicGroup African_Americans 1634:_The_Ram_Rebellion comes from the United_States , where the leader is Barack_Obama and the African_Americans are an ethnic group .
direct_object United_States ethnicGroup Native_Americans 1634:_The_Ram_Rebellion comes from the United_States where Barack_Obama is president and Native_Americans are one of the ethnic groups .
direct_object United_States ethnicGroup African_Americans A_Severed_Wasp is from the United_States where Barack_Obama is the leader and African_Americans are an ethnic group .
direct_object United_States ethnicGroup African_Americans The book Alcatraz_Versus_the_Evil_Librarians comes from the United_States where Joe_Biden is a leader and one ethnic group is the African_Americans .
direct_object Argentina capital Buenos_Aires The ARA_Veinticinco_de_Mayo_(V-2) derives from Argentina where Gabriela_Michetti is the leader and Buenos_Aires the capital .
possessif Malaysia ethnicGroup Malaysian_Chinese An ethnic group in Malaysia which is led by Abdul_Halim_of_Kedah are the Malaysian_Chinese . Asam_pedas is a food found in Malaysia .
possessif Spain demonym Spaniards Felipe_VI_of_Spain is the leader of Spain , the land of Spaniards , which is where Ajoblanco originates from .
possessif Sri_Lanka capital Sri_Jayawardenepura_Kotte Adisham_Hall is located in the country of Sri_Lanka which is lead by Ranil_Wickremesinghe . The capital of Sri_Lanka is Sri_Jayawardenepura_Kotte .
relative_subject Switzerland anthem Swiss_Psalm The Accademia_di_Architettura_di_Mendrisio is in Switzerland . The Swiss anthem is the Swiss_Psalm and its leader is Johann_Schneider-Ammann .
direct_object Arem-arem region "Nationwide_in_Indonesia_,_but_more_specific_to_Java" Arem-arem is a food found "Nationwide_in_Indonesia_,_but_more_specific_to_Java" . Jusuf_Kalla is the leader of Indonesia .
direct_object France language French_language Baked_Alaska is from the country of France , which leader is Manuel_Valls and the spoken language is French_language .
direct_object Adisham_Hall location "Haputale_,_Sri_Lanka" Adisham_Hall is in "Haputale_,_Sri_Lanka" . The leader of Sri_Lanka is Ranil_Wickremesinghe .
possessif Athens_International_Airport cityServed Athens The Athens_International_Airport serves the city of Athens , in Greece where Alexis_Tsipras is the leader .
possessif Allama_Iqbal_International_Airport cityServed Lahore Nawaz_Sharif is the leader in Pakistan where Allama_Iqbal_International_Airport serves the city of Lahore .
possessif Alfredo_Zitarrosa deathPlace Montevideo Alfredo_Zitarrosa death place was in Montevideo , Uruguay , where the leader is Tabaré_Vázquez .
possessif Arem-arem region Javanese_cuisine The Javanese_cuisine dish Arem-arem is found in Indonesia where Jusuf_Kalla is a leader .
possessif Spain ethnicGroup Spaniards The ethnic group of Spaniards come from Spain where Felipe_VI_of_Spain is the leader and Arròs_negre is a dish .
possessif United_States capital Washington A Bacon_Explosion is a dish from the United_States where Joe_Biden is a political leader and the capital city is Washington .
possessif France currency Euro The currency of France is the Euro , it is also where Manuel_Valls is a leader and Baked_Alaska is a popular dish .
possessif Indonesia capital Jakarta The dish Bakso comes from Indonesia where Jusuf_Kalla is the leader and Jakarta is the capitol city .
possessif Spain ethnicGroup Spaniards The dish of Ajoblanco is from Spain where Felipe_VI_of_Spain is the leader and Spaniards are an ethnic group .
possessif United_States ethnicGroup Asian_Americans A_Fortress_of_Grey_Ice is from the United_States where Barack_Obama is the leader and Asian_Americans are one of the ethnic groups .
possessif United_States ethnicGroup Asian_Americans Asian_Americans are one of the ethnic groups in the United_States where Barack_Obama is the leader . It is also the country where ' A_Wizard_of_Mars ' originates from .
possessif Amdavad_ni_Gufa location Gujarat Amdavad_ni_Gufa is located Ahmedabad , Gujarat , in India a country where one of the leaders is Sumitra_Mahajan .
coordinated_clauses Ariane_5 finalFlight 2004-12-18 The Ariane_5 , which last flew from ELA-3 2004-12-18 , was manufactured by Airbus_Defence_and_Space .
coordinated_clauses Atlas_II countryOrigin United_States The Atlas_II , launched from Vandenberg_Air_Force_Base , is manufactured by Lockheed_Martin in the United_States .
relative_subject Antares_(rocket) function "Medium_expendable_launch_system" The Antares_(rocket) function is a "Medium_expendable_launch_system" manufactured at the Yuzhnoye_Design_Office and was launched at the Mid-Atlantic_Regional_Spaceport_Launch_Pad_0 .
existential Antares_(rocket) function "Medium_expendable_launch_system" The Antares_(rocket) , with a "Medium_expendable_launch_system" was made by Yuzhnoye_Design_Office .. It was launched from the Mid-Atlantic_Regional_Spaceport .
existential Atlas_II countryOrigin United_States Atlas_II was built by Lockheed_Martin in the United_States and was launched from Vandenberg_Air_Force_Base .
relative_subject Ariane_5 finalFlight 2009-12-18 The Ariane_5 was launched from the Guiana_Space_Centre and its final flight was on the 2009-12-18 . Its manufacturer was Arianespace .
apposition Antares_(rocket) maidenFlight 2013-04-21 The Antares_(rocket) , produced by Yuzhnoye_Design_Office , was first launched 2013-04-21 from Mid-Atlantic_Regional_Spaceport .
apposition Antares_(rocket) function "Medium_expendable_launch_system" The Antares_(rocket) function is a "Medium_expendable_launch_system" and was manufactured at the Yuzhnoye_Design_Office before launching from the Mid-Atlantic_Regional_Spaceport .
possessif Atlas_II countryOrigin United_States The Atlas_II , from the United_States , was made by Lockheed_Martin and launched from the Spaceport_Florida_Launch_Complex_36 .
possessif Atlas_II countryOrigin United_States The Atlas_II ( from the United_States ) and manufactured by Lockheed_Martin was launched at the Vandenberg_AFB_Space_Launch_Complex_3 .
coordinated_full_clauses Antares_(rocket) function "Medium_expendable_launch_system" The Antares_(rocket) function is a "Medium_expendable_launch_system" manufactured by Orbital_ATK and was launched at the Mid-Atlantic_Regional_Spaceport_Launch_Pad_0 .
coordinated_full_clauses Atlas_II countryOrigin United_States Atlas_II was built by Lockheed_Martin in the United_States and was launched from Vandenberg_Air_Force_Base .
passive_voice Aston_Martin_V8 relatedMeanOfTransportation Aston_Martin_DBS The Aston_Martin_Vantage succeeded the Aston_Martin_DBS which is the manufacturer of the Aston_Martin . The Aston_Martin_V8 and the Aston_Martin_DBS are related means of transport .
coordinated_clauses Aston_Martin_Virage relatedMeanOfTransportation Aston_Martin_DBS The Aston_Martin_V8 was manufactured by Aston_Martin , succeeded by the Aston_Martin_Virage and is a related type of transportation as the Aston_Martin_DBS .
coordinated_clauses DeSoto_Custom relatedMeanOfTransportation Dodge_Coronet The DeSoto_Custom , manufactured by DeSoto_(automobile) , was succeeded by the DeSoto_Firedome . Its is relative to the Dodge_Coronet and the 1955_Dodge .
coordinated_clauses Aston_Martin_V8 manufacturer Aston_Martin The Aston_Martin_V8 , succeeded by the Aston_Martin_Virage , was made by Aston_Martin . Aston_Martin_DBS is a related means of transport .
coordinated_clauses ALV_X-1 diameter 1.524_metres The ALV_X-1 is 1.524_metres in diameter , originated in the United_States and was launched from the Mid-Atlantic_Regional_Spaceport .
coordinated_clauses Atlas_II manufacturer Lockheed_Martin The Atlas_II ( from the United_States ) and manufactured by Lockheed_Martin was launched at the Vandenberg_AFB_Space_Launch_Complex_3 .
coordinated_clauses Antares_(rocket) comparable Delta_II The Delta_II , from the United_States , was launched at Cape_Canaveral_Air_Force_Station_Space_Launch_Complex_17 . The Antares_(rocket) is similar to the Delta_II .
relative_adverb Caterpillar_Inc location Peoria_,_Illinois AIDAluna is powered by Caterpillar_Inc , based in Peoria_,_Illinois . The key person of Caterpillar_Inc is Douglas_Oberhelman .
passive_voice Guiana_Space_Centre headquarter Kourou_,_French_Guiana The launch site of the Ariane_5 was ELA-3 launchpad was is at Guiana_Space_Centre in Kourou_,_French_Guiana .
passive_voice MTU_Friedrichshafen locationCity Friedrichshafen MTU_Friedrichshafen of Friedrichshafen , owned by Rolls-Royce_Holdings , manufactures the A-Rosa_Luna engine .
direct_object 3Arena completionDate "December_2008" 3Arena , completed in "December_2008" is located at North_Wall_,_Dublin . "HOK_SVE" was the architect .
existential Alan_B._Miller_Hall owner College_of_William_&_Mary The College_of_William_&_Mary is owned by Alan_B._Miller_Hall in Williamsburg_,_Virginia . It was designed by Robert_A._M._Stern .
passive_voice 3Arena completionDate "December_2008" The 3Arena , designed by the Populous_(company) and completed in "December_2008" , is located on "North_Wall_Quay" .
relative_subject 3Arena completionDate "December_2008" The 3Arena , designed by the Populous_(company) and completed in "December_2008" , is located on "North_Wall_Quay" .
apposition 3Arena completionDate "December_2008" The 3Arena , designed by the Populous_(company) and completed in "December_2008" , is located on "North_Wall_Quay" .
apposition AC_Hotel_Bella_Sky_Copenhagen tenant Marriott_International AC_Hotel_Bella_Sky_Copenhagen , Denmark , Marriott_International Hotel because 3XN was the architect .
direct_object 3Arena completionDate "December_2008" Populous_(company) were the architects of the 3Arena on "North_Wall_Quay" which was completed in "December_2008" .
possessif 3Arena completionDate "December_2008" 3Arena , designed by the Populous_(company) , was completed in "December_2008" and is located at "East_Link_Bridge" .
possessif Alan_B._Miller_Hall owner College_of_William_&_Mary Robert_A._M._Stern is the architect of Alan_B._Miller_Hall , owned by The College_of_William_&_Mary and located in Virginia .
passive_voice Julia_Morgan significantBuilding Asilomar_State_Beach California born Julia_Morgan designed the Asilomar_Conference_Grounds and other significant buildings including Asilomar_State_Beach .
relative_subject Julia_Morgan significantBuilding Chinatown_,_San_Francisco Julia_Morgan was born in San_Francisco and was the architect of significant buildings in Chinatown_,_San_Francisco and of the Asilomar_Conference_Grounds .
direct_object Julia_Morgan significantBuilding Riverside_Art_Museum Asilomar_Conference_Grounds was designed by Julia_Morgan at the Riverside_Art_Museum in California .
existential Julia_Morgan significantBuilding Los_Angeles_Herald-Examiner San_Francisco born architect Julia_Morgan designed many significant buildings including the Asilomar_Conference_Grounds , the Los_Angeles_Herald-Examiner Building and the Asilomar_State_Beach .
passive_voice Birmingham leaderName Conservative_Party_(UK) Birmingham , led by the Conservative_Party_(UK) , was the birthplace of John_Madin who designed 103_Colmore_Row .
passive_voice Birmingham leaderName Khalid_Mahmood_(British_politician) Birmingham born architect John_Madin designed 103_Colmore_Row . Khalid_Mahmood_(British_politician) is one of the leaders in Birmingham .
passive_voice 3Arena architect Populous_(company) The architect of 3Arena , in North_Wall_,_Dublin and completed in "December_2008" , is Populous_(company) .
apposition 103_Colmore_Row floorCount 23 103_Colmore_Row , located in Birmingham , with 23 floors , was completed in 1976 .
apposition Adisham_Hall architecturalStyle "Tudor_and_Jacabian" Adisham_Hall , located in "Haputale_,_Sri_Lanka" , is in the architectural style of "Tudor_and_Jacabian" and was completed in 1931 .
existential Alan_B._Miller_Hall owner College_of_William_&_Mary Alan_B._Miller_Hall ( located in Williamsburg_,_Virginia ) is owned by The College_of_William_&_Mary and was completed "1_June_2009" .
direct_object 103_Colmore_Row completionDate 1976 103_Colmore_Row , located in Birmingham , with 23 floors , was completed in 1976 .
apposition 103_Colmore_Row completionDate 1976 The building of 103_Colmore_Row completed in 1976 is located in Birmingham and has 23 floors .
apposition 200_Public_Square completionDate 1985 200_Public_Square , completed in 1985 , has 45 floors and is located in "Cleveland_,_Ohio_44114" .
apposition 250_Delaware_Avenue buildingStartDate "January_,_2014" 250_Delaware_Avenue has 12 floors , it ' s construction began in "January_,_2014" is located in Buffalo_,_New_York .
relative_subject 250_Delaware_Avenue floorArea 30843.8_square_metres 250_Delaware_Avenue is a location in Buffalo_,_New_York , it has a floor area of 30843.8_square_metres and 12 floors .
relative_subject AC_Hotel_Bella_Sky_Copenhagen tenant Marriott_International AC_Hotel_Bella_Sky_Copenhagen has 23 floors and is located in Denmark . The tenant is Marriott_International .
passive_voice Perth country Australia The 108_St_Georges_Terrace in Perth , Australia , has a floor count of 50 .
relative_subject Cleveland country United_States With a floor count of 45 , 200_Public_Square is located in Cleveland in the United_States .
relative_subject 103_Colmore_Row location "Colmore_Row_,_Birmingham_,_England" 103_Colmore_Row is located in "Colmore_Row_,_Birmingham_,_England" and was completed in 1976 having 23 floors .
relative_subject 200_Public_Square location "United_States" 200_Public_Square in the "United_States" has 45 floors , it was completed in 1985 .
passive_voice 300_North_LaSalle location Illinois 300_North_LaSalle , with 60 floors and completed in 2009 , is in Illinois .
apposition Adolfo_Suárez_Madrid–Barajas_Airport elevationAboveTheSeaLevel_(in_metres) 610 At an elevation of 610 metres above seal level , Adolfo_Suárez_Madrid–Barajas_Airport is located in Madrid , in Spain .
apposition Adolfo_Suárez_Madrid–Barajas_Airport elevationAboveTheSeaLevel_(in_metres) 610 At an elevation of 610 metres above sea level , Adolfo_Suárez_Madrid–Barajas_Airport is located at Paracuellos_de_Jarama , in Spain .
passive_voice Ícolo_e_Bengo isPartOf Luanda_Province Angola_International_Airport is located in Ícolo_e_Bengo , Luanda_Province in Angola .
passive_voice 108_St_Georges_Terrace buildingStartDate 1981 108_St_Georges_Terrace in Perth , Australia , began construction in 1981 .
existential Adolfo_Suárez_Madrid–Barajas_Airport elevationAboveTheSeaLevel_(in_metres) 610 Adolfo_Suárez_Madrid–Barajas_Airport with an elevation of 610 meters above sea level is located in San_Sebastián_de_los_Reyes , Spain .
apposition Texas capital Austin_,_Texas Andrews_County_Airport is located in Texas , United_States and Texas ' capital is Austin_,_Texas .
existential 108_St_Georges_Terrace buildingStartDate 1981 Construction of 108_St_Georges_Terrace , Perth , Australia began in 1981 .
apposition Cleveland governingBody Cleveland_City_Council 200_Public_Square is in Cleveland , United_States and the governing body is Cleveland_City_Council .
apposition Chicago isPartOf DuPage_County_,_Illinois 300_North_LaSalle is in Chicago in the DuPage_County_,_Illinois within the United_States .
apposition New_York_City isPartOf New_Netherland The Asser_Levy_Public_Baths are located in the United_States in New_York_City , which used to be part of New_Netherland .
existential New_York_City isPartOf Manhattan Asser_Levy_Public_Baths are located in New_York_City , Manhattan , New_York , United_States .
apposition Ethiopia leaderName Mulatu_Teshome Hailemariam_Desalegn is the leader of Ethiopia along with Mulatu_Teshome . Addis_Ababa_City_Hall is located in the country in the city of Addis_Ababa .
existential Angola_International_Airport runwayLength 3800.0 With a runway length of 3800.0 metres , Angola_International_Airport is located at Ícolo_e_Bengo , in Angola .
relative_subject Alpena_County_Regional_Airport elevationAboveTheSeaLevel_(in_metres) 210 Alpena_County_Regional_Airport is located at 210 metres above sea level at Wilson_Township_,_Alpena_County_,_Michigan in the United_States .
relative_subject Atlantic_City_International_Airport elevationAboveTheSeaLevel_(in_metres) 23 Atlantic_City_International_Airport is located at Egg_Harbor_Township_,_New_Jersey , United_States and is 23 metres above sea level .
relative_subject Cleveland isPartOf Cuyahoga_County_,_Ohio The United_States is home to 200_Public_Square in Cleveland , Cuyahoga_County_,_Ohio .
relative_subject Adolfo_Suárez_Madrid–Barajas_Airport elevationAboveTheSeaLevel_(in_metres) 610 The Adolfo_Suárez_Madrid–Barajas_Airport is located in San_Sebastián_de_los_Reyes in Spain . It is elevated 610 metres above sea level .
passive_voice Alan_B._Miller_Hall currentTenants Mason_School_of_Business The Mason_School_of_Business is a tenant of The Alan_B._Miller_Hall , which is located in Virginia which is in the United_States .
passive_voice Dublin isPartOf Leinster 3Arena is located in Dublin ( in the Republic_of_Ireland ) which is a part of Leinster .
relative_subject Adolfo_Suárez_Madrid–Barajas_Airport elevationAboveTheSeaLevel_(in_metres) 610 Adolfo_Suárez_Madrid–Barajas_Airport is located in Alcobendas , Spain and is 610 m above sea level .
passive_voice Akita_,_Akita isPartOf Akita_Prefecture Akita_Museum_of_Art is in Akita_,_Akita , which is part of Akita_Prefecture , Japan .
relative_subject New_York_City isPartOf New_Netherland The Asser_Levy_Public_Baths are located in New_York_City , a part of which is Brooklyn . New_York_City was part of New_Netherland and can be found in the United_States .
existential George_Winkler country United_States 320_South_Boston_Building , designed by architect George_Winkler of the United_States , was completed in 1929 .
coordinated_clauses 3Arena location "East_Link_Bridge" The 3Arena at "East_Link_Bridge" was designed by Populous_(company) and completed in "December_2008" .
coordinated_clauses Adare_Manor location County_Limerick Adare_Manor , completed in 1862 by architect Philip_Charles_Hardwick , is located in Adare , in County_Limerick .
relative_subject 3Arena location Dublin The architects of the 3Arena in Dublin was "HOK_SVE" and it was completed in "December_2008" .
relative_subject Adare_Manor location County_Limerick Adare_Manor , designed by Philip_Charles_Hardwick , was completed in 1862 and is located in Adare , County_Limerick .
direct_object Adare_Manor buildingStartDate "1700" The building of the Adare_Manor started in "1700" and completed in 1862 was designed by the architects James_Pain_and_George_Richard_Pain .
direct_object 11_Diagonal_Street completionDate 1983 11_Diagonal_Street , with 20 floors , was designed by Helmut_Jahn and completed in 1983 .
relative_adverb Texas demonym Tejano Texas , or Tejano , is the location of Andrews_County_Airport , with the capital in Austin_,_Texas .
relative_adverb Texas language English_language Andrews_County_Airport is located in Texas , Austin_,_Texas and the language spoken in Texas is English_language .
relative_adverb United_States leaderName John_Roberts 250_Delaware_Avenue is located in the United_States which capital is Washington and John_Roberts is the Chief of Justice .
apposition Texas demonym Texan With its capital of Austin_,_Texas , Andrews_County_Airport is located in Texas where there are Texan .
apposition United_States leaderName John_Roberts 250_Delaware_Avenue is located in the United_States which capital is Washington and John_Roberts is the Chief of Justice .
apposition Azerbaijan leaderName Artur_Rasizade Artur_Rasizade is the leader of Azerbaijan . The capital is Baku where the Baku_Turkish_Martyrs'_Memorial is located .
apposition Greece leader Nikos_Voutsis The capital city of Greece is Athens where Alexis_Tsipras and Nikos_Voutsis are leaders . The country is the location of AE_Dimitra_Efxeinoupolis .
passive_voice South_Africa ethnicGroup Coloured 11_Diagonal_Street is in South_Africa , the capital of which is Cape_Town . Two of the ethnic groups within South_Africa are Coloured people and Asian_South_Africans .
relative_object Greece leader Prokopis_Pavlopoulos AE_Dimitra_Efxeinoupolis is located in Greece , where Athens in the capital and Nikos_Voutsis and Prokopis_Pavlopoulos are leaders .
relative_object Texas demonym Texan With its capital of Austin_,_Texas , Andrews_County_Airport is located in Texas where there are Texan .
relative_object South_Africa leaderName Cyril_Ramaphosa South_Africa is the location of 11_Diagonal_Street . The capital is Cape_Town and the leader is Cyril_Ramaphosa .
relative_object Greece leader Nikos_Voutsis The capital city of Greece is Athens where Alexis_Tsipras and Nikos_Voutsis are leaders . The country is the location of AE_Dimitra_Efxeinoupolis .
direct_object United_States leaderName John_Roberts 250_Delaware_Avenue is located in the United_States which capital is Washington and John_Roberts is the Chief of Justice .
passive_voice South_Africa leaderName Cyril_Ramaphosa South_Africa ( led by Cyril_Ramaphosa ) is the home of the ethnic group White_South_Africans and the location of 11_Diagonal_Street .
apposition South_Africa leaderName Jacob_Zuma Lead by Cyril_Ramaphosa and Jacob_Zuma , South_Africa is the location of the address 11_Diagonal_Street and has the ethnic group of Asian_South_Africans .
direct_object Uttar_Pradesh isPartOf Bundelkhand Agra_Airport is located in Uttar_Pradesh which is part of Bundelkhand . The leader there is Ram_Naik .
direct_object Uttar_Pradesh isPartOf Bundelkhand Agra_Airport is located in Uttar_Pradesh ( lead by Ram_Naik ) which is part of Awadh and Bundelkhand .
apposition Cleveland isPartOf Ohio 200_Public_Square is located in Cleveland , Ohio and Frank_G._Jackson is a leader .
passive_voice South_Africa ethnicGroup White_South_African 11_Diagonal_Street is in South_Africa where the leader is Jacob_Zuma . An ethnic group found there are White_South_African .
relative_adverb Azerbaijan leaderTitle Prime_Minister The Baku_Turkish_Martyrs'_Memorial is found in Azerbaijan , a country led by the Prime_Minister Artur_Rasizade .
relative_subject South_Africa ethnicGroup Asian_South_Africans The Asian_South_Africans group , under leadership of Cyril_Ramaphosa , is located at 11_Diagonal_Street in South_Africa .
relative_subject Chicago isPartOf DuPage_County_,_Illinois 300_North_LaSalle is in Chicago , who ' s leader is Susana_Mendoza and is a part of DuPage_County_,_Illinois .
relative_subject Cleveland country United_States 200_Public_Square is located in Cleveland , United_States , where one of the leaders is Frank_G._Jackson .
relative_subject Chicago isPartOf DuPage_County_,_Illinois 300_North_LaSalle is located in Chicago , DuPage_County_,_Illinois where the leader is Susana_Mendoza .
apposition AC_Hotel_Bella_Sky_Copenhagen tenant Marriott_International Margrethe_II_of_Denmark is the leader of Denmark where the AC_Hotel_Bella_Sky_Copenhagen , which is tenanted by Marriott_International is located .
relative_adverb Chicago country United_States 300_North_LaSalle is in Chicago , United_States . Rahm_Emanuel leads Chicago .
relative_adverb 3Arena owner Live_Nation_Entertainment 3Arena is owned by Live_Nation_Entertainment , 3Arena is located in Dublin , Críona_Ní_Dhálaigh was the Lord Mayor of Dublin .
apposition United_States capital Washington Paul_Ryan is a leader of the United_States where the capital city is Washington . 250_Delaware_Avenue is located in the country .
relative_adverb Azerbaijan capital Baku The Baku_Turkish_Martyrs'_Memorial is located in Baku , the capital of Azerbaijan , lead by Artur_Rasizade .
coordinated_clauses South_Africa capital Cape_Town South_Africa is the location of 11_Diagonal_Street . The capital is Cape_Town and the leader is Cyril_Ramaphosa .
coordinated_clauses 20_Fenchurch_Street floorCount 34 Boris_Johnson is a leader in London where 20_Fenchurch_Street with 34 floors is located .
coordinated_clauses 3Arena owner Live_Nation_Entertainment The owner of 3Arena in Dublin is Live_Nation_Entertainment . The Lord Mayor of the city was Críona_Ní_Dhálaigh .
coordinated_clauses United_States capital Washington 250_Delaware_Avenue is in the United_States . The capital is Washington and John_Roberts is a leader .
coordinated_clauses Baku_Turkish_Martyrs'_Memorial designer "Hüseyin_Bütüner_and_Hilmi_Güner" The Baku_Turkish_Martyrs'_Memorial , designed by "Hüseyin_Bütüner_and_Hilmi_Güner" , is located in Azerbaijan , where the leader is Artur_Rasizade .
existential South_Africa ethnicGroup Coloured 11_Diagonal_Street is located in South_Africa , populated by Coloured people and led by Jacob_Zuma .
relative_adverb South_Africa capital Cape_Town South_Africa is the location of 11_Diagonal_Street . The capital is Cape_Town and the leader is Cyril_Ramaphosa .
relative_adverb Cleveland isPartOf Cuyahoga_County_,_Ohio 200_Public_Square is in Cleveland which is part of Cuyahoga_County_,_Ohio . The leader in Cleveland is Frank_G._Jackson .
relative_adverb 3Arena owner Live_Nation_Entertainment The owner of 3Arena in Dublin is Live_Nation_Entertainment . The Lord Mayor of the city was Críona_Ní_Dhálaigh .
relative_object 20_Fenchurch_Street floorCount 34 20_Fenchurch_Street , with 34 floors , is located within the United_Kingdom where Elizabeth_II is the monarch .
relative_object 3Arena owner Live_Nation_Entertainment The owner of 3Arena in Dublin is Live_Nation_Entertainment . The Lord Mayor of the city was Críona_Ní_Dhálaigh .
relative_object United_States language English_language Joe_Biden is the leader of the United_States where the native language is English_language . 250_Delaware_Avenue is also located there .
relative_object Baku_Turkish_Martyrs'_Memorial designer "Hüseyin_Bütüner_and_Hilmi_Güner" The Baku_Turkish_Martyrs'_Memorial , designed by "Hüseyin_Bütüner_and_Hilmi_Güner" , is located in Azerbaijan , where the leader is Artur_Rasizade .
existential Azerbaijan leaderTitle Prime_Minister The Baku_Turkish_Martyrs'_Memorial is found in Azerbaijan , a country led by the Prime_Minister Artur_Rasizade .
relative_adverb Uttar_Pradesh isPartOf Bundelkhand Agra_Airport is located in Uttar_Pradesh ( lead by Ram_Naik ) which is part of Awadh and Bundelkhand .
relative_subject Azerbaijan leaderTitle Prime_Minister The Baku_Turkish_Martyrs'_Memorial is found in Azerbaijan , a country led by the Prime_Minister Artur_Rasizade .
coordinated_full_clauses South_Africa ethnicGroup White_South_African 11_Diagonal_Street is in South_Africa where the leader is Jacob_Zuma . An ethnic group found there are White_South_African .
coordinated_full_clauses 20_Fenchurch_Street floorCount 34 Boris_Johnson is a leader in London where 20_Fenchurch_Street with 34 floors is located .
coordinated_full_clauses Amdavad_ni_Gufa country India Anandiben_Patel was the leader of Gujarat , India , the location of Amdavad_ni_Gufa .
coordinated_full_clauses United_States language English_language Joe_Biden is the leader of the United_States where the native language is English_language . 250_Delaware_Avenue is also located there .
possessif Buffalo_,_New_York isPartOf Erie_County_,_New_York 250_Delaware_Avenue is a location in Buffalo_,_New_York , Erie_County_,_New_York , where the leader is Byron_Brown .
possessif Baku_Turkish_Martyrs'_Memorial designer "Hüseyin_Bütüner_and_Hilmi_Güner" Azerbaijan , where the leader is Artur_Rasizade , is the location of the Baku_Turkish_Martyrs'_Memorial . The memorial was designed by "Hüseyin_Bütüner_and_Hilmi_Güner" .
apposition United_States leaderTitle President_of_the_United_States 250_Delaware_Avenue is located in the United_States , with Paul_Ryan being the President_of_the_United_States .
relative_object Chicago isPartOf Cook_County_,_Illinois Susana_Mendoza is the leader of Chicago ( Cook_County_,_Illinois ) where 300_North_LaSalle is located .
passive_voice United_States language English_language Paul_Ryan is a leader of the United_States in which English_language is the language and is home to 250_Delaware_Avenue .
apposition Alcobendas leaderParty People's_Party_(Spain) Adolfo_Suárez_Madrid–Barajas_Airport is found in Alcobendas which is part of the Community_of_Madrid . The leader party at Alcobendas is the People's_Party_(Spain) .
relative_subject Madrid leaderParty Ahora_Madrid Adolfo_Suárez_Madrid–Barajas_Airport is located in Madrid which is part of the Community_of_Madrid lead by Ahora_Madrid .
relative_subject Addis_Ababa country Ethiopia Addis_Ababa in Ethiopia is the location of the Addis_Ababa_Stadium and the Addis_Ababa_City_Hall .
existential New_York_City country United_States Asser_Levy_Public_Baths are located in New_York_City , part of New_York in the United_States .
apposition Greenville_,_Wisconsin country United_States Appleton_International_Airport can be found in Greenville_,_Wisconsin which is part of Ellington_,_Wisconsin , in the United_States .
existential Cleveland leaderName Frank_G._Jackson 200_Public_Square is located in Cleveland , part of Cuyahoga_County_,_Ohio which is lead by Frank_G._Jackson .
relative_subject 3Arena owner Live_Nation_Entertainment The 3Arena is located in Dublin , Leinster and is owned by Live_Nation_Entertainment .
passive_voice Uttar_Pradesh leaderName Ram_Naik Awadh is part of Uttar_Pradesh which is where Agra_Airport is and where Ram_Naik is the leader .
passive_voice Cleveland leaderName Frank_G._Jackson 200_Public_Square is in Cleveland which is part of Cuyahoga_County_,_Ohio . The leader in Cleveland is Frank_G._Jackson .
existential New_York_City country United_States The Asser_Levy_Public_Baths are located in New_York_City ( part of New_Netherland ) United_States .
passive_voice Addis_Ababa country Ethiopia Addis_Ababa in Ethiopia is the location of the Addis_Ababa_Stadium and the Addis_Ababa_City_Hall .
coordinated_clauses Alcobendas leaderParty People's_Party_(Spain) Adolfo_Suárez_Madrid–Barajas_Airport is found in Alcobendas which is part of the Community_of_Madrid . The leader party at Alcobendas is the People's_Party_(Spain) .
coordinated_clauses Cleveland country United_States 200_Public_Square is located in the United_States in Cleveland , Ohio .
coordinated_clauses Akita_Museum_of_Art country Japan The Akita_Museum_of_Art is an art museum located in the city of Akita_,_Akita , Akita_Prefecture , Japan .
relative_object Agra_Airport elevationAboveTheSeaLevel_(in_metres) 167.94 With an elevation of 167.94 metres above sea level , Agra_Airport is located in Uttar_Pradesh which is part of Bundelkhand .
relative_object Alcobendas country Spain Adolfo_Suárez_Madrid–Barajas_Airport is found in Alcobendas , which is part of the Community_of_Madrid , in Spain .
relative_object Egg_Harbor_Township_,_New_Jersey country United_States Atlantic_City_International_Airport is located at Egg_Harbor_Township_,_New_Jersey which is part of New_Jersey , in the United_States .
relative_object Chicago leaderName Rahm_Emanuel 300_North_LaSalle is located in Chicago , Cook_County_,_Illinois , with Rahm_Emanuel being the leader of Chicago .
direct_object Uttar_Pradesh leaderName Ram_Naik Agra_Airport is located in Uttar_Pradesh which is part of Bundelkhand . The leader there is Ram_Naik .
relative_object Madrid leaderParty Ahora_Madrid The leader party in Madrid ( which is part of the Community_of_Madrid and has the Adolfo_Suárez_Madrid–Barajas_Airport ) is Ahora_Madrid .
relative_object Cleveland country United_States Cleveland is part of Cuyahoga_County_,_Ohio in the United_States , it ' s home to 200_Public_Square .
relative_object New_York_City country United_States The location of Asser_Levy_Public_Baths is New_York_City , Manhattan in the United_States .
relative_object Dublin country Republic_of_Ireland 3Arena is located in Dublin ( in the Republic_of_Ireland ) which is a part of Leinster .
direct_object Cleveland country United_States The United_States is home to 200_Public_Square in Cleveland , Cuyahoga_County_,_Ohio .
coordinated_full_clauses 250_Delaware_Avenue floorCount 12 Construction of 250_Delaware_Avenue began in "January_,_2014" and has 12 floors with an area of 30843.8_square_metres .
coordinated_clauses 200_Public_Square completionDate 1985 Completed in 1985 , 200_Public_Square has 45 floors , covering 111484_square_metres .
relative_object United_States leaderName Paul_Ryan 250_Delaware_Avenue is located in the United_States . The leader of this country has the title President_of_the_United_States , which in this case is Paul_Ryan .
direct_object Texas capital Austin_,_Texas In Texas , people are called Texan and the capital of Texas is Austin_,_Texas . Andrews_County_Airport is located in Texas .
direct_object United_Kingdom currency Pound_sterling 20_Fenchurch_Street is located within the United_Kingdom where the currency is Pound_sterling and the demonym for natives of the United_Kingdom is British_people .
apposition Adisham_Hall completionDate 1931 Adisham_Hall , completed in 1931 in the architectural Tudor_Revival_architecture style , is located in "Haputale_,_Sri_Lanka" .
direct_object Adisham_Hall completionDate 1931 Adisham_Hall in Haputale was finished in 1931 and is of the Tudor_Revival_architecture style .
existential Adisham_Hall completionDate 1931 Adisham_Hall in Haputale was finished in 1931 in the "Tudor_and_Jacabian" style .
direct_object Adisham_Hall completionDate 1931 The Adisham_Hall , which is located in Sri_Lanka , was completed in 1931 and architectural style is Tudor_Revival_architecture .
coordinated_clauses Adisham_Hall completionDate 1931 Adisham_Hall completed in 1931 with a Tudor_Revival_architecture style is located at "Haputale_,_Sri_Lanka" .
passive_voice Alan_B._Miller_Hall architect Robert_A._M._Stern Robert_A._M._Stern is the architect of Alan_B._Miller_Hall , owned by The College_of_William_&_Mary and located in Virginia .
existential Alan_B._Miller_Hall architect Robert_A._M._Stern The College_of_William_&_Mary is owned by Alan_B._Miller_Hall in Williamsburg_,_Virginia . It was designed by Robert_A._M._Stern .
existential Alan_B._Miller_Hall completionDate "1_June_2009" Alan_B._Miller_Hall was completed "1_June_2009" in Williamsburg_,_Virginia and is owned by the College_of_William_&_Mary .
relative_subject Alan_B._Miller_Hall buildingStartDate "30_March_2007" The College_of_William_&_Mary is the owner of the Alan_B._Miller_Hall in Virginia . The building was begun on "30_March_2007" .
relative_subject Alan_B._Miller_Hall completionDate "1_June_2009" Alan_B._Miller_Hall was completed "1_June_2009" in Williamsburg_,_Virginia and is owned by the College_of_William_&_Mary .
direct_object Denmark leaderName Lars_Løkke_Rasmussen Lars_Løkke_Rasmussen is a leader of Denmark which is the location of the AC_Hotel_Bella_Sky_Copenhagen , a tenant of the Marriott_International Hotel .
coordinated_clauses AC_Hotel_Bella_Sky_Copenhagen architect 3XN The tenant of the AC_Hotel_Bella_Sky_Copenhagen ( designed by architect 3XN ) in Copenhagen is the Marriott_International Hotel .
existential Augustus_Pugin birthPlace Bloomsbury Augustus_Pugin , born in Bloomsbury , designed Adare_Manor and Palace_of_Westminster .
existential Julia_Morgan birthPlace California California born Julia_Morgan designed significant buildings in Chinatown_,_San_Francisco and also the Asilomar_Conference_Grounds .
passive_voice Akita_Prefecture country Japan The Akita_Museum_of_Art is an art museum in the city of Akita_,_Akita , Akita_Prefecture , Japan .
existential Akita_Prefecture country Japan The Akita_Museum_of_Art is located in Akita_,_Akita , Akita_Prefecture , Japan .
passive_voice India leaderName Sumitra_Mahajan Bhajji originates from India where the currency is the Indian_rupee and the leaders are Narendra_Modi and Sumitra_Mahajan .
passive_voice France leaderName Manuel_Valls The leader of France is Manuel_Valls where the Euro is the currency and the dish Baked_Alaska is from .
relative_adverb India leaderName Sumitra_Mahajan Bhajji comes from the country India , which leader is Sumitra_Mahajan and its currency is the Indian_rupee .
existential Antwerp_International_Airport cityServed Antwerp Antwerp_International_Airport serves the city of Antwerp in Belgium where the French_language is spoken .
passive_voice Italy leaderName Pietro_Grasso Arrabbiata_sauce is from Italy , where the language is Italian_language and Pietro_Grasso is the leader .
passive_voice France leaderName Claude_Bartolone Barny_Cakes originate in France , where French_language is the national language and where Claude_Bartolone is the leader .
passive_voice Philippines ethnicGroup Igorot_people Batchoy is a dish from the Philippines , where Igorot_people are part of one of the ethnic groups and where one of the languages spoken is Arabic .
passive_voice Philippines ethnicGroup Ilocano_people The Ilocano_people are an ethnic group found in the Philippines where Philippine_English is the spoken language . The dish Batchoy comes from the country .
passive_voice Spain ethnicGroup Spaniards Spaniards are the ethnic group of Spain where the language is Spanish_language and where Ajoblanco is from .
relative_adverb Republic_of_Ireland leaderName Enda_Kenny The Republic_of_Ireland is lead by Enda_Kenny , Irish_language is the official language and is home to Adare_Manor .
passive_voice United_States ethnicGroup African_Americans A_Fortress_of_Grey_Ice is from the United_States , where the language is English_language and where one of the ethnic groups is African_Americans .
relative_adverb United_States ethnicGroup Asian_Americans English_language is spoken in the United_States ; which is home to Asian_Americans and the origin place of A_Severed_Wasp .
passive_voice United_States ethnicGroup Native_Americans A_Severed_Wasp is from the United_States where the language is English_language and the Native_Americans are one of the ethnic groups .
relative_adverb Philippines ethnicGroup Zamboangans The language spoken in the Philippines is Philippine_Spanish , Batchoy is eaten there and the Zamboangans and Moro_people can be found there .
direct_object United_States ethnicGroup Asian_Americans The country of Akron_,_Ohio is in the United_States where English_language is the language and Asian_Americans are an ethnic group .
direct_object United_States ethnicGroup Asian_Americans Albany_,_Georgia , is in the United_States , where English_language is spoken and Asian_Americans are one of the ethnic groups .
direct_object Indonesia leaderName Jusuf_Kalla Indonesian_language is the language of Indonesia where Jusuf_Kalla is the leader and Arem-arem is a dish .
direct_object Mexico leaderName Silvano_Aureoles_Conejo Bionico is from Mexico where the leader is Silvano_Aureoles_Conejo and the language is Spanish_language .
direct_object Philippines ethnicGroup Zamboangans Batchoy comes from the Philippines where Philippine_English is the language spoken . It is also where Zamboangans are a group .
direct_object Philippines ethnicGroup Zamboangans Batchoy is eaten in the Philippines where the language used is Philippine_Spanish and one of the ethnic groups are the Zamboangans .
direct_object Philippines ethnicGroup Ilocano_people The Ilocano_people are an ethnic group found in the Philippines where Philippine_English is the spoken language . The dish Batchoy comes from the country .
direct_object Sri_Lanka capital Sri_Jayawardenepura_Kotte Tamil_language is the language of Sri_Lanka where Adisham_Hall is located and the capital city is Sri_Jayawardenepura_Kotte .
direct_object United_States ethnicGroup African_Americans A_Fortress_of_Grey_Ice is from the United_States , where the language is English_language and where one of the ethnic groups is African_Americans .
direct_object United_States ethnicGroup Asian_Americans The book Alcatraz_Versus_the_Evil_Librarians comes from the United_States where English_language is the primary language . Additionally , Asian_Americans is one of the ethnic groups in the United_States .
direct_object United_States ethnicGroup Native_Americans A_Severed_Wasp is from the United_States where English_language is the primary language . In addition Native_Americans are an ethnic group in the United_States .
direct_object English_language spokenIn Great_Britain A_Fortress_of_Grey_Ice is from the United_States where English_language is spoken as it is in Great_Britain .
direct_object Philippines ethnicGroup Zamboangans Batchoy come from the Philippines , a country where one of the languages spoken is Arabic . Also where two of the ethnic groups are , the Ilocano_people and the Zamboangans .
passive_voice United_States ethnicGroup African_Americans Albuquerque_,_New_Mexico is in the United_States , where English_language is the official language and African_Americans are one of the country ' s ethnic group .
passive_voice Philippines ethnicGroup Zamboangans Zamboangans are a group in the Philippines , where the spoken language is Philippine_Spanish and Batchoy is eaten .
passive_voice Republic_of_Ireland leaderName Enda_Kenny Adare_Manor is located in the Republic_of_Ireland , the leader of this country is Enda_Kenny and one of the languages used is English_language .
passive_voice United_States ethnicGroup Native_Americans A_Fortress_of_Grey_Ice is from the United_States , where the language is English_language and Native_Americans are one of the ethnic groups .
passive_voice Philippines ethnicGroup Zamboangans Zamboangans are a group in the Philippines and the Igorot_people are an ethnic group in the Philippines . Philippine_Spanish is the spoken language and one of the foods eaten in the Philippine_Spanish is Batchoy .
relative_adverb Indonesia leaderName Joko_Widodo Arem-arem is a food found in Indonesia , which leader is Joko_Widodo and the language is Indonesian_language .
relative_adverb France leaderName Manuel_Valls Baked_Alaska is from the country of France , which leader is Manuel_Valls and the spoken language is French_language .
relative_adverb Philippines ethnicGroup Zamboangans Zamboangans are a group in the Philippines , where the spoken language is Philippine_Spanish and Batchoy is eaten .
relative_adverb Singapore leaderName Tony_Tan Beef_kway_teow is a dish from the country of Singapore . Standard_Chinese is their language spoken , this Country leader is Tony_Tan .
relative_adverb United_States ethnicGroup Native_Americans A_Fortress_of_Grey_Ice is from the United_States , where the language is English_language and Native_Americans are one of the ethnic groups .
juxtaposition United_States ethnicGroup Asian_Americans Albany_,_Georgia , is in the United_States , where English_language is spoken and Asian_Americans are one of the ethnic groups .
juxtaposition Philippines ethnicGroup Ilocano_people In the Philippines ; the spoken language is Philippine_Spanish , Batchoy is eaten and one of the ethnic groups is the Ilocano_people .
juxtaposition English_language spokenIn Great_Britain English_language is spoken in both Great_Britain and the United_States . Additionally , A_Severed_Wasp is from the United_States .
direct_object Aarhus_Airport location Tirstrup Aarhus_Airport is located in Tirstrup , Denmark and Greenlandic_language is the official language in Denmark .
direct_object A_Loyal_Character_Dancer publisher Soho_Press A_Loyal_Character_Dancer is published in the United_States by Soho_Press . The national language of the United_States is English_language .
direct_object United_States ethnicGroup Native_Americans A_Severed_Wasp originates from the United_States . where the language is English_language and the Native_Americans are an ethnic group .
direct_object Philippines ethnicGroup Zamboangans Batchoy is a dish eaten in the Philippines . The language used there is Philippine_Spanish and two of its ethnic groups are Ilocano_people and Zamboangans .
apposition Aarhus_Airport location Tirstrup The location of Aarhus_Airport is Tirstrup , in Denmark where one of the languages is Faroese_language .
apposition United_States ethnicGroup Asian_Americans The United_States , where the Asian_Americans are an ethnic group , uses the English_language and is the location of Angola_,_Indiana .
apposition Italy leaderName Pietro_Grasso Arrabbiata_sauce is from Italy , where the language is Italian_language and Pietro_Grasso is the leader .
apposition France leaderName Claude_Bartolone Barny_Cakes originated in France , where the leader is Claude_Bartolone and the national language is French_language .
apposition Philippines ethnicGroup Moro_people The Moro_people are an ethnic group in the Philippines , where Arabic is a language spoken . It is also the country Batchoy comes from .
apposition Republic_of_Ireland leaderName Enda_Kenny Adare_Manor is in the Irish_language speaking Republic_of_Ireland , where the leader is Enda_Kenny .
apposition English_language spokenIn Great_Britain Alcatraz_Versus_the_Evil_Librarians is from the United_States , where the language is English_language . English_language is spoken in Great_Britain .
apposition United_States ethnicGroup Native_Americans A_Severed_Wasp is from the United_States where English_language is the primary language . In addition Native_Americans are an ethnic group in the United_States .
apposition Philippines ethnicGroup Ilocano_people The Ilocano_people and the Chinese_Filipino are ethnic groups from the Philippines where Philippine_English is the language spoken . Batchoy comes from this country .
relative_subject Abilene_,_Texas isPartOf Jones_County_,_Texas The United_States uses the English_language and is the location of Abilene_,_Texas , part of Jones_County_,_Texas .
relative_subject United_States ethnicGroup Native_Americans English_language is the language of the United_States , where you will find the town of Akron_,_Ohio and where Native_Americans are an ethnic group .
relative_adverb English_language spokenIn Great_Britain A_Wizard_of_Mars originates from the United_States . In this country and Great_Britain English_language is the language spoken .
relative_subject Abilene_,_Texas isPartOf Jones_County_,_Texas Abilene_,_Texas is part of Jones_County_,_Texas , in the United_States , where English_language is spoken .
relative_subject United_States ethnicGroup Native_Americans In the United_States ; English_language is spoken , Native_Americans are one of the ethnic groups and , it is the location of Angola_,_Indiana .
relative_subject Italy leaderName Pietro_Grasso Arrabbiata_sauce can be found in Italy where the language is Italian_language and Pietro_Grasso is a leader .
relative_subject Republic_of_Ireland leaderName Enda_Kenny The Republic_of_Ireland is lead by Enda_Kenny , Irish_language is the official language and is home to Adare_Manor .
relative_subject United_States ethnicGroup Asian_Americans English_language is spoken in the United_States ; which is home to Asian_Americans and the origin place of A_Severed_Wasp .
relative_subject France leaderName Manuel_Valls Barny_Cakes are found in France where the French_language is spoken and the leaders are François_Hollande and Manuel_Valls .
relative_subject Philippines ethnicGroup Zamboangans The Zamboangans and the Moro_people are ethnic groups in the Philippines ; where one of the languages is Philippine_Spanish . Batchoy is eaten in the Philippines .
possessif Sri_Lanka capital Sri_Jayawardenepura_Kotte Adisham_Hall is located in Sri_Lanka which has the capital city of Sri_Jayawardenepura_Kotte . The language spoken in Sri_Lanka is Tamil_language .
relative_object United_States ethnicGroup Asian_Americans Asian_Americans are an ethnic group of the United_States . It is an English_language speaking country and is where Albany_,_Georgia is located .
relative_object United_States ethnicGroup Native_Americans Albany_,_Oregon is in the United_States . Where English_language is spoken and Native_Americans are one of the ethnic groups .
relative_object Spain leaderName Felipe_VI_of_Spain The leader of Spain is Felipe_VI_of_Spain and it is also where Spanish_language is spoken and the dish Arròs_negre is from .
relative_object English_language spokenIn Great_Britain English_language is spoken in both Great_Britain and the United_States . Additionally , A_Severed_Wasp is from the United_States .
relative_object United_States ethnicGroup African_Americans English_language is the language of the United_States where A_Wizard_of_Mars was published . One of the ethnic groups in the country are the African_Americans .
relative_object Philippines ethnicGroup Zamboangans Batchoy is eaten in the Philippines where the spoken language is Philippine_Spanish . The Zamboangans and Ilocano_people are both ethnic groups in the country .
coordinated_full_clauses Aarhus_Airport location Tirstrup Aarhus_Airport is located in Tirstrup , Denmark . German_language is the language spoken in that country .
coordinated_full_clauses United_States ethnicGroup Native_Americans Angola_,_Indiana is in the United_States , where English_language is spoken . It is also where Native_Americans , are one of the many ethnic groups .
coordinated_full_clauses Italy leaderName Pietro_Grasso Arrabbiata_sauce can be found in Italy where the language is Italian_language and Pietro_Grasso is a leader .
coordinated_full_clauses A_Loyal_Character_Dancer publisher Soho_Press A_Loyal_Character_Dancer is published by Soho_Press in the United_States . The language of the United_States is English_language .
coordinated_full_clauses English_language spokenIn Great_Britain A_Loyal_Character_Dancer is published in the United_States where , just like in Great_Britain , the language spoken is English_language .
coordinated_full_clauses English_language spokenIn Great_Britain A_Fortress_of_Grey_Ice is from the United_States where English_language is spoken as it is in Great_Britain .
coordinated_full_clauses Philippines ethnicGroup Moro_people The Moro_people and the Ilocano_people are ethnic groups within the Philippines where Batchoy is eaten and the language used is Philippine_Spanish .
coordinated_full_clauses Philippines ethnicGroup Ilocano_people The Igorot_people and Ilocano_people are both ethnic groups in the Philippines . Batchoy is eaten there . In addition Arabic is one of the languages spoken in the country .
direct_object English_language spokenIn Great_Britain The English_language is spoken in Great_Britain and the United_States ( the origin of A_Wizard_of_Mars ).
possessif A_Loyal_Character_Dancer publisher Soho_Press A_Loyal_Character_Dancer is published by Soho_Press in the United_States . The language of the United_States is English_language .
possessif English_language spokenIn Great_Britain A_Wizard_of_Mars originates from the United_States . In this country and Great_Britain English_language is the language spoken .
possessif English_language spokenIn Great_Britain The English_language is spoken in Great_Britain and the United_States . A_Fortress_of_Grey_Ice is from the United_States .
possessif Spain leaderName Felipe_VI_of_Spain Arròs_negre is a traditional dish from Spain where they speak Spanish_language and the leader is Felipe_VI_of_Spain .
possessif France leaderName Manuel_Valls Barny_Cakes can be found in France where they speak French_language . Either François_Hollande or Manuel_Valls is the leader of France .
direct_object Philippines ethnicGroup Ilocano_people In the Philippines ; the spoken language is Philippine_Spanish , Batchoy is eaten and one of the ethnic groups is the Ilocano_people .
apposition Indonesia leaderName Joko_Widodo Arem-arem is a food found in Indonesia , which leader is Joko_Widodo and the language is Indonesian_language .
apposition United_States ethnicGroup Native_Americans A_Severed_Wasp originates from the United_States . where the language is English_language and the Native_Americans are an ethnic group .
apposition Philippines ethnicGroup Moro_people Arabic is one of the languages spoken in the Philippines and Batchoy comes from there . Ethnic groups from the Philippines include the Ilocano_people and Moro_people .
relative_subject English_language spokenIn Great_Britain The English_language is spoken in Great_Britain and the United_States ( the origin of A_Wizard_of_Mars ).
relative_subject Philippines ethnicGroup Ilocano_people Batchoy is eaten in the Philippines . The country include the Ilocano_people , Chinese_Filipino and Arabic is spoken there .
possessif Addis_Ababa_City_Hall height "42_m" The "Government_of_Addis_Ababa" is the current tenants of the Addis_Ababa_City_Hall . which has a floor area of 140000_square_metres . It is "42_m" high .
apposition Ethiopia leaderName Hailemariam_Desalegn Ethiopia is lead by Hailemariam_Desalegn and is the location of the Addis_Ababa_City_Hall in Addis_Ababa .
existential Addis_Ababa isPartOf Addis_Ababa_Stadium Addis_Ababa_Stadium and Addis_Ababa_City_Hall are located in Addis_Ababa in the country of Ethiopia .
existential Addis_Ababa isPartOf Addis_Ababa_Stadium Addis_Ababa_City_Hall and the Addis_Ababa_Stadium are located in Addis_Ababa , Ethiopia .
apposition India leaderName T._S._Thakur The Prime Minister of India is Narendra_Modi and T._S._Thakur is another leader . The country is the location of Amdavad_ni_Gufa in Gujarat .
relative_subject Alan_B._Miller_Hall location Virginia Alan_B._Miller_Hall is located in Virginia United_States . The current tenant of the Alan_B._Miller_Hall is the Mason_School_of_Business .
existential Asam_pedas mainIngredients "Fish_cooked_in_sour_and_hot_sauce" The main ingredients of Asam_pedas , a food from the "Sumatra_and_Malay_Peninsula" regions of Malaysia , consist of "Fish_cooked_in_sour_and_hot_sauce" .
existential Ayam_penyet mainIngredients "Squeezed"_or_"smashed"_fried_chicken_served_with_sambal Ayam_penyet ' s main ingredients are "Squeezed"_or_"smashed"_fried_chicken_served_with_sambal . It originates from Java and the Singapore region .
apposition Amatriciana_sauce ingredient Olive_oil Olive_oil is an ingredient of Amatriciana_sauce , which comes from the Lazio region of Italy .
apposition Arròs_negre ingredient Cuttlefish Cuttlefish is an ingredient in Arròs_negre , which is a traditional dish from the Valencian_Community in Spain .
apposition Asam_pedas mainIngredients "Fish_cooked_in_sour_and_hot_sauce" The dish Asam_pedas comes from the region of "Sumatra_and_Malay_Peninsula" , "Indonesia_and_Malaysia" and the main ingredients are "Fish_cooked_in_sour_and_hot_sauce" .
apposition Java ethnicGroup Banyumasan_people Ayam_penyet is a popular dish in Malaysia , but originated in Java where the Banyumasan_people are an ethnic group .
existential Ayam_penyet ingredient Fried_chicken Ayam_penyet ( Indonesia , Singapore ) has Fried_chicken in it .
apposition Bacon_Explosion ingredient Sausage Sausage is an ingredient in a Bacon_Explosion which originates from the Kansas_City_metropolitan_area in the United_States .
apposition Baked_Alaska ingredient Ice_cream Ice_cream is an ingredient in Baked_Alaska , which comes from both the country of France and the region of New_York .
apposition Baked_Alaska ingredient Meringue Meringue is an ingredient of a Baked_Alaska , which is from the New_York region and France .
apposition Bandeja_paisa ingredient Kidney_bean Kidney_bean are an ingredient of Bandeja_paisa which is a Colombian_cuisine from the Antioquia_Department .
apposition Bandeja_paisa ingredient Lemon Bandeja_paisa is from the Paisa_Region , Colombian_cuisine and one of the ingredients is Lemon .
apposition Bandeja_paisa ingredient Chorizo Chorizo is an ingredient in the Colombian_cuisine of Bandeja_paisa which originated in the Antioquia_Department .
apposition Singapore leaderName Tony_Tan Tony_Tan is a leader in Singapore . Beef_kway_teow is also from "Singapore_and_Indonesia" .
apposition Singapore leaderName Tony_Tan A popular food in Indonesia , Beef_kway_teow originates from Singapore . The country where Tony_Tan is the leader .
apposition Malaysia ethnicGroup Malaysian_Malay Asam_pedas is found in the Malay_Peninsula and throughout Malaysia . The Malaysian_Chinese and the Malaysian_Malay are both ethnic groups found in the country .
apposition Hong_Kong leaderName Carrie_Lam_(politician) Baked_Alaska is from France and also served in Hong_Kong . Carrie_Lam_(politician) is the leader of Hong_Kong and Manuel_Valls is a leader in France .
apposition Karnataka leaderName Vajubhai_Vala Bhajji comes from the country India in the Karnataka region where the leaders are Sumitra_Mahajan and Vajubhai_Vala .
relative_subject Indonesia leaderName Jusuf_Kalla Arem-arem is a food found "Nationwide_in_Indonesia_,_but_more_specific_to_Java" . Jusuf_Kalla is the leader of Indonesia .
relative_subject Arròs_negre ingredient Cephalopod_ink Arròs_negre is from the region of Catalonia , Spain and one of its ingredients is Cephalopod_ink .
relative_subject Arròs_negre ingredient Squid Arròs_negre is a dish from the Valencian_Community in Spain and contains Squid .
relative_subject Asam_pedas mainIngredients "Fish_cooked_in_sour_and_hot_sauce" The main ingredient of Asam_pedas is "Fish_cooked_in_sour_and_hot_sauce" . The dish is from the country Indonesia and is eaten in the Malay_Peninsula .
relative_subject Asam_pedas mainIngredients "Fish_cooked_in_sour_and_hot_sauce" The dish Asam_pedas comes from the region of "Sumatra_and_Malay_Peninsula" , "Indonesia_and_Malaysia" and the main ingredients are "Fish_cooked_in_sour_and_hot_sauce" .
relative_subject Java ethnicGroup Javanese_people Ayam_penyet is a dish from Java in the region of Malaysia . The Javanese_people are an ethnic group in Java .
relative_subject Baked_Alaska ingredient Christmas_pudding Baked_Alaska contains Christmas_pudding and is from the New_York region of the United_States .
existential Bandeja_paisa ingredient Rice The Colombian_cuisine dish of Bandeja_paisa which includes Rice can be found in the Antioquia_Department .
relative_subject Bandeja_paisa ingredient Lemon Bandeja_paisa is from the Paisa_Region , Colombian_cuisine and one of the ingredients is Lemon .
relative_subject Bandeja_paisa ingredient Chorizo An ingredient in the dish Bandeja_paisa is Chorizo , the dish comes from the Antioquia_Department and is typical Colombian_cuisine .
relative_subject Bandeja_paisa ingredient Pork_belly Pork_belly is one of the ingredients of the dish Bandeja_paisa . This is a traditional dish from the Paisa_Region and is part of Colombian_cuisine .
relative_subject Catalonia leaderName Carles_Puigdemont Arròs_negre is from the Catalonia region in Spain where Carles_Puigdemont is the leader .
relative_subject Sri_Lanka leaderName Ranil_Wickremesinghe Ampara_Hospital is in Sri_Lanka in Ampara_District . The leader of Sri_Lanka is Ranil_Wickremesinghe .
relative_subject Sumatra ethnicGroup Batak Asam_pedas is a dish from Sumatra and Malaysia . Malaysian_Malay is an ethnic group in Malaysia and Batak of Sumatra .
relative_subject France leaderName Manuel_Valls Baked_Alaska , famous in New_York , is originally from France . The leader of France is either Gérard_Larcher and / or Manuel_Valls .
relative_subject Karnataka leaderName Vajubhai_Vala Bhajji originate from the Karnataka region of India . The leader in Karnataka is Vajubhai_Vala and in India is Sumitra_Mahajan .
passive_voice Arrabbiata_sauce ingredient Chili_pepper Arrabbiata_sauce from Rome in Italy contains Chili_pepper .
passive_voice Arròs_negre ingredient Cubanelle Arròs_negre contains Cubanelle and is a traditional dish from the Valencian_Community in Spain .
direct_object Asam_pedas mainIngredients "Fish_cooked_in_sour_and_hot_sauce" Asam_pedas is a dish of "Fish_cooked_in_sour_and_hot_sauce" , it is found in the region of "Sumatra_and_Malay_Peninsula" and popular in Indonesia .
passive_voice Sumatra ethnicGroup Minangkabau_people Asam_pedas is a food found in Malaysia and Sumatra . Malaysian_Malay is an ethnic group in Malaysia and Sumatra has an ethnic group called the Minangkabau_people .
direct_object Hong_Kong leaderName Carrie_Lam_(politician) Baked_Alaska hail from France and is found in Hong_Kong . France is led by Manuel_Valls ; Hong_Kong , Carrie_Lam_(politician) .
passive_voice Asam_pedas mainIngredients "Fish_cooked_in_sour_and_hot_sauce" Asam_pedas ( from the Malay_Peninsula region ) comes from the countries of "Indonesia_and_Malaysia" . The main ingredients of Asam_pedas are "Fish_cooked_in_sour_and_hot_sauce" .
passive_voice Bakso ingredient Noodle Bakso is a Chinese_cuisine Noodle dish found in Indonesia .
passive_voice Beef_kway_teow ingredient Sesame_oil An ingredient in Beef_kway_teow , from the Singapore region and popular in Indonesia , is Sesame_oil .
passive_voice Sumatra ethnicGroup Malays_(ethnic_group) Asam_pedas is a food found in Malaysia and Sumatra . Malays_(ethnic_group) and Batak are ethnic groups of Sumatra .
passive_voice Bandeja_paisa mainIngredients "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" The main ingredients of Bandeja_paisa are "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" . The dish is from Colombian_cuisine and originates in the Paisa_Region .
passive_voice India leaderName T._S._Thakur Bhajji come from the Karnataka region of India . The leaders of the country are T._S._Thakur and Sumitra_Mahajan .
passive_voice Ajoblanco ingredient Almond Almond is one of the ingredients in Ajoblanco which is from the Andalusia region , in the country of Spain .
passive_voice Andalusia leaderName Susana_Díaz Ajoblanco is from Andalusia where Susana_Díaz is a leader . Ajoblanco is from Spain .
passive_voice Arròs_negre ingredient Squid Squid is an ingredient of Arròs_negre which is from the region of Catalonia in Spain .
passive_voice Arròs_negre ingredient Cubanelle Cubanelle is one of the ingredients in Arròs_negre , which comes from the region of the Valencian_Community in Spain .
passive_voice Arròs_negre ingredient Cuttlefish Cuttlefish is an ingredient in Arròs_negre , which is a traditional dish from the Valencian_Community in Spain .
existential Asam_pedas mainIngredients "Fish_cooked_in_sour_and_hot_sauce" The main ingredients of Asam_pedas consist of "Fish_cooked_in_sour_and_hot_sauce" . The dish originated in Sumatra and is also found in Malaysia .
passive_voice Baked_Alaska ingredient Christmas_pudding Christmas_pudding is an ingredient in Baked_Alaska , which comes from the region of New_York and the country of France .
passive_voice Bandeja_paisa ingredient Kidney_bean Kidney_bean are an ingredient of Bandeja_paisa which is a Colombian_cuisine from the Antioquia_Department .
passive_voice Bandeja_paisa ingredient Chorizo Chorizo is an ingredient in the Colombian_cuisine of Bandeja_paisa which originated in the Antioquia_Department .
passive_voice Singapore leaderName Tony_Tan A popular food in Indonesia , Beef_kway_teow originates from Singapore . The country where Tony_Tan is the leader .
existential Bionico ingredient Raisin Containing Raisin , Bionico is found in the region of Jalisco , in Mexico .
passive_voice Sumatra ethnicGroup Minangkabau_people Asam_pedas is a dish from Sumatra , Malaysia . The Batak and the Minangkabau_people are ethnic groups from Sumatra .
passive_voice India leaderName Sumitra_Mahajan Bhajji originate from the Karnataka region in India . The country ' s leaders are Narendra_Modi and Sumitra_Mahajan .
coordinated_clauses Ajoblanco ingredient Garlic Ajoblanco is a dish from the Andalusia region of Spain and includes the ingredient Garlic .
coordinated_clauses Ajoblanco ingredient Almond Almond is an ingredient in Ajoblanco , which is from Andalusia in Spain .
coordinated_clauses Arròs_negre ingredient Cephalopod_ink Arròs_negre is from the Catalonia region of Spain , it includes Cephalopod_ink .
coordinated_clauses Arròs_negre ingredient Squid Squid is an ingredient of Arròs_negre which is from the region of Catalonia in Spain .
coordinated_clauses Arròs_negre ingredient Squid Arròs_negre made with Squid is a traditional dish from the region of the Valencian_Community in Spain .
coordinated_clauses Spain leaderName Felipe_VI_of_Spain Arròs_negre is a traditional dish from the Valencian_Community of Spain the country of which Felipe_VI_of_Spain is the leader .
coordinated_clauses Asam_pedas mainIngredients "Fish_cooked_in_sour_and_hot_sauce" The dish Asam_pedas comes from the region of "Sumatra_and_Malay_Peninsula" , "Indonesia_and_Malaysia" and the main ingredients are "Fish_cooked_in_sour_and_hot_sauce" .
coordinated_clauses Ayam_penyet ingredient Fried_chicken Fried_chicken is an ingredient in Ayam_penyet which is from Indonesia and a popular Malaysia dish .
coordinated_clauses Baked_Alaska ingredient Christmas_pudding Christmas_pudding is an ingredient in Baked_Alaska , which comes from the region of New_York and the country of France .
coordinated_clauses Baked_Alaska ingredient Sponge_cake An ingredient of Baked_Alaska is Sponge_cake , the dish originates from New_York , United_States .
coordinated_clauses Bandeja_paisa ingredient Avocado Avocado is one of the ingredients in the dish Bandeja_paisa , which is typical Colombian_cuisine originating from the Paisa_Region .
coordinated_clauses Bandeja_paisa ingredient Lemon Bandeja_paisa is from the Paisa_Region , Colombian_cuisine and one of the ingredients is Lemon .
coordinated_clauses Bandeja_paisa ingredient Chorizo Bandeja_paisa is from the Paisa_Region and typical Colombian_cuisine . Chorizo is one of its ingredients .
coordinated_clauses Beef_kway_teow ingredient Sesame_oil One of the ingredients of the Singapore dish Beef_kway_teow is Sesame_oil . The dish is also eaten in Indonesia .
coordinated_clauses Bionico ingredient Granola Granola is one of the ingredients of the food Bionico which comes from the region of Jalisco in Mexico .
coordinated_clauses Catalonia leaderName Carles_Puigdemont Carles_Puigdemont is the leader of Catalonia . Arròs_negre , from the Catalonia region , is a traditional dish from Spain .
coordinated_clauses Java ethnicGroup Baduy From the Singapore region , Ayam_penyet , is a food found in Java where an ethic group is the Baduy .
coordinated_clauses Sumatra ethnicGroup Batak Indonesia is the country Asam_pedas comes from in the Sumatra region . Ethnic groups in Sumatra include the Batak and the Acehnese_people .
coordinated_clauses Sumatra ethnicGroup Minangkabau_people Asam_pedas is a dish from Sumatra , Malaysia . The Batak and the Minangkabau_people are ethnic groups from Sumatra .
coordinated_clauses France leaderName Manuel_Valls Baked_Alaska comes from New_York and also from France where the leaders are Manuel_Valls and Gérard_Larcher .
coordinated_clauses Karnataka leaderName Vajubhai_Vala Bhajji comes from the Karnataka region or India . Vajubhai_Vala is a leader of Karnataka and T._S._Thakur is the leader of India .
coordinated_full_clauses Ajoblanco ingredient Almond Almond is an ingredient in Ajoblanco , which is from Andalusia in Spain .
coordinated_full_clauses Indonesia leaderName Jusuf_Kalla Arem-arem is a food found "Nationwide_in_Indonesia_,_but_more_specific_to_Java" . Jusuf_Kalla is the leader of Indonesia .
coordinated_full_clauses Arròs_negre ingredient Cubanelle Cubanelle is an ingredient in Arròs_negre which is a traditional dish from the Catalonia region of Spain .
coordinated_full_clauses Arròs_negre ingredient White_rice Arròs_negre , containing White_rice , is from the Catalonia region in Spain .
coordinated_full_clauses Arròs_negre ingredient Cuttlefish Cuttlefish is an ingredient in Arròs_negre which comes from the region of the Valencian_Community , in Spain .
coordinated_full_clauses Asam_pedas mainIngredients "Fish_cooked_in_sour_and_hot_sauce" The main ingredients of Asam_pedas are "Fish_cooked_in_sour_and_hot_sauce" . Asam_pedas is a dish from Sumatra , in Indonesia .
coordinated_full_clauses Ayam_penyet ingredient Fried_chicken Fried_chicken is an ingredient in Ayam_penyet which is from the country of Java and also the region of Singapore .
coordinated_full_clauses Baked_Alaska ingredient Christmas_pudding Christmas_pudding is an ingredient in Baked_Alaska , which comes from the region of New_York and the country of France .
coordinated_full_clauses Bandeja_paisa ingredient Avocado Avocado is one of the ingredients in Bandeja_paisa which is typical Colombian_cuisine and found in the Antioquia_Department .
coordinated_full_clauses Bandeja_paisa ingredient Pork_belly Part of Colombian_cuisine and found in the Antioquia_Department , Bandeja_paisa ' s main ingredients is Pork_belly .
coordinated_full_clauses Singapore leaderName Tony_Tan Tony_Tan is a leader in Singapore , where Beef_kway_teow is from . The dish is popular "Nationwide_in_Singapore_and_Indonesia" .
coordinated_full_clauses Spain leaderName Felipe_VI_of_Spain Arròs_negre is a traditional dish from the Catalonia region of Spain , the leader of Spain is Felipe_VI_of_Spain , Catalonia is led by the Parliament_of_Catalonia .
coordinated_full_clauses Malaysia ethnicGroup Malaysian_Malay Asam_pedas is found in the Malay_Peninsula and throughout Malaysia . The Malaysian_Chinese and the Malaysian_Malay are both ethnic groups found in the country .
coordinated_full_clauses Sumatra ethnicGroup Minangkabau_people Asam_pedas is a food from Malaysia and Sumatra . Malaysia is the ethnic home of the Malaysian_Chinese and the Minangkabau_people are and ethnic group from Sumatra .
coordinated_full_clauses Bandeja_paisa mainIngredients "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" Bandeja_paisa is a Colombian_cuisine from the Paisa_Region . The main ingredients of Bandeja_paisa are : "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" .
coordinated_full_clauses Karnataka leaderName Vajubhai_Vala Bhajji come from the Karnataka region in India . Vajubhai_Vala is the leader of Karnataka and Sumitra_Mahajan is a leader in India .
coordinated_full_clauses India leaderName T._S._Thakur Bhajji come from the Karnataka region of India , T._S._Thakur is the leader of India and Narendra_Modi is also one of the names of one of the leaders in India .
direct_object Asilomar_Conference_Grounds added_to_the_National_Register_of_Historic_Places "1987-02-27" Asilomar_Conference_Grounds are located at "Asilomar_Blvd_,_Pacific_Grove_,_California" and given the reference of "87000823" in the National Register of Historic Places "1987-02-27" .
coordinated_clauses Asher_and_Mary_Isabelle_Richardson_House yearOfConstruction 1911 Asher_and_Mary_Isabelle_Richardson_House built in 1911 is located at U.S._Route_83 and has the reference number "88002539" in the National Register of Historic Places .
existential Asilomar_Conference_Grounds yearOfConstruction 1913 The Asilomar_Conference_Grounds constructed in 1913 and located on "Asilomar_Blvd_,_Pacific_Grove_,_California" , has the reference number "87000823" in the the National Register of Historic Places .
existential Asher_and_Mary_Isabelle_Richardson_House ReferenceNumber_in_the_National_Register_of_Historic_Places "88002539" Asher_and_Mary_Isabelle_Richardson_House is located in Asherton_,_Texas and was built in 1911 . It holds the Reference Number "88002539" in the National Register of Historic Places .
existential Asser_Levy_Public_Baths added_to_the_National_Register_of_Historic_Places "1980-04-23" Asser_Levy_Public_Baths located in New_York_City were constructed in 1904 and added to the National Register of Historic Places on "1980-04-23" .
passive_voice Asilomar_Conference_Grounds ReferenceNumber_in_the_National_Register_of_Historic_Places "87000823" The Asilomar_Conference_Grounds constructed in 1913 and located on "Asilomar_Blvd_,_Pacific_Grove_,_California" , has the reference number "87000823" in the the National Register of Historic Places .
coordinated_clauses Asser_Levy_Public_Baths added_to_the_National_Register_of_Historic_Places "1980-04-23" Asser_Levy_Public_Baths were built in 1904 at Avenue_A_(Manhattan) . They were added to the National Register of Historic Places on "1980-04-23" .
coordinated_clauses Asser_Levy_Public_Baths added_to_the_National_Register_of_Historic_Places "1980-04-23" The Asser_Levy_Public_Baths was built in 1904 and are in New_York_City . They were added to the National Register of Historic Places on "1980-04-23" .
relative_subject Asilomar_Conference_Grounds architecture "Arts_and_Crafts_Movement_and_American_craftsman_Bungalows" Asilomar_Conference_Grounds in Pacific_Grove_,_California was constructed in 1913 and is based on the architecture style of "Arts_and_Crafts_Movement_and_American_craftsman_Bungalows" .
relative_subject Asser_Levy_Public_Baths added_to_the_National_Register_of_Historic_Places "1980-04-23" Asser_Levy_Public_Baths were built in 1904 at Avenue_A_(Manhattan) . They were added to the National Register of Historic Places on "1980-04-23" .
relative_subject Asser_Levy_Public_Baths added_to_the_National_Register_of_Historic_Places "1980-04-23" Asser_Levy_Public_Baths constructed in 1904 is located in Manhattan . It was added to the National Register of Historic Places "1980-04-23" .
coordinated_clauses Asilomar_Conference_Grounds yearOfConstruction 1913 The Asilomar_Conference_Grounds is located in "Asilomar_Blvd_,_Pacific_Grove_,_California" . It was constructed in 1913 and added to the National Register of Historic Places "1987-02-27" .
coordinated_clauses Asser_Levy_Public_Baths ReferenceNumber_in_the_National_Register_of_Historic_Places "80002709" Asser_Levy_Public_Baths was added to the National Register of Historic Places "1980-04-23" under reference number "80002709" and is located on Avenue_A_(Manhattan) .
coordinated_clauses Asser_Levy_Public_Baths ReferenceNumber_in_the_National_Register_of_Historic_Places "80002709" Asser_Levy_Public_Baths is in Avenue_A_(Manhattan) . It was added to the National Register of Historic Places in "1980-04-23" with a Reference Number of "80002709" .
coordinated_clauses Asser_Levy_Public_Baths yearOfConstruction 1904 Asser_Levy_Public_Baths is located in Manhattan , built in 1904 and was added to the National Register of Historic Places on "1980-04-23" .
existential Asher_and_Mary_Isabelle_Richardson_House yearOfConstruction 1911 The Asher_and_Mary_Isabelle_Richardson_House on U.S._Route_83 was constructed in 1911 and added to the National Register of Historic Places "1988-11-22" .
existential Asser_Levy_Public_Baths yearOfConstruction 1904 The Asser_Levy_Public_Baths in New_York_City were built in 1904 and added to the National Register of Historic Places on "1980-04-23" .
apposition Asser_Levy_Public_Baths yearOfConstruction 1904 The Asser_Levy_Public_Baths , constructed in 1904 in "Asser_Levy_Place_and_East_23rd_Street" , were added to the National Register of historic Places on "1980-04-23" .
apposition Asser_Levy_Public_Baths yearOfConstruction 1904 Asser_Levy_Public_Baths built in 1904 is located on 23rd_Street_(Manhattan) . Asser_Levy_Public_Baths was added to the National Register of Historic Places "1980-04-23" .
apposition Asser_Levy_Public_Baths yearOfConstruction 1904 Asser_Levy_Public_Baths were built in 1904 at Avenue_A_(Manhattan) . They were added to the National Register of Historic Places on "1980-04-23" .
passive_voice United_States ethnicGroup African_Americans Austin_,_Texas is located in the United_States , the country where Americans live and where African_Americans are an ethnic group .
relative_object Mexico leaderName Silvano_Aureoles_Conejo The name of the leader of Mexico is Silvano_Aureoles_Conejo . In Mexico , Mexicans eat Bionico .
relative_subject United_States ethnicGroup Asian_Americans Albuquerque_,_New_Mexico , United_States . Americans live in the United_States and Asian_Americans are an ethnic group there .
relative_subject Mexico leaderName Silvano_Aureoles_Conejo The name of the leader of Mexico is Silvano_Aureoles_Conejo . In Mexico , Mexicans eat Bionico .
relative_subject Marriott_International location Bethesda_,_Maryland Based in Bethesda_,_Maryland , the Marriott_International was founded in Washington and is the tenant of the AC_Hotel_Bella_Sky_Copenhagen .
coordinated_clauses Alan_B._Miller_Hall buildingStartDate "30_March_2007" Alan_B._Miller_Hall ' s building opened in "30_March_2007" . The Mason_School_of_Business in the United_States are the current tenants of Alan_B._Miller_Hall .
passive_voice Alan_B._Miller_Hall architect Robert_A._M._Stern The architect of Alan_B._Miller_Hall is Robert_A._M._Stern . The current tenants are the Mason_School_of_Business located in the United_States .
relative_adverb Texas demonym Tejano Andrews_County_Airport is located in Texas ( which speaks English_language ) but uses Tejano as their demonym .
passive_voice Greece leader Alexis_Tsipras The AE_Dimitra_Efxeinoupolis club is located in Greece where they speak Greek_language and the leader is Alexis_Tsipras .
apposition Texas country United_States Andrews_County_Airport is located in English_language speaking Texas , in the United_States .
apposition Greece leader Prokopis_Pavlopoulos The AE_Dimitra_Efxeinoupolis club is located in Greece . National leaders are Alexis_Tsipras and Prokopis_Pavlopoulos and the official language is Greek_language .
existential Andra_(singer) genre Rhythm_and_blues Rhythm_and_blues Andra_(singer) is associated with musicians Marius_Moga and Andreea_Bălan .
apposition Aaron_Turner genre Black_metal Black_metal musician Aaron_Turner is associated with Greymachine and Old_Man_Gloom .
apposition Abradab origin Katowice Katowice born Abradab has been with the band Kaliber_44 and associated with Magik_(rapper) .
apposition Alison_O'Donnell genre Folk_rock Folk_rock musician Alison_O'Donnell was in the Mellow_Candle and Flibbertigibbet bands .
passive_voice Suburban_Legends genre Ska Aaron_Bertram played for the Suburban_Legends and the Kids_Imagine_Nation bank . Suburban_Legends genre is Ska .
passive_voice Aaron_Bertram genre Ska_punk Aaron_Bertram plays for the Kids_Imagine_Nation band and the Suburban_Legends band . His genre is Ska_punk music .
passive_voice Al_Anderson_(NRBQ_band) genre Rock_music Al_Anderson_(NRBQ_band) plays with the band NRBQ Rock_music and was a member of The_Wildweeds .
passive_voice Alison_O'Donnell genre Folk_music_of_Ireland Folk_music_of_Ireland is the genre of Alison_O'Donnell , who performed for the band United_Bible_Studies and was a member of the Flibbertigibbet band .
existential Andra_(singer) genre Dance-pop Andra_(singer) , a singer of Dance-pop music , is associated with the band CRBL and Puya_(singer) .
existential Andrew_White_(musician) recordLabel B-Unique_Records Andrew_White_(musician) signed to the record label B-Unique_Records , plays for the band Kaiser_Chiefs and is associated with the musical artist Marry_Banilow .
passive_voice Aaron_Turner genre Post-metal Aaron_Turner plays Post-metal in the Twilight_(band) and associated with the Greymachine group .
coordinated_clauses Suburban_Legends genre Ska Aaron_Bertram is a member of Kids_Imagine_Nation and Ska band , Suburban_Legends .
existential Aaron_Turner genre Drone_music Aaron_Turner plays Drone_music and played with the bands Twilight_(band) Greymachine .
coordinated_clauses Abradab activeYearsStartYear 1994 Abradab started performing in 1994 and is an artist for the band Kaliber_44 . He is musically associated with Magik_(rapper) .
coordinated_clauses Al_Anderson_(NRBQ_band) birthPlace Windsor_,_Connecticut Born in Windsor_,_Connecticut , Al_Anderson_(NRBQ_band) plays with the band NRBQ and was once a member of The_Wildweeds .
coordinated_clauses Andra_(singer) genre Pop_music The genre of Andra_(singer) is Pop_music and is associated with Marius_Moga and Andreea_Bălan .
passive_voice Aaron_Turner genre Ambient_music Aaron_Turner performs Ambient_music and played with Twilight_(band) Greymachine .
coordinated_clauses Aleksandra_Kovač genre Pop_music Aleksandra_Kovač , a performer of Pop_music , is associated with the musical artists Bebi_Dol and Kornelije_Kovač .
coordinated_clauses Andra_(singer) genre Rhythm_and_blues Rhythm_and_blues singer , Andra_(singer) , is associated with Puya_(singer) and the band CRBL .
coordinated_full_clauses Aaron_Turner genre Drone_music Aaron_Turner ; played with Twilight_(band) , is part of the Drone_music genre and is associated with the group Greymachine .
coordinated_full_clauses Aaron_Turner genre Ambient_music Aaron_Turner played with the bands Twilight_(band) Mamiffer . His genre is Ambient_music .
coordinated_full_clauses Aleksandra_Kovač genre Rhythm_and_blues Aleksandra_Kovač is an artist for the band K2_(Kovač_sisters_duo) and is associated with the musical artist Bebi_Dol . The musical genre of Aleksandra_Kovač is Rhythm_and_blues .
coordinated_full_clauses Alison_O'Donnell genre Folk_rock Folk_rock musician Alison_O'Donnell was in the Mellow_Candle and Flibbertigibbet bands .
relative_subject Aaron_Turner genre Black_metal Aaron_Turner played with the bands Twilight_(band) Sumac_(band) . He is a Black_metal musician .
relative_subject NRBQ genre Country_music Al_Anderson_(NRBQ_band) is part of NRBQ and once was part of The_Wildweeds . NRBQ is a Country_music band .
relative_subject Alison_O'Donnell genre Psychedelic_folk Alison_O'Donnell was in the bands Mellow_Candle and Flibbertigibbet . Her genre is Psychedelic_folk .
relative_subject Andra_(singer) genre Pop_music The genre of Andra_(singer) is Pop_music and is associated with Marius_Moga and Andreea_Bălan .
coordinated_full_clauses Aaron_Turner genre Post-metal Aaron_Turner plays Post-metal in the Twilight_(band) and associated with the Greymachine group .
coordinated_full_clauses Alison_O'Donnell genre Folk_music Alison_O'Donnell plays Folk_music for the Mellow_Candle band . He also played for the Flibbertigibbet band .
coordinated_clauses Alex_Day activeYearsStartYear 2006 Alex_Day plays with the band Chameleon_Circuit_(band) associated with the musical artist , Charlie_McDonnell . His active years in music began in 2006 .
apposition Aaron_Turner genre Post-metal Aaron_Turner is an artist for Isis_(band) and is associated with the group Greymachine . He is a performer of the musical genre Post-metal .
apposition Aleksandra_Kovač genre Rhythm_and_blues An exponent of Rhythm_and_blues , Aleksandra_Kovač is associated with Kornelije_Kovač and is an artist for the band K2_(Kovač_sisters_duo) .
apposition Andrew_White_(musician) recordLabel B-Unique_Records Andrew_White_(musician) signed to the record label B-Unique_Records , plays for the band Kaiser_Chiefs and is associated with the musical artist Marry_Banilow .
passive_voice Aaron_Turner genre Post-metal Aaron_Turner ' s music is Post-metal and he is an artist for Isis_(band) . he also played for Old_Man_Gloom .
coordinated_full_clauses Aleksandra_Kovač genre Rhythm_and_blues Aleksandra_Kovač , who is associated with Bebi_Dol Doi and Kristina_Kovač , is an exponent of Rhythm_and_blues .
relative_subject Alex_Day genre Electronic_music Alex_Day ; performs Electronic_music , plays with the band Chameleon_Circuit_(band) is associated with the musical artist , Charlie_McDonnell .
relative_subject Andrew_Rayel genre Trance_music Andrew_Rayel , of the music genre Trance_music , is associated with the musical artists Jonathan_Mendelsohn and Bobina .
coordinated_full_clauses Andrew_White_(musician) recordLabel B-Unique_Records Andrew_White_(musician) signed to the record label B-Unique_Records , plays for the band Kaiser_Chiefs and is associated with the musical artist Marry_Banilow .
coordinated_full_clauses Abradab origin Poland Abradab was born in Katowice , Poland . He ' s played with the band Kaliber_44 and is associated with Magik_(rapper) .
relative_subject Christian_Burns genre House_music Musicians Andrew_Rayel and Christian_Burns are associates and while the latter plays House_music , the former is associated with musician Jwaydan_Moyine .
coordinated_clauses Ska_punk stylisticOrigin Punk_rock Playing for the Suburban_Legends band , Aaron_Bertram performs Ska_punk music which stylistically originates from Punk_rock .
coordinated_full_clauses Black_metal musicFusionGenre Death_metal Aaron_Turner performs Black_metal music and played with Twilight_(band) . Death_metal is a musical fusion of Black_metal .
direct_object Black_metal musicFusionGenre Death_metal Aaron_Turner is a Black_metal musician who played with Twilight_(band) . Death_metal is a musical fusion of Black_metal .
relative_subject Al_Anderson_(NRBQ_band) instrument Guitar Al_Anderson_(NRBQ_band) plays the Guitar for the band NRBQ . Their genre is Rock_music .
direct_object Ska_punk stylisticOrigin Ska Ska_punk ( Origin : Ska ) is the genre of Aaron_Bertram , who is a member of Kids_Imagine_Nation band .
direct_object Ska_punk stylisticOrigin Ska Ska_punk musician Aaron_Bertram is in the band Suburban_Legends . Ska_punk derives from Ska .
coordinated_full_clauses Black_metal musicFusionGenre Death_metal Aaron_Turner is a Black_metal musician who played with Twilight_(band) . Death_metal is a musical fusion of Black_metal .
coordinated_full_clauses Ska_punk stylisticOrigin Ska Ska_punk musician Aaron_Bertram is in the band Suburban_Legends . Ska_punk derives from Ska .
passive_voice Ska_punk stylisticOrigin Punk_rock Aaron_Bertram , whose genre is Ska_punk , plays for the Kids_Imagine_Nation band . Ska_punk music origins are Punk_rock .
passive_voice Black_metal musicFusionGenre Death_metal Aaron_Turner is a Black_metal musician who performed for House_of_Low_Culture . Death_metal is a musical fusion of Black_metal .
passive_voice Rock_music musicFusionGenre Bhangra_(music) Al_Anderson_(NRBQ_band) plays with the band NRBQ , in the Rock_music genre , a musical fusion of which is Bhangra_(music) .
apposition Jazz musicFusionGenre Afrobeat Albennie_Jones is a Jazz musician . Jazz comes from Blues music and the Afrobeat is a fusion genre of that music .
apposition Hip_hop_music derivative Drum_and_bass The musical genre of Abradab is Hip_hop_music , from which Drum_and_bass derives its sounds and the stylistic origin of which is Disco .
apposition Aaron_Deer instrument Bass_guitar Aaron_Deer plays the Bass_guitar , performing songs in the Indie_rock genre , which has its stylistic origins in Garage_rock .
apposition Rhythm_and_blues derivative Funk Allen_Forrest performs Rhythm_and_blues music , which originated from the Blues . Funk is a derivative of Rhythm_and_blues .
apposition Aaron_Bertram associatedBand/associatedMusicalArtist Suburban_Legends Aaron_Bertram plays for the Suburban_Legends band and is an artist with the band Kids_Imagine_Nation . He performs Ska_punk music which originated from Ska music .
existential Hip_hop_music derivative Drum_and_bass Abradab plays Hip_hop_music which has its origins in Funk music . Drum_and_bass gets its sounds from Hip_hop_music .
direct_object Aaron_Bertram associatedBand/associatedMusicalArtist Suburban_Legends Aaron_Bertram plays for both the Suburban_Legends band and Kids_Imagine_Nation band . Aaron_Bertram ' s musical genre is Ska_punk which originated from Ska music .
existential Andrew_Rayel associatedBand/associatedMusicalArtist Jonathan_Mendelsohn Andrew_Rayel plays Trance_music which has its origins in Pop_music . He is associated with Bobina and Jonathan_Mendelsohn .
relative_subject Rhythm_and_blues derivative Disco Albennie_Jones is a Rhythm_and_blues ( originated from Blues music and has Disco as a derivative ) artist .
relative_subject Rhythm_and_blues derivative Funk Andra_(singer) is a Rhythm_and_blues singer . Originating from Blues music , Rhythm_and_blues has Funk and Disco as derivatives .
possessif Aaron_Bertram associatedBand/associatedMusicalArtist Suburban_Legends Playing for the Suburban_Legends band , Aaron_Bertram performs Ska_punk music which stylistically originates from Punk_rock .
possessif Rhythm_and_blues derivative Disco Allen_Forrest plays Rhythm_and_blues which originated from the Blues and in turn gave rise to Disco .
possessif Aaron_Bertram associatedBand/associatedMusicalArtist Suburban_Legends Aaron_Bertram plays for the Suburban_Legends band and the Kids_Imagine_Nation . He performs Ska_punk , which is originated from Punk_rock music .
apposition Rhythm_and_blues derivative Disco Allen_Forrest ' s genre of music is Rhythm_and_blues which originated from Blues music . Disco is a derivative of Rhythm_and_blues .
apposition Hip_hop_music derivative Drum_and_bass Abradab performs Hip_hop_music , which has its stylistic origins in Jazz . Drum_and_bass is a derivative of Hip_hop_music .
relative_object Aaron_Bertram associatedBand/associatedMusicalArtist Suburban_Legends Playing for the Suburban_Legends band , Aaron_Bertram performs Ska_punk music which stylistically originates from Punk_rock .
relative_object Aaron_Deer instrument Bass_guitar Aaron_Deer plays the Bass_guitar and his musical genre is Indie_rock which originated from Garage_rock music .
relative_object Hip_hop_music derivative Drum_and_bass The musical genre of Abradab is Hip_hop_music which has its stylistic origins in Jazz . Drum_and_bass is a derivative of Hip_hop_music .
relative_object Hip_hop_music musicSubgenre musicSubHip_hop_music The musical genre of Ace_Wilder is Hip_hop_music , which has its origins in Funk . Hip_hop_music has a sub genre called musicSubHip_hop_music .
relative_object Synthpop derivative House_music Synthpop music is the genre of Alex_Day . Synthpop music spawned from House_music and has its origins in Disco .
relative_object Rhythm_and_blues derivative Funk Allen_Forrest ' s genre of music is Rhythm_and_blues which originated from the Blues . Both Disco and Funk are derivatives of Rhythm_and_blues .
apposition Aaron_Bertram associatedBand/associatedMusicalArtist Kids_Imagine_Nation Aaron_Bertram plays for the Kids_Imagine_Nation band and performs Ska_punk music which has its origins in Ska .
coordinated_full_clauses Aaron_Deer instrument Piano Piano player Aaron_Deer , is a performer of the music genre Indie_rock . The stylistic origin of Indie_rock is New_wave_music .
coordinated_full_clauses Synthpop derivative House_music Alex_Day performs Synthpop music which is a form of Pop_music derived from House_music .
coordinated_full_clauses Rhythm_and_blues derivative Funk Andra_(singer) ' s genre is Rhythm and blue which originated from the Blues . Both Funk and Disco are derivatives of Rhythm_and_blues .
passive_voice Hip_hop_music musicSubgenre musicSubHip_hop_music Ace_Wilder is an exponent of Hip_hop_music which has a sub genre called musicSubHip_hop_music and the stylistic origin is Disco .
passive_voice Rhythm_and_blues derivative Disco Allen_Forrest performs Rhythm_and_blues music ( which originated from the Blues ) and has Disco as a derivative .
direct_object Indie_rock instrument Guitar Indie_rock , with its origin of Garage_rock and inclusion of Guitar , is the genre of musician Aaron_Deer .
apposition Andrew_Rayel associatedBand/associatedMusicalArtist Jwaydan_Moyine Trance_music performer Andrew_Rayel is associated with musical artists Jwaydan_Moyine and Bobina . Pop_music helped give rise to Trance .
juxtaposition Hip_hop_music derivative Drum_and_bass Hip_hop_music has its origins in Disco , Drum_and_bass is one of its derivative and Abradab is a famous performer of the style .
juxtaposition Synthpop derivative House_music Alex_Day performs Synthpop music which is a form of Pop_music derived from House_music .
coordinated_clauses Agustín_Barboza recordLabel Philips_Records Agustín_Barboza has a background in "solo_singer" singing and plays Guarania_(music) style under the signature of Philips_Records .
existential Aaron_Bertram associatedBand/associatedMusicalArtist Suburban_Legends Aaron_Bertram performs Ska_punk music for the Suburban_Legends band and the band Kids_Imagine_Nation . He has a background with a "non_vocal_instrumentalist" instrument .
direct_object Albennie_Jones birthPlace United_States Albennie_Jones was born in the United_States . She is a "solo_singer" and Jazz artist .
apposition Allen_Forrest birthPlace Dothan_,_Alabama Born in Dothan_,_Alabama , Allen_Forrest has a background as a "solo_singer" and was a Pop_music artist .
apposition Allen_Forrest birthPlace Fort_Campbell Allen_Forrest was born in Fort_Campbell , has a background as a "solo_singer" and performs Rhythm_and_blues .
direct_object Albennie_Jones birthPlace United_States Albennie_Jones who was born in the United_States is a "solo_singer" and a Jazz musician .
relative_subject Alfredo_Zitarrosa recordLabel RCA_Records Alfredo_Zitarrosa music genre is Milonga_(music) . He has a recording label in RCA_Records and his background includes "solo_singer" singing .
relative_subject Allen_Forrest birthPlace Dothan_,_Alabama Born in Dothan_,_Alabama , Allen_Forrest was originally a "solo_singer" and performs Hip_hop_music .
relative_subject Aaron_Deer origin Indianapolis Aaron_Deer is from Indianapolis . in Indiana . He is a performer of the music genre , Indie_rock and has a background as a "solo_singer" .
relative_subject Albennie_Jones birthPlace Errata_,_Mississippi Errata_,_Mississippi born Albennie_Jones is a Rhythm_and_blues "solo_singer" .
relative_subject Rhythm_and_blues derivative Funk The "solo_singer" Andra_(singer) ' s genre is Rhythm_and_blues , of which Funk is a derivative .
possessif Allen_Forrest birthPlace Dothan_,_Alabama Allen_Forrest ( Dothan_,_Alabama ) is a "solo_singer" and an exponent of Pop_music .
possessif Allen_Forrest birthPlace Fort_Campbell Allen_Forrest ( born in Fort_Campbell ) , is a "solo_singer" under the Acoustic_music genre .
direct_object Allen_Forrest birthPlace Dothan_,_Alabama Rhythm_and_blues musician and "solo_singer" Allen_Forrest , was born in Dothan_,_Alabama .
apposition Alfredo_Zitarrosa recordLabel Orfeo_(Uruguayan_record_label) Alfredo_Zitarrosa started out as a "solo_singer" and belongs to the Orfeo_(Uruguayan_record_label) record label . The musical genre of Alfredo_Zitarrosa is Candombe .
existential Agustín_Barboza birthPlace Paraguay Agustín_Barboza , a Guarania_(music) "solo_singer" , was born in Asunción , Paraguay .
coordinated_full_clauses Ahmet_Ertegun origin Washington Ahmet_Ertegun from Washington , United_States . plays Rhythm_and_blues music . His background is as a "non_performing_personnel" person .
relative_object Ahmet_Ertegun origin Washington Ahmet_Ertegun originates from Washington , United_States . His background is "non_performing_personnel" in the Blues genre .
possessif Aaron_Deer origin Indianapolis Aaron_Deer is from Indianapolis . in Indiana . He is a performer of the music genre , Indie_rock and has a background as a "solo_singer" .
coordinated_full_clauses Alfredo_Zitarrosa recordLabel RCA_Records An exponent of Milonga_(music) , Alfredo_Zitarrosa has a background as a "solo_singer" and is part of the record label , RCA_Records .
possessif Ahmet_Ertegun origin United_States From the United_States , Ahmet_Ertegun performs Rock_and_roll music and started out as a "non_performing_personnel" .
coordinated_clauses Alfredo_Zitarrosa recordLabel RCA_Records Alfredo_Zitarrosa ' s record label is RCA_Records , his background includes "solo_singer" singing and his musical genre is Milonga_(music) .
possessif Aaron_Deer origin Indianapolis Coming from Indianapolis , Indiana , Aaron_Deer has a background as a "solo_singer" and performs Psychedelia music .
relative_subject Indie_rock stylisticOrigin New_wave_music Aaron_Deer plays the Piano and his musical genre is Indie_rock . The stylistic origin of Indie_rock is New_wave_music .
coordinated_full_clauses Indie_rock stylisticOrigin New_wave_music Aaron_Deer plays the Piano and his musical genre is Indie_rock . The stylistic origin of Indie_rock is New_wave_music .
coordinated_clauses Jazz derivative Funk Alison_O'Donnell plays Jazz music with the instrument , the Autoharp . Funk music comes from Jazz music .
coordinated_full_clauses Indie_rock stylisticOrigin New_wave_music Aaron_Deer plays the Bass_guitar and is a performer of the music genre Indie_rock , which has its origins in New_wave_music .
coordinated_full_clauses Indie_rock stylisticOrigin New_wave_music Aaron_Deer plays the Keyboard_instrument in the music genre Indie_rock . The genre has its origins in New_wave_music .
apposition Indie_rock stylisticOrigin New_wave_music Indie_rock musician Aaron_Deer plays the Keyboard_instrument . Indie_rock derives from New_wave_music .
relative_subject Indie_rock stylisticOrigin New_wave_music Musician , Aaron_Deer , plays the Guitar and is a performer of the music genre Indie_rock . Indie_rock has its stylistic origins in both Garage_rock and New_wave_music .
relative_subject Indie_rock stylisticOrigin New_wave_music Aaron_Deer is an Indie_rock musician , although it has its origins in the New_wave_music , originated from Garage_rock using Guitar .
relative_object Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Aaron_Turner is a performer of the musical genre Post-metal , which uses Cello as a musical instrument . He played with Twilight_(band) House_of_Low_Culture bands .
relative_object Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Aaron_Turner is a performer of the musical genre Post-metal which has the Cello as an instrument . He played for Old_Man_Gloom and for Twilight_(band) .
apposition Alan_Frew activeYearsStartYear 1983 Alan_Frew is from Newmarket_,_Ontario , Canada and started performing in 1983 .
apposition United_States ethnicGroup African_Americans Ahmet_Ertegun is originally from Washington , in the United_States , where African_Americans are an ethnic group .
passive_voice United_States ethnicGroup African_Americans Ahmet_Ertegun is from Washington , United_States . African_Americans are one of the ethnic groups in the United_States .
relative_subject United_States ethnicGroup African_Americans Ahmet_Ertegun is from Washington , United_States . African_Americans are one of the ethnic groups in the United_States .
relative_object Anders_Osborne recordLabel Rabadash_Records Anders_Osborne is signed to the record label Rabadash_Records and his genre is Rock_music . Bhangra_(music) is part of the genre Rock_music fusion .
passive_voice Aaron_Turner associatedBand/associatedMusicalArtist Isis_(band) Aaron_Turner is an artist for Isis_(band) and Black_metal is his music genre . Death_metal is a musical fusion of Black_metal .
passive_voice Aaron_Turner instrument Electric_guitar Death_metal is a musical fusion of Black_metal , the music played by Electric_guitar , Aaron_Turner .
passive_voice Jazz stylisticOrigin Blues Albennie_Jones is a Jazz musician . Jazz comes from Blues music and the Afrobeat is a fusion genre of that music .
coordinated_full_clauses Aaron_Turner associatedBand/associatedMusicalArtist House_of_Low_Culture Aaron_Turner performed for House_of_Low_Culture and his music genre is Black_metal . Death_metal is a musical fusion of Black_metal .
direct_object Aaron_Turner associatedBand/associatedMusicalArtist Isis_(band) Aaron_Turner is an artist for Isis_(band) and Black_metal is his music genre . Death_metal is a musical fusion of Black_metal .
direct_object Rock_music stylisticOrigin Folk_music Alan_Frew is a performer of Rock_music which stylistically , originated from Folk_music . A musical fusion of Rock_music is Bhangra_(music) .
direct_object Anders_Osborne recordLabel Alligator_Records Anders_Osborne is an exponent of Rock_music and his record label is Alligator_Records . A musical fusion of Rock_music is Bhangra_(music) .
direct_object Rock_music stylisticOrigin Country_music Rock_music originated from Country_music and Bhangra_(music) is one of its musical fusion . Alan_Frew performs in the Rock_music which has its stylistic origins in Blues .
direct_object Aleksandra_Kovač associatedBand/associatedMusicalArtist Bebi_Dol Disco music is a fusion genre of Pop_music which is the musical genre of Aleksandra_Kovač who is associated with the musical artist Bebi_Dol .
coordinated_full_clauses Rock_music stylisticOrigin Folk_music Alan_Frew is a performer of Rock_music , which originated from Folk_music . Bhangra_(music) is part of the fusion genre , partly coming from Rock_music , along with the Blues .
possessif Rock_music stylisticOrigin Country_music Alan_Frew is a performer of Rock_music which originated from Country_music . A musical fusion of Rock_music is Bhangra_(music) .
possessif Rock_music stylisticOrigin Folk_music Alan_Frew is a performer of Rock_music , which originated from Folk_music . Bhangra_(music) is part of the fusion genre , partly coming from Rock_music , along with the Blues .
possessif Alternative_rock stylisticOrigin Punk_rock Nu_metal is a fusion genre of Alternative_rock which originated from Punk_rock . Andrew_White_(musician) plays Alternative_rock which has origins in New_wave_music .
apposition Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Aaron_Turner performs Black_metal music and played with Twilight_(band) . Death_metal is a musical fusion of Black_metal .
apposition Aleksandra_Kovač background "solo_singer" Aleksandra_Kovač is a "solo_singer" in Pop_music which helped give rise to Disco .
possessif Aaron_Turner associatedBand/associatedMusicalArtist Greymachine Black_metal music Aaron_Turner was a member of The Greymachine . Death_metal is a Black_metal fusion .
passive_voice Aleksandra_Kovač background "solo_singer" Aleksandra_Kovač is a "solo_singer" in Pop_music which helped give rise to Disco .
apposition Aaron_Turner associatedBand/associatedMusicalArtist Isis_(band) Aaron_Turner is an artist for Isis_(band) and Black_metal is his music genre . Death_metal is a musical fusion of Black_metal .
apposition Rock_music stylisticOrigin Country_music Rock_music originated from Country_music and Bhangra_(music) is one of its musical fusion . Alan_Frew performs in the Rock_music which has its stylistic origins in Blues .
relative_subject Aaron_Turner associatedBand/associatedMusicalArtist Sumac_(band) Aaron_Turner performed with Sumac_(band) and is a Black_metal musician . Death_metal is a musical fusion of Black_metal .
relative_subject Al_Anderson_(NRBQ_band) associatedBand/associatedMusicalArtist NRBQ Al_Anderson_(NRBQ_band) plays with the band NRBQ , in the Rock_music genre , a musical fusion of which is Bhangra_(music) .
relative_adverb Aaron_Turner associatedBand/associatedMusicalArtist Sumac_(band) Aaron_Turner performed with Sumac_(band) and is a Black_metal musician . Death_metal is a musical fusion of Black_metal .
relative_adverb Rock_music stylisticOrigin Folk_music Alan_Frew is a performer of Rock_music which stylistically , originated from Folk_music . A musical fusion of Rock_music is Bhangra_(music) .
relative_adverb Rock_music stylisticOrigin Country_music Rock_music originated from Country_music and Bhangra_(music) is one of its musical fusion . Alan_Frew performs in the Rock_music which has its stylistic origins in Blues .
relative_adverb Aleksandra_Kovač associatedBand/associatedMusicalArtist Bebi_Dol Aleksandra_Kovač performs in the genre of Pop_music and is associated with the musical artist Bebi_Dol . Disco music is a fusion genre that contain elements of other genres such as Pop_music .
direct_object Alan_Shepard occupation Test_pilot Test_pilot Alan_Shepard was born in New_Hampshire "1923-11-18" .
apposition Akeem_Dent club Houston_Texans Akeem_Dent was born on the 1987-09-27 in "Atlanta_,_Georgia" and now plays with the Houston_Texans club .
apposition Abel_Caballero party Spanish_Socialist_Workers'_Party Abel_Caballero was born in Galicia_(Spain) in Spain and is a member of the Spanish_Socialist_Workers'_Party .
passive_voice Abel_Caballero party Spanish_Socialist_Workers'_Party Abel_Caballero , born in Ponteareas ( Spain ) , is a member of the Spanish_Socialist_Workers'_Party .
apposition Adonis_Georgiadis successor Makis_Voridis Adonis_Georgiadis was born in Athens in Greece and was succeeded by Makis_Voridis .
apposition Adonis_Georgiadis office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Adonis_Georgiadis was born in Athens , Greece and works as office workedAt workedAsObjet1 .
apposition Alfons_Gorbach deathPlace Styria Alfons_Gorbach was born in Austria-Hungary , in Imst and died in Styria , in Austria .
existential Adonis_Georgiadis office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Adonis_Georgiadis , born in Athens , Greece , worked as the office workedAt workedAsObjet1 .
passive_voice Abel_Caballero party Spanish_Socialist_Workers'_Party Abel_Caballero , born in Galicia_(Spain) in Spain , is a member of the Spanish_Socialist_Workers'_Party .
existential Abel_Caballero office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Abel_Caballero , office workedAt workedAsObjet1 , was born in Galicia_(Spain) , Spain .
existential Adonis_Georgiadis successor Makis_Voridis Adonis_Georgiadis was born in Athens in Greece and was succeeded by Makis_Voridis .
existential Alfons_Gorbach deathPlace Graz Alfons_Gorbach was born in the County_of_Tyrol in Austria and later died in Graz .
passive_voice Alfons_Gorbach deathPlace Graz Imst , in the County of Tyrol_(state) , was the birthplace of Alfons_Gorbach , who died in Graz .
relative_subject Abel_Caballero party Spanish_Socialist_Workers'_Party Abel_Caballero was born in Galicia_(Spain) , Spain and is a member of the Spanish_Socialist_Workers'_Party .
relative_subject Adonis_Georgiadis party New_Democracy_(Greece) Adonis_Georgiadis was born in Athens , Greece and was a member of the New_Democracy_(Greece) Party there .
passive_voice Albert_Jennings_Fountain deathPlace Doña_Ana_County_,_New_Mexico Staten_Island ( New_York ) is the birth place of Albert_Jennings_Fountain , who died in Doña_Ana_County_,_New_Mexico .
possessif Abel_Caballero office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Abel_Caballero , office workedAt workedAsObjet1 , was born in Galicia_(Spain) , Spain .
possessif Albert_Jennings_Fountain deathPlace United_States Albert_Jennings_Fountain was born in Staten_Island , New_York_City . He died in New_Mexico_Territory , United_States .
possessif Alfons_Gorbach deathPlace Styria Alfons_Gorbach was born in Imst , Austria-Hungary and died in Styria , Austria .
relative_subject Canada leaderName Elizabeth_II Adam_McQuaid was born in Canada , where the leader is Elizabeth_II and Black_Canadians are an ethnic group .
relative_object Albennie_Jones background "solo_singer" "solo_singer" Albennie_Jones is a Rhythm_and_blues performer which helped create Disco .
relative_object Synthpop stylisticOrigin Disco Alex_Day uses the Synthpop genre . Synthpop is a musical derivative of House_music and originated from Disco .
relative_object Rhythm_and_blues stylisticOrigin Blues Allen_Forrest performs Rhythm_and_blues music , which originated from the Blues . Funk is a derivative of Rhythm_and_blues .
relative_object Synthpop stylisticOrigin New_wave_music Synthpop has its origins in Disco , New_wave_music and House_music . It is also the style of artist Alex_Day .
direct_object Rhythm_and_blues stylisticOrigin Blues Disco derives from Rhythm_and_blues which originated from Blues music . Ahmet_Ertegun ' s genre is Rhythm_and_blues .
possessif Albennie_Jones background "solo_singer" "solo_singer" Albennie_Jones is a Rhythm_and_blues performer which helped create Disco .
possessif Rhythm_and_blues stylisticOrigin Blues Allen_Forrest performs Rhythm_and_blues music , which originated from the Blues . Funk is a derivative of Rhythm_and_blues .
relative_subject Synthpop stylisticOrigin New_wave_music Synthpop has its origins in Disco , New_wave_music and House_music . It is also the style of artist Alex_Day .
relative_subject Synthpop stylisticOrigin Pop_music Alex_Day performs Synthpop music which is a form of Pop_music derived from House_music .
direct_object Allen_Forrest background "solo_singer" Disco derives from Rhythm_and_blues , the musical genre of Allen_Forrest , who , was originally a "solo_singer" .
relative_subject Synthpop stylisticOrigin Pop_music Synthpop has origins in Disco and House_music and is a form of Pop_music . It is the style of artist Alex_Day .
relative_object Synthpop stylisticOrigin Pop_music Synthpop is influenced by Pop_music , New_wave_music and House_music , it is also the genre of Alex_Day .
relative_object Hip_hop_music stylisticOrigin Disco Hip_hop_music has its origins in Disco , Drum_and_bass is one of its derivative and Abradab is a famous performer of the style .
passive_voice Hip_hop_music stylisticOrigin Jazz Drum_and_bass is a derivative of Hip_hop_music ( as performed by Abradab ) which has its stylistic origins in Jazz and Disco .
relative_subject Synthpop stylisticOrigin Pop_music The musical genre of Alex_Day is Synthpop , a form of House_music . The stylistic origin of Synthpop is both New_wave_music and Pop_music .
apposition Albennie_Jones birthPlace Errata_,_Mississippi Albennie_Jones was born in Errata_,_Mississippi and performs in the Rhythm_and_blues genre . Disco is a derivative of Rhythm_and_blues .
apposition Albennie_Jones associatedBand/associatedMusicalArtist Sammy_Price Albennie_Jones a Rhythm_and_blues ( which has Disco as a derivative ) artist has worked with fellow artist Sammy_Price .
relative_subject Ahmet_Ertegun origin Washington The musical genre of Ahmet_Ertegun , originally from Washington , is Rhythm_and_blues , from which Disco is derived .
direct_object Ahmet_Ertegun background "non_performing_personnel" Rhythm_and_blues artist Ahmet_Ertegun was a "non_performing_personnel" artist . Disco comes from R & B .
direct_object Synthpop stylisticOrigin Pop_music Synthpop is influenced by Pop_music , New_wave_music and House_music , it is also the genre of Alex_Day .
relative_object Alison_O'Donnell recordLabel Floating_World_Records Alison_O'Donnell plays Jazz music and his recording label was Floating_World_Records . Funk music is derived from Jazz .
apposition Albennie_Jones background "solo_singer" "solo_singer" Albennie_Jones is a Rhythm_and_blues performer which helped create Disco .
relative_subject Albennie_Jones activeYearsStartYear 1930 Rhythm_and_blues artist Albennie_Jones became active in 1930 and incidentally R and B helped give rise to Disco .
coordinated_clauses Albennie_Jones birthPlace Errata_,_Mississippi Albennie_Jones , born in Errata_,_Mississippi , is a performer of Rhythm_and_blues , of which Disco is a derivative .
coordinated_clauses Alison_O'Donnell recordLabel Static_Caravan_Recordings Jazz musician Alison_O'Donnell is signed with Static_Caravan_Recordings . Jazz is the origin of Funk .
coordinated_clauses Rhythm_and_blues stylisticOrigin Blues Andra_(singer) ' s genre is Rhythm_and_blues . Disco is a derivative of rhythm and Blues which originated from Blues music .
relative_subject Allen_Forrest genre Acoustic_music Allen_Forrest was born in "Fort_Campbell_,_KY_,_raised_in_Dothan_,_AL" . He plays Acoustic_music and was a "solo_singer" .
direct_object Albennie_Jones genre Jazz Born in Errata_,_Mississippi , Albennie_Jones is a Jazz musician and "solo_singer" .
passive_voice Albennie_Jones genre Rhythm_and_blues Albennie_Jones ( United_States ) is a "solo_singer" and performs in the Rhythm_and_blues genre .
passive_voice Allen_Forrest genre Acoustic_music Allen_Forrest is an Acoustic_music "solo_singer" born in Dothan_,_Alabama .
existential Allen_Forrest genre Rhythm_and_blues Rhythm_and_blues musician and "solo_singer" Allen_Forrest , was born in Dothan_,_Alabama .
apposition Albennie_Jones genre Jazz Born in Errata_,_Mississippi , Albennie_Jones is a Jazz musician and "solo_singer" .
apposition Allen_Forrest genre Acoustic_music Allen_Forrest ( born in Fort_Campbell ) , is a "solo_singer" under the Acoustic_music genre .
possessif Alfredo_Zitarrosa genre Milonga_(music) An exponent of Milonga_(music) , Alfredo_Zitarrosa has a background as a "solo_singer" and is part of the record label , RCA_Records .
relative_subject Agustín_Barboza genre Guarania_(music) Agustín_Barboza ( who started out as a "solo_singer" ) , is is a Guarania_(music) artiste under the record labe ; Philips_Records .
direct_object Alfredo_Zitarrosa genre Candombe The musical genre of Alfredo_Zitarrosa is Candombe , he started out as a "solo_singer" and is signed to Orfeo_(Uruguayan_record_label) .
existential Agustín_Barboza genre Guarania_(music) Agustín_Barboza has a background in "solo_singer" singing and plays Guarania_(music) style under the signature of Philips_Records .
apposition Alfred_Garth_Jones nationality United_Kingdom Alfred_Garth_Jones was born in England , United_Kingdom and died in Sidcup .
apposition Alan_Shepard was_selected_by_NASA 1959 Alan_Shepard , born in New_Hampshire , but died in California , was hired by NASA in 1959 .
apposition Elliot_See nationality United_States Elliot_See was born in Dallas in the United_States and later died in St_Louis .
coordinated_clauses Alfred_Garth_Jones nationality United_Kingdom Alfred_Garth_Jones was born in England , United_Kingdom and died in Sidcup .
coordinated_clauses Austria leaderName Doris_Bures Alfons_Gorbach ws born in the County_of_Tyrol and later died in Austria which is led by Doris_Bures .
coordinated_clauses A_Glastonbury_Romance author John_Cowper_Powys John_Cowper_Powys , author of A_Glastonbury_Romance , was born in Shirley_,_Derbyshire and died in Blaenau_Ffestiniog .
coordinated_full_clauses Asunción isPartOf Gran_Asunción Agustín_Barboza was born in Paraguay and died in Asunción ( part of Gran_Asunción ).
coordinated_full_clauses Albert_B._White successor William_M._O._Dawson William_M._O._Dawson was born in Bloomington_,_Maryland , died in Charleston_,_West_Virginia and was succeeded by Albert_B._White .
coordinated_full_clauses Elliot_See nationality United_States Elliot_See was born in Dallas in the United_States and later died in St_Louis .
passive_voice London leaderName Boris_Johnson Alfred_Garth_Jones , born in England , died in Boris_Johnson led , London .
relative_subject Alfred_Garth_Jones nationality United_Kingdom Alfred_Garth_Jones was born in Manchester , United_Kingdom and he passed away in London .
coordinated_full_clauses Paraguay leaderName Juan_Afara Juan_Afara is the leader of Paraguay which was the birth place of Agustín_Barboza in Asunción .
apposition London leaderName Boris_Johnson Alfred_Garth_Jones , born in England , died in Boris_Johnson led , London .
direct_object Albert_B._White successor William_M._O._Dawson William_M._O._Dawson was born in Bloomington_,_Maryland , died in Charleston_,_West_Virginia and was succeeded by Albert_B._White .
apposition Elliot_See deathDate "1966-02-28" Elliot_See died "1966-02-28" in St_Louis a part of the Kingdom_of_France .
apposition Albert_Jennings_Fountain birthPlace Staten_Island Albert_Jennings_Fountain was born in New_York_City , Staten_Island , United_States and died in Doña_Ana_County_,_New_Mexico .
existential Ahmet_Ertegun origin Washington Originating from Washington , Ahmet_Ertegun died in the United_States . the country where one of the ethnic groups is , African_Americans .
relative_adverb Ahmet_Ertegun deathPlace New_York_City Ahmet_Ertegun is from the United_States and passed away in New_York_City . One ethnic group in the United_States are African_Americans .
relative_subject Albennie_Jones background "solo_singer" Albennie_Jones was born in the United_States . She plays Blues music and is a "solo_singer" .
passive_voice Albennie_Jones background "solo_singer" Albennie_Jones ( United_States ) is a "solo_singer" and performs in the Rhythm_and_blues genre .
direct_object Anders_Osborne associatedBand/associatedMusicalArtist Tab_Benoit Anders_Osborne began his Blues musical career in 1989 and has worked with Galactic , Tab_Benoit and Billy_Iuso .
coordinated_clauses Aaron_Turner associatedBand/associatedMusicalArtist Old_Man_Gloom In 1995 began to realize the musician of the genre Post-metal Aaron_Turner in the band of Lotus_Eaters_(band) , played in the Old_Man_Gloom .
direct_object Alex_Day associatedBand/associatedMusicalArtist Charlie_McDonnell Acoustic_music Alex_Day began in 2006 and is associated with the Chameleon_Circuit_(band) Charlie_McDonnell .
apposition Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Aaron_Turner played with Twilight_(band) Old_Man_Gloom bands . He started performing Black_metal in 1995 .
apposition Alex_Day associatedBand/associatedMusicalArtist Charlie_McDonnell Acoustic_music Alex_Day plays with the bands Chameleon_Circuit_(band) the musician Charlie_McDonnell . His active years in music began in 2006 .
coordinated_full_clauses Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Aaron_Turner started performing in 1995 and performs Ambient_music . He played for Old_Man_Gloom and with Twilight_(band) .
possessif Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Aaron_Turner started performing in 1995 and performs Ambient_music . He played for Old_Man_Gloom and with Twilight_(band) .
direct_object Alison_O'Donnell associatedBand/associatedMusicalArtist Mellow_Candle Alison_O'Donnell was a member of the Flibbertigibbet band and the Mellow_Candle . Her genre is the Folk_music_of_Ireland and started performing in 1963 .
relative_subject Al_Anderson_(NRBQ_band) associatedBand/associatedMusicalArtist The_Wildweeds Rock_music , NRBQ band , member , Al_Anderson_(NRBQ_band) started his career in 1966 and was a member of The_Wildweeds .
direct_object Aaron_Turner associatedBand/associatedMusicalArtist Old_Man_Gloom Aaron_Turner started performing in 1995 and played for Old_Man_Gloom . He is a musician in Lotus_Eaters_(band) and his genre is Black_metal .
possessif Alex_Day associatedBand/associatedMusicalArtist Charlie_McDonnell Alex_Day , started performing Folk_music in 2006 and is associated with the musical artist Charlie_McDonnell . He plays with Chameleon_Circuit_(band) .
coordinated_full_clauses Alison_O'Donnell associatedBand/associatedMusicalArtist Mellow_Candle Alison_O'Donnell ' s career began in 1963 and her genre is Folk_music_of_Ireland . She is is an artist for the band Mellow_Candle and was a member of the Flibbertigibbet band .
direct_object Aaron_Turner associatedBand/associatedMusicalArtist Old_Man_Gloom Aaron_Turner started performing in 1995 . He is an Electroacoustic_music and played in the Lotus_Eaters_(band) Old_Man_Gloom bands .
apposition Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Aaron_Turner played with Twilight_(band) and for Old_Man_Gloom . He plays Black_metal music and started performing in 1995 .
apposition Alex_Day associatedBand/associatedMusicalArtist Charlie_McDonnell Alex_Day , whose music career began in 2006 , performs Synthpop music and plays with Chameleon_Circuit_(band) . He has previously worked with the musical artist Charlie_McDonnell .
existential Alex_Day associatedBand/associatedMusicalArtist Charlie_McDonnell Acoustic_music Alex_Day plays with the bands Chameleon_Circuit_(band) the musician Charlie_McDonnell . His active years in music began in 2006 .
relative_object Alison_O'Donnell associatedBand/associatedMusicalArtist Mellow_Candle Alison_O'Donnell was a member of the Flibbertigibbet band and the Mellow_Candle . Her genre is the Folk_music_of_Ireland and started performing in 1963 .
relative_subject Aaron_Turner associatedBand/associatedMusicalArtist Old_Man_Gloom Black_metal musician , Aaron_Turner started performing in 1995 . He played for Old_Man_Gloom and is a musician in Lotus_Eaters_(band) .
relative_subject Alison_O'Donnell associatedBand/associatedMusicalArtist Mellow_Candle Alison_O'Donnell was a member of the Flibbertigibbet band and the Mellow_Candle . Her genre is the Folk_music_of_Ireland and started performing in 1963 .
relative_adverb Aaron_Boogaard club Wichita_Thunder Aaron_Boogaard , whose club is Wichita_Thunder , was born in 1986 in Canada .
relative_adverb 103_Colmore_Row architect John_Madin Labour politician , John_Clancy_(Labour_politician) is the leader of Birmingham . John_Madin was born in this city and was the architect of 103_Colmore_Row .
relative_adverb Canada ethnicGroup Black_Canadians Adam_McQuaid was born in Canada , where the leader is Elizabeth_II and Black_Canadians are an ethnic group .
relative_adverb 103_Colmore_Row architect John_Madin Birmingham born architect John_Madin designed 103_Colmore_Row . Khalid_Mahmood_(British_politician) is one of the leaders in Birmingham .
possessif Netherlands currency Euro Ab_Klink was born in the Netherlands where the Euro is the currency and the leader is , Mark_Rutte .
possessif Canada ethnicGroup Black_Canadians Adam_McQuaid was born in Canada where Elizabeth_II is the leader . It is also where Black_Canadians are one of the ethnic groups .
possessif St_Louis isPartOf Kingdom_of_France St_Louis , which is part of the Kingdom_of_France , is led by Francis_G._Slay and is the birthplace of Alex_Tyus .
existential 103_Colmore_Row architect John_Madin John_Madin , designer of 103_Colmore_Row , was born in Birmingham , currently lead by the Liberal_Democrats .
apposition St_Louis isPartOf Spain St_Louis , which is part of Spain and led by Francis_G._Slay , is the birth place of Alex_Tyus .
relative_subject Alfredo_Zitarrosa deathPlace Montevideo Alfredo_Zitarrosa was born in Uruguay , where the leader is Raúl_Fernando_Sendic_Rodríguez . He died in Montevideo .
relative_subject St_Louis isPartOf Greater_St_Louis St_Louis , part of Greater_St_Louis , is led by Francis_G._Slay and is the birthplace of Alex_Tyus .
relative_subject St_Louis isPartOf French_First_Republic Alex_Tyus was born in St_Louis ( part of the French_First_Republic ) , the leader of which is , Francis_G._Slay .
possessif Elliot_See almaMater University_of_Texas_at_Austin Elliot_See was a student at University_of_Texas_at_Austin and died in St_Louis led by Francis_G._Slay .
existential Elliot_See almaMater University_of_Texas_at_Austin Elliot_See , a past graduate of the University_of_Texas_at_Austin , died in St_Louis where the leader is Francis_G._Slay .
apposition Buenos_Aires governingBody Buenos_Aires_City_Legislature Alberto_Teisaire died in Buenos_Aires which has a current leader called Diego_Santilli and a governing body called Buenos_Aires_City_Legislature .
relative_object Elliot_See almaMater University_of_Texas_at_Austin Elliot_See was a student at University_of_Texas_at_Austin and died in St_Louis led by Francis_G._Slay .
coordinated_full_clauses Agustín_Barboza birthPlace Asunción The birth place of Agustín_Barboza is Asunción Paraguay where he died . The leader of Paraguay is called Juan_Afara .
passive_voice Buenos_Aires governingBody Buenos_Aires_City_Legislature Alberto_Teisaire ' s place of death was Buenos_Aires , where Horacio_Rodríguez_Larreta is the leader and where , the governing body , is the Buenos_Aires_City_Legislature .
possessif Buenos_Aires governingBody Buenos_Aires_City_Legislature Alberto_Teisaire died in Buenos_Aires , the country which is governed by the Buenos_Aires_City_Legislature and the leader is , Horacio_Rodríguez_Larreta .
relative_subject Uruguay leaderName Tabaré_Vázquez Uruguay ' s leader is now Tabaré_Vázquez because Alfredo_Zitarrosa died in Montevideo which is in Uruguay .
possessif Jazz derivative Funk Funk is a derivative of Jazz , the music Alison_O'Donnell plays . Alison_O'Donnell recorded on the record label , Static_Caravan_Recordings .
direct_object Agustín_Barboza birthPlace Paraguay Agustín_Barboza was born in Asunción , in Paraguay . Agustin Barboza , whose musical genre is Guarania_(music) , is signed to Philips_Records .
apposition Alfredo_Zitarrosa background "solo_singer" Alfredo_Zitarrosa , who started as a "solo_singer" , records Zamba_(artform) music for Odeon_Records .
coordinated_full_clauses Alfredo_Zitarrosa background "solo_singer" Alfredo_Zitarrosa ' s record label is RCA_Records , his background includes "solo_singer" singing and his musical genre is Milonga_(music) .
direct_object Rock_music musicFusionGenre Bhangra_(music) Anders_Osborne is an exponent of Rock_music and his record label is Alligator_Records . A musical fusion of Rock_music is Bhangra_(music) .
coordinated_full_clauses Rock_music musicFusionGenre Bhangra_(music) Anders_Osborne , whose genre is Rock_music , is signed to the Rabadash_Records label . A musical fusion of Rock_music is called Bhangra_(music) .
coordinated_full_clauses Alfredo_Zitarrosa background "solo_singer" Alfredo_Zitarrosa , who started out as a "solo_singer" , is with the Orfeo_(Uruguayan_record_label) record label . Alfredo_Zitarrosa ' s musical genre is Candombe .
apposition Rock_music musicFusionGenre Bhangra_(music) Anders_Osborne ' s musical genre is Rock_music and record label is Rabadash_Records . Bhangra_(music) is part of the fusion genre , partly coming from Rock_music .
apposition Agnes_Kant office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Agnes_Kant ' s nationality is Netherlands . She worked as a office workedAt workedAsObjet1 in the Netherlands where Mark_Rutte is the leader .
relative_object Agnes_Kant office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Agnes_Kant ' s nationality is Netherlands . She worked as a office workedAt workedAsObjet1 in the Netherlands where Mark_Rutte is the leader .
apposition United_States capital Washington Abilene_,_Texas is located in Texas in the United_States . Washington is the country ' s capital .
apposition Tarrant_County_,_Texas countySeat Fort_Worth_,_Texas Arlington_,_Texas in the United_States is part of Tarrant_County_,_Texas where the county seat is Fort_Worth_,_Texas .
apposition Madison_County_,_Indiana largestCity Anderson_,_Indiana Alexandria_,_Indiana is part of Madison_County_,_Indiana in the United_States . The largest city in Madison_County_,_Indiana is Anderson_,_Indiana .
apposition United_States capital Washington The capital of the United_States is Washington and King_County_,_Washington which is in Washington is the location of Auburn_,_Washington .
passive_voice New_Jersey capital Trenton_,_New_Jersey Atlantic_City_,_New_Jersey in the United_States . The capital of New_Jersey is Trenton_,_New_Jersey .
relative_subject Lee_County_,_Alabama state Alabama Auburn_,_Alabama , Lee_County_,_Alabama is in Alabama in the United_States .
relative_subject Michigan capital Lansing_,_Michigan Ann_Arbor_,_Michigan in the United_States . Lansing_,_Michigan is the capital of Michigan .
relative_subject United_States capital Washington Abilene_,_Texas is part of Jones_County_,_Texas in the United_States . The capital of the United_States is Washington .
passive_voice Texas capital Austin_,_Texas Arlington_,_Texas in the United_States is part of Texas which has the capital of Austin_,_Texas .
direct_object New_Jersey largestCity Newark_,_New_Jersey Atlantic_City_,_New_Jersey is a part of New_Jersey in the United_States . Newark_,_New_Jersey is the biggest city in New_Jersey .
passive_voice Abilene_Regional_Airport cityServed Abilene_,_Texas Abilene_Regional_Airport serves Abilene_,_Texas , Jones_County_,_Texas , United_States .
passive_voice 200_Public_Square location Cleveland The United_States is home to 200_Public_Square in Cleveland , Cuyahoga_County_,_Ohio .
passive_voice United_States ethnicGroup African_Americans Albany_,_Georgia is part of the state of Georgia in the United_States . African_Americans are an ethnic group in the United_States .
passive_voice United_States ethnicGroup African_Americans Fulton_County_,_Georgia is the location of Alpharetta_,_Georgia , in the United_States , where African_Americans are one of the ethnic groups .
passive_voice United_States ethnicGroup Asian_Americans Most of Atlanta is part of DeKalb_County_,_Georgia in the United_States . An ethnic group in the United_States is Asian_Americans .
passive_voice United_States capital Washington Attica_,_Indiana is in Logan_Township_,_Fountain_County_,_Indiana in the United_States The capital of the United_States is Washington .
relative_subject Angola_International_Airport location Ícolo_e_Bengo Angola_International_Airport is located in Ícolo_e_Bengo , Luanda_Province , Angola .
relative_subject United_States ethnicGroup African_Americans Albany_,_Georgia is a city in Dougherty_County_,_Georgia in the United_States where African_Americans are an ethnic group .
relative_subject Oregon capital Salem_,_Oregon Albany_,_Oregon is a city in Oregon , where the capital is Salem_,_Oregon is in the United_States .
relative_subject United_States ethnicGroup Native_Americans Alpharetta_,_Georgia , is part of Fulton_County_,_Georgia in the United_States where Native_Americans are an ethnic group .
relative_subject United_States ethnicGroup Native_Americans Amarillo_,_Texas is part of Potter_County_,_Texas , United_States and Native_Americans are an ethnic group within the country .
relative_subject United_States ethnicGroup Native_Americans Angola_,_Indiana is a city located in Indiana in the United_States , where Native_Americans are an ethnic group .
relative_subject United_States capital Washington Albany_,_Oregon falls under Linn_County_,_Oregon , in the United_States , the capital of which is , Washington .
relative_subject United_States capital Washington Attica_,_Indiana is in Logan_Township_,_Fountain_County_,_Indiana in the United_States The capital of the United_States is Washington .
relative_subject United_States ethnicGroup Native_Americans The Native_Americans are an ethnic group within the United_States where Auburn_,_Alabama is located in part of Alabama .
relative_subject 300_North_LaSalle location Chicago 300_North_LaSalle is in Chicago in the DuPage_County_,_Illinois within the United_States .
passive_voice United_States language English_language Abilene_,_Texas is part of Texas in the United_States , where English_language is the language used .
passive_voice United_States ethnicGroup Native_Americans Albany_,_Oregon is a city in Oregon , in the United_States , where one of the ethnic groups is Native_Americans .
passive_voice United_States ethnicGroup Native_Americans Angola_,_Indiana is a city located in Indiana in the United_States , where Native_Americans are an ethnic group .
passive_voice United_States ethnicGroup Asian_Americans Angola_,_Indiana , is part of Steuben_County_,_Indiana in the United_States , where one of the ethnic groups , is Asian_Americans .
passive_voice United_States capital Washington Albany_,_Oregon falls under Linn_County_,_Oregon , in the United_States , the capital of which is , Washington .
existential 200_Public_Square location Cleveland 200_Public_Square is located in Cleveland , Cuyahoga_County_,_Ohio in the United_States .
existential Asser_Levy_Public_Baths location New_York_City The Asser_Levy_Public_Baths are located in New_York_City , New_York , United_States .
apposition United_States ethnicGroup Asian_Americans Albany_,_Georgia is in the United_States . State of Georgia . Asian_Americans are an ethnic group in the United_States .
apposition United_States ethnicGroup African_Americans Alpharetta_,_Georgia is part of Fulton_County_,_Georgia in the United_States . The United_States is home to an ethnic group called African_Americans .
apposition United_States ethnicGroup Native_Americans Angola_,_Indiana is a city located in Indiana in the United_States , where Native_Americans are an ethnic group .
apposition United_States ethnicGroup Native_Americans Atlanta is a part of Fulton_County_,_Georgia in the United_States . An ethnic group in the United_States are Native_Americans .
apposition Indiana capital Indianapolis Attica_,_Indiana in the United_States is part of Indiana where the capital is Indianapolis .
apposition United_States capital Washington Attica_,_Indiana is in Logan_Township_,_Fountain_County_,_Indiana in the United_States The capital of the United_States is Washington .
apposition 3Arena location Dublin The 3Arena is located in Dublin , which is a part of Leinster , in the Republic_of_Ireland .
coordinated_clauses Angola_International_Airport location Ícolo_e_Bengo Angola_International_Airport is located in Ícolo_e_Bengo , Luanda_Province , Angola .
coordinated_clauses Texas capital Austin_,_Texas Abilene_,_Texas is located in the United_States state of Texas , where the capital is Austin_,_Texas .
coordinated_clauses United_States ethnicGroup African_Americans Albany_,_Georgia is part of Dougherty_County_,_Georgia in the United_States where African_Americans are one of the ethnic groups .
coordinated_clauses United_States ethnicGroup African_Americans The city of Albany_,_Oregon is in Oregon , in the United_States , where African_Americans are one of the ethnic groups .
coordinated_clauses Oregon capital Salem_,_Oregon Albany_,_Oregon is a city in Oregon , where the capital is Salem_,_Oregon is in the United_States .
coordinated_clauses United_States ethnicGroup Asian_Americans Angola_,_Indiana , is part of Steuben_County_,_Indiana in the United_States . Asian_Americans are an ethnic group in the United_States .
coordinated_clauses United_States ethnicGroup Native_Americans Atlanta is a part of Fulton_County_,_Georgia in the United_States . An ethnic group in the United_States are Native_Americans .
coordinated_clauses United_States capital Washington Akron_,_Ohio , Summit_County_,_Ohio is located within the United_States . The capital of the United_States is Washington .
coordinated_clauses United_States capital Washington Albany_,_Oregon is part of Oregon in the United_States , where the capital is Washington .
coordinated_clauses United_States ethnicGroup Asian_Americans Akron_,_Ohio is a part of Summit_County_,_Ohio , in the United_States , where Asian_Americans are an ethnic group .
direct_object United_States capital Washington Alpharetta_,_Georgia , is in the United_States , where the capital is Washington and where one of the ethnic groups is Asian_Americans .
coordinated_full_clauses United_States capital Washington Washington is the capital of the United_States , where you will find Albany_,_Oregon and where one of the ethnic groups are the African_Americans .
direct_object United_States capital Washington Amarillo_,_Texas is in the United_States , where Washington is the capital and where Asian_Americans are an ethnic group .
coordinated_full_clauses United_States capital Washington The United_States includes the ethnic group of Asian_Americans , has the capital city of Washington and is also the location of Albany_,_Oregon .
coordinated_full_clauses United_States language English_language The United_States uses the English_language , includes the ethnic group of African_Americans and is the location of Albany_,_Oregon .
direct_object Philippines language Arabic Batchoy comes from the Philippines where the Igorot_people are an ethnic group and one of the languages is Arabic .
direct_object United_States language English_language A_Severed_Wasp is from the United_States where the language is English_language and the Asian_Americans are an ethnic group .
coordinated_full_clauses United_States language English_language Alcatraz_Versus_the_Evil_Librarians is from the United_States where they speak English_language and White_Americans live .
direct_object United_States leaderName Barack_Obama A_Severed_Wasp is from the United_States where the leader is Barack_Obama and one of the ethnic groups is African_Americans .
relative_adverb Albany_,_Oregon isPartOf Oregon The city of Albany_,_Oregon is in Oregon , in the United_States , where African_Americans are one of the ethnic groups .
relative_adverb Alpharetta_,_Georgia isPartOf Fulton_County_,_Georgia Fulton_County_,_Georgia is the location of Alpharetta_,_Georgia , in the United_States , where African_Americans are one of the ethnic groups .
relative_adverb Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas , United_States , where African_Americans are one of the ethnic groups .
relative_adverb United_States capital Washington Akron_,_Ohio is located within the United_States ( capital city : Washington ) where Native_Americans are one of the ethnic groups .
relative_adverb United_States capital Washington Akron_,_Ohio , is in the United_States . The capital of the country is Washington and Asian_Americans are one of the ethnic groups in that country .
relative_adverb United_States language English_language Albany_,_Oregon is in the United_States . Where English_language is spoken and Native_Americans are one of the ethnic groups .
relative_adverb United_States capital Washington Bacon_Explosion is from the United_States , home of Asian_Americans and the capital of Washington .
relative_adverb United_States leaderName Barack_Obama Barack_Obama leads the United_States . which is the country of the Bacon_Explosion . Native_Americans are an ethnic group in that country .
existential A_Long_Long_Way publisher Viking_Press The book " A_Long_Long_Way " was published by Viking_Press . It was written in Ireland , where White_people are considered an ethnic group .
relative_adverb United_States capital Washington In the United_States one of the ethnic groups is African_Americans and the capital is Washington . A_Fortress_of_Grey_Ice is from the country .
relative_adverb United_States language English_language Alcatraz_Versus_the_Evil_Librarians is from the United_States where they speak English_language and one of the ethnic groups is African_Americans .
relative_adverb United_States leaderName Barack_Obama Native_Americans and Alcatraz_Versus_the_Evil_Librarians are part of the United_States ; which Barack_Obama is the president of .
direct_object Angola_,_Indiana isPartOf Pleasant_Township_,_Steuben_County_,_Indiana The Asian_Americans are an ethnic group in the United_States which is the location of Angola_,_Indiana , Pleasant_Township_,_Steuben_County_,_Indiana .
direct_object Albany_,_Oregon isPartOf Oregon The Asian_Americans are an ethnic group in the United_States which is the location of Albany_,_Oregon , part of Oregon .
apposition A_Fortress_of_Grey_Ice language English_language A_Fortress_of_Grey_Ice , originally published in English_language , is from the United_States , which includes African_Americans among its ethnic groups .
passive_voice Albany_,_Georgia isPartOf Georgia Albany_,_Georgia is located in the United_States state of Georgia where African_Americans are an ethnic group .
passive_voice United_States capital Washington The city of Alpharetta_,_Georgia is in the United_States where Native_Americans are an ethnic group and Washington is the capital city .
passive_voice United_States capital Washington Atlanta is in the United_States where Washington is the capital and where African_Americans are an ethnic group .
passive_voice Atlanta isPartOf DeKalb_County_,_Georgia Atlanta is in DeKalb_County_,_Georgia of the United_States where one finds many ethnic groups including Asian_Americans .
passive_voice United_States demonym Americans The United_States , where Americans live , includes the ethnic group of African_Americans and is the location of Albuquerque_,_New_Mexico .
passive_voice United_States capital Washington Washington is the capital of the United_States , where Akron_,_Ohio is located and Asian_Americans are one of the ethnic groups .
passive_voice Akron_,_Ohio isPartOf Summit_County_,_Ohio Akron_,_Ohio is in Summit_County_,_Ohio , United_States where The Native_Americans are an ethnic group .
relative_adverb United_States leaderTitle President_of_the_United_States The leader of the United_States is called the President_of_the_United_States . Native_Americans are one of the ethnic groups within the country which is also the location of Albuquerque_,_New_Mexico .
passive_voice Ayam_penyet region Malaysia Ayam_penyet comes from the region of Malaysia and is a food found in Java , where there is a ethnic group called Baduy .
passive_voice Ayam_penyet region Singapore From the Singapore region , Ayam_penyet , is a food found in Java where an ethic group is the Baduy .
passive_voice Philippines language Arabic Arabic is a language spoken in the Philippines , where Zamboangans are an ethnic group and Batchoy is a dish eaten there .
passive_voice Spain language Spanish_language Spaniards are the ethnic group of Spain where the language is Spanish_language and where Ajoblanco is from .
relative_adverb A_Wizard_of_Mars language English_language A_Wizard_of_Mars is written in English_language and was published in the United_States . Additionally , Native_Americans are an ethnic group in the United_States .
passive_voice United_States capital Washington Washington is the capital of the United_States the country where Native_Americans are an ethnic group . It is also where A_Fortress_of_Grey_Ice is from .
passive_voice United_States capital Washington A_Severed_Wasp is from the United_States where Washington is the capital and Native_Americans are an ethnic group .
passive_voice United_States capital Washington A_Severed_Wasp is from the United_States , where there is an ethnic group called Asian_Americans and where the capital is Washington .
passive_voice United_States leaderName Barack_Obama A_Fortress_of_Grey_Ice is from the United_States where Barack_Obama is the leader and Asian_Americans are one of the ethnic groups .
passive_voice United_States leaderName Barack_Obama 1634:_The_Ram_Rebellion comes from the United_States where the African_Americans are an ethnic group and Barack_Obama is President .
relative_object Auburn_,_Alabama isPartOf Alabama The Asian_Americans are an ethnic group in the United_States where Auburn_,_Alabama is located in Alabama .
relative_object Albany_,_Georgia isPartOf Dougherty_County_,_Georgia Albany_,_Georgia is part of Dougherty_County_,_Georgia in the United_States where African_Americans are one of the ethnic groups .
relative_object Albany_,_Georgia isPartOf Dougherty_County_,_Georgia Albany_,_Georgia is part of Dougherty_County_,_Georgia , in the United_States , where Asian_Americans are one of the ethnic groups .
relative_object Albany_,_Oregon isPartOf Benton_County_,_Oregon The city of Albany_,_Oregon is part of Benton_County_,_Oregon , in the United_States , where African_Americans are one of the ethnic groups .
relative_object Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas , in the United_States , where Asian_Americans are an ethnic group .
relative_object Angola_,_Indiana isPartOf Pleasant_Township_,_Steuben_County_,_Indiana Angola_,_Indiana is in Pleasant_Township_,_Steuben_County_,_Indiana of the United_States where Asian_Americans are one of several ethnic groups .
relative_object Angola_,_Indiana isPartOf Steuben_County_,_Indiana Native_Americans are an ethnic group within the United_States and Angola_,_Indiana is part of Steuben_County_,_Indiana there .
relative_object Atlanta isPartOf Georgia The United_States include the ethnic group of Native_Americans and is the location of Atlanta in the state of Georgia .
relative_object United_States capital Washington The Asian_Americans are an ethnic group within the United_States where the capital is Washington and Auburn_,_Alabama is located .
relative_object United_States demonym Americans Albuquerque_,_New_Mexico , United_States . Americans live in the United_States and Asian_Americans are an ethnic group there .
relative_object United_States leader Barack_Obama Albuquerque_,_New_Mexico is located in the United_States where Barack_Obama is the president and African_Americans are an ethnic group within the country .
relative_object Auburn_,_Alabama isPartOf Alabama The Native_Americans are an ethnic group within the United_States where Auburn_,_Alabama is located in part of Alabama .
relative_object United_States language English_language Albany_,_Oregon is in the United_States where African_Americans are an ethic group and English_language is spoken .
relative_object Malaysia leaderName Abu_Zahar_Ujang Abu_Zahar_Ujang is the leader of Malaysia . An ethnic group there are the Malaysian_Chinese . Asam_pedas is a food found in that country .
relative_object United_States capital Washington One of the ethnic groups in the United_States are Native_Americans , the country is also the home of the dish Bacon_Explosion and has Washington as it ' s capitol city .
relative_object United_States leaderName Joe_Biden Bacon_Explosion comes from the United_States , where Joe_Biden is a leader and White_Americans are an ethnic group .
relative_object Philippines language Arabic One of the languages in the Philippines is Arabic . Batchoy comes from there . Chinese_Filipino people are also from the Philippines .
relative_object A_Severed_Wasp language English_language Native_Americans are one of the ethnic groups of the United_States where A_Severed_Wasp ( written in English_language ) is from .
relative_object Alcatraz_Versus_the_Evil_Librarians language English_language The book Alcatraz_Versus_the_Evil_Librarians comes from the United_States and is written in English_language . One of the ethnic groups in the United_States are the Native_Americans .
relative_object United_States capital Washington The United_States is home to Native_Americans and is the origin place of A_Fortress_of_Grey_Ice . Washington is the capital .
relative_object United_States capital Washington A_Fortress_of_Grey_Ice is from the United_States where African_Americans are one of the ethnic groups and the capital city is Washington .
relative_object United_States capital Washington One of the ethnic Groups of the United_States ( where the capital city is Washington ) is African_Americans . Additionally , A_Wizard_of_Mars originates from the United_States .
relative_object United_States capital Washington The capital city of the United_States is Washington . Some Asian_Americans live in the United_States and A_Wizard_of_Mars was Published there .
relative_object United_States language English_language A_Fortress_of_Grey_Ice is from the United_States , where the language is English_language and Native_Americans are one of the ethnic groups .
relative_object United_States language English_language Alcatraz_Versus_the_Evil_Librarians is from the United_States where they speak English_language and one of the ethnic groups is African_Americans .
relative_object United_States leaderName Barack_Obama A_Severed_Wasp is from the United_States , where Barack_Obama is the leader . Asian_Americans are one of the ethnic groups there .
possessif Albany_,_Georgia isPartOf Georgia Albany_,_Georgia is located in the United_States state of Georgia where African_Americans are an ethnic group .
possessif Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas , in the United_States , where Asian_Americans are an ethnic group .
possessif Angola_,_Indiana isPartOf Indiana Angola_,_Indiana is a city in Indiana , which is in the United_States . A country with multiple ethnic groups such as Native_Americans .
possessif Angola_,_Indiana isPartOf Steuben_County_,_Indiana Angola_,_Indiana , is part of Steuben_County_,_Indiana in the United_States . Asian_Americans are an ethnic group in the United_States .
possessif Angola_,_Indiana isPartOf Steuben_County_,_Indiana Angola_,_Indiana is in Steuben_County_,_Indiana in the United_States , where Native_Americans , is one of the ethnic groups .
possessif Atlanta isPartOf Georgia One of the ethnic groups in the United_States is Asian_Americans and Atlanta is in the state of Georgia .
possessif Atlanta isPartOf Georgia The United_States include the ethnic group of Native_Americans and is the location of Atlanta in the state of Georgia .
possessif Austin_,_Texas leaderTitle City_Manager Austin_,_Texas is in the United_States ; where the City_Manager is the leader and African_Americans call home .
possessif United_States capital Washington Akron_,_Ohio is located in United_States . A country where African_Americans are one of the ethnic groups and the capital is Washington .
possessif Austin_,_Texas leaderTitle Mayor Austin_,_Texas is led by a Mayor and is located in the United_States where Asian_Americans are one of the ethnic groups .
possessif United_States language English_language Albany_,_Oregon is in the United_States where English_language is the spoken language . Asian_Americans are an ethnic group in the United_States .
possessif United_States demonym Americans Americans and Native_Americans are people living in the United_States and Albuquerque_,_New_Mexico is a location .
possessif United_States language English_language The United_States uses the English_language , includes the ethnic group of African_Americans and is the location of Albany_,_Oregon .
possessif Ayam_penyet region Malaysia Ayam_penyet is a popular dish in Malaysia , but originated in Java where the Banyumasan_people are an ethnic group .
possessif United_States capital Washington The Bacon_Explosion is from the United_States where African_Americans are one of the ethnic groups and the capital is Washington .
possessif United_States leaderName Barack_Obama The Bacon_Explosion comes from the United_States where Barack_Obama is the leader and Asian_Americans live .
possessif Philippines language Philippine_Spanish Philippines is the country Batchoy comes from , one of the ethnic groups there is the Zamboangans and the spoken language is Philippine_Spanish .
possessif Philippines language Arabic Arabic is a language spoken in the Philippines , where Zamboangans are an ethnic group and Batchoy is a dish eaten there .
possessif A_Loyal_Character_Dancer publisher Soho_Press A_Loyal_Character_Dancer was published by Soho_Press in the United_States where Native_Americans are one of the ethnic groups .
possessif Alcatraz_Versus_the_Evil_Librarians language English_language Written in English_language , Alcatraz_Versus_the_Evil_Librarians comes from the United_States . Native_Americans are one of the ethnic groups of the United_States .
possessif United_States capital Washington A_Fortress_of_Grey_Ice is from the United_States , where the capital is Washington and Native_Americans are one of the ethnic groups .
possessif United_States capital Washington A_Wizard_of_Mars comes from the United_States where Native_Americans are one of the ethnic groups and the capital city is Washington .
possessif United_States language English_language A_Severed_Wasp is from the United_States where English_language is the primary language . In addition Native_Americans are an ethnic group in the United_States .
possessif United_States leaderName Barack_Obama 1634:_The_Ram_Rebellion comes from the United_States , where the leader is Barack_Obama and the African_Americans are an ethnic group .
possessif United_States leaderName Barack_Obama Native_Americans and Alcatraz_Versus_the_Evil_Librarians are part of the United_States ; which Barack_Obama is the president of .
direct_object Albany_,_Oregon isPartOf Oregon Albany_,_Oregon is in Oregon in the United_States where African_Americans are part of an ethnic group .
direct_object Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas , United_States and Native_Americans are an ethnic group within the country .
direct_object Angola_,_Indiana isPartOf Indiana Angola_,_Indiana is a city in Indiana , which is in the United_States . A country with multiple ethnic groups such as Native_Americans .
direct_object United_States capital Washington Atlanta is in the United_States ; where the capital is Washington and African_Americans are one of the ethnic groups .
direct_object Atlanta isPartOf DeKalb_County_,_Georgia Most of Atlanta , United_States is part of DeKalb_County_,_Georgia . Native_Americans are an ethnic group in the United_States .
direct_object United_States capital Washington Amarillo_,_Texas is located in the United_States ( capital : Washington ) and African_Americans are an ethnic group there .
direct_object United_States leader Barack_Obama Albuquerque_,_New_Mexico is located in the United_States where Barack_Obama is the president and African_Americans are an ethnic group within the country .
direct_object United_States language English_language Akron_,_Ohio , is located in the English_language speaking , United_States , where Asian_Americans are one of the ethnic groups .
direct_object United_States language English_language Albany_,_Oregon is in the United_States where English_language is the language spoken . Native_Americans are an ethnic group in the United_States .
direct_object Malaysia leaderName Abu_Zahar_Ujang Abu_Zahar_Ujang is the leader of Malaysia . An ethnic group there are the Malaysian_Chinese . Asam_pedas is a food found in that country .
direct_object United_States capital Washington The capital of the United_States is Washington and White_Americans are one of the ethnic groups . It is home to the Bacon_Explosion .
direct_object Philippines language Philippine_Spanish Zamboangans are a group in the Philippines , where the spoken language is Philippine_Spanish and Batchoy is eaten .
direct_object Philippines language Philippine_English Philippine_English is the language spoken in the Philippines , where Batchoy comes from . It is also the country where the Igorot_people are an ethnic group .
direct_object Philippines language Philippine_Spanish The Ilocano_people are an ethnic group from the Philippines , where the spoken language is Philippine_Spanish . It is also where Batchoy comes from .
direct_object Spain leaderName Felipe_VI_of_Spain The dish of Ajoblanco is from Spain where Felipe_VI_of_Spain is the leader and Spaniards are an ethnic group .
direct_object A_Wizard_of_Mars language English_language The United_States is not only home to Native_Americans but also the origin place of the English_language book A_Wizard_of_Mars .
direct_object United_States capital Washington A_Wizard_of_Mars comes from the United_States where Native_Americans are one of the ethnic groups and the capital city is Washington .
direct_object United_States capital Washington Washington is the capital of the United_States a country where one of the ethnic groups is African_Americans . Also where A_Wizard_of_Mars originates from .
direct_object United_States capital Washington The United_States is home to Asian_Americans and the origin place of A_Fortress_of_Grey_Ice . Washington is the capital .
direct_object United_States leaderName Barack_Obama A_Fortress_of_Grey_Ice is from the United_States where Barack_Obama is the leader and Asian_Americans are one of the ethnic groups .
direct_object United_States language English_language A_Fortress_of_Grey_Ice is from the United_States , where the language is English_language and Native_Americans are one of the ethnic groups .
direct_object United_States leaderName Barack_Obama The United_States , home to Barack_Obama and Native_Americans , is also the origin place of A_Fortress_of_Grey_Ice .
direct_object United_States leaderName Barack_Obama A_Severed_Wasp originates from the United_States where the President is Barack_Obama . One of the ethnic groups in the United_States are the Native_Americans .
possessif United_States capital Washington The Asian_Americans are an ethnic group found in the United_States which has the capital city of Washington and is the location of Atlanta .
possessif Philippines language Philippine_English Philippines is the country Batchoy comes from , in this country there is a ethnic group called Ilocano_people and they speak Philippine_English .
possessif United_States language English_language Alcatraz_Versus_the_Evil_Librarians is from the United_States where they speak English_language and White_Americans live .
coordinated_full_clauses United_States leaderName Barack_Obama A_Severed_Wasp originates from the United_States which has Barack_Obama as leader and includes many Asian_Americans .
relative_subject United_States capital Washington The United_States has the capital city of Washington is the location of Alpharetta_,_Georgia and includes the ethnic group of African_Americans .
relative_subject Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas in the United_States . Asian_Americans are an ethnic group in the United_States .
relative_subject Atlanta isPartOf DeKalb_County_,_Georgia Most of Atlanta , United_States is part of DeKalb_County_,_Georgia . Native_Americans are an ethnic group in the United_States .
relative_subject Angola_,_Indiana isPartOf Indiana Angola_,_Indiana is in Indiana , United_States . African_Americans are an ethnic group in the United_States .
relative_subject Albany_,_Oregon isPartOf Linn_County_,_Oregon Albany_,_Oregon falls under Linn_County_,_Oregon , in the United_States , where Asian_Americans are one of the ethnic groups .
relative_subject United_States capital Washington The United_States contain the ethnic group of Asian_Americans , has the capital of Washington and is the location of Albany_,_Oregon .
relative_subject Auburn_,_Alabama isPartOf Alabama The Native_Americans are an ethnic group within the United_States where Auburn_,_Alabama is located in part of Alabama .
relative_subject United_States capital Washington Auburn_,_Alabama is in the United_States ; where Native_Americans are one of the ethnic groups and where the capital is Washington .
relative_subject United_States language English_language Albany_,_Oregon is in the United_States where African_Americans are an ethic group and English_language is spoken .
relative_subject United_States language English_language Akron_,_Ohio is located in the United_States where English_language is the spoken language and Native_Americans are an ethnic group .
relative_subject United_States capital Washington One of the ethnic groups in the United_States are Native_Americans , the country is also the home of the dish Bacon_Explosion and has Washington as it ' s capitol city .
relative_subject United_States leaderName Barack_Obama The country Bacon_Explosion comes from is the United_States . It is also the country where Barack_Obama is the leader and Asian_Americans are one of the ethnic groups .
relative_subject United_States leaderName Joe_Biden The Bacon_Explosion originates from the United_States where Joe_Biden is a political leader and the White_Americans are an ethnic group .
relative_subject Philippines language Arabic The ethnic group Moro_people come from the Philippines where Arabic is one of the languages used , it is also where the dish Batchoy comes from .
relative_subject A_Loyal_Character_Dancer publisher Soho_Press A_Loyal_Character_Dancer is published by Soho_Press in the United_States where Native_Americans are one of the ethnic groups .
relative_subject A_Severed_Wasp language English_language A_Severed_Wasp is from the United_States and is written in the English_language . Asian_Americans are one of many ethnic groups in the United_States .
relative_subject Alcatraz_Versus_the_Evil_Librarians language English_language Written in English_language , Alcatraz_Versus_the_Evil_Librarians comes from the United_States . Native_Americans are one of the ethnic groups of the United_States .
relative_subject United_States capital Washington Washington is the capital of the United_States a country where one of the ethnic groups is African_Americans . Also where A_Wizard_of_Mars originates from .
relative_subject United_States language English_language A_Severed_Wasp is from the United_States where the language is English_language and the Native_Americans are one of the ethnic groups .
relative_subject United_States leaderName Barack_Obama Native_Americans are one of the ethnic groups in the United_States where Barack_Obama is the president . It is also the country that A_Fortress_of_Grey_Ice is from .
relative_subject United_States leaderName Barack_Obama Native_Americans and Alcatraz_Versus_the_Evil_Librarians are part of the United_States ; which Barack_Obama is the president of .
coordinated_full_clauses Albany_,_Georgia isPartOf Dougherty_County_,_Georgia Albany_,_Georgia is part of Dougherty_County_,_Georgia , in the United_States , where Asian_Americans are one of the ethnic groups .
coordinated_full_clauses Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas , in the United_States , where Asian_Americans are an ethnic group .
coordinated_full_clauses Angola_,_Indiana isPartOf Steuben_County_,_Indiana Angola_,_Indiana is in Steuben_County_,_Indiana in the United_States , where Native_Americans , is one of the ethnic groups .
coordinated_full_clauses Atlanta isPartOf Georgia The African_Americans are an ethnic group in the United_States where Atlanta is located within the state of Georgia .
coordinated_full_clauses United_States capital Washington The African_Americans are an ethnic group in the United_States where the capital city is Washington and Auburn_,_Alabama is located .
coordinated_full_clauses United_States demonym Americans Albuquerque_,_New_Mexico , United_States . Americans live in the United_States and Asian_Americans are an ethnic group there .
coordinated_full_clauses Albany_,_Oregon isPartOf Linn_County_,_Oregon Albany_,_Oregon falls under Linn_County_,_Oregon , in the United_States , where Asian_Americans are one of the ethnic groups .
coordinated_full_clauses United_States leader Barack_Obama Albuquerque_,_New_Mexico , is in the United_States . The country led by Barack_Obama and where Native_Americans are an ethnic group .
coordinated_full_clauses Spain leaderName Felipe_VI_of_Spain Arròs_negre is a traditional dish from Spain where Felipe_VI_of_Spain is the leader and Spaniards are the ethnic group .
coordinated_full_clauses Ayam_penyet region Singapore Ayam_penyet is from Singapore and also Java , where the Baduy are an ethnic group .
coordinated_full_clauses Philippines language Arabic Arabic is a language spoken in the Philippines , where Zamboangans are an ethnic group and Batchoy is a dish eaten there .
coordinated_full_clauses A_Loyal_Character_Dancer publisher Soho_Press A_Loyal_Character_Dancer was published in the United_States by Soho_Press . One of the ethnic groups in the United_States are African_Americans .
coordinated_full_clauses United_States language English_language In the United_States , where English_language is the primary language , one of the ethnic groups is African_Americans . In addition , A_Fortress_of_Grey_Ice is also from the United_States .
coordinated_full_clauses United_States leaderName Barack_Obama A_Severed_Wasp originates from the United_States where the President is Barack_Obama . One of the ethnic groups in the United_States are the Native_Americans .
apposition Auburn_,_Alabama isPartOf Alabama Auburn_,_Alabama is part of Alabama in the United_States where Asian_Americans are an ethnic group .
relative_adverb Albany_,_Georgia isPartOf Georgia Albany_,_Georgia is part of the state of Georgia , United_States , of which Native_Americans are one of the ethnic groups .
apposition Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas which is part of the United_States where one of the ethnic groups are Native_Americans .
apposition Angola_,_Indiana isPartOf Pleasant_Township_,_Steuben_County_,_Indiana Angola_,_Indiana is in Pleasant_Township_,_Steuben_County_,_Indiana of the United_States where Asian_Americans are one of several ethnic groups .
apposition United_States capital Washington Amarillo_,_Texas is in the United_States , where Washington is the capital and where Asian_Americans are an ethnic group .
apposition United_States demonym Americans The United_States , where Americans live , includes the ethnic group of African_Americans and is the location of Albuquerque_,_New_Mexico .
apposition United_States leader Barack_Obama Barack_Obama is the leader of the United_States where Albuquerque_,_New_Mexico is located and African_Americans are one of the ethnic groups of the country .
apposition Auburn_,_Alabama isPartOf Alabama The Native_Americans are an ethnic group within the United_States where Auburn_,_Alabama is located in part of Alabama .
apposition United_States language English_language English_language is the language of the United_States , where you will find the town of Akron_,_Ohio and where Native_Americans are an ethnic group .
apposition Malaysia capital Putrajaya Asam_pedas is a food found in Malaysia where Putrajaya is the capital and the Malaysian_Malay are an ethnic group .
apposition United_States capital Washington Bacon_Explosion originated in the United_States , where the capital is Washington and Native_Americans are an ethnic group .
apposition United_States leaderName John_Roberts The Asian_Americans are an ethnic group in the United_States where John_Roberts is a leader and Bacon_Explosion originated .
apposition Philippines language Philippine_English Batchoy comes from the Philippines where Philippine_English is the language spoken . It is also where Zamboangans are a group .
apposition Philippines language Arabic Arabic is a language spoken in the Philippines , where Zamboangans are an ethnic group and Batchoy is a dish eaten there .
apposition Philippines language Philippine_English Philippine_English is the language of the Philippines , where Batchoy comes from and where the Igorot_people are an ethnic group .
apposition Spain leaderName Felipe_VI_of_Spain Felipe_VI_of_Spain is the leader of Spain , where Spaniards are the ethnic group and where Ajoblanco originates from .
apposition Alcatraz_Versus_the_Evil_Librarians language English_language Alcatraz_Versus_the_Evil_Librarians , written in English_language , is from the United_States , where there is an ethnic group called the African_Americans .
apposition United_States capital Washington A_Severed_Wasp originates from the United_States where the capital is Washington and the Native_Americans are an ethnic group .
apposition United_States language English_language In the United_States , where English_language is the primary language , one of the ethnic groups is African_Americans . In addition , A_Fortress_of_Grey_Ice is also from the United_States .
apposition United_States leaderName Barack_Obama A_Fortress_of_Grey_Ice is from the United_States where Barack_Obama is the leader and the African_Americans are one of the ethnic groups .
apposition United_States capital Washington A_Fortress_of_Grey_Ice is from the United_States , where Washington is the capital and Asian_Americans are one of the ethnic groups .
apposition United_States language English_language A_Severed_Wasp is from the United_States where the language is English_language and one of the ethnic groups are the Asian_Americans .
apposition United_States language English_language A_Severed_Wasp is from the United_States where English_language is the primary language . In addition Native_Americans are an ethnic group in the United_States .
apposition United_States leaderName Barack_Obama Native_Americans are one of the ethnic groups in the United_States where Barack_Obama is the president . It is also the country that A_Fortress_of_Grey_Ice is from .
apposition United_States leaderName Barack_Obama A_Wizard_of_Mars originates from the United_States where Barack_Obama is the leader and there are many Asian_Americans .
possessif United_States capital Washington The city of Alpharetta_,_Georgia is in the United_States where Native_Americans are an ethnic group and Washington is the capital city .
possessif United_States capital Washington African_Americans are an ethnic group of the United_States , where the capital city is Washington and where Auburn_,_Alabama is located .
possessif United_States language English_language The Asian_Americans are an ethnic group in the United_States , where English_language is spoken and Albany_,_Oregon is located .
possessif Philippines language Philippine_Spanish Batchoy is eaten in the Philippines where the language used is Philippine_Spanish and one of the ethnic groups are the Zamboangans .
possessif United_States capital Washington 1634:_The_Ram_Rebellion is from the United_States where the capital city is Washington and an ethnic group is African_Americans .
possessif United_States capital Washington Washington is the capital of the United_States , where many Asian_Americans live . A_Wizard_of_Mars was written in the United_States .
possessif United_States leaderName Barack_Obama Native_Americans are an ethnic group in the United_States where Barack_Obama is president and where A_Severed_Wasp is from .
relative_object Albany_,_Georgia isPartOf Dougherty_County_,_Georgia The United_States includes the ethnic group of Native_Americans and is the location of Albany_,_Georgia , part of Dougherty_County_,_Georgia ,.
relative_object Alpharetta_,_Georgia isPartOf Georgia Alpharetta_,_Georgia is part of the United_States . state , Georgia where Native_Americans are an ethnic group .
relative_object Atlanta isPartOf Fulton_County_,_Georgia Atlanta is part of Fulton_County_,_Georgia , United_States where Asian_Americans are an ethnic group .
apposition Austin_,_Texas leaderTitle City_Manager The City_Manager is the leader of Austin_,_Texas , in the United_States . A country with many ethnic groups , one of which is African_Americans .
apposition Albany_,_Georgia isPartOf Dougherty_County_,_Georgia Albany_,_Georgia is located in Dougherty_County_,_Georgia in the United_States . Native_Americans are an ethnic group in the United_States .
apposition Alpharetta_,_Georgia isPartOf Fulton_County_,_Georgia Alpharetta_,_Georgia is part of Fulton_County_,_Georgia in the United_States . The United_States is home to an ethnic group called African_Americans .
apposition Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas , United_States and Native_Americans are an ethnic group within the country .
apposition Angola_,_Indiana isPartOf Steuben_County_,_Indiana Native_Americans are an ethnic group within the United_States and Angola_,_Indiana is part of Steuben_County_,_Indiana there .
apposition Atlanta isPartOf Georgia One of the ethnic groups in the United_States is Asian_Americans and Atlanta is in the state of Georgia .
apposition Atlanta isPartOf DeKalb_County_,_Georgia Most of Atlanta is part of DeKalb_County_,_Georgia , in the United_States , where Native_Americans are one of the ethnic groups .
apposition United_States demonym Americans The United_States includes the ethnic group of Asian_Americans amongst the Americans living there . It is the location of Albuquerque_,_New_Mexico .
apposition United_States capital Washington The United_States contain the ethnic group of Asian_Americans , has the capital of Washington and is the location of Albany_,_Oregon .
apposition United_States capital Washington Auburn_,_Alabama is located in the United_States ( capital city : Washington ) and the Native_Americans are one of the country ' s ethnic group .
apposition United_States capital Washington The capitol of the United_States is Washington . One of the ethnic groups are African_Americans and it is also home to the Bacon_Explosion .
apposition United_States leaderName Barack_Obama The United_States is the country of the Bacon_Explosion , the leader of the United_States is Barack_Obama and African_Americans are one the the ethnic groups in the United_States .
apposition Philippines language Arabic One of the languages in the Philippines is Arabic . Batchoy comes from there . Chinese_Filipino people are also from the Philippines .
apposition A_Severed_Wasp language English_language A_Severed_Wasp is from the United_States and is written in the English_language . Asian_Americans are one of many ethnic groups in the United_States .
apposition United_States leaderName Barack_Obama Native_Americans and Alcatraz_Versus_the_Evil_Librarians are part of the United_States ; which Barack_Obama is the president of .
relative_adverb Albany_,_Georgia isPartOf Georgia The United_States is home to the ethnic group Native_Americans and is where you will find Albany_,_Georgia , in the state of Georgia .
relative_adverb United_States capital Washington The Asian_Americans are an ethnic group in the United_States which is the location of Atlanta and has the capital of Washington .
direct_object United_States language English_language Albany_,_Georgia , is in the United_States , where English_language is spoken and Asian_Americans are one of the ethnic groups .
relative_adverb United_States leaderName Joe_Biden White_Americans are an ethnic group in the United_States and that country has a political leader called Joe_Biden . The Bacon_Explosion comes from the United_States .
direct_object Philippines language Philippine_English Batchoy is eaten in the Philippines , where there is a ethnic group called Zamboangans and the language is Philippine_English .
direct_object A_Severed_Wasp language English_language A_Severed_Wasp was written in English_language in the United_States , where African_Americans are an ethnic group .
direct_object A_Fortress_of_Grey_Ice language English_language A_Fortress_of_Grey_Ice is written in the English_language . It is from the United_States where one of the ethnic groups is African_Americans .
direct_object A_Fortress_of_Grey_Ice language English_language A_Fortress_of_Grey_Ice is written in the English_language and is from the United_States where one of the ethnic groups are Asian_Americans .
direct_object United_States leaderName Barack_Obama A_Severed_Wasp was written in the United_States where Barack_Obama is the leader and Asian_Americans are one of the ethnic groups .
juxtaposition Albany_,_Oregon isPartOf Benton_County_,_Oregon The city of Albany_,_Oregon is part of Benton_County_,_Oregon , in the United_States , where African_Americans are one of the ethnic groups .
juxtaposition Alpharetta_,_Georgia isPartOf Fulton_County_,_Georgia Alpharetta_,_Georgia is part of Fulton_County_,_Georgia in the United_States . The United_States is home to an ethnic group called African_Americans .
juxtaposition Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas , United_States . One of the ethnic groups in the United_States is Asian_Americans .
juxtaposition Angola_,_Indiana isPartOf Steuben_County_,_Indiana Angola_,_Indiana , is part of Steuben_County_,_Indiana in the United_States , where one of the ethnic groups , is Asian_Americans .
juxtaposition Atlanta isPartOf DeKalb_County_,_Georgia Most of Atlanta , United_States is part of DeKalb_County_,_Georgia . Native_Americans are an ethnic group in the United_States .
juxtaposition United_States capital Washington Amarillo_,_Texas is located within the United_States . The country where African_Americans are one of the ethnic groups and where the capital is Washington .
juxtaposition Angola_,_Indiana isPartOf Indiana Angola_,_Indiana is in Indiana , United_States . African_Americans are an ethnic group in the United_States .
juxtaposition United_States demonym Americans Americans inhabit the United_States and an ethnic group in that country are Asian_Americans . Austin_,_Texas is also located in the United_States .
juxtaposition United_States capital Washington Akron_,_Ohio , is in the United_States . The capital of the country is Washington and Asian_Americans are one of the ethnic groups in that country .
juxtaposition Albuquerque_,_New_Mexico leaderTitle "State_Senate" Albuquerque_,_New_Mexico is led by the "State_Senate" in the United_States . Native_Americans are an ethnic group in that country .
juxtaposition United_States capital Washington Auburn_,_Alabama is in the United_States ; where Native_Americans are one of the ethnic groups and where the capital is Washington .
juxtaposition United_States language English_language Albany_,_Oregon is in the United_States where English_language is the language spoken . Native_Americans are an ethnic group in the United_States .
juxtaposition United_States capital Washington One of the ethnic groups in the United_States are Native_Americans , the country is also the home of the dish Bacon_Explosion and has Washington as it ' s capitol city .
juxtaposition United_States leaderName Barack_Obama Barack_Obama leads the United_States . which is the country of the Bacon_Explosion . Native_Americans are an ethnic group in that country .
juxtaposition United_States leaderName John_Roberts The Bacon_Explosion originated in the United_States where John_Roberts is the leader and the Asian_Americans are an ethnic group .
juxtaposition Philippines language Arabic One of the languages in the Philippines is Arabic . Batchoy comes from there . Chinese_Filipino people are also from the Philippines .
juxtaposition Philippines language Philippine_English Philippine_English is the language of the Philippines , where Batchoy comes from and where the Igorot_people are an ethnic group .
juxtaposition Spain language Spanish_language Arròs_negre is from Spain , where the language is Spanish_language and Spaniards are the ethnic group .
juxtaposition Alcatraz_Versus_the_Evil_Librarians language English_language Alcatraz_Versus_the_Evil_Librarians ( written in English_language ) is from the United_States , where White_Americans are an ethnic group .
juxtaposition United_States capital Washington In the United_States one of the ethnic groups is African_Americans and the capital is Washington . A_Fortress_of_Grey_Ice is from the country .
juxtaposition United_States leaderName Barack_Obama A_Fortress_of_Grey_Ice is from the United_States where Barack_Obama is the leader and the African_Americans are one of the ethnic groups .
juxtaposition United_States capital Washington The United_States are home to Asian_Americans as well as where " A_Wizard_of_Mars " was published . Washington is the capital .
apposition Austin_,_Texas leaderTitle City_Manager Asian_Americans are an ethnic group in the United_States , where Austin_,_Texas is located . The leader of Austin_,_Texas has the title of the ' City_Manager '.
apposition United_States language English_language Albany_,_Georgia is in the United_States ; where English_language is spoken and African_Americans call home .
passive_voice A_Fortress_of_Grey_Ice language English_language Many Asian_Americans live in the United_States , the country where English_language is the language and where ' A_Fortress_of_Grey_Ice ' is from .
apposition United_States language English_language Alcatraz_Versus_the_Evil_Librarians is from The United_States where the English_language is spoken and one of the ethnic groups are the Native_Americans .
passive_voice Albany_,_Georgia isPartOf Dougherty_County_,_Georgia The Asian_Americans are an ethnic group within the United_States , which is the location of Albany_,_Georgia , part of Dougherty_County_,_Georgia .
passive_voice Amarillo_,_Texas isPartOf Randall_County_,_Texas Amarillo_,_Texas , is part of Randall_County_,_Texas , United_States , where Native_Americans are one of the ethnic groups .
apposition Austin_,_Texas leaderTitle City_Manager One of the ethnic groups living in the United_States are the Asian_Americans . The country is the location of Austin_,_Texas which is led by the City_Manager .
apposition United_States leaderTitle President_of_the_United_States The leader of the United_States is called the President_of_the_United_States . Native_Americans are one of the ethnic groups within the country which is also the location of Albuquerque_,_New_Mexico .
passive_voice Albany_,_Georgia isPartOf Dougherty_County_,_Georgia Albany_,_Georgia is part of Dougherty_County_,_Georgia , in the United_States , where Asian_Americans are one of the ethnic groups .
passive_voice United_States capital Washington Atlanta is in the United_States ; where the capital is Washington and African_Americans are one of the ethnic groups .
passive_voice United_States demonym Americans The United_States includes the ethnic group of Asian_Americans amongst the Americans living there . It is the location of Albuquerque_,_New_Mexico .
passive_voice United_States capital Washington The United_States contain the ethnic group of Asian_Americans , has the capital of Washington and is the location of Albany_,_Oregon .
passive_voice Auburn_,_Alabama isPartOf Alabama Auburn_,_Alabama is located in Alabama in the United_States . Native_Americans are one ethnic group in the United_States .
passive_voice United_States language English_language Albany_,_Oregon is in the United_States where African_Americans are an ethic group and English_language is spoken .
passive_voice Alcatraz_Versus_the_Evil_Librarians language English_language Written in English_language , Alcatraz_Versus_the_Evil_Librarians comes from the United_States . Native_Americans are one of the ethnic groups of the United_States .
passive_voice United_States capital Washington The United_States is home to Asian_Americans and the origin place of A_Fortress_of_Grey_Ice . Washington is the capital .
possessif Akron_,_Ohio isPartOf Summit_County_,_Ohio The United_States , which includes the ethnic group of Asian_Americans , is the location of Akron_,_Ohio , Summit_County_,_Ohio .
possessif Malaysia leaderName Abdul_Halim_of_Kedah An ethnic group in Malaysia which is led by Abdul_Halim_of_Kedah are the Malaysian_Chinese . Asam_pedas is a food found in Malaysia .
passive_voice Ayam_penyet region Malaysia Ayam_penyet comes from the Java region of Malaysia , where the Banyumasan_people live .
relative_subject Austin_,_Texas leaderTitle Mayor Asian_Americans are an ethnic group of the United_States and Austin_,_Texas , whose leader is the Mayor , is found here .
passive_voice United_States language English_language English_language is the language of the United_States which includes the ethnic group of Asian_Americans and is the location of Albany_,_Georgia .
relative_subject United_States language English_language Asian_Americans are an ethnic group of the United_States . It is an English_language speaking country and is where Albany_,_Georgia is located .
relative_subject Philippines language Philippine_Spanish In the Philippines ; the spoken language is Philippine_Spanish , Batchoy is eaten and one of the ethnic groups is the Ilocano_people .
relative_subject A_Severed_Wasp language English_language A_Severed_Wasp was written in English_language in the United_States , where African_Americans are an ethnic group .
relative_subject United_States leaderName Barack_Obama Barack_Obama , president of the United_States is an African_Americans man , which is one of the ethnic groups of the United_States . A_Wizard_of_Mars was published in the United_States .
relative_subject United_States demonym Americans The United_States , where Americans live , includes the ethnic group of African_Americans and is the location of Albuquerque_,_New_Mexico .
direct_object Malaysia leaderName Abdul_Halim_of_Kedah An ethnic group in Malaysia which is led by Abdul_Halim_of_Kedah are the Malaysian_Chinese . Asam_pedas is a food found in Malaysia .
existential United_States leaderName Barack_Obama A_Severed_Wasp originates from the United_States which has Barack_Obama as leader and includes many Asian_Americans .
coordinated_clauses Atlanta areaCode 404 Atlanta , which has the area code of 404 , has a population density of 1299.0_inhabitants_per_square_kilometre and a total area of 347.1_square_kilometres .
coordinated_clauses Attica_,_Indiana isPartOf United_States Part of the United_States , Attica_,_Indiana ( Indiana ) , has an area of 4.14_square_kilometres and a population density of 783.1_inhabitants_per_square_kilometre .
coordinated_full_clauses Atlantic_City_,_New_Jersey leaderTitle Clerk_(municipal_official) A Clerk_(municipal_official) is the leader of Atlantic_City_,_New_Jersey where the population density is 1421.2_inhabitants_per_square_kilometre and where the total are is 44.125_square_kilometres .
direct_object Attica_,_Indiana isPartOf United_States Attica_,_Indiana is part of the United_States . The population density of Attica_,_Indiana Indiana is 783.1_inhabitants_per_square_kilometre and its area is 4.14_square_kilometres .
direct_object Atlantic_City_,_New_Jersey leaderTitle Clerk_(municipal_official) A Clerk_(municipal_official) is the leader of Atlantic_City_,_New_Jersey where the population density is 1421.2_inhabitants_per_square_kilometre and where the total are is 44.125_square_kilometres .
existential Auburn_,_Alabama elevationAboveTheSeaLevel 214.0 Auburn_,_Alabama is 214.0 above sea level , covers an area of 140.8_square_kilometres and has 368.65_inhabitants_per_square_kilometre .
apposition Alpharetta_,_Georgia elevationAboveTheSeaLevel 346.0 Alpharetta_,_Georgia has a population density of 992.6_inhabitants_per_square_kilometre , is located at 346.0 above sea level and is 55.4_square_kilometres .
relative_subject Alexandria_,_Indiana elevationAboveTheSeaLevel 265.0 Alexandria_,_Indiana is located 265.0 above sea level , has a total area of 6.81_square_kilometres and a population density of 755.3_inhabitants_per_square_kilometre .
relative_subject Atlanta areaOfLand 344.9_square_kilometres With a total area of 347.1_square_kilometres , Atlanta , covers 344.9_square_kilometres and has a population density of 1299.0_inhabitants_per_square_kilometre .
relative_subject Tarrant_County_,_Texas country United_States Arlington_,_Texas in the United_States is part of Tarrant_County_,_Texas where the largest city is Fort_Worth_,_Texas .
relative_subject New_Jersey country United_States Atlantic_City_,_New_Jersey is a part of New_Jersey in the United_States . Newark_,_New_Jersey is the biggest city in New_Jersey .
apposition Texas country United_States In Texas , which is in the United_States , Houston is the largest city and Arlington_,_Texas is a smaller place .
apposition Michigan capital Lansing_,_Michigan Detroit is the largest city in Michigan but Lansing_,_Michigan is the capital and Ann_Arbor_,_Michigan is a notable city .
passive_voice Tarrant_County_,_Texas country United_States Arlington_,_Texas in the United_States is part of Tarrant_County_,_Texas where the largest city is Fort_Worth_,_Texas .
relative_adverb Texas language English_language In Texas ; Houston is the largest city , Arlington_,_Texas is part of the state and English_language is spoken .
passive_voice Atlantic_City country United_States Atlantic_City , New_Jersey is in the United_States . The largest city in New_Jersey is Newark_,_New_Jersey .
passive_voice New_Jersey country United_States Atlantic_City_,_New_Jersey is a part of New_Jersey in the United_States . Newark_,_New_Jersey is the biggest city in New_Jersey .
relative_adverb Attica_,_Indiana country United_States Attica_,_Indiana is in the United_States . the capital of Indiana is Indianapolis .
relative_adverb New_Jersey country United_States The capital of the state of New_Jersey in the United_States is Trenton_,_New_Jersey . Atlantic_City_,_New_Jersey is also part of that state .
possessif Washington_(state) country United_States Olympia_,_Washington is the state capital of Washington_(state) in the United_States . Auburn_,_Washington is also part of Washington_(state) .
possessif New_Jersey country United_States Atlantic_City_,_New_Jersey in the United_States . The capital of New_Jersey is Trenton_,_New_Jersey .
relative_object Indiana country United_States The city of Alexandria_,_Indiana is in Indiana United_States and the state capital is Indianapolis .
relative_object New_Jersey country United_States The capital of the state of New_Jersey in the United_States is Trenton_,_New_Jersey . Atlantic_City_,_New_Jersey is also part of that state .
direct_object Washington_(state) country United_States Olympia_,_Washington is the state capital of Washington_(state) in the United_States . Auburn_,_Washington is also part of Washington_(state) .
direct_object New_Jersey country United_States The capital of the state of New_Jersey in the United_States is Trenton_,_New_Jersey . Atlantic_City_,_New_Jersey is also part of that state .
passive_voice Texas largestCity Houston Arlington_,_Texas is part of Texas where the largest city is Houston and the capital is Austin_,_Texas .
apposition New_Jersey country United_States The capital of the state of New_Jersey in the United_States is Trenton_,_New_Jersey . Atlantic_City_,_New_Jersey is also part of that state .
relative_object Texas country United_States Abilene_,_Texas , is part of Texas ( United_States ) , where the capital city is Austin_,_Texas .
direct_object Albuquerque_City_Council leader Richard_Berry Albuquerque_,_New_Mexico , United_States , is led by the Albuquerque_City_Council led by Richard_Berry .
direct_object Orange_County_,_California isPartOf Greater_Los_Angeles_Area Led by the California_State_Assembly , Anaheim_,_California , is in Orange_County_,_California ( part of the Greater_Los_Angeles_Area ) , in the United_States .
coordinated_full_clauses New_Mexico_Senate leader John_Sánchez Albuquerque_,_New_Mexico is a city in New_Mexico_Senate of the United_States and is led by the New_Mexico_Senate where John_Sánchez is leader .
apposition United_States ethnicGroup African_Americans Austin_,_Texas is in the United_States ; where the City_Manager is the leader and African_Americans call home .
coordinated_full_clauses Anaheim_,_California areaCode 657 With a total area of 131.6_square_kilometres , Anaheim_,_California has a UTC offset of "-7" and the area code 657 .
existential Austin_,_Texas populationDensity 1296.65_inhabitants_per_square_kilometre With an area of 703.95_square_kilometres , Austin_,_Texas , has the area codes 512_and_737 and the population density of 1296.65_inhabitants_per_square_kilometre .
existential Anderson_,_Indiana populationDensity 523.9_inhabitants_per_square_kilometre Anderson_,_Indiana has an area of 107.43_square_kilometres , is elevated 268.0 above sea level and has a population density of 523.9_inhabitants_per_square_kilometre .
coordinated_clauses Alpharetta_,_Georgia populationDensity 992.6_inhabitants_per_square_kilometre Alpharetta_,_Georgia is elevated 346.0 above sea level and has a total area of 55.4_square_kilometres and 992.6_inhabitants_per_square_kilometre .
coordinated_clauses Attica_,_Indiana populationDensity 783.1_inhabitants_per_square_kilometre Attica_,_Indiana is elevated 166.0 m above sea level with a 4.14_square_kilometres area and has a 783.1_inhabitants_per_square_kilometre population density .
passive_voice Albany_,_Oregon country United_States The city of Albany_,_Oregon is part of both Linn_County_,_Oregon and Benton_County_,_Oregon , in the United_States .
passive_voice Atlanta country United_States Most of Atlanta is part of DeKalb_County_,_Georgia in the United_States state of Georgia .
passive_voice Indiana capital Indianapolis Angola_,_Indiana is a city in Indiana in the United_States . Indianapolis is the capital of Indiana .
relative_subject Albany_,_Oregon country United_States Albany_,_Oregon is a city in Linn_County_,_Oregon and part of Benton_County_,_Oregon in the United_States .
relative_subject Randall_County_,_Texas country United_States Amarillo_,_Texas , is part of both Potter_County_,_Texas and Randall_County_,_Texas , in the United_States .
relative_subject Michigan capital Lansing_,_Michigan Ann_Arbor_,_Michigan is part of Washtenaw_County_,_Michigan in Michigan ( capital : Lansing_,_Michigan ).
relative_subject Auburn_,_Washington country United_States Auburn_,_Washington is in Pierce_County_,_Washington , part of Washington_(state) in the United_States .
existential Adirondack_Regional_Airport cityServed Saranac_Lake_,_New_York Adirondack_Regional_Airport is located in Saranac_Lake_,_New_York Harrietstown_,_New_York United_States .
apposition Albany_,_Oregon country United_States The city of Albany_,_Oregon is part of both Linn_County_,_Oregon and Benton_County_,_Oregon , in the United_States .
apposition Potter_County_,_Texas country United_States Amarillo_,_Texas is part of Potter_County_,_Texas and Randall_County_,_Texas in the United_States .
apposition Michigan capital Lansing_,_Michigan Ann_Arbor_,_Michigan is part of Washtenaw_County_,_Michigan in Michigan ( capital : Lansing_,_Michigan ).
existential Asser_Levy_Public_Baths location New_York_City Asser_Levy_Public_Baths are located in New_York_City , Manhattan as is Brooklyn .
direct_object Texas largestCity Houston In Texas ; Houston is the largest city , Arlington_,_Texas is part of the state and English_language is spoken .
passive_voice Texas largestCity Houston In Texas ; Houston is the largest city , Arlington_,_Texas is part of the state and English_language is spoken .
passive_voice United_States ethnicGroup Native_Americans Native_Americans are one of the ethnic groups in the United_States , where Barack_Obama is leader and where Albuquerque_,_New_Mexico is located .
passive_voice 1634:_The_Ram_Rebellion mediaType E-book 1634:_The_Ram_Rebellion , written by Virginia_DeMarce , has the ISBN number "1-4165-2060-0" and is available as an E-book .
existential A_Loyal_Character_Dancer mediaType Hardcover A_Loyal_Character_Dancer was written by Qiu_Xiaolong . and was published in Hardcover . It has the ISBN number of "1-56947-301-3" .
passive_voice Above_the_Veil mediaType Hardcover Above_the_Veil , written by Garth_Nix is available in Hardcover and has the ISBN number "0-439-17685-9" .
apposition Aenir OCLC_number 45644811 Aenir was written by Garth_Nix and has the ISBN number "0-439-17684-0" as well as the OCLC number 45644811 .
coordinated_full_clauses A_Fortress_of_Grey_Ice mediaType "Print" A_Fortress_of_Grey_Ice , written by J._V._Jones , is available in "Print" with the ISBN number "0-7653-0633-6" .
direct_object 1634:_The_Bavarian_Crisis mediaType "Print" 1634:_The_Bavarian_Crisis was put into "Print" and written by Eric_Flint . It can be located by the ISBN number "978-1-4165-4253-7" .
apposition Aenir OCLC_number 45644811 Aenir was written by Garth_Nix , has an OCLC number of 45644811 and an ISBN number of "0-439-17684-0" .
relative_subject 1634:_The_Ram_Rebellion mediaType Hardcover 1634:_The_Ram_Rebellion , ISBN number "1-4165-2060-0" , was written by Eric_Flint and is available in Hardcover .
existential Aenir OCLC_number 45644811 Aenir , which was written by Garth_Nix has the OCLC number 45644811 and the ISBN number "0-439-17684-0" .
relative_subject A_Fortress_of_Grey_Ice OCLC_number 51969173 A_Fortress_of_Grey_Ice is written by J._V._Jones . It has a OCLC number of 51969173 and a ISBN number of "0-7653-0633-6" .
relative_subject Aenir OCLC_number 45644811 Aenir was written by Garth_Nix and has the ISBN number "0-439-17684-0" as well as the OCLC number 45644811 .
relative_subject 1634:_The_Ram_Rebellion mediaType E-book 1634:_The_Ram_Rebellion , written by Virginia_DeMarce , has the ISBN number "1-4165-2060-0" and is available as an E-book .
relative_subject A_Loyal_Character_Dancer OCLC_number 49805501 Written by Qiu_Xiaolong , A_Loyal_Character_Dancer has the ISBN number "1-56947-301-3" and the OCLC number 49805501 .
relative_subject A_Wizard_of_Mars OCLC_number 318875313 A_Wizard_of_Mars , authored by Diane_Duane has a OCLC number of 318875313 and a ISBN number of "978-0-15-204770-2" .
coordinated_clauses Aenir OCLC_number 45644811 Aenir , which was written by Garth_Nix has the OCLC number 45644811 and the ISBN number "0-439-17684-0" .
direct_object 1634:_The_Ram_Rebellion mediaType "Print" 1634:_The_Ram_Rebellion was published in "Print" and written by Virginia_DeMarce . The ISBN number is "1-4165-2060-0" .
apposition Above_the_Veil OCLC_number 46451790 Above_the_Veil , written by Garth_Nix , has a OCLC number of 46451790 and ISBN number "0-439-17685-9" .
existential 1634:_The_Bavarian_Crisis numberOfPages "448" Available in Hardcover , 1634:_The_Bavarian_Crisis is written by "Virginia_DeMarce_and_Eric_Flint" and has "448" pages .
existential A_Loyal_Character_Dancer ISBN_number "1-56947-301-3" A_Loyal_Character_Dancer was written by Qiu_Xiaolong . and was published in Hardcover . It has the ISBN number of "1-56947-301-3" .
passive_voice Above_the_Veil OCLC_number 46451790 Above_the_Veil , written by Garth_Nix , is available in Hardcover and has the OCLC number 46451790 .
passive_voice Aenir ISBN_number "0-439-17684-0" Aenir written by Garth_Nix is available in Paperback with the ISBN number "0-439-17684-0" .
passive_voice 1634:_The_Bavarian_Crisis numberOfPages "448" 1634:_The_Bavarian_Crisis by Eric_Flint comes in Hardcover and has "448" pages .
relative_subject 1634:_The_Ram_Rebellion ISBN_number "1-4165-2060-0" 1634:_The_Ram_Rebellion , written by Virginia_DeMarce , has the ISBN number "1-4165-2060-0" and is available as an E-book .
relative_subject A_Wizard_of_Mars OCLC_number 318875313 A_Wizard_of_Mars by Diane_Duane is in "Print" and has an OCLC number of 318875313 .
existential 1634:_The_Ram_Rebellion ISBN_number "1-4165-2060-0" Eric_Flint wrote 1634:_The_Ram_Rebellion found in Hardcover with an ISBN of "1-4165-2060-0" .
coordinated_full_clauses 1634:_The_Bavarian_Crisis numberOfPages "448" 1634:_The_Bavarian_Crisis , written by Eric_Flint , has "448" pages and is available in Hardcover .
coordinated_full_clauses 1634:_The_Ram_Rebellion numberOfPages "512" 1634:_The_Ram_Rebellion , written by Eric_Flint , is available in Paperback and is "512" pages long .
coordinated_full_clauses Above_the_Veil numberOfPages "248" Above_the_Veil is available in Hardcover , has "248" pages and was written by Garth_Nix .
coordinated_full_clauses Aenir numberOfPages "233" Garth_Nix is the author of the "233" page Paperback , Aenir .
relative_subject A_Wizard_of_Mars ISBN_number "978-0-15-204770-2" The Hardcover book . A_Wizard_of_Mars , was written by Diane_Duane and has the ISBN number "978-0-15-204770-2" .
coordinated_clauses 1634:_The_Ram_Rebellion ISBN_number "1-4165-2060-0" 1634:_The_Ram_Rebellion , written by Virginia_DeMarce , has the ISBN number "1-4165-2060-0" and is available as an E-book .
coordinated_clauses Aenir OCLC_number 45644811 The book Aenir , by Garth_Nix , is available in Paperback and has the OCLC number 45644811 .
possessif Eric_Flint birthPlace Burbank_,_California Eric_Flint , who was born in Burbank_,_California , is the author of 1634:_The_Bavarian_Crisis which was preceded by 1634:_The_Ram_Rebellion .
possessif John_Cowper_Powys birthPlace Shirley_,_Derbyshire A_Glastonbury_Romance , the sequel to Wolf_Solent was written by John_Cowper_Powys who was from Shirley_,_Derbyshire .
existential Eric_Flint influencedBy Robert_A._Heinlein 1634:_The_Bavarian_Crisis is preceded by 1634:_The_Baltic_War . The books were written by Eric_Flint who was influenced by Robert_A._Heinlein .
apposition Eric_Flint birthPlace Burbank_,_California The author , Eric_Flint , wrote 1634:_The_Ram_Rebellion and followed this with 1634:_The_Bavarian_Crisis . Eric_Flint was born in Burbank_,_California .
relative_subject Eric_Flint birthPlace Burbank_,_California The author , Eric_Flint , wrote 1634:_The_Ram_Rebellion and followed this with 1634:_The_Bavarian_Crisis . Eric_Flint was born in Burbank_,_California .
apposition Weymouth_Sands followedBy Maiden_Castle_(novel) Maiden_Castle_(novel) was preceded by Weymouth_Sands . The book " A_Glastonbury_Romance " was followed by " Weymouth_Sands ", preceded By Wolf_Solent and was written by John_Cowper_Powys .
existential 1634:_The_Bavarian_Crisis precededBy 1634:_The_Baltic_War 1634:_The_Bavarian_Crisis , written by Eric_Flint born in Burbank_,_California , was preceded by 1634:_The_Baltic_War and "DeMarce_short_stories_in_the_The_Grantville_Gazettes" .
relative_subject A_Glastonbury_Romance precededBy Wolf_Solent John_Cowper_Powys was born in Shirley_,_Derbyshire and wrote Wolf_Solent and A_Glastonbury_Romance .
direct_object 1634:_The_Bavarian_Crisis precededBy The_Grantville_Gazettes 1634:_The_Bavarian_Crisis which was preceded by 1634:_The_Baltic_War and was the sequel to The_Grantville_Gazettes was written by Eric_Flint who was born in Burbank_,_California .
apposition 1634:_The_Bavarian_Crisis precededBy 1634:_The_Baltic_War 1634:_The_Bavarian_Crisis and it ' s predecessor , 1634:_The_Baltic_War , was written by Eric_Flint who was born in Burbank_,_California .
relative_subject Alcatraz_Versus_the_Evil_Librarians followedBy Alcatraz_Versus_the_Scrivener's_Bones Alcatraz_Versus_the_Scrivener's_Bones , written after Alcatraz_Versus_the_Evil_Librarians was written by Brandon_Sanderson of Lincoln_,_Nebraska .
apposition 1634:_The_Bavarian_Crisis precededBy Grantville_Gazette_III 1634:_The_Bavarian_Crisis , which was preceded by Grantville_Gazette_III , was written by Eric_Flint , who was influenced by Robert_A._Heinlein .
direct_object 1634:_The_Bavarian_Crisis precededBy Grantville_Gazette_II Eric_Flint was influenced by Robert_A._Heinlein and is the author of 1634:_The_Bavarian_Crisis , that was preceded by Grantville_Gazette_II .
existential A_Severed_Wasp country United_States English_language is spoken in the United_States and Great_Britain . A_Severed_Wasp was published in the United_States .
existential A_Wizard_of_Mars country United_States The English_language is spoken in Great_Britain and the United_States ( the origin of A_Wizard_of_Mars ).
existential A_Loyal_Character_Dancer country United_States A_Loyal_Character_Dancer is published in the United_States and written in English_language , which is also spoken in Great_Britain .
passive_voice Association_for_Computing_Machinery leaderName Alexander_L._Wolf Alexander_L._Wolf is a leader of the Association_for_Computing_Machinery , headquarters in New_York_City , are the publishers of ACM_Transactions_on_Information_Systems .
apposition AIDS_(journal) ISSN_number "0269-9370" AIDS_(journal) was first published in 1987 and comes under the academic discipline of HIV . It has the ISSN number os "0269-9370" .
coordinated_clauses United_States language English_language A_Loyal_Character_Dancer is published by Soho_Press in the United_States . The language of the United_States is English_language .
coordinated_clauses United_States leaderName Barack_Obama A_Loyal_Character_Dancer was published by Soho_Press in the United_States which is lead by Barack_Obama .
relative_subject United_States ethnicGroup Asian_Americans A_Loyal_Character_Dancer is published by Soho_Press in the United_States . The United_States has an ethnic group called Asian_Americans .
relative_subject United_States leaderName Barack_Obama A_Loyal_Character_Dancer is published by Soho_Press in the United_States , which leader is Barack_Obama .
relative_subject United_States ethnicGroup African_Americans A_Loyal_Character_Dancer is published by Soho_Press in the United_States where African_Americans are an ethnic group .
passive_voice Johns_Hopkins_University_Press parentCompany Johns_Hopkins_University Johns_Hopkins_University is the parent company of Johns_Hopkins_University_Press in the United_States who publish the American_Journal_of_Mathematics .
existential A_Long_Long_Way followedBy The_Secret_Scripture Located in the United_States , Viking_Press published A_Long_Long_Way , the novel which was followed by The_Secret_Scripture .
passive_voice Addiction_(journal) LCCN_number 93645978 John_Wiley_&_Sons is the parent company of Wiley-Blackwell which in turn is the publisher of Addiction_(journal) which has the LCCN number 93645978 .
relative_subject A.T._Charlie_Johnson nationality United_States A.T._Charlie_Johnson from the United_States and studied at Harvard_University , is the editor of AIP_Advances .
relative_object AIP_Advances publisher American_Institute_of_Physics AIP_Advances publisher is American_Institute_of_Physics and his editor is A.T._Charlie_Johnson , his almaMater is Harvard_University .
apposition AIP_Advances publisher American_Institute_of_Physics Harvard_University is the alma mater of A.T._Charlie_Johnson , who is the editor of AIP_Advances , published by American_Institute_of_Physics .
relative_subject A.T._Charlie_Johnson nationality United_States A.T._Charlie_Johnson , a United_States national , is editor of AIP_Advances and doctoral advisor for Michael_Tinkham .
relative_subject A.T._Charlie_Johnson almaMater Harvard_University A.T._Charlie_Johnson from the United_States and studied at Harvard_University , is the editor of AIP_Advances .
relative_subject A.T._Charlie_Johnson almaMater Stanford_University A.T._Charlie_Johnson , of AIP_Advances , is from the United_States and has Stanford_University for an Alma mater .
existential A_Fortress_of_Grey_Ice mediaType "Print" The book " A_Fortress_of_Grey_Ice " by J._V._Jones can be found in "Print" and the OCLC number 51969173 .
existential Above_the_Veil mediaType "Print" Above_the_Veil by Garth_Nix , OCLC 46451790 , was produced in "Print" .
direct_object Above_the_Veil mediaType Hardcover Above_the_Veil by Garth_Nix is available in Hardcover with the OCLC number 46451790 .
apposition Aenir mediaType Paperback The book Aenir , by Garth_Nix , is available in Paperback and has the OCLC number 45644811 .
relative_subject A_Loyal_Character_Dancer ISBN_number "1-56947-301-3" Written by Qiu_Xiaolong , A_Loyal_Character_Dancer has the ISBN number "1-56947-301-3" and the OCLC number 49805501 .
relative_subject Above_the_Veil ISBN_number "0-439-17685-9" Above_the_Veil was written by Garth_Nix . It has an ISBN number of "0-439-17685-9" and a OCLC number of 46451790 .
relative_subject Aenir ISBN_number "0-439-17684-0" Aenir was written by Garth_Nix and has the ISBN number "0-439-17684-0" as well as the OCLC number 45644811 .
existential English_language spokenIn Great_Britain A_Loyal_Character_Dancer , published in the United_States , is written in English_language ( also spoken in Great_Britain ).
passive_voice United_States ethnicGroup White_Americans Alcatraz_Versus_the_Evil_Librarians ( written in English_language ) is from the United_States , where White_Americans are an ethnic group .
coordinated_clauses English_language spokenIn Great_Britain A_Loyal_Character_Dancer , published in the United_States , is written in English_language ( also spoken in Great_Britain ).
coordinated_clauses United_States ethnicGroup Asian_Americans A_Severed_Wasp is from the United_States and is written in the English_language . Asian_Americans are one of many ethnic groups in the United_States .
passive_voice A_Severed_Wasp OCLC_number 8805735 A_Severed_Wasp is in "Print" with the OCLC 8805735 and ISBN "0-374-26131-8" .
passive_voice Aenir author Garth_Nix Aenir written by Garth_Nix is available in Paperback with the ISBN number "0-439-17684-0" .
existential Alcatraz_Versus_the_Evil_Librarians OCLC_number 78771100 Published in Hardcover , Alcatraz_Versus_the_Evil_Librarians has the OCLC number 78771100 and the ISBN number "0-439-92550-9" .
existential A_Fortress_of_Grey_Ice author J._V._Jones A_Fortress_of_Grey_Ice by J._V._Jones was printed in Hardcover with the ISBN "0-7653-0633-6" .
apposition 1634:_The_Ram_Rebellion author Virginia_DeMarce 1634:_The_Ram_Rebellion was published in "Print" and written by Virginia_DeMarce . The ISBN number is "1-4165-2060-0" .
apposition A_Long_Long_Way numberOfPages "292" Available in Hardcover , A_Long_Long_Way is "292" pages long and has the ISBN number "0-670-03380-4" .
apposition A_Severed_Wasp numberOfPages "388" A_Severed_Wasp is a Hardcover book with "388" pages and the ISBN number "0-374-26131-8" .
apposition Alcatraz_Versus_the_Evil_Librarians OCLC_number 78771100 The book Alcatraz_Versus_the_Evil_Librarians is in "Print" and has the ISBN number "0-439-92550-9" as well as the OCLC number of 78771100 .
possessif A_Glastonbury_Romance mediaType Hardcover A_Glastonbury_Romance has "1174" pages and is available in Hardcover . It has the ISBN number "0-7156-3648-0" .
possessif A_Severed_Wasp mediaType Hardcover A_Severed_Wasp is a Hardcover book with "388" pages and the ISBN number "0-374-26131-8" .
relative_subject Alcatraz_Versus_the_Evil_Librarians mediaType Hardcover Alcatraz_Versus_the_Evil_Librarians is published in Hardcover and is "320" pages long . It has the ISBN number of "0-439-92550-9" .
coordinated_full_clauses A_Fortress_of_Grey_Ice author J._V._Jones J._V._Jones is the author of A_Fortress_of_Grey_Ice OCLC 51969173 ISBN "0-7653-0633-6" .
coordinated_full_clauses Alcatraz_Versus_the_Evil_Librarians mediaType "Print" The book Alcatraz_Versus_the_Evil_Librarians is in "Print" and has the ISBN number "0-439-92550-9" as well as an OCLC number of 78771100 .
relative_subject A_Loyal_Character_Dancer author Qiu_Xiaolong Written by Qiu_Xiaolong , A_Loyal_Character_Dancer has the ISBN number "1-56947-301-3" and the OCLC number 49805501 .
relative_subject A_Wizard_of_Mars author Diane_Duane A_Wizard_of_Mars , authored by Diane_Duane has a OCLC number of 318875313 and a ISBN number of "978-0-15-204770-2" .
relative_subject Alcatraz_Versus_the_Evil_Librarians mediaType Hardcover Published in Hardcover , Alcatraz_Versus_the_Evil_Librarians has the OCLC number 78771100 and the ISBN number "0-439-92550-9" .
existential A_Long_Long_Way numberOfPages "292" A_Long_Long_Way was printed in "Print__&_Paperback" . It has "292" pages and the OCLC number 57392246 .
direct_object Above_the_Veil author Garth_Nix Above_the_Veil by Garth_Nix is available in Hardcover with the OCLC number 46451790 .
coordinated_full_clauses A_Glastonbury_Romance numberOfPages "1174" A_Glastonbury_Romance has "1174" pages and is in "Print" with the OCLC number 76798317 .
coordinated_full_clauses Above_the_Veil author Garth_Nix Above_the_Veil by Garth_Nix is available in Hardcover with the OCLC number 46451790 .
coordinated_full_clauses Alcatraz_Versus_the_Evil_Librarians ISBN_number "0-439-92550-9" The book Alcatraz_Versus_the_Evil_Librarians is in "Print" and has the ISBN number "0-439-92550-9" as well as an OCLC number of 78771100 .
passive_voice Alcatraz_Versus_the_Evil_Librarians ISBN_number "0-439-92550-9" Published in Hardcover , Alcatraz_Versus_the_Evil_Librarians has the OCLC number 78771100 and the ISBN number "0-439-92550-9" .
relative_subject A_Wizard_of_Mars author Diane_Duane A_Wizard_of_Mars by Diane_Duane is in "Print" and has an OCLC number of 318875313 .
coordinated_clauses A_Glastonbury_Romance ISBN_number "0-7156-3648-0" A_Glastonbury_Romance , which can be found in Hardcover , has the ISBN number "0-7156-3648-0" as well as the OCLC number 76798317 .
coordinated_clauses Alcatraz_Versus_the_Evil_Librarians ISBN_number "0-439-92550-9" Alcatraz_Versus_the_Evil_Librarians is published in Hardcover , has the OCLC number of 78771100 and the ISBN number "0-439-92550-9" .
relative_subject A_Severed_Wasp numberOfPages "388" A_Severed_Wasp has "388" pages and can be found in "Print" with the OCLC number of 8805735 .
coordinated_clauses A_Fortress_of_Grey_Ice author J._V._Jones A_Fortress_of_Grey_Ice , by J._V._Jones , is in "Print" with OCLC number 51969173 .
coordinated_clauses A_Severed_Wasp ISBN_number "0-374-26131-8" The book " A_Severed_Wasp " is available in "Print" with the OCLC number of 8805735 and ISBN number of "0-374-26131-8" .
coordinated_clauses Alcatraz_Versus_the_Evil_Librarians genre Fantasy_literature Alcatraz_Versus_the_Evil_Librarians is a Fantasy_literature Hardcover book . The OCLC number is 78771100 .
relative_subject A_Glastonbury_Romance numberOfPages "1174" A_Glastonbury_Romance is in "Print" , has "1174" pages and the OCLC number 76798317 .
relative_subject Above_the_Veil author Garth_Nix Above_the_Veil by Garth_Nix is available in Hardcover with the OCLC number 46451790 .
direct_object A_Fortress_of_Grey_Ice author J._V._Jones The book " A_Fortress_of_Grey_Ice " by J._V._Jones can be found in "Print" and the OCLC number 51969173 .
passive_voice Alcatraz_Versus_the_Evil_Librarians ISBN_number "0-439-92550-9" The book Alcatraz_Versus_the_Evil_Librarians is in "Print" and has the ISBN number "0-439-92550-9" as well as an OCLC number of 78771100 .
apposition A_Severed_Wasp ISBN_number "0-374-26131-8" The book " A_Severed_Wasp " is available in "Print" with the OCLC number of 8805735 and ISBN number of "0-374-26131-8" .
possessif A_Severed_Wasp LibraryofCongressClassification "PS3523.E55_S4_1982" A_Severed_Wasp can be found in "Print" and has the OCLC number 8805735 and the Library of Congress Classification is "PS3523.E55_S4_1982" .
relative_subject Above_the_Veil language English_language Above_the_Veil is written in English_language by Garth_Nix and was preceded by Aenir .
relative_subject A_Glastonbury_Romance ISBN_number "0-7156-3648-0" A_Glastonbury_Romance is available in Hardcover and has "1174" pages . It can be found by ISBN number "0-7156-3648-0" .
direct_object 1634:_The_Bavarian_Crisis author "Virginia_DeMarce_and_Eric_Flint" 1634:_The_Bavarian_Crisis was written by "Virginia_DeMarce_and_Eric_Flint" , has "448" pages and is available in "Print" form .
direct_object A_Long_Long_Way ISBN_number "0-670-03380-4" A_Long_Long_Way , with "292" pages , is available in Hardcover . Its ISBN number is "0-670-03380-4" .
direct_object Aenir author Garth_Nix Garth_Nix is the author of the "233" page Paperback , Aenir .
direct_object Alcatraz_Versus_the_Evil_Librarians OCLC_number 78771100 Alcatraz_Versus_the_Evil_Librarians is published in Hardcover and has "320" pages . The OCLC number is 78771100 .
passive_voice 1634:_The_Bavarian_Crisis author Virginia_DeMarce 1634:_The_Bavarian_Crisis by Virginia_DeMarce is "448" pages long and in "Print" .
passive_voice A_Fortress_of_Grey_Ice author J._V._Jones A_Fortress_of_Grey_Ice is "672" pages , available in "Print" and authored by J._V._Jones .
passive_voice A_Glastonbury_Romance ISBN_number "0-7156-3648-0" A_Glastonbury_Romance is in "Print" and has "1174" pages and the ISBN number "0-7156-3648-0" .
passive_voice A_Long_Long_Way ISBN_number "0-670-03380-4" A_Long_Long_Way , with "292" pages , is available in Hardcover . Its ISBN number is "0-670-03380-4" .
relative_subject 1634:_The_Bavarian_Crisis author "Virginia_DeMarce_and_Eric_Flint" 1634:_The_Bavarian_Crisis was written by "Virginia_DeMarce_and_Eric_Flint" . It is available in Hardcover and has "448" pages .
direct_object Administrative_Science_Quarterly OCLC_number 1461102 The Administrative_Science_Quarterly , OCLC 1461102 , is published by SAGE_Publications founded by Sara_Miller_McCune .
relative_subject Viking_Press parentCompany Penguin_Random_House A_Long_Long_Way and it ' s sequel The_Secret_Scripture is published by Viking_Press , parent company Penguin_Random_House .
direct_object Aenir country Australians The book Aenir was written by Australians and Above_the_Veil followed up in English_language .
passive_voice Cornell_University affiliation Association_of_Public_and_Land-grant_Universities Cornell_University , in New_York is the publisher of the Administrative_Science_Quarterly as well as being affiliated with the Association_of_Public_and_Land-grant_Universities and the Association_of_American_Universities .
coordinated_clauses Aenir author Garth_Nix Aenir and the sequel Above_the_Veil were written in English_language by Garth_Nix .
relative_subject Weymouth_Sands author John_Cowper_Powys The author , John_Cowper_Powys , has written the books A_Glastonbury_Romance , Weymouth_Sands and Maiden_Castle_(novel) ( in that order ).
direct_object John_Cowper_Powys birthPlace Shirley_,_Derbyshire John_Cowper_Powys , born in Shirley_,_Derbyshire , is known for works such as Wolf_Solent and A_Glastonbury_Romance .
relative_subject 107_Camilla periapsis 479343000.0_kilometres 107_Camilla was discovered by N._R._Pogson , has an epoch 2006-12-31 and a periapsis of 479343000.0_kilometres .
existential (15788)_1993_SB periapsis 3997100000000.0 (15788)_1993_SB has an epoch 2006-03-06 , has a periapsis of 3997100000000.0 and it was discovered by the Roque_de_los_Muchachos_Observatory .
coordinated_full_clauses 107_Camilla periapsis 479343000.0_kilometres 107_Camilla was discovered by N._R._Pogson , has an epoch 2006-12-31 and a periapsis of 479343000.0_kilometres .
coordinated_clauses (410777)_2009_FD orbitalPeriod 39447000.0 (410777)_2009_FD has an epoch 2015-06-27 and an orbital period of 39447000.0 . It was discovered by Spacewatch .
coordinated_clauses 107_Camilla periapsis 479343000.0_kilometres 107_Camilla has a periapsis of 479343000.0_kilometres and an epoch 2006-12-31 . It was discovered by E._Wells .
existential (15788)_1993_SB periapsis 3997100000000.0 2006-03-06 Donal_O'Ceallaigh discovered (15788)_1993_SB , which has a periapsis of 3997100000000.0 . .
existential 107_Camilla periapsis 479343000.0_kilometres A._Storrs discovered 107_Camilla , which has a periapsis of 479343000.0_kilometres 2006-12-31 .
relative_subject 107_Camilla periapsis 479343000.0_kilometres N._R._Pogson discoverered 107_Camilla with its epoch date being 2006-12-31 . The periapsis of 107_Camilla is 479343000.0_kilometres .
apposition 107_Camilla periapsis 479343000.0_kilometres A._Storrs discovered 107_Camilla , which has a periapsis of 479343000.0_kilometres 2006-12-31 .
passive_voice 107_Camilla periapsis 479343000.0_kilometres A._Storrs discovered 107_Camilla , which has a periapsis of 479343000.0_kilometres 2006-12-31 .
coordinated_full_clauses 10_Hygiea surfaceArea 837080.744_square_kilometres The asteroid 10_Hygiea has a surface area of 837080.744_square_kilometres and an apoapsis of 523951582.33968_kilometres . Its average speed is 16.76_kilometres_per_sec .
existential (19255)_1994_VK8 density 2.0_grams_per_cubic_centimetre (19255)_1994_VK8 has an average speed of 4.56_kilometres_per_sec . It has a density of 2.0_grams_per_cubic_centimetre and an apoapsis of 6603633000.0_kilometres .
existential (66391)_1999_KW4 density 2.0_grams_per_cubic_centimetre The celestial body known as (66391)_1999_KW4 has an apoapsis of 162164091.8388_kilometres , an average speed of 37.16_kilometres_per_sec and a density of 2.0_grams_per_cubic_centimetre .
existential 11264_Claudiomaccone orbitalPeriod 1513.722_days The asteroid called 11264_Claudiomaccone ; has an average speed of 18.29_kilometres_per_sec , an apoapsis of 475426000.0_kilometres and an orbital period of 1513.722_days .
apposition 10_Hygiea surfaceArea 837080.744_square_kilometres The celestial body known as 10_Hygiea , has a surface area of 837080.744_square_kilometres and an average speed of 16.76_kilometres_per_sec . 10_Hygiea has an apoapsis of 523951582.33968_kilometres .
existential 1097_Vicia escapeVelocity 0.0112_kilometres_per_sec The celestial body known as 1097_Vicia has a temperature of 171.0_kelvins , an escape velocity of 0.0112_kilometres_per_sec and an apoapsis of 511592000.0_kilometres .
existential 109_Felicitas escapeVelocity 0.0473_kilometres_per_sec With an escape velocity of 0.0473_kilometres_per_sec , 109_Felicitas , has a temperature of 170.0_kelvins and an apoapsis of 523329000.0_kilometres .
relative_subject 1097_Vicia escapeVelocity 0.0112_kilometres_per_sec The celestial body known as 1097_Vicia has a temperature of 171.0_kelvins , an escape velocity of 0.0112_kilometres_per_sec and an apoapsis of 511592000.0_kilometres .
relative_subject 109_Felicitas escapeVelocity 0.0473_kilometres_per_sec With an escape velocity of 0.0473_kilometres_per_sec , 109_Felicitas , has a temperature of 170.0_kelvins and an apoapsis of 523329000.0_kilometres .
relative_subject 1101_Clematis escapeVelocity 0.02_kilometres_per_sec With a temperature of 155.0_kelvins , 1101_Clematis , has an escape velocity of 0.02_kilometres_per_sec and an apoapsis of 520906000.0_kilometres .
passive_voice 1101_Clematis orbitalPeriod 183309000.0 1101_Clematis , which has a mass of 5.7_kilograms and an orbital period of 183309000.0 , has an epoch 2006-12-31 .
coordinated_full_clauses 110_Lydia orbitalPeriod 142603000.0 2006-12-31 is the epoch of 110_Lydia , which has a mass of 6.7_kilograms and an orbital period of 142603000.0 .
coordinated_full_clauses 109_Felicitas apoapsis 523329000.0_kilometres The epoch date of 109_Felicitas was 2006-12-31 , has an orbital period of 139705000.0 and its apoapsis is 523329000.0_kilometres .
possessif 1097_Vicia formerName "1928_PC" "1928_PC" is the former name of 1097_Vicia , which has an epoch 2006-12-31 and an orbital period of 135589000.0 .
possessif 1101_Clematis formerName "1928_SJ" 1101_Clematis has an orbital period of 183309000.0 and an epoch 2006-12-31 . "1928_SJ" is the former name of 1101_Clematis .
apposition 109_Felicitas apoapsis 523329000.0_kilometres 109_Felicitas has an epoch 2006-12-31 . Its orbital period is is 139705000.0 and its apoapsis is 523329000.0_kilometres .
relative_subject (19255)_1994_VK8 apoapsis 6603633000.0_kilometres (19255)_1994_VK8 ; has an orbital period of 8788850000.0 , an apoapsis of 1994 VK8 is 6603633000.0_kilometres and the epoch 2006-12-31 .
relative_subject (66391)_1999_KW4 periapsis 29919600000.0 (66391)_1999_KW4 has an epoch 2004-07-14 . It ' s orbital period is 16244700.0 and has a periapsis of 29919600000.0 .
relative_subject 109_Felicitas apoapsis 523329000.0_kilometres The epoch date of 109_Felicitas was 2006-12-31 , has an orbital period of 139705000.0 and its apoapsis is 523329000.0_kilometres .
relative_subject 110_Lydia periapsis 377016000000.0 110_Lydia ; has a periapsis measurement of 377016000000.0 , an orbital period of 142603000.0 and its epoch is 2006-12-31 .
coordinated_clauses (19255)_1994_VK8 apoapsis 6603633000.0_kilometres 2006-12-31 is the epoch date of (19255)_1994_VK8 . it has an orbital period of 8788850000.0 and an apoapsis of 6603633000.0_kilometres .
coordinated_clauses (29075)_1950_DA periapsis 124950000000.0 The celestial body known as (29075)_1950_DA has an orbital period of 69862200.0 , a periapsis of 124950000000.0 and an epoch 2011-Aug-27 .
coordinated_clauses 1089_Tama apoapsis 373513000.0_kilometres The epoch of 1089_Tama is 2005-11-26 . It has an orbital period of 1202.846_days and and an apoapsis of 373513000.0_kilometres .
coordinated_clauses 1097_Vicia mass 9.8_kilograms 1097_Vicia has a mass of 9.8_kilograms , the epoch date 2006-12-31 and an orbital period of 135589000.0 .
coordinated_clauses 110_Lydia apoapsis 440756000.0_kilometres The celestial body known as 110_Lydia has an orbital period of 142603000.0 . It has an apoapsis of 440756000.0_kilometres and an epoch 2006-12-31 .
existential (29075)_1950_DA periapsis 124950000000.0 The celestial body known as (29075)_1950_DA has an orbital period of 69862200.0 , a periapsis of 124950000000.0 and an epoch 2011-Aug-27 .
existential (66391)_1999_KW4 escapeVelocity 0 (66391)_1999_KW4 has an epoch 2004-07-14 . It has an escape velocity of 0 and an orbital period of 16244700.0 .
possessif (15788)_1993_SB discoverer Donal_O'Ceallaigh Donal_O'Ceallaigh discovered (15788)_1993_SB and it has an epoch 2006-03-06 . It ' s periapsis is 3997100000000.0 .
possessif 1001_Gaussia formerName "1923_OAA907_XC" The periapsis of the 1001_Gaussia ( formally known as "1923_OAA907_XC" ) is 419113394.55312_kilometres and it has an epoch date of 2015-06-27 .
coordinated_full_clauses (66391)_1999_KW4 apoapsis 162164091.8388_kilometres The epoch date for (66391)_1999_KW4 is 2004-07-14 . It has a periapsis of 29919600000.0 and an apoapsis measurement of 162164091.8388_kilometres .
relative_subject (19255)_1994_VK8 apoapsis 6603633000.0_kilometres The epoch of (19255)_1994_VK8 is 2006-12-31 . It has a periapsis of 6155910000000.0 and an apoapsis of 6603633000.0_kilometres .
relative_subject 107_Camilla discoverer A._Storrs Discovered by A._Storrs , 107_Camilla , has an epoch 2006-12-31 and the periapsis of 479343000.0_kilometres .
relative_subject 1099_Figneria apoapsis 605718000.0_kilometres The epoch date for 1099_Figneria is 2006-12-31 . It has an apoapsis of 605718000.0_kilometres and a periapsis of 349206000000.0 .
relative_subject 11264_Claudiomaccone orbitalPeriod 1513.722_days 11264_Claudiomaccone has the epoch 2005-11-26 , an orbital period of 1513.722_days and a periapsis of 296521000.0_kilometres .
relative_subject (19255)_1994_VK8 apoapsis 6603633000.0_kilometres The epoch of (19255)_1994_VK8 is 2006-12-31 and it has a periapsis of 6155910000000.0 with an apoapsis of 6603633000.0_kilometres .
existential 1001_Gaussia formerName "1923_OAA907_XC" 1001_Gaussia , which was formerly called "1923_OAA907_XC" , has an epoch date of 2015-06-27 and a periapsis of 419113394.55312_kilometres .
existential 107_Camilla discoverer M._Gaffey M._Gaffey discovered 107_Camilla , which has the epoch 2006-12-31 and a periapsis of 479343000.0_kilometres .
existential 103_Hera apoapsis 437170000.0_kilometres 103_Hera has an epoch 2011-08-27 , has a periapsis of 371240000.0_kilometres and an apoapsis of 437170000.0_kilometres .
existential 1089_Tama formerName "A894_VA;_A904_VD;" The 1089_Tama ( formally known as "A894_VA;_A904_VD;" ) has a periapsis of 288749000000.0 and an epoch date of the 2005-11-26 .
apposition 103_Hera temperature 170.0_kelvins The celestial body known as 103_Hera has a temperature of 170.0_kelvins . It has has an escape velocity of 0.0482_kilometres_per_sec and an apoapsis of 437170000.0_kilometres .
apposition 103_Hera mass 7.9_kilograms 103_Hera has an escape velocity of 0.0482_kilometres_per_sec , an apoapsis of 437170000.0_kilometres and a mass of 7.9_kilograms .
apposition 1099_Figneria temperature 156.0_kelvins The celestial body known as 1099_Figneria has a temperature of 156.0_kelvins , an apoapsis of 605718000.0_kilometres and an escape velocity of 0.0155_kilometres_per_sec .
apposition 109_Felicitas mass 7.5_kilograms The 109_Felicitas with a mass of 7.5_kilograms , has an apoapsis of 523329000.0_kilometres and an escape velocity of 0.0473_kilometres_per_sec .
apposition 109_Felicitas temperature 170.0_kelvins With a temperature of 170.0_kelvins , the asteroid called 109_Felicitas , has an escape velocity of 0.0473_kilometres_per_sec and an apoapsis of 523329000.0_kilometres .
existential 1089_Tama temperature 179.0_kelvins 1089_Tama has an escape velocity of 0.0068_kilometres_per_sec , a temperature of 179.0_kelvins and an apoapsis of 373513000.0_kilometres .
existential 109_Felicitas temperature 170.0_kelvins With an escape velocity of 0.0473_kilometres_per_sec , 109_Felicitas , has a temperature of 170.0_kelvins and an apoapsis of 523329000.0_kilometres .
existential 1101_Clematis temperature 155.0_kelvins The celestial body known as 1101_Clematis has an escape velocity of 0.02_kilometres_per_sec , a temperature of 155.0_kelvins and an apoapsis of 520906000.0_kilometres .
relative_subject 1097_Vicia temperature 171.0_kelvins The temperature of 1097_Vicia is 171.0_kelvins , has an escape velocity of 0.0112_kilometres_per_sec and its apoapsis is 511592000.0_kilometres .
relative_subject 1099_Figneria temperature 156.0_kelvins The celestial body known as 1099_Figneria has a temperature of 156.0_kelvins , an apoapsis of 605718000.0_kilometres and an escape velocity of 0.0155_kilometres_per_sec .
coordinated_clauses 101_Helena escapeVelocity 0.0348_kilometres_per_sec 101_Helena ; has a mass of 3.0_kilograms , an escape velocity of 0.0348_kilometres_per_sec and an apoapsis of 441092000.0_kilometres .
coordinated_clauses 103_Hera escapeVelocity 0.0482_kilometres_per_sec 103_Hera has an escape velocity of 0.0482_kilometres_per_sec , an apoapsis of 437170000.0_kilometres and a mass of 7.9_kilograms .
relative_subject 11264_Claudiomaccone apoapsis 475426000.0_kilometres 11264_Claudiomaccone has the epoch 2005-11-26 , an apoapsis of 475426000.0_kilometres and a rotation period of 11473.9 .
coordinated_full_clauses (66063)_1998_RO1 epoch 2013-11-04 The mean temperature of (66063)_1998_RO1 is 265.0_kelvins . It has an apoapsis of 254989570.60815_kilometres and an epoch 2013-11-04 .
coordinated_clauses (410777)_2009_FD apoapsis 259776702.47055_kilometres (410777)_2009_FD has an orbital period of 39447000.0 , an apoapsis of 259776702.47055_kilometres and it was discovered by the Spacewatch .
passive_voice 107_Camilla epoch 2006-12-31 C._Woods discovered 107_Camilla , which has a periapsis of 479343000.0_kilometres and the epoch 2006-12-31 .
passive_voice 107_Camilla epoch 2006-12-31 N._R._Pogson discovered 107_Camilla , which has an epoch 2006-12-31 and a periapsis of 479343000.0_kilometres .
existential 1001_Gaussia orbitalPeriod 5.75_days 1001_Gaussia , which was formerly known as "1923_OAA907_XC" , has an epoch date of 2015-06-27 and an orbital period of 5.75_days .
existential 1099_Figneria orbitalPeriod 179942000.0 1099_Figneria ( formerly known as "1928_RQ" ) has an epoch 2006-12-31 . It has an orbital period of 179942000.0 .
apposition 1101_Clematis orbitalPeriod 183309000.0 1101_Clematis has an orbital period of 183309000.0 and an epoch 2006-12-31 . "1928_SJ" is the former name of 1101_Clematis .
coordinated_full_clauses (66063)_1998_RO1 periapsis 41498400000.0 (66063)_1998_RO1 ( formerly called "1999_SN5" ) has an epoch date of 2013-11-04 and its periapsis is 41498400000.0 Gm .
possessif 1001_Gaussia periapsis 419113394.55312_kilometres The periapsis of the 1001_Gaussia ( formally known as "1923_OAA907_XC" ) is 419113394.55312_kilometres and it has an epoch date of 2015-06-27 .
apposition (66063)_1998_RO1 meanTemperature 265.0_kelvins The mean temperature of (66063)_1998_RO1 is 265.0_kelvins . It has an apoapsis of 254989570.60815_kilometres and its epoch is on the 2013-11-04 .
apposition 11264_Claudiomaccone orbitalPeriod 1513.722_days 11264_Claudiomaccone has the epoch 2005-11-26 , an orbital period of 1513.722_days and its apoapsis is 475426000.0_kilometres .
direct_object 1089_Tama periapsis 288749000000.0 The epoch date of 1089_Tama is 2005-11-26 . It has a periapsis of 288749000000.0 and 373513000.0_kilometres is the apoapsis .
existential 101_Helena periapsis 331683000.0_kilometres 101_Helena ; has an apoapsis of 441092000.0_kilometres , a periapsis of 331683000.0_kilometres . and epoch date of , 2006-12-31 .
existential 1099_Figneria orbitalPeriod 179942000.0 1099_Figneria ; has an apoapsis of 605718000.0_kilometres , an orbital period of 179942000.0 and an epoch 2006-12-31 .
existential 110_Lydia orbitalPeriod 142603000.0 The celestial body known as 110_Lydia has an orbital period of 142603000.0 . It has an apoapsis of 440756000.0_kilometres and an epoch 2006-12-31 .
existential 11264_Claudiomaccone rotationPeriod 11473.9 11264_Claudiomaccone has the epoch 2005-11-26 , an apoapsis of 475426000.0_kilometres and a rotation period of 11473.9 .
direct_object James_Craig_Watson deathCause Peritonitis 103_Hera was discovered by James_Craig_Watson whose alma mater was the University_of_Michigan . He died of Peritonitis .
relative_subject James_Craig_Watson stateOfOrigin Canada James_Craig_Watson , originally from Canada , discovered 101_Helena before dying of Peritonitis .
existential James_Craig_Watson deathPlace Madison_,_Wisconsin 103_Hera was discovered by James_Craig_Watson , who died of Peritonitis in Madison_,_Wisconsin .
passive_voice James_Craig_Watson deathPlace Madison_,_Wisconsin James_Craig_Watson , who discovered 103_Hera , died in Madison_,_Wisconsin of Peritonitis .
existential Walter_Baade almaMater University_of_Göttingen Walter_Baade , who attended the University_of_Göttingen discovered 1036_Ganymed . He died in West_Germany ,.
passive_voice 101_Helena apoapsis 441092000.0_kilometres James_Craig_Watson , who died in Madison_,_Wisconsin , discovered 101_Helena , which has an apoapsis of 441092000.0_kilometres .
coordinated_full_clauses Walter_Baade almaMater University_of_Göttingen Walter_Baade , who attended the University_of_Göttingen discovered 1036_Ganymed . He died in West_Germany ,.
possessif James_Craig_Watson deathPlace Madison_,_Wisconsin James_Craig_Watson , who discovered 103_Hera , came from Canada and died in Madison_,_Wisconsin .
passive_voice N._R._Pogson deathPlace Chennai Nottingham born N._R._Pogson , who died in Chennai , discovered 107_Camilla .
existential Walter_Baade almaMater University_of_Göttingen Walter_Baade , who discovered 1036_Ganymed , studied at the University_of_Göttingen and had Allan_Sandage as a doctoral student .
existential 1097_Vicia epoch 2006-12-31 1097_Vicia has the epoch 2006-12-31 , an orbital period of 135589000.0 and an apoapsis of 511592000.0_kilometres .
existential 110_Lydia epoch 2006-12-31 The celestial body known as 110_Lydia has an orbital period of 142603000.0 . It has an apoapsis of 440756000.0_kilometres and an epoch 2006-12-31 .
direct_object 10_Hygiea escapeVelocity 0.21_kilometres_per_sec 10_Hygiea has an apoapsis of 523951582.33968_kilometres , an escape velocity of 0.21_kilometres_per_sec and a surface are of 837080.744_square_kilometres .
relative_subject Alan_Bean was_selected_by_NASA 1963 Alan_Bean ( born on "1932-03-15" ) was selected by NASA in 1963 and now is "Retired" .
existential Elliot_See birthPlace Dallas Elliot_See ( "Deceased" ) was born in Dallas "1927-07-23" .
passive_voice Elliot_See almaMater University_of_Texas_at_Austin Elliot_See , a United_States national born in Dallas , was a student at University_of_Texas_at_Austin .
passive_voice Netherlands leaderName Mark_Rutte Stellendam born Ab_Klink is a national of the Netherlands , where Mark_Rutte is the leader .
passive_voice Netherlands currency Euro Born in Stellendam , Ab_Klink is a national of the Netherlands where the Euro is the currency .
passive_voice Buzz_Aldrin was_a_crew_member_of Apollo_11 Buzz_Aldrin was a United_States national , being born in Glen_Ridge_,_New_Jersey and was a member of the Apollo_11 crew .
relative_subject Ace_Wilder background "solo_singer" Ace_Wilder , a "solo_singer" and a Songwriter , was born in Sweden .
existential Alan_Bean timeInSpace 100305.0_minutes The Test_pilot Alan_Bean , who spent 100305.0_minutes in space , was born in Wheeler_,_Texas .
relative_subject William_Anders was_selected_by_NASA 1963 William_Anders was born in British_Hong_Kong and his occupation was a Fighter_pilot when selected by NASA in 1963 .
apposition Alan_Shepard birthDate "1923-11-18" Alan_Shepard , who served as a Test_pilot , was born in New_Hampshire on the "1923-11-18" .
passive_voice William_Anders dateOfRetirement "1969-09-01" William_Anders , a Fighter_pilot from British_Hong_Kong , retired "1969-09-01" .
possessif Buzz_Aldrin timeInSpace 52_minutes On the NASA operated Apollo_11 program , crew member Buzz_Aldrin spent 52_minutes in space .
passive_voice Alan_Bean almaMater "UT_Austin_,_B.S._1955" United_States citizen Alan_Bean graduated from "UT_Austin_,_B.S._1955" and is now "Retired" .
relative_subject William_Anders birthPlace British_Hong_Kong William_Anders , born in British_Hong_Kong , was a Fighter_pilot a member of Apollo_8 ' s crew .
relative_subject William_Anders birthPlace British_Hong_Kong William_Anders ( born in British_Hong_Kong ) served as a Fighter_pilot and was a crew member of Apollo_8 .
coordinated_clauses William_Anders birthPlace British_Hong_Kong William_Anders ( born in British_Hong_Kong ) served as a Fighter_pilot and was a crew member of Apollo_8 .
existential Adam_Holloway militaryBranch Grenadier_Guards Adam_Holloway was born in Kent , graduated from Magdalene_College_,_Cambridge and served in the Grenadier_Guards .
existential Agnes_Kant office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Agnes_Kant was born in Hessisch_Oldendorf , studied at Radboud_University_Nijmegen and worked as a office workedAt workedAsObjet1 .
direct_object Ab_Klink party Christian_Democratic_Appeal Ab_Klink was born in the Netherlands , belongs to the Christian_Democratic_Appeal party and graduated from Erasmus_University_Rotterdam .
possessif Ab_Klink party Christian_Democratic_Appeal Stellendam was the birthplace of Ab_Klink who belongs to the Christian_Democratic_Appeal party . Ab_Klink ' s alma mater is Leiden_University .
juxtaposition Adam_Holloway militaryBranch Grenadier_Guards Born in "Faversham_,_Kent_,_England" , Adam_Holloway , served in the Grenadier_Guards and his alma mater was Magdalene_College_,_Cambridge .
relative_subject Agnes_Kant office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Agnes_Kant was born in West_Germany , attended Radboud_University_Nijmegen and later worked as a office workedAt workedAsObjet1 .
possessif Erasmus_University_Rotterdam affiliation Association_of_MBAs Erasmus_University_Rotterdam is affiliated to Association_of_MBAs and is the alma mater of Ab_Klink who was born in Stellendam .
passive_voice Ab_Klink party Christian_Democratic_Appeal Born in Stellendam , Ab_Klink , whose alma mater is Leiden_University , belongs to the Christian_Democratic_Appeal party .
coordinated_full_clauses Agnes_Kant office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Agnes_Kant was born in West_Germany and was a office workedAt workedAsObjet1 . He attended the Radboud_University_Nijmegen .
relative_object Adam_Holloway militaryBranch Grenadier_Guards Adam_Holloway , who as in the Grenadier_Guards , was born in Kent . His Alma mater is Magdalene_College_,_Cambridge .
relative_subject Adam_Holloway militaryBranch Grenadier_Guards Adam_Holloway was born in Kent , graduated from Magdalene_College_,_Cambridge and served in the Grenadier_Guards .
relative_subject 1036_Ganymed discoverer Walter_Baade 1036_Ganymed was discovered by German_Empire born , Walter_Baade , a graduate from University_of_Göttingen .
possessif Agnes_Kant office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Agnes_Kant was born in Hessisch_Oldendorf , studied at Radboud_University_Nijmegen and worked as a office workedAt workedAsObjet1 .
existential William_Anders was_selected_by_NASA 1963 William_Anders was born in British_Hong_Kong , graduated from "AFIT_,_M.S._1962" in 1962 and joined NASA in 1963 .
apposition Adam_Holloway militaryBranch Grenadier_Guards Faversham born Adam_Holloway , who studied at Magdalene_College_,_Cambridge , served in the army in the Grenadier_Guards .
apposition Alan_Bean was_a_crew_member_of Apollo_12 Alan_Bean has "Retired" , having been born in Wheeler_,_Texas and serving on the crew of Apollo_12 .
relative_adverb Alan_Shepard timeInSpace 13017_minutes Alan_Shepard was born in New_Hampshire , the home of the Purple_finch and spent 13017_minutes in space .
passive_voice Alan_Shepard timeInSpace 13017_minutes Alan_Shepard ( born in New_Hampshire ) was a crew member of Apollo_14 and spent 13017_minutes in space .
passive_voice William_Anders occupation Fighter_pilot William_Anders ( born in British_Hong_Kong ) was a part of Apollo_8 and served as Fighter_pilot .
coordinated_clauses Alan_Bean status "Retired" Now "Retired" , Alan_Bean was born "1932-03-15" and hired by NASA in 1963 .
coordinated_full_clauses Aleksandr_Chumakov birthYear 1948 Aleksandr_Chumakov was born in the Soviet_Union , in 1948 . Aleksandr_Chumakov ' s club is the Soviet_Union_national_football_team .
coordinated_clauses Aaron_Boogaard birthYear 1986 Aaron_Boogaard , who played for Wichita_Thunder club , was born in 1986 in Regina_,_Saskatchewan .
coordinated_clauses Alex_Plante height 1.9304 Alex_Plante was born in Brandon_,_Manitoba and is 1.9304 m tall . He plays for the Anyang_Halla club .
possessif Alex_Plante height 1.9304 Alex_Plante , who is 1.9304 m in height and was born in Manitoba , has played for the club Anyang_Halla .
relative_subject Aaron_Boogaard height 1.905 Aaron_Boogaard was born in Canada and is 1.905 in height and is a member of the Wichita_Thunder .
apposition Aaron_Hunt youthclub youthHamburger_SV Aaron_Hunt plays for VfL_Wolfsburg and Hamburger_SV as well as belonging to the youth club youthHamburger_SV .
apposition Esteghlal_Ahvaz_FC manager Adnan_Hamad Ahmad_Kadhim_Assad , played for the club PAS_Tehran_FC and is part of the Esteghlal_Ahvaz_FC Adnan_Hamad is the manager of Esteghlal_Ahvaz_FC .
apposition Alaa_Abdul-Zahra birthPlace Iraq Iraq born , Alaa_Abdul-Zahra , is a member of the clubs , Al_Kharaitiyat_SC and Sanat_Mes_Kerman_FC
apposition Aleksandre_Guruli height 178.0_centimetres At 178.0_centimetres tall , Aleksandre_Guruli , played for the Georgia_national_football_team and plays for FC_Karpaty_Lviv club .
apposition 1_FC_Köln manager Peter_Stöger Peter_Stöger is the manager of 1_FC_Köln , he plays for the Austria_national_football_team and FK_Austria_Wien also .
apposition 1_FC_Magdeburg manager Jens_Härtel Jens_Härtel has been the manager of 1_FC_Magdeburg and plays for both 1_FC_Lokomotive_Leipzig and SV_Germania_Schöneiche .
apposition AC_Lumezzane manager Michele_Marcolini Michele_Marcolini manages the AC_Lumezzane , plays for AC_Chievo_Verona and previously played for Vicenza_Calcio .
apposition ACF_Fiorentina manager Paulo_Sousa Paulo_Sousa is the manager of the ACF_Fiorentina . He plays for Maccabi_Tel_Aviv_FC and is also a member of the Portugal_national_football_team .
apposition AEK_Athens_FC manager Gus_Poyet AEK_Athens_FC manager is Gus_Poyet who ' s club is Real_Zaragoza , having previously played for Chelsea_FC .
apposition AZ_Alkmaar manager John_van_den_Brom John_van_den_Brom plays for both Vitesse_Arnhem and Jong_Ajax . He has also managed AZ_Alkmaar .
coordinated_clauses SV_Werder_Bremen manager Viktor_Skrypnyk Aaron_Hunt has played for , Viktor_Skrypnyk managed , SV_Werder_Bremen and Hamburger_SV .
coordinated_clauses United_Petrotrin_FC ground Palo_Seco Akeem_Adams played for W_Connection_FC and is a member of the United_Petrotrin_FC club . which play in Palo_Seco .
coordinated_clauses Alaa_Abdul-Zahra birthPlace Iraq Iraq born , Alaa_Abdul-Zahra , belongs to the clubs , Al_Kharaitiyat_SC and Sanat_Mes_Kerman_FC
coordinated_clauses Accrington_Stanley_FC ground Accrington Footballer , Alan_Martin_(footballer) , plays for both , Hamilton_Academical_FC and Accrington_Stanley_FC . The latter , played in Accrington .
coordinated_clauses FC_Spartak_Moscow ground Otkrytiye_Arena Aleksandr_Prudnikov plays for FC_Anzhi_Makhachkala and also for FC_Spartak_Moscow who play at the Otkrytiye_Arena .
coordinated_clauses 1_FC_Köln manager Peter_Stöger Peter_Stöger is the manager of 1_FC_Köln and plays for both SC_Wiener_Neustadt and the Austria_national_football_team .
coordinated_clauses 1_FC_Magdeburg manager Jens_Härtel Jens_Härtel has played for SV_Babelsberg_03 and 1_FC_Union_Berlin . He is manager of 1_FC_Magdeburg .
coordinated_clauses AC_Lumezzane manager Michele_Marcolini Michele_Marcolini manages the AC_Lumezzane , plays for AC_Chievo_Verona and previously played for Vicenza_Calcio .
coordinated_clauses AZ_Alkmaar manager John_van_den_Brom John_van_den_Brom manages the AZ_Alkmaar as well as playing for the ADO_Den_Haag and the De_Graafschap .
coordinated_clauses AZ_Alkmaar manager John_van_den_Brom John_van_den_Brom plays for Vitesse_Arnhem and in the Jong_Ajax club . He is also the manager of AZ_Alkmaar .
coordinated_clauses 1_FC_Köln manager Peter_Stöger Peter_Stöger ( football ) , First_Vienna_FC , now a manager of 1_FC_Köln , played for SK_Vorwärts_Steyr .
possessif Aaron_Hunt youthclub youthGoslarer_SC_08 Aaron_Hunt played for SV_Werder_Bremen_II and currently plays for Goslarer_SC_08 while belonging to the youth club youthGoslarer_SC_08 .
direct_object Abel_Hernández youthclub youthCentral_Español Abel_Hernández , was a member of youthCentral_Español ' s youthclub and now belongs to the clubs , Central_Español and the Uruguay_national_football_team .
possessif Alan_Martin_(footballer) birthDate 1989-01-01 Alan_Martin_(footballer) was born 1989-01-01 and previously played football for Accrington_Stanley_FC and currently with Crewe_Alexandra_FC .
possessif Clyde_FC ground Broadwood_Stadium Alan_Martin_(footballer) played football for Clyde_FC based at the Broadwood_Stadium and now plays for the Scotland_national_under-19_football_team .
possessif 1_FC_Magdeburg manager Jens_Härtel Jens_Härtel is part of the SV_Babelsberg_03 club , played for Berliner_AK_07 and manages 1_FC_Magdeburg .
possessif AC_Chievo_Verona manager Rolando_Maran Rolando_Maran has managed AC_Chievo_Verona and plays for both FC_Bari_1908 and Carrarese_Calcio .
possessif AC_Lumezzane manager Michele_Marcolini Michele_Marcolini manages the AC_Lumezzane , plays for FC_Bari_1908 and once played for Vicenza_Calcio .
possessif AZ_Alkmaar manager John_van_den_Brom John_van_den_Brom manages the AZ_Alkmaar and is in the Jong_Ajax club . He currently plays for İstanbulspor_AŞ .
possessif Aaron_Hunt youthclub youthGoslarer_SC_08 Aaron_Hunt is in the youth club youthGoslarer_SC_08 and also plays for the clubs Hamburger_SV and Goslarer_SC_08 .
existential Adam_Maher birthPlace Netherlands Netherlands born Adam_Maher ' s former clubs include PSV_Eindhoven and AZ_Alkmaar .
possessif Orange_County_Blues_FC manager Oliver_Wyss Akeem_Priestley plays for both the Dayton_Dutch_Lions and the Orange_County_Blues_FC . The manager of the Orange_County_Blues_FC is Oliver_Wyss .
existential Olympique_Lyonnais ground Parc_Olympique_Lyonnais Aleksandre_Guruli plays for FC_Karpaty_Lviv club and the Olympique_Lyonnais club . The latter club plays its home games at the Parc_Olympique_Lyonnais .
possessif Aleksandre_Guruli height 178.0_centimetres Aleksandre_Guruli , who is 178.0_centimetres tall , played for FC_Karpaty_Lviv and FC_Dinamo_Batumi .
possessif United_Petrotrin_FC ground Palo_Seco_Velodrome Akeem_Adams plays for T&TEC_Sports_Club and is a member of the United_Petrotrin_FC club playing at the Palo_Seco_Velodrome .
possessif 1_FC_Magdeburg manager Jens_Härtel Jens_Härtel has been the manager of 1_FC_Magdeburg . He first played for 1_FC_Lokomotive_Leipzig and later for Berliner_AK_07 .
possessif AC_Cesena manager Massimo_Drago Massimo_Drago , manager of AC_Cesena , is part of clubs Vigor_Lamezia and Calcio_Catania .
possessif AC_Lumezzane manager Michele_Marcolini Michele_Marcolini has been manager of AC_Lumezzane he is part of AC_Chievo_Verona and plays for Vicenza_Calcio .
possessif AC_Lumezzane manager Michele_Marcolini Michele_Marcolini has been manager of AC_Lumezzane and plays for both Torino_FC and Vicenza_Calcio .
possessif AFC_Blackpool manager Stuart_Parker_(footballer) Stuart_Parker_(footballer) was a footballer for Chesterfield_FC and now plays for Bury_FC and is the manager at AFC_Blackpool .
possessif AZ_Alkmaar manager John_van_den_Brom John_van_den_Brom has been manager of AZ_Alkmaar , he plays for both ADO_Den_Haag and De_Graafschap .
possessif AZ_Alkmaar manager John_van_den_Brom John_van_den_Brom is the manager of AZ_Alkmaar . He plays for Vitesse_Arnhem and ADO_Den_Haag .
possessif AS_Roma manager Luciano_Spalletti Luciano_Spalletti is a manager for the AS_Roma club and plays for Empoli_FC . C . as well as Virtus_Entella .
possessif 1_FC_Köln manager Peter_Stöger Peter_Stöger is in the SK_Vorwärts_Steyr club , is manager of 1_FC_Köln and is affiliated with the SC_Wiener_Neustadt club .
coordinated_full_clauses United_Petrotrin_FC ground Palo_Seco Akeem_Adams played for United_Petrotrin_FC whose home is Palo_Seco grounds . He now plays with Ferencvárosi_TC .
coordinated_full_clauses Al_Kharaitiyat_SC ground Al_Khor Alaa_Abdul-Zahra plays for Al_Kharaitiyat_SC . whose base is Al_Khor . He also played for the Duhok_SC .
coordinated_full_clauses FC_Spartak_Moscow ground Otkrytiye_Arena Aleksandr_Prudnikov ' s club is FC_Spartak_Moscow which is at the Otkrytiye_Arena . He also plays for the FC_Alania_Vladikavkaz FC .
coordinated_full_clauses Aleksandre_Guruli height 178.0_centimetres Standing 178.0_centimetres tall , Aleksandre_Guruli plays for FC_Dinamo_Batumi . His club is FC_Karpaty_Lviv .
coordinated_full_clauses AC_Milan manager Siniša_Mihajlović Alessio_Romagnoli plays in the club AC_Milan whose manager is Siniša_Mihajlović . He ' s alos in the Italy_national_under-16_football_team .
coordinated_full_clauses AC_Cesena manager Massimo_Drago Massimo_Drago has been the manager of AC_Cesena whose club is the SS_Chieti_Calcio . and played for ASD_SS_Nola_1925 .
relative_object Adam_Maher birthPlace Diemen Adam_Maher ' s birthplace is Diemen and he belongs to the clubs PSV_Eindhoven and AZ_Alkmaar .
relative_object Akeem_Priestley youthclub youthAntigua_GFC Akeem_Priestley ' s youth club was youthAntigua_GFC . He now plays for the Dayton_Dutch_Lions and the Antigua_GFC .
relative_object Accrington_Stanley_FC ground Accrington Alan_Martin_(footballer) ' s football club is Motherwell_FC and he has also played for the Accrington based club Accrington_Stanley_FC .
relative_object Aleksandre_Guruli height 178.0_centimetres Aleksandre_Guruli ' s height is 178.0_centimetres , his club is FC_Karpaty_Lviv and he is a member of the Georgia_national_football_team .
possessif FC_Spartak_Moscow ground Otkrytiye_Arena FC_Kuban_Krasnodar club member , Aleksandr_Prudnikov , plays for FC_Spartak_Moscow , the home ground of which , is Otkrytiye_Arena .
relative_object AFC_Blackpool manager Stuart_Parker_(footballer) AFC_Blackpool have had Stuart_Parker_(footballer) as their manager , he was also a footballer for Chesterfield_FC and Runcorn_FC_Halton .
relative_object AC_Cesena manager Massimo_Drago Massimo_Drago ' s club is the SS_Chieti_Calcio , he also manages AC_Cesena and was once a player for Castrovillari_Calcio .
direct_object Aaron_Hunt youthclub youthSV_Werder_Bremen Aaron_Hunt plays for VfL_Wolfsburg and SV_Werder_Bremen . He was a member of the youthSV_Werder_Bremen youth team .
direct_object Accrington_Stanley_FC ground Accrington Footballer , Alan_Martin_(footballer) , plays for both , Hamilton_Academical_FC and Accrington_Stanley_FC . The latter , played in Accrington .
direct_object Aleksandre_Guruli height 178.0_centimetres At 178.0_centimetres tall , Aleksandre_Guruli , played for the Georgia_national_football_team and plays for FC_Karpaty_Lviv club .
direct_object FC_Spartak_Moscow ground Otkrytiye_Arena Aleksandr_Prudnikov plays for FC_Anzhi_Makhachkala and also for FC_Spartak_Moscow who play at the Otkrytiye_Arena .
direct_object AEK_Athens_FC manager Gus_Poyet AEK_Athens_FC manager is Gus_Poyet who ' s club is Real_Zaragoza , having previously played for Chelsea_FC .
direct_object AZ_Alkmaar manager John_van_den_Brom John_van_den_Brom plays for RSC_Anderlecht and İstanbulspor_AŞ . He is also manager of AZ_Alkmaar .
direct_object 1_FC_Köln manager Peter_Stöger The manager of 1_FC_Köln is Peter_Stöger , he has played for SC_Wiener_Neustadt and is in the SK_Vorwärts_Steyr club .
possessif Orange_County_Blues_FC manager Oliver_Wyss Akeem_Priestley plays for Sheikh_Russel_KC and also Orange_County_Blues_FC managed by Oliver_Wyss .
possessif Aleksandr_Prudnikov birthDate 1989-02-24 Born , 1989-02-24 , Aleksandr_Prudnikov , plays for FC_Spartak_Moscow and FC_Tom_Tomsk .
existential Aaron_Hunt youthclub youthGoslarer_SC_08 Aaron_Hunt has played football for youthGoslarer_SC_08 youth team , SV_Werder_Bremen_II senior team and also Goslarer_SC_08 .
existential Aaron_Hunt youthclub youthSV_Werder_Bremen_II Aaron_Hunt played for SV_Werder_Bremen_II and currently plays for VfL_Wolfsburg while belonging to the youth club youthSV_Werder_Bremen_II ..
existential Aleksandre_Guruli height 178.0_centimetres At 178.0_centimetres tall , Aleksandre_Guruli , played for the Georgia_national_football_team and plays for FC_Karpaty_Lviv club .
coordinated_clauses SV_Werder_Bremen_II manager Alexander_Nouri Aaron_Hunt has played for Hamburger_SV and for SV_Werder_Bremen_II , which was managed by Alexander_Nouri .
coordinated_clauses Esteghlal_Ahvaz_FC manager Adnan_Hamad Ahmad_Kadhim_Assad played for the club Al-Quwa_Al-Jawiya and for Esteghlal_Ahvaz_FC , which is managed by Adnan_Hamad .
coordinated_clauses Orange_County_Blues_FC manager Oliver_Wyss Akeem_Priestley , who played for Antigua_GFC , plays for the Orange_County_Blues_FC , which is managed by , Oliver_Wyss .
coordinated_clauses Alan_Martin_(footballer) birthDate 1989-01-01 Alan_Martin_(footballer) was born 1989-01-01 . plays for Crewe_Alexandra_FC and Aldershot_Town_FC
coordinated_clauses Italy_national_under-16_football_team coach Daniele_Zoratto Alessio_Romagnoli , who plays for the club U.C._Sampdoria , played in the Italy_national_under-16_football_team which was coached by , Daniele_Zoratto .
coordinated_clauses PSV_Eindhoven manager Phillip_Cocu Adam_Maher belongs to the clubs , AZ_Alkmaar and PSV_Eindhoven . The latter is managed by , Phillip_Cocu .
passive_voice 1_FC_Magdeburg manager Jens_Härtel Jens_Härtel has been the manager of 1_FC_Magdeburg and is part of the clubs Berliner_AK_07 and 1_FC_Union_Berlin .
passive_voice AC_Lumezzane manager Michele_Marcolini Michele_Marcolini is at the Torino_FC club . He has been manager of AC_Lumezzane and was previously at FC_Bari_1908 .
coordinated_clauses AS_Livorno_Calcio manager Christian_Panucci AS_Livorno_Calcio are managed by Christian_Panucci who plays for Inter_Milan and previously for football for Genoa_CFC .
passive_voice AS_Roma manager Luciano_Spalletti Luciano_Spalletti manages AS_Roma and also plays for Udinese_Calcio and Virtus_Entella .
passive_voice AC_Cesena manager Massimo_Drago Attached to the club Vigor_Lamezia , Massimo_Drago manages AC_Cesena and once played for the club SSD_Potenza_Calcio .
relative_subject Aaron_Hunt youthclub youthSV_Werder_Bremen_II Aaron_Hunt played for SV_Werder_Bremen_II and currently plays for VfL_Wolfsburg while belonging to the youth club youthSV_Werder_Bremen_II ..
relative_subject Esteghlal_Ahvaz_FC manager Adnan_Hamad Ahmad_Kadhim_Assad , played for the club PAS_Tehran_FC and is part of the Esteghlal_Ahvaz_FC Adnan_Hamad is the manager of Esteghlal_Ahvaz_FC .
relative_subject Akeem_Priestley youthclub youthDayton_Dutch_Lions Akeem_Priestley is in the youthDayton_Dutch_Lions youth club and plays for the Jamaica_national_football_team in the Dayton_Dutch_Lions .
relative_subject Al_Kharaitiyat_SC ground Al_Khor Alaa_Abdul-Zahra plays for his club Al_Kharaitiyat_SC , located in Al_Khor and for the Iraq_national_under-23_football_team .
relative_subject Aleksandr_Prudnikov birthDate 1989-02-24 Born , 1989-02-24 , Aleksandr_Prudnikov , plays for FC_Spartak_Moscow and FC_Tom_Tomsk .
relative_subject United_Petrotrin_FC ground Palo_Seco_Velodrome Akeem_Adams plays for T&TEC_Sports_Club and is a member of the United_Petrotrin_FC club playing at the Palo_Seco_Velodrome .
relative_subject 1_FC_Köln manager Peter_Stöger Peter_Stöger plays for SC_Wiener_Neustadt and FK_Austria_Wien while also being a manager at 1_FC_Köln .
relative_subject AC_Lumezzane manager Michele_Marcolini Michele_Marcolini plays for both Atalanta_BC and AC_Chievo_Verona and has been manager of AC_Lumezzane .
relative_subject AS_Livorno_Calcio manager Christian_Panucci AS_Livorno_Calcio is managed by Christian_Panucci . is was attached to the Real_Madrid_CF club . He is currently attached to the club Genoa_CFC .
relative_subject AZ_Alkmaar manager John_van_den_Brom John_van_den_Brom is the manager of AZ_Alkmaar and plays for AFC_Ajax and Jong_Ajax .
relative_subject AZ_Alkmaar manager John_van_den_Brom John_van_den_Brom has been manager of AZ_Alkmaar and plays for RSC_Anderlecht . He previously played for the Netherlands_national_football_team .
relative_subject AS_Roma manager Luciano_Spalletti Luciano_Spalletti played for Empoli_FC and Virtus_Entella . He has also been manager of AS_Roma .
relative_subject AC_Cesena manager Massimo_Drago Massimo_Drago has been the manager of AC_Cesena but currently plays for Castrovillari_Calcio and SS_Chieti_Calcio .
coordinated_full_clauses Abel_Hernández youthclub youthU.S._Città_di_Palermo Abel_Hernández , who was a member of the youth club youthU.S._Città_di_Palermo , plays for the Uruguay_national_football_team and the U.S._Città_di_Palermo club .
coordinated_full_clauses Akeem_Adams birthPlace "Point_Fortin_,_Trinidad_and_Tobago" Akeem_Adams was born in "Point_Fortin_,_Trinidad_and_Tobago" . He is a member of Central_FC and Ferencvárosi_TC .
coordinated_full_clauses Aleksandre_Guruli height 178.0_centimetres Aleksandre_Guruli , who is 178.0_centimetres tall , played for the FC_Karpaty_Lviv FC and the AS_Lyon-Duchère FC .
coordinated_full_clauses FC_Samtredia ground Erosi_Manjgaladze_Stadium Aleksandre_Guruli played for FC_Dinamo_Batumi and FC_Samtredia . Erosi_Manjgaladze_Stadium is the ground of FC_Samtredia .
existential United_Petrotrin_FC ground Palo_Seco Akeem_Adams has played for Central_FC and United_Petrotrin_FC , the latter of which play at Palo_Seco .
coordinated_full_clauses 1_FC_Köln manager Peter_Stöger Peter_Stöger plays for SC_Wiener_Neustadt and FK_Austria_Wien while also being a manager at 1_FC_Köln .
coordinated_full_clauses 1_FC_Magdeburg manager Jens_Härtel Jens_Härtel has been the manager of 1_FC_Magdeburg and is part of the clubs Berliner_AK_07 and 1_FC_Union_Berlin .
coordinated_full_clauses AC_Chievo_Verona manager Rolando_Maran Rolando_Maran has managed AC_Chievo_Verona and is a member of the Calcio_Catania . He currently plays at the Carrarese_Calcio .
coordinated_full_clauses AC_Lumezzane manager Michele_Marcolini The manager of AC_Lumezzane is Michele_Marcolini who was at FC_Bari_1908 but currently plays for AC_Chievo_Verona .
coordinated_full_clauses AC_Lumezzane manager Michele_Marcolini Michele_Marcolini has been manager of AC_Lumezzane and plays for both Torino_FC and Vicenza_Calcio .
coordinated_full_clauses AFC_Blackpool manager Stuart_Parker_(footballer) The manager of AFC_Blackpool is Stuart_Parker_(footballer) who is with Drogheda_United_FC and plays for Runcorn_FC_Halton .
coordinated_full_clauses AFC_Fylde manager Dave_Challinor Dave_Challinor played for Stockport_County_FC , is at Colwyn_Bay_FC and manages AFC_Fylde .
coordinated_full_clauses ACF_Fiorentina manager Paulo_Sousa Paulo_Sousa is the manager of the ACF_Fiorentina . He has played for Maccabi_Tel_Aviv_FC and is a member of the Portugal_national_football_team .
coordinated_full_clauses AZ_Alkmaar manager John_van_den_Brom John_van_den_Brom manages the AZ_Alkmaar and is in the Jong_Ajax club . He currently plays for İstanbulspor_AŞ .
coordinated_full_clauses AZ_Alkmaar manager John_van_den_Brom John_van_den_Brom , the manager of AZ_Alkmaar , is in Vitesse_Arnhem and plays for De_Graafschap .
coordinated_full_clauses AC_Cesena manager Massimo_Drago Massimo_Drago was once a player for Castrovillari_Calcio and has been the manager of AC_Cesena . He was also at the club ASD_Licata_1931 .
coordinated_full_clauses AC_Cesena manager Massimo_Drago Massimo_Drago manages AC_Cesena and is a former player of Vigor_Lamezia and SSD_Potenza_Calcio .
relative_subject Abner_(footballer) birthDate 1996-05-30 Abner_(footballer) , born 1996-05-30 , plays football for the C.D._FAS club and also Real_Madrid_Castilla .
relative_subject Esteghlal_Ahvaz_FC manager Adnan_Hamad Ahmad_Kadhim_Assad is part of the Esteghlal_Ahvaz_FC in the Iraq_national_football_team . Esteghlal_Ahvaz_FC manages Adnan_Hamad .
direct_object AC_Milan manager Siniša_Mihajlović Alessio_Romagnoli played for the Italy_national_under-16_football_team . He plays for the club AC_Milan which is managed by , Siniša_Mihajlović .
direct_object Italy_national_under-16_football_team coach Daniele_Zoratto Alessio_Romagnoli , who plays for the club U.C._Sampdoria , played in the Italy_national_under-16_football_team which was coached by , Daniele_Zoratto .
relative_subject 1_FC_Magdeburg manager Jens_Härtel Jens_Härtel is part of the SV_Babelsberg_03 club . He has also been manager of 1_FC_Magdeburg and represented FC_Sachsen_Leipzig .
direct_object AC_Lumezzane manager Michele_Marcolini The manager of AC_Lumezzane is Michele_Marcolini who was at FC_Bari_1908 but currently plays for AC_Chievo_Verona .
relative_subject AFC_Blackpool manager Stuart_Parker_(footballer) Stuart_Parker_(footballer) was at Drogheda_United_FC . He is a member of the Chesterfield_FC and now manages AFC_Blackpool .
direct_object AZ_Alkmaar manager John_van_den_Brom John_van_den_Brom , who is a member of the Jong_Ajax club and plays for Vitesse_Arnhem , has been manager of AZ_Alkmaar .
relative_subject AC_Cesena manager Massimo_Drago Attached to the club Vigor_Lamezia , Massimo_Drago manages AC_Cesena and once played for the club SSD_Potenza_Calcio .
apposition Alaa_Abdul-Zahra birthPlace Iraq Iraq is the birth place of Alaa_Abdul-Zahra , who played for Duhok_SC . Alaa_Abdul-Zahra plays for the Iraq_national_under-23_football_team .
apposition Aleksandre_Guruli height 178.0_centimetres Standing 178.0_centimetres tall , Aleksandre_Guruli plays for FC_Dinamo_Batumi . His club is FC_Karpaty_Lviv .
apposition VfL_Wolfsburg manager Dieter_Hecking Aaron_Hunt ' s clubs are Goslarer_SC_08 and also VfL_Wolfsburg managed by Dieter_Hecking .
apposition AC_Chievo_Verona manager Rolando_Maran Rolando_Maran is attached to the club Calcio_Catania , plays at Carrarese_Calcio and is manager of AC_Chievo_Verona .
apposition AD_Isidro_Metapán manager Jorge_Humberto_Rodríguez Jorge_Humberto_Rodríguez has been manager of AD_Isidro_Metapán and is a member of the club FC_Dallas , as well as of the El_Salvador_national_football_team .
apposition AFC_Blackpool manager Stuart_Parker_(footballer) Stuart_Parker_(footballer) plays for Blackburn_Rovers_FC manages AFC_Blackpool and is a member of Chesterfield_FC .
apposition AFC_Fylde manager Dave_Challinor Dave_Challinor is affiliated with Tranmere_Rovers_FC , plays for Colwyn_Bay_FC and is manager of AFC_Fylde .
coordinated_clauses Aaron_Hunt youthclub youthHamburger_SV Aaron_Hunt , who played youth football for youthHamburger_SV , later played for Hamburger_SV and SV_Werder_Bremen_II .
coordinated_clauses Hamilton_Academical_FC ground Hamilton_,_South_Lanarkshire Alan_Martin_(footballer) plays football for Hamilton_Academical_FC that is in Hamilton_,_South_Lanarkshire . He also played with the Accrington_Stanley_FC
coordinated_clauses 1_FC_Magdeburg manager Jens_Härtel Jens_Härtel is part of the SV_Babelsberg_03 club , played for Berliner_AK_07 and manages 1_FC_Magdeburg .
coordinated_clauses AFC_Blackpool manager Stuart_Parker_(footballer) AFC_Blackpool have had Stuart_Parker_(footballer) as their manager . He has also represented the club KV_Mechelen and once played for Stockport_County_FC .
existential Aaron_Hunt youthclub youthGermany_national_youth_football_team Aaron_Hunt , who played for Hamburger_SV , belongs to the youthclub youthGermany_national_youth_football_team and plays for the Germany_national_youth_football_team .
existential Aaron_Hunt youthclub youthHamburger_SV Aaron_Hunt , who plays for VfL_Wolfsburg , has previously payed for both , the youthHamburger_SV youth team and Hamburger_SV .
existential Akeem_Adams birthPlace "Point_Fortin_,_Trinidad_and_Tobago" Akeem_Adams , who was born in "Point_Fortin_,_Trinidad_and_Tobago" , played for Ferencvárosi_TC and T&TEC_Sports_Club .
existential Akeem_Priestley youthclub youthDayton_Dutch_Lions Akeem_Priestley , who currently plays for the Jamaica_national_football_team and the Dayton_Dutch_Lions played for the youth club youthDayton_Dutch_Lions .
relative_subject AC_Milan manager Siniša_Mihajlović Alessio_Romagnoli is a member of AS_Roma and plays for AC_Milan under their manager Siniša_Mihajlović .
existential AC_Milan manager Siniša_Mihajlović Alessio_Romagnoli , who is connected to U.C._Sampdoria , plays for AC_Milan which , is managed by , Siniša_Mihajlović .
existential Real_Madrid_Castilla manager Luis_Miguel_Ramis Abner_(footballer) plays for Coritiba_Foot_Ball_Club and also Real_Madrid_Castilla which is managed by Luis_Miguel_Ramis .
relative_subject 1_FC_Magdeburg manager Jens_Härtel Jens_Härtel ( Berliner_AK_07 ) played for FC_Sachsen_Leipzig , has been the manager of 1_FC_Magdeburg .
existential AS_Livorno_Calcio manager Christian_Panucci Christian_Panucci , who manages AS_Livorno_Calcio , used to play for both AS_Roma and Real_Madrid_CF .
relative_subject ACF_Fiorentina manager Paulo_Sousa Paulo_Sousa is the manager of the ACF_Fiorentina but played for Maccabi_Tel_Aviv_FC . His club is Juventus_FC .
passive_voice Alaa_Abdul-Zahra birthPlace Iraq Iraq born Alaa_Abdul-Zahra ' s club is Al_Kharaitiyat_SC and he plays for the Iraq_national_under-23_football_team .
possessif VfL_Wolfsburg manager Dieter_Hecking Aaron_Hunt played for SV_Werder_Bremen and also VfL_Wolfsburg , the latter of which is managed by Dieter_Hecking .
possessif Peñarol manager Jorge_Orosmán_da_Silva Uruguay_Olympic_football_team member Abel_Hernández plays for the club Peñarol , which is managed by Jorge_Orosmán_da_Silva .
possessif Esteghlal_Ahvaz_FC manager Adnan_Hamad Ahmad_Kadhim_Assad , who formerly played for Esteghlal_Ahvaz_FC , plays for the Iraq_national_football_team which is managed by Adnan_Hamad .
possessif Alaa_Abdul-Zahra birthPlace Iraq Alaa_Abdul-Zahra was born in Iraq and has played for Al-Wakrah_Sport_Club and Tractor_Sazi_FC .
passive_voice FC_Samtredia ground Erosi_Manjgaladze_Stadium Aleksandre_Guruli plays for both FC_Dinamo_Batumi and FC_Samtredia . The latter , play their home games at the Erosi_Manjgaladze_Stadium .
passive_voice SV_Werder_Bremen manager Viktor_Skrypnyk Viktor_Skrypnyk is the manager of SV_Werder_Bremen , the club Aaron_Hunt ( who plays for Hamburger_SV ) , used to play for .
apposition Esteghlal_Ahvaz_FC manager Adnan_Hamad Ahmad_Kadhim_Assad played for the club Al-Quwa_Al-Jawiya and for Esteghlal_Ahvaz_FC , which is managed by Adnan_Hamad .
passive_voice Alan_Martin_(footballer) birthDate 1989-01-01 Alan_Martin_(footballer) , born 1989-01-01 , plays for the club , Crewe_Alexandra_FC . He has also played for Aldershot_Town_FC .
apposition AC_Milan manager Siniša_Mihajlović Alessio_Romagnoli plays for the Italy_national_under-17_football_team and for for AC_Milan , which is managed by Siniša_Mihajlović .
apposition Alessio_Romagnoli birthPlace Italy Alessio_Romagnoli , born in Italy , plays for the Italy_national_under-19_football_team and is connected to U.C._Sampdoria .
passive_voice Real_Madrid_Castilla manager Luis_Miguel_Ramis Footballer , Abner_(footballer) plays for the club , Real_Madrid_Castilla and belongs to C.D._FAS club . Luis_Miguel_Ramis manages the Real_Madrid_Castilla .
coordinated_full_clauses Al-Zawra'a_SC manager Basim_Qasim Ahmad_Kadhim_Assad ' s clubs are Esteghlal_Ahvaz_FC and also Al-Zawra'a_SC which is managed by Basim_Qasim .
passive_voice Olympique_Lyonnais ground Parc_Olympique_Lyonnais Aleksandre_Guruli ' s clubs are US_Boulogne and also Olympique_Lyonnais which has it ' s home ground at the Parc_Olympique_Lyonnais stadium .
possessif United_Petrotrin_FC ground Palo_Seco Akeem_Adams club is Central_FC and he has also played for United_Petrotrin_FC who are based at Palo_Seco .
coordinated_full_clauses Adam_McQuaid birthYear 1986 Adam_McQuaid , who weighs 94.8024_(kilograms) kg , was born in Canada in 1986 .
possessif FC_Torpedo_Moscow manager Valery_Petrakov Aleksandr_Chumakov has played for FC_Torpedo_Moscow , where the manager is Valery_Petrakov and the chairman is Aleksandr_Tukmanov .
coordinated_clauses Houston_Texans city Houston Akeem_Dent , who made his debut with the Atlanta_Falcons , used to play for the Houston_Texans based in the city of Houston .
coordinated_full_clauses Atlanta_Falcons city Atlanta Akeem_Dent ' s first team was the Atlanta based Atlanta_Falcons . He later played for the Houston_Texans .
coordinated_full_clauses Akeem_Ayers draftRound "2" Akeem_Ayers , whose former team was Tennessee_Titans , was in draft pick "39" , in draft round "2" .
possessif Akeem_Ayers draftRound "2" Akeem_Ayers used to play for the St_Louis_Rams . He was number "39" in the draft pick in draft round "2" .
possessif Akeem_Ayers draftRound "2" St_Louis_Rams is the former team of Akeem_Ayers , who was draft pick "39" , in the draft round , "2" .
relative_subject Akeem_Ayers draftPick "39" Akeem_Ayers formerly played for the Tennessee_Titans and was "39" in the draft pick round "2" .
existential Akeem_Dent formerTeam Houston_Texans Akeem_Dent debuted with the Atlanta_Falcons , who play in Atlanta . He used to play for the Houston_Texans .
coordinated_clauses Aaron_Boogaard birthPlace Regina_,_Saskatchewan Wichita_Thunder member , Aaron_Boogaard , was born in 1986 , in Regina_,_Saskatchewan .
coordinated_clauses Aleksandr_Chumakov birthPlace Moscow Aleksandr_Chumakov , who plays for the Soviet_Union_national_football_team , was born in Moscow in 1948 .
relative_subject Anadolu_Efes_S.K. coach Dušan_Ivković Alex_Tyus plays for the Euroleague club Anadolu_Efes_S.K. , where the coach is Dušan_Ivković .
existential 11th_Mississippi_Infantry_Monument established 2000 The 11th_Mississippi_Infantry_Monument ( established in 2000 ) is located in Gettysburg_,_Pennsylvania and falls under the category of Contributing_property .
direct_object 11th_Mississippi_Infantry_Monument country "United_States" The 11th_Mississippi_Infantry_Monument was established in the "United_States" in 2000 and falls under the category of Contributing_property .
apposition Adams_County_,_Pennsylvania has_to_its_southeast Carroll_County_,_Maryland The 11th_Mississippi_Infantry_Monument which was completed in 2000 is located in Adams_County_,_Pennsylvania . Adams_County_,_Pennsylvania is to the north west of Carroll_County_,_Maryland .
apposition 11th_Mississippi_Infantry_Monument category Contributing_property The 11th_Mississippi_Infantry_Monument , categorized as a Contributing_property , is placed in the municipality of Gettysburg_,_Pennsylvania , the Adams_County_,_Pennsylvania .
coordinated_clauses Monocacy_National_Battlefield location Frederick_County_,_Maryland Frederick_County_,_Maryland is the location of the Monocacy_National_Battlefield . The Battlefield is the location of the 14th_New_Jersey_Volunteer_Infantry_Monument and is categorised as a Historic_districts_in_the_United_States .
existential Accademia_di_Architettura_di_Mendrisio academicStaffSize 100 The Accademia_di_Architettura_di_Mendrisio was established in Switzerland in 1996 . It has 100 academic staff .
existential Atatürk_Monument_(İzmir) material "Bronze" The "Bronze" Atatürk_Monument_(İzmir) is located in Turkey and was inaugurated "1932-07-27" .
direct_object Baku_Turkish_Martyrs'_Memorial dedicatedTo "Ottoman_Army_soldiers_killed_in_the_Battle_of_Baku" The Baku_Turkish_Martyrs'_Memorial , designed by "Hüseyin_Bütüner_and_Hilmi_Güner" , is made from "Red_granite_and_white_marble" and is dedicated to the "Ottoman_Army_soldiers_killed_in_the_Battle_of_Baku" .
existential Baku_Turkish_Martyrs'_Memorial material "Red_granite_and_white_marble" The Baku_Turkish_Martyrs'_Memorial was designed in "Red_granite_and_white_marble" by "Hüseyin_Bütüner_and_Hilmi_Güner" and is known as "Türk_Şehitleri_Anıtı" .
passive_voice Baku_Turkish_Martyrs'_Memorial designer "Hüseyin_Bütüner_and_Hilmi_Güner" "Hüseyin_Bütüner_and_Hilmi_Güner" designed the Baku_Turkish_Martyrs'_Memorial in "Red_granite_and_white_marble" . The native name of the memorial is "Türk_Şehitleri_Anıtı" .
existential Rolando_Maran placeOfBirth Italy Rolando_Maran ( born : Italy ) plays for Carrarese_Calcio and manages AC_Chievo_Verona .
existential AZAL_PFK fullname "AZAL_Peşəkar_Futbol_Klubu" AZAL_PFK stands for "AZAL_Peşəkar_Futbol_Klubu" . They competed in the season 2014 and have 3500 members .
coordinated_clauses AS_Gubbio_1910 fullname "Associazione_Sportiva_Gubbio_1910_Srl" The full name of AS_Gubbio_1910 is "Associazione_Sportiva_Gubbio_1910_Srl" . It was a part of the 2014–15_Lega_Pro season and has 5300 members .
coordinated_clauses ACF_Fiorentina fullname "ACF_Fiorentina_SpA" ACF_Fiorentina , who competed in the 2014 season has the fullname of "ACF_Fiorentina_SpA" . They currently have 47290 members .
coordinated_full_clauses AC_Lumezzane league "Lega_Pro/A" I am interested in the 2014 AC_Lumezzane season . It plays in "Lega_Pro/A" . and has 4150 members .
apposition AC_Lumezzane league "Lega_Pro/A" I am interested in the 2014 AC_Lumezzane season . It plays in "Lega_Pro/A" . and has 4150 members .
coordinated_full_clauses 1_FC_Magdeburg league 3_Liga 1_FC_Magdeburg is in the 3_Liga league . They played in the 2014 season and have 27250 members .
coordinated_full_clauses AE_Dimitra_Efxeinoupolis ground Efxeinoupoli AE_Dimitra_Efxeinoupolis have 1500 members and were in the 2014–15_A_EPSTH_,_Greece . Their home ground is located in Efxeinoupoli .
coordinated_full_clauses AFC_Ajax_(amateurs) fullname "Amsterdamsche_Football_Club_Ajax_Amateurs" The full name of AFC_Ajax_(amateurs) is "Amsterdamsche_Football_Club_Ajax_Amateurs" they have 5000 members and played in the 2014–15_Topklasse season .
existential AS_Gubbio_1910 fullname "Associazione_Sportiva_Gubbio_1910_Srl" AS_Gubbio_1910 ( "Associazione_Sportiva_Gubbio_1910_Srl" ) , with 5300 members , participated in the 2014–15_Lega_Pro season .
apposition AZAL_PFK fullname "AZAL_Peşəkar_Futbol_Klubu" The full name of AZAL_PFK is "AZAL_Peşəkar_Futbol_Klubu" . They competed in the 2014 season and have 3500 members .
apposition Agremiação_Sportiva_Arapiraquense nickname "''Alvinegro" The nickname of Agremiação_Sportiva_Arapiraquense is "''Alvinegro" , they were in 2015_Campeonato_Brasileiro_Série_C in 2015 and have 17000 members .
apposition Akron_Summit_Assault manager Denzil_Antonio Akron_Summit_Assault are in season 2011 and have 3000 members . Their manager is Denzil_Antonio .
relative_subject AZ_Alkmaar fullname "Alkmaar_Zaanstreek" "Alkmaar_Zaanstreek" ( AZ_Alkmaar ) has 17023 members and played in the 2014 season .
possessif AZ_Alkmaar fullname "Alkmaar_Zaanstreek" "Alkmaar_Zaanstreek" ( AZ_Alkmaar ) has 17023 members and played in the 2014 season .
coordinated_clauses Azerbaijan_Premier_League champions Qarabağ_FK Qarabağ_FK are champions of the Azerbaijan_Premier_League which AZAL_PFK play in and they have 3500 members .
coordinated_full_clauses AC_Lumezzane fullname "Associazione_Calcio_Lumezzane_SpA" The fullname of AC_Lumezzane is "Associazione_Calcio_Lumezzane_SpA" . It is in the "Lega_Pro/A" league and has 4150 members .
possessif AC_Lumezzane season 2014 I am interested in the 2014 AC_Lumezzane season . It plays in "Lega_Pro/A" . and has 4150 members .
juxtaposition Serie_A champions Juventus_FC "Verona_,_Italy" is home to AC_Chievo_Verona who play in Serie_A . Juventus_FC have previously been champions of Serie_A .
possessif Serie_A champions Juventus_FC "Verona_,_Italy" is the home to AC_Chievo_Verona who is in the league , Juventus_FC have been Serie_A champions .
possessif Superleague_Greece champions Olympiacos_FC AEK_Athens_FC who ' s grounds are in Athens compete in the Superleague_Greece , the champion of which is Olympiacos_FC .
relative_object Campeonato_Brasileiro_Série_C country Brazil Agremiação_Sportiva_Arapiraquense play in the Campeonato_Brasileiro_Série_C league in Brazil . Their ground is the Estádio_Municipal_Coaracy_da_Mata_Fonseca .
possessif AC_Cesena numberOfMembers 23900 AC_Cesena , in the Serie_B League , has 23900 members and is located is Cesena .
coordinated_full_clauses Serie_A champions Juventus_FC AS_Roma play their home games in Serie_A in Rome , the current champions are Juventus_FC .
apposition Serie_A champions Juventus_FC AS_Roma , who have a ground in Rome , play in Serie_A alongside Juventus_FC who are former champions .
apposition Serie_A champions Juventus_FC AS_Roma ' s ground is in "Rome_,_Italy" and they played in Serie_A where the champions were Juventus_FC .
apposition AEK_Athens_FC numberOfMembers 69618 AEK_Athens_FC , with 69618 members , play in the Superleague_Greece . Their ground is in Athens .
apposition Serie_A champions Juventus_FC With grounds in Verona , AC_Chievo_Verona play in the Serie_A league , of which Juventus_FC have been champions .
coordinated_clauses Campeonato_Brasileiro_Série_C country Brazil Agremiação_Sportiva_Arapiraquense play in the Campeonato_Brasileiro_Série_C league in Brazil . Their ground is the Estádio_Municipal_Coaracy_da_Mata_Fonseca .
coordinated_clauses Serie_A champions Juventus_FC "Verona_,_Italy" is the home to AC_Chievo_Verona who is in the league , Juventus_FC have been Serie_A champions .
coordinated_full_clauses AC_Cesena numberOfMembers 23900 AC_Cesena , with 23900 members , is in the Serie_B league and has a ground called Stadio_Dino_Manuzzi .
coordinated_full_clauses Serie_A champions Juventus_FC "Verona_,_Italy" is home to AC_Chievo_Verona who play in Serie_A . Juventus_FC have previously been champions of Serie_A .
coordinated_full_clauses Serie_A champions Juventus_FC Juventus_FC are former champions of Serie_A . AC_Chievo_Verona is in that league now and play in Verona .
apposition Serie_A champions Juventus_FC AC_Chievo_Verona ' s home ground is Stadio_Marc'Antonio_Bentegodi . They play in the Serie_A league of which Juventus_FC have been champions .
passive_voice Serie_A champions Juventus_FC Juventus_FC are former champions of Serie_A . AC_Chievo_Verona is in that league now and play in Verona .
direct_object Campeonato_Brasileiro_Série_C country Brazil Agremiação_Sportiva_Arapiraquense play in the Campeonato_Brasileiro_Série_C league in Brazil . Their ground is known as the Estádio_Municipal_Coaracy_da_Mata_Fonseca .
relative_subject Azerbaijan_Premier_League champions Qarabağ_FK AZAL_Arena is the ground of AZAL_PFK and competes in The Azerbaijan_Premier_League , which champions are Qarabağ_FK .
relative_subject Serie_A champions Juventus_FC The Serie_A champions are Juventus_FC . AS_Roma also play in the same league and their ground is Stadio_Olimpico .
relative_object AC_Chievo_Verona fullname "AC_Cheivo_Verona_Srl" "AC_Cheivo_Verona_Srl" ( abbreviated to AC_Chievo_Verona ) has 39371 members and its home ground is Stadio_Marc'Antonio_Bentegodi .
relative_object AS_Roma fullname "Associazione_Sportiva_Roma_SpA" AS_Roma play at the Stadio_Olimpico , have 70634 members and their full name is "Associazione_Sportiva_Roma_SpA" .
direct_object AFC_Blackpool fullname "Association_Football_Club_Blackpool" AFC_Blackpool fullname is "Association_Football_Club_Blackpool" . They have 1500 members and their ground is "The_Mechanics_," .
coordinated_full_clauses AC_Cesena league Serie_B AC_Cesena ' s ground is the Stadio_Dino_Manuzzi . They have 23900 members and play in the Serie_B league .
coordinated_full_clauses AE_Dimitra_Efxeinoupolis season 2014 AE_Dimitra_Efxeinoupolis played in the 2014 season and has 1500 members . Their ground is located in the town of Efxeinoupoli .
coordinated_full_clauses AFC_Blackpool fullname "Association_Football_Club_Blackpool" AFC_Blackpool fullname is "Association_Football_Club_Blackpool" , they have 1500 members and their ground is located in Blackpool .
coordinated_full_clauses AFC_Fylde fullname "Association_Football_Club_Fylde" AFC_Fylde has the full name "Association_Football_Club_Fylde" ," they have 3180 members and their ground is located in Warton_,_Fylde .
possessif AC_Chievo_Verona fullname "AC_Cheivo_Verona_Srl" "Verona_,_Italy" is the home to AC_Chievo_Verona which has 39371 members and the full name of "AC_Cheivo_Verona_Srl" .
relative_subject AE_Dimitra_Efxeinoupolis chairman Antonis_Milionis The ground of AE_Dimitra_Efxeinoupolis is located in Efxeinoupoli , has 1500 members and its chairman is Antonis_Milionis .
relative_subject AFC_Fylde fullname "Association_Football_Club_Fylde" AFC_Fylde ' s ground is "Kellamergh_Park" . They have 3180 members and the full name of "Association_Football_Club_Fylde" .
relative_subject AFC_Fylde fullname "Association_Football_Club_Fylde" "Association_Football_Club_Fylde" , abbreviated to AFC_Fylde , has 3180 members and its ground is in Lancashire .
coordinated_clauses AC_Chievo_Verona fullname "AC_Cheivo_Verona_Srl" "AC_Cheivo_Verona_Srl" ( abbreviated to AC_Chievo_Verona ) has 39371 members and its home ground is Stadio_Marc'Antonio_Bentegodi .
coordinated_clauses AFC_Fylde fullname "Association_Football_Club_Fylde" AFC_Fylde has the full name "Association_Football_Club_Fylde" ," they have 3180 members and their ground is located in Warton_,_Fylde .
coordinated_full_clauses AFC_Fylde fullname "Association_Football_Club_Fylde" "Association_Football_Club_Fylde" ( abbreviated to AFC_Fylde ) has 3180 members and are based at "Warton_,_Fylde_,_Lancashire" .
passive_voice AS_Livorno_Calcio fullname "Livorno_Calcio_SpA" "Livorno_Calcio_SpA" , abbreviated to AS_Livorno_Calcio , has 19238 members and has the ground Stadio_Armando_Picchi .
passive_voice AE_Dimitra_Efxeinoupolis league A_EPSTH_2nd_GROUP AE_Dimitra_Efxeinoupolis ground is in the town of Efxeinoupoli . It has 1500 members and plays in the A_EPSTH_2nd_GROUP .
relative_subject AS_Roma fullname "Associazione_Sportiva_Roma_SpA" "Associazione_Sportiva_Roma_SpA" , or AS_Roma for short , has the Stadio_Olimpico as its ground and 70634 members .
apposition AD_Isidro_Metapán fullname "Isidro_Metapán" AD_Isidro_Metapán has 10000 members and their ground is in "Metapán_,_El_Salvador" . Their full name is "Isidro_Metapán" .
apposition AS_Roma fullname "Associazione_Sportiva_Roma_SpA" AS_Roma , or "Associazione_Sportiva_Roma_SpA" , has 70634 members and its ground is in "Rome_,_Italy" .
coordinated_full_clauses AC_Chievo_Verona fullname "AC_Cheivo_Verona_Srl" "Verona_,_Italy" is the home to AC_Chievo_Verona which has 39371 members and the full name of "AC_Cheivo_Verona_Srl" .
coordinated_full_clauses AD_Isidro_Metapán fullname "Asociación_Deportiva" The "Asociación_Deportiva" is now known as AD_Isidro_Metapán and has 10000 members and plays in Metapán .
passive_voice AS_Roma fullname "Associazione_Sportiva_Roma_SpA" The "Associazione_Sportiva_Roma_SpA" is the non - abbreviated name of AS_Roma which has 70634 members and a home ground in "Rome_,_Italy" .
apposition AS_Livorno_Calcio fullname "Associazione_Sportiva" The AS_Livorno_Calcio ( "Associazione_Sportiva" ) , with 19238 members , has the Stadio_Armando_Picchi as its home ground .
juxtaposition AEK_Athens_FC ground Athens AEK_Athens_FC who ' s grounds are in Athens compete in the Superleague_Greece , the champion of which is Olympiacos_FC .
relative_object Campeonato_Brasileiro_Série_C country Brazil Agremiação_Sportiva_Arapiraquense play in the Campeonato_Brasileiro_Série_C league , that is based in Brazil . The champions are Vila_Nova_Futebol_Clube .
juxtaposition AZAL_PFK ground AZAL_Arena AZAL_Arena is the ground of AZAL_PFK and competes in The Azerbaijan_Premier_League , which champions are Qarabağ_FK .
juxtaposition Campeonato_Brasileiro_Série_C country Brazil Agremiação_Sportiva_Arapiraquense play in the Campeonato_Brasileiro_Série_C league , that is based in Brazil . The champions are Vila_Nova_Futebol_Clube .
relative_adverb Campeonato_Brasileiro_Série_C country Brazil Agremiação_Sportiva_Arapiraquense play in the Campeonato_Brasileiro_Série_C league , that is based in Brazil . The champions are Vila_Nova_Futebol_Clube .
relative_adverb AC_Cesena ground Stadio_Dino_Manuzzi With grounds at the Stadio_Dino_Manuzzi , AC_Cesena play in Serie_B . Carpi_FC_1909 are previous champions of that league .
relative_adverb Campeonato_Brasileiro_Série_C country Brazil Agremiação_Sportiva_Arapiraquense play in the Campeonato_Brasileiro_Série_C league in Brazil . The Vila_Nova_Futebol_Clube were champions at the Campeonato_Brasileiro_Série_C .
relative_adverb AC_Chievo_Verona ground "Verona_,_Italy" "Verona_,_Italy" is home to AC_Chievo_Verona who play in Serie_A . Juventus_FC have previously been champions of Serie_A .
relative_adverb AS_Roma ground Stadio_Olimpico Juventus_FC have been Serie_A champions . AS_Roma , who ' s ground is Stadio_Olimpico also play in the same league .
coordinated_full_clauses AC_Cesena ground Stadio_Dino_Manuzzi With grounds at the Stadio_Dino_Manuzzi , AC_Cesena play in Serie_B . Carpi_FC_1909 are previous champions of that league .
coordinated_full_clauses AS_Roma ground Stadio_Olimpico Juventus_FC have been Serie_A champions . AS_Roma , who ' s ground is Stadio_Olimpico also play in the same league .
apposition AC_Chievo_Verona ground Stadio_Marc'Antonio_Bentegodi The home ground of AC_Chievo_Verona is Stadio_Marc'Antonio_Bentegodi , they are in the Serie_A league , of which Juventus_FC have been champions .
relative_subject AFC_Fylde ground Warton_,_Fylde "Association_Football_Club_Fylde" , abbreviated to AFC_Fylde , has 3180 members and its ground is Warton_,_Fylde .
direct_object AS_Roma ground "Rome_,_Italy" AS_Roma , or "Associazione_Sportiva_Roma_SpA" , has 70634 members and its ground is in "Rome_,_Italy" .
coordinated_full_clauses AC_Chievo_Verona ground "Verona_,_Italy" "Verona_,_Italy" is the home to AC_Chievo_Verona which has 39371 members and the full name of "AC_Cheivo_Verona_Srl" .
coordinated_full_clauses AFC_Blackpool ground "The_Mechanics_," The fullname of AFC_Blackpool is "Association_Football_Club_Blackpool" . Their ground is "The_Mechanics_," . and they have 1500 members .
coordinated_full_clauses AS_Gubbio_1910 season 2014–15_Lega_Pro The full name of AS_Gubbio_1910 is "Associazione_Sportiva_Gubbio_1910_Srl" . It was a part of the 2014–15_Lega_Pro season and has 5300 members .
coordinated_full_clauses AS_Gubbio_1910 season 2014 The full name of AS_Gubbio_1910 is "Associazione_Sportiva_Gubbio_1910_Srl" . It competed in the 2014 season and has 5300 members .
coordinated_full_clauses AFC_Ajax season 2014 The full name of AFC_Ajax is "Amsterdamsche_Football_Club_Ajax" , they have 53502 members and played in season 2014 .
juxtaposition AFC_Fylde ground Lancashire "Association_Football_Club_Fylde" , abbreviated to AFC_Fylde , has 3180 members and its ground is in Lancashire .
coordinated_full_clauses AS_Livorno_Calcio ground "Livorno_,_Italy" The full name of AS_Livorno_Calcio is "Livorno_Calcio_SpA" , they have 19238 members and their ground is in "Livorno_,_Italy" .
passive_voice Italy demonym Italians Italy ( led by Pietro_Grasso ) is home to the Italians people and AS_Gubbio_1910 .
relative_adverb Greece capital Athens Greece , led by Nikos_Voutsis , who ' s capital is Athens , is the location of club AE_Dimitra_Efxeinoupolis .
relative_adverb Greece capital Athens Athens is Greece ' s capital ( led by Alexis_Tsipras ). Greece is the location of AE_Dimitra_Efxeinoupolis .
passive_voice Atatürk_Monument_(İzmir) material "Bronze" Turkey , where the leader is Ahmet_Davutoğlu , is the location of the "Bronze" Atatürk_Monument_(İzmir) .
relative_object Greece language Greek_language AE_Dimitra_Efxeinoupolis is located in Greece , which leader is Prokopis_Pavlopoulos and the language is Greek_language .
relative_subject Agremiação_Sportiva_Arapiraquense ground Estádio_Municipal_Coaracy_da_Mata_Fonseca Agremiação_Sportiva_Arapiraquense play in the Campeonato_Brasileiro_Série_C league based in Brazil . Their ground is known as Estádio_Municipal_Coaracy_da_Mata_Fonseca .
relative_adverb Amsterdam leader Eberhard_van_der_Laan The leader of Amsterdam is Eberhard_van_der_Laan . The Amsterdam-Noord is located there as well as the AFC_Ajax_(amateurs) .
apposition Sportpark_De_Toekomst tenant Ajax_Youth_Academy AFC_Ajax_(amateurs) ' s ground is Sportpark_De_Toekomst , tenanted by Ajax_Youth_Academy and operated by AFC_Ajax .
apposition Sportpark_De_Toekomst tenant Jong_Ajax AFC_Ajax own and operate Sportpark_De_Toekomst . Tenants include AFC_Ajax_(amateurs) , Ajax_Youth_Academy and Jong_Ajax .
relative_adverb Sportpark_De_Toekomst operator AFC_Ajax AFC_Ajax_(amateurs) ' s ground is Sportpark_De_Toekomst , tenanted by Ajax_Youth_Academy and operated by AFC_Ajax .
relative_subject Sportpark_De_Toekomst operator AFC_Ajax AFC_Ajax_(amateurs) ' s ground is Sportpark_De_Toekomst , operated by AFC_Ajax . The tenant is Jong_Ajax .
relative_subject Athens mayor Giorgos_Kaminis The ground for AEK_Athens_FC is the Olympic_Stadium_(Athens) in Athens whose mayor is Giorgos_Kaminis .
passive_voice Akron_Summit_Assault season 2011 Denzil_Antonio is the manager of Akron_Summit_Assault who are in season 2011 and have 3000 members .
relative_subject Akron_,_Ohio location Summit_County_,_Ohio St_Vincent–St_Mary_High_School , Akron_,_Ohio ( a Summit_County_,_Ohio ) - is home to the Akron_Summit_Assault ' s ground .
passive_voice Akron_,_Ohio location Summit_County_,_Ohio St_Vincent–St_Mary_High_School is in Akron_,_Ohio which is in Summit_County_,_Ohio and home to the ground of Akron_Summit_Assault .
coordinated_full_clauses Aleksandr_Chumakov club Soviet_Union_national_football_team Aleksandr_Chumakov , who is in the Soviet_Union_national_football_team , plays for is FC_Torpedo_Moscow . Valery_Petrakov is the manager of FC_Torpedo_Moscow which played in the 2014–15_Russian_Premier_League .
relative_object Italy leader Sergio_Mattarella The ground of AS_Gubbio_1910 is located in Italy where Rome is the capital and two of the leaders are Pietro_Grasso and Sergio_Mattarella .
relative_subject Arròs_negre region Catalonia Arròs_negre , containing White_rice , is from the Catalonia region in Spain .
relative_subject Ayam_penyet region Malaysia Fried_chicken is one of the ingredients in the dish Ayam_penyet . This is a dish from both Malaysia and Indonesia .
relative_subject Bacon_sandwich dishVariation BLT A variation of the BLT , the Bacon_sandwich is from the United_Kingdom and can contain Brown_sauce .
relative_subject Baked_Alaska region Hong_Kong Baked_Alaska , containing Christmas_pudding , is from France and Hong_Kong .
relative_subject Bandeja_paisa region Paisa_Region Bandeja_paisa is from the Paisa_Region , Colombian_cuisine and one of the ingredients is Lemon .
relative_subject Batchoy mainIngredients "noodles_,_pork_organs_,_vegetables_,_chicken_,_shrimp_,_beef" Noodle are an ingredient in Batchoy , it also "noodles_,_pork_organs_,_vegetables_,_chicken_,_shrimp_,_beef" . It hails from the Philippines .
relative_subject Beef_kway_teow region Singapore An ingredient in Beef_kway_teow , from the Singapore region and popular in Indonesia , is Sesame_oil .
relative_subject Binignit mainIngredients Banana The main ingredient of Binignit is Banana , along with Sago . The dish can be found in the Philippines .
coordinated_full_clauses Ajoblanco alternativeName "Ajo_blanco" Bread is an ingredient of Ajoblanco , which is from Spain and an alternative name to "Ajo_blanco" .
coordinated_full_clauses Amatriciana_sauce region Lazio Pecorino_Romano is an ingredient of Amatriciana_sauce which comes from the Lazio region , in Italy .
coordinated_full_clauses Arròs_negre region Valencian_Community Cuttlefish is an ingredient in Arròs_negre which comes from the region of the Valencian_Community , in Spain .
coordinated_full_clauses Ayam_penyet region Singapore Fried_chicken is an ingredient of the dish Ayam_penyet which comes from Java and Singapore .
coordinated_full_clauses Bacon_sandwich dishVariation BLT A variation of the BLT , the Bacon_sandwich is from the United_Kingdom and can contain Brown_sauce .
coordinated_full_clauses Baked_Alaska region "Paris_,_New_York_or_Hong_Kong" Ice_cream is an ingredient of Baked_Alaska which is a dish found in the United_States , "Paris_,_New_York_or_Hong_Kong" .
coordinated_full_clauses Bandeja_paisa region Antioquia_Department Chicharrón is an ingredient found in Bandeja_paisa , part of Colombian_cuisine , is found in the Antioquia_Department .
coordinated_full_clauses Bandeja_paisa region Antioquia_Department Bandeja_paisa , containing Lemon , is a typical Colombian_cuisine found in the Antioquia_Department .
coordinated_full_clauses Bandeja_paisa region Paisa_Region Pork_belly is one of the ingredients of the dish Bandeja_paisa . This is a traditional dish from the Paisa_Region and is part of Colombian_cuisine .
coordinated_full_clauses Beef_kway_teow region Indonesia Palm_sugar is an ingredient of Beef_kway_teow . The dish is found in Indonesia and is a popular dish in Singapore .
coordinated_full_clauses Bhajji alternativeName "Bhaji_,_bajji" Bhajji originate from India where they are also known as "Bhaji_,_bajji" or Bhajji . One of the ingredients used is Gram_flour .
coordinated_full_clauses Binignit mainIngredients Coconut_milk Sweet_potato and Coconut_milk are main ingredients in the dish Binignit which comes from is the Philippines .
coordinated_full_clauses Binignit mainIngredients Banana One of the main ingredients in Binignit is Banana , originating from the Philippines and has Taro as an ingredient .
coordinated_full_clauses Binignit mainIngredients Sweet_potato Taro is an ingredient in the Philippines dish of Binignit along with the main ingredient of Sweet_potato .
coordinated_full_clauses Bionico region Jalisco Granola is one of the ingredients of the food Bionico which comes from the region of Jalisco in Mexico .
direct_object Ajoblanco alternativeName "Ajo_blanco" Bread is an ingredient of Ajoblanco , which is from Spain and an alternative name to "Ajo_blanco" .
direct_object Amatriciana_sauce region Lazio Guanciale is an ingredient in Amatriciana_sauce which comes from the Lazio region in Italy .
existential Arrabbiata_sauce region Rome Arrabbiata_sauce contains Garlic and comes from the Rome region of Italy .
direct_object Arròs_negre region Catalonia Cephalopod_ink is an ingredient in Arròs_negre . A dish which is from the Catalonia region , in Spain .
direct_object Arròs_negre region Catalonia Cubanelle is in Arròs_negre , a dish from the Catalonia region of Spain .
direct_object Arròs_negre region Valencian_Community Cubanelle is one of the ingredients in Arròs_negre , which comes from the region of the Valencian_Community in Spain .
existential Arròs_negre region Valencian_Community Arròs_negre contains White_rice and comes from the region of the Valencian_Community in Spain .
direct_object Bacon_Explosion course "Main_course" Bacon_Explosion is a "Main_course" including Sausage from the United_States .
direct_object Bacon_sandwich dishVariation BLT Bread is an ingredient of a Bacon_sandwich , which is popular in the United_Kingdom . Another variation for a Bacon_sandwich is a BLT .
direct_object Baked_Alaska region New_York An ingredient of Baked_Alaska is Sponge_cake , the dish originates from New_York , United_States .
direct_object Bandeja_paisa region Paisa_Region Avocado is one of the ingredients in the dish Bandeja_paisa , which is typical Colombian_cuisine originating from the Paisa_Region .
direct_object Beef_kway_teow region Indonesia One of the ingredients of the Singapore dish Beef_kway_teow is Sesame_oil . The dish is also eaten in Indonesia .
existential Binignit mainIngredients Sago The Philippines dish of Binignit contains Sago and Sweet_potato .
direct_object Bionico region Jalisco Granola is one of the ingredients of the food Bionico which comes from the region of Jalisco in Mexico .
possessif Ajoblanco region Andalusia Ajoblanco is a dish from the Andalusia region of Spain and includes the ingredient Garlic .
possessif Amatriciana_sauce region Lazio Pecorino_Romano is an ingredient of Amatriciana_sauce which comes from the Lazio region , in Italy .
possessif Arròs_negre region Catalonia Arròs_negre is from the Catalonia region of Spain , it includes Cephalopod_ink .
possessif Arròs_negre region Catalonia Cuttlefish is an ingredient in Arròs_negre which is from the Catalonia region , in Spain .
possessif Arròs_negre region Valencian_Community Arròs_negre is made with White_rice and is a traditional dish from the Valencian_Community in Spain .
possessif Bacon_Explosion region Kansas_City_metropolitan_area Bacon is an ingredient in a Bacon_Explosion which is from the Kansas_City_metropolitan_area , in the United_States .
possessif Bacon_sandwich dishVariation BLT A Bacon_sandwich , also known as a BLT , is popular in the United_Kingdom and can contain the ingredient Brown_sauce .
possessif Baked_Alaska region New_York Ice_cream is an ingredient in Baked_Alaska which comes from both France and the New_York region .
possessif Baked_Alaska region New_York Baked_Alaska contains Christmas_pudding and is from the New_York region of the United_States .
possessif Baked_Alaska region Hong_Kong Christmas_pudding is an ingredient in Baked_Alaska which is from both Hong_Kong and France .
possessif Bandeja_paisa region Paisa_Region Avocado is one of the ingredients in the dish Bandeja_paisa , which is a dish from Colombian_cuisine . It originates from the Paisa_Region .
possessif Bandeja_paisa region Paisa_Region Pork_belly is one of the ingredients of the dish Bandeja_paisa . This is a traditional dish from the Paisa_Region and is part of Colombian_cuisine .
possessif Beef_kway_teow region Singapore An ingredient of Beef_kway_teow , which originates from Singapore and is a popular food of Indonesia , is Palm_sugar .
possessif Bionico course Dessert Bionico is a Dessert containing Sour_cream from Mexico .
possessif Bionico region Jalisco Bionico is a food found in Mexico , comes from the region of Jalisco and has Condensed_milk as an ingredient .
relative_subject Ajoblanco alternativeName "Ajo_blanco" Ajoblanco ( or "Ajo_blanco" ) is from Spain and contains Water .
direct_object Arrabbiata_sauce dishVariation Oregano Arrabbiata_sauce , which is made with Oregano and Garlic , is from Italy .
direct_object Arròs_negre region Catalonia Cubanelle is an ingredient in Arròs_negre , that is a dish from the Catalonia region , Spain .
relative_subject Bacon_sandwich dishVariation BLT A BLT is a type of Bacon_sandwich ( which includes Ketchup ) and can be found in the United_Kingdom .
relative_subject Baked_Alaska region New_York Sponge_cake is an ingredient of Baked_Alaska , which it is claimed originated in New_York and France .
relative_subject Bandeja_paisa region Antioquia_Department Bandeja_paisa is found in the Antioquia_Department , is a typical Colombian_cuisine and has Hogao as an ingredient .
direct_object Binignit mainIngredients Coconut_milk Coconut_milk and Sweet_potato are the main ingredients of the Binignit dish , which can be found in the Philippines .
relative_subject Shumai dishVariation Siomay Batagor , a variation of Shumai ( a variation of Siomay ) , contains Peanut_sauce and is from Indonesia .
existential Ajoblanco region Andalusia Garlic is and ingredient of Ajoblanco , a food found in Andalusia , in Spain .
apposition Ajoblanco alternativeName "Ajo_blanco" Almond are an ingredient in Ajoblanco ( alternatively called "Ajo_blanco" ) from Spain .
apposition Arrabbiata_sauce region Rome Arrabbiata_sauce originates from the region of Rome , Italy and Tomato is one of its ingredients .
apposition Arròs_negre region Valencian_Community Cephalopod_ink is an ingredient in the dish Arròs_negre which comes from the region of the Valencian_Community in Spain .
apposition Arròs_negre region Valencian_Community Cuttlefish is an ingredient of the dish Arròs_negre , which is a dish that comes from the Valencian_Community in Spain .
apposition Bacon_Explosion mainIngredients Sausage Bacon and Sausage are the main ingredients in a Bacon_Explosion , which comes from the United_States .
apposition Bandeja_paisa region Antioquia_Department Avocado is one of the ingredients in Bandeja_paisa which is typical Colombian_cuisine and found in the Antioquia_Department .
apposition Bandeja_paisa region Paisa_Region Bandeja_paisa , containing Chicharrón , originates from the Paisa_Region and is part of Colombian_cuisine .
apposition Beef_kway_teow region Indonesia Palm_sugar is an ingredient of Beef_kway_teow . The dish is found in Indonesia and is a popular dish in Singapore .
apposition Beef_kway_teow region Singapore An ingredient of Beef_kway_teow , which originates from Singapore and is a popular food of Indonesia , is Palm_sugar .
apposition Binignit mainIngredients Taro Taro is the main ingredient of Binignit , along with Sweet_potato . The dish is found in the Philippines .
apposition Binignit mainIngredients Coconut_milk The main ingredient in the Philippines dish of Binignit is Coconut_milk . Sago is also used .
apposition Bionico course Dessert Bionico is a Dessert containing Sour_cream from Mexico .
relative_subject Amatriciana_sauce region Lazio Amatriciana_sauce from Lazio , Italy , contains Pecorino_Romano .
relative_subject Bacon_sandwich alternativeName "Bacon_butty_,_bacon_sarnie_,_rasher_sandwich_,_bacon_sanger_,_piece_'n_bacon_,_bacon_cob_,_bacon_barm_,_bacon_muffin" Coming from the United_Kingdom , the Bacon_sandwich ( which has Ketchup as an ingredient ) , can also be known as a "Bacon_butty_,_bacon_sarnie_,_rasher_sandwich_,_bacon_sanger_,_piece_'n_bacon_,_bacon_cob_,_bacon_barm_,_bacon_muffin" .
relative_subject Beef_kway_teow region Singapore Beef_kway_teow comes from the Singapore region of Indonesia and has Sesame_oil as an ingredient .
coordinated_clauses Arròs_negre region Catalonia Arròs_negre contains Cuttlefish and is from Catalonia in Spain .
coordinated_clauses Baked_Alaska region Hong_Kong Baked_Alaska , containing Christmas_pudding , is from France and Hong_Kong .
coordinated_clauses Binignit mainIngredients Sago The Philippines dish of Binignit contains Sago and Sweet_potato .
coordinated_clauses Siomay dishVariation Shumai Batagor comes from Indonesia , it includes Peanut_sauce and is a variation on Shumai / Siomay .
direct_object Bionico region Guadalajara Bionico , from the Guadalajara region of Mexico , is made with Sour_cream .
possessif Bacon_sandwich dishVariation BLT The United_Kingdom dish , the Bacon_sandwich uses Condiment as an ingredient and has the variation BLT .
possessif Binignit mainIngredients Sweet_potato Binignit contains the ingredients Sweet_potato and Banana and comes from the Philippines .
passive_voice Ajoblanco region Andalusia Water is an ingredient in Ajoblanco , which is a food found in Andalusia , in Spain .
passive_voice Arrabbiata_sauce dishVariation Oregano Garlic is an ingredient in Arrabbiata_sauce ( found in Italy ) , which can also have Oregano added to it .
passive_voice Arròs_negre region Catalonia Cuttlefish is an ingredient in Arròs_negre which is from the Catalonia region , in Spain .
passive_voice Bacon_sandwich dishVariation BLT Bread is an ingredient of a Bacon_sandwich , which is popular in the United_Kingdom . Another variation for a Bacon_sandwich is a BLT .
passive_voice Beef_kway_teow region Singapore An ingredient of Beef_kway_teow , which originates from Singapore and is a popular food of Indonesia , is Palm_sugar .
relative_subject Binignit mainIngredients Banana The main ingredients of Binignit are Banana and Sweet_potato and it can be found in the Philippines .
relative_subject Bacon_Explosion course "Main_course" The Bacon_Explosion originates in the United_States . It contains Bacon and is served as a "Main_course" .
coordinated_full_clauses Bacon_Explosion mainIngredients Bacon Bacon is the main ingredient in a Bacon_Explosion which also has Sausage in it . It comes from the United_States .
coordinated_full_clauses Shumai dishVariation Siomay Batagor is a variant of Shumai and Siomay , an Indonesia dish containing Peanut_sauce .
passive_voice Arròs_negre region Catalonia Arròs_negre is from the Catalonia region of Spain , it includes Cephalopod_ink .
passive_voice Arròs_negre region Valencian_Community Arròs_negre made with Squid is a traditional dish from the region of the Valencian_Community in Spain .
existential Baked_Alaska region New_York Baked_Alaska is made with Christmas_pudding and comes from France and New_York .
passive_voice Bandeja_paisa region Paisa_Region Bandeja_paisa , containing Chicharrón , originates from the Paisa_Region and is part of Colombian_cuisine .
passive_voice Bandeja_paisa region Paisa_Region Bandeja_paisa is from the Paisa_Region and typical Colombian_cuisine . Chorizo is one of its ingredients .
passive_voice Binignit mainIngredients Banana Binignit is a dish from the Philippines made from Banana and Sweet_potato .
passive_voice Arròs_negre region Valencian_Community Arròs_negre contains White_rice and comes from the region of the Valencian_Community in Spain .
passive_voice Ayam_penyet region Singapore Ayam_penyet ( Indonesia , Singapore ) has Fried_chicken in it .
passive_voice Binignit mainIngredients Banana Binignit has the main ingredient of Banana but also includes Taro . It originates in the Philippines .
juxtaposition Ajoblanco alternativeName "Ajo_blanco" Bread is an ingredient of Ajoblanco , which is from Spain and an alternative name to "Ajo_blanco" .
juxtaposition Ajoblanco region Andalusia Water is an ingredient in Ajoblanco , which is a food found in Andalusia , in Spain .
juxtaposition Amatriciana_sauce region Lazio Pecorino_Romano is an ingredient of Amatriciana_sauce which comes from the Lazio region , in Italy .
juxtaposition Arròs_negre region Catalonia Arròs_negre is from the region of Catalonia , Spain and one of its ingredients is Cephalopod_ink .
juxtaposition Arròs_negre region Valencian_Community Arròs_negre made with Squid is a traditional dish from the region of the Valencian_Community in Spain .
juxtaposition Bacon_sandwich dishVariation BLT A Bacon_sandwich , also known as a BLT , is popular in the United_Kingdom and can contain the ingredient Brown_sauce .
juxtaposition Baked_Alaska region Hong_Kong Christmas_pudding is an ingredient in Baked_Alaska which is from both Hong_Kong and France .
juxtaposition Bandeja_paisa region Paisa_Region Avocado is one of the ingredients in the dish Bandeja_paisa , which is typical Colombian_cuisine originating from the Paisa_Region .
juxtaposition Beef_kway_teow region Singapore An ingredient in Beef_kway_teow , from the Singapore region and popular in Indonesia , is Sesame_oil .
juxtaposition Binignit mainIngredients Taro Taro is the main ingredient of Binignit , along with Sweet_potato . The dish is found in the Philippines .
juxtaposition Binignit mainIngredients Sweet_potato Taro is an ingredient in the Philippines dish of Binignit along with the main ingredient of Sweet_potato .
juxtaposition Chicharrón region Andalusia Found in the Andalusia region , Chicharrón is one of the ingredients in Bandeja_paisa which is is part of Colombian_cuisine .
juxtaposition Siomay dishVariation Shumai Batagor comes from Indonesia , it includes Peanut_sauce and is a variation on Shumai / Siomay .
coordinated_full_clauses Bacon_sandwich dishVariation BLT Bacon_sandwich are made using Bread and originate from the United_Kingdom . A variation is known as the BLT .
coordinated_clauses Arrabbiata_sauce region Rome Arrabbiata_sauce , a traditional dish from Rome in Italy , has Chili_pepper as one of its important ingredients .
possessif Bacon_sandwich alternativeName "Bacon_butty_,_bacon_sarnie_,_rasher_sandwich_,_bacon_sanger_,_piece_'n_bacon_,_bacon_cob_,_bacon_barm_,_bacon_muffin" Popular in the United_Kingdom , a Bacon_sandwich includes the ingredient Bacon and can also be known as a "Bacon_butty_,_bacon_sarnie_,_rasher_sandwich_,_bacon_sanger_,_piece_'n_bacon_,_bacon_cob_,_bacon_barm_,_bacon_muffin" .
possessif Bionico course Dessert Bionico is served as a Dessert course . It is found in Mexico and requires Granola as an ingredient .
possessif Ajoblanco alternativeName "Ajo_blanco" "Ajo_blanco" ( sometimes called Ajoblanco ) is a dish from Spain that contains Water .
possessif Bacon_sandwich dishVariation BLT Bacon_sandwich , which include Bacon , are popular in the United_Kingdom and are a variation of the BLT , which is itself a variation of the Club_sandwich .
relative_object Ajoblanco region Andalusia Ajoblanco is made from Olive_oil and is found in Andalusia in Spain .
relative_object Beef_kway_teow region Indonesia Palm_sugar is an ingredient of Beef_kway_teow . The dish is found in Indonesia and is a popular dish in Singapore .
relative_object Binignit mainIngredients Taro Taro is the main ingredient of Binignit , along with Sweet_potato . The dish is found in the Philippines .
passive_voice Arròs_negre region Valencian_Community Arròs_negre comes from the region of the Valencian_Community , is a traditional dish from Spain and has Cephalopod_ink as an ingredient .
coordinated_clauses Bacon_sandwich dishVariation BLT The country that Bacon_sandwich comes from is the United_Kingdom . It can be served with Condiment and variations include the BLT and Club_sandwich .
coordinated_clauses Arrabbiata_sauce dishVariation Oregano Garlic is an ingredient in Arrabbiata_sauce ( found in Italy ) , which can also have Oregano added to it .
coordinated_clauses Arròs_negre region Catalonia Cubanelle is an ingredient in Arròs_negre which is a traditional dish from the Catalonia region of Spain .
coordinated_clauses Baked_Alaska region New_York An ingredient of Baked_Alaska is Meringue which originates in France and the New_York regions .
coordinated_clauses Baked_Alaska region "Paris_,_New_York_or_Hong_Kong" Ice_cream is an ingredient of Baked_Alaska which is a dish found in the United_States , "Paris_,_New_York_or_Hong_Kong" .
coordinated_clauses Bandeja_paisa region Paisa_Region Avocado is one of the ingredients in the dish Bandeja_paisa , which is typical Colombian_cuisine originating from the Paisa_Region .
coordinated_clauses Bandeja_paisa region Paisa_Region Bandeja_paisa is a traditional dish from the Paisa_Region and part of Colombian_cuisine . Hogao is an ingredient of Bandeja_paisa .
coordinated_clauses Beef_kway_teow region Singapore An ingredient of Beef_kway_teow , which originates from Singapore and Indonesia is Palm_sugar .
coordinated_clauses Binignit mainIngredients Sweet_potato One of the main ingredients of the cuisine of Binignit , in the Philippines , is the Sweet_potato and another is Coconut_milk .
apposition Ajoblanco alternativeName "Ajo_blanco" From Spain , Ajoblanco ( alternatively known as "Ajo_blanco" ) has Bread as an ingredient .
apposition Bacon_sandwich dishVariation BLT The United_Kingdom dish , the Bacon_sandwich uses Condiment as an ingredient and has the variation BLT .
apposition Bacon_Explosion mainIngredients Sausage Bacon_Explosion , whose name comes from the United_States , has Bacon and Sausage .
juxtaposition Arròs_negre region Valencian_Community Arròs_negre comes from the region of the Valencian_Community , is a traditional dish from Spain and has Cephalopod_ink as an ingredient .
coordinated_clauses Bionico course Dessert Found in Mexico , the food , Bionico ( with Granola as an ingredient ) , is served at the Dessert course .
relative_subject Baked_Alaska region New_York Baked_Alaska is made with Christmas_pudding and comes from France and New_York .
possessif Bionico region Guadalajara Bionico is from the Guadalajara region of Mexico and it contains Condensed_milk .
relative_object Ajoblanco alternativeName "Ajo_blanco" Ajoblanco , also known as "Ajo_blanco" , comes from Spain . Bread is one of the ingredients .
relative_object Ajoblanco region Andalusia Ajoblanco is a food containing Olive_oil from Andalusia in Spain .
relative_object Arrabbiata_sauce region Rome Arrabbiata_sauce is from the region of Rome in Italy and one of the main ingredients is Chili_pepper .
relative_object Arròs_negre region Catalonia Squid is an ingredient of Arròs_negre which is from the region of Catalonia in Spain .
relative_object Ayam_penyet region Singapore Fried_chicken is an ingredient in Ayam_penyet which is from the country of Java and also the region of Singapore .
relative_object Bacon_Explosion region Kansas_City_metropolitan_area Bacon is an ingredient in a Bacon_Explosion which is from the Kansas_City_metropolitan_area , in the United_States .
relative_object Bacon_sandwich dishVariation BLT A variation of the BLT , the Bacon_sandwich is from the United_Kingdom and can contain Brown_sauce .
relative_object Baked_Alaska region New_York Sponge_cake is one of the ingredients in Baked_Alaska , a dish from the New_York region and found in the United_States .
relative_object Baked_Alaska region Hong_Kong Christmas_pudding is an ingredient in Baked_Alaska which is from both Hong_Kong and France .
relative_object Bandeja_paisa region Paisa_Region Part of Colombian_cuisine , Bandeja_paisa is a traditional dish from the Paisa_Region . One of the main ingredients in this dish is Avocado .
relative_object Bandeja_paisa region Antioquia_Department Part of Colombian_cuisine and found in the Antioquia_Department , Bandeja_paisa ' s main ingredients is Pork_belly .
relative_object Batchoy mainIngredients "noodles_,_pork_organs_,_vegetables_,_chicken_,_shrimp_,_beef" "noodles_,_pork_organs_,_vegetables_,_chicken_,_shrimp_,_beef" , Shrimp , pork organs , vegetables , chicken and beef are ingredients in the dish called Batchoy which comes from the Philippines .
relative_object Binignit mainIngredients Banana One of the main ingredients in Binignit is Banana , originating from the Philippines and has Taro as an ingredient .
relative_object Bionico region Jalisco Granola is one of the ingredients of the food Bionico which comes from the region of Jalisco in Mexico .
existential Bacon_sandwich dishVariation BLT A variation of the BLT , a Bacon_sandwich , comes from the United_Kingdom and has Bacon as the main ingredient .
apposition Binignit mainIngredients Coconut_milk The main ingredient of Binignit is Coconut_milk and it also contains Sago . The dish comes from the Philippines .
existential Siomay dishVariation Shumai Batagor , a variant of Shumai and Siomay , contains Peanut_sauce and originates from Indonesia .
existential Bacon_Explosion mainIngredients Sausage Bacon_Explosion , whose name comes from the United_States , has Bacon and Sausage .
possessif Arrabbiata_sauce country Italy Arrabbiata_sauce from Rome in Italy contains Chili_pepper .
possessif Arròs_negre country Spain Arròs_negre contains Cubanelle and is a traditional dish from the Valencian_Community in Spain .
possessif Baked_Alaska country France Baked_Alaska , containing Christmas_pudding , is from France and Hong_Kong .
possessif Beef_kway_teow country Indonesia Beef_kway_teow comes from the Singapore region of Indonesia and has Sesame_oil as an ingredient .
possessif Bionico country Mexico Containing Raisin , Bionico is found in the region of Jalisco , in Mexico .
possessif Arròs_negre country Spain Arròs_negre comes from the region of the Valencian_Community , is a traditional dish from Spain and has Cephalopod_ink as an ingredient .
direct_object Bionico country Mexico Bionico is found in the region of Jalisco , Mexico and one of its ingredients is Sour_cream .
coordinated_full_clauses Arròs_negre country Spain Arròs_negre contains White_rice and is a traditional dish from the Catalonia region of Spain .
coordinated_full_clauses Antioquia_Department country Colombia Antioquia_Department , Colombia , is the regions where Bandeja_paisa comes from , it includes Lemon .
possessif Arrabbiata_sauce country Italy Garlic is an ingredient in Arrabbiata_sauce which originates from the region of Rome , in Italy .
possessif Bacon_Explosion country United_States Bacon is an ingredient in a Bacon_Explosion which is from the Kansas_City_metropolitan_area , in the United_States .
possessif Baked_Alaska country France Ice_cream is an ingredient in Baked_Alaska which comes from both France and the New_York region .
possessif Baked_Alaska country United_States Christmas_pudding is an ingredient of Baked_Alaska which is a dish from the New_York region and found in the whole of the United_States .
possessif Bakewell_pudding dishVariation Bakewell_tart Bakewell_tart includes Fruit_preserves among it ' s ingredients . It is a variant of Bakewell_pudding and comes from the region of the Derbyshire_Dales .
possessif Bandeja_paisa country Colombian_cuisine Part of Colombian_cuisine , Bandeja_paisa is a dish from the Antioquia_Department region . One of the ingredients in this dish is Cooking_plantain .
possessif Bandeja_paisa country Colombian_cuisine Part of Colombian_cuisine , Bandeja_paisa is a traditional dish from the Paisa_Region . One of the main ingredients in this dish is Avocado .
possessif Bandeja_paisa country Colombian_cuisine Bandeja_paisa is a dish from the Antioquia_Department region , one of the ingredients is Chicharrón , Bandeja_paisa is part of Colombian_cuisine .
possessif Bionico country Mexico Granola is an ingredient in Bionico which comes from the Guadalajara region of Mexico .
relative_subject Arròs_negre country Spain Arròs_negre is from the Catalonia region of Spain , it includes Cephalopod_ink .
relative_subject Arròs_negre country Spain Arròs_negre made with Squid is a traditional dish from the region of the Valencian_Community in Spain .
existential Bandeja_paisa country Colombian_cuisine The Colombian_cuisine dish of Bandeja_paisa which includes Rice can be found in the Antioquia_Department .
relative_subject Bandeja_paisa country Colombian_cuisine An ingredient in the dish Bandeja_paisa is Chorizo , the dish comes from the Antioquia_Department and is typical Colombian_cuisine .
apposition Ajoblanco country Spain Water is an ingredient in Ajoblanco , which is a food found in Andalusia , in Spain .
apposition Arròs_negre country Spain Cephalopod_ink is an ingredient in Arròs_negre . A dish which is from the Catalonia region , in Spain .
apposition Arròs_negre country Spain Arròs_negre contains White_rice and is a traditional dish from the Catalonia region of Spain .
apposition Arròs_negre country Spain Arròs_negre is a dish from the Valencian_Community in Spain and contains Squid .
apposition Baked_Alaska country France Christmas_pudding is an ingredient in Baked_Alaska , that is from France and New_York region .
apposition Baked_Alaska country United_States Sponge_cake is one of the ingredients in Baked_Alaska , a dish from the New_York region and found in the United_States .
apposition Baked_Alaska country France Baked_Alaska , containing Christmas_pudding , is from France and Hong_Kong .
apposition Bandeja_paisa country Colombian_cuisine Pork_belly is one of the ingredients of the dish Bandeja_paisa . This is a traditional dish from the Paisa_Region and is part of Colombian_cuisine .
apposition Beef_kway_teow country Indonesia An ingredient of Beef_kway_teow , which originates from Singapore and is a popular food of Indonesia , is Palm_sugar .
direct_object Ajoblanco country Spain Bread is an ingredient of Ajoblanco , which comes from the Andalusia region , in Spain .
direct_object Ajoblanco country Spain Almond is an ingredient in Ajoblanco , which is from Andalusia in Spain .
direct_object Amatriciana_sauce country Italy Tomato is an ingredient of Amatriciana_sauce which comes from the Lazio region of Italy .
direct_object Arròs_negre country Spain Cephalopod_ink is an ingredient in the dish Arròs_negre which comes from the region of the Valencian_Community in Spain .
relative_subject Baked_Alaska country France Sponge_cake is an ingredient of Baked_Alaska , which it is claimed originated in New_York and France .
direct_object Bakewell_pudding dishVariation Bakewell_tart Frangipane is an ingredient of Bakewell_tart which is a variation of Bakewell_pudding . It is popular in the area of the Derbyshire_Dales .
direct_object Bandeja_paisa country Colombian_cuisine Kidney_bean are an ingredient of Bandeja_paisa which is a Colombian_cuisine from the Antioquia_Department .
direct_object Beef_kway_teow country Singapore Sesame_oil is an ingredient in Beef_kway_teow which is served in the region of Indonesia and is a popular dish in Singapore .
direct_object Arròs_negre country Spain Arròs_negre , a traditional dish , comes from the region of the Valencian_Community in Spain . Cephalopod_ink is an ingredient in it .
direct_object Ayam_penyet country Indonesia Fried_chicken is one of the ingredients in the dish Ayam_penyet . This is a dish from both Malaysia and Indonesia .
direct_object Bandeja_paisa country Colombian_cuisine Bandeja_paisa is a dish from the Antioquia_Department region , one of the ingredients is Chicharrón , Bandeja_paisa is part of Colombian_cuisine .
direct_object Bandeja_paisa country Colombian_cuisine Bandeja_paisa is a traditional dish from Paisa_Region , Colombian_cuisine . The ingredients found in this dish is Chicharrón .
direct_object Beef_kway_teow country Singapore One of the ingredients of the Singapore dish Beef_kway_teow is Sesame_oil . The dish is also eaten in Indonesia .
passive_voice Ayam_penyet country Indonesia Fried_chicken is one of the ingredients in the dish Ayam_penyet . This is a dish from both Malaysia and Indonesia .
passive_voice Bakewell_pudding dishVariation Bakewell_tart The Bakewell_tart originates from the Derbyshire_Dales region , has Frangipane as one of it ' s ingredients and Bakewell_pudding as a variant .
passive_voice Bandeja_paisa country Colombian_cuisine Bandeja_paisa is a dish from the Antioquia_Department region , one of the ingredients is Chicharrón , Bandeja_paisa is part of Colombian_cuisine .
direct_object Bionico country Mexico Bionico , from the Guadalajara region of Mexico , is made with Sour_cream .
passive_voice Bionico country Mexico Bionico is from the Guadalajara region of Mexico and it contains Condensed_milk .
coordinated_full_clauses Arròs_negre country Spain Cephalopod_ink is an ingredient in Arròs_negre . A dish which is from the Catalonia region , in Spain .
coordinated_full_clauses Arròs_negre country Spain Arròs_negre made with Squid is a traditional dish from the region of the Valencian_Community in Spain .
coordinated_full_clauses Fried_chicken mainIngredients Chicken Chicken is the main ingredient of Fried_chicken . Fried_chicken is one of the ingredients in the dish , from Singapore , called Ayam_penyet .
coordinated_full_clauses Ayam_penyet country Indonesia Fried_chicken is an ingredient in Ayam_penyet which is from Indonesia and a popular Malaysia dish .
coordinated_full_clauses Baked_Alaska country United_States An ingredient of Baked_Alaska is Sponge_cake , the dish originates from New_York , United_States .
coordinated_full_clauses Baked_Alaska country France Christmas_pudding is an ingredient in Baked_Alaska which is from both Hong_Kong and France .
coordinated_full_clauses Bandeja_paisa country Colombian_cuisine Avocado is one of the ingredients in the dish Bandeja_paisa , which is a dish from Colombian_cuisine . It originates from the Paisa_Region .
coordinated_full_clauses Bandeja_paisa country Colombian_cuisine Chorizo is an ingredient in the Colombian_cuisine of Bandeja_paisa which originated in the Antioquia_Department .
coordinated_full_clauses Bionico country Mexico Granola is one of the ingredients of the food Bionico which comes from the region of Jalisco in Mexico .
juxtaposition Arròs_negre country Spain Arròs_negre comes from the region of the Valencian_Community , is a traditional dish from Spain and has Cephalopod_ink as an ingredient .
juxtaposition Antioquia_Department country Colombia Antioquia_Department , Colombia , is the regions where Bandeja_paisa comes from , it includes Lemon .
juxtaposition Bhajji country India Bhajji is a dish which contains Vegetable from the Karnataka region of India .
juxtaposition Chicharrón country Spain Chicharrón is a dish traditional in Spain . Bandeja_paisa , which has Chicharrón as an ingredient , is a traditional dish from the Paisa_Region .
relative_subject Ayam_penyet country Indonesia Ayam_penyet ( Indonesia , Singapore ) has Fried_chicken in it .
relative_subject Bandeja_paisa country Colombian_cuisine Bandeja_paisa , a typical Colombian_cuisine from the Antioquia_Department , contains Pork_belly .
direct_object Ajoblanco country Spain Ajoblanco is made from Olive_oil and is found in Andalusia in Spain .
coordinated_clauses Arròs_negre country Spain Arròs_negre is a dish from the Valencian_Community in Spain and contains Squid .
coordinated_clauses Bakewell_pudding dishVariation Bakewell_tart The Bakewell_tart originates from the Derbyshire_Dales region , has Frangipane as one of it ' s ingredients and Bakewell_pudding as a variant .
coordinated_clauses Bandeja_paisa country Colombian_cuisine Bandeja_paisa is typical Colombian_cuisine from the Antioquia_Department region and contains Lemon as an ingredient .
coordinated_clauses Bionico country Mexico Bionico is a food from the Guadalajara region of Mexico and contains the ingredient of Sour_cream .
apposition Ayam_penyet country Indonesia The dish Ayam_penyet has the ingredient of Fried_chicken and originates from Malaysia and Indonesia .
apposition Bionico country Mexico Containing Raisin , Bionico is found in the region of Jalisco , in Mexico .
coordinated_full_clauses Bhajji country India Bhajji come from the Karnataka region of India , it includes Vegetable .
possessif Ajoblanco country Spain Garlic is and ingredient of Ajoblanco , a food found in Andalusia , in Spain .
possessif Bandeja_paisa country Colombian_cuisine Cooking_plantain is an ingredient in Bandeja_paisa , which is part of Colombian_cuisine and is found in the Antioquia_Department .
passive_voice Amatriciana_sauce country Italy Guanciale is an ingredient in Amatriciana_sauce which comes from the Lazio region in Italy .
passive_voice Ayam_penyet country Java Fried_chicken is an ingredient in Ayam_penyet which is from the country of Java and also the region of Singapore .
passive_voice Baked_Alaska country France Christmas_pudding is an ingredient in Baked_Alaska which is from both Hong_Kong and France .
passive_voice Bandeja_paisa country Colombian_cuisine Avocado is one of the ingredients in Bandeja_paisa which is typical Colombian_cuisine and found in the Antioquia_Department .
passive_voice Bandeja_paisa country Colombian_cuisine Chorizo is an ingredient in Bandeja_paisa which is part of Colombian_cuisine and is from the Paisa_Region .
passive_voice Bionico country Mexico Bionico , which contains Raisin , is from the Guadalajara region of Mexico .
passive_voice Chicharrón country Spain Chicharrón is an ingredient in Bandeja_paisa which comes from the Antioquia_Department District in Spain .
passive_voice Ajoblanco country Spain Ajoblanco is a dish from the Andalusia region of Spain and includes the ingredient Garlic .
passive_voice Antioquia_Department country Colombia Bandeja_paisa containing Chicharrón is from the Antioquia_Department in Colombia .
passive_voice Arròs_negre country Spain Arròs_negre , containing White_rice , is from the Catalonia region in Spain .
passive_voice Bhajji country India Bhajji come from the Karnataka region of India , it includes Vegetable .
apposition Almond family Rosaceae Almond is classed as a Flowering_plant and is from the Rosaceae family . It is an ingredient used in Ajoblanco .
direct_object Tomato family Solanaceae An ingredient found in Arrabbiata_sauce is the Tomato , which belongs to the Solanaceae family and is pat of the Flowering_plant division .
coordinated_clauses Tomato order Solanales The Tomato , used in an Amatriciana_sauce , belongs to the family Solanaceae and is of the order Solanales .
relative_subject Celery genus Apium Celery is from the genus Apium and the family Apiaceae . It is an ingredient of Bakso .
apposition Almond order Rosids Almond , which is one of the ingredients of Ajoblanco , is part of the Rosaceae family and also part of the order of Rosids .
direct_object Tomato genus Solanum The Tomato belongs to the genus Solanum and the Solanaceae plant family . It is a main ingredient in the dish Amatriciana_sauce .
relative_subject Almond family Rosaceae Almond , which is one of the ingredients of Ajoblanco , is part of the Rosaceae family and also part of the order of Rosids .
relative_subject Binignit mainIngredients Taro The main ingredients of Binignit are Taro and Sweet_potato ( of the order Solanales ).
relative_adverb Beef_kway_teow country "Singapore_and_Indonesia" Tony_Tan is a leader in Singapore . Beef_kway_teow is also from "Singapore_and_Indonesia" .
coordinated_full_clauses Ajoblanco country Spain Susana_Díaz is the leader of Andalusia ( in Spain ) where Ajoblanco is from .
coordinated_full_clauses Arròs_negre country Spain Arròs_negre comes from the region of the Valencian_Community , Spain , The leader of the community of Valencia is Ximo_Puig .
coordinated_full_clauses Derbyshire_Dales isPartOf Derbyshire Bakewell_pudding originates from the Derbyshire_Dales region of Derbyshire where Patrick_McLoughlin is the leader .
possessif Beef_kway_teow country "Singapore_and_Indonesia" Tony_Tan is a leader in Singapore . Beef_kway_teow is also from "Singapore_and_Indonesia" .
direct_object Arròs_negre country Spain Arròs_negre is from the Catalonia region of Spain where the leader is Carles_Puigdemont .
passive_voice Ajoblanco country Spain Originating from Spain , Ajoblanco is a food found in Andalusia , where Susana_Díaz is the leader .
passive_voice Arròs_negre country Spain Carles_Puigdemont is the leader of Catalonia . Arròs_negre , from the Catalonia region , is a traditional dish from Spain .
relative_adverb Beef_kway_teow country Indonesia Beef_kway_teow is popular in Indonesia and is founded in Halimah_Yacob led , Singapore .
apposition Arròs_negre country Spain Carles_Puigdemont is the leader of Catalonia . Arròs_negre , from the Catalonia region , is a traditional dish from Spain .
direct_object Bakewell_pudding servingTemperature "Warm_(freshly_baked)_or_cold" Bakewell_pudding originates from the Derbyshire_Dales , it can be served "Warm_(freshly_baked)_or_cold" or cold " and its main ingredients are "Ground_almond_,_jam_,_butter_,_eggs" .
existential Ayam_penyet country Java Ayam_penyet ' s main ingredients are "Squeezed"_or_"smashed"_fried_chicken_served_with_sambal . It originates from Java and the Singapore region .
apposition Asam_pedas country Indonesia The main ingredients of Asam_pedas are "Fish_cooked_in_sour_and_hot_sauce" . Asam_pedas is a dish from Sumatra , in Indonesia .
apposition Asam_pedas country Malaysia The dish Asam_pedas comes from the region of "Sumatra_and_Malay_Peninsula" . Malaysia and the main ingredients are "Fish_cooked_in_sour_and_hot_sauce" .
apposition Asam_pedas country "Indonesia_and_Malaysia" Asam_pedas is a dish from Sumatra , "Indonesia_and_Malaysia" . Its main ingredient is "Fish_cooked_in_sour_and_hot_sauce" .
direct_object Asam_pedas country Malaysia The main ingredients of Asam_pedas , a food from the "Sumatra_and_Malay_Peninsula" regions of Malaysia , consist of "Fish_cooked_in_sour_and_hot_sauce" .
direct_object Ayam_penyet country Java Ayam_penyet originates from Malaysia and Java . The main ingredients are "Squeezed"_or_"smashed"_fried_chicken_served_with_sambal .
direct_object Bandeja_paisa country Colombian_cuisine Bandeja_paisa is a Colombian_cuisine from the Paisa_Region . The main ingredients are "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" .
direct_object Asam_pedas country Indonesia Asam_pedas is a Sumatra dish which is popular in Indonesia . The main ingredient is "Fish_cooked_in_sour_and_hot_sauce" .
relative_subject Binignit ingredient Sweet_potato Taro is the main ingredient of Binignit , along with Sweet_potato . The dish is found in the Philippines .
relative_subject Asam_pedas region Sumatra Asam_pedas is a dish from Sumatra , "Indonesia_and_Malaysia" . Its main ingredient is "Fish_cooked_in_sour_and_hot_sauce" .
coordinated_full_clauses Asam_pedas region Malay_Peninsula A dish popular in Indonesia is Asam_pedas from the Malay_Peninsula region , which is "Fish_cooked_in_sour_and_hot_sauce" .
coordinated_full_clauses Asam_pedas region "Sumatra_and_Malay_Peninsula" The main ingredients of Asam_pedas are "Fish_cooked_in_sour_and_hot_sauce" . It is popular in Indonesia and can be found in the region of "Sumatra_and_Malay_Peninsula" .
coordinated_full_clauses Asam_pedas region "Sumatra_and_Malay_Peninsula" The dish Asam_pedas comes from the region of "Sumatra_and_Malay_Peninsula" . Malaysia and the main ingredients are "Fish_cooked_in_sour_and_hot_sauce" .
coordinated_full_clauses Ayam_penyet region Malaysia Ayam_penyet ' s main ingredients are "Squeezed"_or_"smashed"_fried_chicken_served_with_sambal . It is a popular dish in Malaysia and also found in Java .
coordinated_full_clauses Batchoy ingredient Shrimp "noodles_,_pork_organs_,_vegetables_,_chicken_,_shrimp_,_beef" , Shrimp , pork organs , vegetables , chicken and beef are ingredients in the dish called Batchoy which comes from the Philippines .
coordinated_full_clauses Binignit ingredient Taro One of the main ingredients in Binignit is Banana , originating from the Philippines and has Taro as an ingredient .
juxtaposition Binignit ingredient Sweet_potato The main ingredients of Binignit are Banana and Sweet_potato and it can be found in the Philippines .
passive_voice Asam_pedas region Sumatra The main ingredients of Asam_pedas are "Fish_cooked_in_sour_and_hot_sauce" . Asam_pedas is a dish from Sumatra , in Indonesia .
apposition Asam_pedas region Malay_Peninsula Asam_pedas comes from "Indonesia_and_Malaysia" and the Malay_Peninsula . It is a dish of "Fish_cooked_in_sour_and_hot_sauce" .
apposition Batchoy ingredient Noodle Batchoy is a dish eaten in the Philippines . It is a Noodle dish which "noodles_,_pork_organs_,_vegetables_,_chicken_,_shrimp_,_beef" .
apposition Binignit ingredient Sweet_potato Sweet_potato are in the Philippines dish of Binignit along with the main ingredient of Coconut_milk .
relative_subject Asam_pedas region Sumatra The main ingredients of Asam_pedas consist of "Fish_cooked_in_sour_and_hot_sauce" . The dish originated in Sumatra and is also found in Malaysia .
coordinated_clauses Binignit ingredient Sweet_potato Sweet_potato are in the Philippines dish of Binignit along with the main ingredient of Coconut_milk .
coordinated_clauses Binignit ingredient Taro One of the main ingredients in Binignit is Banana , originating from the Philippines and has Taro as an ingredient .
apposition Amatriciana_sauce ingredient Olive_oil "Tomatoes_,_guanciale_,_cheese" , Olive_oil are the main ingredients of Amatriciana_sauce , which can be found in Italy .
direct_object Asam_pedas region Sumatra The main ingredients of Asam_pedas consist of "Fish_cooked_in_sour_and_hot_sauce" . The dish originated in Sumatra and is also found in Malaysia .
direct_object Asam_pedas region "Sumatra_and_Malay_Peninsula" Asam_pedas is a dish of "Fish_cooked_in_sour_and_hot_sauce" , it is found in the region of "Sumatra_and_Malay_Peninsula" and popular in Indonesia .
direct_object Binignit ingredient Sweet_potato The Binignit dish can be found in the Philippines . Two of the main ingredients in it are Banana and Sweet_potato .
apposition Dessert dishVariation Cookie A Cookie can be a Dessert , as can Baked_Alaska which has Christmas_pudding as an ingredient .
direct_object Dessert dishVariation Sandesh_(confectionery) Banana is an ingredient in Binignit , a type of Dessert . Sandesh_(confectionery) is a dish that can be served as a Dessert .
apposition Dessert dishVariation Cake Cake is served as a Dessert , as is Bionico , which contains the ingredient of Sour_cream .
direct_object Dessert dishVariation Cookie Binignit is a type of Dessert with the ingredient Sago , a Cookie is also a Dessert .
direct_object Dessert dishVariation Cookie Sago is one of the ingredients in the dish Binignit which is a Dessert course . Cookie are also a type of Dessert .
direct_object Dessert dishVariation Ice_cream Granola is a key ingredient in Bionico , which like Ice_cream is a type of Dessert .
relative_subject Dessert dishVariation Sandesh_(confectionery) Baked_Alaska ( which has Ice_cream as an ingredient ) and Sandesh_(confectionery) are both types of Dessert .
direct_object Dessert dishVariation Cookie Coconut_milk is an ingredient in Binignit , which should be served as the Dessert course and a Cookie is another Dessert .
relative_subject Dessert dishVariation Cookie Christmas_pudding is an ingredient in the Dessert Baked_Alaska . Cookie is also a Dessert .
relative_subject Dessert dishVariation Cookie Sponge_cake is an ingredient in the Dessert Baked_Alaska , Another type of Dessert is a Cookie .
relative_subject Dessert dishVariation Sandesh_(confectionery) Meringue is an ingredient of the Dessert Baked_Alaska . Sandesh_(confectionery) is also a Dessert .
relative_subject Dessert dishVariation Cookie Cookie and Binignit are types of Dessert . A key ingredient of Binignit is Sago .
apposition Dessert dishVariation Sandesh_(confectionery) Sandesh_(confectionery) can be served as a Dessert , as can Binignit which contains the ingredient of Sago .
coordinated_full_clauses Dessert dishVariation Sandesh_(confectionery) Sandesh_(confectionery) and Bionico are Dessert where Bionico has Granola as an ingredient .
possessif Bacon_Explosion mainIngredients Sausage Bacon_Explosion is a "Main_course" dish that includes the ingredients Sausage and Bacon .
possessif Dessert dishVariation Sandesh_(confectionery) Sandesh_(confectionery) and Baked_Alaska are Dessert . Baked_Alaska uses Christmas_pudding as an ingredient .
possessif Dessert dishVariation Cookie Cookie and Binignit ( which contains Sweet_potato ) are types of Dessert .
coordinated_full_clauses Bionico dishVariation Cottage_cheese The Dessert of Bionico contains Raisin and can be varied by adding Cottage_cheese .
passive_voice Dessert dishVariation Sandesh_(confectionery) Sago is a key ingredient of a dish called Binignit which like Sandesh_(confectionery) is a Dessert .
passive_voice Dessert dishVariation Cookie Cookie is a type of Dessert , sames as Binignit , that is an ingredient found in Taro .
direct_object Sweet_potato order Solanales One of the main ingredients of Binignit is Coconut_milk . Another ingredient is Sweet_potato which is from the order Solanales .
existential Bacon_Explosion country United_States The Bacon_Explosion originates from the United_States and the ingredients include Bacon and Sausage .
coordinated_full_clauses Sweet_potato order Solanales The order of Solanales contains Sweet_potato which , along with Sago , is an ingredient in Binignit .
passive_voice Binignit country Philippines Taro is an ingredient in the Philippines dish of Binignit along with the main ingredient of Sweet_potato .
possessif Binignit country Philippines Banana is one of the main ingredients in the dish Binignit which is an ingredient in Taro , the dish is from the Philippines .
coordinated_full_clauses Sweet_potato order Solanales The main ingredient of Binignit is Coconut_milk . Sweet_potato from the order of Solanales are also included .
coordinated_full_clauses Binignit country Philippines The main ingredient of Binignit is Banana , along with Sago . The dish can be found in the Philippines .
coordinated_clauses Binignit country Philippines The Binignit dish can be found in the Philippines . Two of the main ingredients in it are Banana and Sweet_potato .
coordinated_clauses Sweet_potato order Solanales One of the main ingredients of Binignit is Coconut_milk . Another ingredient is Sweet_potato which is from the order Solanales .
coordinated_clauses Binignit country Philippines Taro is an ingredient in the Philippines dish of Binignit along with the main ingredient of Sweet_potato .
coordinated_clauses Sweet_potato order Solanales The main ingredients of Binignit are Sweet_potato , that is of the order Solanales and Coconut_milk .
direct_object Sweet_potato order Solanales Sweet_potato are part of the order of Solanales and are one of the main ingredients of Binignit , along with Banana .
coordinated_clauses Binignit country Philippines The Philippines dish of Binignit contains Sago and Sweet_potato .
relative_subject Bacon_sandwich country United_Kingdom Popular in the United_Kingdom , Bacon_sandwich ( which can have Ketchup as an ingredient ) are also can also be known as a "Bacon_butty_,_bacon_sarnie_,_rasher_sandwich_,_bacon_sanger_,_piece_'n_bacon_,_bacon_cob_,_bacon_barm_,_bacon_muffin" .
passive_voice Ajoblanco country Spain Ajoblanco ( alternatively known as "Ajo_blanco" ) comes from Spain and has Garlic as an ingredient .
passive_voice Ajoblanco country Spain Bread is an ingredient of Ajoblanco , which is from Spain and an alternative name to "Ajo_blanco" .
direct_object Bakewell_tart region Derbyshire_Dales Fruit_preserves are an ingredient of Bakewell_tart ( also called Bakewell_pudding ) and is popular in the Derbyshire_Dales area .
direct_object Bakewell_pudding region Derbyshire_Dales Frangipane is an ingredient in a Bakewell_tart , it is a variation of Bakewell_pudding , that originates from the Derbyshire_Dales .
coordinated_full_clauses Bakewell_pudding region Derbyshire_Dales Frangipane is an ingredient of Bakewell_tart , a variant of Bakewell_pudding , from the Derbyshire_Dales region .
relative_subject Bakewell_tart region Derbyshire_Dales The Bakewell_tart originates from the Derbyshire_Dales region , has Frangipane as one of it ' s ingredients and Bakewell_pudding as a variant .
direct_object Arrabbiata_sauce country Italy Garlic is an ingredient in Arrabbiata_sauce ( found in Italy ) , which can also have Oregano added to it .
relative_subject Bionico course Dessert The Dessert dish Bionico requires Granola as one of it ' s ingredients and can be varied using Cottage_cheese .
existential Bacon_sandwich country United_Kingdom Bacon_sandwich are made using Bread and originate from the United_Kingdom . A variation is known as the BLT .
existential Baked_Alaska country France Baked_Alaska , a Cookie type Dessert , comes from France .
direct_object Baked_Alaska ingredient Sponge_cake Sponge_cake is an ingredient of Baked_Alaska , a Dessert . Sandesh_(confectionery) is a nice confectionery Dessert .
direct_object Binignit ingredient Sago Binignit is a type of Dessert with the ingredient Sago , a Cookie is also a Dessert .
direct_object Binignit ingredient Taro Taro is an ingredient in Binignit which is a Dessert . Similarly , a Cookie is a Dessert .
coordinated_full_clauses Baked_Alaska ingredient Ice_cream An ingredient of Baked_Alaska is Ice_cream which , along with Cookie , are Dessert .
coordinated_full_clauses Baked_Alaska ingredient Sponge_cake Sponge_cake is a part of the Dessert Baked_Alaska , Cookie are also a Dessert .
coordinated_full_clauses Baked_Alaska ingredient Christmas_pudding Christmas_pudding is an ingredient in Baked_Alaska , which is a Dessert , same as Sandesh_(confectionery) .
coordinated_full_clauses Binignit ingredient Taro Taro is an ingredient in Binignit which is a Dessert . Similarly , a Cookie is a Dessert .
coordinated_full_clauses Bionico ingredient Granola Granola is an ingredient of Bionico , a dish served as an alternative to Sandesh_(confectionery) for Dessert .
passive_voice Baked_Alaska ingredient Ice_cream Sandesh_(confectionery) and Baked_Alaska ( which contains Ice_cream ) are both Dessert .
passive_voice Binignit ingredient Sweet_potato Binignit is a Dessert which contains Sweet_potato . Sandesh_(confectionery) is also a Dessert .
passive_voice Baked_Alaska ingredient Sponge_cake Sponge_cake is an ingredient of Baked_Alaska , a Dessert . Sandesh_(confectionery) is a nice confectionery Dessert .
passive_voice Bionico ingredient Raisin Sandesh_(confectionery) and Bionico are Dessert , Bionico contains Raisin .
relative_subject Tomato family Solanaceae The Tomato belongs to the genus Solanum and the Solanaceae plant family . It is a main ingredient in the dish Amatriciana_sauce .
direct_object Celery family Apiaceae One ingredient in Bakso is Celery , which is a member of the genus Apium in the family Apiaceae .
passive_voice Arrabbiata_sauce ingredient Garlic Garlic is an ingredient in Arrabbiata_sauce ( found in Italy ) , which can also have Oregano added to it .
relative_subject Siomay ingredient Peanut_sauce Shumai is a variation of Batagor and Siomay . One of the ingredients in Siomay is Peanut_sauce .
relative_object Switzerland leaderName Johann_Schneider-Ammann The Accademia_di_Architettura_di_Mendrisio is in Switzerland . The Swiss anthem is the Swiss_Psalm and its leader is Johann_Schneider-Ammann .
passive_voice Auburn_,_Alabama isPartOf Lee_County_,_Alabama Auburn_,_Alabama , Lee_County_,_Alabama is in Alabama in the United_States .
apposition Acharya_Institute_of_Technology established 2000 The Acharya_Institute_of_Technology is located in Karnataka , "India" and it was established in 2000 .
relative_subject School_of_Business_and_Social_Sciences_at_the_Aarhus_University academicStaffSize 737 The School_of_Business_and_Social_Sciences_at_the_Aarhus_University was established in 1928 . It has 16000 students and an academic staff of 737 .
relative_subject School_of_Business_and_Social_Sciences_at_the_Aarhus_University country Denmark School_of_Business_and_Social_Sciences_at_the_Aarhus_University in Denmark was established in 1928 and currently has a student body of 16000 .
possessif AWH_Engineering_College state Kerala AWH_Engineering_College in Kerala , India has a staff of 250 .
coordinated_clauses AWH_Engineering_College country India AWH_Engineering_College was established in 2001 and is located in India . It currently has 250 members of staff .
coordinated_clauses School_of_Business_and_Social_Sciences_at_the_Aarhus_University latinName "Universitas_Aarhusiensis" The School_of_Business_and_Social_Sciences_at_the_Aarhus_University ( Latin name - "Universitas_Aarhusiensis" ) was established in 1928 and it is affiliated with the European_University_Association .
coordinated_clauses Aaron_S._Daggett birthPlace Greene_,_Maine Born in Greene_,_Maine , Aaron_S._Daggett , fought in the Battle_of_Gettysburg and was awarded the Purple_Heart .
apposition Aaron_S._Daggett birthPlace Greene_,_Maine Born in Greene_,_Maine , Aaron_S._Daggett fought in battles during the American_Civil_War and was awarded the Purple_Heart .
apposition Aaron_S._Daggett birthPlace Maine Maine born Aaron_S._Daggett fought in the Battle_of_the_Wilderness and was awarded the Purple_Heart .
relative_subject Aaron_S._Daggett birthPlace Greene_,_Maine Aaron_S._Daggett , born in Greene_,_Maine , fought in the American_Civil_War and was awarded the Purple_Heart .
passive_voice Airey_Neave activeYearsEndDate 1979-03-30 Airey_Neave fought in the Battle_of_France and given the Territorial_Decoration . His career ended 1979-03-30 .
possessif Aaron_S._Daggett birthPlace Maine Aaron_S._Daggett was born in Maine , fought in the Battle_of_the_Wilderness and was awarded the Purple_Heart .
direct_object Adam_Holloway militaryBranch Grenadier_Guards John_Major was a commander in the Gulf_War in which Adam_Holloway of the Grenadier_Guards was involved .
direct_object Battle_of_Salem_Church isPartOfMilitaryConflict American_Civil_War The Battle_of_Salem_Church , in which Aaron_S._Daggett fought and in which Robert_E._Lee was a commander , was part of the American_Civil_War .
relative_object Adam_Koc birthPlace Congress_Poland Born in Congress_Poland , Adam_Koc fought in the Polish–Soviet_War , which Joseph_Stalin was a commander in .
relative_adverb Adam_Holloway militaryBranch Grenadier_Guards Adam_Holloway was a Grenadier_Guards during the Gulf_War and Colin_Powell was a commander .
relative_adverb Adam_Koc birthPlace Congress_Poland Born in Congress_Poland , Adam_Koc , fought in the Polish–Soviet_War , during which , Joseph_Stalin was a commander .
relative_adverb Albert_Jennings_Fountain birthPlace United_States The United_States was the birthplace of Albert_Jennings_Fountain , who fought in the American_Civil_War . A commander in the American_Civil_War was Jefferson_Davis .
relative_adverb Battle_of_Salem_Church isPartOfMilitaryConflict American_Civil_War Robert_E._Lee was a commander in the Battle_of_Salem_Church , which was one of the military conflicts in the American_Civil_War . Aaron_S._Daggett fought in the same battle .
direct_object Albert_Jennings_Fountain militaryBranch 1st_Regiment_New_Mexico_Volunteer_Cavalry Albert_Jennings_Fountain belonged to the 1st_Regiment_New_Mexico_Volunteer_Cavalry and battled in the American_Civil_War , under commander Abraham_Lincoln .
relative_subject World_War_II place South-East_Asian_theatre_of_World_War_II Benito_Mussolini was a commander in World_War_II which Airey_Neave fought in . One of the places in World_War_II was named South-East_Asian_theatre_of_World_War_II .
coordinated_full_clauses Adam_Holloway militaryBranch Grenadier_Guards Adam_Holloway was in the Grenadier_Guards in the military and was involved in Gulf_War battles . Colin_Powell was a commander in the Gulf_War .
coordinated_full_clauses Adam_Koc birthPlace Congress_Poland Born in Congress_Poland , Adam_Koc , fought in the Polish–Soviet_War . A commander at the Polish–Soviet_War was Joseph_Stalin .
coordinated_full_clauses Albert_Jennings_Fountain birthPlace United_States Abraham_Lincoln was a commander in the American_Civil_War , where United_States born Albert_Jennings_Fountain fought .
passive_voice Albert_Jennings_Fountain militaryBranch 1st_Regiment_New_Mexico_Volunteer_Cavalry Albert_Jennings_Fountain served in the 1st_Regiment_New_Mexico_Volunteer_Cavalry and fought in the American_Civil_War , where Abraham_Lincoln was a commander .
passive_voice Battle_of_Salem_Church isPartOfMilitaryConflict American_Civil_War The Battle_of_Salem_Church , where Aaron_S._Daggett fought and Robert_E._Lee was a commander , was part of the American_Civil_War .
relative_adverb Adam_Holloway militaryBranch Grenadier_Guards During the Gulf_War George_H._W._Bush was a commander and Adam_Holloway was involved in the battles whilst serving in the Grenadier_Guards .
apposition Albert_Jennings_Fountain birthPlace Staten_Island Albert_Jennings_Fountain , who was born on Staten_Island , fought in the American_Civil_War , where Abraham_Lincoln was a commander .
relative_object Albert_Jennings_Fountain birthPlace New_York Albert_Jennings_Fountain was born in New_York and fought in the American_Civil_War when Abraham_Lincoln was a commander .
apposition Adam_Holloway militaryBranch Grenadier_Guards During the Gulf_War George_H._W._Bush was a commander and Adam_Holloway was involved in the battles whilst serving in the Grenadier_Guards .
apposition Albert_Jennings_Fountain birthPlace United_States Born in the United_States , Albert_Jennings_Fountain , fought in the American_Civil_War . Jefferson_Davis was a commander in the American_Civil_War .
passive_voice Albert_Jennings_Fountain militaryBranch Union_Army Albert_Jennings_Fountain served in the Union_Army and fought in the American_Civil_War , where Jefferson_Davis was one of the commanders .
passive_voice Albert_Jennings_Fountain militaryBranch Union_Army Albert_Jennings_Fountain was a member of the Union_Army that fought in the American_Civil_War . Abraham_Lincoln was a commander in that war .
apposition Adam_Holloway militaryBranch Grenadier_Guards Adam_Holloway served in the Grenadier_Guards and fought in the Gulf_War . George_H._W._Bush was a commander in the Gulf_War .
apposition Allan_Shivers militaryBranch United_States_Army Allan_Shivers , served in the United_States_Army and fought battles in World_War_II . One of the commanders in World_War_II , was Joseph_Stalin .
coordinated_clauses Abdul_Taib_Mahmud residence Sarawak Abdul_Taib_Mahmud , a member of the "Barisan_Ra'ayat_Jati_Sarawak" party , was born in Miri_,_Malaysia and lives in Sarawak .
relative_subject Abdul_Taib_Mahmud residence Sarawak Abdul_Taib_Mahmud was born in Miri_,_Malaysia but lives in Sarawak . He was part of the Parti_Pesaka_Bumiputera_Bersatu .
relative_subject Abdul_Taib_Mahmud residence Sarawak Born in Miri_,_Malaysia and living in , Sarawak , Abdul_Taib_Mahmud , is a member of "Barisan_Ra'ayat_Jati_Sarawak" party .
possessif Abdul_Taib_Mahmud residence Kuching Abdul_Taib_Mahmud was born in Miri_,_Malaysia , now lives in Kuching and is a member of the Parti_Pesaka_Bumiputera_Bersatu .
apposition Ab_Klink almaMater Erasmus_University_Rotterdam Ab_Klink was born in the Netherlands , belongs to the Christian_Democratic_Appeal party and graduated from Erasmus_University_Rotterdam .
apposition Abdul_Taib_Mahmud birthPlace Kingdom_of_Sarawak Born in the Kingdom_of_Sarawak Abdul_Taib_Mahmud , was succeeded by Abdul_Rahman_Ya'kub , who was in office whilst Tuanku_Bujang_Tuanku_Othman was Vice President .
direct_object Abdul_Taib_Mahmud birthPlace Kingdom_of_Sarawak Born in the Kingdom_of_Sarawak Abdul_Taib_Mahmud , was succeeded by Abdul_Rahman_Ya'kub , who was in office whilst Tuanku_Bujang_Tuanku_Othman was Vice President .
direct_object Abdul_Taib_Mahmud birthPlace Miri_,_Malaysia Miri_,_Malaysia , is the birthplace of Abdul_Taib_Mahmud , who was succeeded by Abdul_Rahman_Ya'kub . Abdul_Rahman_Ya'kub was in office whilst Tuanku_Bujang_Tuanku_Othman was Vice President .
coordinated_clauses Abdul_Taib_Mahmud party Parti_Pesaka_Bumiputera_Bersatu Abdul_Taib_Mahmud was born in Miri_,_Malaysia and lives in "Demak_Jaya_,_Jalan_Bako_,_Kuching_,_Sarawak" . He belongs to the Parti_Pesaka_Bumiputera_Bersatu .
relative_subject Abdul_Taib_Mahmud party "Barisan_Ra'ayat_Jati_Sarawak" Born in Miri_,_Malaysia and living in , Sarawak , Abdul_Taib_Mahmud , is a member of "Barisan_Ra'ayat_Jati_Sarawak" party .
relative_subject Abdul_Taib_Mahmud residence "Demak_Jaya_,_Jalan_Bako_,_Kuching_,_Sarawak" Abdul_Taib_Mahmud was born in Miri_,_Malaysia and resides in "Demak_Jaya_,_Jalan_Bako_,_Kuching_,_Sarawak" . He was succeeded by Abdul_Rahman_Ya'kub .
relative_subject Alberto_Teisaire office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Alberto_Teisaire , born in Mendoza_,_Argentina , served as a office workedAt workedAsObjet1 and was succeeded by Isaac_Rojas .
coordinated_full_clauses Abdul_Taib_Mahmud residence "Demak_Jaya_,_Jalan_Bako_,_Kuching_,_Sarawak" Abdul_Taib_Mahmud , born in the Kingdom_of_Sarawak where he lives in "Demak_Jaya_,_Jalan_Bako_,_Kuching_,_Sarawak" . He was succeeded by Sulaiman_Abdul_Rahman_Taib .
coordinated_full_clauses Abraham_A._Ribicoff office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Abraham_A._Ribicoff was born in the United_States and was the office workedAt workedAsObjet1 before being succeeded by Anthony_J._Celebrezze .
possessif Abdul_Rahman_Ya'kub hasDeputy Stephen_Yong_Kuet_Tze Abdul_Taib_Mahmud was born in the Kingdom_of_Sarawak . Stephen_Yong_Kuet_Tze is the deputy to Abdul_Rahman_Ya'kub who was succeeded by Abdul_Taib_Mahmud .
possessif Alberto_Teisaire profession Rear_admiral Alberto_Teisaire was born in Mendoza_,_Argentina and was a Rear_admiral who was succeeded by Isaac_Rojas .
coordinated_clauses Abraham_A._Ribicoff office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Abraham_A._Ribicoff was born in the United_States and was the office workedAt workedAsObjet1 before being succeeded by Anthony_J._Celebrezze .
coordinated_clauses William_M._O._Dawson deathPlace Charleston_,_West_Virginia Albert_B._White , born in Cleveland , was succeeded by William_M._O._Dawson who died in Charleston_,_West_Virginia .
apposition Abdul_Rahman_Ya'kub inOfficeWhileVicePresident Tuanku_Bujang_Tuanku_Othman Miri_,_Malaysia , is the birthplace of Abdul_Taib_Mahmud , who was succeeded by Abdul_Rahman_Ya'kub . Abdul_Rahman_Ya'kub was in office whilst Tuanku_Bujang_Tuanku_Othman was Vice President .
relative_subject Antonis_Samaras inOfficeWhilePrimeMinister Konstantinos_Mitsotakis Born in Athens and succeeded by Makis_Voridis , Adonis_Georgiadis was in office while Antonis_Samaras was Prime Minister . In turn , Antonis_Samaras , was in office while Konstantinos_Mitsotakis was Prime Minister .
existential Abraham_A._Ribicoff office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Abraham_A._Ribicoff , who was born in the United_States . worked as the office workedAt workedAsObjet1 , where he was succeeded by John_N._Dempsey .
apposition Abdul_Taib_Mahmud residence "Demak_Jaya_,_Jalan_Bako_,_Kuching_,_Sarawak" Abdul_Taib_Mahmud was born in Miri_,_Malaysia and resides in "Demak_Jaya_,_Jalan_Bako_,_Kuching_,_Sarawak" . He was succeeded by Abdul_Rahman_Ya'kub .
apposition William_M._O._Dawson deathPlace Charleston_,_West_Virginia Albert_B._White , born in Cleveland , was succeeded by William_M._O._Dawson who died in Charleston_,_West_Virginia .
apposition Alfred_N._Phillips office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Born in Connecticut , Alfred_N._Phillips was the office workedAt workedAsObjet1 and was succeeded by , Albert_E._Austin .
existential Abdul_Taib_Mahmud residence "Demak_Jaya_,_Jalan_Bako_,_Kuching_,_Sarawak" Abdul_Taib_Mahmud was born in Miri_,_Malaysia and resides in "Demak_Jaya_,_Jalan_Bako_,_Kuching_,_Sarawak" . He was succeeded by Abdul_Rahman_Ya'kub .
existential Abraham_A._Ribicoff office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Anthony_J._Celebrezze succeeded United_States born Abraham_A._Ribicoff as office workedAt workedAsObjet1 .
existential Abraham_A._Ribicoff office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Abraham_A._Ribicoff was born in the United_States and worked as the office workedAt workedAsObjet1 . He was succeeded by Anthony_J._Celebrezze .
existential Alberto_Teisaire profession Rear_admiral Rear_admiral , Alberto_Teisaire , was born in Mendoza_,_Argentina and succeeded by , Isaac_Rojas .
direct_object William_M._O._Dawson deathPlace Charleston_,_West_Virginia William_M._O._Dawson , who was succeeded by Albert_B._White , was born in Bloomington_,_Maryland and died in Charleston_,_West_Virginia .
apposition Abdulsalami_Abubakar successor Olusegun_Obasanjo Abdulsalami_Abubakar served in the Nigerian_Army , was in office while Mike_Akhigbe was Vice President and was later succeeded by Olusegun_Obasanjo .
relative_subject Abdulsalami_Abubakar militaryBranch Nigerian_Army Abdulsalami_Abubakar served in the Nigerian_Army , was in office while Mike_Akhigbe was Vice President and was later succeeded by Olusegun_Obasanjo .
direct_object Abdulsalami_Abubakar militaryBranch Nigerian_Air_Force Abdulsalami_Abubakar ; served in the Nigerian_Air_Force , was in office while Mike_Akhigbe was Vice President and was succeeded by , Olusegun_Obasanjo .
relative_subject Abraham_A._Ribicoff spouse "Ruth_Ribicoff" Abraham_A._Ribicoff worked as office workedAt workedAsObjet1 . He was born in the United_States and is the spouse of , "Ruth_Ribicoff" .
existential Abraham_A._Ribicoff spouse "Ruth_Ribicoff" "Ruth_Ribicoff" was married to Abraham_A._Ribicoff who was born in Connecticut and worked as office workedAt workedAsObjet1 .
existential Abraham_A._Ribicoff successor Anthony_J._Celebrezze Abraham_A._Ribicoff was born in the United_States and worked as the office workedAt workedAsObjet1 . He was succeeded by Anthony_J._Celebrezze .
existential Abraham_A._Ribicoff successor Anthony_J._Celebrezze Abraham_A._Ribicoff was born in the United_States and before being succeeded by Anthony_J._Celebrezze worked as office workedAt workedAsObjet1 .
passive_voice Agnes_Kant almaMater Radboud_University_Nijmegen Born in Hessisch_Oldendorf , Agnes_Kant was a office workedAt workedAsObjet1 and her Alma mater is Radboud_University_Nijmegen .
existential Alberto_Teisaire successor Isaac_Rojas Alberto_Teisaire , born in Mendoza_,_Argentina , served as a office workedAt workedAsObjet1 and was succeeded by Isaac_Rojas .
apposition Abraham_A._Ribicoff spouse "Ruth_Ribicoff" Abraham_A._Ribicoff worked as office workedAt workedAsObjet1 . He was born in the United_States and is the spouse of , "Ruth_Ribicoff" .
coordinated_clauses Abraham_A._Ribicoff spouse "Ruth_Ribicoff" Abraham_A._Ribicoff ; was born in Connecticut , is married to "Ruth_Ribicoff" and worked as office workedAt workedAsObjet1 .
coordinated_clauses Abraham_A._Ribicoff successor Anthony_J._Celebrezze Abraham_A._Ribicoff was born in the United_States and was the office workedAt workedAsObjet1 before being succeeded by Anthony_J._Celebrezze .
passive_voice Agnes_Kant almaMater Radboud_University_Nijmegen Agnes_Kant , born in West_Germany , graduated from Radboud_University_Nijmegen and worked as a office workedAt workedAsObjet1 .
coordinated_clauses Alberto_Teisaire party Justicialist_Party Alberto_Teisaire was born in Mendoza_,_Argentina and was a member of the Justicialist_Party . He was also the office workedAt workedAsObjet1 .
apposition Alvah_Sabin region Vermont's_3rd_congressional_district Alvah_Sabin ; was born in Georgia_,_Vermont , represented Vermont's_3rd_congressional_district and worked as the office workedAt workedAsObjet1 .
passive_voice Alvah_Sabin region Vermont's_3rd_congressional_district Alvah_Sabin ; was born in Georgia_,_Vermont , represented Vermont's_3rd_congressional_district and worked as the office workedAt workedAsObjet1 .
coordinated_full_clauses Alberto_Teisaire profession Rear_admiral Alberto_Teisaire was born in Mendoza_,_Argentina as a office workedAt workedAsObjet1 . He is a Rear_admiral .
relative_subject Abraham_A._Ribicoff successor Anthony_J._Celebrezze Abraham_A._Ribicoff , born in New_Britain_,_Connecticut , worked as the office workedAt workedAsObjet1 and was succeeded by Anthony_J._Celebrezze .
relative_subject Abraham_A._Ribicoff spouse Casey_Ribicoff Abraham_A._Ribicoff , born in the United_States , was married to Casey_Ribicoff . He worked as the office workedAt workedAsObjet1 .
passive_voice Abraham_A._Ribicoff birthPlace New_Britain_,_Connecticut Abraham_A._Ribicoff , born New_Britain_,_Connecticut , once worked for the office workedAt workedAsObjet1 office and was succeeded by , John_N._Dempsey .
relative_subject Abraham_A._Ribicoff birthPlace New_Britain_,_Connecticut Born in New_Britain_,_Connecticut , Abraham_A._Ribicoff worked as the office workedAt workedAsObjet1 and was succeeded by , Anthony_J._Celebrezze .
relative_subject Adonis_Georgiadis birthPlace Greece Adonis_Georgiadis was born in Athens , Greece and worked for the office workedAt workedAsObjet1 . He was succeeded by Makis_Voridis .
apposition Abraham_A._Ribicoff birthPlace United_States Abraham_A._Ribicoff was born in the United_States and worked as the office workedAt workedAsObjet1 . He was succeeded by Anthony_J._Celebrezze .
apposition Adonis_Georgiadis birthPlace Greece Adonis_Georgiadis was born in Athens , Greece and worked in the office workedAt workedAsObjet1 before being succeeded by Makis_Voridis .
existential Abraham_A._Ribicoff birthPlace Connecticut Abraham_A._Ribicoff was born in Connecticut and worked as the office workedAt workedAsObjet1 before being succeeded by Anthony_J._Celebrezze .
existential Abraham_A._Ribicoff birthPlace United_States Abraham_A._Ribicoff was born in the United_States and before being succeeded by Anthony_J._Celebrezze worked as office workedAt workedAsObjet1 .
existential Adonis_Georgiadis birthPlace Greece Adonis_Georgiadis was born in Athens , Greece and worked for the office workedAt workedAsObjet1 . He was succeeded by Makis_Voridis .
existential Adonis_Georgiadis birthPlace Greece Adonis_Georgiadis was born in Athens , Greece and held office as office workedAt workedAsObjet1 before being succeeded by Makis_Voridis .
existential Albert_B._White activeYearsStartDate 1901-03-04 Albert_B._White ; started his career 1901-03-04 , finished his career 1905-03-04 and was succeeded by , William_M._O._Dawson .
relative_subject Juan_Carlos_I_of_Spain predecessor Francisco_Franco Abel_Caballero was born in Galicia_(Spain) . He was in office during Monarch , Juan_Carlos_I_of_Spain ' s reign . Francisco_Franco was Juan_Carlos_I_of_Spain ' predecessor .
passive_voice Juan_Carlos_I_of_Spain spouse Queen_Sofía_of_Spain Abel_Caballero , born in Galicia_(Spain) , was in office whilst Juan_Carlos_I_of_Spain was Monarch . Queen_Sofía_of_Spain is the wife of Juan_Carlos_I_of_Spain .
coordinated_clauses Gulf_War commander John_Major Adam_Holloway was in the Grenadier_Guards in the military and fought in the Gulf_War . John_Major was a commander during the Gulf_War .
passive_voice Gulf_War commander John_Major Adam_Holloway was in the Grenadier_Guards in the military and fought in the Gulf_War . John_Major was a commander during the Gulf_War .
passive_voice Alfred_N._Phillips activeYearsEndDate 1939-01-03 Alfred_N._Phillips retired 1939-01-03 after serving in the United_States_Army , which fought in the Korean_War .
passive_voice Alfred_N._Phillips activeYearsEndDate 1939-01-03 Alfred_N._Phillips retired on 1939-01-03 and served in the United_States_Army , which has battled in the Invasion_of_Grenada .
direct_object Alfred_N._Phillips activeYearsEndDate 1939-01-03 Alfred_N._Phillips , who retired on 1939-01-03 , served in the United_States_Army , which fought in the Korean_War .
coordinated_clauses Albert_B._White successor William_M._O._Dawson Albert_B._White , born in Cleveland and married to Agnes_Ward_White , was succeeded by William_M._O._Dawson .
relative_subject Abraham_A._Ribicoff office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Abraham_A._Ribicoff , born in the United_States , was married to Casey_Ribicoff . He worked as the office workedAt workedAsObjet1 .
apposition Abraham_A._Ribicoff office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Abraham_A._Ribicoff , married to Casey_Ribicoff , was born in the United_States and served as office workedAt workedAsObjet1 .
apposition Albert_B._White successor William_M._O._Dawson Albert_B._White , born in Cleveland and married to Agnes_Ward_White , was succeeded by William_M._O._Dawson .
existential Netherlands leaderName Mark_Rutte The Netherlands , with the leader Mark_Rutte , is where Agnes_Kant , a member of the Socialist_Party_(Netherlands) ( Netherlands ) hails from .
passive_voice American_Civil_War commander Abraham_Lincoln Alfred_Moore_Scales was at the Siege_of_Petersburg that took place during the American_Civil_War . Abraham_Lincoln was a commander in that war .
passive_voice American_Civil_War commander Jefferson_Davis Aaron_S._Daggett fought at the Battle_of_Gettysburg which took place during the American_Civil_War . Jefferson_Davis was a commander in that war .
direct_object Polish–Soviet_War commander Joseph_Stalin Adam_Koc was in the battles in the Polish–Soviet_War which was a conflict within the Russian_Civil_War . Joseph_Stalin was a commander during the Polish–Soviet_War .
relative_subject American_Civil_War commander Jefferson_Davis Alfred_Moore_Scales took part in the Siege_of_Petersburg which was part of the American_Civil_War conflict where Jefferson_Davis was a commander .
direct_object American_Civil_War commander Abraham_Lincoln Abraham_Lincoln was a commander in the American_Civil_War . Alfred_Moore_Scales fought in the Battle_of_Fredericksburg during the American_Civil_War .
coordinated_full_clauses American_Civil_War commander Jefferson_Davis One of the battles Alfred_Moore_Scales was involved in , was the Battle_of_Gettysburg , which took place during the American_Civil_War . A commander in the American_Civil_War was Jefferson_Davis .
possessif American_Civil_War commander Abraham_Lincoln Abraham_Lincoln was a commander in the American_Civil_War and the Battle_of_Gettysburg was part of that conflict . Alfred_Moore_Scales was in the Battle_of_Gettysburg .
possessif American_Civil_War commander Jefferson_Davis During the American_Civil_War conflict one of the commanders was Jefferson_Davis and Alfred_Moore_Scales participated in the Battle_of_Chancellorsville .
possessif American_Civil_War commander Abraham_Lincoln Aaron_S._Daggett fought in the American_Civil_War , Battle_of_Fredericksburg and Abraham_Lincoln was a commander in the Civil War .
passive_voice Poland language Polish_language Polish_language is the language spoken in Poland where and ethnic group is Romani_people and Adam_Koc ' s nationality is Polish_language .
possessif Abraham_A._Ribicoff birthPlace New_Britain_,_Connecticut Native_Americans are an ethnic group in the United_States , where Abraham_A._Ribicoff was born in New_Britain_,_Connecticut .
apposition Adonis_Georgiadis successor Makis_Voridis Antonis_Samaras , in office while Konstantinos_Mitsotakis was Prime Minister , was himself Prime Minister when Adonis_Georgiadis was in office . Makis_Voridis succeeded Adonis_Georgiadis .
possessif Airey_Neave battles World_War_II Airey_Neave fought in World_War_II and had a political career that started 1953-06-30 and ended 1979-03-30 .
coordinated_full_clauses Airey_Neave award Territorial_Decoration Airey_Neave , was involved in the Battle_of_France , won the Territorial_Decoration award and ended his career 1979-03-30 .
coordinated_clauses Airey_Neave award Military_Cross Before ending his career on the 1979-03-30 , Airey_Neave , fought in the Battle_of_France and was awarded the Military_Cross .
relative_subject Airey_Neave activeYearsStartDate 1974-03-04 Airey_Neave began his career 1974-03-04 , fought in the Battle_of_France and ended his career 1979-03-30 .
possessif Airey_Neave activeYearsEndDate 1979-03-30 Airey_Neave fought in World_War_II and had a political career that started 1953-06-30 and ended 1979-03-30 .
coordinated_clauses Aaron_S._Daggett award Purple_Heart Purple_Heart recipient Aaron_S._Daggett was born in Maine and , most notably , fought in the Battle_of_Fredericksburg .
relative_subject American_Civil_War commander Jefferson_Davis Born in the United_States , Albert_Jennings_Fountain , fought in the American_Civil_War . Jefferson_Davis was a commander in the American_Civil_War .
existential Albert_B._White birthPlace Cleveland Cleveland born Albert_B._White , who died in Parkersburg_,_West_Virginia , was succeeded by William_M._O._Dawson .
passive_voice Poland ethnicGroup Romani_people The Romani_people are an ethnic group in Poland , where Polish_language is the language . Adam_Koc is of Polish_language nationality .
passive_voice Poland ethnicGroup Romani_people Polish_language is the language spoken in Poland where and ethnic group is Romani_people and Adam_Koc ' s nationality is Polish_language .
apposition Juan_Perón spouse Eva_Perón Alberto_Teisaire , who belongs to the Justicialist_Party was in office while Juan_Perón , married to Eva_Perón , was president .
possessif Juan_Perón party Labour_Party_(Argentina) Alberto_Teisaire was born in Mendoza_,_Argentina and was in office while Labour_Party_(Argentina) member Juan_Perón was president .
relative_object Alvah_Sabin birthPlace Georgia_,_Vermont Alvah_Sabin was born in Georgia_,_Vermont and was a member of the Whig_Party_(United_States) led by Henry_Clay .
passive_voice Alvah_Sabin birthDate 1793-10-23 Alvah_Sabin , born 1793-10-23 , was a member of the Whig_Party_(United_States) . Daniel_Webster was one of the leaders of the party .
relative_object Adonis_Georgiadis inOfficeWhilePrimeMinister Antonis_Samaras Adonis_Georgiadis , a member of the New_Democracy_(Greece) Party ( Blue is their colour ) , served in office while Antonis_Samaras was Prime Minister .
possessif Alvah_Sabin region Vermont's_3rd_congressional_district From Vermont , Alvah_Sabin , represented Vermont's_3rd_congressional_district . The largest city in Vermont is Burlington_,_Vermont .
coordinated_full_clauses René_Goscinny nationality French_people The comic character Asterix_(comicsCharacter) ( also known as "Astérix" ) , was created by René_Goscinny , who is French_people .
apposition René_Goscinny nationality French_people The comic character Asterix_(comicsCharacter) ( also known as "Astérix" ) , was created by René_Goscinny , who is French_people .
existential Blockbuster_(comicsCharacter) alternativeName "Mark_Desmond" The comic book character Blockbuster_(comicsCharacter) , also known as "Mark_Desmond" , was created by Roger_Stern and Carmine_Infantino .
existential Paris_Cullins nationality United_States The comic character Bolt_(comicsCharacter) , was created by Paris_Cullins from the United_States and Dan_Mishkin .
passive_voice Ernie_Colón nationality Puerto_Ricans Ernie_Colón , a Puerto_Ricans national created Bolt_(comicsCharacter) along with Dan_Mishkin .
direct_object Airman_(comicsCharacter) alternativeName "Drake_Stevens" The comic character Airman_(comicsCharacter) , AKA "Drake_Stevens" was created by Harry_Sahle and George_Kapitan .
apposition Auron_(comicsCharacter) fullName "Lambien" Karl_Kesel and Walt_Simonson created the comic book character of Auron_(comicsCharacter) who has the full name "Lambien" .
direct_object Gene_Colan nationality Americans Ben_Urich was created by the Americans , Gene_Colan and the comic book writer , Roger_McKenzie_(comic_book_writer) .
direct_object Blockbuster_(comicsCharacter) alternativeName "Mark_Desmond" The comic character Blockbuster_(comicsCharacter) , aka "Mark_Desmond" , was created by Gardner_Fox and Roger_Stern .
direct_object Paris_Cullins nationality United_States The comic character of Bolt_(comicsCharacter) is created by Ernie_Colón and the United_States National , Paris_Cullins .
apposition Jerry_Ordway nationality Americans Bibbo_Bibbowski was created by Marv_Wolfman and Jerry_Ordway , Jerry_Ordway is Americans .
apposition Blockbuster_(comicsCharacter) alternativeName "Mark_Desmond" The comic character Blockbuster_(comicsCharacter) , with the alternative name "Mark_Desmond" , was created by Tom_Lyle and Roger_Stern .
apposition Paris_Cullins nationality United_States The comic character Bolt_(comicsCharacter) , was created by Paris_Cullins from the United_States and Dan_Mishkin .
direct_object Ballistic_(comicsCharacter) alternativeName "Kelvin_Mao" Ballistic_(comicsCharacter) , also known as "Kelvin_Mao" , is a fictional comic superhero created by "Michael_Manley" and Doug_Moench .
direct_object Blockbuster_(comicsCharacter) alternativeName "Mark_Desmond" Blockbuster_(comicsCharacter) ( also known as "Mark_Desmond" ) is a comic book character created by Roger_Stern and Gardner_Fox .
existential Arion_(comicsCharacter) alternativeName "Ahri'ahn" Jan_Duursema and Paul_Kupperberg created the comic book character of Arion_(comicsCharacter) who is also known as "Ahri'ahn" .
apposition Ballistic_(comicsCharacter) alternativeName "Kelvin_Mao" Ballistic_(comicsCharacter) , also known as "Kelvin_Mao" , is a fictional comic superhero created by "Michael_Manley" and Doug_Moench .
existential Blockbuster_(comicsCharacter) alternativeName "Mark_Desmond" The comic character Blockbuster_(comicsCharacter) , AKA "Mark_Desmond" , was created by Gardner_Fox and Carmine_Infantino .
existential Blockbuster_(comicsCharacter) alternativeName "Roland_Desmond" The comic character , Blockbuster_(comicsCharacter) ( a . k . a . "Roland_Desmond" ) was created by Gardner_Fox and Roger_Stern .
existential Paris_Cullins nationality United_States The comic character of Bolt_(comicsCharacter) is created by Ernie_Colón and the United_States National , Paris_Cullins .
passive_voice Sheldon_Moldoff award Inkpot_Award The Americans , Sheldon_Moldoff , won the Inkpot_Award for creating the Black_Pirate .
direct_object Black_Pirate alternativeName "Jon_Valor" Sheldon_Moldoff , an Americans national is the creator of Black_Pirate AKA "Jon_Valor" .
relative_subject Bananaman firstAired "1983-10-03" Bananaman , starring Tim_Brooke-Taylor , was first broadcast by "STV" on the "1983-10-03" .
relative_subject Bananaman firstAired "1983-10-03" Bill_Oddie starred in Bananaman which first aired "1983-10-03" and was broadcast by "STV" .
direct_object Bananaman firstAired "1983-10-03" Bill_Oddie starred in Bananaman which was broadcast by the BBC and first aired "1983-10-03" .
coordinated_clauses Bananaman firstAired "1983-10-03" Bananaman , starring Jill_Shilling , is broadcast by the BBC and first aired "1983-10-03" .
relative_subject Bill_Oddie child Kate_Hardie Bill_Oddie starred in Bananaman was born in Rochdale and Kate_Hardie is his child .
juxtaposition Curitiba isPartOf Paraná_(state) Afonso_Pena_International_Airport serves the city of Curitiba which is part of Paraná_(state) and led by the Democratic_Labour_Party_(Brazil) .
relative_subject Atlantic_City_International_Airport runwayName "13/31" The Port_Authority_of_New_York_and_New_Jersey is the operating organisation of Atlantic_City_International_Airport ., This airport has a runway length of 1873 metres and a runway named "13/31" .
possessif Appleton_International_Airport elevationAboveTheSeaLevel_(in_metres) 280 Appleton_International_Airport is 280 metres above sea level and has a runway length of 2439 . It is operated from Outagamie_County_,_Wisconsin .
existential Appleton_International_Airport elevationAboveTheSeaLevel_(in_metres) 280 Appleton_International_Airport is elevated 280 metres above sea level and has a runway length of 1982 . It is operated by an organisation in Outagamie_County_,_Wisconsin .
passive_voice Aarhus_Airport runwayName "10L/28R" Aarhus_Airport , operated by Aktieselskab has a runway length of 2776 and a name of "10L/28R" .
passive_voice Aarhus_Airport runwayName "10L/28R" Aktieselskab operates Aarhus_Airport which has a runway that is 2777 meters long and the runway name "10L/28R" .
relative_subject Aarhus_Airport runwayName "10L/28R" Operated by "Aarhus_Lufthavn_A/S" , Aarhus_Airport , has a runway length of 2702 metres and a runway named "10L/28R" .
juxtaposition Al_Asad_Airbase location "Al_Anbar_Province_,_Iraq" Al_Asad_Airbase is operated by the United_States_Air_Force . It is located at "Al_Anbar_Province_,_Iraq" and its runway length is 3090.0 .
passive_voice Atlantic_City_International_Airport runwayLength 1873 The Port_Authority_of_New_York_and_New_Jersey is the operating organisation of Atlantic_City_International_Airport ., This airport has a runway length of 1873 metres and a runway named "13/31" .
relative_subject Aarhus_Airport runwayLength 2776 Operated by the Aktieselskab organisation , Aarhus_Airport , has a runway length of 2776 metres and a runway named "10R/28L" .
direct_object Aarhus_Airport runwayLength 2777 The "10R/28L" runway at Aarhus_Airport is 2777 in length , which is run by the operating organization of Aktieselskab .
existential Aarhus_Airport runwayLength 2776 Aarhus_Airport , operated by Aktieselskab has a runway length of 2776 and a name of "10L/28R" .
existential Atlantic_City_International_Airport runwayLength 3048.0 The Port_Authority_of_New_York_and_New_Jersey operate Atlantic_City_International_Airport . This airport has a runway length of 3048.0 metres and a runway named "13/31" .
apposition Iraq leaderName Fuad_Masum The Al-Taqaddum_Air_Base serves the city of Fallujah in Iraq . The country is led by Fuad_Masum .
apposition Pakistan leaderName Sardar_Ayaz_Sadiq Allama_Iqbal_International_Airport serves the city of Lahore in Pakistan . The country is led by Sardar_Ayaz_Sadiq .
passive_voice Abilene_,_Texas isPartOf Jones_County_,_Texas Abilene_Regional_Airport serves Abilene_,_Texas , Jones_County_,_Texas , United_States .
relative_subject Greece leaderName Nikos_Voutsis The Athens_International_Airport serves the city of Athens , Greece . Nikos_Voutsis is the leader of that country .
relative_subject Greece leaderName Prokopis_Pavlopoulos Athens_International_Airport serves the city of Athens , Greece . Alexis_Tsipras and Prokopis_Pavlopoulos are leaders in that country .
direct_object Pakistan leaderName Mamnoon_Hussain Lahore , located in Pakistan and led by Mamnoon_Hussain , is the home of the Allama_Iqbal_International_Airport .
apposition Curitiba leaderName Democratic_Labour_Party_(Brazil) Afonso_Pena_International_Airport serves the city of Curitiba which is part of Paraná_(state) and led by the Democratic_Labour_Party_(Brazil) .
coordinated_clauses Wilson_Township_,_Alpena_County_,_Michigan country United_States With an elevation of 210 metres above sea level , Alpena_County_Regional_Airport is located in the Wilson_Township_,_Alpena_County_,_Michigan , in the United_States .
existential Andrews_County_Airport runwayLength 8 Located in Texas , Andrews_County_Airport is 973 below sea level and has a runway length of 8 .
coordinated_clauses Uttar_Pradesh isPartOf Bundelkhand Agra_Airport is 167.94 metres above sea level and located in Uttar_Pradesh , part of Awadh and Bundelkhand .
passive_voice Wilson_Township_,_Alpena_County_,_Michigan country United_States With an elevation of 210 metres above sea level , Alpena_County_Regional_Airport is located in the Wilson_Township_,_Alpena_County_,_Michigan , in the United_States .
passive_voice Wilson_Township_,_Alpena_County_,_Michigan country United_States The location of Alpena_County_Regional_Airport is Wilson_Township_,_Alpena_County_,_Michigan , United_States and is 210 metres above sea level .
existential Uttar_Pradesh isPartOf Awadh Agra_Airport , located in Uttar_Pradesh , Awadh has an elevation of 167.94 meters about sea level .
direct_object Andrews_County_Airport runwayLength 896 The runway length of Andrews_County_Airport ( located in Texas and 973 below sea level ) is 896 .
direct_object Ícolo_e_Bengo country Angola Angola_International_Airport is located in Ícolo_e_Bengo , Angola and is 159 metres above sea level .
direct_object Egg_Harbor_Township_,_New_Jersey country United_States Atlantic_City_International_Airport is located at Egg_Harbor_Township_,_New_Jersey , United_States and is 23 metres above sea level .
passive_voice Ashgabat_International_Airport runwayLength 3800 Ashgabat is the location of Ashgabat_International_Airport which has a runway length of 3800 and is 211 metres above sea level .
relative_subject Uttar_Pradesh isPartOf Bundelkhand Uttar_Pradesh is part of Bundelkhand and is the location of Agra_Airport at 167.94 metres above sea level ,.
relative_subject Wilson_Township_,_Alpena_County_,_Michigan country United_States With an elevation of 210 metres above sea level , Alpena_County_Regional_Airport is located in the Wilson_Township_,_Alpena_County_,_Michigan , in the United_States .
relative_subject Ícolo_e_Bengo country Angola At 159 meters above sea level , Angola_International_Airport is located in Ícolo_e_Bengo , in Angola .
existential Adolfo_Suárez_Madrid–Barajas_Airport runwayName "18L/36R" Located at "Madrid_,_Paracuellos_de_Jarama_,_San_Sebastián_de_los_Reyes_and_Alcobendas" , Adolfo_Suárez_Madrid–Barajas_Airport has a runway length of 4100.0 metres and a runway name "18L/36R" .
existential Afonso_Pena_International_Airport runwayName "11/29" The "11/29" runway in Afonso_Pena_International_Airport in São_José_dos_Pinhais has a length of 2215 .
existential Ícolo_e_Bengo country Angola Angola_International_Airport , located in Ícolo_e_Bengo , Angola has a runway length of 4000.0 .
passive_voice Adolfo_Suárez_Madrid–Barajas_Airport runwayName "18L/36R" Adolfo_Suárez_Madrid–Barajas_Airport which is located at Paracuellos_de_Jarama has a runway name of "18L/36R" with a length of 4349 .
passive_voice Al_Asad_Airbase operatingOrganisation United_States_Air_Force Operated by the United_States_Air_Force , Al_Asad_Airbase is located at "Al_Anbar_Province_,_Iraq" and has a runway that is 3090.0 metres long .
passive_voice Ashgabat_International_Airport elevationAboveTheSeaLevel_(in_metres) 211 Ashgabat is the location of Ashgabat_International_Airport which is elevated 211 metres above sea level and has a runway length of 900 metres .
passive_voice Ashgabat_International_Airport elevationAboveTheSeaLevel_(in_metres) 211 Ashgabat is the location of Ashgabat_International_Airport which has a runway length of 2989 and is located 211 metres above sea level .
coordinated_clauses Adolfo_Suárez_Madrid–Barajas_Airport runwayName "14L/32R" Located in Alcobendas is Adolfo_Suárez_Madrid–Barajas_Airport . This airport has a runway length of 3500.0 metres and a runway name "14L/32R" .
apposition Adolfo_Suárez_Madrid–Barajas_Airport runwayName "14L/32R" The runway length at Adolfo_Suárez_Madrid–Barajas_Airport in Madrid is 3500.0 m and named "14L/32R" .
apposition Maple_Ridge_Township_,_Alpena_County_,_Michigan country United_States Alpena_County_Regional_Airport has a runway length is 2744 metres and is located in Maple_Ridge_Township_,_Alpena_County_,_Michigan , in the United_States .
passive_voice Adolfo_Suárez_Madrid–Barajas_Airport runwayName "18R/36L" Located in Alcobendas , Adolfo_Suárez_Madrid–Barajas_Airport has a runway length of 3500 metres and a runway named "18R/36L" .
passive_voice Alpena_County_Regional_Airport elevationAboveTheSeaLevel_(in_metres) 210 Alpena_County_Regional_Airport ; is elevated 210 metres above sea level , has a runway length of 1533 metres and is located in Wilson_Township_,_Alpena_County_,_Michigan .
passive_voice Wilson_Township_,_Alpena_County_,_Michigan country United_States Alpena_County_Regional_Airport , located in Wilson_Township_,_Alpena_County_,_Michigan , United_States and has a length of 1533.0 .
relative_subject Adolfo_Suárez_Madrid–Barajas_Airport runwayName "14L/32R" The Adolfo_Suárez_Madrid–Barajas_Airport is in Madrid . Its runway , called "14L/32R" has length of 4100.0 .
relative_subject Maple_Ridge_Township_,_Alpena_County_,_Michigan country United_States The location of Alpena_County_Regional_Airport is Maple_Ridge_Township_,_Alpena_County_,_Michigan , United_States and it has a 1533 meters runway .
relative_subject Athens_International_Airport cityServed Athens With a runway length of 3800.0 metres , Athens_International_Airport is in Spata and serves the city of Athens .
relative_subject Adolfo_Suárez_Madrid–Barajas_Airport runwayName "14L/32R" The runway length at Adolfo_Suárez_Madrid–Barajas_Airport in Madrid is 3500.0 m and named "14L/32R" .
relative_subject Al_Asad_Airbase operatingOrganisation United_States_Air_Force Al_Asad_Airbase , found in Iraq , is operated by the United_States_Air_Force . The runway is 3992.88 feet long .
relative_subject Andrews_County_Airport elevationAboveTheSeaLevel_(in_metres) 973 The runway length of Andrews_County_Airport ( located in Texas and 973 below sea level ) is 896 .
apposition Adolfo_Suárez_Madrid–Barajas_Airport runwayName "14L/32R" The "14L/32R" runway of the Adolfo_Suárez_Madrid–Barajas_Airport in Madrid has a length of 4100.0 .
apposition Allama_Iqbal_International_Airport runwayName "18L/36R" Allama_Iqbal_International_Airport in Punjab_,_Pakistan has a runway known as "18L/36R" with a length of 2900.0 .
apposition Andrews_County_Airport elevationAboveTheSeaLevel_(in_metres) 973 Located in Texas , Andrews_County_Airport , is 973 below sea level and has a runway length of 1773 metres .
apposition Egg_Harbor_Township_,_New_Jersey country United_States Atlantic_City_International_Airport is located at Egg_Harbor_Township_,_New_Jersey , United_States and has a runway length of 1873 .
coordinated_full_clauses Maple_Ridge_Township_,_Alpena_County_,_Michigan country United_States Alpena_County_Regional_Airport is located in Maple_Ridge_Township_,_Alpena_County_,_Michigan , United_States and is 2744 meters long .
relative_subject Adolfo_Suárez_Madrid–Barajas_Airport runwayName "14L/32R" The "14L/32R" runway in Adolfo_Suárez_Madrid–Barajas_Airport located in Madrid , has a length of 4349.0 .
relative_subject Afonso_Pena_International_Airport runwayName "11/29" Located at São_José_dos_Pinhais , Afonso_Pena_International_Airport has the runway name "11/29" and a runway length of 1800 metres .
relative_subject Al_Asad_Airbase operatingOrganisation United_States_Air_Force Operated by the United_States_Air_Force , Al_Asad_Airbase , in Iraq , has a runway length of 3078.48 metres .
passive_voice Maple_Ridge_Township_,_Alpena_County_,_Michigan country United_States Alpena_County_Regional_Airport has a runway length is 2744 metres and is located in Maple_Ridge_Township_,_Alpena_County_,_Michigan , in the United_States .
passive_voice Appleton_International_Airport elevationAboveTheSeaLevel_(in_metres) 280 Appleton_International_Airport , which is located in Greenville_,_Wisconsin is located at 280 metres above sea level and a runway length of 1982.0 .
existential Agra_Airport ICAO_Location_Identifier "VIAG" Agra_Airport , with the ICAO identifier of "VIAG" , located in Uttar_Pradesh is operated by Airports_Authority_of_India .
existential Al_Asad_Airbase runwayLength 3078.48 Operated by the United_States_Air_Force , Al_Asad_Airbase , in Iraq , has a runway length of 3078.48 metres .
direct_object Pakistan_Civil_Aviation_Authority headquarter Jinnah_International_Airport The Allama_Iqbal_International_Airport , in Punjab_,_Pakistan is operated by the Pakistan_Civil_Aviation_Authority , based in Jinnah_International_Airport .
relative_subject Uttar_Pradesh isPartOf Bundelkhand Agra_Airport is in Uttar_Pradesh , Awadh , Bundelkhand and is operated by the Indian_Air_Force .
passive_voice Al_Asad_Airbase runwayLength 3090.0 The United_States_Air_Force , operating the Al_Asad_Airbase which is located in "Al_Anbar_Province_,_Iraq" , which has a runway length of 3090.0 meters .
existential Punjab_,_Pakistan leaderTitle Provincial_Assembly_of_the_Punjab The Pakistan_Civil_Aviation_Authority runs Allama_Iqbal_International_Airport in Punjab_,_Pakistan which is led by the Provincial_Assembly_of_the_Punjab .
possessif Agra_Airport operatingOrganisation Indian_Air_Force Agra_Airport , located in India , is operated by the Indian_Air_Force and has an ICAO identifier is "VIAG" .
existential Agra_Airport operatingOrganisation Airports_Authority_of_India Agra_Airport , with the ICAO identifier of "VIAG" , located in Uttar_Pradesh is operated by Airports_Authority_of_India .
passive_voice Indian_Air_Force aircraftHelicopter HAL_Light_Combat_Helicopter The operating organization for Agra_Airport is the Indian_Air_Force who deploy the HAL_Light_Combat_Helicopter and the Boeing_C-17_Globemaster_III transport aircraft .
relative_subject Abilene_Regional_Airport elevationAboveTheSeaLevel_(in_metres) 546 Serving the city of Abilene_,_Texas , Abilene_Regional_Airport is 546 metres above sea level and has a runway length of 1121 metres .
relative_subject Athens_International_Airport location Spata With a runway length of 3800.0 metres , Athens_International_Airport is in Spata and serves the city of Athens .
coordinated_full_clauses Abilene_Regional_Airport elevationAboveTheSeaLevel_(in_metres) 546 Abilene_Regional_Airport , serving the city of Abilene_,_Texas , has a runway length of 2195 and is located at 546 meters above sea level .
coordinated_full_clauses Angola_International_Airport elevationAboveTheSeaLevel_(in_metres) 159 The city of Luanda is served by Angola_International_Airport . This airport is is 159 meters above sea level and has a runway length of 4000.0 metres .
coordinated_clauses Athens_International_Airport location Spata Located in Spata and serving the city of Athens , Athens_International_Airport has a runway length of 4000 metres .
possessif Al-Taqaddum_Air_Base elevationAboveTheSeaLevel_(in_metres) 84 The Al-Taqaddum_Air_Base which serves the city of Fallujah has a runway length of 3684 and is 84 metres above sea level .
relative_subject Abilene_Regional_Airport elevationAboveTheSeaLevel_(in_metres) 546 Abilene_Regional_Airport serves the city of Abilene_,_Texas . Its runway is 2194 feet and is 546 m above sea level .
relative_object Abilene_Regional_Airport elevationAboveTheSeaLevel_(in_metres) 546 Abilene_Regional_Airport serves the city of Abilene_,_Texas . Its runway length is 2194 and 546 m above sea level .
coordinated_full_clauses Alderney_Airport 1st_runway_SurfaceType Poaceae Alderney_Airport , whose runway is 877.0 m , serves the city of Alderney . Its surface is Poaceae .
relative_subject United_States_Air_Force battles Invasion_of_Grenada The McDonnell_Douglas_F-15_Eagle is an aircraft fighter in the United_States_Air_Force operated Al_Asad_Airbase and was used during the Invasion_of_Grenada .
relative_adverb Alcobendas isPartOf Community_of_Madrid Adolfo_Suárez_Madrid–Barajas_Airport is found in Alcobendas which is part of the Community_of_Madrid . The leader party at Alcobendas is the People's_Party_(Spain) .
apposition Alderney_Airport runwayLength 497 The Alderney_Airport serves the island of Alderney and its 1st runway is surfaced with Poaceae and has a 497 meters long runway .
possessif Poaceae order Commelinids The 1st runway at Alderney_Airport is made from Poaceae which belongs to the order of Commelinids and is in the class Monocotyledon .
possessif Port_Authority_of_New_York_and_New_Jersey regionServed New_Jersey The Atlantic_City_International_Airport is operated by the Port_Authority_of_New_York_and_New_Jersey which serves the region of New_Jersey and is based in New_York .
coordinated_full_clauses Antwerp_International_Airport cityServed Antwerp Antwerp_International_Airport , serving Antwerp , with an elevation of 12 meters is operated by the Flemish_Government .
relative_subject Afonso_Pena_International_Airport location São_José_dos_Pinhais Afonso_Pena_International_Airport , located in São_José_dos_Pinhais , is 911.0 meters above sea level and is operated by Infraero .
relative_subject Antwerp_International_Airport owner Flemish_Region Antwerp_International_Airport is operated by the "Flemish_department_of_Mobility_and_Public_Works" and owned by the Flemish_Region . It is 12 metres above sea level .
relative_subject Antwerp_International_Airport cityServed Antwerp Antwerp_International_Airport , serving Antwerp , with an elevation of 12 meters is operated by the Flemish_Government .
existential Poaceae division Flowering_plant The 2nd runway at Ardmore_Airport_(New_Zealand) is made of Poaceae , which belongs to the division of Flowering_plant and the order Poales .
juxtaposition Poaceae division Flowering_plant Poaceae is of the order Poales and in the division of Flowering_plant . It is the surface type of the second runway of Ardmore_Airport_(New_Zealand) .
coordinated_full_clauses Ardmore_Airport_(New_Zealand) elevationAboveTheSeaLevel_(in_metres) 34.0 Ardmore_Airport_(New_Zealand) is 34.0 metres above sea level , a runway length of 518.0 and a 3rd runway surfaced with Poaceae .
passive_voice Poaceae division Flowering_plant Ardmore_Airport_(New_Zealand) ' s 3rd runway surface type is Poaceae which is in the order of Commelinids and belongs to the division of Flowering_plant .
direct_object Greenville_,_Wisconsin isPartOf Ellington_,_Wisconsin Appleton_International_Airport , located in Greenville_,_Wisconsin , services the Appleton_,_Wisconsin , Ellington_,_Wisconsin and Dale_,_Wisconsin areas .
passive_voice Poaceae order Poales Poaceae , which is of the order Poales and in the class Monocotyledon , is the surface type of the second runway of Ardmore_Airport_(New_Zealand) .
coordinated_full_clauses Poaceae order Commelinids Poaceae , a Flowering_plant , belongs to the order of Commelinids and is the surface type for the second runway of Ardmore_Airport_(New_Zealand) .
existential Christian_Panucci club Genoa_CFC Christian_Panucci played football for Genoa_CFC and Chelsea_FC . Today he manages AS_Livorno_Calcio .
existential Massimo_Drago club Castrovillari_Calcio Massimo_Drago plays for Castrovillari_Calcio and ASD_SS_Nola_1925 . He also manages AC_Cesena .
existential Peter_Stöger club SK_Vorwärts_Steyr Peter_Stöger ( football ) , First_Vienna_FC , now a manager of 1_FC_Köln , played for SK_Vorwärts_Steyr .
direct_object Alessio_Romagnoli club Italy_national_under-19_football_team Alessio_Romagnoli plays for AC_Milan and the Italy_national_under-19_football_team . The manager of AC_Milan is Siniša_Mihajlović .
direct_object Michele_Marcolini club Vicenza_Calcio Michele_Marcolini has been manager of AC_Lumezzane and plays for both Torino_FC and Vicenza_Calcio .
direct_object Gus_Poyet club Real_Zaragoza AEK_Athens_FC manager is Gus_Poyet who ' s club is Real_Zaragoza , having previously played for Chelsea_FC .
direct_object John_van_den_Brom club Jong_Ajax John_van_den_Brom manages for the AZ_Alkmaar , plays for AFC_Ajax and is in the Jong_Ajax Club .
direct_object John_van_den_Brom club RSC_Anderlecht John_van_den_Brom plays for AFC_Ajax and RSC_Anderlecht . He is also the manager of AZ_Alkmaar .
direct_object John_van_den_Brom club İstanbulspor_AŞ John_van_den_Brom plays for RSC_Anderlecht and İstanbulspor_AŞ . He is also manager of AZ_Alkmaar .
direct_object Aaron_Hunt club SV_Werder_Bremen Viktor_Skrypnyk is the manager of SV_Werder_Bremen , the club Aaron_Hunt ( who plays for Hamburger_SV ) , used to play for .
direct_object Akeem_Priestley club Orange_County_Blues_FC Oliver_Wyss is the manager for Orange_County_Blues_FC , the team , Akeem_Priestley plays for . He also plays for Jacksonville_Dolphins .
apposition Michele_Marcolini club Torino_FC Michele_Marcolini manages the AC_Lumezzane and plays for both AC_Chievo_Verona and Torino_FC .
apposition Christian_Panucci club Genoa_CFC Christian_Panucci played football for Genoa_CFC and now manages AS_Livorno_Calcio and plays at the AS_Roma .
apposition John_van_den_Brom club De_Graafschap John_van_den_Brom manages the AZ_Alkmaar as well as playing for the ADO_Den_Haag and the De_Graafschap .
apposition Luciano_Spalletti club Virtus_Entella Luciano_Spalletti plays for Empoli_FC and Virtus_Entella while managing AS_Roma .
apposition Luciano_Spalletti club Virtus_Entella Luciano_Spalletti played for Udinese_Calcio . He now manages AS_Roma and plays for Virtus_Entella .
apposition Massimo_Drago club Vigor_Lamezia Attached to the club Vigor_Lamezia , Massimo_Drago manages AC_Cesena and once played for the club SSD_Potenza_Calcio .
apposition Jorge_Humberto_Rodríguez club FC_Dallas Jorge_Humberto_Rodríguez manages the Isidro_Metapán . He also plays for Alianza_FC , FC_Dallas and the El_Salvador_national_football_team .
apposition Paulo_Sousa club Portugal_national_football_team Paulo_Sousa plays for Inter_Milan , Maccabi_Tel_Aviv_FC and the Portugal_national_football_team . In addition , he also manages ACF_Fiorentina .
possessif Peter_Stöger club SC_Wiener_Neustadt Peter_Stöger plays for SC_Wiener_Neustadt and FK_Austria_Wien while also being a manager at 1_FC_Köln .
possessif Jens_Härtel club SV_Babelsberg_03 The first club Jens_Härtel played for 1_FC_Lokomotive_Leipzig , he is part of the SV_Babelsberg_03 club and manages 1_FC_Magdeburg .
possessif Jens_Härtel club RB_Leipzig The manager of 1_FC_Magdeburg is Jens_Härtel who played for RB_Leipzig and is part of the club 1_FC_Union_Berlin .
possessif Michele_Marcolini club Vicenza_Calcio Michele_Marcolini has been manager of AC_Lumezzane he is part of AC_Chievo_Verona and plays for Vicenza_Calcio .
possessif Michele_Marcolini club Vicenza_Calcio Michele_Marcolini has been manager of AC_Lumezzane and was at FC_Bari_1908 . He is in the Vicenza_Calcio club .
possessif Stuart_Parker_(footballer) club Runcorn_FC_Halton Stuart_Parker_(footballer) once played for Runcorn_FC_Halton , is attached to Irlam_Town_FC and is now a manager of AFC_Blackpool .
possessif John_van_den_Brom club RSC_Anderlecht John_van_den_Brom has been manager of AZ_Alkmaar and plays for RSC_Anderlecht . He previously played for the Netherlands_national_football_team .
possessif Massimo_Drago club Castrovillari_Calcio Massimo_Drago once played for Castrovillari_Calcio , is in the club Calcio_Catania and manages AC_Cesena .
possessif Jens_Härtel club SV_Babelsberg_03 Jens_Härtel played for both Berliner_AK_07 and FC_Sachsen_Leipzig . He has been manager for 1_FC_Magdeburg and now plays for SV_Babelsberg_03 .
possessif John_van_den_Brom club Vitesse_Arnhem John_van_den_Brom was previously the manager at AZ_Alkmaar . He plays for AFC_Ajax , ADO_Den_Haag and Vitesse_Arnhem .
possessif Peter_Stöger club SK_Vorwärts_Steyr Peter_Stöger is attached to First_Vienna_FC and is also in FK_Austria_Wien Club and SK_Vorwärts_Steyr . He is currently manager of 1_FC_Köln .
possessif Peter_Stöger club SK_Vorwärts_Steyr Peter_Stöger is manager of 1_FC_Köln , as well as playing for SC_Wiener_Neustadt . He is also in the SK_Vorwärts_Steyr and FC_Admira_Wacker_Mödling clubs .
possessif Massimo_Drago club ASD_SS_Nola_1925 Massimo_Drago played for ASD_SS_Nola_1925 , managed AC_Cesena and now plays for ASD_Licata_1931 .
possessif Rolando_Maran club Unione_Triestina_2012_SSD Rolando_Maran plays for Unione_Triestina_2012_SSD and Carrarese_Calcio . He has also managed AC_Chievo_Verona .
possessif John_van_den_Brom club İstanbulspor_AŞ John_van_den_Brom , who manages AZ_Alkmaar , plays for Jong_Ajax and İstanbulspor_AŞ .
possessif Massimo_Drago club SS_Chieti_Calcio Massimo_Drago played for ASD_SS_Nola_1925 , he now plays for SS_Chieti_Calcio and manages AC_Cesena .
direct_object Jens_Härtel club SV_Babelsberg_03 The manager of 1_FC_Magdeburg is Jens_Härtel . He also plays for 1_FC_Lokomotive_Leipzig and SV_Babelsberg_03 and his club is Berliner_AK_07 .
possessif Frank_de_Boer club Netherlands_national_football_team Frank_de_Boer once played for Al-Shamal_Sports_Club and the Netherlands_national_football_team . He is now attached to FC_Barcelona while managing AFC_Ajax .
passive_voice Peter_Stöger club SC_Wiener_Neustadt Peter_Stöger plays for SC_Wiener_Neustadt and FK_Austria_Wien while also being a manager at 1_FC_Köln .
passive_voice Rolando_Maran club FC_Bari_1908 AC_Chievo_Verona manager Rolando_Maran has formerly worked at FC_Bari_1908 and is a member of the Carrarese_Calcio club .
passive_voice Michele_Marcolini club Vicenza_Calcio Michele_Marcolini manages the AC_Lumezzane , plays for AC_Chievo_Verona and previously played for Vicenza_Calcio .
passive_voice Christian_Panucci club Italy_national_football_team Christian_Panucci played for the Italy_national_football_team and Inter_Milan as well as managing AS_Livorno_Calcio .
passive_voice John_van_den_Brom club Jong_Ajax John_van_den_Brom was manager of AZ_Alkmaar and now plays for AFC_Ajax and Jong_Ajax .
passive_voice John_van_den_Brom club RSC_Anderlecht John_van_den_Brom plays for AFC_Ajax and RSC_Anderlecht . He is also the manager of AZ_Alkmaar .
passive_voice John_van_den_Brom club Vitesse_Arnhem John_van_den_Brom plays for both Vitesse_Arnhem and Jong_Ajax . He has also managed AZ_Alkmaar .
passive_voice Luciano_Spalletti club Virtus_Entella Luciano_Spalletti is a manager for the AS_Roma club and plays for Empoli_FC . C . as well as Virtus_Entella .
passive_voice Jens_Härtel club SV_Babelsberg_03 Jens_Härtel played for Berliner_AK_07 and has been manager of 1_FC_Magdeburg . He is a member of the FSV_Zwickau club and plays for SV_Babelsberg_03 .
passive_voice Paulo_Sousa club Portugal_national_football_team Paulo_Sousa is manager of ACF_Fiorentina , plays for Maccabi_Tel_Aviv_FC , Inter_Milan club and the Portugal_national_football_team .
passive_voice Peter_Stöger club SK_Vorwärts_Steyr Peter_Stöger is the manager at 1_FC_Köln . He is a former player of SK_Vorwärts_Steyr , FC_Admira_Wacker_Mödling and FK_Austria_Wien .
existential Alan_Martin_(footballer) club Hamilton_Academical_FC Footballer , Alan_Martin_(footballer) , plays for both , Hamilton_Academical_FC and Accrington_Stanley_FC . The latter , played in Accrington .
possessif Alaa_Abdul-Zahra club Al_Kharaitiyat_SC Al-Zawra'a_SC member , Alaa_Abdul-Zahra is also a member of Al_Khor located , Al_Kharaitiyat_SC .
possessif Aleksandr_Prudnikov club FC_Terek_Grozny Aleksandr_Prudnikov is in the FC_Terek_Grozny club which is located in Grozny . He plays for the FC_Kuban_Krasnodar .
direct_object Alan_Martin_(footballer) club Motherwell_FC Alan_Martin_(footballer) played for Accrington_Stanley_FC and currently for Motherwell_FC at their ground of Fir_Park .
passive_voice Alan_Martin_(footballer) club Hamilton_Academical_FC Footballer , Alan_Martin_(footballer) , plays for the clubs Hamilton_Academical_FC and Accrington_Stanley_FC . Accrington_Stanley_FC play in Accrington .
passive_voice Akeem_Adams club United_Petrotrin_FC Akeem_Adams ' former clubs include Ferencvárosi_TC and Palo_Seco based United_Petrotrin_FC .
possessif Alan_Martin_(footballer) club Hamilton_Academical_FC Footballer , Alan_Martin_(footballer) , plays for both , Hamilton_Academical_FC and Accrington_Stanley_FC . The latter , played in Accrington .
direct_object Alan_Martin_(footballer) club Hamilton_Academical_FC Footballer , Alan_Martin_(footballer) , plays for the clubs Hamilton_Academical_FC and Accrington_Stanley_FC . Accrington_Stanley_FC play in Accrington .
direct_object Aleksandre_Guruli club FC_Samtredia Aleksandre_Guruli played for FC_Dinamo_Batumi and FC_Samtredia . Erosi_Manjgaladze_Stadium is the ground of FC_Samtredia .
apposition Abner_(footballer) club Coritiba_Foot_Ball_Club Abner_(footballer) was born 1996-05-30 , belongs to the club C.D._FAS and plays for Coritiba_Foot_Ball_Club .
existential Alan_Martin_(footballer) club Crewe_Alexandra_FC Alan_Martin_(footballer) , born 1989-01-01 , plays for the club , Crewe_Alexandra_FC . He has also played for Aldershot_Town_FC .
existential Alan_Martin_(footballer) club Hamilton_Academical_FC Born 1989-01-01 , footballer , Alan_Martin_(footballer) , played football for Hamilton_Academical_FC and plays for the club , Ayr_United_FC
passive_voice Al_Anderson_(NRBQ_band) associatedBand/associatedMusicalArtist The_Wildweeds Born in Windsor_,_Connecticut , Al_Anderson_(NRBQ_band) plays with the band NRBQ and was once a member of The_Wildweeds .
apposition Alaa_Abdul-Zahra club Al_Kharaitiyat_SC Alaa_Abdul-Zahra was born in Iraq and he plays for the Al_Kharaitiyat_SC as well as the Al-Khor_Sports_Club .
existential Al_Anderson_(NRBQ_band) associatedBand/associatedMusicalArtist The_Wildweeds Al_Anderson_(NRBQ_band) plays with the band NRBQ and played once with The_Wildweeds . He was born in Windsor_,_Connecticut .
apposition Al_Anderson_(NRBQ_band) associatedBand/associatedMusicalArtist The_Wildweeds Born in Windsor_,_Connecticut , Al_Anderson_(NRBQ_band) plays with the band NRBQ and was once a member of The_Wildweeds .
apposition Alaa_Abdul-Zahra club Sanat_Mes_Kerman_FC Iraq born , Alaa_Abdul-Zahra , is a member of the clubs , Al_Kharaitiyat_SC and Sanat_Mes_Kerman_FC
apposition Aleksandre_Guruli club FC_Karpaty_Lviv Aleksandre_Guruli , who is 178.0_centimetres tall , formerly played for FC_Karpaty_Lviv and currently for AZAL_PFK .
direct_object Aleksandre_Guruli club Georgia_national_football_team At 178.0_centimetres tall , Aleksandre_Guruli , played for the Georgia_national_football_team and plays for FC_Karpaty_Lviv club .
direct_object Aaron_Hunt club Hamburger_SV Aaron_Hunt previously played for Hamburger_SV , but now belongs to the youthclub youthGoslarer_SC_08 and plays for Goslarer_SC_08 .
direct_object Aaron_Hunt club VfL_Wolfsburg Aaron_Hunt , who previously played for the youthHamburger_SV youth team is currently playing for VfL_Wolfsburg and Hamburger_SV .
possessif Aaron_Hunt club VfL_Wolfsburg Aaron_Hunt , who previously played for the youthHamburger_SV youth team is currently playing for VfL_Wolfsburg and Hamburger_SV .
passive_voice Aaron_Hunt club SV_Werder_Bremen_II Aaron_Hunt has played football for youthGoslarer_SC_08 youth team , SV_Werder_Bremen_II senior team and also Goslarer_SC_08 .
existential Alfred_Garth_Jones birthPlace Manchester Alfred_Garth_Jones was born in Manchester , England and he passed away in Sidcup .
relative_adverb Alan_Frew origin Newmarket_,_Ontario Alan_Frew is from Newmarket_,_Ontario , Canada and started performing in 1983 .
possessif Houston_Texans city Texas Based in Houston , Texas , the Houston_Texans are the team Akeem_Dent used to play for .
possessif Alfred_Garth_Jones birthPlace Manchester Alfred_Garth_Jones was born in Manchester , in England . The Labour_Party_(UK) is the leader of Manchester .
apposition Alfred_Garth_Jones deathPlace Sidcup Alfred_Garth_Jones died in Sidcup in London where Boris_Johnson is a leader .
possessif Agustín_Barboza deathPlace Paraguay Agustín_Barboza died in Asunción , in Paraguay which is led by Juan_Afara .
passive_voice Albany_,_Oregon isPartOf Linn_County_,_Oregon The city of Albany_,_Oregon is part of both Linn_County_,_Oregon and Benton_County_,_Oregon , in the United_States .
passive_voice Anderson_,_Indiana isPartOf Indiana Anderson_,_Indiana is part of "Adams_,_Fall_Creek_,_Lafayette_,_Richland_,_Union" in Indiana in the United_States .
passive_voice Indonesia leaderName Jusuf_Kalla Arem-arem originates from the country of Indonesia , where two of the leaders are , Joko_Widodo and Jusuf_Kalla .
passive_voice Aenir precededBy Castle_(novel) Above_the_Veil is from Australians and was preceded by Aenir and Castle_(novel) .
direct_object Amarillo_,_Texas isPartOf Randall_County_,_Texas Amarillo_,_Texas is located in the counties of Randall_County_,_Texas and Potter_County_,_Texas in the United_States .
relative_subject Albany_,_Oregon isPartOf Linn_County_,_Oregon Albany_,_Oregon is a city in Linn_County_,_Oregon and part of Benton_County_,_Oregon in the United_States .
relative_subject Anderson_,_Indiana isPartOf Indiana Anderson_,_Indiana is part of "Adams_,_Fall_Creek_,_Lafayette_,_Richland_,_Union" in Indiana in the United_States .
relative_subject Atlanta isPartOf Fulton_County_,_Georgia Atlanta is part of both Fulton_County_,_Georgia and DeKalb_County_,_Georgia , in the United_States .
relative_subject France leaderName Manuel_Valls Barny_Cakes come from France , where François_Hollande and Manuel_Valls are leaders .
relative_subject Singapore leaderName Tony_Tan Beef_kway_teow is a dish from Singapore where the leaders are Tony_Tan and Halimah_Yacob .
direct_object Orange_County_,_California isPartOf Greater_Los_Angeles_Area Orange_County_,_California is part of the Greater_Los_Angeles_Area , where Anaheim_,_California is located in the United_States .
direct_object Auburn_,_Washington isPartOf Washington_(state) Auburn_,_Washington is part of King_County_,_Washington , Washington_(state) , United_States .
direct_object France leaderName Gérard_Larcher François_Hollande and Gérard_Larcher are leaders of France where Barny_Cakes originate .
possessif Albany_,_Oregon isPartOf Linn_County_,_Oregon Albany_,_Oregon is a city in Linn_County_,_Oregon and part of Benton_County_,_Oregon in the United_States .
possessif Amarillo_,_Texas isPartOf Texas Amarillo_,_Texas is part of Potter_County_,_Texas , located in Texas , United_States .
possessif Atlanta isPartOf Fulton_County_,_Georgia Atlanta is part of both DeKalb_County_,_Georgia and Fulton_County_,_Georgia , United_States .
possessif Auburn_,_Alabama isPartOf Lee_County_,_Alabama Alabama is located in the United_States . Auburn_,_Alabama , which is part of Lee_County_,_Alabama is located in Alabama .
possessif France leaderName Manuel_Valls Two of the leaders of France are Manuel_Valls and Gérard_Larcher , it is also where Baked_Alaska is from .
possessif France leaderName Manuel_Valls Barny_Cakes come from France . Claude_Bartolone and Manuel_Valls are leaders in France .
possessif Mexico leaderName Silvano_Aureoles_Conejo Bionico is a native food of Mexico where the leaders are Silvano_Aureoles_Conejo and Enrique_Peña_Nieto .
apposition Albany_,_Oregon isPartOf Oregon Albany_,_Oregon is part of Benton_County_,_Oregon , which is located in Oregon in the United_States .
apposition Atlanta isPartOf Fulton_County_,_Georgia Atlanta is part of both Fulton_County_,_Georgia and DeKalb_County_,_Georgia , in the United_States .
apposition France leaderName Manuel_Valls Baked_Alaska is from France , where Manuel_Valls and Gérard_Larcher are leaders .
apposition India leaderName Sumitra_Mahajan Bhajji originates from India where Narendra_Modi and Sumitra_Mahajan are leaders ,.
existential Akita_Museum_of_Art location Akita_Prefecture The Akita_Museum_of_Art is located in Akita_,_Akita , Akita_Prefecture , Japan .
passive_voice Contra_Costa_County_,_California isPartOf San_Francisco_Bay_Area Antioch_,_California , is part of Contra_Costa_County_,_California . Contra Costa County is part of the San_Francisco_Bay_Area . One of the languages spoken in California is Vietnamese_language .
passive_voice Contra_Costa_County_,_California isPartOf San_Francisco_Bay_Area Antioch_,_California , is part of Contra_Costa_County_,_California , which is part of the San_Francisco_Bay_Area in the state of California . Parts of California speak Chinese_language .
passive_voice Contra_Costa_County_,_California isPartOf San_Francisco_Bay_Area Antioch_,_California , is part of Contra_Costa_County_,_California , which is turn , is is part of the San_Francisco_Bay_Area , in , English_language Speaking , California .
apposition Agustín_Barboza deathPlace Paraguay Agustín_Barboza was Paraguay and died in Asunción which is part of Gran_Asunción .
direct_object Ballistic_(comicsCharacter) creator Doug_Moench Ballistic_(comicsCharacter) , also known as "Kelvin_Mao" , is a fictional comic superhero created by "Michael_Manley" and Doug_Moench .
direct_object Airman_(comicsCharacter) creator Harry_Sahle Airman_(comicsCharacter) , AKA "Drake_Stevens" was created by Harry_Sahle and George_Kapitan .
direct_object Blockbuster_(comicsCharacter) creator Roger_Stern The comic character , Blockbuster_(comicsCharacter) ( a . k . a . "Roland_Desmond" ) was created by Gardner_Fox and Roger_Stern .
existential Blockbuster_(comicsCharacter) creator Gardner_Fox The comics character Blockbuster_(comicsCharacter) , aka "Mark_Desmond" , was created by Gardner_Fox and Carmine_Infantino .
existential 1955_Dodge relatedMeanOfTransportation Plymouth_Plaza The 1955_Dodge is manufactured by Dodge and related to the Plymouth_Plaza and the DeSoto_Custom .
direct_object 1955_Dodge relatedMeanOfTransportation Plymouth_Plaza The 1955_Dodge and the Plymouth_Plaza and the DeSoto_Custom are related means of transport in that they are all cars . Plymouth_(automobile) are the manufacturers of the Plymouth_Plaza .
apposition Greece leader Prokopis_Pavlopoulos The AE_Dimitra_Efxeinoupolis club is located in Greece , where the leader is Alexis_Tsipras and / or Prokopis_Pavlopoulos .
existential Atlas_II launchSite Vandenberg_Air_Force_Base Originating from the United_States , the Atlas_II , was launched from both the Vandenberg_Air_Force_Base and Cape_Canaveral_Air_Force_Station .
passive_voice Alfa_Romeo_164 relatedMeanOfTransportation Saab_9000 The Alfa_Romeo_164 , made in Milan , along with the Fiat_Croma and the Saab_9000 are related cars .
possessif Alfa_Romeo_164 relatedMeanOfTransportation Lancia_Thema The Alfa_Romeo_164 , the Lancia_Thema and the Fiat_Croma are similar means of transport . The Alfa_Romeo_164 was assembled in Milan .
possessif Alfa_Romeo_164 relatedMeanOfTransportation Lancia_Thema The Alfa_Romeo_164 ( manufactured in Milan ) , Lancia_Thema and the Fiat_Croma are related means of transportation .
possessif Alexandria_,_Indiana isPartOf Monroe_Township_,_Madison_County_,_Indiana The capital of Indiana is Indianapolis . Alexandria_,_Indiana which is in Indiana , is part of the Monroe_Township_,_Madison_County_,_Indiana .
direct_object Almond order Rosids Almond is part of the order of Rosids and a member of the Rosales order . It is an ingredient of Ajoblanco .
possessif Almond order Rosids Almond is part of the order of Rosids and a member of the Rosales order . It is an ingredient of Ajoblanco .
possessif Siomay dishVariation Shumai An ingredient of the dish Batagor is Peanut_sauce , Batagor and Siomay are variatons of the same dish and Shumai is a variation of Siomay .
passive_voice Siomay dishVariation Shumai Batagor , made with Peanut_sauce , is a variation of the Siomay and similar Shumai dishes .
passive_voice Bacon_sandwich dishVariation BLT The BLT is a variation of the Club_sandwich and Bacon_sandwich and contains Tomato .
passive_voice Julia_Morgan significantBuilding Riverside_Art_Museum Julia_Morgan , architect of the Asilomar_Conference_Grounds , designed the significant Los_Angeles_Herald-Examiner building and The Riverside_Art_Museum .
possessif Aaron_Deer origin United_States Aaron_Deer , an Indie_rock musician is from Indianapolis and born in the United_States .
possessif Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Aaron_Turner ; played with Twilight_(band) , is part of the Drone_music genre and is associated with the group Greymachine .
possessif Al_Anderson_(NRBQ_band) associatedBand/associatedMusicalArtist The_Wildweeds Al_Anderson_(NRBQ_band) plays with the band NRBQ Rock_music and was a member of The_Wildweeds .
possessif Alison_O'Donnell associatedBand/associatedMusicalArtist Mellow_Candle Folk_music is the genre of Alison_O'Donnell , who was in the bands Mellow_Candle and the Flibbertigibbet band .
possessif Alison_O'Donnell associatedBand/associatedMusicalArtist Mellow_Candle Alison_O'Donnell ; performs Jazz music , was in the band Mellow_Candle and was a member of the Flibbertigibbet band .
possessif Andrew_Rayel associatedBand/associatedMusicalArtist Jonathan_Mendelsohn The musical genre of Andrew_Rayel is Trance_music . He is associated with the musical artists Jonathan_Mendelsohn and Christian_Burns .
direct_object Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Aaron_Turner is a musician and performer of the musical genre Post-metal with Lotus_Eaters_(band) , having previously played with Twilight_(band) .
direct_object Anders_Osborne associatedBand/associatedMusicalArtist Tab_Benoit Rhythm_and_blues artist Anders_Osborne is associated with artists Tab_Benoit , Galactic and Billy_Iuso .
existential Aaron_Turner associatedBand/associatedMusicalArtist Old_Man_Gloom Aaron_Turner played for Old_Man_Gloom and Lotus_Eaters_(band) . Aaron_Turner plays Black_metal music .
existential Alison_O'Donnell associatedBand/associatedMusicalArtist Mellow_Candle Alison_O'Donnell played Folk_music_of_Ireland for the band Mellow_Candle and the Flibbertigibbet band .
passive_voice Aaron_Turner associatedBand/associatedMusicalArtist Old_Man_Gloom Post-metal musician Aaron_Turner , is a member of Isis_(band) and formerly of Old_Man_Gloom .
direct_object Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Aaron_Turner ; played with Twilight_(band) , is part of the Drone_music genre and is associated with the group Greymachine .
passive_voice Al_Anderson_(NRBQ_band) associatedBand/associatedMusicalArtist The_Wildweeds Al_Anderson_(NRBQ_band) who is a member of the band NRBQ , performs Rock_music . He also played once with The_Wildweeds .
direct_object Aleksandra_Kovač associatedBand/associatedMusicalArtist Kristina_Kovač Aleksandra_Kovač , who is associated with Bebi_Dol Doi and Kristina_Kovač , is an exponent of Rhythm_and_blues .
existential Aleksandra_Kovač associatedBand/associatedMusicalArtist Kristina_Kovač Aleksandra_Kovač is musically involved with Kristina_Kovač and plays Rhythm_and_blues with Kornelije_Kovač in the same band .
existential Alison_O'Donnell recordLabel Osmosys_Records Jazz performer , Alison_O'Donnell , is signed to the record labels Kissing_Spell_Records and Osmosys_Records .
direct_object Anders_Osborne recordLabel Shanachie_Records Anders_Osborne ' s genre is Rock_music . He is signed to Shanachie_Records having been previously signed to the record label ' Alligator_Records '.
existential Rhythm_and_blues derivative Funk Andra_(singer) plays Rhythm_and_blues music which is derived from Funk music . Disco is also derived from Rhythm_and_blues .
direct_object Andrew_Rayel associatedBand/associatedMusicalArtist Jonathan_Mendelsohn The musical genre of Andrew_Rayel is Trance_music . He is associated with the musical artists Jonathan_Mendelsohn and Christian_Burns .
direct_object Andrew_Rayel associatedBand/associatedMusicalArtist Christian_Burns Andrew_Rayel is associated with Bobina and also Christian_Burns who is an exponent of House_music .
direct_object Anders_Osborne associatedBand/associatedMusicalArtist Voice_of_the_Wetlands_All-Stars Anders_Osborne is associated with the Galactic band and he is an exponent of Rock_music . He is associated with Tab_Benoit and Voice_of_the_Wetlands_All-Stars .
possessif Andra_(singer) associatedBand/associatedMusicalArtist Marius_Moga Rhythm_and_blues is the genre of the singer , Andra_(singer) , who is associated with , Marius_Moga and Andreea_Bălan .
possessif Andrew_Rayel associatedBand/associatedMusicalArtist Jonathan_Mendelsohn House_music is the genre of musician , Christian_Burns who is an associate of Andrew_Rayel . Andrew_Rayel is associated with the musical artist Jonathan_Mendelsohn .
coordinated_full_clauses Aaron_Deer origin United_States From Indiana ( United_States ) , Aaron_Deer ' s musical genre is , Indie_rock .
coordinated_full_clauses Al_Anderson_(NRBQ_band) associatedBand/associatedMusicalArtist The_Wildweeds Having previously played with The_Wildweeds , Al_Anderson_(NRBQ_band) , whose musical genre is Country_music , plays with the band NRBQ .
coordinated_full_clauses Synthpop stylisticOrigin New_wave_music Synthpop , with its origins in Disco and New_wave_music , is the genre of artist Alex_Day .
apposition Aaron_Turner associatedBand/associatedMusicalArtist House_of_Low_Culture Aaron_Turner ' s genre is Post-metal , is associated with the group Greymachine and performed for House_of_Low_Culture .
apposition Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Black_metal musician Aaron_Turner , played with Twilight_(band) and is in Lotus_Eaters_(band) .
apposition Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Aaron_Turner who is a Post-metal artist and member of Twilight_(band) , performs for Mamiffer .
apposition Aaron_Turner associatedBand/associatedMusicalArtist Twilight_(band) Aaron_Turner played with the bands Twilight_(band) Sumac_(band) . He is a Black_metal musician .
apposition Al_Anderson_(NRBQ_band) associatedBand/associatedMusicalArtist The_Wildweeds Al_Anderson_(NRBQ_band) performs Country_music with the band NRBQ . He was previously a member of The_Wildweeds .
apposition Alison_O'Donnell associatedBand/associatedMusicalArtist United_Bible_Studies Alison_O'Donnell was a member of the Flibbertigibbet band and a musician for the band United_Bible_Studies . She also played Folk_music_of_Ireland .
apposition Andra_(singer) associatedBand/associatedMusicalArtist Puya_(singer) Andra_(singer) ' s genre is Rhythm_and_blues . She is associated with the band CRBL and with Puya_(singer) .
existential Anders_Osborne associatedBand/associatedMusicalArtist Voice_of_the_Wetlands_All-Stars Blues performer Anders_Osborne is associated with musical artists Billy_Iuso and Tab_Benoit as well as the band , Voice_of_the_Wetlands_All-Stars .
apposition Anders_Osborne associatedBand/associatedMusicalArtist Tab_Benoit Anders_Osborne is associated with the musical artists Billy_Iuso and Tab_Benoit . He is an exponent of Rock_music and worked with the Galactic band .
possessif Agustín_Barboza birthPlace Paraguay Agustín_Barboza , born in Asunción , Paraguay , was signed to Philips_Records .
existential Agustín_Barboza birthPlace Paraguay Agustín_Barboza , born in Asunción , Paraguay , was signed to Philips_Records .
existential Bolt_(comicsCharacter) creator Gary_Cohn_(comics) The comic character of Bolt_(comicsCharacter) was created by Gary_Cohn_(comics) and the Puerto_Ricans national , Ernie_Colón .
existential Auron_(comicsCharacter) creator Walt_Simonson Karl_Kesel , an Americans along with Walt_Simonson created the comic book character Auron_(comicsCharacter) .
apposition Bolt_(comicsCharacter) creator Paris_Cullins Ernie_Colón Colton and Paris_Cullins created the comic character Bolt_(comicsCharacter) . Cullins is from the United_States .
direct_object Auron_(comicsCharacter) creator Marv_Wolfman Marv_Wolfman and Karl_Kesel are the creators of the comic character Auron_(comicsCharacter) , whose full name is "Lambien" .
passive_voice Arion_(comicsCharacter) creator Paul_Kupperberg Jan_Duursema won the Eisner_Award and is the creator of the comic book character Arion_(comicsCharacter) along with Paul_Kupperberg .
existential 1634:_The_Bavarian_Crisis precededBy The_Grantville_Gazettes The_Grantville_Gazettes preceded 1634:_The_Baltic_War ( written by David_Weber ) and 1634:_The_Bavarian_Crisis .
passive_voice Weymouth_Sands followedBy Maiden_Castle_(novel) Weymouth_Sands , written by John_Cowper_Powys , is a prequel to Maiden_Castle_(novel) and a sequel to A_Glastonbury_Romance .
apposition Weymouth_Sands followedBy Maiden_Castle_(novel) These books were written in this order , Wolf_Solent , A_Glastonbury_Romance , Weymouth_Sands and Maiden_Castle_(novel) .
existential Weymouth_Sands followedBy Maiden_Castle_(novel) These books were published in the following order : Wolf_Solent , A_Glastonbury_Romance , Weymouth_Sands and Maiden_Castle_(novel) .
existential Adam_Koc award Legion_of_Honour Adam_Koc fought in battles in the Polish–Soviet_War and was awarded the Legion_of_Honour and the Cross_of_Valour_(Poland) .
possessif Adam_Koc award Legion_of_Honour Adam_Koc fought in the Polish–Soviet_War and is a recipient of the Cross_of_Valour_(Poland) and the Legion_of_Honour Award .
existential Adonis_Georgiadis birthPlace Greece Born in Athens , Greece , Adonis_Georgiadis , has worked as the office workedAt workedAsObjet1 .
existential Adonis_Georgiadis birthPlace Greece Born in Athens , Greece , Adonis_Georgiadis , worked for the office of the office workedAt workedAsObjet1 .
direct_object Adonis_Georgiadis birthPlace Greece Adonis_Georgiadis was born in Athens , Greece and works as office workedAt workedAsObjet1 .
possessif Saranac_Lake_,_New_York isPartOf United_States Adirondack_Regional_Airport is located in Saranac_Lake_,_New_York Harrietstown_,_New_York United_States .
passive_voice Saranac_Lake_,_New_York isPartOf Harrietstown_,_New_York Adirondack_Regional_Airport serves the city of Saranac_Lake_,_New_York , which is part of Harrietstown_,_New_York , in the United_States .
apposition Curitiba isPartOf South_Region_,_Brazil Curitiba is part of the State of Paraná_(state) , in the South_Region_,_Brazil . The Afonso_Pena_International_Airport serves the city .
possessif Adirondack_Regional_Airport cityServed Saranac_Lake_,_New_York Adirondack_Regional_Airport serves the city of Lake_Placid_,_New_York and Saranac_Lake_,_New_York . The runway is 2003.0 feet long .
existential Adirondack_Regional_Airport cityServed Saranac_Lake_,_New_York Adirondack_Regional_Airport serves the city of Saranac_Lake_,_New_York and the residents of Lake_Placid_,_New_York and has a runway that ' s 1219 long .
relative_adverb Addis_Ababa isPartOf Addis_Ababa_Stadium Addis_Ababa_Stadium and the Addis_Ababa_City_Hall are in Addis_Ababa , Ethiopia . Amharic is a language spoken there .
relative_adverb Tirstrup isPartOf Central_Denmark_Region Denmark uses the Danish_language and is the location of Aarhus_Airport in Tirstrup located in the Central_Denmark_Region .
apposition Allama_Iqbal_International_Airport cityServed Lahore Allama_Iqbal_International_Airport is located in Punjab_,_Pakistan and serves the city of Lahore . Pakistan is home to Punjab and is led by Anwar_Zaheer_Jamali .
apposition 3Arena owner Live_Nation_Entertainment 3Arena is located in Dublin , which is part of Leinster in the Republic_of_Ireland . It is owned by Live_Nation_Entertainment .
apposition Denmark demonym Danes Aarhus_Airport is located in Tirstrup which is part of the Central_Denmark_Region . The demonym for the people of Denmark is Danes .
relative_subject Angola_International_Airport elevationAboveTheSeaLevel_(in_metres) 159 Angola_International_Airport is located in Ícolo_e_Bengo in Luanda_Province , Angola . It is 159 metres above sea level ..
relative_subject Chicago leaderName Rahm_Emanuel Rahm_Emanuel is the leader of Chicago ( part of DuPage_County_,_Illinois , United_States ) where 300_North_LaSalle is located .
relative_subject Ethiopia leaderName Hailemariam_Desalegn Addis_Ababa_Stadium and Addis_Ababa_City_Hall is located in Addis_Ababa , Ethiopia , where Hailemariam_Desalegn is the leader .
coordinated_clauses Chicago leaderName Rahm_Emanuel Rahm_Emanuel is a leader in Chicago , DuPage_County_,_Illinois , United_States , which is the location of 300_North_LaSalle .
coordinated_clauses Denmark language Faroese_language The location of Aarhus_Airport is Tirstrup which is part of the Central_Denmark_Region , in Denmark where one of the languages is Faroese_language .
existential 3Arena owner Live_Nation_Entertainment Live_Nation_Entertainment own 3Arena in Dublin , Leinster , Republic_of_Ireland .
apposition Atlantic_City_International_Airport elevationAboveTheSeaLevel_(in_metres) 23 Atlantic_City_International_Airport is located at Egg_Harbor_Township_,_New_Jersey , Atlantic_County_,_New_Jersey in the United_States and is 23 metres above sea level .
apposition San_Sebastián_de_los_Reyes leaderParty People's_Party_(Spain) San_Sebastián_de_los_Reyes is part of the Community_of_Madrid , in Spain . It is where the Adolfo_Suárez_Madrid–Barajas_Airport is located and is lead by the People's_Party_(Spain) .
relative_subject Denmark language German_language Denmark uses the German_language and is the location of Aarhus_Airport in Tirstrup in the Central_Denmark_Region .
apposition Angola_International_Airport runwayName "05L/23R" Angola_International_Airport is located at Ícolo_e_Bengo , Luanda_Province , Angola and has a runway called "05L/23R" .
passive_voice Denmark language Danish_language Tirstrup , part of the Central_Denmark_Region , is the location of Aarhus_Airport in Denmark where the language spoken is Danish_language .
passive_voice Angola_International_Airport runwayName "South_Runway" Angola_International_Airport , has a runway named "South_Runway" and is located at Ícolo_e_Bengo . Ícolo_e_Bengo is in the Luanda_Province , in Angola .
relative_subject Manhattan leaderName Cyrus_Vance_,_Jr. The Asser_Levy_Public_Baths are located in Brooklyn , Manhattan , New_York_City where Cyrus_Vance_,_Jr. is one of the leaders .
apposition Uttar_Pradesh leaderName Ram_Naik Agra_Airport is located in Uttar_Pradesh ( lead by Ram_Naik ) which is part of Awadh and Bundelkhand .
passive_voice Agra_Airport IATA_Location_Identifier "AGR" "AGR" is the ATA Location Identifier for Agra_Airport in Uttar_Pradesh which is part of both Awadh and Bundelkhand .
relative_object Appleton_International_Airport cityServed Appleton_,_Wisconsin Appleton_International_Airport ( Appleton_,_Wisconsin ) is located in Greenville_,_Wisconsin which is in Grand_Chute_,_Wisconsin and part of Dale_,_Wisconsin .
passive_voice Agra_Airport ICAO_Location_Identifier "VIAG" Agra_Airport has the ICAO location identifier "VIAG" and is in Uttar_Pradesh , part of Awadh and Bundelkhand .
passive_voice Appleton_International_Airport cityServed Appleton_,_Wisconsin Appleton_International_Airport serves the city of Appleton_,_Wisconsin in Greenville_,_Wisconsin . Both Ellington_,_Wisconsin and Menasha_(town)_,_Wisconsin are parts of Greenville_,_Wisconsin .
direct_object Agra_Airport IATA_Location_Identifier "AGR" "AGR" is the ATA Location Identifier for Agra_Airport in Uttar_Pradesh which is part of both Awadh and Bundelkhand .
relative_subject Appleton_International_Airport cityServed Appleton_,_Wisconsin Greenville_,_Wisconsin , where Appleton_International_Airport ( serving Appleton_,_Wisconsin ) is located , is part of Ellington_,_Wisconsin and Dale_,_Wisconsin .
coordinated_clauses New_York_City country United_States Asser_Levy_Public_Baths are in New_York_City , United_States which is part of Manhattan . New_York_City was part of New_Netherland .
coordinated_clauses Agra_Airport operatingOrganisation Airports_Authority_of_India Agra_Airport is in Uttar_Pradesh which is ni turn part of both Awadh and Bundelkhand . The Airports_Authority_of_India is the operating organisation of Agra_Airport .
apposition Al_Asad_Airbase location Iraq The operating organisation of Al_Asad_Airbase in Iraq is the United_States_Air_Force . The runway length is 3992.88 and has the name "08/26" .
relative_adverb Aarhus_Airport elevationAboveTheSeaLevel_(in_metres) 25.0 Aarhus_Airport is 25.0 meters above sea level with a runway length of 2777.0 and named "10L/28R" . It is operated by Aktieselskab .
relative_adverb Al_Asad_Airbase location "Al_Anbar_Province_,_Iraq" Al_Asad_Airbase is located in "Al_Anbar_Province_,_Iraq" and is operated by the United_States_Air_Force . It has a runway name of "09L/27R" with a length of 3990 .
possessif Aarhus_Airport elevationAboveTheSeaLevel_(in_metres) 25.0 Aarhus_Airport is run by "Aarhus_Lufthavn_A/S" and is 25.0 metres above sea level . The runway length is 2776.0 and it is called "10R/28L" .
passive_voice Adolfo_Suárez_Madrid–Barajas_Airport location "Madrid_,_Paracuellos_de_Jarama_,_San_Sebastián_de_los_Reyes_and_Alcobendas" Located in "Madrid_,_Paracuellos_de_Jarama_,_San_Sebastián_de_los_Reyes_and_Alcobendas" and operated by ENAIRE , Adolfo_Suárez_Madrid–Barajas_Airport runway name is "14L/32R" and its runway length is 3500 .
passive_voice Adolfo_Suárez_Madrid–Barajas_Airport location "Madrid_,_Paracuellos_de_Jarama_,_San_Sebastián_de_los_Reyes_and_Alcobendas" Adolfo_Suárez_Madrid–Barajas_Airport is located at "Madrid_,_Paracuellos_de_Jarama_,_San_Sebastián_de_los_Reyes_and_Alcobendas" and run by ENAIRE . Its runway name is "18R/36L" and it is 3500.0 m long ,.
coordinated_clauses Aarhus_Airport elevationAboveTheSeaLevel_(in_metres) 25.0 Aarhus_Airport which is 25.0 metres above sea level , is operated by "Aarhus_Lufthavn_A/S" . The runway name is "10R/28L" and has a length of 2776.0 .
relative_subject Adolfo_Suárez_Madrid–Barajas_Airport location Madrid Adolfo_Suárez_Madrid–Barajas_Airport is found in Madrid and is operated by ENAIRE . The runway name is "18L/36R" and has a length of 3500.0 .
juxtaposition Atlantic_City_International_Airport elevationAboveTheSeaLevel_(in_metres) 23 The Atlantic_City_International_Airport is operated by the Port_Authority_of_New_York_and_New_Jersey . "13/31" is the name of the runway and it is 23 meters above sea level . Its length is 3048 .
possessif Al_Asad_Airbase location "Al_Anbar_Province_,_Iraq" The Al_Asad_Airbase is situated in the "Al_Anbar_Province_,_Iraq" and is operated by the United_States_Air_Force . The runway length of Al_Asad_Airbase is 3992.88 and it is called "08/26" .
coordinated_clauses Atlantic_City_International_Airport elevationAboveTheSeaLevel_(in_metres) 23 Atlantic_City_International_Airport is operated by the Port_Authority_of_New_York_and_New_Jersey and is 23 metres above sea level . The runway name is "4/22" and it has a length of 1873 .
possessif Al_Asad_Airbase location Iraq Al_Asad_Airbase in Iraq is operated by the United_States_Air_Force . The runway name is "08/26" and it has a length of 3992.88 .
relative_subject Al_Asad_Airbase location "Al_Anbar_Province_,_Iraq" The United_States_Air_Force is the operating organisation for Al_Asad_Airbase in the "Al_Anbar_Province_,_Iraq" . The runway name at the airport is "08/26" and has a length of 3078.48 .
relative_subject Al_Asad_Airbase location "Al_Anbar_Province_,_Iraq" The United_States_Air_Force operates the Al_Asad_Airbase ; located in "Al_Anbar_Province_,_Iraq" . The runway named "09R/27L" is 3990.0 in length .
relative_subject Aarhus_Airport elevationAboveTheSeaLevel_(in_metres) 25.0 Aarhus_Airport is operated by the Aktieselskab organisation . Its runway name is "10L/28R" , is 25.0 meters above sea level and 2777.0 meters long .
relative_adverb Al_Asad_Airbase location "Al_Anbar_Province_,_Iraq" The United_States_Air_Force is the operating organisation for Al_Asad_Airbase in the "Al_Anbar_Province_,_Iraq" . The name of the runway at the Airbase is "09R/27L" and it has a length of 3078.48 .
coordinated_full_clauses Atlantic_City_International_Airport elevationAboveTheSeaLevel_(in_metres) 23 Operated by the Port_Authority_of_New_York_and_New_Jersey , Atlantic_City_International_Airport ' s runway name is "13/31" . It has a runway length of 3048 and is 23 metres above sea level .
possessif Atlantic_City_International_Airport elevationAboveTheSeaLevel_(in_metres) 23 Atlantic_City_International_Airport is 23 metres above sea level and is run by The Port_Authority_of_New_York_and_New_Jersey . The runway is 3048 long and is called "13/31" /.
existential Athens_International_Airport location Spata Athens_International_Airport , located in Spata , serves the city of Athens . It has a runway length of 4000 and is elevated 94 metres above sea level .
possessif Athens_International_Airport location Spata Athens_International_Airport is located in Spata and serves the city of Athens . The airport is 94 metres above sea level and has a runway length of 3800.0 .
relative_subject Abilene_Regional_Airport runwayName "17L/35R" Abilene_Regional_Airport is 546 m . above sea level , a runway length of 1121 ( "17L/35R" ) and serves Abilene_,_Texas .
relative_subject Andrews_County_Airport location Texas Andrews_County_Airport located in Texas and serving the city of Andrews_,_Texas , has a runway length of 896 and is 973 metres above sea level .
apposition Amsterdam_Airport_Schiphol runwayName "06/24_'Kaagbaan'" Amsterdam_Airport_Schiphol serves the city of Amsterdam . The runway named "06/24_'Kaagbaan'" is 3800 long . The airport is -3.3528 above sea level .
apposition Athens_International_Airport location Spata Athens_International_Airport , located in Spata , serves the city of Athens . It has a runway length of 4000 and is elevated 94 metres above sea level .
coordinated_clauses Amsterdam_Airport_Schiphol runwayName "18L/36R_'Aalsmeerbaan'" Amsterdam_Airport_Schiphol is -3.3528 above sea level , has a runway name "18L/36R_'Aalsmeerbaan'" ' which is 2014 in length and serves the city of Amsterdam .
relative_subject Athens_International_Airport location Spata Athens_International_Airport is located in Spata and serves the city of Athens . The airport is 94 metres above sea level and has a runway length of 3800.0 .
coordinated_clauses Athens_International_Airport location Spata At 94 metres above sea level , with a runway length of 4000 , Athens_International_Airport is located in Spata and serves the city of Athens .
coordinated_full_clauses Abilene_Regional_Airport runwayName "17L/35R" Abilene_Regional_Airport served Abilene_,_Texas and is 546 metres above sea level . The runway is called "17L/35R" and is 2195.0 long .
coordinated_full_clauses Amsterdam_Airport_Schiphol runwayName "18C/36C_'Zwanenburgbaan'" Amsterdam_Airport_Schiphol serves the city of Amsterdam is -3.3528 metres above sea level and has runway "18C/36C_'Zwanenburgbaan'" ' that is 2014 long .
juxtaposition Abilene_Regional_Airport runwayName "17R/35L" Abilene_Regional_Airport serves Abilene_,_Texas and is 546 metres above sea level . The runway name is "17R/35L" and has a length of 2194 .
juxtaposition Al-Taqaddum_Air_Base location Habbaniyah Al-Taqaddum_Air_Base serves the city of Fallujah and is located in Habbaniyah . The airbase is 84 metres above sea level and has a runway length of 4019 .
juxtaposition Andrews_County_Airport location Texas Andrews_County_Airport is located in Texas and serves the city of Andrews_,_Texas . It has an 8 km long runway and situated 973 meters above sea level .
juxtaposition Athens_International_Airport location Spata Located in Spata and serving the city of Athens , Athens_International_Airport is 94 metres above sea level and has a runway length of 4000 .
apposition Amsterdam_Airport_Schiphol runwayName "09/27_'Buitenveldertbaan'" Amsterdam_Airport_Schiphol serves the city of Amsterdam and is -3.3528 from sea level . The runway name is "09/27_'Buitenveldertbaan'" and is 2014 in length .
apposition Adirondack_Regional_Airport runwayLength 1219.0 Adirondack_Regional_Airport serves both the city of Lake_Placid_,_New_York and city of Saranac_Lake_,_New_York . It has a runway length of 1219.0 and is 507 metres above sea level .
passive_voice Alpena_County_Regional_Airport runwayLength 1533.0 Alpena_County_Regional_Airport has a runway length of 1533.0 and is 210 metres above sea level . It is located in Maple_Ridge_Township_,_Alpena_County_,_Michigan , United_States .
direct_object Egg_Harbor_Township_,_New_Jersey isPartOf Atlantic_County_,_New_Jersey Atlantic_City_International_Airport is located at Egg_Harbor_Township_,_New_Jersey , Atlantic_County_,_New_Jersey in the United_States and is 23 metres above sea level .
coordinated_clauses Alpena_County_Regional_Airport runwayLength 2744 Alpena_County_Regional_Airport is located in the Wilson_Township_,_Alpena_County_,_Michigan , United_States . The runway length is 2744 and it is 210 metres above sea level .
relative_subject Al_Asad_Airbase operatingOrganisation United_States_Air_Force The United_States_Air_Force is the operating organisation for Al_Asad_Airbase in the "Al_Anbar_Province_,_Iraq" . The name of the runway at the Airbase is "09R/27L" and it has a length of 3078.48 .
direct_object Adolfo_Suárez_Madrid–Barajas_Airport operatingOrganisation ENAIRE Adolfo_Suárez_Madrid–Barajas_Airport can be found in "Madrid_,_Paracuellos_de_Jarama_,_San_Sebastián_de_los_Reyes_and_Alcobendas" . The runway named "14L/32R" is 3500 long . The Airport is operated by ENAIRE .
passive_voice Afonso_Pena_International_Airport elevationAboveTheSeaLevel_(in_metres) 911.0 Located in São_José_dos_Pinhais , Afonso_Pena_International_Airport has the elevation of 911.0 meters above the sea level . It also has a runway with the name "11/29" , as well as a runway length of 2215.0 .
coordinated_full_clauses Al_Asad_Airbase operatingOrganisation United_States_Air_Force Al_Asad_Airbase , located in "Al_Anbar_Province_,_Iraq" , is operated by the United_States_Air_Force . Its runway name is "08/26" and its length 3990 .
coordinated_full_clauses Adolfo_Suárez_Madrid–Barajas_Airport operatingOrganisation ENAIRE Adolfo_Suárez_Madrid–Barajas_Airport is located in "Madrid_,_Paracuellos_de_Jarama_,_San_Sebastián_de_los_Reyes_and_Alcobendas" and is operated by ENAIRE . It has a runway name of "14R/32L" with a length of 3500 .
coordinated_full_clauses Afonso_Pena_International_Airport elevationAboveTheSeaLevel_(in_metres) 911.0 Afonso_Pena_International_Airport is located in São_José_dos_Pinhais . "11/29" is the name given to the runway which is 1800 meters long and 911.0 meters above sea level .
coordinated_full_clauses Al_Asad_Airbase operatingOrganisation United_States_Air_Force The United_States_Air_Force operate Al_Asad_Airbase which is located at "Al_Anbar_Province_,_Iraq" . The base has a runway length of 3990 which is named "09L/27R" .
apposition Al_Asad_Airbase operatingOrganisation United_States_Air_Force The United_States_Air_Force operates the Al_Asad_Airbase in Iraq . It has a runway length of 3990 and it ' s runway is named "08/26" .
possessif Adolfo_Suárez_Madrid–Barajas_Airport operatingOrganisation ENAIRE Adolfo_Suárez_Madrid–Barajas_Airport is located in "Madrid_,_Paracuellos_de_Jarama_,_San_Sebastián_de_los_Reyes_and_Alcobendas" and is operated by ENAIRE . It has a runway name of "14R/32L" with a length of 3500 .
coordinated_full_clauses Al_Asad_Airbase operatingOrganisation United_States_Air_Force The United_States_Air_Force is the operating organisation for Al_Asad_Airbase in the "Al_Anbar_Province_,_Iraq" . The runway name at the airport is "08/26" and has a length of 3078.48 .
relative_subject Adolfo_Suárez_Madrid–Barajas_Airport operatingOrganisation ENAIRE Adolfo_Suárez_Madrid–Barajas_Airport is located at "Madrid_,_Paracuellos_de_Jarama_,_San_Sebastián_de_los_Reyes_and_Alcobendas" . It is operated by ENAIRE and has a runway name of "14R/32L" with a length of 3500 .
relative_subject Adolfo_Suárez_Madrid–Barajas_Airport operatingOrganisation ENAIRE Adolfo_Suárez_Madrid–Barajas_Airport is found in Madrid and is operated by ENAIRE . The runway name is "18L/36R" and has a length of 3500.0 .
relative_subject Afonso_Pena_International_Airport elevationAboveTheSeaLevel_(in_metres) 911.0 Afonso_Pena_International_Airport is located in São_José_dos_Pinhais and is 911.0 metres above sea level . The runway name is "15/33" and it has a length of 1800 .
existential Al_Asad_Airbase operatingOrganisation United_States_Air_Force located at "Al_Anbar_Province_,_Iraq" , Al_Asad_Airbase ; has a runway length , 3992.88 , a runway name , "08/26" and is operated by the United_States_Air_Force .
apposition Curitiba leaderName Democratic_Labour_Party_(Brazil) Curitiba is part of the state of Paraná_(state) in South_Region_,_Brazil . It is lead by the Democratic_Labour_Party_(Brazil) and served by Afonso_Pena_International_Airport .
coordinated_clauses Paracuellos_de_Jarama country Spain Paracuellos_de_Jarama is a Community_of_Madrid , Spain . The Adolfo_Suárez_Madrid–Barajas_Airport is in Paracuellos_de_Jarama , which is located 610 metres above sea level .
apposition Egg_Harbor_Township_,_New_Jersey country United_States Atlantic_City_International_Airport is in Egg_Harbor_Township_,_New_Jersey , Atlantic_County_,_New_Jersey , United_States and is 23 metres above sea level .
direct_object San_Sebastián_de_los_Reyes country Spain Adolfo_Suárez_Madrid–Barajas_Airport is elevated 610 metres above sea level and is located at San_Sebastián_de_los_Reyes in Spain and is part of the Community_of_Madrid .
coordinated_clauses Paracuellos_de_Jarama country Spain The Adolfo_Suárez_Madrid–Barajas_Airport which is 610 metres above sea level , is in Paracuellos_de_Jarama . part of the Community_of_Madrid in Spain .
coordinated_clauses Madrid country Spain Adolfo_Suárez_Madrid–Barajas_Airport in Madrid has an elevation of 610 metres above sea level . Madrid is part of the Community_of_Madrid and located in Spain .
relative_adverb South_Africa capital Cape_Town Lead by Cyril_Ramaphosa and Jacob_Zuma , South_Africa has the capital Cape_Town and is the location of the address 11_Diagonal_Street .
coordinated_full_clauses Chicago country United_States Rahm_Emanuel is the leader of Chicago , Cook_County_,_Illinois , United_States which is the location of 300_North_LaSalle .
relative_subject Chicago country United_States 300_North_LaSalle is located in Chicago , Illinois , United_States , where Susana_Mendoza is the leader .
relative_subject Illinois country United_States 300_North_LaSalle is located in Chicago , Illinois , United_States and the leader of Chicago is Susana_Mendoza .
direct_object Illinois country United_States Susana_Mendoza is a leader in Chicago , Illinois , United_States where 300_North_LaSalle is located .
relative_adverb Cleveland country United_States 200_Public_Square is in Cleveland , Ohio , United_States ( local leader Frank_G._Jackson ).
apposition Chicago country United_States 300_North_LaSalle is located in Chicago , Illinois , United_States , where Susana_Mendoza is the leader .
relative_adverb Illinois country United_States 300_North_LaSalle is located in Chicago , Illinois , United_States . The city leader is Rahm_Emanuel .
coordinated_clauses Cleveland country United_States 200_Public_Square is in Cleveland , Ohio in the United_States . Frank_G._Jackson is a leader in Cleveland .
passive_voice Cleveland country United_States Part of the Cuyahoga_County_,_Ohio if the United_States , Cleveland has the leader Frank_G._Jackson and is the location of the 200_Public_Square .
possessif Cleveland country United_States 200_Public_Square is in Cleveland , United_States , which is part of Cuyahoga_County_,_Ohio . Frank_G._Jackson is the leader of Cleveland .
relative_subject Belgium leaderName Philippe_of_Belgium Antwerp_International_Airport serves Antwerp in Belgium . Philippe_of_Belgium and Charles_Michel are leaders of Belgium , where German_language is the language spoken .
relative_adverb Belgium leaderName Charles_Michel Antwerp_International_Airport serves the city of Antwerp which is a popular tourist destination in Belgium . One of the languages spoken in Belgium is German_language and the leader is Charles_Michel .
direct_object Belgium leaderName Philippe_of_Belgium The Dutch_language is spoken in Belgium which is lead by Philippe_of_Belgium . Antwerp , in Belgium is served by Antwerp_International_Airport .
apposition Iraq leaderName Haider_al-Abadi Al-Taqaddum_Air_Base serves the city of Fallujah in Iraq . The Kurdish_languages are spoken in Iraq , where there are leaders called Haider_al-Abadi and Fuad_Masum .
passive_voice Iraq leaderName Fuad_Masum Al-Taqaddum_Air_Base serves the city of Fallujah in Iraq . Fuad_Masum leads that country where Arabic is the spoken language .
relative_subject Adolfo_Suárez_Madrid–Barajas_Airport runwayName "14L/32R" Found in "Madrid_,_Paracuellos_de_Jarama_,_San_Sebastián_de_los_Reyes_and_Alcobendas" , Adolfo_Suárez_Madrid–Barajas_Airport is operated by ENAIRE . It has a runway with the length of 4349 and "14L/32R" is the runway name .
passive_voice Adolfo_Suárez_Madrid–Barajas_Airport runwayName "18R/36L" Adolfo_Suárez_Madrid–Barajas_Airport is located at "Madrid_,_Paracuellos_de_Jarama_,_San_Sebastián_de_los_Reyes_and_Alcobendas" and run by ENAIRE . Its runway name is "18R/36L" and it is 3500.0 m long ,.
possessif Al_Asad_Airbase runwayName "09L/27R" Al_Asad_Airbase is operated by the United_States_Air_Force and is located at "Al_Anbar_Province_,_Iraq" . The runway name is "09L/27R" and has a length of 3990 .
passive_voice Allama_Iqbal_International_Airport cityServed Lahore The Pakistan_Civil_Aviation_Authority is the operating organisation of the Allama_Iqbal_International_Airport in Pakistan . The airport serves the city of Lahore and has a runway length of 3360.12 .
coordinated_clauses Ashgabat_International_Airport elevationAboveTheSeaLevel_(in_metres) 211 Ashgabat_International_Airport located in Ashgabat is operated by Turkmenistan_Airlines , it ' s runway is 211 metres above sea level and has a runway length of 900 .
direct_object Al_Asad_Airbase runwayName "08/26" Al_Asad_Airbase ( located in Iraq ) is operated by the United_States_Air_Force . The length of the "08/26" runway is 3992.88 .
passive_voice Adolfo_Suárez_Madrid–Barajas_Airport runwayName "18L/36R" Operated by ENAIRE , the Adolfo_Suárez_Madrid–Barajas_Airport is in Madrid . It has a runway name "18L/36R" and a runway length of 3500.0 .
passive_voice United_States_Air_Force attackAircraft Lockheed_AC-130 The United_States_Air_Force , who is the operating organisation for Al_Asad_Airbase , fought battles in the Korean_War . They deploy the Lockheed_AC-130 attack aircraft and the McDonnell_Douglas_F-15_Eagle aircraft fighter .
relative_subject United_States_Air_Force attackAircraft Lockheed_AC-130 The attack aircraft , the Lockheed_AC-130 and the fighter aircraft , McDonnell_Douglas_F-15_Eagle are used by the United_States_Air_Force . It is the operating organisation for Al_Asad_Airbase and one of the noted United_States_Air_Force battles was the Invasion_of_Grenada .
coordinated_clauses United_States_Air_Force attackAircraft Lockheed_AC-130 The United_States_Air_Force , which is the operating organisation for Al_Asad_Airbase was involved in battles at the Invasion_of_Grenada . They deploy the Lockheed_AC-130 attack aircraft and the General_Dynamics_F-16_Fighting_Falcon .
existential United_States_Air_Force attackAircraft Lockheed_AC-130 The United_States_Air_Force deploy the aircraft fighter General_Dynamics_F-16_Fighting_Falcon and the attack aircraft Lockheed_AC-130 . They fought in the Korean_War and currently operate the Al_Asad_Airbase .
possessif United_States_Air_Force attackAircraft Lockheed_AC-130 Al_Asad_Airbase is run by the United_States_Air_Force who fought in the Korean_War , were involved in battles at the Invasion_of_Grenada and deploy the Lockheed_AC-130 as an attack aircraft .
relative_adverb San_Sebastián_de_los_Reyes isPartOf Community_of_Madrid San_Sebastián_de_los_Reyes is part of the Community_of_Madrid , in Spain . It is where the Adolfo_Suárez_Madrid–Barajas_Airport is located and is lead by the People's_Party_(Spain) .
apposition San_Sebastián_de_los_Reyes isPartOf Community_of_Madrid San_Sebastián_de_los_Reyes is part of the Community_of_Madrid , in Spain . It is where the Adolfo_Suárez_Madrid–Barajas_Airport is located and is lead by the People's_Party_(Spain) .
direct_object Alderney_Airport elevationAboveTheSeaLevel_(in_metres) 88 Alderney is served by Alderney_Airport which is located 88 meters above sea level . The 1st runway , which is made from Poaceae is 497 in length .
possessif Alderney_Airport elevationAboveTheSeaLevel_(in_metres) 88 Alderney is served by Alderney_Airport and is 88 metres above sea level . It has a 1st runway with an Asphalt surface which is 497 in length .
passive_voice Poaceae division Flowering_plant Poaceae , which is the surface of the 1st runway at Alderney_Airport , belongs to the division of Flowering_plant and is of the order Poales and the class Monocotyledon .
coordinated_clauses Adirondack_Regional_Airport cityServed Saranac_Lake_,_New_York Adirondack_Regional_Airport is 507 metres above sea level and serves Lake_Placid_,_New_York and Saranac_Lake_,_New_York . It is located in Harrietstown_,_New_York and has a runway length of 1219 .
coordinated_clauses Andrews_County_Airport cityServed Andrews_,_Texas Andrews_County_Airport in Texas serves the city of Andrews_,_Texas . It is 973 metres above sea level and has a runway length of 929 .
existential Athens_International_Airport cityServed Athens At 94 metres above sea level , with a runway length of 4000 , Athens_International_Airport is located in Spata and serves the city of Athens .
coordinated_clauses Ashgabat_International_Airport operatingOrganisation Turkmenistan_Airlines The operating organization for Ashgabat_International_Airport in Ashgabat is Turkmenistan_Airlines . The runway length is 2989 and the airport is 211 metres above sea level .
possessif Andrews_County_Airport cityServed Andrews_,_Texas Andrews_County_Airport is located in Texas and serves the city of Andrews_,_Texas . It has an 8 km long runway and situated 973 meters above sea level .
apposition Andrews_County_Airport cityServed Andrews_,_Texas Andrews_County_Airport is located in Texas and serves the city of Andrews_,_Texas . It has an 8 km long runway and situated 973 meters above sea level .
coordinated_full_clauses Al-Taqaddum_Air_Base cityServed Fallujah At 84.0 metres above sea level and with a runway length of 3684.0 , Al-Taqaddum_Air_Base is located in Habbaniyah and serves the city of Fallujah .
relative_subject Adirondack_Regional_Airport cityServed Saranac_Lake_,_New_York Adirondack_Regional_Airport is 507 metres above sea level and serves Lake_Placid_,_New_York and Saranac_Lake_,_New_York . It is located in Harrietstown_,_New_York and has a runway length of 1219 .
relative_subject Andrews_County_Airport cityServed Andrews_,_Texas Andrews_County_Airport is located in Texas and serves the city of Andrews_,_Texas . It is 973 below sea level and has a runway length of 8 .
relative_subject Ashgabat_International_Airport operatingOrganisation Turkmenistan_Airlines Ashgabat_International_Airport in Ashgabat is operated by Turkmenistan_Airlines . It is 211 metres above sea level and has a runway length of 3800.0 .
possessif Athens_International_Airport cityServed Athens Located in Spata and serving the city of Athens , Athens_International_Airport is 94 metres above sea level and has a runway length of 4000 .
passive_voice Afonso_Pena_International_Airport runwayName "11/29" The location of Afonso_Pena_International_Airport is São_José_dos_Pinhais and it is located 911.0 metres above sea level . The runway name is "11/29" and it has a length of 1800 .
coordinated_full_clauses Alpena_County_Regional_Airport cityServed Alpena_,_Michigan Alpena_,_Michigan is served by Alpena_County_Regional_Airport which is located in Maple_Ridge_Township_,_Alpena_County_,_Michigan at 210 metres above sea level and with a runway length of 2744 .
coordinated_clauses Ícolo_e_Bengo isPartOf Luanda_Province Angola_International_Airport is located at Ícolo_e_Bengo , Luanda_Province , Angola . It has a runway length of 4000 ft .
passive_voice Alpena_County_Regional_Airport elevationAboveTheSeaLevel_(in_metres) 210 Alpena_County_Regional_Airport has a runway length of 1533.0 and is 210 metres above sea level . It is located in Maple_Ridge_Township_,_Alpena_County_,_Michigan , United_States .
apposition Ícolo_e_Bengo isPartOf Luanda_Province Angola_International_Airport is located in Ícolo_e_Bengo , Luanda_Province , Angola and has a runway length of 4000 .
possessif Ícolo_e_Bengo isPartOf Luanda_Province Angola_International_Airport is located in Ícolo_e_Bengo , Luanda_Province , Angola . It has a runway 3800 long .
possessif Texas country United_States Andrews_County_Airport is located in Texas , where the capital is Austin_,_Texas is part of the United_States and the inhabitants are called Texan .
direct_object Texas demonym Texan Andrews_County_Airport is located in Texas within the United_States , the people of Texas are referred to as Texan and the local language is English_language .
direct_object Texas capital Austin_,_Texas Andrews_County_Airport is located in Texas whose capital city is Austin_,_Texas . English_language is one of the spoken languages in Texas and the state ' s inhabitants are called Texan .
possessif Texas capital Austin_,_Texas Texas is the location of Andrews_County_Airport and has the capital city of Austin_,_Texas . The inhabitants of Texas are known as Texan and the Spanish_language is spoken .
coordinated_full_clauses Ícolo_e_Bengo isPartOf Luanda_Province Angola_International_Airport is located at Ícolo_e_Bengo , Luanda_Province , Angola and has the runway name of "05L/23R" .
existential Antwerp_International_Airport runwayLength 600 The Flemish_Government operate Antwerp_International_Airport which is owned by the Flemish_Region . The airport has an elevation of 12 metres above sea level and a runway length of 600 .
relative_subject Aarhus_Airport runwayName "10L/28R" Operated by "Aarhus_Lufthavn_A/S" , Aarhus_Airport is 25 metres above sea level . It has a runway length of 2776 and the runway name , "10L/28R" .
passive_voice Aarhus_Airport runwayName "10L/28R" Aktieselskab is the operating organisation for Aarhus_Airport which is located at 25 metres above sea level . It has a runway name of "10L/28R" and has a length of 2776 .
direct_object Atlantic_City_International_Airport runwayName "13/31" The Atlantic_City_International_Airport is operated by the Port_Authority_of_New_York_and_New_Jersey . It is 23 metres above sea level . The runway , "13/31" , is 1873 long .
coordinated_clauses Aarhus_Airport runwayName "10R/28L" Aarhus_Airport , which is 25.0 metres above sea level , is operated by "Aarhus_Lufthavn_A/S" . The runway name is "10R/28L" and has a length of 2776.0 .
direct_object Ardmore_Airport_(New_Zealand) runwayName "03R/21L" The runway at Ardmore_Airport_(New_Zealand) is called "03R/21L" . It is 1411.0 in length and 34.0 meters above sea level . The third runway there is made of Poaceae .
possessif Ardmore_Airport_(New_Zealand) runwayName "07/25" Ardmore_Airport_(New_Zealand) has a 3rd runway made of Poaceae and is 34 metres above sea level . The runway name is "07/25" and has a length of 518 .
relative_subject Poaceae order Poales The 3rd runway at Ardmore_Airport_(New_Zealand) is made of Poaceae . This plant ; belongs to the division of Flowering_plant , is of the Poales order and is in the Monocotyledon class .
relative_subject Poaceae division Flowering_plant Ardmore_Airport_(New_Zealand) ' s 3rd runway surface type is Poaceae . This plant is in the class Monocotyledon as well as belonging to the division of Flowering_plant and being part of the Poales order .
direct_object Belgium language German_language Charles_Michel is the leader of Belgium where the German_language is spoken . Antwerp is located in the country and served by Antwerp_International_Airport .
relative_adverb Iraq language Kurdish_languages Al-Taqaddum_Air_Base serves the city of Fallujah , which is in Iraq , where Fuad_Masum is the leader and the Kurdish_languages are spoken .
existential Belgium language German_language Antwerp_International_Airport services ( Charles_Michel ) led Antwerp , Belgium . German_language is spoken here ..
passive_voice Iraq language Arabic Al-Taqaddum_Air_Base serves the city of Fallujah in Iraq . Fuad_Masum leads that country where Arabic is the spoken language .
apposition Belgium language German_language Charles_Michel is the leader of Belgium where the German_language is spoken . Antwerp is located in the country and served by Antwerp_International_Airport .
relative_subject Belgium language French_language Charles_Michel is the leader of Belgium where the French_language is spoken . Antwerp in the country is served by Antwerp_International_Airport .
coordinated_clauses Alpena_County_Regional_Airport runwayLength 1533 Alpena_County_Regional_Airport is located in Maple_Ridge_Township_,_Alpena_County_,_Michigan and serves the city of Alpena_,_Michigan . It is located 210 metres above sea level and has a runway length of 1533 .
coordinated_clauses Angola_International_Airport runwayLength 4000 Angola_International_Airport is located in Ícolo_e_Bengo which serves Luanda . The runway length is 4000 and 159 meters above sea level .
coordinated_clauses Athens_International_Airport runwayLength 4000 Located in Spata and serving the city of Athens , Athens_International_Airport is 94 metres above sea level and has a runway length of 4000 .
possessif Al-Taqaddum_Air_Base elevationAboveTheSeaLevel_(in_metres) 84.0 Al-Taqaddum_Air_Base is located in Habbaniyah and serves the city of Fallujah . It is at an altitude of 84.0 metres above sea level and has a runway 3684.0 metres in length .
possessif Alpena_County_Regional_Airport elevationAboveTheSeaLevel_(in_metres) 210 Alpena_County_Regional_Airport is located in Maple_Ridge_Township_,_Alpena_County_,_Michigan and serves the city of Alpena_,_Michigan . It is located 210 metres above sea level and has a runway length of 1533 .
possessif Andrews_County_Airport elevationAboveTheSeaLevel_(in_metres) 973 Andrews_County_Airport located in Texas and serving the city of Andrews_,_Texas , has a runway length of 896 and is 973 metres above sea level .
apposition Allama_Iqbal_International_Airport operatingOrganisation Pakistan_Civil_Aviation_Authority The Pakistan_Civil_Aviation_Authority is the operating organisation of the Allama_Iqbal_International_Airport located in Pakistan and serving the city of Lahore . The airport has a runway length of 3360.12 .
apposition Andrews_County_Airport elevationAboveTheSeaLevel_(in_metres) 973 Andrews_County_Airport located in Texas and serving the city of Andrews_,_Texas , has a runway length of 896 and is 973 metres above sea level .
existential Allama_Iqbal_International_Airport operatingOrganisation Pakistan_Civil_Aviation_Authority Allama_Iqbal_International_Airport in Punjab_,_Pakistan , is operated by Pakistan_Civil_Aviation_Authority . The airport serves the city of Lahore and has a runway length of 2900.0 .
passive_voice Andrews_County_Airport elevationAboveTheSeaLevel_(in_metres) 973 Andrews_County_Airport is located in Texas and serves the city of Andrews_,_Texas . It is 973 below sea level and has a runway length of 8 .
relative_subject Poaceae division Flowering_plant The 2nd runway at Ardmore_Airport_(New_Zealand) is made of Poaceae . Poaceae is a Flowering_plant in the class of Monocotyledon in the order of Commelinids .
apposition Poaceae class Monocotyledon The 2nd runway at Ardmore_Airport_(New_Zealand) is made of Poaceae . Poaceae is a Flowering_plant in the class of Monocotyledon in the order of Commelinids .
existential Rhythm_and_blues derivative Funk Andra_(singer) , associated with Andreea_Bălan , plays Rhythm_and_blues music . That music type and Disco and Funk came from Blues music .
coordinated_clauses Aaron_Turner genre Black_metal Aaron_Turner played with Twilight_(band) Old_Man_Gloom bands . He started performing Black_metal in 1995 .
relative_subject Alison_O'Donnell genre Folk_music Alison_O'Donnell started performing in 1963 and plays Folk_music . She was with the Mellow_Candle and the Bajik bands .
possessif Aaron_Turner genre Ambient_music Aaron_Turner started performing in 1995 and performs Ambient_music . He played for Old_Man_Gloom and with Twilight_(band) .
coordinated_clauses Andrew_Rayel genre Trance_music Trance_music Andrew_Rayel , who began his career in 2009 , has worked with Jwaydan_Moyine and Jonathan_Mendelsohn .
passive_voice Aaron_Turner genre Black_metal Black_metal musician , Aaron_Turner started performing in 1995 . He played for Old_Man_Gloom and is a musician in Lotus_Eaters_(band) .
existential Alex_Day genre Electronic_music Chameleon_Circuit_(band) member and Electronic_music Alex_Day began in 2006 and is associated with artist Charlie_McDonnell .
existential Andrew_Rayel genre Trance_music Trance_music Andrew_Rayel is associated with artists Armin_van_Buuren and Christian_Burns , beginning his career in 2009 .
relative_subject Abradab origin Poland Abradab came from Katowice , Poland and started peforming in 1994 . He played with Kaliber_44 and is associated with Magik_(rapper) .
existential Aaron_Turner genre Black_metal Black_metal performer Aaron_Turner , who started his musical career in 1995 , has played with the bands Twilight_(band) Old_Man_Gloom .
existential Aaron_Turner genre Post-metal Aaron_Turner , a Post-metal musician , started performing in 1995 . He played for the Lotus_Eaters_(band) Old_Man_Gloom bands .
possessif Aaron_Turner genre Electroacoustic_music Aaron_Turner started performing Electroacoustic_music in 1995 . He played for Twilight_(band) Old_Man_Gloom .
relative_subject Al_Anderson_(NRBQ_band) birthPlace Windsor_,_Connecticut Al_Anderson_(NRBQ_band) of the NRBQ band was born in Windsor_,_Connecticut and started his career in 1966 . Previously he was a member of The_Wildweeds .
possessif Post-metal instrument Cello Aaron_Turner plays Post-metal music for Twilight_(band) and also played for Isis_(band) . Cello is a Post-metal instrument .
coordinated_clauses Trance_music stylisticOrigin Pop_music Trance_music performer Andrew_Rayel is associated with musical artists Jwaydan_Moyine and Bobina . Pop_music helped give rise to Trance .
existential Aaron_Turner activeYearsStartYear 1995 Post-metal musician Aaron_Turner , who began performing in 1995 , has played with the bands Twilight_(band) the Lotus_Eaters_(band) .
existential Aaron_Turner activeYearsStartYear 1995 Aaron_Turner started performing Electroacoustic_music in 1995 . He played for Twilight_(band) Old_Man_Gloom .
relative_subject Aaron_Turner activeYearsStartYear 1995 Aaron_Turner started performing in 1995 . He is an Electroacoustic_music and played in the Lotus_Eaters_(band) Old_Man_Gloom bands .
relative_subject Alison_O'Donnell activeYearsStartYear 1963 Alison_O'Donnell was a member of the Flibbertigibbet band and the Mellow_Candle . Her genre is the Folk_music_of_Ireland and started performing in 1963 .
relative_subject Post-metal instrument Cello Aaron_Turner performs Post-metal music with the Twilight_(band) . He also played with the Sumac_(band) . Cello is a post - metal instrument .
juxtaposition Alison_O'Donnell activeYearsStartYear 1963 Alison_O'Donnell was a member of the Flibbertigibbet band and is musically associated with the band Head_South_By_Weaving . 1963 was the beginning of Folk_music performer Alison_O'Donnell ' s active years .
passive_voice Alison_O'Donnell activeYearsStartYear 1963 Folk_music singer , Alison_O'Donnell started performing in 1963 and is an artist for the band Mellow_Candle after she was a member of the Flibbertigibbet band .
passive_voice Post-metal instrument Cello Post-metal ( which uses the Cello ) musician Aaron_Turner , formerly of Twilight_(band) , is with the Lotus_Eaters_(band) .
direct_object Aaron_Turner activeYearsStartYear 1995 Black_metal musician , Aaron_Turner started performing in 1995 . He played for Old_Man_Gloom and is a musician in Lotus_Eaters_(band) .
direct_object Alison_O'Donnell activeYearsStartYear 1963 Folk_music singer , Alison_O'Donnell started performing in 1963 and is an artist for the band Mellow_Candle after she was a member of the Flibbertigibbet band .
passive_voice Aaron_Turner activeYearsStartYear 1995 Aaron_Turner started performing in 1995 and played for Old_Man_Gloom . He is a musician in Lotus_Eaters_(band) and his genre is Black_metal .
coordinated_clauses Pop_music musicFusionGenre Disco The musical genre of Aleksandra_Kovač is Pop_music , in whihc one of its fusion is Disco music . He is part of the K2_(Kovač_sisters_duo) and is associated with the musician Kristina_Kovač .
existential Andrew_Rayel activeYearsStartYear 2009 Trance_music Andrew_Rayel began his career in 2009 and is associated with artists Jonathan_Mendelsohn and Christian_Burns .
passive_voice Aaron_Turner activeYearsStartYear 1995 In 1995 began to realize the musician of the genre Post-metal Aaron_Turner in the band of Lotus_Eaters_(band) , played in the Old_Man_Gloom .
passive_voice Aaron_Turner activeYearsStartYear 1995 Aaron_Turner started performing Electroacoustic_music in 1995 . He played for Twilight_(band) Old_Man_Gloom .
coordinated_full_clauses Alison_O'Donnell activeYearsStartYear 1963 Alison_O'Donnell ' s career began in 1963 and her genre is Folk_music_of_Ireland . She is is an artist for the band Mellow_Candle and was a member of the Flibbertigibbet band .
coordinated_clauses Aaron_Turner activeYearsStartYear 1995 Aaron_Turner , who began performing in 1995 , plays Electroacoustic_music and has been a member of the bands Twilight_(band) Old_Man_Gloom .
coordinated_clauses Post-metal instrument Cello Aaron_Turner played with Twilight_(band) and is now an artist for Isis_(band) . His genre is Post-metal , which uses the Cello .
coordinated_clauses Post-metal instrument Cello Aaron_Turner is a performer of the musical genre Post-metal which has the Cello as an instrument . He played for Old_Man_Gloom and for Twilight_(band) .
direct_object Trance_music stylisticOrigin Pop_music Trance_music ( origin : Pop_music ) Andrew_Rayel is associated with artists Bobina and Christian_Burns .
direct_object Aaron_Turner activeYearsStartYear 1995 Post-metal musician , Aaron_Turner , began performing in 1995 and has played with such bands as Twilight_(band) The Lotus_Eaters_(band) .
coordinated_clauses Al_Anderson_(NRBQ_band) instrument Guitar Al_Anderson_(NRBQ_band) , who once played with The_Wildweeds , plays Guitar in the Rock_music band NRBQ .
apposition Al_Anderson_(NRBQ_band) birthDate 1947-03-13 Al_Anderson_(NRBQ_band) plays with the band NRBQ and was a member of The_Wildweeds . His genre is Country_music and he was born on 1947-03-13 .
apposition Al_Anderson_(NRBQ_band) instrument Guitar Guitar Country_music performer Al_Anderson_(NRBQ_band) plays with the band NRBQ and once played with The_Wildweeds .
apposition Trance_music stylisticOrigin Pop_music Trance_music ( origin : Pop_music ) Andrew_Rayel is associated with artists Bobina and Christian_Burns .
apposition Post-metal instrument Cello Post-metal ( which involves the Cello ) artist Aaron_Turner is associated with Twilight_(band) Sumac_(band) .
relative_object Post-metal instrument Cello The Post-metal instrument is a Cello , which is Aaron_Turner ' s genre . He played with Twilight_(band) Greymachine bands .
coordinated_full_clauses Ska_punk stylisticOrigin Ska Aaron_Bertram plays for the Suburban_Legends band and is an artist with the band Kids_Imagine_Nation . He performs Ska_punk music which originated from Ska music .
coordinated_full_clauses Post-metal instrument Cello Aaron_Turner played Post-metal music with the Twilight_(band) . He also played with the House_of_Low_Culture . Cello is a Post-metal instrument .
relative_object Al_Anderson_(NRBQ_band) instrument Guitar Al_Anderson_(NRBQ_band) plays Guitar with the band NRBQ and played once with The_Wildweeds . His genre is Rock_music .
direct_object Al_Anderson_(NRBQ_band) birthDate 1947-03-13 Born 1947-03-13 , Country_music Al_Anderson_(NRBQ_band) , who was once a member of the The_Wildweeds , now plays with the band NRBQ .
existential Andrew_Rayel activeYearsStartYear 2009 Trance_music Andrew_Rayel , who began his career in 2009 , has worked with Jwaydan_Moyine and Jonathan_Mendelsohn .
passive_voice Post-metal instrument Cello Aaron_Turner is a performer of the musical genre Post-metal , which uses Cello as a musical instrument . He played with Twilight_(band) House_of_Low_Culture bands .
existential Aaron_Turner activeYearsStartYear 1995 Aaron_Turner , a Post-metal player , started performing in 1995 and he played for Lotus_Eaters_(band) and Old_Man_Gloom .
apposition Aaron_Turner activeYearsStartYear 1995 Aaron_Turner is Post-metal musician who started performing in 1995 . He has played with the bands Twilight_(band) Old_Man_Gloom .
relative_object Al_Anderson_(NRBQ_band) activeYearsStartYear 1966 Rock_music , NRBQ band , member , Al_Anderson_(NRBQ_band) started his career in 1966 and was a member of The_Wildweeds .
passive_voice Al_Anderson_(NRBQ_band) instrument Guitar Former The_Wildweeds band member , Al_Anderson_(NRBQ_band) , who plays the Guitar in the NRBQ band , performs Country_music .
apposition Post-metal instrument Cello Aaron_Turner is a performer of the musical genre Post-metal , which uses Cello as a musical instrument . He played with Twilight_(band) House_of_Low_Culture bands .
coordinated_clauses Al_Anderson_(NRBQ_band) birthDate 1947-03-13 Born 1947-03-13 , Country_music performer Al_Anderson_(NRBQ_band) once played with The_Wildweeds and is now a member of the band NRBQ .
direct_object Aaron_Turner activeYearsStartYear 1995 Aaron_Turner started performing in 1995 and is associated with the group Greymachine . He played with Twilight_(band) and his musical genre is Post-metal .
passive_voice Post-metal instrument Cello Aaron_Turner , who has played for the bands Twilight_(band) Old_Man_Gloom , performs the genre Post-metal , which sometimes uses the Cello .
direct_object Aaron_Deer background "solo_singer" Psychedelia "solo_singer" Aaron_Deer is from Indianapolis , Indiana .
relative_subject Aaron_Deer background "solo_singer" Aaron_Deer is from Indianapolis . in Indiana . He is a performer of the music genre , Indie_rock and has a background as a "solo_singer" .
passive_voice Aaron_Turner instrument Electric_guitar Death_metal is a musical fusion of Black_metal which is the music genre of Aaron_Turner . He performed for House_of_Low_Culture and plays the Electric_guitar .
apposition Aaron_Turner instrument Electric_guitar Aaron_Turner is a Black_metal musician and plays Electric_guitar for the Mamiffer . Death_metal is a musical fusion of Black_metal .
coordinated_clauses Alternative_rock stylisticOrigin Punk_rock Andrew_White_(musician) is an Alternative_rock musician with the Kaiser_Chiefs . Alternative_rock has origins in Punk_rock and New_wave_music and Nu_metal is a fusion genre of it .
relative_subject Alternative_rock stylisticOrigin Punk_rock Andrew_White_(musician) is an Alternative_rock musician with the Kaiser_Chiefs . Alternative_rock has origins in Punk_rock and New_wave_music and Nu_metal is a fusion genre of it .
existential Aaron_Turner instrument Electric_guitar Aaron_Turner plays Electric_guitar and performed Black_metal music for House_of_Low_Culture . A fusion of Black_metal music is called Death_metal .
possessif Aaron_Turner instrument Electric_guitar Electric_guitar player Aaron_Turner is a Black_metal musician who performed with Sumac_(band) . Death_metal is a fusion of Black_metal music .
existential Alternative_rock stylisticOrigin New_wave_music Alternative_rock performer Andrew_White_(musician) plays for the band Kaiser_Chiefs . Alternative_rock can be traced back to New_wave_music and later the genre helped give rise to Nu_metal .
apposition Aaron_Turner associatedBand/associatedMusicalArtist Sumac_(band) Black_metal Electric_guitar Aaron_Turner has performed with Sumac_(band) and Old_Man_Gloom . Death_metal is a fusion of Black_metal .
apposition Aaron_Turner associatedBand/associatedMusicalArtist Sumac_(band) Electric_guitar player Aaron_Turner is a Black_metal musician who performed with Sumac_(band) . Death_metal is a fusion of Black_metal music .
apposition Aaron_Turner associatedBand/associatedMusicalArtist Sumac_(band) Aaron_Turner is a Black_metal musician who plays the Electric_guitar in Sumac_(band) and Old_Man_Gloom . He plays the Electric_guitar in Black_metal music which can be found in a fusion as Death_metal .
existential Rock_music stylisticOrigin Blues Al_Anderson_(NRBQ_band) plays the Guitar in the NRBQ band which plays Rock_music . This genre originated from Blues music and is partly originating the source of the fusion genre , Bhangra_(music) .
existential Abradab birthPlace Poland Abradab was born in Katowice , Poland 1978-11-12 . He is associated with the Magik_(rapper) and played with the Kaliber_44 band .
direct_object Abradab associatedBand/associatedMusicalArtist Magik_(rapper) Abradab was born in Katowice , Poland 1978-11-12 . He is associated with the Magik_(rapper) and played with the Kaliber_44 band .
direct_object Alex_Day background "solo_singer" Synthpop is a form of Pop_music derived from House_music . It is performed by the "solo_singer" Alex_Day .
relative_object Andra_(singer) background "solo_singer" Beginning her career as a "solo_singer" , Andra_(singer) ' s musical genre is Rhythm_and_blues . Disco is a derivative of rhythm and Blues which originated from Blues music .
possessif Hip_hop_music musicSubgenre Gangsta_rap Abradab performs Hip_hop_music which originated from Funk music . Drum_and_bass is a derivative of Hip_hop_music which has a sub genre called Gangsta_rap .
coordinated_clauses Ace_Wilder background "solo_singer" The musical genre of "solo_singer" Ace_Wilder , is Hip_hop_music which has its stylistic origins in Funk . Drum_and_bass derives its sounds from Hip_hop_music .
existential Ace_Wilder background "solo_singer" "solo_singer" Ace_Wilder performs Hip_hop_music , which has its stylistic origins in Disco . Drum_and_bass has its origins in Hip_hop_music .
passive_voice Albennie_Jones activeYearsStartYear 1930 Rhythm_and_blues artist Albennie_Jones began in 1930 . Rhythm_and_blues originates from Blues and gives itself to Disco .
relative_object Albennie_Jones associatedBand/associatedMusicalArtist Sammy_Price Albennie_Jones has worked with the musical artist Sammy_Price and he performs Rhythm_and_blues . Disco is a derivative of Rhythm_and_blues , which is originated from Blues music .
relative_subject Andra_(singer) background "solo_singer" Andra_(singer) is a "solo_singer" rhythm and Blues singer . Rhythm_and_blues originated from Blues music and Funk was derived from the same music .
passive_voice Hip_hop_music musicSubgenre Gangsta_rap The musical genre of Abradab is Hip_hop_music which has its stylistic origins in Jazz . Drum_and_bass is a derivative of Hip_hop_music which has the sub genre Gangsta_rap .
passive_voice Andra_(singer) activeYearsStartYear 2000 The singer called Andra_(singer) , whose genre is Rhythm_and_blues , became active in the year 2000 . Disco is a derivative of rhythm and Blues which intern was originated from Blues music .
coordinated_clauses Ahmet_Ertegun origin Washington Ahmet_Ertegun is from Washington , United_States . He plays Rhythm_and_blues which originated from the Blues and which has Disco as a derivative .
direct_object Ahmet_Ertegun birthYear 1923 Rhythm_and_blues player Ahmet_Ertegun was born in 1923 . Disco derives from rhythm and Blues , which originated from Blues music .
apposition Ace_Wilder background "solo_singer" "solo_singer" Ace_Wilder performs Hip_hop_music , which has its stylistic origins in Disco . Drum_and_bass has its origins in Hip_hop_music .
coordinated_clauses Ahmet_Ertegun birthPlace Istanbul Ahmet_Ertegun , born in Istanbul , performs Rhythm_and_blues , which originates from Blues music and is the origin of Disco .
coordinated_clauses Albennie_Jones birthPlace United_States Disco is derived from Rhythm_and_blues which itself originated from the Blues music . Albennie_Jones , who was born in the United_States performs in the Rhythm_and_blues genre .
existential Hip_hop_music stylisticOrigin Jazz Abradab performs Hip_hop_music which originated from Jazz . Drum_and_bass derive their sounds from Hip_hop_music which also has a sub genre called Gangsta_rap .
apposition Aaron_Bertram associatedBand/associatedMusicalArtist Suburban_Legends Aaron_Bertram is a musician who plays for the Suburban_Legends and the Kids_Imagine_Nation bands . He plays Ska_punk which has its origins in Ska and Punk_rock music .
apposition Hip_hop_music derivative Drum_and_bass The musical genre of Allen_Forrest is Hip_hop_music which has its origins in Disco and its stylistic origins in Funk . Drum_and_bass is a derivative of Hip_hop_music .
possessif Aaron_Bertram associatedBand/associatedMusicalArtist Suburban_Legends Aaron_Bertram played Ska_punk music for the Suburban_Legends Band and the Kids_Imagine_Nation Band . Ska_punk comes from Ska which originates from Punk_rock .
relative_subject Synthpop derivative House_music Synthpop has its origins in Disco , House_music and Pop_music and is the style of artist Alex_Day .
relative_subject Aaron_Deer instrument Bass_guitar Aaron_Deer plays the Bass_guitar and performs Indie_rock music . Indie_rock originated from Garage_rock music and New_wave_music .
apposition Rock_music musicFusionGenre Bhangra_(music) Alan_Frew is a performer of Rock_music , which originated from Folk_music . Bhangra_(music) is part of the fusion genre , partly coming from Rock_music , along with the Blues .
coordinated_clauses Aaron_Bertram associatedBand/associatedMusicalArtist Suburban_Legends Aaron_Bertram is a musician who plays for the Suburban_Legends and the Kids_Imagine_Nation bands . He plays Ska_punk which has its origins in Ska and Punk_rock music .
coordinated_clauses Hip_hop_music derivative Drum_and_bass The musical genre of Allen_Forrest is Hip_hop_music which has its origins in Disco and its stylistic origins in Funk . Drum_and_bass is a derivative of Hip_hop_music .
relative_subject Hip_hop_music musicSubgenre musicSubHip_hop_music The musical genre of Ace_Wilder is Hip_hop_music and the style ' s sub - genre is the musicSubHip_hop_music . Hip_hop_music originated from Funk music and has its stylistic origins in Disco .
coordinated_clauses Synthpop derivative House_music Synthpop is influenced by Pop_music , New_wave_music and House_music , it is also the genre of Alex_Day .
direct_object Rock_music musicFusionGenre Bhangra_(music) Alan_Frew is a performer of Rock_music , which originated from Folk_music . Bhangra_(music) is part of the fusion genre , partly coming from Rock_music , along with the Blues .
passive_voice Hip_hop_music derivative Drum_and_bass Hip_hop_music originated from Jazz music and Funk music . Abradab is Hip_hop_music as well as Drum_and_bass .
relative_object Rock_music musicFusionGenre Bhangra_(music) Alan_Frew is a performer of Rock_music , which has its origins in Blues music and its stylistic origins in Folk_music . Bhangra_(music) is part of the fusion genre , with some of its ideas coming from Rock_music .
relative_object Synthpop derivative House_music Synthpop has its origins in Disco , House_music and Pop_music and is the style of artist Alex_Day .
relative_subject Andra_(singer) genre Dance-pop Andra_(singer) was born in Câmpia_Turzii and is a "singer_,_Reality_television_judge" and reality tv judge . She did Dance-pop and began as a "solo_singer" .
coordinated_clauses Alfredo_Zitarrosa genre Candombe "solo_singer" Alfredo_Zitarrosa was born in Uruguay . He records Candombe music for the record label Movieplay .
possessif Hip_hop_music derivative Drum_and_bass Abradab performs Hip_hop_music which originated from Funk music . Drum_and_bass is a derivative of Hip_hop_music which has a sub genre called Gangsta_rap .
direct_object Alfredo_Zitarrosa birthPlace Uruguay Uruguay born and Candombe style "solo_singer" Alfredo_Zitarrosa is signed with Odeon_Records .
possessif Alfredo_Zitarrosa birthPlace Montevideo Alfredo_Zitarrosa , born in Montevideo , is a "solo_singer" that plays Milonga_(music) for RCA_Records .
relative_subject Alfredo_Zitarrosa birthPlace Montevideo The birth place of Alfredo_Zitarrosa is Montevideo . He was a "solo_singer" Candombe music singer with Odeon_Records .
coordinated_clauses Alfredo_Zitarrosa birthPlace Uruguay "solo_singer" Alfredo_Zitarrosa was born in Uruguay . He plays Candombe music and records for Odeon_Records .
relative_subject Manchester isPartOf Greater_Manchester Alfred_Garth_Jones was born in Manchester , Greater_Manchester , England . The Labour_Party_(UK) is in the majority in Manchester .
coordinated_clauses Austria language Austrian_German Alfons_Gorbach was born in the County_of_Tyrol . He died in Austria , where Doris_Bures is a leader and Austrian_German is the language spoken .
coordinated_clauses Agustín_Barboza birthDate 1913-05-05 Agustín_Barboza was born in Asunción , Paraguay 1913-05-05 . He is signed to the record label of Philips_Records .
relative_adverb Austria language Austrian_German Alfons_Gorbach was born in Austria and he died in Graz . In Austria the language is Austrian_German and the leader is Doris_Bures .
coordinated_clauses Austria language Austrian_German Alfons_Gorbach was born in Austria whose language is Austrian_German and died in Graz . The leader of Austria is Doris_Bures .
relative_subject Rhythm_and_blues stylisticOrigin Blues Albennie_Jones was born in Errata_,_Mississippi and he plays Rhythm_and_blues . R & B is originates from Blues and Disco is its derivative .
existential Rhythm_and_blues stylisticOrigin Blues Albennie_Jones was born in Errata_,_Mississippi and plays Rhythm_and_blues music . That music genre originated from Blues music and Disco was derived from Rhythm_and_blues .
juxtaposition Rhythm_and_blues stylisticOrigin Blues Albennie_Jones has a background as a "solo_singer" in the Rhythm_and_blues genre . Disco is a derivative of Rhythm_and_blues which originated from Blues music .
passive_voice Hip_hop_music stylisticOrigin Funk "solo_singer" Allen_Forrest plays Hip_hop_music . The genre derived from Funk music and in turn helped spin - off Drum_and_bass .
relative_subject Hip_hop_music stylisticOrigin Disco Allen_Forrest was originally a "solo_singer" and performs Hip_hop_music . The stylistic origin of Hip_hop_music is Disco and Drum_and_bass derives its sounds from it .
apposition Synthpop stylisticOrigin New_wave_music Synthpop is derived from New_wave_music and is a form of House_music . Alex_Day , whose background is that of a "solo_singer" , uses the Synthpop genre .
possessif Rhythm_and_blues stylisticOrigin Blues Rhythm_and_blues singer Allen_Forrest is a "solo_singer" performer . Rhythm_and_blues came from the Blues and later gave rise to Funk .
existential Rhythm_and_blues stylisticOrigin Blues Disco derives from Rhythm_and_blues which itself originated from the Blues . Ahmet_Ertegun from the United_States performs the musical genre of Rhythm_and_blues .
apposition Ahmet_Ertegun deathPlace New_York_City Ahmet_Ertegun comes from Washington in the United_States . He died in New_York_City . One ethnic group in the United_States are African_Americans .
possessif Black_metal musicFusionGenre Death_metal Aaron_Turner , Electric_guitar , performs Black_metal music for Mamiffer . Death_metal is a musical fusion of Black_metal .
passive_voice Country_music instrument Banjo Banjo is an instrument for Country_music , the genre of the band , NRBQ . Al_Anderson_(NRBQ_band) from the NRBQ band was a member of The_Wildweeds .
relative_object Alan_Frew background "solo_singer" "solo_singer" Alan_Frew is a performer of Rock_music which originated from Country_music . Bhangra_(music) is part of the Rock_music fusion genre .
relative_subject Andrew_White_(musician) associatedBand/associatedMusicalArtist Kaiser_Chiefs The band Kaiser_Chiefs included the member Andrew_White_(musician) , who ' s genre is Alternative_rock . Nu_metal is a music fusion genre of Alternative_rock , who ' s stylistic origin is Punk_rock .
direct_object Andrew_White_(musician) associatedBand/associatedMusicalArtist Kaiser_Chiefs The musical genre of Kaiser_Chiefs band member , Andrew_White_(musician) , is Alternative_rock . Nu_metal is a music fusion genre of Alternative_rock which comes from New_wave_music .
coordinated_clauses Alternative_rock musicSubgenre musicSubAlternative_rock Andrew_White_(musician) ' s genre is Alternative_rock which has origins in New_wave_music . Nu_metal is a music fusion genre of Alternative_rock which has the sub genre musicSubAlternative_rock .
coordinated_full_clauses Rhythm_and_blues stylisticOrigin Blues Rhythm_and_blues artist Albennie_Jones began in 1930 . Rhythm_and_blues originates from Blues and gives itself to Disco .
direct_object Rhythm_and_blues stylisticOrigin Blues Albennie_Jones , a rhythm and Blues player , worked with Sammy_Price . Rhythm_and_blues and Disco both come from Blues music .
apposition Alfredo_Zitarrosa recordLabel Odeon_Records "solo_singer" Alfredo_Zitarrosa was born in Montevideo . He plays Milonga_(music) and records for Odeon_Records .
coordinated_clauses Alfredo_Zitarrosa recordLabel Movieplay "solo_singer" Alfredo_Zitarrosa was born in Uruguay . He records Candombe music for the record label Movieplay .
coordinated_full_clauses Alfredo_Zitarrosa recordLabel RCA_Records Montevideo born Alfredo_Zitarrosa , a "solo_singer" , is signed to RCA_Records and performs the genre Candombe .
passive_voice Albennie_Jones birthYear 1914 Born in the United_States in 1914 , Albennie_Jones was a "solo_singer" who played the Blues .
existential Alfredo_Zitarrosa recordLabel RCA_Records "solo_singer" Alfredo_Zitarrosa records for RCA_Records . He plays Candombe music and was born in Montevideo .
existential Aleksandra_Kovač birthYear 1972 Rhythm_and_blues "solo_singer" Aleksandra_Kovač was born in Belgrade in 1972 .
passive_voice Albennie_Jones birthYear 1914 Born in 1914 in Errata_,_Mississippi and with a background as a "solo_singer" , Albennie_Jones is a performer of Rhythm_and_blues .
relative_subject Andra_(singer) occupation "singer_,_Reality_television_judge" Andra_(singer) was born in Câmpia_Turzii and is a "singer_,_Reality_television_judge" and reality tv judge . She did Dance-pop and began as a "solo_singer" .
direct_object Albennie_Jones birthYear 1914 Born in 1914 in Errata_,_Mississippi and with a background as a "solo_singer" , Albennie_Jones is a performer of Rhythm_and_blues .
apposition Albennie_Jones birthYear 1914 Albennie_Jones , born in 1914 in Errata_,_Mississippi , is a Jazz player and "solo_singer" .
apposition Allen_Forrest birthYear 1981 Born in 1981 in Dothan_,_Alabama , the "solo_singer" Allen_Forrest is a performer of Hip_hop_music .
relative_subject Alfredo_Zitarrosa recordLabel Movieplay Alfredo_Zitarrosa was born in Uruguay and his musical genre is Candombe . His background includes "solo_singer" singing and the record label he uses is Movieplay .
direct_object Andra_(singer) occupation "singer_,_Reality_television_judge" Rhythm_and_blues "solo_singer" "singer_,_Reality_television_judge" Andra_(singer) is from Romania and is a Reality tv judge .
relative_subject Andra_(singer) occupation "singer_,_Reality_television_judge" Born in Câmpia_Turzii , Andra_(singer) began her career as a "solo_singer" . A performer of Rhythm_and_blues music , she is a "singer_,_Reality_television_judge" .
direct_object Alfredo_Zitarrosa recordLabel Odeon_Records Uruguay born and Candombe style "solo_singer" Alfredo_Zitarrosa is signed with Odeon_Records .
direct_object Alfred_Garth_Jones deathPlace Sidcup Alfred_Garth_Jones was born in Manchester , England in 1872 and died in London in a place called Sidcup .
coordinated_clauses Austria leaderName Doris_Bures Alfons_Gorbach was born in Imst , Austria-Hungary and died in Austria which is led by Doris_Bures .
relative_subject Manchester leaderName Labour_Party_(UK) The birthplace of Alfred_Garth_Jones is Manchester , England , where the Labour_Party_(UK) is in the majority . He died in London .
passive_voice Alan_Shepard dateOfRetirement "1974-08-01" Alan_Shepard ( born in New_Hampshire of the United_States ) retired on "1974-08-01" and died in California .
apposition Alan_Shepard occupation Test_pilot Alan_Shepard was a national of the United_States . He was born in New_Hampshire became a Test_pilot and died in California .
existential London leaderTitle European_Parliament Alfred_Garth_Jones died in Boris_Johnson and European_Parliament led Sidcup , London .
coordinated_full_clauses Deram_Records location London Alison_O'Donnell performs Jazz music and recorded under Osmosys_Records . She also recorded with Deram_Records which is based in London .
existential Deram_Records location London Alison_O'Donnell was once signed to London based , Deram_Records . A Jazz musician , Alison_O'Donnell is signed with the label , Floating_World_Records .
direct_object Alligator_Records location Chicago Blues artist Anders_Osborne has been signed to the record label Rabadash_Records and also to Chicago based Alligator_Records .
coordinated_clauses Alligator_Records location Chicago Anders_Osborne , a Blues artist , is with Alligator_Records which is based in Chicago . He was also signed to Shanachie_Records .
coordinated_clauses Deram_Records location London Jazz musician , Alison_O'Donnell is signed with the recording labels Stanyan_Records and Deram_Records , in London .
existential Deram_Records location London Folk_rock musician Alison_O'Donnell is signed with the record label Deram_Records , which is based in London . She is also signed to the record label Static_Caravan_Recordings .
coordinated_full_clauses Deram_Records location London Alison_O'Donnell , who performs Folk_music_of_Ireland , has been signed the record labels Fruits_de_Mer_Records and London based Deram_Records .
direct_object Deram_Records location London Alison_O'Donnell was once signed to London based , Deram_Records . A Jazz musician , Alison_O'Donnell is signed with the label , Floating_World_Records .
passive_voice Alligator_Records location Chicago Anders_Osborne ' s record label is Alligator_Records , which is located in Chicago . He plays Rock_music and recorded with Okeh_Records .
possessif Alligator_Records location Chicago Chicago based Rhythm_and_blues artist , Anders_Osborne , was signed to the Shanachie_Records label and is now signed to the record label , Alligator_Records .
relative_subject Deram_Records location London Jazz musician Alison_O'Donnell is with Stanyan_Records and formerly with London bases , Deram_Records .
relative_subject Alligator_Records location Chicago Anders_Osborne performs Rhythm_and_blues music . He was with Alligator_Records from Chicago . He was also with Shanachie_Records label .
possessif Alligator_Records location Chicago Anders_Osborne is a Blues artist who signed to Alligator_Records which is located in Chicago . He also signed to Rabadash_Records music label .
passive_voice Deram_Records location London Psychedelic_folk performer Alison_O'Donnell records for London - based Deram_Records and Stanyan_Records .
coordinated_full_clauses Anders_Osborne genre Blues Blues performer Anders_Osborne is associated with musical artists Billy_Iuso and Tab_Benoit as well as the band , Voice_of_the_Wetlands_All-Stars .
coordinated_clauses Anders_Osborne genre Blues Blues artist , Anders_Osborne is associated with Tab_Benoit , as well as the musical artist Billy_Iuso and with Voice_of_the_Wetlands_All-Stars .
coordinated_clauses Anders_Osborne genre Rock_music Anders_Osborne is associated with the musical artists Billy_Iuso and Tab_Benoit . He is an exponent of Rock_music and worked with the Galactic band .
apposition Alligator_Records location Chicago Anders_Osborne was signed to the Shanachie_Records label as well as Chicago - based Blues label Alligator_Records .
coordinated_clauses Anders_Osborne genre Blues Anders_Osborne is signed to the record label Alligator_Records which is located in Chicago . The record label of Blues artist , Anders_Osborne , is Shanachie_Records .
direct_object Alison_O'Donnell genre Psychedelic_folk Alison_O'Donnell is a Psychedelic_folk musician and recorded on the record label , Static_Caravan_Recordings . She records with Deram_Records , which is based in London .
direct_object Anders_Osborne genre Blues Anders_Osborne is signed to the record label Alligator_Records which is located in Chicago . The record label of Blues artist , Anders_Osborne , is Shanachie_Records .
existential Anders_Osborne genre Blues Blues artist Anders_Osborne has been signed to the record label Rabadash_Records and also to Chicago based Alligator_Records .
possessif Deram_Records genre Pop_music Alison_O'Donnell records with London based , Deram_Records ( Pop_music label ) and is signed with Static_Caravan_Recordings .
relative_object Uruguay demonym Uruguayans Alfredo_Zitarrosa died in Montevideo in Uruguay . The leader of Uruguay is Raúl_Fernando_Sendic_Rodríguez and the people that live there are called Uruguayans .
passive_voice Addis_Ababa_City_Hall location Addis_Ababa The Addis_Ababa_Stadium and Addis_Ababa_City_Hall are located in the city of Addis_Ababa , Ethiopia , where the language spoken is Amharic .
passive_voice United_States ethnicGroup African_Americans Albany_,_Georgia is located in the United_States state of Georgia_(U.S._state) where English_language is the spoken language . African_Americans are an ethnic group in the United_States .
existential Aarhus_Airport location Tirstrup The Greenlandic_language is spoken in Denmark where Aarhus_Airport is located in Tirstrup , part of the Central_Denmark_Region .
possessif United_States largestCity New_York_City Atlantic_City_,_New_Jersey , United_States . The capital of New_Jersey is Trenton_,_New_Jersey . The largest city in United_States is New_York_City .
possessif United_States ethnicGroup African_Americans Albany_,_Oregon is a city in Oregon , where the capital is Salem_,_Oregon . Albany_,_Oregon is located in the United_States , where there is an ethnic group called African_Americans .
relative_object Texas largestCity Houston Austin and Arlington ( capital ) are both part of Texas and the United_States , as is its largest city , Houston .
relative_object United_States ethnicGroup Native_Americans Albany_,_Oregon is part of Linn_County_,_Oregon , United_States . The capital of the United_States is Washington and Native_Americans are among the ethnic groups of the country .
apposition United_States ethnicGroup Native_Americans Native_Americans are one of the ethnic groups in the United_States . The capital of which , is Washington . Also in the United_States , is Akron_,_Ohio , which is located within Summit_County_,_Ohio .
relative_object United_States ethnicGroup African_Americans The city of Albany_,_Oregon is part of Benton_County_,_Oregon , in the United_States . The capital of the country is , Washington and African_Americans , are one of the country ' s ethnic groups .
apposition United_States ethnicGroup Native_Americans The Native_Americans are one of the ethnic groups in the United_States which has the capital city of Washington . The country is the location for the city of Albany_,_Oregon which is part of Benton_County_,_Oregon .
juxtaposition United_States ethnicGroup Native_Americans Native_Americans are one of the ethnic groups in the United_States . The capital of which , is Washington . Also in the United_States , is Akron_,_Ohio , which is located within Summit_County_,_Ohio .
direct_object United_States ethnicGroup Native_Americans Albany_,_Oregon , Benton_County_,_Oregon is located within the United_States . The capital of the United_States is Washington and some Native_Americans live there .
relative_subject United_States ethnicGroup Asian_Americans The city of Albany_,_Oregon is part of Benton_County_,_Oregon , in the United_States , where Washington is the capital and where Asian_Americans are one of the ethnic groups .
passive_voice United_States ethnicGroup African_Americans Akron_,_Ohio is in Summit_County_,_Ohio , United_States . The capital of the United_States is Washington and African_Americans are one of the ethnic groups .
passive_voice United_States demonym Americans Americans live in the United_States , where the capital is Washington . Also in the United_States is Akron_,_Ohio , in Summit_County_,_Ohio .
apposition United_States ethnicGroup Native_Americans Albany_,_Oregon is part of Oregon , in the United_States , where the capital is Washington and where Native_Americans are an ethnic group .
coordinated_clauses United_States ethnicGroup African_Americans Amarillo_,_Texas is part of Potter_County_,_Texas , in the United_States . Washington is the capital of the United_States , where African_Americans are an ethnic group .
coordinated_clauses Aarhus_Airport location Tirstrup Copenhagen is the capital of Denmark where Aarhus_Airport is located in Tirstrup which is part of the Central_Denmark_Region .
relative_subject United_States ethnicGroup African_Americans The United_States has the capital city of Washington and includes the ethnic group of African_Americans among its population . The country is the location of Akron_,_Ohio in Summit_County_,_Ohio .
possessif United_States language English_language Abilene_,_Texas is part of Texas within the United_States . Washington is the capital of the country and English_language is the official language .
relative_object Texas language English_language Abilene_,_Texas is part of Texas in the United_States where English_language is the spoken language . Washington is the capital of the United_States .
relative_object United_States language English_language Abilene_,_Texas is part of Texas in the United_States . English_language is spoken in the United_States and the capital city is Washington .
direct_object United_States language English_language Abilene_,_Texas is part of Taylor_County_,_Texas , United_States . The capital of the United_States is Washington and English_language is its official language .
relative_adverb United_States language English_language Abilene_,_Texas is part of Texas within the United_States . Washington is the capital of the country and English_language is the official language .
coordinated_full_clauses United_States language English_language Abilene_,_Texas is part of Taylor_County_,_Texas , United_States . The capital of the United_States is Washington and English_language is its official language .
passive_voice United_States ethnicGroup Native_Americans Native_Americans are an ethnic group in the United_States , the capital of which is Washington . The United_States is home to Amarillo_,_Texas , part of Potter_County_,_Texas .
apposition Texas language Spanish_language Abilene_,_Texas is in Texas in the United_States and Spanish_language is a language spoken there . The capital of the United_States is Washington .
relative_subject Texas language Spanish_language Abilene_,_Texas is in Texas in the United_States and Spanish_language is a language spoken there . The capital of the United_States is Washington .
apposition United_States demonym Americans Albany_,_Georgia is part of the state of Georgia_(U.S._state) in the United_States . People living in the United_States are Americans and an ethnic group there are Native_Americans .
apposition Oregon capital Salem_,_Oregon Albany_,_Oregon is in the United_States , as is Salem_,_Oregon , the capital of Oregon . Some Native_Americans live in the United_States .
apposition Akron_,_Ohio leaderTitle Mayor Akron_,_Ohio , led by the Mayor , is in Summit_County_,_Ohio , in the United_States , where Asian_Americans are one of the ethnic groups .
apposition United_States capital Washington The city of Albany_,_Oregon is part of Benton_County_,_Oregon , in the United_States , where Washington is the capital and where Asian_Americans are one of the ethnic groups .
apposition United_States language English_language Akron_,_Ohio in Summit_County_,_Ohio , is part of the United_States . where English_language is spoken and is home to Native_Americans .
passive_voice United_States capital Washington One of the ethnic groups in the United_States , where the capital city is Washington , are the African_Americans . Alpharetta_,_Georgia in the state of Georgia_(U.S._state) is located in the country .
relative_adverb United_States demonym Americans Albany_,_Georgia is part of Dougherty_County_,_Georgia , United_States . The inhabitants of the United_States are known as Americans and some of them are Asian_Americans .
passive_voice United_States demonym Americans Akron_,_Ohio in Summit_County_,_Ohio is part of the United_States where Asian_Americans are one of several ethnic groups among all Americans .
relative_adverb United_States language English_language Albany_,_Georgia is part of Dougherty_County_,_Georgia ; United_States , home to Asian_Americans and English_language speaking .
relative_adverb Oregon capital Salem_,_Oregon Albany_,_Oregon is in the United_States , as is Salem_,_Oregon , the capital of Oregon . Some Native_Americans live in the United_States .
relative_adverb United_States language English_language Angola_,_Indiana , a city in Indiana , is in the United_States . The same English_language speaking country , where African_Americans are one of the ethnic groups .
coordinated_full_clauses United_States capital Washington The Asian_Americans are an ethnic group in the United_States , which has the capital city of Washington . It is also the location of Albany_,_Oregon , part of Benton_County_,_Oregon .
passive_voice United_States language English_language Albany_,_Georgia is part of Dougherty_County_,_Georgia , United_States . English_language is the language of the United_States and The Native_Americans one of the ethnic groups .
passive_voice Oregon capital Salem_,_Oregon Albany_,_Oregon is in the United_States , as is Salem_,_Oregon , the capital of Oregon . Some Native_Americans live in the United_States .
passive_voice United_States capital Washington Akron_,_Ohio is in Summit_County_,_Ohio , United_States . The capital of the United_States is Washington and African_Americans are one of the ethnic groups .
passive_voice Akron_,_Ohio leaderTitle Mayor The Asian_Americans are an ethnic group within the United_States which is the location of Akron_,_Ohio . It is located in Summit_County_,_Ohio and led by the Mayor .
relative_subject United_States capital Washington Alpharetta_,_Georgia , Georgia_(U.S._state) is in the United_States where Washington is the capital and African_Americans are one of several ethnic groups .
apposition United_States capital Washington African_Americans are an ethnic group in the United_States , where the capital is Washington . The United_States is home to Amarillo_,_Texas , which is part of Potter_County_,_Texas .
relative_subject United_States capital Washington Akron_,_Ohio is in Summit_County_,_Ohio , United_States . The capital of the United_States is Washington and African_Americans are one of the ethnic groups .
relative_subject United_States capital Washington The city of Albany_,_Oregon , United_States is part of Benton_County_,_Oregon . One of the ethnic groups in the United_States are the African_Americans and the capital is Washington .
relative_subject United_States demonym Americans Albany_,_Georgia is a city in Dougherty_County_,_Georgia , United_States . Americans and Native_Americans are an ethnic group within the United_States .
apposition United_States capital Washington The Asian_Americans are an ethnic group in the United_States which has the capital of Washington . The country is the location of Akron_,_Ohio which is in Summit_County_,_Ohio ,.
relative_subject United_States language English_language Angola_,_Indiana is part of Steuben_County_,_Indiana , United_States . English_language is the language spoken in the United_States and some Asian_Americans live there .
existential United_States capital Washington United_States has its capital as Washington , home to African_Americans and Albany_,_Oregon , Benton_County_,_Oregon .
coordinated_full_clauses United_States demonym Americans Albany_,_Georgia is located in the United_States state of Georgia_(U.S._state) . Americans are the people occupying the United_States , where one of the ethnic groups is the African_Americans .
coordinated_full_clauses United_States capital Washington The Asian_Americans are an ethnic group of the United_States which has the capital city of Washington . Akron_,_Ohio is located within the country in Summit_County_,_Ohio .
relative_object United_States capital Washington Albany_,_Oregon is part of Linn_County_,_Oregon , United_States . The capital of the United_States is Washington and Native_Americans are among the ethnic groups of the country .
relative_object United_States capital Washington Washington is the capital of the United_States , which is home to Albany_,_Oregon , a city in Oregon . Within the United_States there is an ethic group called African_Americans .
relative_object United_States language English_language Angola_,_Indiana , part of the state of Indiana , is in the United_States . English_language is the language spoken in the United_States and African_Americans are one of the ethic groups .
relative_object United_States language English_language Angola_,_Indiana is part of Steuben_County_,_Indiana , United_States . English_language is the language spoken in the United_States and some Asian_Americans live there .
relative_object United_States capital Washington The Asian_Americans are an ethnic group of the United_States which has the capital city of Washington . Akron_,_Ohio is located within the country in Summit_County_,_Ohio .
relative_subject United_States demonym Americans Albany_,_Georgia is part of the state of Georgia_(U.S._state) in the United_States . Americans live in the United_States which also has an ethnic group called Asian_Americans .
relative_subject United_States capital Washington African_Americans are an ethnic group in the United_States , where Washington is the capital . Located in the United_States , is Akron_,_Ohio , in Summit_County_,_Ohio .
relative_subject United_States capital Washington Albany_,_Oregon is part of Oregon in the United_States . The United_States is home to an ethnic group called Asian_Americans and its capital city is Washington .
relative_subject United_States language English_language English_language is the language of the United_States , where African_Americans are one of the ethnic groups . Also in the United_States , is Akron_,_Ohio , in Summit_County_,_Ohio .
apposition United_States capital Washington Native_Americans are one of the ethnic groups found in the United_States where the capital is Washington . Also found in the United_States is the city of Albany_,_Oregon which is part of Benton_County_,_Oregon .
relative_adverb United_States capital Washington The city of Albany_,_Oregon , United_States is part of Benton_County_,_Oregon . One of the ethnic groups in the United_States are the African_Americans and the capital is Washington .
relative_adverb United_States language English_language Angola_,_Indiana is part of Steuben_County_,_Indiana , United_States . English_language is the language spoken in the United_States and some Asian_Americans live there .
relative_adverb United_States capital Washington The African_Americans are an ethnic group found in the United_States which has the capital of Washington . The country is the location of Alpharetta_,_Georgia , part of Fulton_County_,_Georgia .
direct_object United_States demonym Americans Akron_,_Ohio is located in Summit_County_,_Ohio in the United_States . Americans live in the United_States and Native_Americans are one ethnic group there .
direct_object Akron_,_Ohio leaderTitle Mayor Akron_,_Ohio , led by the Mayor , is in Summit_County_,_Ohio , in the United_States , where Asian_Americans are one of the ethnic groups .
direct_object United_States capital Washington Albany_,_Oregon is part of Oregon in the United_States . The United_States is home to an ethnic group called Asian_Americans and its capital city is Washington .
juxtaposition United_States language English_language Angola_,_Indiana , Pleasant_Township_,_Steuben_County_,_Indiana is part of The United_States , home to Native_Americans and includes the English_language .
relative_adverb United_States capital Washington The Asian_Americans are an ethnic group of the United_States which has the capital city of Washington . Akron_,_Ohio is located within the country in Summit_County_,_Ohio .
direct_object United_States language English_language English_language is a language used in the United_States , where Native_Americans are an ethnic group . The United_States is home to Angola_,_Indiana , which is part of Pleasant_Township_,_Steuben_County_,_Indiana .
relative_adverb United_States language English_language Angola_,_Indiana , in Steuben_County_,_Indiana , is part of the United_States which is also home to Native_Americans and the people speak English_language .
relative_object United_States language English_language Angola_,_Indiana is in Pleasant_Township_,_Steuben_County_,_Indiana , in the United_States , which is where English_language is spoken and African_Americans are one of the ethnic groups .
relative_object Akron_,_Ohio leaderTitle Mayor The Asian_Americans are an ethnic group within the United_States which is the location of Akron_,_Ohio . It is located in Summit_County_,_Ohio and led by the Mayor .
coordinated_full_clauses Albany_,_Oregon areaCode 541,_458 Albany_,_Oregon has a population density of 1104.1_inhabitants_per_square_kilometre and is located 64.008 above sea level . It uses the area codes of 541,_458 and has a total area of 45.97_square_kilometres .
relative_subject Amarillo_,_Texas isPartOf Randall_County_,_Texas At 1099.0 metres above sea level , Amarillo_,_Texas is part of Randall_County_,_Texas . Amarillo_,_Texas has a total area of 233.9_square_kilometres and a population density of 746.0_inhabitants_per_square_kilometre .
relative_subject Anderson_,_Indiana isPartOf Madison_County_,_Indiana Anderson_,_Indiana is located in Madison_County_,_Indiana and lies 268.0 above sea level . Covering an area of 107.43_square_kilometres it has a population density of 523.9_inhabitants_per_square_kilometre .
relative_adverb United_States demonym Americans Albany_,_Georgia , is in the United_States , where the occupying people are Americans and Native_Americans are one of the ethnic groups .
existential Asser_Levy_Public_Baths location New_York_City Asser_Levy_Public_Baths are located in New_York_City , Manhattan , New_Netherland in the United_States .
apposition DeKalb_County_,_Georgia largestCity Dunwoody_,_Georgia Most of Atlanta , which is in "Georgia" in the United_States , is part of DeKalb_County_,_Georgia where Dunwoody_,_Georgia is the largest city .
apposition Abilene_Regional_Airport cityServed Abilene_,_Texas Abilene_,_Texas is served by the Abilene_Regional_Airport . Abilene_,_Texas resides in the counties of Taylor_County_,_Texas and Jones_County_,_Texas within the United_States .
relative_subject Adirondack_Regional_Airport cityServed Saranac_Lake_,_New_York Adirondack_Regional_Airport serves the cities of Lake_Placid_,_New_York and Saranac_Lake_,_New_York , United_States . Saranac_Lake_,_New_York is part of Harrietstown_,_New_York , Essex_County_,_New_York .
relative_subject Asser_Levy_Public_Baths location New_York_City The Asser_Levy_Public_Baths are located in New_York_City , New_York , United_States . Manhattan is one part of New_York_City .
apposition Angola_,_Indiana areaTotal 16.55_square_kilometres Angola_,_Indiana , is part of Pleasant_Township_,_Steuben_County_,_Indiana , which is in Steuben_County_,_Indiana . The town is part of the United_States and its total area is 16.55_square_kilometres .
passive_voice Albany_,_Georgia areaTotal 144.7_square_kilometres With a total area of 144.7_square_kilometres , Albany_,_Georgia is a city in Dougherty_County_,_Georgia , Georgia_(U.S._state) , in the United_States .
passive_voice Randall_County_,_Texas countySeat Canyon_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas and Randall_County_,_Texas , United_States . Canyon_,_Texas is the county seat of Randall_County_,_Texas .
passive_voice Lee_County_,_Alabama countySeat Opelika_,_Alabama Auburn_,_Alabama , Lee County , Alabama is located in the United_States . Opelika_,_Alabama is the county seat of Lee_County_,_Alabama .
passive_voice Abilene_Regional_Airport cityServed Abilene_,_Texas Abilene_Regional_Airport serves the city of Abilene_,_Texas , part of Taylor_County_,_Texas and Jones_County_,_Texas , United_States .
direct_object Fulton_County_,_Georgia countySeat Atlanta Alpharetta_,_Georgia , Fulton_County_,_Georgia , Georgia_(U.S._state) is located within the United_States . Atlanta is the county seat of Fulton_County_,_Georgia .
passive_voice Atlantic_City_,_New_Jersey areaCode 609 Atlantic_City_,_New_Jersey , has a population density of 1421.2_inhabitants_per_square_kilometre and a total area of 44.125_square_kilometres . It has an area code of 609 and the Clerk_(municipal_official) leads this city .
relative_subject Attica_,_Indiana country United_States Attica_,_Indiana is a city in Logan_Township_,_Fountain_County_,_Indiana , United_States . Indianapolis is the capital of Indiana .
relative_subject Indiana country United_States Anderson_,_Indiana , Anderson_Township_,_Madison_County_,_Indiana and Indianapolis ( capital ) are part of Indiana in the United_States .
relative_object Washtenaw_County_,_Michigan country United_States Ann_Arbor_,_Michigan is part of Washtenaw_County_,_Michigan , United_States . Michigan ' s capital is Lansing_,_Michigan .
apposition Michigan country United_States Ann_Arbor_,_Michigan is part of Washtenaw_County_,_Michigan in the state of Michigan . Located in the United_States , the state has the capital of Lansing_,_Michigan .
apposition Fountain_County_,_Indiana country United_States Attica_,_Indiana , in Fountain_County_,_Indiana , is located in the United_States in the state of Indiana . The state capital is Indianapolis .
apposition Indiana country United_States Anderson_,_Indiana is part of the Anderson_Township_,_Madison_County_,_Indiana within the United_States . Indianapolis is the capital of Indiana .
coordinated_clauses Indiana capital Indianapolis Alexandria_,_Indiana , Monroe_Township_,_Madison_County_,_Indiana and Indianapolis , Indiana are in the United_States .
passive_voice Fulton_County_,_Georgia countySeat Atlanta Alpharetta_,_Georgia , Fulton_County_,_Georgia , Atlanta , Georgia_(U.S._state) are in the United_States .
apposition Michigan capital Lansing_,_Michigan Ann_Arbor_,_Michigan is part of Washtenaw_County_,_Michigan in the United_States . Michigan ' s capital is Lansing_,_Michigan .
apposition Indiana capital Indianapolis Attica_,_Indiana is part of Fountain_County_,_Indiana in the United_States . The state capital of Indiana is Indianapolis .
relative_subject Michigan capital Lansing_,_Michigan Ann_Arbor_,_Michigan is part of Washtenaw_County_,_Michigan , United_States . The capital of Michigan is Lansing_,_Michigan .
relative_subject Fulton_County_,_Georgia countySeat Atlanta Alpharetta_,_Georgia is part of Fulton_County_,_Georgia , Georgia_(U.S._state) , in the United_States . And , Atlanta is the county seat of Fulton_County_,_Georgia , Georgia_(U.S._state) .
relative_subject Indiana capital Indianapolis Indianapolis is the capital of Indiana , United_States . Anderson_,_Indiana is part of Lafayette_Township_,_Madison_County_,_Indiana .
relative_subject Michigan capital Lansing_,_Michigan Ann_Arbor_,_Michigan is part of Washtenaw_County_,_Michigan in the United_States . The capital of Michigan is Lansing_,_Michigan .
relative_subject Texas largestCity Houston Arlington_,_Texas is part of Tarrant_County_,_Texas in the United_States . Houston is the biggest city in Texas .
relative_subject Indiana capital Indianapolis Anderson_,_Indiana is part of the Anderson_Township_,_Madison_County_,_Indiana within the United_States . Indianapolis is the capital of Indiana .
direct_object United_States leaderName Joe_Biden Bacon_Explosion comes from the United_States which is lead by Joe_Biden . The capital city is Washington and one of the ethnic groups in the country are the Native_Americans .
direct_object Amarillo_,_Texas isPartOf Randall_County_,_Texas The United_States is home to the capital Washington and the ethnic group of African_Americans . Amarillo_,_Texas which is part of Randall_County_,_Texas , is found in the United_States .
direct_object Akron_,_Ohio isPartOf Summit_County_,_Ohio Native_Americans are one of the ethnic groups in the United_States . The capital of which , is Washington . Also in the United_States , is Akron_,_Ohio , which is located within Summit_County_,_Ohio .
direct_object United_States leaderName Barack_Obama The leader of the United_States was Barack_Obama . The capital of the United_States is Washington . 1634:_The_Ram_Rebellion comes from the United_States and one of it ' s ethnic groups is Asian_Americans .
direct_object United_States leaderName Barack_Obama The country Bacon_Explosion comes from is the United_States where Barack_Obama was once the leader . Native_Americans are one of the ethnic groups of the country and the capital is Washington .
direct_object Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas in the United_States . Washington is the United_States capital and African_Americans are an ethnic group in that country .
direct_object Albany_,_Oregon isPartOf Linn_County_,_Oregon Albany_,_Oregon is part of Linn_County_,_Oregon , United_States . The Native_Americans are an ethnic group within the United_States , where Washington is the capital .
direct_object Albany_,_Oregon isPartOf Benton_County_,_Oregon Albany_,_Oregon , Benton_County_,_Oregon is located within the United_States . The capital of the United_States is Washington and some Native_Americans live there .
direct_object Albany_,_Oregon isPartOf Oregon Albany_,_Oregon is part of Oregon in the United_States . The United_States is home to an ethnic group called Asian_Americans and its capital city is Washington .
coordinated_full_clauses United_States leaderName John_Roberts Bacon_Explosion comes from the United_States where John_Roberts is a leader . The country ' s capital city is Washington and Asian_Americans are one of the ethnic groups of the United_States .
coordinated_full_clauses United_States leaderName Paul_Ryan Paul_Ryan is a leader in the United_States , where Washington is the capital . Native_Americans are one of the ethnic groups . Bacon_Explosion also comes from the United_States .
coordinated_full_clauses Alpharetta_,_Georgia isPartOf Georgia_(U.S._state) Alpharetta_,_Georgia , Georgia_(U.S._state) is in the United_States where Washington is the capital and African_Americans are one of several ethnic groups .
coordinated_full_clauses Alpharetta_,_Georgia isPartOf Fulton_County_,_Georgia One of the ethnic groups found in the United_States , where the capital city is Washington , are the Native_Americans . The country is the location of Alpharetta_,_Georgia , part of Fulton_County_,_Georgia .
coordinated_full_clauses Albany_,_Oregon isPartOf Benton_County_,_Oregon In the United_States , where the capital is Washington , you ' ll find Albany_,_Oregon in Benton_County_,_Oregon and Asian_Americans .
apposition Alcatraz_Versus_the_Evil_Librarians language English_language The book Alcatraz_Versus_the_Evil_Librarians is written in English_language , in the United_States . The main language there is English_language , the capital is Washington and Asian_Americans are one ethnic group .
apposition United_States leaderName Joe_Biden The United_States is the country of the Bacon_Explosion . Joe_Biden is one of the leaders there , Washington is the capital and Asian_Americans are one ethnic group of the country .
apposition Akron_,_Ohio isPartOf Summit_County_,_Ohio African_Americans are an ethnic group in the United_States , where Washington is the capital . Located in the United_States , is Akron_,_Ohio , in Summit_County_,_Ohio .
apposition Albany_,_Oregon isPartOf Benton_County_,_Oregon The city of Albany_,_Oregon is part of Benton_County_,_Oregon , in the United_States , where Washington is the capital and where Asian_Americans are one of the ethnic groups .
passive_voice United_States leaderName Barack_Obama The country Bacon_Explosion comes from is the United_States where Barack_Obama was once the leader . Native_Americans are one of the ethnic groups of the country and the capital is Washington .
passive_voice United_States leaderName Joe_Biden The Bacon_Explosion comes from the United_States , where Joe_Biden is a leader , the capital is Washington and White_Americans make up an ethnic group .
passive_voice Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas . It is located in the United_States where the capital city is Washington and one of the ethnic groups are the Native_Americans .
passive_voice Amarillo_,_Texas isPartOf Potter_County_,_Texas African_Americans are an ethnic group in the United_States , where the capital is Washington . The United_States is home to Amarillo_,_Texas , which is part of Potter_County_,_Texas .
relative_adverb Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas in the United_States . Washington is the United_States capital and African_Americans are an ethnic group in that country .
relative_adverb Albany_,_Oregon isPartOf Benton_County_,_Oregon The city of Albany_,_Oregon is part of Benton_County_,_Oregon , in the United_States . The capital of the country is , Washington and African_Americans , are one of the country ' s ethnic groups .
relative_object Amarillo_,_Texas isPartOf Randall_County_,_Texas The United_States is home to the capital Washington and the ethnic group of African_Americans . Amarillo_,_Texas which is part of Randall_County_,_Texas , is found in the United_States .
possessif Albany_,_Oregon isPartOf Oregon Albany_,_Oregon is part of Oregon , in the United_States , where the capital is Washington and where Native_Americans are an ethnic group .
passive_voice United_States leaderName Paul_Ryan The country Bacon_Explosion comes from is the United_States . A political leader there is Paul_Ryan , the capital is Washington and White_Americans are one of the ethnic groups .
passive_voice United_States leaderName Joe_Biden The United_States is the country of the Bacon_Explosion . Joe_Biden is one of the leaders there , Washington is the capital and Asian_Americans are one ethnic group of the country .
direct_object Alpharetta_,_Georgia isPartOf Georgia_(U.S._state) Alpharetta_,_Georgia , Georgia_(U.S._state) is in the United_States where Washington is the capital and African_Americans are one of several ethnic groups .
relative_subject Albany_,_Oregon isPartOf Oregon Albany_,_Oregon is part of Oregon , in the United_States , where the capital is Washington and where Native_Americans are an ethnic group .
apposition United_States leaderName John_Roberts Bacon_Explosion comes from the United_States where John_Roberts is a leader and the capital is Washington . African_Americans are an ethic group there .
relative_object United_States leaderName Barack_Obama The United_States is the country of the Bacon_Explosion . Other facts about the United_States : Barack_Obama was once the leader , one ethnic group are Native_Americans and Washington is the capital .
relative_object Alpharetta_,_Georgia isPartOf Fulton_County_,_Georgia Alpharetta_,_Georgia is part of Fulton_County_,_Georgia , United_States . The capital of the United_States is Washington and African_Americans are an ethnic group .
relative_object Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas in the United_States . Washington is the United_States capital and African_Americans are an ethnic group in that country .
possessif United_States leaderName Paul_Ryan Bacon_Explosion comes from the United_States , where one of the leaders is Paul_Ryan . It is also where Asian_Americans are one of the ethnic groups and Washington is the capital .
coordinated_full_clauses United_States leaderName Paul_Ryan The country Bacon_Explosion comes from is the United_States . A political leader there is Paul_Ryan , the capital is Washington and White_Americans are one of the ethnic groups .
coordinated_full_clauses United_States leaderName John_Roberts The United_States is the country of the Bacon_Explosion . John_Roberts is the leader of the United_States and the capital is Washington . African_Americans are one of the ethnic groups of the country .
coordinated_full_clauses Albany_,_Oregon isPartOf Linn_County_,_Oregon Albany_,_Oregon is part of Linn_County_,_Oregon , United_States . The capital of the United_States is Washington and Native_Americans are among the ethnic groups of the country .
direct_object Akron_,_Ohio isPartOf Summit_County_,_Ohio Akron_,_Ohio , Summit_County_,_Ohio is located within the United_States , whose capital is Washington . Asian_Americans are an ethnic group in the United_States .
coordinated_clauses United_States leaderName Barack_Obama The leader of the United_States was Barack_Obama . The capital of the United_States is Washington . 1634:_The_Ram_Rebellion comes from the United_States and one of it ' s ethnic groups is Asian_Americans .
coordinated_clauses United_States leaderName Joe_Biden The United_States is the country of the Bacon_Explosion . Joe_Biden is one of the leaders there , Washington is the capital and Asian_Americans are one ethnic group of the country .
coordinated_clauses United_States leaderName Paul_Ryan The Bacon_Explosion comes from is the United_States where Paul_Ryan is a politician and Native_Americans are one ethnic group . The capital of the country is Washington .
coordinated_clauses Amarillo_,_Texas isPartOf Potter_County_,_Texas Amarillo_,_Texas is part of Potter_County_,_Texas in the United_States . Washington is the capital of the United_States and Native_Americans are an ethnic group there .
coordinated_clauses Akron_,_Ohio isPartOf Summit_County_,_Ohio African_Americans are an ethnic group in the United_States , where Washington is the capital . Located in the United_States , is Akron_,_Ohio , in Summit_County_,_Ohio .
relative_object United_States leaderName Joe_Biden The Bacon_Explosion is a dish from the United_States where Joe_Biden is a political leader . The capital of the country is Washington and the Native_Americans are an ethnic group .
direct_object Antioch_,_California populationTotal 102372 The total area of Antioch_,_California is 75.324_square_kilometres and uses UTC offset of "-7" . It has a total population of 102372 inhabitants and its area code is 925 .
apposition Anaheim_,_California populationTotal 336265 Anaheim_,_California has a population of 336265 and its total area is 131.6_square_kilometres . It has a UTC offset of "-7" and 657,_714 are its area codes .
direct_object California language Spanish_language The United_States_House_of_Representatives is located at the United_States_Capitol and is the leader of the city of Anaheim_,_California . Spanish_language is one of the languages used in California .
apposition Michigan capital Lansing_,_Michigan While Lansing_,_Michigan is the capital of Michigan , Detroit is considered the largest city and Ann_Arbor_,_Michigan is part of Washtenaw_County_,_Michigan .
relative_adverb Alpharetta_,_Georgia country United_States Fulton_County_,_Georgia is the location of Alpharetta_,_Georgia , Georgia_(U.S._state) , in the United_States . Atlanta is the largest city in Fulton_County_,_Georgia .
relative_adverb DeKalb_County_,_Georgia country United_States Atlanta is part of DeKalb_County_,_Georgia and Fulton_County_,_Georgia , United_States . Dunwoody_,_Georgia is the largest city in DeKalb_County_,_Georgia .
relative_adverb Antioch_,_California leaderTitle United_States_House_of_Representatives Antioch_,_California , is part of Contra_Costa_County_,_California and is led by the United_States_House_of_Representatives . A language spoken in California is Chinese_language .
relative_adverb Atlanta country United_States Most of Atlanta , which is in "Georgia" in the United_States , is part of DeKalb_County_,_Georgia where Dunwoody_,_Georgia is the largest city .
coordinated_full_clauses Texas language Spanish_language Spanish_language is one of the languages spoken in Texas which is in the United_States . Arlington can be found in Texas although the largest city is Houston .
relative_subject Texas language Spanish_language Arlington and Houston ( largest city in Texas ) are both part of Spanish_language speaking Texas In the United_States .
apposition Albuquerque_,_New_Mexico areaOfLand 486.2_square_kilometres Albuquerque_,_New_Mexico , has a total area of 490.9_square_kilometres and a land area of 486.2_square_kilometres . It has a population density of 1142.3_inhabitants_per_square_kilometre and the area codes are 505,_575 .
juxtaposition New_Mexico_Senate leader John_Sánchez Albuquerque_,_New_Mexico , in the United_States , where one of the ethnic groups is Asian_Americans . The New_Mexico_Senate leads Albuquerque_,_New_Mexico and John_Sánchez is one of the leaders .
relative_object New_Mexico_Senate leader John_Sánchez Albuquerque_,_New_Mexico , in the United_States , where one of the ethnic groups is Asian_Americans . The New_Mexico_Senate leads Albuquerque_,_New_Mexico and John_Sánchez is one of the leaders .
relative_subject United_States leader Barack_Obama Albuquerque_,_New_Mexico is in the United_States and is lead by The New_Mexico_Senate . Barack_Obama is a leader of the United_States , where The Native_Americans are an ethnic group .
apposition United_States leader Barack_Obama Albuquerque_,_New_Mexico is located in the United_States and led by the Mayor_of_Albuquerque . Barack_Obama is the leader of the United_States and African_Americans are one of the ethnic groups in the country .
direct_object New_Mexico_Senate leader John_Sánchez Albuquerque_,_New_Mexico is in the United_States and is lead by The New_Mexico_Senate and John_Sánchez . One of the ethnic groups in the United_States are the African_Americans .
direct_object Akron_,_Ohio isPartOf Summit_County_,_Ohio Akron_,_Ohio , led by the Mayor , is in Summit_County_,_Ohio , in the United_States , where Asian_Americans are one of the ethnic groups .
apposition Akron_,_Ohio isPartOf Summit_County_,_Ohio Akron_,_Ohio is located in Summit_County_,_Ohio in the United_States . Native_Americans are an ethnic group in the United_States . The people living in that country are known as Americans .
direct_object Akron_,_Ohio isPartOf Summit_County_,_Ohio Akron_,_Ohio is located in Summit_County_,_Ohio in the United_States . Native_Americans are an ethnic group in the United_States . The people living in that country are known as Americans .
direct_object Albany_,_Georgia isPartOf Dougherty_County_,_Georgia Americans live in the United_States , where there is an ethnic group called African_Americans . The United_States is home to Albany_,_Georgia , which is part of Dougherty_County_,_Georgia .
direct_object United_States capital Washington Abilene_,_Texas is part of Texas in the United_States where English_language is the spoken language . Washington is the capital of the United_States .
relative_adverb Texas capital Austin Spanish_language is one of the languages used in Texas , where the capital is Austin and Houston is the largest city . Arlington can also be found in Texas .
apposition Italy leaderName Sergio_Mattarella Sergio_Mattarella is a leader in Italy alongside Matteo_Renzi . You ' ll find Amatriciana_sauce here where Italian_language is the language spoken and the capital is Rome .
apposition Abilene isPartOf Jones_County_,_Texas Abilene is part of Jones_County_,_Texas in the United_States . The capital city of the United_States is Washington and English_language is its official language .
relative_adverb Italy leaderName Matteo_Renzi Italy is the country Amatriciana_sauce comes from . The capital of the country is Rome , the language spoken is Italian_language and the leaders include Matteo_Renzi and Laura_Boldrini .
possessif English_language spokenIn Great_Britain A_Wizard_of_Mars was published in the United_States where many Asian_Americans live . The language of the United_States and Great_Britain is English_language .
possessif Spain leaderName Felipe_VI_of_Spain Arròs_negre comes from Spain where the Spaniards are an ethnic group . The leader of the country , where the spoken language is Spanish_language , is Felipe_VI_of_Spain .
direct_object English_language spokenIn Great_Britain A_Wizard_of_Mars was published in the United_States where many Asian_Americans live . The language of the United_States and Great_Britain is English_language .
direct_object English_language spokenIn Great_Britain A_Fortress_of_Grey_Ice is from the United_States where African_Americans is an ethnic group and English_language is spoken like in Great_Britain .
direct_object Angola_,_Indiana isPartOf Pleasant_Township_,_Steuben_County_,_Indiana Angola_,_Indiana is in Pleasant_Township_,_Steuben_County_,_Indiana , in the United_States , which is where English_language is spoken and African_Americans are one of the ethnic groups .
apposition English_language spokenIn Great_Britain Asian_Americans is one of the ethnic group in the United_States where English_language is spoken along with Great_Britain . Alcatraz_Versus_the_Evil_Librarians is from The United_States .
apposition United_States leaderName Barack_Obama English_language is the language of the United_States where Barack_Obama is president , Asian_Americans are an ethnic group and A_Severed_Wasp originates .
apposition Albany_,_Georgia isPartOf Dougherty_County_,_Georgia English_language is the language of the United_States , where Asian_Americans are an ethnic group . The United_States is home to Albany_,_Georgia , which is part of Dougherty_County_,_Georgia .
direct_object English_language spokenIn Great_Britain A_Loyal_Character_Dancer is published in the United_States where the Asian_Americans are an ethnic group . English_language is spoken both in the United_States and Great_Britain .
direct_object English_language spokenIn Great_Britain A_Wizard_of_Mars was written in the United_States where the main language is English_language ( originated in Great_Britain ). Native_Americans are an ethnic group of the United_States .
direct_object United_States leaderName Barack_Obama A_Severed_Wasp originates from the United_States where the leader is Barack_Obama . The English_language is spoken in the United_States where one of the ethnic groups is African_Americans .
direct_object Albany_,_Georgia isPartOf Georgia_(U.S._state) Albany_,_Georgia , Georgia_(U.S._state) , is in the United_States , where English_language is spoken and , where Native_Americans are one of the ethnic groups .
direct_object Angola_,_Indiana isPartOf Steuben_County_,_Indiana English_language is the language of the United_States , where Asian_Americans are an ethnic group and where Angola_,_Indiana , Steuben_County_,_Indiana is located .
relative_adverb A_Loyal_Character_Dancer publisher Soho_Press A_Loyal_Character_Dancer was published by Soho_Press located in the United_States . The language of the United_States is English_language and one of United_States ethnic groups is Native_Americans .
coordinated_full_clauses English_language spokenIn Great_Britain A_Severed_Wasp originates from the United_States where English_language is spoken . That language is also spoken in Great_Britain . African_Americans are an ethnic group in the United_States .
coordinated_full_clauses English_language spokenIn Great_Britain English_language is spoken in Great_Britain and the United_States . A_Fortress_of_Grey_Ice is from the United_States where one of the ethnic groups are African_Americans .
coordinated_full_clauses Akron_,_Ohio isPartOf Summit_County_,_Ohio Akron_,_Ohio is located in Summit_County_,_Ohio , United_States , where English_language is the language spoken and Native_Americans are an ethnic group .
apposition English_language spokenIn Great_Britain Alcatraz_Versus_the_Evil_Librarians is from The United_States where one of the ethnic groups are the Native_Americans . The English_language is spoken in the United_States as well as Great_Britain .
relative_subject A_Loyal_Character_Dancer publisher Soho_Press Soho_Press ( located in the United_States ) published A_Loyal_Character_Dancer . Native_Americans are an ethnic group of the English_language speaking United_States .
relative_subject English_language spokenIn Great_Britain The Book Alcatraz_Versus_the_Evil_Librarians originated in the United_States where Native_Americans make up many of the countries ethnic groups . English_language is the national language of both Great_Britain and the United_States .
relative_subject Spain leaderName Felipe_VI_of_Spain Arròs_negre is a dish from Spain where they speak Spanish_language and the people are called Spaniards . Felipe_VI_of_Spain is the leader .
passive_voice Albany_,_Georgia isPartOf Georgia_(U.S._state) Albany_,_Georgia , Georgia_(U.S._state) , is in the United_States , where English_language is the language and where Asian_Americans are one of the ethnic groups .
coordinated_full_clauses English_language spokenIn Great_Britain Asian_Americans are one of the ethnic groups in the United_States of which A_Fortress_of_Grey_Ice is also , the English_language is spoken in both the United_States and in Great_Britain .
apposition United_States leaderName Barack_Obama A_Wizard_of_Mars was published in the United_States where the language used is English_language . One of the ethnic groups in the country are the African_Americans and the leader is Barack_Obama .
direct_object English_language spokenIn Great_Britain A_Wizard_of_Mars originates from the United_States where African_Americans is an ethnic group and where English_language is the language of the United_States which is also spoken in Great_Britain .
passive_voice Angola_,_Indiana isPartOf Indiana Angola_,_Indiana is part of the state of Indiana , United_States . English_language is the language of the United_States and some Native_Americans live there .
coordinated_full_clauses Angola_,_Indiana isPartOf Pleasant_Township_,_Steuben_County_,_Indiana Angola_,_Indiana , Pleasant_Township_,_Steuben_County_,_Indiana is part of The United_States , home to Native_Americans and includes the English_language .
juxtaposition A_Loyal_Character_Dancer publisher Soho_Press A_Loyal_Character_Dancer is published in the United_States by Soho_Press . English_language is spoken in the United_States and Native_Americans are the original ethnic group of that country .
juxtaposition English_language spokenIn Great_Britain A_Wizard_of_Mars was published in the United_States , where English_language is the main language , as in Great_Britain . African_Americans are one of the ethnic groups in the United_States .
juxtaposition Akron_,_Ohio isPartOf Summit_County_,_Ohio Akron_,_Ohio in Summit_County_,_Ohio , is part of the United_States . where English_language is spoken and is home to Native_Americans .
juxtaposition English_language spokenIn Great_Britain A_Loyal_Character_Dancer is published in the United_States , where one of the ethnic groups is Native_Americans . The language spoken in the United_States is English_language , which is also the language of Great_Britain .
coordinated_full_clauses Angola_,_Indiana isPartOf Indiana Angola_,_Indiana , a city in Indiana , is in the United_States . The same English_language speaking country , where African_Americans are one of the ethnic groups .
existential Bhajji region Karnataka Bhajji comes from the Karnataka region of India . The dish contains Vegetable and also known as "Bhaji_,_bajji" or Bhajji .
apposition Ajoblanco region Andalusia Ajoblanco is a food originates from the Andalusia region of Spain and contains Water . An other alternative name of the dish is "Ajo_blanco" .
possessif Ajoblanco region Andalusia Ajoblanco is a food found in Andalusia , Spain . "Ajo_blanco" is an alternative name and it includes Almond .
possessif Bacon_sandwich dishVariation BLT Bacon_sandwich is a dish from the United_Kingdom and a variation is the BLT . An ingredient used is Brown_sauce and it ' s different names include "Bacon_butty_,_bacon_sarnie_,_rasher_sandwich_,_bacon_sanger_,_piece_'n_bacon_,_bacon_cob_,_bacon_barm_,_bacon_muffin" .
passive_voice Bhajji region Karnataka Bhajji comes from the Karnataka region of India . The dish contains Vegetable and also known as "Bhaji_,_bajji" or Bhajji .
relative_subject Ajoblanco alternativeName "Ajo_blanco" Ajoblanco ( or "Ajo_blanco" ) originates from the country of the Andalusia region of Spain . Olive_oil is an ingredient .
relative_subject Java ethnicGroup Javanese_people Ayam_penyet , a Fried_chicken dish , is from Malaysia and Java ( country of the Javanese_people ).
relative_subject Binignit mainIngredients Taro Binignit is a dish from the Visayas region of the Philippines and includes the main ingredient Taro and also Sweet_potato .
apposition Java ethnicGroup Banyumasan_people The Fried_chicken dish Ayam_penyet is from Malaysia and Java . The Banyumasan_people are an ethnic group from Java .
apposition Beef_kway_teow mainIngredients "Kway_teow_,_beef_tender_loin_,_gula_Melaka_,_sliced_,_dried_black_beans_,_garlic_,_dark_soy_sauce_,_lengkuas_,_oyster_sauce_,_soya_sauce_,_chilli_and_sesame_oil" Beef_kway_teow originates from Singapore and Indonesia . The main ingredients of the dish are : "Kway_teow_,_beef_tender_loin_,_gula_Melaka_,_sliced_,_dried_black_beans_,_garlic_,_dark_soy_sauce_,_lengkuas_,_oyster_sauce_,_soya_sauce_,_chilli_and_sesame_oil" and Sesame_oil .
direct_object Spain leaderName Felipe_VI_of_Spain one of the ingredients of Ajoblanco is Almond . it is from Spain , which leader is Felipe_VI_of_Spain and from Andalusia regions , which leader is Susana_Díaz .
existential Arròs_negre mainIngredients "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" Arròs_negre , from the Catalonia region of Spain , includes "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" , cuttlefish or squid , Cephalopod_ink , cubanelle peppers .
direct_object Java ethnicGroup Javanese_people Ayam_penyet , a Fried_chicken dish , is from Malaysia and Java ( country of the Javanese_people ).
existential Baked_Alaska mainIngredients "Meringue_,_ice_cream_,_sponge_cake_or_Christmas_pudding" Baked_Alaska ( New_York and France ) contains : Sponge_cake , ( aka Christmas Pudding ) , "Meringue_,_ice_cream_,_sponge_cake_or_Christmas_pudding" and ice cream .
direct_object Bandeja_paisa mainIngredients "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" Bandeja_paisa is part of Colombian_cuisine that originates from the Paisa_Region . Main ingredients are Kidney_bean , "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" .
direct_object Bandeja_paisa mainIngredients "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" The main ingredients of Colombian_cuisine Bandeja_paisa are : "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" and Lemon . The dish hails from the Paisa_Region .
direct_object Binignit mainIngredients Taro Binignit is a dish from the region of Visayas , the Philippines . Taro and Sago are main ingredients .
existential Bionico course Dessert Bionico , a Dessert found in the Jalisco region of Mexico , has Granola in it .
coordinated_clauses Bhajji alternativeName "Bhaji_,_bajji" Gram_flour is an ingredient of Bhajji , sometimes known as "Bhaji_,_bajji" or Bhajji . Bhajji are found in the Karnataka region of India .
apposition Ajoblanco alternativeName "Ajo_blanco" Ajoblanco is a food originates from the Andalusia region of Spain and contains Water . An other alternative name of the dish is "Ajo_blanco" .
coordinated_clauses Ajoblanco alternativeName "Ajo_blanco" Ajoblanco , from the Andalusia region in Spain , has Almond as one of its ingredients . "Ajo_blanco" is an alternative name of Ajoblanco .
possessif Binignit mainIngredients Sago Binignit is a dish from the region of Visayas , Philippines . It includes Sago and Sweet_potato .
possessif Bionico course Dessert Using one of the required ingredients , Granola , Bionico is a dish served for Dessert and is found in the region of Jalisco , in Mexico .
coordinated_full_clauses Ajoblanco alternativeName "Ajo_blanco" Ajoblanco is from Spain and Andalusia . It contains Olive_oil and another name for it is "Ajo_blanco" .
coordinated_full_clauses Arròs_negre mainIngredients "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" Arròs_negre is a traditional dish from Spain which comes from the region of the Valencian_Community . The main ingredients are "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" and Squid .
coordinated_full_clauses Java ethnicGroup Javanese_people The Fried_chicken dish Ayam_penyet is a popular dish in Malaysia and Java . The Javanese_people are an ethnic group of Java .
coordinated_full_clauses Avocado family Lauraceae A typical Colombian_cuisine is Bandeja_paisa of which the Avocado is one ingredient . The Avocado plant belongs to the Lauraceae family while Bandeja_paisa is from the Paisa_Region .
coordinated_full_clauses Beef_kway_teow mainIngredients "Kway_teow_,_beef_tender_loin_,_gula_Melaka_,_sliced_,_dried_black_beans_,_garlic_,_dark_soy_sauce_,_lengkuas_,_oyster_sauce_,_soya_sauce_,_chilli_and_sesame_oil" Beef_kway_teow originates from Singapore and Indonesia . The main ingredients of the dish are : "Kway_teow_,_beef_tender_loin_,_gula_Melaka_,_sliced_,_dried_black_beans_,_garlic_,_dark_soy_sauce_,_lengkuas_,_oyster_sauce_,_soya_sauce_,_chilli_and_sesame_oil" and Sesame_oil .
coordinated_full_clauses Binignit mainIngredients Banana Binignit is a dish from the Visayas region of the Philippines . Sago is included along with the main ingredient of Banana .
apposition Malaysia ethnicGroup Malaysian_Chinese Ayam_penyet , which has Fried_chicken in it , is a popular dish in Malaysia and Java . The Malaysian_Chinese are an ethnic group from Malaysia .
apposition Bandeja_paisa mainIngredients "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" Bandeja_paisa is part of Colombian_cuisine that originates from the Paisa_Region . Main ingredients are Kidney_bean , "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" .
apposition Bhajji alternativeName "Bhaji_,_bajji" Gram_flour is an ingredient of Bhajji , sometimes known as "Bhaji_,_bajji" or Bhajji . Bhajji are found in the Karnataka region of India .
possessif Spain leaderName Felipe_VI_of_Spain one of the ingredients of Ajoblanco is Almond . it is from Spain , which leader is Felipe_VI_of_Spain and from Andalusia regions , which leader is Susana_Díaz .
possessif Spain leaderName Felipe_VI_of_Spain Almond is an ingredient in Ajoblanco which comes from the Andalusia region in Spain . Felipe_VI_of_Spain is the leader of that country .
possessif Arròs_negre mainIngredients "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" Arròs_negre comes from the region of the Valencian_Community in Spain . The main ingredients are "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" , cuttlefish or squid , Cephalopod_ink , cubanelle peppers .
possessif Bandeja_paisa mainIngredients "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" The main ingredients of Colombian_cuisine Bandeja_paisa are "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" and Lemon . The dish originates from the Paisa_Region .
possessif Java ethnicGroup Javanese_people Ayam_penyet is a Fried_chicken dish from Singapore and Java . The Javanese_people are an ethnic group in Java .
passive_voice Spain leaderName Felipe_VI_of_Spain Almond is an ingredient in Ajoblanco which comes from the Andalusia region in Spain . Felipe_VI_of_Spain is the leader of that country .
passive_voice Malaysia ethnicGroup Malaysian_Chinese The Fried_chicken dish Ayam_penyet is a popular dish in Malaysia and Indonesia . Malaysian_Chinese is an ethnic group from Malaysia .
passive_voice Bandeja_paisa mainIngredients "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" The main ingredients of Colombian_cuisine Bandeja_paisa are "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" and Lemon . The dish originates from the Paisa_Region .
passive_voice Binignit mainIngredients Banana Binignit is a dish from the region of Visayas , Philippines . Sago and Banana are main ingredients .
juxtaposition Spain leaderName Felipe_VI_of_Spain Ajoblanco has Almond as one of its ingredients , it originates from the Andalusia region of Spain . Felipe_VI_of_Spain is the leader of the country .
passive_voice Malaysia ethnicGroup Malaysian_Chinese Fried_chicken is one of the ingredients of the dish Ayam_penyet , which is from Indonesia . The dish comes from the region of Malaysia , where the Malaysian_Chinese are an ethnic group .
relative_subject Binignit mainIngredients Banana The Binignit is a dish from Visayas and it can be found in the Philippines . It contains Sago and a main ingredient is Banana .
relative_subject Spain leaderName Felipe_VI_of_Spain Ajoblanco has Almond as one of its ingredients , it originates from the Andalusia region of Spain . Felipe_VI_of_Spain is the leader of the country .
relative_subject Arròs_negre mainIngredients "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" Arròs_negre comes from the region of the Valencian_Community in Spain and includes the main ingredients "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" , cuttlefish or squid , Cephalopod_ink , cubanelle peppers .
coordinated_clauses Ajoblanco alternativeName "Ajo_blanco" Ajoblanco ( or "Ajo_blanco" ) originates from the country of the Andalusia region of Spain . Olive_oil is an ingredient .
coordinated_clauses Arròs_negre mainIngredients "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" Arròs_negre is a traditional dish from Spain which comes from the region of the Valencian_Community . The main ingredients are "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" and Squid .
coordinated_clauses Java ethnicGroup Banyumasan_people Ayam_penyet is a Fried_chicken dish from Malaysia and Java . The Banyumasan_people are an ethnic group from Java .
coordinated_clauses Avocado family Lauraceae A typical Colombian_cuisine is Bandeja_paisa of which the Avocado is one ingredient . The Avocado plant belongs to the Lauraceae family while Bandeja_paisa is from the Paisa_Region .
coordinated_full_clauses Ajoblanco alternativeName "Ajo_blanco" "Ajo_blanco" is the alternative name of Ajoblanco which comes from the Andalusia region of Spain and has Garlic as an ingredient .
coordinated_full_clauses Binignit mainIngredients Coconut_milk Binignit , which contains Sweet_potato and Coconut_milk , is a dish from the Visayas region of the Philippines .
juxtaposition Ajoblanco alternativeName "Ajo_blanco" Ajoblanco is from Spain and Andalusia . It contains Olive_oil and another name for it is "Ajo_blanco" .
juxtaposition Binignit mainIngredients Taro Binignit is a dish from the Visayas region of the Philippines and includes the main ingredient Taro and also Sweet_potato .
coordinated_full_clauses Binignit mainIngredients Sago Binignit is a dish from the region of Visayas , Philippines . It includes Sago and Sweet_potato .
passive_voice Bionico course Dessert Bionico , a Dessert found in the Jalisco region of Mexico , has Granola in it .
apposition Binignit mainIngredients Banana Binignit contains both Sweet_potato and Banana and can be found in the Visayas region of the Philippines .
relative_subject Almond order Rosids Almond are from the Rosaceae family order of Rosids and is classed as a Flowering_plant . It is an ingredient in Ajoblanco .
apposition Tomato genus Solanum Tomato is an ingredient in Amatriciana_sauce , it comes from the Solanaceae family of plants and belongs to the genus Solanum and the order Solanales .
existential Indonesia language Indonesian_language Indonesia has Jusuf_Kalla as a leader , Indonesian_language and Bakso , ( from Chinese_cuisine ) , as a traditional dish .
relative_subject Java ethnicGroup Banyumasan_people The Banyumasan_people are an ethnic group in Java , where the dish Ayam_penyet is found . The dish originates in Singapore , where Tony_Tan is the leader .
apposition Java ethnicGroup Baduy Baduy is an ethnic group of Java , where the dish Ayam_penyet can be found . This dish is from the Singapore region , where Tony_Tan is a leader .
possessif Ajoblanco ingredient Garlic Ajoblanco is a food found in Andalusia , Spain and it contains Garlic . Susana_Díaz is the leader of Andalusia .
apposition Java ethnicGroup Banyumasan_people Ayam_penyet is a dish from Singapore and Java . Banyumasan_people is one of the ethnic groups in Java and Tony_Tan is a leader in Singapore .
direct_object Singapore language English_language The English_language is spoken in Singapore , which is where Tony_Tan is the leader . It is also where the dish Beef_kway_teow ( which is also popular in Indonesia ) comes from .
direct_object Java ethnicGroup Banyumasan_people The Banyumasan_people are an ethnic group in Java , where the dish Ayam_penyet is found . The dish originates in Singapore , where Tony_Tan is the leader .
passive_voice Spain ethnicGroup Spaniards Ajoblanco originates from the Andalusia region of Spain where Susana_Díaz is the leader . The country ' s main ethnic group are the Spaniards .
relative_adverb Java ethnicGroup Baduy Ayam_penyet is originates from Singapore and Tony_Tan is the country ' s leader . It can also be found in Java where Baduy people are one of the ethnic groups .
relative_subject Binignit region Visayas Binignit is a dish from the region of Visayas in the Philippines . One of the main ingredients of Binignit is Coconut_milk and another ingredient is Sago .
relative_subject Bandeja_paisa region Paisa_Region Bandeja_paisa , a dish containing Ground_meat , is part of Colombian_cuisine and from the Paisa_Region . The main ingredients are "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" .
direct_object Bandeja_paisa region Paisa_Region The main ingredients of Colombian_cuisine Bandeja_paisa are : "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" and Lemon . The dish hails from the Paisa_Region .
existential Arròs_negre region Catalonia Arròs_negre , from the Catalonia region of Spain , includes "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" , cuttlefish or squid , Cephalopod_ink , cubanelle peppers .
juxtaposition Beef_kway_teow region Singapore Beef_kway_teow originates from Singapore and Indonesia . The main ingredients of the dish are : "Kway_teow_,_beef_tender_loin_,_gula_Melaka_,_sliced_,_dried_black_beans_,_garlic_,_dark_soy_sauce_,_lengkuas_,_oyster_sauce_,_soya_sauce_,_chilli_and_sesame_oil" and Sesame_oil .
juxtaposition Binignit region Visayas Binignit is a dish from the region of Visayas , Philippines . The main ingredients are Sweet_potato and Sago .
passive_voice Bandeja_paisa region Paisa_Region Bandeja_paisa is from the Paisa_Region and part of the Colombian_cuisine . The main ingredients of Bandeja_paisa are : "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" , Avocado and lemon .
passive_voice Binignit region Visayas Binignit is a dish from the region of Visayas in the Philippines . It includes Sweet_potato and Banana .
coordinated_clauses Bacon_Explosion course "Main_course" The United_States is home to the "Main_course" dish Bacon_Explosion which contains Bacon and Sausage .
coordinated_clauses Arròs_negre region Catalonia Arròs_negre , from the Catalonia region of Spain , includes "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" , cuttlefish or squid , Cephalopod_ink , cubanelle peppers .
coordinated_full_clauses Binignit region Visayas Binignit is a dish from the region of Visayas , Philippines . It includes Sago and Sweet_potato .
coordinated_full_clauses Arròs_negre region Catalonia Arròs_negre is a traditional dish from the Catalonia region of Spain . The main ingredients of Arròs_negre are "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" , cuttlefish or squid , Cephalopod_ink , cubanelle peppers .
coordinated_full_clauses Binignit region Visayas Binignit originates from the Visayas region of the Philippines . Its main ingredients are Sweet_potato and Banana .
coordinated_full_clauses English_language spokenIn Great_Britain A_Fortress_of_Grey_Ice is from the United_States where they speak English_language and Barack_Obama is the president . They also speak English_language in Great_Britain .
coordinated_full_clauses Beef_kway_teow region Indonesia Beef_kway_teow comes from the region of Indonesia and also Singapore . The leader of Singapore , where Standard_Chinese is the spoken language , is Halimah_Yacob .
coordinated_full_clauses Spain ethnicGroup Spaniards Arròs_negre is a dish from Spain where they speak Spanish_language and the people are called Spaniards . Felipe_VI_of_Spain is the leader .
direct_object Addis_Ababa_City_Hall location Addis_Ababa Addis_Ababa_City_Hall is located in Addis_Ababa , Ethiopia . Hailemariam_Desalegn is the ruler , where the language spoken is Amharic .
relative_adverb Arem-arem region Javanese_cuisine Arem-arem is from Javanese_cuisine and is commonly served in Indonesia . The country ' s leader is Jusuf_Kalla and the spoken language is Indonesian_language .
relative_adverb AIDS_(journal) publisher Lippincott_Williams_&_Wilkins AIDS_(journal) is published by Lippincott_Williams_&_Wilkins in the United_Kingdom . The leader of the United_Kingdom is David_Cameron and English_language is the language spoken .
relative_adverb English_language spokenIn Great_Britain A_Wizard_of_Mars was Published in the United_States . The previous president there was Barack_Obama and the main language is English_language ( the language of Great_Britain ).
relative_adverb Antwerp_International_Airport cityServed Antwerp Antwerp_International_Airport covers the city of Antwerp . Antwerp is a popular tourist destination in Belgium . A leader there is Charles_Michel and Dutch_language is the language spoken there .
passive_voice English_language spokenIn Great_Britain A_Fortress_of_Grey_Ice is from the United_States where they speak English_language and Barack_Obama is the president . They also speak English_language in Great_Britain .
coordinated_clauses Al-Taqaddum_Air_Base cityServed Fallujah The leader of Iraq is Haider_al-Abadi and they speak Arabic . Fallujah which is located in Iraq is served by the Al-Taqaddum_Air_Base .
direct_object Italy capital Rome Rome is the capital city of the country of Italy , where the language is Italian_language , the leader is Pietro_Grasso and Arrabbiata_sauce can be found .
coordinated_full_clauses Al-Taqaddum_Air_Base cityServed Fallujah Al-Taqaddum_Air_Base serves the city of Fallujah , Iraq . Fuad_Masum is the leader of Iraq , where the language spoken is Arabic .
apposition Republic_of_Ireland currency Euro In the Republic_of_Ireland the currency is the Euro , the language English_language and the leader is Enda_Kenny . It is the location of Adare_Manor .
possessif English_language spokenIn Great_Britain English_language is spoken in both Great_Britain and the United_States ( home of President Barack_Obama ). A_Severed_Wasp is from the United_States .
coordinated_full_clauses English_language spokenIn Great_Britain English_language is spoken in both Great_Britain and the United_States ( home of President Barack_Obama ). A_Severed_Wasp is from the United_States .
relative_adverb United_States ethnicGroup African_Americans The President of the United_States is Barack_Obama who is African_Americans , which makes up one of the many ethnic groups in the United_States . The national language spoken in the United_States is English_language . The book A_Severed_Wasp originated in the United_States .
direct_object Beef_kway_teow region Indonesia Halimah_Yacob is a leader of Singapore where English_language is spoken . Beef_kway_teow is a dish served in Singapore and in the region of Indonesia .
coordinated_clauses AIDS_(journal) publisher Lippincott_Williams_&_Wilkins In the United_Kingdom , the leader ( Queen ) is Elizabeth_II , English_language is the primary language and the AIDS_(journal) was also published here by Lippincott_Williams_&_Wilkins .
coordinated_clauses English_language spokenIn Great_Britain A_Wizard_of_Mars was Published in the United_States . The previous president there was Barack_Obama and the main language is English_language ( the language of Great_Britain ).
coordinated_clauses Italy capital Rome Amatriciana_sauce hails from Italian_language speaking Italy ; where the capital is Rome and is led by Matteo_Renzi .
coordinated_clauses Antwerp_International_Airport cityServed Antwerp Charles_Michel is the leader of Belgium where the French_language is spoken . Antwerp in the country is served by Antwerp_International_Airport .
existential Al-Taqaddum_Air_Base cityServed Fallujah Al-Taqaddum_Air_Base serves the city of Fallujah in Iraq . The country is lead by Haider_al-Abadi and uses the Kurdish_languages .
passive_voice Al-Taqaddum_Air_Base cityServed Fallujah Al-Taqaddum_Air_Base serves the city of Fallujah , Iraq . Fuad_Masum is the leader of Iraq , where the language spoken is Arabic .
direct_object Lemon family Rutaceae Bandeja_paisa comes from the Antioquia_Department in Colombia . One of the ingredient is Lemon which is a member of the Rutaceae family .
coordinated_full_clauses Ampara_Hospital state Eastern_Province_,_Sri_Lanka Ampara_Hospital is located in the Eastern_Province_,_Sri_Lanka , where the currency is the Sri_Lankan_rupee . One of the leaders of Sri_Lanka is Ranil_Wickremesinghe .
possessif Sri_Lanka capital Sri_Jayawardenepura_Kotte Ampara_Hospital is in Sri_Lanka , where the leader is Ranil_Wickremesinghe , the currency is the Sri_Lankan_rupee and the capital is Sri_Jayawardenepura_Kotte .
relative_object Ampara_Hospital state Eastern_Province_,_Sri_Lanka Ampara_Hospital is in the Eastern_Province_,_Sri_Lanka . The leader of Sri_Lanka is Ranil_Wickremesinghe and its currency is the Sri_Lankan_rupee .
apposition Republic_of_Ireland language English_language In the Republic_of_Ireland the currency is the Euro , the language English_language and the leader is Enda_Kenny . It is the location of Adare_Manor .
relative_adverb Bhajji region Karnataka Bhajji come from the Karnataka region of India . The leaders of the country are T._S._Thakur and Sumitra_Mahajan .
relative_adverb Bhajji region Karnataka Bhajji originates from the Karnataka region of India . The country ' s leaders are Narendra_Modi and Sumitra_Mahajan .
juxtaposition Bhajji region Karnataka Bhajji comes from the Karnataka region in India . Two leaders of India are T._S._Thakur and Narendra_Modi .
juxtaposition Italy capital Rome Pietro_Grasso and Sergio_Mattarella are the leaders of Italy . The capital is Rome and Amatriciana_sauce is a traditional Italian sauce .
juxtaposition Amdavad_ni_Gufa location Gujarat Amdavad_ni_Gufa is located in Gujarat India . Both T._S._Thakur and Sumitra_Mahajan have been leaders in India .
passive_voice Baked_Alaska region New_York Baked_Alaska comes from New_York and also from France where the leaders are Manuel_Valls and Gérard_Larcher .
passive_voice India currency Indian_rupee The Indian_rupee is the currency in India , where Bhajji is a traditional dish and Sumitra_Mahajan and Narendra_Modi are leaders .
relative_adverb Amdavad_ni_Gufa location Ahmedabad Amdavad_ni_Gufa is located in Ahmedabad , India . Two leaders of the country are T._S._Thakur and Narendra_Modi .
coordinated_full_clauses India currency Indian_rupee Bhajji comes from the country India , where two of the leaders are T._S._Thakur and Narendra_Modi . It is also where the Indian_rupee is the currency .
coordinated_full_clauses Athens_International_Airport cityServed Athens Athens_International_Airport serves the city of Athens in Greece , where the leaders are Alexis_Tsipras and Nikos_Voutsis .
direct_object Italy capital Rome Rome is the capital of Italy , where Sergio_Mattarella and Matteo_Renzi are leaders and where Amatriciana_sauce is a traditional dish .
apposition Arem-arem region Javanese_cuisine Arem-arem is a dish of Javanese_cuisine and is commonly served in Indonesia where the leaders are Jusuf_Kalla and Joko_Widodo .
relative_object Bhajji region Karnataka Bhajji originates from the Karnataka region of India . The country ' s leaders are Narendra_Modi and Sumitra_Mahajan .
possessif Alfredo_Zitarrosa deathPlace Montevideo Alfredo_Zitarrosa died in Montevideo , in Uruguay , the leaders of which , are Raúl_Fernando_Sendic_Rodríguez and Tabaré_Vázquez .
apposition France language French_language Barny_Cakes come from France where French_language is spoken and François_Hollande and / or Claude_Bartolone are the leader .
passive_voice Indonesia language Indonesian_language Arem-arem is a Javanese_cuisine dish that originates from the country of Indonesia , where Jusuf_Kalla is the leader and Indonesian_language is the language spoken .
apposition Singapore language English_language Beef_kway_teow is a food found in Indonesia but originates in Singapore where Halimah_Yacob is the leader and the spoken language is English_language .
relative_object Sumatra ethnicGroup Malays_(ethnic_group) Asam_pedas is a dish from Sumatra in Indonesia , a country led by Joko_Widodo . The Acehnese_people and Malays_(ethnic_group) are both ethnic groups in Sumatra .
relative_object Ajoblanco ingredient Almond Almond is an ingredient in Ajoblanco which comes from the Andalusia region in Spain . Felipe_VI_of_Spain is the leader of that country .
relative_subject Singapore language Standard_Chinese Standard_Chinese is the language spoken in Singapore where Tony_Tan is the leader . The popular dish of Beef_kway_teow from Indonesia is eaten in the country .
passive_voice Malaysia ethnicGroup Malaysian_Malay Asam_pedas is a food found on the Malay_Peninsula , Malaysia . Arifin_Zakaria is the leader of the country and ethnic groups there include the Malaysian_Malay and Malaysian_Chinese .
relative_adverb United_States ethnicGroup African_Americans The leader of the United_States is Paul_Ryan and the capital city is Washington . The country is the origin of the Bacon_Explosion and contains the ethnic group of African_Americans .
relative_adverb Sri_Lanka currency Sri_Lankan_rupee Ampara_Hospital is in Sri_Lanka , where the leader is Ranil_Wickremesinghe , the currency is the Sri_Lankan_rupee and Sri_Jayawardenepura_Kotte is the capital .
passive_voice United_States ethnicGroup African_Americans Bacon_Explosion comes from the United_States , the capital of which is Washington . Barack_Obama is leader of the United_States and African_Americans are an ethnic group there .
relative_adverb United_States ethnicGroup African_Americans Bacon_Explosion comes from the United_States , the capital of which is Washington . Barack_Obama is leader of the United_States and African_Americans are an ethnic group there .
possessif United_States ethnicGroup African_Americans 1634:_The_Ram_Rebellion was written in the United_States . The previous president there was Barack_Obama and the capital is Washington . One ethnic group there is African_Americans .
possessif United_States ethnicGroup White_Americans Bacon_Explosion comes from the United_States where Joe_Biden was once a leader and Washington is the capital . White_Americans are one ethnic group there .
possessif United_States ethnicGroup Native_Americans The Bacon_Explosion comes from is the United_States where Paul_Ryan is a politician and Native_Americans are one ethnic group . The capital of the country is Washington .
juxtaposition AIDS_(journal) publisher Lippincott_Williams_&_Wilkins David_Cameron is a leader in the United_Kingdom and the capital is London . The AIDS_(journal) is published there by Lippincott_Williams_&_Wilkins .
juxtaposition 1_Decembrie_1918_University city Alba_Iulia Romania ' s leader is Klaus_Iohannis . Although the capital city is Bucharest , the 1_Decembrie_1918_University is located in the city of Alba_Iulia in Romania .
direct_object United_States ethnicGroup African_Americans The African_Americans are an ethnic group in the United_States where 1634:_The_Ram_Rebellion was written . The President in the United_States is Barack_Obama and the capital city is Washington .
direct_object United_States ethnicGroup Asian_Americans Bacon_Explosion originated in the United_States , where Barack_Obama is leader , Washington is the capital and where Asian_Americans are an ethnic group .
direct_object United_States ethnicGroup Native_Americans Bacon_Explosion comes from the United_States where Joe_Biden is one of the leaders . Native_Americans are an ethnic group there and the capital is Washington .
direct_object Italy language Italian_language Amatriciana_sauce is a traditional sauce in Italy . The country where Rome is the capital , the language is Italian_language and Laura_Boldrini is the leader .
apposition AIDS_(journal) publisher Lippincott_Williams_&_Wilkins AIDS_(journal) is published by Lippincott_Williams_&_Wilkins in the United_Kingdom . The capital of the country is London and it ' s leader is Elizabeth_II .
apposition Italy language Italian_language Rome is the capital city of the country of Italy , where the language is Italian_language , the leader is Pietro_Grasso and Arrabbiata_sauce can be found .
coordinated_full_clauses Adisham_Hall location Haputale Adisham_Hall is located in Haputale , Sri_Lanka , where Ranil_Wickremesinghe is one of the countries leaders and Sri_Jayawardenepura_Kotte is the capital city .
juxtaposition United_States ethnicGroup African_Americans The United_States is the country of the Bacon_Explosion . Paul_Ryan is a leader in the United_States whose capital is Washington . African_Americans are one of the ethnic groups in that country .
coordinated_full_clauses United_States ethnicGroup Asian_Americans The United_States is the country of the Bacon_Explosion and the leader is Paul_Ryan . Asian_Americans are an ethnic group in the United_States and the capital is Washington .
juxtaposition United_States ethnicGroup Asian_Americans Washington is the capital of the United_States where Joe_Biden is a political leader . The Asian_Americans are an ethnic group within the country which is where the Bacon_Explosion comes from .
passive_voice United_States ethnicGroup White_Americans The Bacon_Explosion comes from the United_States where Barack_Obama is the leader . The capital of the country is Washington and the White_Americans are an ethnic group .
relative_adverb Romania ethnicGroup Germans_of_Romania The 1_Decembrie_1918_University is located in Romania ( capital Bucharest ). The leader of Romania is Klaus_Iohannis and the ethnic group is Germans_of_Romania .
relative_subject AIDS_(journal) publisher Lippincott_Williams_&_Wilkins Published by Lippincott_Williams_&_Wilkins , the AIDS_(journal) was published in the United_Kingdom , tha capital of the United_Kingdom is London and the head of state is Elizabeth_II .
relative_subject United_States ethnicGroup African_Americans 1634:_The_Ram_Rebellion was written in the United_States . The previous president there was Barack_Obama and the capital is Washington . One ethnic group there is African_Americans .
passive_voice United_States ethnicGroup Asian_Americans Washington is the capital of the United_States where Joe_Biden is a political leader . The Asian_Americans are an ethnic group within the country which is where the Bacon_Explosion comes from .
relative_subject 1_Decembrie_1918_University city Alba_Iulia Romania ' s leader is Klaus_Iohannis . Although the capital city is Bucharest , the 1_Decembrie_1918_University is located in the city of Alba_Iulia in Romania .
possessif United_States ethnicGroup Native_Americans Bacon_Explosion comes from the United_States which is lead by Joe_Biden . The capital city is Washington and one of the ethnic groups in the country are the Native_Americans .
possessif AIDS_(journal) publisher Lippincott_Williams_&_Wilkins The AIDS_(journal) is published in the United_Kingdom by Lippincott_Williams_&_Wilkins . The capital city of the United_Kingdom is London and the leader is David_Cameron .
possessif Italy language Italian_language Rome is the capital city of the country of Italy , where the language is Italian_language , the leader is Pietro_Grasso and Arrabbiata_sauce can be found .
possessif Ampara_Hospital region Ampara_District Ampara_Hospital is located in Ampara_District , Sri_Lanka . The capital of the country is Sri_Jayawardenepura_Kotte and the leader is Ranil_Wickremesinghe .
possessif United_States ethnicGroup White_Americans The Bacon_Explosion comes from the United_States where Barack_Obama is the leader . The capital of the country is Washington and the White_Americans are an ethnic group .
coordinated_clauses Bacon_sandwich alternativeName "Bacon_butty_,_bacon_sarnie_,_rasher_sandwich_,_bacon_sanger_,_piece_'n_bacon_,_bacon_cob_,_bacon_barm_,_bacon_muffin" A Bacon_sandwich , which contains Ketchup , can also be known as a "Bacon_butty_,_bacon_sarnie_,_rasher_sandwich_,_bacon_sanger_,_piece_'n_bacon_,_bacon_cob_,_bacon_barm_,_bacon_muffin" . It comes from the United_Kingdom and is a variation of the BLT .
coordinated_clauses Italy capital Rome Like Italians , Arrabbiata_sauce can be found in Italy . Where the capital is Rome and Sergio_Mattarella is one of the country ' s leaders .
direct_object Italy capital Rome Arrabbiata_sauce comes from Italy , where the leader is Pietro_Grasso , Rome is the capital and where Italians live .
direct_object Republic_of_Ireland language Irish_language Irish_language and English_language used in the Republic_of_Ireland by the Irish_people . The Republic is lead by Enda_Kenny and is the location of Adare_Manor .
direct_object Spain language Spanish_language Arròs_negre is a traditional dish from Spain , country of Spaniards and the Spanish_language . The leader is Felipe_VI_of_Spain .
coordinated_full_clauses ARA_Veinticinco_de_Mayo_(V-2) builder Cammell_Laird The ARA_Veinticinco_de_Mayo_(V-2) comes from Argentina , where Gabriela_Michetti is the leader and Argentines live . The ARA_Veinticinco_de_Mayo_(V-2) was built by Cammell_Laird .
coordinated_full_clauses Italy capital Rome Like Italians , Arrabbiata_sauce can be found in Italy . Where the capital is Rome and Sergio_Mattarella is one of the country ' s leaders .
passive_voice Alfredo_Zitarrosa deathPlace Montevideo Alfredo_Zitarrosa died in Montevideo in Uruguay . The leader of Uruguay is Raúl_Fernando_Sendic_Rodríguez and the people that live there are called Uruguayans .
relative_object United_States capital Washington Bacon_Explosion comes from the United_States , its capital city being Washington ; Paul_Ryan is a leader and one of its ethnic groups are African_Americans .
relative_adverb United_States capital Washington The country Bacon_Explosion comes from is the United_States . A political leader there is Paul_Ryan , the capital is Washington and White_Americans are one of the ethnic groups .
relative_adverb United_States capital Washington The United_States is the country of the Bacon_Explosion and Asian_Americans are one of the ethnic groups found there . John_Roberts is the leader and the capital is Washington .
relative_subject A_Wizard_of_Mars language English_language A_Wizard_of_Mars is written in English_language and was published in the United_States , where Barack_Obama is the President and Asian_Americans is one of the ethnic groups .
relative_subject United_States leaderTitle President_of_the_United_States Barack_Obama was the President_of_the_United_States . Other random facts about the United_States : 1634:_The_Ram_Rebellion was written there and Native_Americans are it ' s original ethnic group .
possessif United_States language English_language A_Wizard_of_Mars originates from the United_States . A country where the language is English_language and Barack_Obama is the leader . It is also where one of the ethnic groups is African_Americans .
coordinated_full_clauses United_States capital Washington Bacon_Explosion comes from the United_States where John_Roberts is a leader . The country ' s capital city is Washington and Asian_Americans are one of the ethnic groups of the United_States .
coordinated_full_clauses Arròs_negre region Valencian_Community Arròs_negre comes from the Valencian_Community region of Spain , the country where Felipe_VI_of_Spain is the leader and Spaniards are the ethnic group .
coordinated_full_clauses United_States capital Washington Bacon_Explosion comes from the United_States where Joe_Biden was once a leader and Washington is the capital . White_Americans are one ethnic group there .
relative_adverb A_Wizard_of_Mars language English_language Barack_Obama is the leader of the United_States where Native_Americans are one of the ethnic groups . It is also where A_Wizard_of_Mars , which is written in English_language , originates from .
relative_subject United_States capital Washington Paul_Ryan is a leader in the United_States , where Washington is the capital . Native_Americans are one of the ethnic groups . Bacon_Explosion also comes from the United_States .
direct_object United_States capital Washington Paul_Ryan is a leader in the United_States and the capital of the United_States is Washington . White_Americans are one of the ethnic groups in the United_States . The United_States is also the country of the Bacon_Explosion .
direct_object United_States capital Washington Bacon_Explosion comes from the United_States where Barack_Obama was once leader . The capital is Washington and White_Americans are one ethnic group .
direct_object United_States capital Washington The United_States is the country of the Bacon_Explosion and Asian_Americans are one of the ethnic groups found there . John_Roberts is the leader and the capital is Washington .
direct_object United_States capital Washington The United_States is the country of the Bacon_Explosion and the leader is Paul_Ryan . Asian_Americans are an ethnic group in the United_States and the capital is Washington .
direct_object Spain currency Euro Arròs_negre is a dish from Spain where the leader is Felipe_VI_of_Spain . Spaniards are an ethnic group of Spain and the currency used is the Euro .
direct_object United_States capital Washington Bacon_Explosion originated in the United_States , where Barack_Obama is leader , Washington is the capital and where Asian_Americans are an ethnic group .
coordinated_full_clauses A_Severed_Wasp language English_language The leader of the United_States is Barack_Obama where Asian_Americans is one of the ethnic groups and A_Severed_Wasp was written in the English_language .
coordinated_full_clauses United_States capital Washington 1634:_The_Ram_Rebellion was written in the United_States . The previous president there was Barack_Obama and the capital is Washington . One ethnic group there is African_Americans .
coordinated_full_clauses United_States capital Washington The United_States is the country of the Bacon_Explosion and the leader is Paul_Ryan . Asian_Americans are an ethnic group in the United_States and the capital is Washington .
relative_subject United_States capital Washington Bacon_Explosion comes from the United_States , the capital of which is Washington . Barack_Obama is leader of the United_States and African_Americans are an ethnic group there .
relative_subject Spain currency Euro Ajoblanco originates from the country of Spain ( country of Spaniards ). It used the Euro as currency and the leader is Felipe_VI_of_Spain .
passive_voice United_States capital Washington Bacon_Explosion comes from the United_States where Joe_Biden is one of the leaders . Native_Americans are an ethnic group there and the capital is Washington .
possessif Spain language Spanish_language Arròs_negre is a dish from Spain where they speak Spanish_language and the people are called Spaniards . Felipe_VI_of_Spain is the leader .
direct_object Arròs_negre region Valencian_Community Arròs_negre comes from the Valencian_Community region of Spain , the country where Felipe_VI_of_Spain is the leader and Spaniards are the ethnic group .
apposition United_States capital Washington Bacon_Explosion comes from the United_States where Barack_Obama was once leader . The capital is Washington and White_Americans are one ethnic group .
apposition United_States capital Washington Paul_Ryan is a leader in the United_States , where Washington is the capital . Native_Americans are one of the ethnic groups . Bacon_Explosion also comes from the United_States .
apposition Ajoblanco region Andalusia Ajoblanco is from Andalusia in Spain . Felipe_VI_of_Spain is the leader there and Spaniards are an ethnic group there as well .
relative_adverb A_Severed_Wasp language English_language A_Severed_Wasp originates from the United_States and is written in English_language . The leader of the United_States is Barack_Obama and one of the ethnic groups in the country are the Asian_Americans .
passive_voice Spain language Spanish_language Arròs_negre is a dish from Spain where they speak Spanish_language and the people are called Spaniards . Felipe_VI_of_Spain is the leader .
relative_subject Bandeja_paisa ingredient Avocado Bandeja_paisa is a typical Colombian_cuisine from the Paisa_Region . The main ingredients are "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" , Avocado and lemon .
relative_subject Binignit ingredient Sweet_potato Binignit comes from the Visayas region of the Philippines . Its main ingredients are Sweet_potato and Coconut_milk .
relative_subject Asam_pedas alternativeName "Asam_padeh" Asam_pedas is found in the region of "Sumatra_and_Malay_Peninsula" in Malaysia . It can also be called "Asam_padeh" and it ' s ingredients are "Fish_cooked_in_sour_and_hot_sauce" .
passive_voice Asam_pedas alternativeName "Asam_padeh" Asam_pedas ( aka "Asam_padeh" ) is from the "Sumatra_and_Malay_Peninsula" regions of Malaysia . The main ingredients are "Fish_cooked_in_sour_and_hot_sauce" .
passive_voice Bandeja_paisa ingredient Lemon The main ingredients of Colombian_cuisine Bandeja_paisa are "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" and Lemon . The dish originates from the Paisa_Region .
apposition Arròs_negre ingredient Cephalopod_ink Arròs_negre is a traditional dish from the Catalonia region of Spain . The main ingredients of Arròs_negre are "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" , cuttlefish or squid , Cephalopod_ink , cubanelle peppers .
apposition Bandeja_paisa ingredient Lemon Bandeja_paisa comes from the Paisa_Region and part of the Colombian_cuisine . The main ingredients of the food are : "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" and Lemon .
apposition Beef_kway_teow ingredient Sesame_oil Beef_kway_teow originates from Singapore and Indonesia . The main ingredients of the dish are : "Kway_teow_,_beef_tender_loin_,_gula_Melaka_,_sliced_,_dried_black_beans_,_garlic_,_dark_soy_sauce_,_lengkuas_,_oyster_sauce_,_soya_sauce_,_chilli_and_sesame_oil" and Sesame_oil .
possessif Arròs_negre ingredient Cephalopod_ink Arròs_negre is from the region of Catalonia in Spain . The main ingredients of Arròs_negre are "White_rice_,_cuttlefish_or_squid_,_cephalopod_ink_,_cubanelle_peppers" , cuttlefish or squid , Cephalopod_ink , cubanelle peppers .
possessif Bandeja_paisa ingredient Avocado Bandeja_paisa is from the Paisa_Region and part of the Colombian_cuisine . The main ingredients of Bandeja_paisa are : "red_beans_,_pork_belly_,_white_rice_,_ground_meat_,_chicharon_,_fried_egg_,_plantain_(patacones)_,_chorizo_,_arepa_,_hogao_sauce_,_black_pudding_(morcilla)_,_avocado_and_lemon" , Avocado and lemon .
possessif Binignit ingredient Sweet_potato Binignit is a dish from the region of Visayas in the Philippines . It includes Sweet_potato and Banana .
possessif Binignit ingredient Sago Binignit is a dish from the region of Visayas , Philippines . The main ingredients are Sweet_potato and Sago .
coordinated_clauses Singapore leaderName Halimah_Yacob Ayam_penyet is a dish from the region of Singapore which is lead by Halimah_Yacob . It is also found in Java where the Baduy are an ethnic group .
coordinated_clauses Singapore language Standard_Chinese The Javanese_people are an ethnic group in Java , where the dish Ayam_penyet can be found . The dish is from Singapore , where Standard_Chinese is spoken .
coordinated_full_clauses Ayam_penyet ingredient Fried_chicken Fried_chicken is an ingredient in Ayam_penyet and originates from Singapore . It can also be found in Java where the Banyumasan_people are an ethnic group .
coordinated_full_clauses Singapore leaderName Halimah_Yacob The Banyumasan_people are an ethnic group in Java , where the dish Ayam_penyet can be found . The dish comes from Singapore , where Halimah_Yacob is a leader .
relative_adverb Valencian_Community leaderName Ximo_Puig Arròs_negre comes from the region of the Valencian_Community ( leader Ximo_Puig ) , Spain ( leader Felipe_VI_of_Spain ). Spaniards are the population name there .
relative_adverb Ayam_penyet ingredient Fried_chicken The Fried_chicken dish Ayam_penyet is a popular dish in Malaysia and Java . The Javanese_people are an ethnic group of Java .
relative_adverb Spain leaderName Felipe_VI_of_Spain Ajoblanco is a dish of Andalusia and Spain ( led by Felipe_VI_of_Spain and inhabited by Spaniards ).
direct_object Ayam_penyet ingredient Fried_chicken Ayam_penyet , a Fried_chicken dish , is from Malaysia and Java ( country of the Javanese_people ).
relative_adverb Valencian_Community leaderName Ximo_Puig Arròs_negre comes from the region of the Valencian_Community , Spain . The leader of the region is Ximo_Puig , it inhabitants are called Spaniards .
apposition Singapore leaderName Halimah_Yacob The Banyumasan_people are an ethnic group in Java , where the dish Ayam_penyet can be found . The dish comes from Singapore , where Halimah_Yacob is a leader .
relative_adverb Singapore leaderName Tony_Tan Ayam_penyet is originates from Singapore and Tony_Tan is the country ' s leader . It can also be found in Java where Baduy people are one of the ethnic groups .
apposition Singapore language English_language Ayam_penyet comes from Singapore and Java . English_language is spoken in Singapore . Baduy are an ethnic group living in Java .
possessif Spain leaderName Felipe_VI_of_Spain Arròs_negre comes from the Valencian_Community region of Spain , the country where Felipe_VI_of_Spain is the leader and Spaniards are the ethnic group .
apposition Bacon_Explosion mainIngredients Sausage The United_States is the country of the Bacon_Explosion , it is a "Main_course" which includes Bacon and Sausage .
passive_voice Bionico region Guadalajara Bionico is from the Guadalajara region of Mexico . It is a Dessert that includes Granola .
passive_voice Bionico region Jalisco Using one of the required ingredients , Granola , Bionico is a dish served for Dessert and is found in the region of Jalisco , in Mexico .
passive_voice Mexico leaderName Enrique_Peña_Nieto Ice_cream Bionico are types of Dessert . Enrique_Peña_Nieto is a leader in Mexico where they eat Bionico .
direct_object Mexico leaderName Silvano_Aureoles_Conejo Sandesh_(confectionery) and Bionico are served as a Dessert course . The latter dish is found in Mexico where the leader is Silvano_Aureoles_Conejo .
direct_object Philippines language Philippine_English Cookie and Binignit are types of Dessert . The latter dish can be found in the Philippines where the spoken language is known as Philippine_English .
apposition United_States ethnicGroup Native_Americans Baked_Alaska is a dish from the United_States and Sandesh_(confectionery) is a Dessert . Native_Americans in the United_States are one of the ethnic groups of the country .
coordinated_full_clauses France leaderName Manuel_Valls Baked_Alaska , from France and Cookie are types of Dessert . Manuel_Valls leads France .
coordinated_full_clauses Mexico leaderName Silvano_Aureoles_Conejo Bionico and Cookie are Dessert . Bionico originates from Mexico , where the leader is Silvano_Aureoles_Conejo .
relative_subject Binignit mainIngredients Banana Binignit is a Dessert , whose ingredients include Banana and Sweet_potato . Sandesh_(confectionery) is a nice confectionary Dessert .
juxtaposition Sweet_potato order Solanales Cookie and Binignit are types of Dessert . One of the ingrediens in Binignit is Sweet_potato which belongs to the order Solanales .
passive_voice Sweet_potato order Solanales Sandesh_(confectionery) is a Dessert confection . Binignit is a Dessert that includes Sweet_potato ( a member of the Solanales order ).
relative_subject Derbyshire_Dales isPartOf Derbyshire Bakewell_tart contains Fruit_preserves and comes from Derbyshire_Dales , Derbyshire . It is a variation of Bakewell_pudding .
apposition Derbyshire_Dales isPartOf Derbyshire The Bakewell_tart is popular in the Derbyshire_Dales area which is part of Derbyshire . It ' s a variant of Bakewell_pudding and uses Frangipane .
coordinated_clauses Bakewell_pudding served "Warm__or_cold" Bakewell_pudding is a "Dessert" that can be served "Warm__or_cold" . It comes from the Derbyshire_Dales region and the main ingredients are "Ground_almond_,_jam_,_butter_,_eggs" .
juxtaposition Derbyshire_Dales isPartOf Derbyshire Bakewell_pudding is from the Derbyshire_Dales region , which is part of Derbyshire . Bakewell_tart is it ' s variant , but has Frangipane as an ingredient .
relative_subject Derbyshire_Dales leaderName Patrick_McLoughlin Bakewell_tart contains Fruit_preserves and is a variation of Bakewell_pudding from the Derbyshire_Dales region lead by Patrick_McLoughlin .
coordinated_clauses Bakewell_pudding dishVariation Bakewell_tart Bakewell_pudding , which can be served "Warm_(freshly_baked)_or_cold" or cold , is from the Derbyshire_Dales region . Some main ingredients in it are "Ground_almond_,_jam_,_butter_,_eggs" . A variant of Bakewell_pudding is Bakewell_tart .
relative_subject Indonesia leaderName Jusuf_Kalla Bakso is a food originates from Indonesia and contains Tapioca . The country ' s leader is Jusuf_Kalla .
direct_object Batagor country Indonesia Peanut_sauce is an ingredient of Batagor which is a variation of Shumai or Siomay and hails from Indonesia .
relative_subject Sweet_potato division Flowering_plant Sweet_potato ( belonging to Flowering_plant and of the Solanales order ) and Taro are ingredients in Binignit .
direct_object Sweet_potato division Flowering_plant Sago and Sweet_potato are ingredients in Binignit . Sweet_potato belongs to the Solanales order of Flowering_plant .
apposition Sweet_potato division Flowering_plant Sweet_potato ( belonging to Flowering_plant and of the Solanales order ) and Taro are ingredients in Binignit .
relative_subject Sweet_potato division Flowering_plant The main ingredients of Binignit are Sweet_potato ( Flowering_plant ; a Solanales ) and Coconut_milk .
relative_subject Sweet_potato division Flowering_plant Binignit includes the ingredients of Sago and Sweet_potato , the latter of which is part of the order Solanales and is a Flowering_plant .
relative_subject Tomato order Solanales Tomato is an ingredient in Amatriciana_sauce , it comes from the Solanaceae family of plants and belongs to the genus Solanum and the order Solanales .
direct_object Aaron_S._Daggett deathPlace West_Roxbury American_Civil_War soldier and Purple_Heart recipient Aaron_S._Daggett , was born in Maine and died in West_Roxbury .
relative_subject Ab_Klink office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Ab_Klink studied at the Erasmus_University_Rotterdam . He was born in Stellendam , worked as a office workedAt workedAsObjet1 and belonged to the Christian_Democratic_Appeal party .
relative_subject Ab_Klink office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Leiden_University is where Stellendam born Ab_Klink studied . He belongs to the Christian_Democratic_Appeal party and worked as a office workedAt workedAsObjet1 .
passive_voice Ab_Klink office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Stellendam was the birthplace of Ab_Klink , who belonged to the Christian_Democratic_Appeal party . Ab_Klink worked at the office workedAt workedAsObjet1 and attended , Erasmus_University_Rotterdam .
direct_object Ab_Klink party Christian_Democratic_Appeal Stellendam was the birthplace of Ab_Klink , who belongs to the Christian_Democratic_Appeal party . The alma mater of Ab_Klink is Erasmus_University_Rotterdam , which is affiliated to Association_of_MBAs .
possessif Ab_Klink birthPlace Stellendam Stellendam born Ab_Klink , who is a office workedAt workedAsObjet1 , studied at Erasmus_University_Rotterdam , which is affiliated to the Association_of_MBAs .
coordinated_full_clauses Ab_Klink birthPlace Netherlands Ab_Klink , who worked as a office workedAt workedAsObjet1 , was born in the Netherlands and attended Erasmus_University_Rotterdam which is affiliated with the Association_of_MBAs .
apposition Abdul_Taib_Mahmud birthPlace Kingdom_of_Sarawak Abdul_Taib_Mahmud , born in the Kingdom_of_Sarawak , was succeeded by Abdul_Rahman_Ya'kub ( in office during Vice President Tuanku_Bujang_Tuanku_Othman ' s rule ). Ya ' Kub ' s deputy was Stephen_Yong_Kuet_Tze .
relative_object Abdul_Taib_Mahmud successor Sulaiman_Abdul_Rahman_Taib Abdul_Taib_Mahmud was born in the Kingdom_of_Sarawak and lives in Kuching . His party is the Parti_Pesaka_Bumiputera_Bersatu and he was succeeded by Sulaiman_Abdul_Rahman_Taib .
coordinated_clauses Abdul_Taib_Mahmud successor Abdul_Rahman_Ya'kub Abdul_Taib_Mahmud was born in Miri_,_Malaysia and lived in Kuching . He belonged to the Parti_Pesaka_Bumiputera_Bersatu . Abdul_Rahman_Ya'kub was his successor .
existential Abdul_Taib_Mahmud successor Adenan_Satem Abdul_Taib_Mahmud was born in Miri_,_Malaysia and resides at "Demak_Jaya_,_Jalan_Bako_,_Kuching_,_Sarawak" . His party is called the Parti_Pesaka_Bumiputera_Bersatu and he was succeeded by Adenan_Satem .
relative_subject Abdul_Taib_Mahmud successor Sulaiman_Abdul_Rahman_Taib Abdul_Taib_Mahmud was born in the Kingdom_of_Sarawak , resides in Kuching , belongs to the Parti_Pesaka_Bumiputera_Bersatu and was succeeded by , Sulaiman_Abdul_Rahman_Taib .
passive_voice Abdul_Taib_Mahmud region Balingian Abdul_Taib_Mahmud , born in Miri_,_Malaysia , resides in Sarawak he represented Balingian and his party is the Parti_Pesaka_Bumiputera_Bersatu .
apposition Abdul_Taib_Mahmud region Asajaya Abdul_Taib_Mahmud was born in Miri_,_Malaysia and lives in Sarawak . He represented Asajaya and belonged to the Parti_Pesaka_Bumiputera_Bersatu .
coordinated_clauses Erasmus_University_Rotterdam affiliation Association_of_MBAs Stellendam born Ab_Klink studied at the Erasmus_University_Rotterdam , which is affiliated to the Association_of_MBAs . He worked at the office workedAt workedAsObjet1 .
relative_subject Ab_Klink party Christian_Democratic_Appeal Ab_Klink was born in the Netherlands and attended Erasmus_University_Rotterdam . He belongs to the Christian_Democratic_Appeal party and worked as a office workedAt workedAsObjet1 .
relative_subject Agnes_Kant residence Doesburg Agnes_Kant , a office workedAt workedAsObjet1 , was born in Hessisch_Oldendorf , lived in Doesburg and studied at the Radboud_University_Nijmegen .
direct_object Ab_Klink party Christian_Democratic_Appeal Leiden_University is where Stellendam born Ab_Klink studied . He belongs to the Christian_Democratic_Appeal party and worked as a office workedAt workedAsObjet1 .
relative_object Agnes_Kant residence Netherlands Agnes_Kant was born in Hessisch_Oldendorf and lived in the Netherlands . His alma mater was the Radboud_University_Nijmegen and he worked as a office workedAt workedAsObjet1 .
relative_subject Agnes_Kant residence Doesburg Agnes_Kant was born in Hessisch_Oldendorf and lived in Doesburg . He attended the Radboud_University_Nijmegen and was a office workedAt workedAsObjet1 .
juxtaposition Ab_Klink party Christian_Democratic_Appeal Born in Stellendam , Ab_Klink , belongs to the Christian_Democratic_Appeal party . He worked as a office workedAt workedAsObjet1 and his Alma mater is Erasmus_University_Rotterdam .
coordinated_full_clauses Ab_Klink party Christian_Democratic_Appeal Ab_Klink , was born in Stellendam and belongs to the Christian_Democratic_Appeal party . He worked at the office workedAt workedAsObjet1 and his Alma mater is Erasmus_University_Rotterdam .
existential Ab_Klink party Christian_Democratic_Appeal Ab_Klink worked as a office workedAt workedAsObjet1 as a member of Christian_Democratic_Appeal party , was born in the Netherlands and went Erasmus_University_Rotterdam .
existential Alfons_Gorbach deathPlace Styria A member of the Austrian_People's_Party , Alfons_Gorbach was born in Imst , in the state of Tyrol_(state) and died in Graz , in Styria .
apposition Alfons_Gorbach deathPlace Graz Alfons_Gorbach was born in Imst in the County of Tyrol_(state) . He died in Graz , Austria and during his life was a member of the Austrian_People's_Party .
apposition Abel_Caballero office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Abel_Caballero , who was born in Galicia_(Spain) Spain , worked at the office workedAt workedAsObjet1 and belongs to the Spanish_Socialist_Workers'_Party .
coordinated_clauses Abel_Caballero party Spanish_Socialist_Workers'_Party Abel_Caballero was born in Galicia_(Spain) in Spain , is a member of the Spanish_Socialist_Workers'_Party and worked as office workedAt workedAsObjet1 .
existential Adonis_Georgiadis successor Makis_Voridis Adonis_Georgiadis was born in Athens , Greece and worked in the office workedAt workedAsObjet1 before being succeeded by Makis_Voridis .
direct_object Abel_Caballero party Spanish_Socialist_Workers'_Party Abel_Caballero was born in Spain in Ponteareas and was the office workedAt workedAsObjet1 . He was also a member of the Spanish_Socialist_Workers'_Party .
direct_object Abel_Caballero inOfficeWhilePrimeMinister Felipe_González Abel_Caballero was in office while Felipe_González was Prime Minister , which was during the reign of Monarch , Juan_Carlos_I_of_Spain . Francisco_Franco was the predecessor of Juan_Carlos_I_of_Spain , who was born in Rome .
apposition Abraham_A._Ribicoff spouse "Ruth_Ribicoff" Native_Americans are an ethnic group in the United_States where in New_Britain_,_Connecticut , Abraham_A._Ribicoff was born . United_States . Abraham_A._Ribicoff ' s spouse is "Ruth_Ribicoff" .
relative_adverb Abraham_A._Ribicoff spouse Casey_Ribicoff Abraham_A._Ribicoff , married to Casey_Ribicoff , was born in Connecticut in the United_States . One of the ethnic groups in the United_States is the Native_Americans .
relative_adverb Abraham_A._Ribicoff spouse "Ruth_Ribicoff" Abraham_A._Ribicoff , who was married to "Ruth_Ribicoff" , was born in the United_States where Native_Americans are one of the ethnic groups .
coordinated_clauses Alfred_N._Phillips party Democratic_Party_(United_States) Connecticut born Alfred_N._Phillips was a member of Democratic_Party_(United_States) and served as the office workedAt workedAsObjet1 . He was succeeded by Albert_E._Austin .
direct_object Abdulsalami_Abubakar almaMater Technical_Institute_,_Kaduna Abdulsalami_Abubakar , who was born in Minna and studied at the Technical_Institute_,_Kaduna , served as the office workedAt workedAsObjet1 . He was succeeded by Al-Amin_Daggash .
direct_object Abraham_A._Ribicoff deathPlace New_York_City Abraham_A._Ribicoff , who was born in the United_States and died in New_York_City , worked as the office workedAt workedAsObjet1 and was succeeded by John_N._Dempsey .
direct_object Alberto_Teisaire profession "Rear_Admiral_in_the_Argentine_Navy" Alberto_Teisaire , who was born in Mendoza_,_Argentina and served as a "Rear_Admiral_in_the_Argentine_Navy" , worked as a office workedAt workedAsObjet1 and was succeeded by Isaac_Rojas .
coordinated_full_clauses Abraham_A._Ribicoff deathPlace United_States Abraham_A._Ribicoff was born in New_Britain_,_Connecticut and died in the United_States . He once worked for the office workedAt workedAsObjet1 office where he was succeeded by Anthony_J._Celebrezze .
relative_adverb Abraham_A._Ribicoff deathPlace New_York_City Abraham_A._Ribicoff , who was born in Connecticut and died in New_York_City , once worked for the office workedAt workedAsObjet1 office and was succeeded by John_N._Dempsey .
existential Abraham_A._Ribicoff deathPlace United_States office workedAt workedAsObjet1 member Abraham_A._Ribicoff was born in Connecticut and died in the United_States . He was succeeded by Anthony_J._Celebrezze .
existential Abraham_A._Ribicoff deathPlace New_York_City Abraham_A._Ribicoff was born in the United_States and died in New_York_City . He worked as the office workedAt workedAsObjet1 and was succeeded by Anthony_J._Celebrezze .
relative_subject Abraham_A._Ribicoff deathPlace New_York_City Abraham_A._Ribicoff ' s place of birth was Connecticut and later passed in New_York_City . He worked as the office workedAt workedAsObjet1 and was succeeded by Anthony_J._Celebrezze .
relative_subject Abner_Sibal party Republican_Party_(United_States) Abner_Sibal was born in Ridgewood_,_Queens and was a member of the Republican_Party_(United_States) . He was also a office workedAt workedAsObjet1 and was succeeded by Marjorie_Farmer .
relative_subject Abraham_A._Ribicoff deathPlace United_States Abraham_A._Ribicoff was born in Connecticut , he died in the United_States . He worked under the office workedAt workedAsObjet1 . John_N._Dempsey was preceded by him .
relative_subject Abraham_A._Ribicoff deathPlace New_York_City Abraham_A._Ribicoff was born in the United_States and died in New_York_City . He worked as the office workedAt workedAsObjet1 and was succeeded by Anthony_J._Celebrezze .
relative_subject Alfred_N._Phillips party Democratic_Party_(United_States) Alfred_N._Phillips was born in Darien_,_Connecticut and belonged to Democratic_Party_(United_States) . He worked at the office workedAt workedAsObjet1 and was succeeded by Albert_E._Austin .
possessif Alberto_Teisaire party Justicialist_Party Alberto_Teisaire was born in Mendoza_,_Argentina and was a member of the Justicialist_Party . He worked as the office workedAt workedAsObjet1 and was succeeded by Isaac_Rojas .
possessif Alfred_N._Phillips party Democratic_Party_(United_States) Alfred_N._Phillips was born in Darien_,_Connecticut and was a member of Democratic_Party_(United_States) . He worked at the office workedAt workedAsObjet1 where he was succeeded by Albert_E._Austin .
coordinated_full_clauses Alberto_Teisaire profession Rear_admiral Isaac_Rojas was the successor to Alberto_Teisaire , who was born in Mendoza_,_Argentina . Alberto_Teisaire was a Rear_admiral by profession and worked as a office workedAt workedAsObjet1 .
relative_subject Alberto_Teisaire profession Rear_admiral Alberto_Teisaire was born in Mendoza_,_Argentina and he was a Rear_admiral . He worked as a office workedAt workedAsObjet1 and his successor was Isaac_Rojas .
possessif Abdulsalami_Abubakar almaMater Technical_Institute_,_Kaduna Abdulsalami_Abubakar , who was born in Minna and studied at the Technical_Institute_,_Kaduna , was the office workedAt workedAsObjet1 . He was succeeded by Al-Amin_Daggash .
coordinated_full_clauses Abraham_A._Ribicoff deathPlace United_States Born in Connecticut , Abraham_A._Ribicoff worked as the office workedAt workedAsObjet1 , was succeeded by John_N._Dempsey and died in the United_States .
coordinated_full_clauses Abraham_A._Ribicoff deathPlace New_York_City Abraham_A._Ribicoff was born in the United_States and died in New_York_City . He worked as the office workedAt workedAsObjet1 and was succeeded by Anthony_J._Celebrezze .
apposition Abraham_A._Ribicoff deathPlace New_York_City Abraham_A._Ribicoff was born in Connecticut and died in New_York_City . He worked as the office workedAt workedAsObjet1 and was succeeded by Anthony_J._Celebrezze .
apposition Abraham_A._Ribicoff deathPlace New_York_City Abraham_A._Ribicoff , who was born in Connecticut and died in New_York_City , once worked for the office workedAt workedAsObjet1 office and was succeeded by John_N._Dempsey .
apposition Alfred_N._Phillips party Democratic_Party_(United_States) Alfred_N._Phillips was born in Connecticut and went on to become office workedAt workedAsObjet1 there . He was also a member of Democratic_Party_(United_States) until he was succeeded by Albert_E._Austin .
apposition Alvah_Sabin birthDate 1793-10-23 Alvah_Sabin was born in Georgia_,_Vermont 1793-10-23 . He worked as the office workedAt workedAsObjet1 and was succeeded by Homer_Elihu_Royce .
direct_object Abraham_A._Ribicoff deathPlace New_York_City Abraham_A._Ribicoff was from and was born in the United_States , where African_Americans are an ethnic group . Casey_Ribicoff was the wife of Abraham_A._Ribicoff , who died in New_York_City .
possessif Adam_Holloway almaMater Magdalene_College_,_Cambridge Kent ( United_Kingdom ) born Adam_Holloway is in the Conservative_Party_(UK) ( United_Kingdom ) and is a graduate of Magdalene_College_,_Cambridge .
relative_adverb New_York language French_language Born in New_York city ( where French_language is one of the languages spoken ) , Albert_Jennings_Fountain battled in the American_Civil_War . The same conflict in which Abraham_Lincoln was a commander .
coordinated_clauses United_States ethnicGroup African_Americans Albert_Jennings_Fountain , who was born in the United_States , where African_Americans are an ethnic group . He fought in the American_Civil_War and a commander in this war was Jefferson_Davis .
coordinated_clauses United_States ethnicGroup African_Americans Albert_Jennings_Fountain was born in the United_States where African_Americans are an ethnic group . He fought in the American_Civil_War which Abraham_Lincoln also participated in as a commander .
coordinated_clauses New_York language French_language Jefferson_Davis was a commander in the American_Civil_War , which Albert_Jennings_Fountain fought in . Albert_Jennings_Fountain was born in New_York , where French_language is one of the languages spoken .
relative_adverb Albert_Jennings_Fountain deathPlace Doña_Ana_County_,_New_Mexico Albert_Jennings_Fountain was born in New_York and he died in Doña_Ana_County_,_New_Mexico . He fought fought in the American_Civil_War which was where Abraham_Lincoln was a commander .
relative_adverb New_York language French_language French_language is a language spoken in New_York which is where Albert_Jennings_Fountain was born . He battled in the American_Civil_War , the conflict which Abraham_Lincoln was a commander in .
relative_subject United_States ethnicGroup African_Americans Albert_Jennings_Fountain was born in the United_States where African_Americans are an ethnic group . He fought in the American_Civil_War which Abraham_Lincoln also participated in as a commander .
direct_object Adam_Koc deathPlace New_York_City Adam_Koc , who died in New_York_City , fought in the Polish–Soviet_War - one of the commanders there was Leon_Trotsky . The Polish–Soviet_War was part of the Russian_Civil_War .
relative_object Adam_Koc birthPlace Congress_Poland Adam_Koc , born in Congress_Poland , fought in the Polish–Soviet_War which was part of the Russian_Civil_War . Leon_Trotsky was a commander in this war .
apposition Makis_Voridis birthPlace Athens Makis_Voridis was born in Athens who was the predecessor of Adonis_Georgiadis . Adonis_Georgiadis was in office while Antonis_Samaras was Prime Minister and Antonis_Samaras was in office while Konstantinos_Mitsotakis was Prime Minister .
passive_voice Netherlands currency Euro Agnes_Kant is a national of the Netherlands where the currency is the Euro . Emile_Roemer is a member and leader of the Socialist_Party_(Netherlands) ( Netherlands ).
direct_object Agnes_Kant office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Agnes_Kant , a Netherlands National , worked at the office workedAt workedAsObjet1 . She is a member of the Socialist_Party_(Netherlands) , the leader of which is , Mark_Rutte .
relative_object Ab_Klink birthPlace Stellendam The currency of The Netherlands is the Euro and the leader is Mark_Rutte . Ab_Klink was born in Stellendam and is a national of the Netherlands .
passive_voice Airey_Neave birthPlace "Knightsbridge_,_London" Airey_Neave , born in "Knightsbridge_,_London" , began his career 1974-03-04 , fought in World_War_II and ended his career 1979-03-30 .
relative_subject Airey_Neave birthPlace Knightsbridge Knightsbridge born Airey_Neave fought in World_War_II . His career began 1953-06-30 and ended 1979-03-30 .
relative_subject Abner_Sibal office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Abner_Sibal was born in Ridgewood_,_Queens and died in Alexandria_,_Virginia . He was a office workedAt workedAsObjet1 before being succeeded by Donald_Irwin .
relative_subject Abraham_A._Ribicoff office_(workedAt_,_workedAs) office workedAt workedAsObjet1 Born in Connecticut , Abraham_A._Ribicoff worked as the office workedAt workedAsObjet1 , was succeeded by John_N._Dempsey and died in the United_States .
relative_object Parkersburg_,_West_Virginia country United_States William_M._O._Dawson was born in Bloomington_,_Maryland in the United_States . His successor was Albert_B._White who passed away in Parkersburg_,_West_Virginia in the United_States .
relative_subject Alfred_N._Phillips successor Albert_E._Austin Connecticut born Alfred_N._Phillips was a member of Democratic_Party_(United_States) and served as the office workedAt workedAsObjet1 . He was succeeded by Albert_E._Austin .
apposition Ab_Klink almaMater Erasmus_University_Rotterdam Netherlands born , Ab_Klink attended Erasmus_University_Rotterdam , is a member of Christian_Democratic_Appeal party and is a office workedAt workedAsObjet1 .
existential Alfred_N._Phillips successor Albert_E._Austin Alfred_N._Phillips was born in Darien_,_Connecticut and belonged to Democratic_Party_(United_States) . He worked at the office workedAt workedAsObjet1 and was succeeded by Albert_E._Austin .
existential Alberto_Teisaire successor Isaac_Rojas Alberto_Teisaire was born in Mendoza_,_Argentina and served as a "Rear_Admiral_in_the_Argentine_Navy" . He worked as the office workedAt workedAsObjet1 and was succeeded by Isaac_Rojas .
apposition Alberto_Teisaire birthDate 1891-05-20 Alberto_Teisaire was born in Mendoza_,_Argentina 1891-05-20 and worked as a Rear_admiral , later succeeded by Isaac_Rojas .
existential Alberto_Teisaire office_(workedAt_,_workedAs) office workedAt workedAsObjet1 office workedAt workedAsObjet1 and Rear_admiral Alberto_Teisaire was born in Mendoza_,_Argentina . he was succeeded by Isaac_Rojas .
relative_subject Aaron_S._Daggett award Purple_Heart The Battle_of_Gettysburg took place during the American_Civil_War when Robert_E._Lee was a commander . Aaron_S._Daggett fought at the Battle_of_Gettysburg and was awarded the Purple_Heart .
relative_object Alfred_Moore_Scales activeYearsStartDate 1875-03-04 American_Civil_War , Battle_of_Chancellorsville participant Alfred_Moore_Scales began his career 1875-03-04 . Abraham_Lincoln was an American_Civil_War commander .
apposition Birmingham postalCode B_postcode_area The B_postcode_area is the postal code of Birmingham which is lead by the Conservative_Party_(UK) . The city is the birthplace of the architect John_Madin who designed 103_Colmore_Row .
existential 200_Public_Square floorArea 111484_square_metres 200_Public_Square in "Cleveland_,_Ohio_44114" , Ohio was completed in 1985 and has 45 floors and measures 111484_square_metres .
coordinated_clauses 300_North_LaSalle architect Pickard_Chilton 300_North_LaSalle in Chicago was completed in 2009 has 60 floors and its designer is Pickard_Chilton .
direct_object 103_Colmore_Row architect John_Madin 103_Colmore_Row in Birmingham was completed in 1976 with 23 floors and was designed by the architect John_Madin .
juxtaposition 200_Public_Square floorArea 111484_square_metres 200_Public_Square , with 45 floors covering 111484_square_metres , is located in Cleveland and was completed in 1985 .
relative_subject 200_Public_Square floorArea 111484_square_metres 200_Public_Square , with 45 floors covering 111484_square_metres , is located in Cleveland and was completed in 1985 .
relative_subject 300_North_LaSalle floorArea 120770_square_metres 300_North_LaSalle in Illinois was completed in 2009 and has 60 floors with a total area of 120770_square_metres .
relative_subject 103_Colmore_Row architect John_Madin Designed by architect John_Madin , 103_Colmore_Row was completed in 1976 , is comprised of 23 floors and is located at Colmore_Row .
apposition Alan_B._Miller_Hall currentTenants Mason_School_of_Business Alan_B._Miller_Hall was completed on "1_June_2009" in Virginia , United_States . The current tenants are the Mason_School_of_Business .
existential 300_North_LaSalle location Chicago Pickard_Chilton designed 300_North_LaSalle , Chicago , which has 60 floors and was completed in 2009 .
coordinated_full_clauses London leaderTitle European_Parliament London , the capital of the United_Kingdom is lead via the European_Parliament and Elizabeth_II . 20_Fenchurch_Street is located within the United_Kingdom .
relative_object South_Africa ethnicGroup Coloured 11_Diagonal_Street is located in South_Africa , which has Cape_Town as its capital and Cyril_Ramaphosa as leader . Coloured people are an ethic group of this country .
passive_voice South_Africa ethnicGroup Asian_South_Africans One of the ethnic groups of South_Africa , where the leader is Jacob_Zuma , are the Asian_South_Africans . The capital city is Cape_Town and 11_Diagonal_Street is located within the country .
coordinated_full_clauses Azerbaijan leaderTitle Prime_Minister_of_Azerbaijan The Baku_Turkish_Martyrs'_Memorial is located in Baku , the capital of Azerbaijan , where the leader ( Prime_Minister_of_Azerbaijan ) is called Artur_Rasizade .
coordinated_full_clauses South_Africa ethnicGroup White_South_Africans 11_Diagonal_Street is located in South_Africa ( led by Jacob_Zuma ) , where Cape_Town is the capital and White_South_Africans are an ethnic group .
relative_adverb South_Africa capital Cape_Town Cyril_Ramaphosa is one of the leaders of South_Africa , which capital is Cape_Town . The address , 11_Diagonal_Street is located in that country and one of the the ethnic groups is Asian_South_Africans .
apposition South_Africa capital Cape_Town 11_Diagonal_Street is located in South_Africa , Cape_Town and the leader is Jacob_Zuma of the White_South_Africans .
direct_object 200_Public_Square location Public_Square_,_Cleveland Completed in 1985 with 45 floors and 111484_square_metres floor area , 200_Public_Square can be found in Public_Square_,_Cleveland .
relative_subject 200_Public_Square location "Cleveland_,_Ohio_44114" Completed in 1985 , 200_Public_Square has 45 floors and 111484_square_metres space and can be found in "Cleveland_,_Ohio_44114" .
relative_subject 300_North_LaSalle location Illinois 300_North_LaSalle in Illinois was completed in 2009 and has 60 floors with a total area of 120770_square_metres .
coordinated_full_clauses Cleveland isPartOf Ohio 200_Public_Square is located in Cleveland , Ohio , United_States and the governing body of Cleveland is Cleveland_City_Council .
relative_adverb Cleveland isPartOf Cuyahoga_County_,_Ohio Part of the Cuyahoga_County_,_Ohio if the United_States , Cleveland has the leader Frank_G._Jackson and is the location of the 200_Public_Square .
relative_adverb Chicago isPartOf Illinois 300_North_LaSalle is located in Chicago , Illinois , United_States . The leader of Chicago is Rahm_Emanuel .
direct_object Cleveland isPartOf Cuyahoga_County_,_Ohio 200_Public_Square is in Cleveland , United_States , which is part of Cuyahoga_County_,_Ohio . Frank_G._Jackson is the leader of Cleveland .
relative_adverb London leaderTitle European_Parliament Boris_Johnson is the leader in London . 20_Fenchurch_Street has 34 floors and is located in London . London is lead to the European_Parliament .
passive_voice United_States language English_language 250_Delaware_Avenue is located in the United_States , where English_language is the main language and Paul_Ryan is a leader alongside the President_of_the_United_States .
apposition 20_Fenchurch_Street floorCount 34 Boris_Johnson is the leader in London . 20_Fenchurch_Street has 34 floors and is located in London . London is lead to the European_Parliament .
apposition 250_Delaware_Avenue cost "110_million_(dollars)" 250_Delaware_Avenue in Buffalo_,_New_York cost "110_million_(dollars)" and has 12 floors with a total area of 30843.8_square_metres .
apposition 200_Public_Square completionDate 1985 200_Public_Square , with 45 floors covering 111484_square_metres , is located in Cleveland and was completed in 1985 .
relative_subject Chicago leaderName Susana_Mendoza Susana_Mendoza is a leader in Chicago , Illinois , United_States where 300_North_LaSalle is located .
relative_object 300_North_LaSalle floorCount 60 300_North_LaSalle in Chicago was completed in 2009 has 60 floors and its designer is Pickard_Chilton .
direct_object 300_North_LaSalle floorCount 60 Pickard_Chilton is the architect of 300_North_LaSalle , Illinois , which was completed in 2009 with 60 floors .
passive_voice Dublin isPartOf Leinster Live_Nation_Entertainment own 3Arena in Dublin , Leinster , Republic_of_Ireland .
relative_subject AC_Hotel_Bella_Sky_Copenhagen architect 3XN The AC_Hotel_Bella_Sky_Copenhagen is located in Copenhagen and has 23 floors . The hotel was designed by the architects of the 3XN firm and the current tenant is Marriott_International .
passive_voice Ethiopia leaderName Mulatu_Teshome Addis_Ababa_City_Hall is in Addis_Ababa , Ethiopia and the Addis_Ababa_Stadium can also be found there . The leaders of Ethiopia are Mulatu_Teshome and Hailemariam_Desalegn .
apposition Ethiopia leaderName Hailemariam_Desalegn The Addis_Ababa_City_Hall is in the country of Ethiopia ( leader : Hailemariam_Desalegn ). Addis_Ababa is home to the Addis_Ababa_City_Hall and The Addis_Ababa_Stadium .
relative_subject Ethiopia leaderName Mulatu_Teshome Addis_Ababa_City_Hall and Addis_Ababa_Stadium are both located in Addis_Ababa , Ethiopia . Mulatu_Teshome and Hailemariam_Desalegn are key country leaders .
coordinated_clauses Adisham_Hall completionDate 1931 Built between 1931 and "1927" , Adisham_Hall is located in Haputale and has the Architectural style of "Tudor_and_Jacabian" .
coordinated_clauses Adisham_Hall completionDate 1931 Adisham_Hall was constructed in the Tudor_Revival_architecture style between "1927" and 1931 in "Haputale_,_Sri_Lanka" .
coordinated_clauses Adisham_Hall buildingStartDate "1927" The "Tudor_and_Jacabian" styled Adisham_Hall was built in Sri_Lanka between "1927" and 1931 .
possessif Adisham_Hall buildingStartDate "1927" Built between 1931 and "1927" , Adisham_Hall is located in Haputale and has the Architectural style of "Tudor_and_Jacabian" .
possessif Adisham_Hall buildingStartDate "1927" The architectural styled " Tudor_Revival_architecture " Adisham_Hall is located in Sri_Lanka and was built in "1927" and completed in 1931 .
relative_object Sri_Lanka language Tamil_language Adisham_Hall is located in "Haputale_,_Sri_Lanka" , Sri_Lanka , a country where Tamil_language is the language and one of the leaders is Ranil_Wickremesinghe .
relative_object Sri_Lanka capital Sri_Jayawardenepura_Kotte Sri_Lanka ' s leader is called Ranil_Wickremesinghe and the capital is Sri_Jayawardenepura_Kotte . Adisham_Hall is located in "Haputale_,_Sri_Lanka" .
relative_subject Alan_B._Miller_Hall location Virginia Located in Virginia , United_States and with the current tenants of the Mason_School_of_Business , the Alan_B._Miller_Hall was completed in "1_June_2009" .
relative_subject Alan_B._Miller_Hall completionDate "1_June_2009" The Alan_B._Miller_Hall , Virginia , designed by Robert_A._M._Stern , was completed "1_June_2009" and is owned by the College_of_William_&_Mary .
relative_subject Alan_B._Miller_Hall architect Robert_A._M._Stern The current tenants of the Robert_A._M._Stern designed Alan_B._Miller_Hall in Virginia , United_States are the Mason_School_of_Business .
coordinated_clauses Asilomar_Conference_Grounds added_to_the_National_Register_of_Historic_Places "1987-02-27" The Asilomar_Conference_Grounds is located on "Asilomar_Blvd_,_Pacific_Grove_,_California" , was constructed in 1913 , added to the National Register of Historic Places "1987-02-27" where is has the reference number "87000823" .
apposition Asher_and_Mary_Isabelle_Richardson_House added_to_the_National_Register_of_Historic_Places "1988-11-22" Asher_and_Mary_Isabelle_Richardson_House , constructed in 1911 , is located in Asherton_,_Texas and was added to the National Register of Historic Places on "1988-11-22" with the reference number "88002539" .
apposition Asser_Levy_Public_Baths added_to_the_National_Register_of_Historic_Places "1980-04-23" The Asser_Levy_Public_Baths constructed in 1904 are located in New_York_City and were added to the National Register of Historic Places on "1980-04-23" with the reference number "80002709" .
direct_object Asher_and_Mary_Isabelle_Richardson_House ReferenceNumber_in_the_National_Register_of_Historic_Places "88002539" Asher_and_Mary_Isabelle_Richardson_House is located in Asherton_,_Texas and was built in 1911 . The building was added to the National Register of Historic Places "1988-11-22" with the reference number "88002539" .
direct_object Asser_Levy_Public_Baths ReferenceNumber_in_the_National_Register_of_Historic_Places "80002709" The Asser_Levy_Public_Baths , constructed in 1904 are in "Asser_Levy_Place_and_East_23rd_Street" . They were added to the National Register of Historic Places "1980-04-23" with the reference number "80002709" .
direct_object Asser_Levy_Public_Baths ReferenceNumber_in_the_National_Register_of_Historic_Places "80002709" The Asser_Levy_Public_Baths constructed in 1904 are located in New_York_City and were added to the National Register of Historic Places on "1980-04-23" with the reference number "80002709" .
coordinated_clauses Asilomar_Conference_Grounds ReferenceNumber_in_the_National_Register_of_Historic_Places "87000823" The Asilomar_Conference_Grounds constructed in 1913 is located at Pacific_Grove_,_California . It was added to the National Register of Historic Places "1987-02-27" with the reference number "87000823" .
apposition Julia_Morgan birthPlace California California born Julia_Morgan designed the significant buildings of Asilomar_State_Beach , Asilomar_Conference_Grounds and Chinatown_,_San_Francisco .
existential Julia_Morgan birthPlace San_Francisco San_Francisco born Julia_Morgan has designed many significant buildings including the Asilomar_Conference_Grounds , Asilomar_State_Beach and many buildings in Chinatown_,_San_Francisco .
existential Asher_and_Mary_Isabelle_Richardson_House yearOfConstruction 1911 Asher_and_Mary_Isabelle_Richardson_House is located in Asherton_,_Texas and was built in 1911 . The building was added to the National Register of Historic Places "1988-11-22" with the reference number "88002539" .
existential Asser_Levy_Public_Baths yearOfConstruction 1904 The Asser_Levy_Public_Baths constructed in 1904 are located in New_York_City and were added to the National Register of Historic Places on "1980-04-23" with the reference number "80002709" .
direct_object Asser_Levy_Public_Baths yearOfConstruction 1904 The Asser_Levy_Public_Baths was built in 1904 and is located in Manhattan . The building was added to the National Register of Historic Places "1980-04-23" with the reference number "80002709" .
direct_object Asser_Levy_Public_Baths yearOfConstruction 1904 Asser_Levy_Public_Baths are located in Manhattan and were constructed in 1904 . The Baths were added to the National Register of Historic Places on "1980-04-23" under reference "80002709" .
coordinated_clauses Asilomar_Conference_Grounds ReferenceNumber_in_the_National_Register_of_Historic_Places "87000823" The Asilomar_Conference_Grounds built in 1913 is located at Pacific_Grove_,_California and was built in the architectural style of the "Arts_and_Crafts_Movement_and_American_craftsman_Bungalows" . "87000823" is the reference number in the National Register of Historic Places .
passive_voice Ernie_Colón nationality Puerto_Ricans Dan_Mishkin and the Puerto_Ricans national , Ernie_Colón created the comic character of Bolt_(comicsCharacter) who has the alternative name of "Larry_Bolatinsky" .
coordinated_clauses Ernie_Colón nationality Puerto_Ricans The character Bolt_(comicsCharacter) , also known as "Larry_Bolatinsky" , was created by Paris_Cullins and Ernie_Colón , a Puerto_Ricans National .
apposition Paris_Cullins nationality United_States Bolt_(comicsCharacter) , also known as "Larry_Bolatinsky" , is a comic book character created by Gary_Cohn_(comics) and the United_States national Paris_Cullins .
coordinated_clauses Paris_Cullins nationality United_States Bolt_(comicsCharacter) ( "Larry_Bolatinsky" ) is a comic character which was created by both Dan_Mishkin and United_States . national Paris_Cullins .
relative_subject Paris_Cullins nationality United_States The United_States national , Paris_Cullins along with comic book writer , Gary_Cohn_(comics) created the comic character of Bolt_(comicsCharacter) . The character has the alternative name of "Larry_Bolatinsky" ,.
coordinated_full_clauses Ernie_Colón nationality Puerto_Ricans Bolt_(comicsCharacter) ( "Larry_Bolatinsky" ) is a comic character that was created by both Paris_Cullins and Ernie_Colón who is a national of Puerto_Ricans Rico .
relative_subject Sheldon_Moldoff award Inkpot_Award The Black_Pirate , also know as "Jon_Valor" , was created by the Inkpot_Award winning Americans , Sheldon_Moldoff .
passive_voice Bolt_(comicsCharacter) alternativeName "Larry_Bolatinsky" Bolt_(comicsCharacter) , also known as "Larry_Bolatinsky" , is a comic book character created by Gary_Cohn_(comics) and the United_States national Paris_Cullins .
passive_voice Bolt_(comicsCharacter) alternativeName "Larry_Bolatinsky" The United_States national , Paris_Cullins along with comic book writer , Gary_Cohn_(comics) created the comic character of Bolt_(comicsCharacter) . The character has the alternative name of "Larry_Bolatinsky" ,.
direct_object Karl_Kesel nationality Americans Marv_Wolfman and the Americans Karl_Kesel are the creators of the comic book character Auron_(comicsCharacter) , whose full name is "Lambien" .
passive_voice Baymax firstAppearanceInFilm Big_Hero_6_(film) Big_Hero_6_(film) was the first appearance for Baymax , created by Americans Duncan_Rouleau and Steven_T._Seagle .
existential Baymax series Big_Hero_6_(film) Baymax , a character in Big_Hero_6_(film) , was created by Steven_T._Seagle and the Americans , Duncan_Rouleau .
relative_object Baymax series Big_Hero_6_(film) Baymax is a character from Big_Hero_6_(film) , who was created by Americans born Duncan_Rouleau and Steven_T._Seagle .
relative_subject Bananaman creator Steve_Bright Bananaman , created by Steve_Bright and starring Bill_Oddie , was first broadcast by the BBC on the "1983-10-03" .
relative_subject Bananaman creator Steve_Bright Bananaman was created by Steve_Bright and starred Tim_Brooke-Taylor . It was first aired "1983-10-03" , being broadcast by the BBC .
relative_subject Bananaman creator Steve_Bright Bananaman was created by Steve_Bright and starred Tim_Brooke-Taylor . The show was first aired "1983-10-03" "STV" .
passive_voice Duncan_Rouleau nationality Americans Big_Hero_6_(film) was the first appearance for Baymax , created by Americans Duncan_Rouleau and Steven_T._Seagle .
direct_object Alan_Shepard birthPlace New_Hampshire The now "Deceased" Alan_Shepard was born in New_Hampshire "1923-11-18" . He was selected by NASA in 1959 .
apposition Buzz_Aldrin was_a_crew_member_of Apollo_11 Buzz_Aldrin is a United_States National and was born in Glen_Ridge_,_New_Jersey . He was a Fighter_pilot and was a crew member of Apollo_11 .
direct_object Alan_Bean nationality United_States Alan_Bean ( of the United_States ) was a crew member of NASA ' s Apollo_12 under the commander David_Scott .
coordinated_clauses Alan_Shepard birthDate "1923-11-18" Alan_Shepard was born "1923-11-18" in New_Hampshire . He obtained a MA in 1957 from "NWC_,_M.A._1957" " he retired "1974-08-01" .
relative_subject William_Anders occupation Fighter_pilot William_Anders was born in British_Hong_Kong . He graduated from "AFIT_,_M.S._1962" with a M . S . in 1962 and became a Fighter_pilot . He retired "1969-09-01" .
coordinated_clauses 1036_Ganymed discoverer Walter_Baade 1036_Ganymed was discovered by Walter_Baade , who was born in Preußisch_Oldendorf , went to the University_of_Göttingen and died in West_Germany .
apposition Elliot_See occupation Test_pilot Elliot_See attended the University_of_Texas_at_Austin . He became a Test_pilot . He died in St_Louis on "1966-02-28" .
coordinated_full_clauses Alan_Bean occupation Test_pilot Test_pilot Alan_Bean was born "1932-03-15" in Wheeler_,_Texas and in 1963 was picked by NASA to join the space program .
relative_subject Elliot_See almaMater University_of_Texas_at_Austin Elliot_See who was born in Dallas and died in St_Louis , graduated from the University_of_Texas_at_Austin and was selected by NASA in 1962 ,.
relative_subject William_Anders was_a_crew_member_of Apollo_8 William_Anders was born "1933-10-17" in British_Hong_Kong , served as a Fighter_pilot and crew member of Apollo_8 .
coordinated_clauses Alan_Bean occupation Test_pilot Alan_Bean is a United_States citizen born in Wheeler_,_Texas . He served as a Test_pilot and was a crew member of the Apollo_12 mission .
coordinated_clauses Alan_Bean birthDate "1932-03-15" Test_pilot Alan_Bean was born "1932-03-15" in Wheeler_,_Texas and in 1963 was picked by NASA to join the space program .
apposition Aaron_Boogaard height 1.905 Aaron_Boogaard was born in 1986 in Canada and is 1.905 m tall . He plays for Wichita_Thunder team .
apposition Alex_Plante height 1.9304 Alex_Plante was born in Brandon_,_Manitoba in 1989 and measures 1.9304 m tall . He plays for the Anyang_Halla club .
possessif Aaron_Boogaard height 1.905 Aaron_Boogaard was born in 1986 in Canada and is 1.905 m tall . He plays for Wichita_Thunder team .
coordinated_clauses Alex_Plante height 1.9304 Alex_Plante was born in 1989 , in Brandon_,_Manitoba . At a height of 1.9304 m , Alex_Plante , has played for the club Anyang_Halla .
passive_voice Alex_Plante height 1.9304 Born in 1989 , in Manitoba , Alex_Plante has played for the club Anyang_Halla and is 1.9304 metres tall .
coordinated_clauses Aaron_Boogaard birthYear 1986 Aaron_Boogaard was born in 1986 , in Regina_,_Saskatchewan . He is 1.905 tall and played for the club , Wichita_Thunder .
apposition Alex_Plante birthYear 1989 Alex_Plante was born in Brandon_,_Manitoba in 1989 and measures 1.9304 m tall . He plays for the Anyang_Halla club .
passive_voice Alex_Plante birthYear 1989 Born in 1989 , in Manitoba , Alex_Plante has played for the club Anyang_Halla and is 1.9304 metres tall .
relative_subject Alex_Plante birthYear 1989 1.9304 m tall Alex_Plante , whose club is Anyang_Halla , was born in Canada , in 1989 .
possessif Peñarol manager Jorge_Orosmán_da_Silva Abel_Hernández currently plays for the club US_Città_di_Palermo , but he has also played for the youth football team youthPeñarol and also at Peñarol where the manager is Jorge_Orosmán_da_Silva .
direct_object AC_Lumezzane manager Michele_Marcolini Michele_Marcolini is the manager of AC_Lumezzane . Previously he played for Atalanta_BC and youth football for youthAC_Chievo_Verona . His own club is AC_Chievo_Verona .
passive_voice Clyde_FC ground Broadwood_Stadium Alan_Martin_(footballer) is a footballer for the Clyde_FC club but currently plays for Barrow_A.FC . Barry_Ferguson is the manager of Clyde_FC who play at the Broadwood_Stadium .
apposition VfL_Wolfsburg league Bundesliga Aaron_Hunt played for the club SV_Werder_Bremen and club VfL_Wolfsburg in Bundesliga . Dieter_Hecking is Wolfsburg ' s manager .
apposition SV_Werder_Bremen league Bundesliga Aaron_Hunt has played for the club SV_Werder_Bremen in the Bundesliga and also for VfL_Wolfsburg who are managed by Dieter_Hecking .
relative_subject FC_Terek_Grozny ground Grozny Aleksandr_Prudnikov ' s club is FC_Dynamo_Moscow but he is currently playing for FC_Terek_Grozny , their ground is located in Grozny and they are managed by Rashid_Rakhimov .
existential FC_Terek_Grozny ground Grozny FC_Terek_Grozny , which has its ground in Grozny , is managed by Rashid_Rakhimov . Aleksandr_Prudnikov , who played for FC_Spartak_Moscow , plays for FC_Terek_Grozny .
coordinated_full_clauses Hamburger_SV league Bundesliga Aaron_Hunt has previously played for both SV_Werder_Bremen under their manager Viktor_Skrypnyk and also Hamburger_SV which is a member of the Bundesliga .
possessif Al_Kharaitiyat_SC ground Al_Khor Alaa_Abdul-Zahra has played for Al-Merrikh_SC and Al_Kharaitiyat_SC . The latter ' s home ground is Al_Khor and the manager is Amar_Osim .
coordinated_full_clauses SV_Werder_Bremen league Bundesliga The manager of SV Werder Breman , is Viktor_Skrypnyk . Aaron_Hunt played for SV_Werder_Bremen which play in the Bundesliga . Aaron_Hunt ' s club is the Germany_national_under-21_football_team .
coordinated_clauses Al_Kharaitiyat_SC ground Al_Khor Alaa_Abdul-Zahra , who plays for the Al-Wakrah_Sport_Club , plays for Al_Kharaitiyat_SC . This club is located in Al_Khor and is managed by , Amar_Osim .
existential SV_Werder_Bremen league Bundesliga Aaron_Hunt has played for the club SV_Werder_Bremen in the Bundesliga and also for VfL_Wolfsburg who are managed by Dieter_Hecking .
existential SV_Werder_Bremen league Bundesliga Aaron_Hunt , who plays for Goslarer_SC_08 , previously played in the Bundesliga league for the club SV_Werder_Bremen which is managed by Viktor_Skrypnyk .
possessif FC_Terek_Grozny ground Grozny FC_Amkar_Perm player , Aleksandr_Prudnikov , is in the FC_Terek_Grozny club . Rashid_Rakhimov is the manager of FC_Terek_Grozny , the ground of which is , based at Grozny .
apposition SV_Werder_Bremen league Bundesliga Aaron_Hunt plays for SV_Werder_Bremen along with playing for the Germany_national_under-21_football_team . SV_Werder_Bremen play in the Bundesliga and are managed by Viktor_Skrypnyk .
apposition VfL_Wolfsburg league Bundesliga Aaron_Hunt previously played for SV_Werder_Bremen , but currently plays for VfL_Wolfsburg who play in the Bundesliga and are managed by Dieter_Hecking .
relative_subject Jacksonville_Dolphins stadium John_Sessions_Stadium Akeem_Priestley is connected to the Orange_County_Blues_FC Football Club where the manager is , Oliver_Wyss . Akeem_Priestley plays for Jacksonville_Dolphins , the club play at the John_Sessions_Stadium .
possessif FC_Terek_Grozny ground Grozny Aleksandr_Prudnikov plays for FC_Anzhi_Makhachkala and also for FC_Terek_Grozny who are located in Grozny and managed by Rashid_Rakhimov .
passive_voice Al_Kharaitiyat_SC ground Al_Khor Alaa_Abdul-Zahra has played for Duhok_SC and currently plays for Al_Kharaitiyat_SC , managed by Amar_Osim , which is located in Al_Khor .
passive_voice FC_Torpedo_Moscow season 2014–15_Russian_Premier_League Aleksandr_Chumakov , who is in the Soviet_Union_national_football_team , plays for is FC_Torpedo_Moscow . Valery_Petrakov is the manager of FC_Torpedo_Moscow which played in the 2014–15_Russian_Premier_League .
passive_voice Al_Kharaitiyat_SC ground Al_Khor Alaa_Abdul-Zahra ' s club is Al_Kharaitiyat_SC whose home place is Al_Khor and whose manager is Amar_Osim . Alaa_Abdul-Zahra plays for the Iraq_national_under-20_football_team .
relative_subject Al_Kharaitiyat_SC ground Al_Khor Alaa_Abdul-Zahra ' s club is Al_Kharaitiyat_SC , located in Al_Khor and managed by Amar_Osim . Alaa_Abdul-Zahra also played with the Duhok_SC .
relative_subject Clyde_FC ground Broadwood_Stadium A footballer , Alan_Martin_(footballer) , played with the Clyde_FC and the Leeds_United_FC . The former club is located in Broadwood_Stadium and managed by Barry_Ferguson .
possessif RoPS league Veikkausliiga Akeem_Priestley has played for the Orange_County_Blues_FC and for RoPS who are in the Veikkausliiga league . Orange_County_Blues_FC are managed by Oliver_Wyss .
direct_object Abel_Hernández youthclub youthUS_Città_di_Palermo Abel_Hernández was a member of youth club youthUS_Città_di_Palermo and now plays for the US_Città_di_Palermo and the Uruguay_Olympic_football_team . Giuseppe_Iachini is the manager of the Palermo club .
direct_object FC_Samtredia ground Erosi_Manjgaladze_Stadium Aleksandre_Guruli plays for both FC_Dinamo_Batumi ( managed by Levan_Khomeriki ) and FC_Samtredia , ground of which is Erosi_Manjgaladze_Stadium .
apposition FC_Samtredia ground Erosi_Manjgaladze_Stadium Aleksandre_Guruli plays for FC_Dinamo_Batumi whose manager is Levan_Khomeriki . He also plays for FC_Samtredia whose ground is the Erosi_Manjgaladze_Stadium .
existential Al_Kharaitiyat_SC ground Al_Khor Alaa_Abdul-Zahra plays for Al_Kharaitiyat_SC , in Al_Khor and for the Iraq_national_under-23_football_team . Al_Kharaitiyat_SC is managed by Amar_Osim .
possessif Abel_Hernández youthclub youthPeñarol Abel_Hernández currently plays for the club US_Città_di_Palermo , but he has also played for the youth football team youthPeñarol and also at Peñarol where the manager is Jorge_Orosmán_da_Silva .
apposition Al_Kharaitiyat_SC ground Al_Khor Alaa_Abdul-Zahra plays for Al_Kharaitiyat_SC , in Al_Khor and for the Iraq_national_under-23_football_team . Al_Kharaitiyat_SC is managed by Amar_Osim .
relative_adverb Ferencvárosi_TC chairman Gábor_Kubatov Akeem_Adams , who plays for W_Connection_FC , also plays for Ferencvárosi_TC . Thomas_Doll is the manager for and Gábor_Kubatov is the chairman of Ferencvárosi_TC .
coordinated_full_clauses Al_Kharaitiyat_SC ground Al_Khor Alaa_Abdul-Zahra plays for Al_Kharaitiyat_SC , in Al_Khor and for the Iraq_national_under-23_football_team . Al_Kharaitiyat_SC is managed by Amar_Osim .
coordinated_full_clauses Hamilton_Academical_FC ground New_Douglas_Park Alan_Martin_(footballer) is a footballer for the Hamilton_Academical_FC . in New_Douglas_Park . He plays for the Crewe_Alexandra_FC that is managed by Steve_Davis_(footballer) .
possessif Hamilton_Academical_FC ground New_Douglas_Park Alan_Martin_(footballer) played for Clyde_FC where Barry_Ferguson is the manager . He now plays for Hamilton_Academical_FC based at New_Douglas_Park .
relative_object Hamilton_Academical_FC ground New_Douglas_Park Alan_Martin_(footballer) played for Clyde_FC where Barry_Ferguson is the manager . He now plays for Hamilton_Academical_FC based at New_Douglas_Park .
passive_voice Al_Kharaitiyat_SC ground Al_Khor Alaa_Abdul-Zahra , whose club is Shabab_Al-Ordon_Club , also plays for Al_Kharaitiyat_SC . which is located in Al_Khor . The manager of Al_Kharaitiyat_SC is Amar_Osim .
passive_voice FC_Terek_Grozny ground Grozny Aleksandr_Prudnikov has played for FC_Tom_Tomsk and FC_Terek_Grozny . Rashid_Rakhimov is the manager of FC_Terek_Grozny , whose ground is located in Grozny .
direct_object Alan_Martin_(footballer) height 185.42_centimetres Footballer , Alan_Martin_(footballer) , was born on the 1989-01-01 . He is 185.42_centimetres tall and plays for both the Hamilton_Academical_FC and Crewe_Alexandra_FC .
possessif Alan_Martin_(footballer) height 185.42_centimetres At 185.42_centimetres tall , Footballer , Alan_Martin_(footballer) , was born 1989-01-01 . He played for the Scotland_national_under-21_football_team and is a footballer for the Crewe_Alexandra_FC .
coordinated_full_clauses Alan_Martin_(footballer) height 185.42_centimetres Alan_Martin_(footballer) is 185.42_centimetres tall and was born 1989-01-01 . He currently plays for Crewe_Alexandra_FC and previously for Accrington_Stanley_FC .
coordinated_full_clauses Alan_Martin_(footballer) height 185.42_centimetres At 185.42_centimetres tall , Footballer , Alan_Martin_(footballer) , was born 1989-01-01 . He played for the Scotland_national_under-21_football_team and is a footballer for the Crewe_Alexandra_FC .
coordinated_clauses Alan_Martin_(footballer) height 185.42_centimetres Crewe_Alexandra_FC , Footballer , Alan_Martin_(footballer) , was born 1989-01-01 . He is 185.42_centimetres tall and played for Accrington_Stanley_FC .
coordinated_clauses Aleksandre_Guruli height 178.0_centimetres Aleksandre_Guruli was born in Batumi and is 178.0_centimetres high . He plays for the AS_Lyon-Duchère and the FC_Karpaty_Lviv FC .
possessif Aleksandre_Guruli height 178.0_centimetres Aleksandre_Guruli , who is 178.0_centimetres tall , was born in the Georgian_Soviet_Socialist_Republic . Aleksandre_Guruli plays for FC_Dinamo_Batumi and FC_Karpaty_Lviv club .
coordinated_full_clauses Alex_Plante club Anyang_Halla 1.9304 m tall Alex_Plante , whose club is Anyang_Halla , was born in Canada , in 1989 .
existential Alex_Plante club Anyang_Halla Alex_Plante was born in Brandon_,_Manitoba in 1989 and measures 1.9304 m tall . He plays for the Anyang_Halla club .
possessif FC_Dinamo_Batumi manager Levan_Khomeriki Aleksandre_Guruli plays for both FC_Dinamo_Batumi ( managed by Levan_Khomeriki ) and FC_Samtredia , ground of which is Erosi_Manjgaladze_Stadium .
relative_subject FC_Terek_Grozny manager Rashid_Rakhimov FC_Amkar_Perm player , Aleksandr_Prudnikov , played for FC_Terek_Grozny . Rashid_Rakhimov is the manager of Grozny based , FC_Terek_Grozny .
relative_subject FC_Terek_Grozny manager Rashid_Rakhimov Aleksandr_Prudnikov , of FC_Terek_Grozny club ( managed by Rashid_Rakhimov ) , plays for FC_Anzhi_Makhachkala . Grozny is the home of FC_Terek_Grozny .
coordinated_full_clauses Clyde_FC manager Barry_Ferguson Alan_Martin_(footballer) played football for Motherwell_FC whose home territory is Fir_Park . Alan_Martin_(footballer) also played with the Clyde_FC which is managed by Barry_Ferguson .
possessif FC_Terek_Grozny manager Rashid_Rakhimov Aleksandr_Prudnikov is in the FC_Terek_Grozny club which is located at Grozny and managed by Rashid_Rakhimov . Aleksandr_Prudnikov plays also for FC_Anzhi_Makhachkala .
relative_subject FC_Terek_Grozny manager Rashid_Rakhimov FC_Amkar_Perm player , Aleksandr_Prudnikov , is in the FC_Terek_Grozny club . Rashid_Rakhimov is the manager of FC_Terek_Grozny , the ground of which is , based at Grozny .
possessif Al_Kharaitiyat_SC manager Amar_Osim Alaa_Abdul-Zahra has played for Al-Merrikh_SC and Al_Kharaitiyat_SC . The latter ' s home ground is Al_Khor and the manager is Amar_Osim .
direct_object Al_Kharaitiyat_SC manager Amar_Osim Alaa_Abdul-Zahra plays for Al_Kharaitiyat_SC . found in Al_Khor and also plays for Al-Merrikh_SC . The Al_Kharaitiyat_SC is managed by Amar_Osim .
direct_object FC_Dinamo_Batumi manager Levan_Khomeriki Aleksandre_Guruli plays for both FC_Dinamo_Batumi ( managed by Levan_Khomeriki ) and FC_Samtredia , ground of which is Erosi_Manjgaladze_Stadium .
direct_object FC_Dinamo_Batumi manager Levan_Khomeriki Aleksandre_Guruli plays for FC_Dinamo_Batumi whose manager is Levan_Khomeriki . He also plays for FC_Samtredia whose ground is the Erosi_Manjgaladze_Stadium .
relative_object FC_Terek_Grozny manager Rashid_Rakhimov FC_Amkar_Perm player , Aleksandr_Prudnikov , played for FC_Terek_Grozny . Rashid_Rakhimov is the manager of Grozny based , FC_Terek_Grozny .
relative_subject FC_Terek_Grozny manager Rashid_Rakhimov Aleksandr_Prudnikov plays for FC_Terek_Grozny whose base is in Grozny and is managed by Rashid_Rakhimov . He also plays for the FC_Alania_Vladikavkaz club .
relative_subject Al_Kharaitiyat_SC manager Amar_Osim Alaa_Abdul-Zahra has played for Al_Kharaitiyat_SC and Al-Zawra'a_SC . Al_Kharaitiyat_SC . are managed by Amar_Osim and play their home games at Al_Khor .
direct_object Crewe_Alexandra_FC manager Steve_Davis_(footballer) Alan_Martin_(footballer) is a footballer who plays for Hamilton_Academical_FC at their home of New_Douglas_Park and also for Crewe_Alexandra_FC which is managed by Steve_Davis_(footballer) .
possessif FC_Terek_Grozny manager Rashid_Rakhimov FC_Amkar_Perm player , Aleksandr_Prudnikov , is in the FC_Terek_Grozny club . Rashid_Rakhimov is the manager of FC_Terek_Grozny , the ground of which is , based at Grozny .
possessif Ferencvárosi_TC manager Thomas_Doll Akeem_Adams plays for Ferencvárosi_TC which is managed by Thomas_Doll . Akeem_Adams used to play for United_Petrotrin_FC which play in Palo_Seco_Velodrome .
coordinated_full_clauses Al_Kharaitiyat_SC manager Amar_Osim Alaa_Abdul-Zahra , who plays for the Al-Wakrah_Sport_Club , plays for Al_Kharaitiyat_SC . This club is located in Al_Khor and is managed by , Amar_Osim .
coordinated_full_clauses Crewe_Alexandra_FC manager Steve_Davis_(footballer) Alan_Martin_(footballer) is a footballer for the Hamilton_Academical_FC . in New_Douglas_Park . He plays for the Crewe_Alexandra_FC that is managed by Steve_Davis_(footballer) .
direct_object FC_Karpaty_Lviv manager Oleh_Luzhny Aleksandre_Guruli has played for FC_Karpaty_Lviv who are managed by Oleh_Luzhny and also for Olympique_Lyonnais whose ground is the Parc_Olympique_Lyonnais .
relative_object Al_Kharaitiyat_SC manager Amar_Osim Alaa_Abdul-Zahra has played for Al-Merrikh_SC and Al_Kharaitiyat_SC . The latter ' s home ground is Al_Khor and the manager is Amar_Osim .
relative_subject Al_Kharaitiyat_SC manager Amar_Osim Alaa_Abdul-Zahra , whose club is Sanat_Mes_Kerman_FC , plays for Al_Kharaitiyat_SC . Al_Khor is the home ground of Al_Kharaitiyat_SC . which is managed by , Amar_Osim .
apposition Al_Kharaitiyat_SC manager Amar_Osim Alaa_Abdul-Zahra ' s club is Al_Kharaitiyat_SC whose home place is Al_Khor and whose manager is Amar_Osim . Alaa_Abdul-Zahra plays for the Iraq_national_under-20_football_team .
apposition Al_Kharaitiyat_SC manager Amar_Osim Alaa_Abdul-Zahra plays for Al_Kharaitiyat_SC which is in Al_Khor and is managed by Amar_Osim . He also plays for the Iraq_national_under-20_football_team .
relative_subject Clyde_FC manager Barry_Ferguson A footballer , Alan_Martin_(footballer) , played with the Clyde_FC and the Leeds_United_FC . The former club is located in Broadwood_Stadium and managed by Barry_Ferguson .
existential Crewe_Alexandra_FC manager Steve_Davis_(footballer) Alan_Martin_(footballer) plays football for Crewe_Alexandra_FC managed by Steve_Davis_(footballer) and also for Clyde_FC who have their home ground at the Broadwood_Stadium .
relative_subject Clyde_FC manager Barry_Ferguson Alan_Martin_(footballer) plays for Hamilton_Academical_FC and previously played for Broadwood_Stadium based Clyde_FC where Barry_Ferguson is the manager .
coordinated_full_clauses Al_Kharaitiyat_SC manager Amar_Osim Alaa_Abdul-Zahra ' s club is Al_Kharaitiyat_SC , located in Al_Khor and managed by Amar_Osim . Alaa_Abdul-Zahra also played with the Duhok_SC .
direct_object Clyde_FC manager Barry_Ferguson Former Leeds_United_FC footballer , Alan_Martin_(footballer) , plays for the Clyde_FC club . Barry_Ferguson is the manager of Clyde_FC , the ground of which , is called Broadwood_Stadium .
relative_subject Akeem_Ayers position Linebacker Akeem_Ayers was born 1989-07-10 . He plays in the position of Linebacker and made his debut for the Tennessee_Titans before going on to play for the St_Louis_Rams .
possessif St_Louis_Rams coach Steve_Spagnuolo Akeem_Ayers debuted with the Tennessee_Titans and is a former player of the St_Louis based , St_Louis_Rams . The Rams are coached by Steve_Spagnuolo .
coordinated_clauses Aleksander_Barkov_,_Jr. height 1.905 1.905 tall , Aleksander_Barkov_,_Jr. , has played for the Florida_Panthers . He was born in "Tampere_,_Finland" , on 1995-09-02 .
relative_subject Aleksandre_Guruli birthPlace Georgian_Soviet_Socialist_Republic Aleksandre_Guruli was born in the Georgian_Soviet_Socialist_Republic . He is 178.0_centimetres tall . He has played for FC_Karpaty_Lviv and AS_Lyon-Duchère .
coordinated_clauses Alex_Tyus height 2.032 Alex_Tyus eas born 1988-01-08 and was drafted in 2011 . He is 2.032 m tall and played on the Florida_Gators_men's_basketball team .
coordinated_full_clauses Canada language English_language O_Canada is the national anthem of Canada which is lead by Elizabeth_II . Aaron_Boogaard was born in Canada where the English_language is spoken .
apposition Canada anthem O_Canada O_Canada is the national anthem of Canada , where Aaron_Boogaard was born . Elizabeth_II is the leader of Canada , where one of the ethnic groups , is Asian_Canadians .
direct_object Akron_Summit_Assault fullname "Akron_Metro_Futbol_Club_Summit_Assault" 3000 member strong Akron_Summit_Assault , fullname "Akron_Metro_Futbol_Club_Summit_Assault" , play in the 2011_PDL_season . Denzil_Antonio was previously their manager .
passive_voice Rolando_Maran placeOfBirth Italy Rolando_Maran ( born in Italy ) is manager of AC_Chievo_Verona and has played for Calcio_Catania and Carrarese_Calcio .
passive_voice Michele_Marcolini placeOfBirth Italy Michele_Marcolini , born in Italy , is part of the Atalanta_BC club . He plays for AC_Chievo_Verona and has been manager of AC_Lumezzane .
possessif AZ_Alkmaar owner Robert_Eenhoorn John_van_den_Brom is the manager of AZ_Alkmaar , owned by Robert_Eenhoorn , as well as playing for AFC_Ajax and De_Graafschap .
direct_object Rolando_Maran placeOfBirth Italy Rolando_Maran ( born in Italy ) is a member of the Calcio_Catania . He plays for Carrarese_Calcio and is the manager of AC_Chievo_Verona .
coordinated_clauses Rolando_Maran placeOfBirth Italy Rolando_Maran ( born in Italy ) is a member of the Calcio_Catania . He plays for Carrarese_Calcio and is the manager of AC_Chievo_Verona .
apposition Rolando_Maran placeOfBirth Italy Rolando_Maran ( born in Italy ) is manager of AC_Chievo_Verona and has played for Calcio_Catania and Carrarese_Calcio .
apposition AZ_Alkmaar owner Robert_Eenhoorn John_van_den_Brom is the manager of AZ_Alkmaar , owned by Robert_Eenhoorn , as well as playing for AFC_Ajax and De_Graafschap .
possessif Michele_Marcolini youthclub youthAC_Chievo_Verona Michele_Marcolini , who was manager of AC_Lumezzane , played football for youthAC_Chievo_Verona Youths , AC_Chievo_Verona and Atalanta_BC .
passive_voice Michele_Marcolini youthclub youthAC_Chievo_Verona Michele_Marcolini , who was manager of AC_Lumezzane , played football for youthAC_Chievo_Verona Youths , AC_Chievo_Verona and Atalanta_BC .
juxtaposition AZ_Alkmaar owner Max_Huiberts John_van_den_Brom is the manager of AZ_Alkmaar which is owned by Max_Huiberts . He also plays for both AFC_Ajax and İstanbulspor_AŞ .
coordinated_clauses AZ_Alkmaar owner Robert_Eenhoorn John_van_den_Brom plays for AFC_Ajax and İstanbulspor_AŞ . He manages AZ_Alkmaar which is owned by Robert_Eenhoorn .
passive_voice AZ_Alkmaar owner Max_Huiberts John_van_den_Brom , former manager of AZ_Alkmaar , now owned by Max_Huiberts , plays for AFC_Ajax and İstanbulspor_AŞ .
coordinated_full_clauses AZ_Alkmaar owner Max_Huiberts The manager of AZ_Alkmaar , owned by Max_Huiberts , is John_van_den_Brom who plays for AFC_Ajax and Vitesse_Arnhem .
juxtaposition Rolando_Maran placeOfBirth Italy Born in Italy , Rolando_Maran is the manager of AC_Chievo_Verona . He also plays for Carrarese_Calcio and is in the Unione_Triestina_2012_SSD club .
coordinated_clauses Azerbaijan_Premier_League champions Qarabağ_FK Qarabağ_FK , are the champions of the Azerbaijan_Premier_League . Also to play in that league are AZAL_PFK , who are in the 2014 season and have 3500 members .
relative_subject AEK_Athens_FC ground Greece AEK_Athens_FC has a ground in Greece and has 69618 members . They played in the 2014 season and compete in the Superleague_Greece .
coordinated_clauses Superleague_Greece champions Olympiacos_FC AEK_Athens_FC have 69618 members , their ground is the Olympic_Stadium_(Athens) and they play in the Superleague_Greece whose current champions are Olympiacos_FC .
possessif Superleague_Greece champions Olympiacos_FC AEK_Athens_FC ground is in Athens and they compete in the Superleague_Greece . It has has 69618 members . Olympiacos_FC are previous champions of the Superleague_Greece .
coordinated_clauses Serie_B champions Carpi_FC_1909 AC_Cesena ' s ground is located in Italy and they have 23900 members . They play in the Serie_B league of which Carpi_FC_1909 are the champions .
relative_subject Superleague_Greece champions Olympiacos_FC AEK_Athens_FC home ground is The Olympic_Stadium_(Athens) . The club has 69618 members and competes in the Superleague_Greece . Olympiacos_FC are previous champions of the league .
possessif AS_Roma numberOfMembers 70634 AS_Roma ' s ground in Rome holds 70634 fans . The club play in Serie_A , the champions of which are Juventus_FC .
existential AS_Roma fullname "Associazione_Sportiva_Roma_SpA" Juventus_FC , former champions of Serie_A , compete with AS_Roma , known under the full name "Associazione_Sportiva_Roma_SpA" , based in "Rome_,_Italy" , playing in Serie_A .
relative_object Olympic_Stadium_(Athens) location Marousi The ground for AEK_Athens_FC is the Olympic_Stadium_(Athens) located in Marousi . The club competes in the Superleague_Greece which was previously won by Olympiacos_FC .
relative_subject AS_Roma fullname "Associazione_Sportiva_Roma_SpA" The "Associazione_Sportiva_Roma_SpA" is the non - abbreviated name of AS_Roma with their ground in Stadio_Olimpico . AS_Roma play in Serie_A whose current champions are Juventus_FC .
coordinated_full_clauses Estádio_Municipal_Coaracy_da_Mata_Fonseca location Brazil Agremiação_Sportiva_Arapiraquense ' s ground is the Estádio_Municipal_Coaracy_da_Mata_Fonseca in Brazil . They play in the Campeonato_Brasileiro_Série_C league where Vila_Nova_Futebol_Clube have been champions .
passive_voice St_Vincent–St_Mary_High_School country United_States Akron_Summit_Assault play in the Premier_Development_League which plays at St_Vincent–St_Mary_High_School located in the United_States where K-W_United_FC have been champions .
passive_voice Campeonato_Brasileiro_Série_C country Brazil Estádio_Municipal_Coaracy_da_Mata_Fonseca is the ground of Agremiação_Sportiva_Arapiraquense , who play in the Campeonato_Brasileiro_Série_C league . This league is from Brazil and champions of it have included Vila_Nova_Futebol_Clube .
relative_object St_Vincent–St_Mary_High_School country United_States Akron_Summit_Assault play in the Premier_Development_League which plays at St_Vincent–St_Mary_High_School located in the United_States where K-W_United_FC have been champions .
relative_adverb AEK_Athens_FC numberOfMembers 69618 AEK_Athens_FC ground is in Athens and they compete in the Superleague_Greece . It has has 69618 members . Olympiacos_FC are previous champions of the Superleague_Greece .
juxtaposition Campeonato_Brasileiro_Série_C country Brazil Agremiação_Sportiva_Arapiraquense play their home games at Estádio_Municipal_Coaracy_da_Mata_Fonseca in the Campeonato_Brasileiro_Série_C league , Brazil . Vila_Nova_Futebol_Clube are currently the champions of Campeonato_Brasileiro_Série_C .
possessif AS_Roma fullname "Associazione_Sportiva_Roma_SpA" "Associazione_Sportiva_Roma_SpA" is the full name AS_Roma . They have a ground in Rome and play in the Serie_A league . the former champions of this league are Juventus_FC .
relative_object AZAL_PFK location "Shuvalan_,_Baku_,_Azerbaijan" The champions of the Azerbaijan_Premier_League is Qarabağ_FK . AZAL_PFK , who also play in the league , have their home ground at the AZAL_Arena in "Shuvalan_,_Baku_,_Azerbaijan" .
relative_subject AS_Gubbio_1910 season 2014 The full name of AS_Gubbio_1910 is "Associazione_Sportiva_Gubbio_1910_Srl" . The club has 5300 members and has a ground in Italy . It competed in the 2014 season .
passive_voice AC_Chievo_Verona season 2014 AC_Chievo_Verona , full name "Associazione_Calcio_ChievoVerona_Srl" , play out of "Verona_,_Italy" . They competed in the 2014 Serie A season and have 39371 members .
possessif A.F.C._Fylde season 2014 Based at "Warton_,_Fylde_,_Lancashire" , is A.F.C._Fylde . The full name of which , is "Association_Football_Club_Fylde" . The club played in 2014 and has 3180 members .
juxtaposition AC_Chievo_Verona season 2014 The fullname of AC_Chievo_Verona is "Associazione_Calcio_ChievoVerona_Srl" and their ground is in Verona . The club played in the 2014 season and has 39371 members .
relative_subject AC_Chievo_Verona season 2014 The fullname of AC_Chievo_Verona is "Associazione_Calcio_ChievoVerona_Srl" and their ground is in Verona . The club played in the 2014 season and has 39371 members .
relative_subject AS_Gubbio_1910 season 2014 The fullname of AS_Gubbio_1910 is "Associazione_Sportiva_Gubbio_1910_Srl" and they competed in the 2014 season . Their ground is called the Stadio_Pietro_Barbetti and they have 5300 members .
coordinated_full_clauses AS_Roma season 2014 "Associazione_Sportiva_Roma_SpA" is the full name of , AS_Roma , who have their ground in "Rome_,_Italy" . They competed in the 2014 season and have 70634 members .
juxtaposition AS_Gubbio_1910 season 2014 The full name of AS_Gubbio_1910 is "Associazione_Sportiva_Gubbio_1910_Srl" . The club has 5300 members and has a ground in Italy . It competed in the 2014 season .
coordinated_clauses AFC_Blackpool season 2014–15_North_West_Counties_Football_League AFC_Blackpool played in the 2014–15_North_West_Counties_Football_League season and has 1500 members . Their full name is "Association_Football_Club_AFC_Blackpool" and their homeground is "Jepson_Way_," .
juxtaposition AFC_Blackpool season 2014–15_North_West_Counties_Football_League In the season 2014 - 2015 AFC_Blackpool , who have 1500 members , played in the 2014–15_North_West_Counties_Football_League . They have the full name "Association_Football_Club_AFC_Blackpool" and their home ground is "The_Mechanics_," .
relative_subject A.F.C._Fylde season 2014 "Association_Football_Club_Fylde" ( abbreviated to A.F.C._Fylde ) has 3180 members , are based at "Bryning_Lane" and competed in the 2014 season .
coordinated_full_clauses Azerbaijan leaderTitle Prime_Minister_of_Azerbaijan The Baku_Turkish_Martyrs'_Memorial is located in Baku , the capital of Azerbaijan , a country led by the Prime_Minister_of_Azerbaijan , Artur_Rasizade .
possessif Greece language Greek_language AE_Dimitra_Efxeinoupolis is located in Greece where the leader is Alexis_Tsipras . Athens is the capital of Greece and the language they speak is Greek_language .
relative_adverb Greece capital Athens The AE_Dimitra_Efxeinoupolis club is located in Greece , the capital of which is Athens . Two of the leaders in that country are , Alexis_Tsipras and Prokopis_Pavlopoulos .
coordinated_clauses AC_Chievo_Verona fullname "Associazione_Calcio_ChievoVerona_Srl" The fullname of AC_Chievo_Verona is "Associazione_Calcio_ChievoVerona_Srl" and their ground is in Verona . The club played in the 2014 season and has 39371 members .
coordinated_clauses AFC_Blackpool fullname "Association_Football_Club_AFC_Blackpool" AFC_Blackpool played in the 2014–15_North_West_Counties_Football_League season and has 1500 members . Their full name is "Association_Football_Club_AFC_Blackpool" and their homeground is "Jepson_Way_," .
apposition AS_Livorno_Calcio fullname "Livorno_Calcio_SpA" AS_Livorno_Calcio , known under the full name "Livorno_Calcio_SpA" has the ground in Stadio_Armando_Picchi , where 19238 members were active for the 2014 season .
direct_object A.F.C._Fylde ground The_Fylde With 3180 members and a home ground called The_Fylde , A.F.C._Fylde played in 2014 . The full name of which , is "Association_Football_Club_Fylde" .
apposition AC_Chievo_Verona ground Verona The fullname of AC_Chievo_Verona is "Associazione_Calcio_ChievoVerona_Srl" and their ground is in Verona . The club played in the 2014 season and has 39371 members .
coordinated_full_clauses AS_Livorno_Calcio ground Stadio_Armando_Picchi 19238 members strong "Livorno_Calcio_SpA" , AKA AS_Livorno_Calcio , played season 2014–15_Serie_B from their home stadium Stadio_Armando_Picchi .
juxtaposition AFC_Ajax_(amateurs) nickname "Joden_,_Godenzonen" The full name of AFC_Ajax_(amateurs) is "Amsterdamsche_Football_Club_Ajax_Amateurs" , they played in the 2014 season and have 5000 members . Their nickname is "Joden_,_Godenzonen" .
possessif AFC_Ajax_(amateurs) nickname "Lucky_Ajax" The nickname of AFC_Ajax_(amateurs) , or "Amsterdamsche_Football_Club_Ajax_Amateurs" , is "Lucky_Ajax" . AFC_Ajax_(amateurs) played in the 2014–15_Topklasse season and have 5000 members .
relative_subject AFC_Ajax_(amateurs) nickname "Joden_,_Godenzonen" The full name of AFC_Ajax_(amateurs) is "Amsterdamsche_Football_Club_Ajax_Amateurs" , they played in the 2014 season and have 5000 members . Their nickname is "Joden_,_Godenzonen" .
existential AC_Cesena ground Cesena AC_Cesena , who have a ground in Cesena , has 23900 members . They play in the Serie_B league which has Carpi_FC_1909 as previous champions .
coordinated_full_clauses Agremiação_Sportiva_Arapiraquense fullname "Agremiação_Sportiva_Arapiraquense" Agremiação_Sportiva_Arapiraquense ' s full name is "Agremiação_Sportiva_Arapiraquense" although their nickname is Alvinegro . The club has 17000 members and were in 2015_Campeonato_Brasileiro_Série_C in 2015 .
passive_voice Italy officialLanguage Italian_language AC_Lumezzane ' s ground is in Italy where the capital is Rome , the official language is Italian_language and the leader is Pietro_Grasso .
relative_object Italy demonym Italians AS_Gubbio_1910 ' s home ground is in Italy which is lead by Pietro_Grasso . The inhabitants of the country are known as Italians and their capital city is Rome .
passive_voice Italy capital Rome AC_Lumezzane ' s ground is in Italy where Rome is the capital , Italians live and the leader is Pietro_Grasso .
relative_subject (410777)_2009_FD orbitalPeriod 39447000.0 The celestial body known as (410777)_2009_FD and discovered by Spacewatch has an orbital period of 39447000.0 . It ' s epoch date is 2015-06-27 and has a periapsis of 88234300000.0 .
coordinated_full_clauses (15788)_1993_SB apoapsis 7715100000.0_kilometres Discovered by Roque_de_los_Muchachos_Observatory , (15788)_1993_SB , has an epoch 2006-03-06 . It has a periapsis of 3997100000000.0 and an apoapsis of , 7715100000.0_kilometres .
coordinated_full_clauses 107_Camilla apoapsis 560937000.0_kilometres Discovered by A._Storrs , 107_Camilla has an epoch 2006-12-31 . It has a periapsis of 479343000.0_kilometres and an apoapsis of 560937000.0_kilometres .
possessif (410777)_2009_FD mass 8.3_kilograms The epoch date of (410777)_2009_FD is 2015-06-27 and it was discovered by Spacewatch . It has a mass of 8.3_kilograms and a periapsis of 88234300000.0 .
possessif 107_Camilla apoapsis 560937000.0_kilometres 560937000.0_kilometres is the apoapais for 107_Camilla , which was discovered by A._Storrs . The epoch date for 107_Camilla is 2006-12-31 and it has a periapsis of 479343000.0_kilometres .
existential (15788)_1993_SB apoapsis 7715100000.0_kilometres Iwan_P._Williams discovered (15788)_1993_SB , it has an epoch 2006-03-06 , a periapsis of 3997100000000.0 . and an apoapsis of 7715100000.0_kilometres .
existential 107_Camilla apoapsis 560937000.0_kilometres Discovered by B._Zellner , 107_Camilla has the epoch 2006-12-31 . this asteroid has an apoapsis of 560937000.0_kilometres and a periapsis of 479343000.0_kilometres .
apposition (15788)_1993_SB apoapsis 7715100000.0_kilometres The celestial body , discovered by Iwan_P._Williams , known as (15788)_1993_SB , has a periapsis of 3997100000000.0 . It has an epoch 2006-03-06 and an apoapsis of 7715100000.0_kilometres .
apposition 107_Camilla apoapsis 560937000.0_kilometres 107_Camilla was discovered by C._Woods . It has a periapsis of 479343000.0_kilometres , an apoapsis of 560937000.0_kilometres and an epoch 2006-12-31 .
possessif (15788)_1993_SB apoapsis 7715100000.0_kilometres Donal_O'Ceallaigh discovered (15788)_1993_SB , which has an epoch 2006-03-06 and an apoapsis of 7715100000.0_kilometres . The periapsis of this celestial body measures 3997100000000.0 .
possessif (410777)_2009_FD apoapsis 259776702.47055_kilometres (410777)_2009_FD , discovered by Spacewatch , has an epoch 2015-06-27 . It has a periapsis of 88234300000.0 and an apoapsis of 259776702.47055_kilometres .
possessif 107_Camilla apoapsis 560937000.0_kilometres Discovered by B._Zellner , 107_Camilla has the epoch 2006-12-31 . this asteroid has an apoapsis of 560937000.0_kilometres and a periapsis of 479343000.0_kilometres .
coordinated_clauses (15788)_1993_SB apoapsis 7715100000.0_kilometres Donal_O'Ceallaigh discovered (15788)_1993_SB , which has an epoch 2006-03-06 and an apoapsis of 7715100000.0_kilometres . The periapsis of this celestial body measures 3997100000000.0 .
coordinated_clauses 1001_Gaussia orbitalPeriod 5.75_days 2015-06-27 is the epoch date for 1001_Gaussia which was discovered by Sergey_Belyavsky . It has an orbital period of 5.75_days and a periapsis measurement of 419113394.55312_kilometres .
coordinated_clauses (410777)_2009_FD orbitalPeriod 39447000.0 (410777)_2009_FD ( discovered by Spacewatch ); has an orbital period of 39447000.0 , a periapsis of 88234300000.0 and an epoch 2015-06-27 .
coordinated_clauses 107_Camilla apoapsis 560937000.0_kilometres Discovered by B._Zellner , 107_Camilla has the epoch 2006-12-31 . this asteroid has an apoapsis of 560937000.0_kilometres and a periapsis of 479343000.0_kilometres .
apposition (66063)_1998_RO1 periapsis 41498400000.0 2013-11-04 is the epoch date for (66063)_1998_RO1 which has an orbital period of 360.29_days . It has an apoapsis of 254989570.60815_kilometres and a periapsis of 41498400000.0 .
existential (410777)_2009_FD discoverer Spacewatch The celestial body known as (410777)_2009_FD ( discovered by Spacewatch ); has an orbital period of 39447000.0 , the epoch 2015-06-27 and the apoapsis of , 259776702.47055_kilometres .
direct_object 1099_Figneria escapeVelocity 0.0155_kilometres_per_second 1099_Figneria had am epoch 2006-12-31 and its orbital period is 179942000.0 . It has an apoapsis of 605718000.0_kilometres and its escape velocity is 0.0155_kilometres_per_second .
existential 11264_Claudiomaccone rotationPeriod 11473.9 11264_Claudiomaccone , with the epoch 2005-11-26 , has a rotation period of 11473.9 , an apoapsis of 475426000.0_kilometres and an orbital period of 1513.722_days .
apposition (66391)_1999_KW4 periapsis 29919600000.0 The celestial body known as (66391)_1999_KW4 has an epoch 2004-07-14 and an orbital period of 16244700.0 . It has a periapsis of 29919600000.0 and an apoapsis of 162164091.8388_kilometres .
apposition 109_Felicitas escapeVelocity 0.0473_kilometres_per_second 109_Felicitas has an escape velocity of 0.0473_kilometres_per_second , orbital period of 139705000.0 , apoapsis of 523329000.0_kilometres and an epoch date on the 2006-12-31 .
apposition 11264_Claudiomaccone rotationPeriod 11473.9 11264_Claudiomaccone has an epoch 2005-11-26 . It has an orbital period of 1513.722_days , an apoapsis of 475426000.0_kilometres and a rotation period of 11473.9 .
relative_subject 1097_Vicia periapsis 279142000000.0 1097_Vicia has a periapsis of 279142000000.0 and an orbital period of 135589000.0 . The epoch of 1097_Vicia is on 2006-12-31 and it has an apoapsis of , 511592000.0_kilometres .
relative_subject 110_Lydia periapsis 377016000000.0 2006-12-31 was the epoch date of 110_Lydia and its apoapsis is 440756000.0_kilometres . It has an orbital period of 142603000.0 and its periapsis is 377016000000.0 .
possessif 103_Hera periapsis 371240000.0_kilometres The apoapsis of 103_Hera is 437170000.0_kilometres and the periapsis is 371240000.0_kilometres . With an epoch date of 2011-08-27 it has an orbital period of 1622.213_days .
possessif 1099_Figneria mass 2.7_kilograms 1099_Figneria has an apoapsis of 605718000.0_kilometres and an orbital period of 179942000.0 . The epoch date is 2006-12-31 and it has a mass of 2.7_kilograms .
coordinated_full_clauses (19255)_1994_VK8 escapeVelocity 0.0925_kilometres_per_second The epoch of (19255)_1994_VK8 is 2006-12-31 . It has an orbital period of 8788850000.0 , an escape velocity of 0.0925_kilometres_per_second . and an apoapsis of 6603633000.0_kilometres .
coordinated_full_clauses 1101_Clematis mass 5.7_kilograms The epoch of 1101_Clematis is 2006-12-31 and has a mass of 5.7_kilograms . 1101_Clematis has an apoapsis of 520906000.0_kilometres and an orbital period of 183309000.0 .
coordinated_full_clauses 11264_Claudiomaccone averageSpeed 18.29_kilometres_per_second 11264_Claudiomaccone has an epoch 2005-11-26 and it ' s average speed is 18.29_kilometres_per_second . The celestial body has an orbital period of 1513.722_days and an apoapsis of 475426000.0_kilometres .
relative_subject 1099_Figneria periapsis 349206000000.0 1099_Figneria , has an orbital period of 179942000.0 and an apoapsis of 605718000.0_kilometres . With a periapsis of 349206000000.0 , 1099_Figneria , has the epoch 2006-12-31 .
relative_subject 11264_Claudiomaccone rotationPeriod 11473.9 11264_Claudiomaccone , with the epoch 2005-11-26 , has a rotation period of 11473.9 , an apoapsis of 475426000.0_kilometres and an orbital period of 1513.722_days .
possessif 1099_Figneria escapeVelocity 0.0155_kilometres_per_second 1099_Figneria , which has the epoch 2006-12-31 , has an escape velocity of 0.0155_kilometres_per_second . It ' s orbital period is 179942000.0 and the apoapsis is 605718000.0_kilometres .
relative_subject (15788)_1993_SB discoverer Iwan_P._Williams Iwan_P._Williams discovered (15788)_1993_SB on its epoch 2006-03-06 . That celestial body has an orbital period of 7729430000.0 and a periapsis of 3997100000000.0 .
relative_subject (66391)_1999_KW4 escapeVelocity 0 The asteroid called (66391)_1999_KW4 has an orbital period of 16244700.0 and an epoch 2004-07-14 . It also , has an escape velocity of 0 and a periapsis of 29919600000.0 .
relative_subject 109_Felicitas apoapsis 523329000.0_kilometres 109_Felicitas has a periapsis of 109_Felicitas is 283326000000.0 and an apoapsis of 523329000.0_kilometres . It has an orbital period of 139705000.0 and an epoch 2006-12-31 .
existential (19255)_1994_VK8 escapeVelocity 0.0925_kilometres_per_second (19255)_1994_VK8 has an escape velocity of 0.0925_kilometres_per_second , a periapsis of 6155910000000.0 , an orbital period of 8788850000.0 and an epoch 2006-12-31 .
existential (29075)_1950_DA mass 4.0_kilograms (29075)_1950_DA with the epoch 2011-Aug-27 , has an orbital period of 69862200.0 , a periapsis of 124950000000.0 and a mass of 4.0_kilograms .
existential (66391)_1999_KW4 apoapsis 162164091.8388_kilometres The asteroid (66391)_1999_KW4 , with an epoch 2004-07-14 , has an orbital period of 16244700.0 , an apoapsis of 162164091.8388_kilometres and a periapsis of 29919600000.0 .
existential 109_Felicitas escapeVelocity 0.0473_kilometres_per_second 109_Felicitas , with an escape velocity of 0.0473_kilometres_per_second , has an epoch 2006-12-31 , an orbital period of 139705000.0 and a periapsis of 283326000000.0 .
possessif (66063)_1998_RO1 apoapsis 254989570.60815_kilometres 2013-11-04 is the epoch date for (66063)_1998_RO1 which has an orbital period of 360.29_days . It has an apoapsis of 254989570.60815_kilometres and a periapsis of 41498400000.0 .
direct_object 110_Lydia mass 6.7_kilograms 110_Lydia ' s has a mass of 6.7_kilograms , orbital period of 142603000.0 , periapsis of 377016000000.0 and its epoch is the 2006-12-31 .
coordinated_full_clauses 1000_Piazzia escapeVelocity 0.0252_kilometres_per_second The dark asteroid called 1000_Piazzia has an orbital period of 488160.0 and its epoch is 2015-06-27 . Additionally , it has periapsis of 352497000000.0 and an escape velocity of 0.0252_kilometres_per_second .
relative_subject (66391)_1999_KW4 escapeVelocity 0 The epoch date for (66391)_1999_KW4 is 2004-07-14 and it has an escape velocity of 0 . It has an orbital period of 16244700.0 and a periapsis of 29919600000.0 .
relative_subject 103_Hera apoapsis 437170000.0_kilometres 103_Hera , has a periapsis measurement of 371240000.0_kilometres and an apoapsis of 437170000.0_kilometres . Its epoch is 2011-08-27 and it has an orbital period of 1622.213_days .
relative_subject 109_Felicitas escapeVelocity 0.0473_kilometres_per_second 109_Felicitas has an orbital period of 139705000.0 and a periapsis of 283326000000.0 . Its epoch date is 2006-12-31 and it has an escape velocity of 0.0473_kilometres_per_second .
passive_voice (66391)_1999_KW4 escapeVelocity 0 (66391)_1999_KW4 has an epoch 2004-07-14 , an escape velocity of 0 , an orbital period of 16244700.0 and a periapsis of 29919600000.0 .
passive_voice 1089_Tama formerName "1930_ST;_1952_HE4" The epoch of 1089_Tama ( formerly known as "1930_ST;_1952_HE4" ) is 2005-11-26 . It has an orbital period of 1202.846_days and a periapsis of 288749000000.0 .
passive_voice 1099_Figneria escapeVelocity 0.0155_kilometres_per_second 1099_Figneria has a periapsis of 349206000000.0 and an orbital period of 179942000.0 . It has the epoch 2006-12-31 and the escape velocity is 0.0155_kilometres_per_second .
passive_voice 110_Lydia apoapsis 440756000.0_kilometres 110_Lydia , with the epoch 2006-12-31 , has an orbital period of 142603000.0 , a periapsis of 377016000000.0 and an apoapsis of 440756000.0_kilometres .
relative_subject 1097_Vicia formerName "1928_PC" 1097_Vicia , formerly known as "1928_PC" , has an orbital period of 135589000.0 and a periapsis of 279142000000.0 . Its epoch is on 2006-12-31 .
coordinated_clauses (66063)_1998_RO1 discoverer Lincoln_Near-Earth_Asteroid_Research The Lincoln_Near-Earth_Asteroid_Research centre discovered the asteroid , (66063)_1998_RO1 , which has an orbital period of 360.29_days . With an epoch on 2013-11-04 , (66063)_1998_RO1 , has a perisapsis of 41498400000.0 Gm .
apposition (29075)_1950_DA mass 4.0_kilograms (29075)_1950_DA with the epoch 2011-Aug-27 , has an orbital period of 69862200.0 , a periapsis of 124950000000.0 and a mass of 4.0_kilograms .
apposition (66391)_1999_KW4 apoapsis 162164091.8388_kilometres The celestial body known as (66391)_1999_KW4 has an epoch 2004-07-14 and an orbital period of 16244700.0 . It has a periapsis of 29919600000.0 and an apoapsis of 162164091.8388_kilometres .
apposition 109_Felicitas apoapsis 523329000.0_kilometres 109_Felicitas epoch date is 2006-12-31 . It has an orbital period of 139705000.0 , a periapsis of 283326000000.0 and an apoapsis of 523329000.0_kilometres .
existential (15788)_1993_SB discoverer Roque_de_los_Muchachos_Observatory Discovered by Roque_de_los_Muchachos_Observatory , (15788)_1993_SB , has an epoch 2006-03-06 . It has a periapsis of 3997100000000.0 and an apoapsis of , 7715100000.0_kilometres .
existential (66391)_1999_KW4 escapeVelocity 0 (66391)_1999_KW4 has an epoch 2004-07-14 and an escape velocity of 0 . It has a periapsis of 29919600000.0 and an apoapsis of 162164091.8388_kilometres .
relative_subject (19255)_1994_VK8 orbitalPeriod 8788850000.0 The epoch of (19255)_1994_VK8 is 2006-12-31 . It has an orbital period of 8788850000.0 , a periapsis of 6155910000000.0 and an apoapsis of 6603633000.0_kilometres .
relative_subject 1099_Figneria orbitalPeriod 179942000.0 The epoch date for 1099_Figneria is 2006-12-31 . It has an orbital period of 179942000.0 , a periapsis measurement of 349206000000.0 and an apoapsis of 605718000.0_kilometres .
apposition (66063)_1998_RO1 orbitalPeriod 360.29_days (66063)_1998_RO1 has a periapsis of 41498400000.0 and an apoapsis of 254989570.60815_kilometres . It has an orbital period of 360.29_days and an epoch 2013-11-04 .
apposition (66391)_1999_KW4 orbitalPeriod 16244700.0 The celestial body known as (66391)_1999_KW4 has an epoch 2004-07-14 and an orbital period of 16244700.0 . It has a periapsis of 29919600000.0 and an apoapsis of 162164091.8388_kilometres .
apposition 1099_Figneria orbitalPeriod 179942000.0 The epoch date for 1099_Figneria is 2006-12-31 . It has an orbital period of 179942000.0 , a periapsis measurement of 349206000000.0 and an apoapsis of 605718000.0_kilometres .
possessif 103_Hera orbitalPeriod 1622.213_days The apoapsis of 103_Hera is 437170000.0_kilometres and the periapsis is 371240000.0_kilometres . With an epoch date of 2011-08-27 it has an orbital period of 1622.213_days .
direct_object 107_Camilla discoverer M._Gaffey 107_Camilla was discovered by M._Gaffey and its epoch date was 2006-12-31 . Its apoapsis is 560937000.0_kilometres and its periapsis is 479343000.0_kilometres .
possessif 1089_Tama orbitalPeriod 1202.846_days The epoch date of 1089_Tama is 2005-11-26 and it has an orbital period of 1202.846_days . It has a periapsis of 288749000000.0 and an apoapsis of 373513000.0_kilometres .
apposition 1089_Tama orbitalPeriod 1202.846_days The epoch date of 1089_Tama is 2005-11-26 and it has an orbital period of 1202.846_days . It has a periapsis of 288749000000.0 and an apoapsis of 373513000.0_kilometres .
coordinated_clauses 1099_Figneria orbitalPeriod 179942000.0 The epoch date for 1099_Figneria , which has an orbital period of 179942000.0 , is 2006-12-31 . It has a periapsis measurement of . 349206000000.0 and an apoapsis of 605718000.0_kilometres .
coordinated_clauses 110_Lydia periapsis 377016000000.0 110_Lydia has an orbital period of 142603000.0 and a mass of 6.7_kilograms . 2006-12-31 is the epoch of 110_Lydia which has a periapsis of , 377016000000.0 .
coordinated_clauses (29075)_1950_DA periapsis 124950000000.0 (29075)_1950_DA , has a mass of 4.0_kilograms and an epoch 2011-Aug-27 . Its orbital period is , 69862200.0 and it has a periapsis of , 124950000000.0 .
coordinated_clauses 1097_Vicia apoapsis 511592000.0_kilometres 1097_Vicia , with the epoch 2006-12-31 , has a mass of 9.8_kilograms , an apoapsis of 511592000.0_kilometres and an orbital period of 135589000.0 .
coordinated_clauses 1101_Clematis periapsis 445895000000.0 1101_Clematis , with an orbital period of 183309000.0 and a periapsis of 445895000000.0 , has a mass of 5.7_kilograms and an epoch date of 2006-12-31 .
apposition 109_Felicitas periapsis 283326000000.0 109_Felicitas has an epoch 2006-12-31 and a mass of 7.5_kilograms . The periapsis measurement is 283326000000.0 . and it has an orbital period of 139705000.0 .
apposition 1101_Clematis periapsis 445895000000.0 1101_Clematis , with an orbital period of 183309000.0 and a periapsis of 445895000000.0 , has a mass of 5.7_kilograms and an epoch date of 2006-12-31 .
relative_subject 1101_Clematis apoapsis 520906000.0_kilometres The epoch of 1101_Clematis is 2006-12-31 and has a mass of 5.7_kilograms . 1101_Clematis has an apoapsis of 520906000.0_kilometres and an orbital period of 183309000.0 .
apposition 1101_Clematis epoch 2006-12-31 1101_Clematis , with the epoch 2006-12-31 , has an orbital period of 183309000.0 and a mass of 5.7_kilograms . The periapsis measurement for 1101_Clematis is 445895000000.0 .
passive_voice 107_Camilla epoch 2006-12-31 Discovered by B._Zellner , 107_Camilla has the epoch 2006-12-31 . this asteroid has an apoapsis of 560937000.0_kilometres and a periapsis of 479343000.0_kilometres .
coordinated_clauses 1001_Gaussia periapsis 419113394.55312_kilometres 2015-06-27 is the epoch date for 1001_Gaussia which was discovered by Sergey_Belyavsky . It has an orbital period of 5.75_days and a periapsis measurement of 419113394.55312_kilometres .
passive_voice (410777)_2009_FD periapsis 88234300000.0 The celestial body known as (410777)_2009_FD , has an orbital period of 39447000.0 . Discovered by Spacewatch , it has an apoapsis of 259776702.47055_kilometres and a periapsis of , 88234300000.0 .
coordinated_full_clauses (410777)_2009_FD epoch 2015-06-27 (410777)_2009_FD has an epoch 2015-06-27 and an orbital period of 39447000.0 . It was discovered by Spacewatch and has an apoapsis of 259776702.47055_kilometres .
coordinated_full_clauses (15788)_1993_SB epoch 2006-03-06 (15788)_1993_SB has an epoch 2006-03-06 and was discovered by Donal_O'Ceallaigh . It has an orbital period of 7729430000.0 and a periapsis of 3997100000000.0 .
passive_voice (66063)_1998_RO1 epoch 2013-11-04 The Lincoln_Near-Earth_Asteroid_Research centre discovered the asteroid , (66063)_1998_RO1 , which has an orbital period of 360.29_days . With an epoch on 2013-11-04 , (66063)_1998_RO1 , has a perisapsis of 41498400000.0 Gm .
apposition 10_Hygiea escapeVelocity 0.21_kilometres_per_second 10_Hygiea - formerly known as "A900_GA" - had its epoch 2015-06-27 . It has am escape velocity of 0.21_kilometres_per_second and its periapsis is 416136000000.0 .
direct_object 1101_Clematis orbitalPeriod 183309000.0 "1928_SJ" AM was the former name of 1101_Clematis and its epoch date was 2006-12-31 . Its periapsis is 445895000000.0 and its orbital period is 183309000.0 .
coordinated_full_clauses 1097_Vicia orbitalPeriod 135589000.0 1097_Vicia , formerly known as "1928_PC" , has an orbital period of 135589000.0 and a periapsis of 279142000000.0 . Its epoch is on 2006-12-31 .
passive_voice 1089_Tama orbitalPeriod 1202.846_days 1089_Tama ( previously "A919_HA;_1927_WB;" ) has ; an orbital period of 1202.846_days , a periapsis of 288749000000.0 and the epoch date 2005-11-26 .
relative_subject 1001_Gaussia discoverer Sergey_Belyavsky The epoch of 1001_Gaussia ( formerly known as "1923_OAA907_XC" ) is 2015-06-27 . It was discovered by Sergey_Belyavsky and has a periapsis of 419113394.55312_kilometres .
existential 1089_Tama apoapsis 373513000.0_kilometres 1089_Tama ( formerly "A919_HA;_1927_WB;" ); has a periapsis of 288749000000.0 , an apoapsis of 373513000.0_kilometres and the epoch date 2005-11-26 .
apposition 1001_Gaussia discoverer Sergey_Belyavsky The epoch of 1001_Gaussia ( formerly known as "1923_OAA907_XC" ) is 2015-06-27 . It was discovered by Sergey_Belyavsky and has a periapsis of 419113394.55312_kilometres .
coordinated_clauses (66063)_1998_RO1 periapsis 41498400000.0 (66063)_1998_RO1 , which was once known as "1999_SN5" , has an orbital period of 360.29_days . This celestial body has an epoch date of 2013-11-04 and a periapsis of 41498400000.0 .
coordinated_full_clauses 1101_Clematis periapsis 445895000000.0 1101_Clematis , formerly known as "1928_SJ" , has an orbital period of 183309000.0 and a periapsis of 445895000000.0 . Its epoch date is 2006-12-31 .
existential 1000_Piazzia periapsis 352497000000.0 1000_Piazzia , once known as "1967_ED" , has an orbital period of 488160.0 , a periapsis of 352497000000.0 and the epoch 2015-06-27 .
coordinated_full_clauses 1089_Tama periapsis 288749000000.0 The epoch of 1089_Tama ( formerly known as "1930_ST;_1952_HE4" ) is 2005-11-26 . It has an orbital period of 1202.846_days and a periapsis of 288749000000.0 .
existential (66063)_1998_RO1 orbitalPeriod 360.29_days (66063)_1998_RO1 , with an average temperature of 265.0_kelvins and an epoch 2013-11-04 , has an orbital period of 360.29_days and an apoapsis of 254989570.60815_kilometres .
direct_object James_Craig_Watson stateOfOrigin Canada 101_Helena was discovered by James_Craig_Watson , who originated from Canada , studied at the University_of_Michigan and died from Peritonitis .
direct_object James_Craig_Watson deathCause Peritonitis 101_Helena , was discovered by James_Craig_Watson , who studied at the University_of_Michigan and died from Peritonitis , in Madison_,_Wisconsin .
apposition James_Craig_Watson stateOfOrigin Canada James_Craig_Watson , who discovered 101_Helena , originated from Canada , studied at the University_of_Michigan and died in Madison_,_Wisconsin .
coordinated_clauses James_Craig_Watson deathCause Peritonitis 101_Helena was discovered by James_Craig_Watson , who originated from Canada , studied at the University_of_Michigan and died from Peritonitis .
direct_object James_Craig_Watson nationality Canada James_Craig_Watson of Canada passed in Madison_,_Wisconsin from Peritonitis . He was the discoverer of 103_Hera .
relative_subject Walter_Baade nationality Germany The Germany , Walter_Baade , was born in Preußisch_Oldendorf , discovered 1036_Ganymed and graduated from the University_of_Göttingen .
relative_subject Walter_Baade deathPlace West_Germany 1036_Ganymed was discovered by Walter_Baade . He died in West_Germany but was born in the German_Empire and studied at University_of_Göttingen .
coordinated_clauses Walter_Baade almaMater University_of_Göttingen Walter_Baade , who was born in Preußisch_Oldendorf , discovered 1036_Ganymed . He studied at The University_of_Göttingen and his doctoral student was Halton_Arp .
relative_subject James_Craig_Watson deathPlace Madison_,_Wisconsin James_Craig_Watson of Canada passed in Madison_,_Wisconsin from Peritonitis . He was the discoverer of 103_Hera .
relative_subject 107_Camilla discovered 2001-03-01 N._R._Pogson was born in Nottingham and died in Chennai . He discovered the 107_Camilla asteroid on the 2001-03-01 .
apposition 11264_Claudiomaccone discoverer Nikolai_Chernykh Nikolai_Chernykh discovered 11264_Claudiomaccone , which has an average speed of 18.29_kilometres_per_second . It also , has an apoapsis of 475426000.0_kilometres and an orbital period of , 1513.722_days .
apposition 1089_Tama periapsis 288749000000.0 The epoch of 1089_Tama is 2005-11-26 and it has a periapsis of 288749000000.0 . It was formerly called "A894_VA;_A904_VD;" and it has an apoapsis of 373513000.0_kilometres .
direct_object 11th_Mississippi_Infantry_Monument state "Pennsylvania" The 11th_Mississippi_Infantry_Monument is located in "Pennsylvania" , "United_States" . It was established in 2000 and categorised as Contributing_property .
apposition 11th_Mississippi_Infantry_Monument established 2000 The municipality of Gettysburg_,_Pennsylvania in Adams_County_,_Pennsylvania is the location of the 11th_Mississippi_Infantry_Monument which was erected in 2000 and categorised as Contributing_property .
passive_voice Adams_County_,_Pennsylvania has_to_its_north Cumberland_County_,_Pennsylvania The 11th_Mississippi_Infantry_Monument is a Contributing_property located in Adams_County_,_Pennsylvania , which has Cumberland_County_,_Pennsylvania to its north and Carroll_County_,_Maryland to its southeast .
coordinated_clauses Azerbaijan leader Artur_Rasizade Artur_Rasizade was an Azerbaijan leader . Baku_Turkish_Martyrs'_Memorial is made of "Red_granite_and_white_marble" , is dedicated to the "Ottoman_Army_soldiers_killed_in_the_Battle_of_Baku" and located in Azerbaijan .
relative_adverb England capital London The Dead_Man's_Plack is located in England , of which the capital city is London . Cornish_language is a language spoken in England and one of the ethnic groups are The British_Arabs .
existential 1634:_The_Ram_Rebellion numberOfPages "512" 1634:_The_Ram_Rebellion was written by Virginia_DeMarce and has "512" pages . It can be found as an E-book and has the ISBN number of "1-4165-2060-0" .
existential A_Wizard_of_Mars OCLC_number 318875313 A_Wizard_of_Mars ( OCLC 318875313 , ISBN "978-0-15-204770-2" ) was written by Diane_Duane and was published in "Print" .
juxtaposition 1634:_The_Bavarian_Crisis numberOfPages "448" 1634:_The_Bavarian_Crisis , available in Hardcover and containing "448" pages , was written by Eric_Flint . The ISBN number is "978-1-4165-4253-7" .
possessif 1634:_The_Bavarian_Crisis numberOfPages "448" 1634:_The_Bavarian_Crisis is written by "Virginia_DeMarce_and_Eric_Flint" and is in "Print" . The book has "448" pages and the ISBN number is "978-1-4165-4253-7" .
coordinated_clauses 1634:_The_Ram_Rebellion numberOfPages "512" 1634:_The_Ram_Rebellion was written by "Eric_Flint_,_Virginia_DeMarce_,_et_al." . and has "512" pages . It can be found as an E-book and the ISBN number of "1-4165-2060-0" .
direct_object 1634:_The_Ram_Rebellion numberOfPages "512" 1634:_The_Ram_Rebellion ( ISBN "1-4165-2060-0" ) written by "Eric_Flint_,_Virginia_DeMarce_,_et_al." is "512" pages long and available in E-book .
existential A_Loyal_Character_Dancer OCLC_number 49805501 The book A_Loyal_Character_Dancer , written by Qiu_Xiaolong , was published in Hardcover . It has the ISBN number "1-56947-301-3" and OCLC number 49805501 .
apposition 1634:_The_Ram_Rebellion numberOfPages "512" 1634:_The_Ram_Rebellion is available as an E-book and is "512" pages long . It was written by "Eric_Flint_,_Virginia_DeMarce_,_et_al." . and has the ISBN number "1-4165-2060-0" .
apposition 1634:_The_Ram_Rebellion numberOfPages "512" The Hardcover book , 1634:_The_Ram_Rebellion was written by "Eric_Flint_,_Virginia_DeMarce_,_et_al." . It has "512" pages altogether and has an ISBN number of "1-4165-2060-0" .
apposition 1634:_The_Bavarian_Crisis numberOfPages "448" 1634:_The_Bavarian_Crisis has "448" pages can be located by it ' s ISBN number "978-1-4165-4253-7" The authors are "Virginia_DeMarce_and_Eric_Flint" and was put in "Print" .
existential 1634:_The_Ram_Rebellion numberOfPages "512" 1634:_The_Ram_Rebellion was written by "Eric_Flint_,_Virginia_DeMarce_,_et_al." . and has the ISBN number of "1-4165-2060-0" . It is published in Hardcover and has "512" pages .
juxtaposition A_Fortress_of_Grey_Ice OCLC_number 51969173 The author of A_Fortress_of_Grey_Ice , available in "Print" , is J._V._Jones and has an OCLC number of 51969173 and the ISBN number is "0-7653-0633-6" .
passive_voice 1634:_The_Ram_Rebellion numberOfPages "512" Eric_Flint is the author of 1634:_The_Ram_Rebellion which is a Hardcover book . It has "512" pages and the ISBN number is "1-4165-2060-0" .
relative_subject 1634:_The_Ram_Rebellion numberOfPages "512" Virginia_DeMarce ' s novel 1634:_The_Ram_Rebellion , printed in Hardcover , has "512" pages . The ISBN code is "1-4165-2060-0" .
relative_subject A_Wizard_of_Mars OCLC_number 318875313 The author of A_Wizard_of_Mars is Diane_Duane and the "Print" format has an OCLC number of 318875313 and an ISBN number of "978-0-15-204770-2" .
coordinated_clauses Above_the_Veil OCLC_number 46451790 Garth_Nix wrote Above_the_Veil which is available in Hardcover . This book has the OCLC number 46451790 and an ISBN number "0-439-17685-9" .
coordinated_full_clauses 1634:_The_Bavarian_Crisis numberOfPages "448" 1634:_The_Bavarian_Crisis , that can be located by it ' s ISBN number "978-1-4165-4253-7" , comes in Hardcover and has "448" pages . The authors of the book are "Virginia_DeMarce_and_Eric_Flint" .
coordinated_full_clauses Above_the_Veil OCLC_number 46451790 Above_the_Veil is available in Hardcover and was written by Garth_Nix . It has the OCLC number 46451790 and the ISBN number "0-439-17685-9" .
juxtaposition 1634:_The_Bavarian_Crisis numberOfPages "448" 1634:_The_Bavarian_Crisis has "448" pages and was written by "Virginia_DeMarce_and_Eric_Flint" . The "Print" copies have the ISBN number of "978-1-4165-4253-7" .
juxtaposition 1634:_The_Ram_Rebellion numberOfPages "512" 1634:_The_Ram_Rebellion was written by Virginia_DeMarce and has "512" pages . It can be found as an E-book and has the ISBN number of "1-4165-2060-0" .
direct_object 1634:_The_Ram_Rebellion ISBN_number "1-4165-2060-0" 1634:_The_Ram_Rebellion ( ISBN "1-4165-2060-0" ) written by "Eric_Flint_,_Virginia_DeMarce_,_et_al." is "512" pages long and available in E-book .
apposition 1634:_The_Bavarian_Crisis ISBN_number "978-1-4165-4253-7" 1634:_The_Bavarian_Crisis , which was written by Eric_Flint , has "448" pages and was put into "Print" with the ISBN number "978-1-4165-4253-7" .
apposition 1634:_The_Ram_Rebellion ISBN_number "1-4165-2060-0" 1634:_The_Ram_Rebellion , written by Eric_Flint , has "512" pages and was published in "Print" with the ISBN number "1-4165-2060-0" .
relative_subject 1634:_The_Bavarian_Crisis ISBN_number "978-1-4165-4253-7" 1634:_The_Bavarian_Crisis has "448" pages can be located by it ' s ISBN number "978-1-4165-4253-7" The authors are "Virginia_DeMarce_and_Eric_Flint" and was put in "Print" .
relative_subject 1634:_The_Ram_Rebellion ISBN_number "1-4165-2060-0" The book titled 1634:_The_Ram_Rebellion by Eric_Flint is a Hardcover , has "512" pages and an ISBN number of "1-4165-2060-0" .
coordinated_clauses 1634:_The_Bavarian_Crisis ISBN_number "978-1-4165-4253-7" 1634:_The_Bavarian_Crisis , available in Hardcover and containing "448" pages , was written by Eric_Flint . The ISBN number is "978-1-4165-4253-7" .
apposition Eric_Flint influencedBy Robert_A._Heinlein Eric_Flint ( born in Burbank_,_California ) is the author of 1634:_The_Bavarian_Crisis , which was preceded by 1634:_The_Ram_Rebellion . Eric_Flint was influenced by Robert_A._Heinlein .
apposition Eric_Flint influencedBy Robert_A._Heinlein Eric_Flint is the author of 1634:_The_Bavarian_Crisis , which was preceded by 1634:_The_Ram_Rebellion . Eric_Flint was influenced by Robert_A._Heinlein and was born in Burbank_,_California .
apposition Eric_Flint birthPlace Burbank_,_California Eric_Flint , who was born in Burbank_,_California , wrote the book " 1634:_The_Ram_Rebellion " which had been preceded by 1634:_The_Galileo_Affair , which was influenced by Robert_A._Heinlein .
existential Eric_Flint birthPlace Burbank_,_California 1634:_The_Bavarian_Crisis , written by Eric_Flint born in Burbank_,_California , was preceded by 1634:_The_Baltic_War and "DeMarce_short_stories_in_the_The_Grantville_Gazettes" .
direct_object 1634:_The_Bavarian_Crisis precededBy 1634:_The_Ram_Rebellion Eric_Flint is the author of 1634:_The_Bavarian_Crisis , which was preceded by 1634:_The_Ram_Rebellion . Eric_Flint was influenced by Robert_A._Heinlein and was born in Burbank_,_California .
relative_object Ireland ethnicGroup White_people A_Long_Long_Way was written in Ireland and published by Viking_Press . Penguin_Random_House is the parent company of Viking_Press . White_people are an ethnic group in Ireland .
relative_adverb United_Kingdom language English_language In the United_Kingdom , the leader ( Queen ) is Elizabeth_II , English_language is the primary language and the AIDS_(journal) was also published here by Lippincott_Williams_&_Wilkins .
passive_voice Lippincott_Williams_&_Wilkins parentCompany Wolters_Kluwer AIDS_(journal) , published by Lippincott_Williams_&_Wilkins , is from the United_Kingdom ( led by Elizabeth_II ). The parent company of Lippincott is Wolters_Kluwer .
juxtaposition United_Kingdom leaderName Elizabeth_II London is the capital city of the United_Kingdom where David_Cameron is a leader along with Elizabeth_II . The AIDS_(journal) is published in the United_Kingdom by Lippincott_Williams_&_Wilkins .
possessif A_Long_Long_Way followedBy The_Secret_Scripture Penguin_Random_House is the parent company of Viking_Press which is located in the United_States . They ' ve published A_Long_Long_Way which was followed by The_Secret_Scripture .
possessif AIP_Advances publisher American_Institute_of_Physics Published by American_Institute_of_Physics , AIP_Advances was edited by A.T._Charlie_Johnson who ' s almaMater is Harvard_University and who is the doctoral advisor for Michael_Tinkham .
passive_voice A.T._Charlie_Johnson nationality United_States The editor of AIP_Advances is A.T._Charlie_Johnson , who is from the United_States . He is doctoral advisor to Michael_Tinkham and his almaMater is Stanford_University .
passive_voice A.T._Charlie_Johnson doctoralAdvisor Michael_Tinkham The editor of AIP_Advances is A.T._Charlie_Johnson , who is from the United_States . He is doctoral advisor to Michael_Tinkham and his almaMater is Stanford_University .
passive_voice AIP_Advances ISSN_number "2158-3226" A.T._Charlie_Johnson lives in the United_States and his alma mater is Stanford_University . He is the editor of AIP_Advances which has the ISSN number of "2158-3226" .
relative_subject A.T._Charlie_Johnson residence United_States A.T._Charlie_Johnson lives in the United_States and is the editor of AIP_Advances published by the American_Institute_of_Physics . His alma mater is Harvard_University .
coordinated_full_clauses A_Fortress_of_Grey_Ice mediaType "Print" J._V._Jones authored A_Fortress_of_Grey_Ice , made available in "Print" it ' s OCLC and ISBN numbers are 51969173 and "0-7653-0633-6" respectively .
passive_voice Aenir mediaType Paperback Aenir , written by Garth_Nix , has the OCLC number 45644811 , the ISBN number "0-439-17684-0" and is available in Paperback .
coordinated_clauses Above_the_Veil mediaType Hardcover Above_the_Veil was written by Garth_Nix and is available in Hardcover . It has an ISBN number "0-439-17685-9" and an OCLC number 46451790 .
coordinated_clauses A_Fortress_of_Grey_Ice ISBN_number "0-7653-0633-6" A_Fortress_of_Grey_Ice was written by J._V._Jones and is available in "Print" . Th book which has the ISBN number "0-7653-0633-6" . It also has the OCLC number 51969173 .
existential A_Wizard_of_Mars ISBN_number "978-0-15-204770-2" A_Wizard_of_Mars ( OCLC 318875313 , ISBN "978-0-15-204770-2" ) was written by Diane_Duane and was published in "Print" .
passive_voice United_States capital Washington The United_States has an ethnic group called Asian_Americans and the capital is Washington . Alcatraz_Versus_the_Evil_Librarians comes from there and was written in English_language .
relative_adverb English_language spokenIn Great_Britain Alcatraz_Versus_the_Evil_Librarians is written in English_language which is the language spoken in Great_Britain . The book originates from the United_States where the Asian_Americans are one of the ethnic groups ,.
relative_adverb English_language spokenIn Great_Britain A_Fortress_of_Grey_Ice is from the United_States and is written in the English_language ( spoken in Great_Britain ). Asian_Americans live in the United_States .
relative_adverb United_States leaderName Barack_Obama A_Wizard_of_Mars is written in English_language and was published in the United_States , where Barack_Obama is the President and Asian_Americans is one of the ethnic groups .
coordinated_clauses English_language spokenIn Great_Britain A_Loyal_Character_Dancer is written in English_language spoken in Great_Britain . It is published in the United_States where African_Americans are an ethnic group .
coordinated_clauses English_language spokenIn Great_Britain English_language is spoken in Great_Britain and is the language used in A_Severed_Wasp . The book originates from the United_States where the African_Americans are an ethnic group ,.
coordinated_clauses English_language spokenIn Great_Britain The book Alcatraz_Versus_the_Evil_Librarians is written in English_language spoken in Great_Britain . The book comes from the United_States , where African_Americans are an ethnic group .
relative_adverb English_language spokenIn Great_Britain A_Severed_Wasp is from the United_States and is in English_language , also spoken in Great_Britain . Asian_Americans are one of the ethnic groups in the United_States .
relative_subject United_States capital Washington The book Alcatraz_Versus_the_Evil_Librarians is written in English_language , in the United_States . The main language there is English_language , the capital is Washington and Asian_Americans are one ethnic group .
coordinated_full_clauses A_Loyal_Character_Dancer publisher Soho_Press A_Loyal_Character_Dancer was published in English_language by Soho_Press . It originates from the United_States where the Native_Americans are an ethnic group .
coordinated_full_clauses English_language spokenIn Great_Britain The book Alcatraz_Versus_the_Evil_Librarians is written in English_language spoken in Great_Britain . The book come from the United_States , where White_Americans are an ethnic group .
coordinated_clauses English_language spokenIn Great_Britain Alcatraz_Versus_the_Evil_Librarians was written in English_language which is spoken in Great_Britain . The book originates from the United_States where one of the ethnic groups are the Native_Americans .
coordinated_clauses United_States leaderName Barack_Obama A_Severed_Wasp was written in English_language and is from the United_States where Native_Americans are an ethic group and where Barack_Obama is president .
apposition English_language spokenIn Great_Britain English_language is spoken in Great_Britain and is the language Alcatraz_Versus_the_Evil_Librarians was written in . The book Alcatraz_Versus_the_Evil_Librarians comes from the United_States where there is an ethnic group called Asian_Americans .
relative_subject United_States ethnicGroup African_Americans A_Loyal_Character_Dancer is written in English_language spoken in Great_Britain . It is published in the United_States where African_Americans are an ethnic group .
relative_subject United_States ethnicGroup White_Americans The book Alcatraz_Versus_the_Evil_Librarians is written in English_language spoken in Great_Britain . The book come from the United_States , where White_Americans are an ethnic group .
existential United_States ethnicGroup African_Americans A_Severed_Wasp was written in English_language ( the language originated in Great_Britain ) in the United_States . The United_States has many ethnic groups , including African_Americans .
coordinated_clauses United_States ethnicGroup African_Americans A_Severed_Wasp was written in English_language ( the language originated in Great_Britain ) in the United_States . The United_States has many ethnic groups , including African_Americans .
existential A_Fortress_of_Grey_Ice author J._V._Jones A_Fortress_of_Grey_Ice , written by J._V._Jones and produced in Hardcover , has the ISBN number "0-7653-0633-6" and the OCLC number 51969173 .
coordinated_full_clauses A_Long_Long_Way numberOfPages "292" A_Long_Long_Way , available in Hardcover , has "292" pages . The OCLC number of the work is 57392246 and has an ISBN number of "0-670-03380-4" .
coordinated_full_clauses A_Severed_Wasp numberOfPages "388" The book A_Severed_Wasp is available in "Print" and has the OCLC number 8805735 and the ISBN number "0-374-26131-8" . The book has a total of "388" pages .
passive_voice A_Glastonbury_Romance numberOfPages "1174" A_Glastonbury_Romance , available in Hardcover , has "1174" pages . The ISBN # is : "0-7156-3648-0" and OCLC #: 76798317 .
passive_voice Alcatraz_Versus_the_Evil_Librarians numberOfPages "320" Alcatraz_Versus_the_Evil_Librarians , a "320" page Hardcover has the OCLC number 78771100 and ISBN number "0-439-92550-9" .
possessif Above_the_Veil author Garth_Nix Above_the_Veil is written by Garth_Nix and is produced in "Print" . The OCLC number is 46451790 and the ISBN number is "0-439-17685-9" .
coordinated_clauses A_Severed_Wasp numberOfPages "388" A_Severed_Wasp is a Hardcover book with "388" pages . It ' s OCLC number is 8805735 and the ISBN number is "0-374-26131-8" .
apposition A_Fortress_of_Grey_Ice author J._V._Jones A_Fortress_of_Grey_Ice , by J._V._Jones , has the OCLC number 51969173 , the ISBN number "0-7653-0633-6" and is available in Hardcover .
apposition A_Long_Long_Way numberOfPages "292" A_Long_Long_Way is available in Hardcover and has "292" pages . The book has the ISBN number "0-670-03380-4" and the OCLC number 57392246 .
apposition A_Loyal_Character_Dancer author Qiu_Xiaolong The book A_Loyal_Character_Dancer , written by Qiu_Xiaolong , was published in Hardcover . It has the ISBN number "1-56947-301-3" and OCLC number 49805501 .
passive_voice 1634:_The_Bavarian_Crisis author "Virginia_DeMarce_and_Eric_Flint" The authors of 1634:_The_Bavarian_Crisis are "Virginia_DeMarce_and_Eric_Flint" . It has "448" pages , an ISBN number of "978-1-4165-4253-7" is available in Hardcover .
passive_voice A_Long_Long_Way OCLC_number 57392246 A_Long_Long_Way , available in Hardcover , has "292" pages . The OCLC number of the work is 57392246 and has an ISBN number of "0-670-03380-4" .
relative_subject 1634:_The_Ram_Rebellion author Eric_Flint 1634:_The_Ram_Rebellion by Eric_Flint is "512" pages , published in "Print" and has the ISBN number "1-4165-2060-0" .
possessif 1634:_The_Bavarian_Crisis author "Virginia_DeMarce_and_Eric_Flint" 1634:_The_Bavarian_Crisis is written by "Virginia_DeMarce_and_Eric_Flint" and is in "Print" . The book has "448" pages and the ISBN number is "978-1-4165-4253-7" .
apposition 1634:_The_Bavarian_Crisis author Eric_Flint The ISBN number of 1634:_The_Bavarian_Crisis is "978-1-4165-4253-7" . It was written by Eric_Flint , has "448" pages and is available in "Print" form .
coordinated_clauses 1634:_The_Bavarian_Crisis author Eric_Flint 1634:_The_Bavarian_Crisis , available in Hardcover and containing "448" pages , was written by Eric_Flint . The ISBN number is "978-1-4165-4253-7" .
apposition 1634:_The_Bavarian_Crisis author Virginia_DeMarce 1634:_The_Bavarian_Crisis is a Hardcover publication written by Virginia_DeMarce . It has "448" pages and has the ISBN number "978-1-4165-4253-7" .
apposition Aenir author Garth_Nix Garth_Nix is the author of Aenir which has the ISBN number of "0-439-17684-0" . It has "233" pages and is available in Paperback .
passive_voice 1634:_The_Ram_Rebellion author Eric_Flint Eric_Flint wrote the E-book " 1634:_The_Ram_Rebellion ". The novel is "512" pages long and the ISBN code is "1-4165-2060-0" .
coordinated_clauses 1634:_The_Bavarian_Crisis author "Virginia_DeMarce_and_Eric_Flint" The authors of 1634:_The_Bavarian_Crisis are "Virginia_DeMarce_and_Eric_Flint" . It has "448" pages , an ISBN number of "978-1-4165-4253-7" is available in Hardcover .
coordinated_clauses Alcatraz_Versus_the_Evil_Librarians OCLC_number 78771100 Alcatraz_Versus_the_Evil_Librarians is published in Hardcover ( "320" pages ). The OCLC and ISBN codes are 78771100 and "0-439-92550-9" , respectively .
existential A_Glastonbury_Romance ISBN_number "0-7156-3648-0" A_Glastonbury_Romance , available in Hardcover , has "1174" pages . The ISBN # is : "0-7156-3648-0" and OCLC #: 76798317 .
direct_object Alcatraz_Versus_the_Evil_Librarians ISBN_number "0-439-92550-9" Alcatraz_Versus_the_Evil_Librarians is published in Hardcover ( "320" pages ). The OCLC and ISBN codes are 78771100 and "0-439-92550-9" , respectively .
passive_voice A_Long_Long_Way ISBN_number "0-670-03380-4" A_Long_Long_Way is available in Hardcover and has "292" pages . The book has the ISBN number "0-670-03380-4" and the OCLC number 57392246 .
passive_voice A_Glastonbury_Romance ISBN_number "0-7156-3648-0" A_Glastonbury_Romance , with "1174" pages , is available in "Print" . Its ISBN number is "0-7156-3648-0" and its OCLC number is 76798317 .
possessif Alcatraz_Versus_the_Evil_Librarians ISBN_number "0-439-92550-9" Alcatraz_Versus_the_Evil_Librarians is published in Hardcover ( "320" pages ). The OCLC and ISBN codes are 78771100 and "0-439-92550-9" , respectively .
passive_voice A_Long_Long_Way followedBy The_Secret_Scripture The_Secret_Scripture followed the book A_Long_Long_Way published by Viking_Press . A_Long_Long_Way comes from Ireland where an ethnic group is White_people .
relative_adverb United_States ethnicGroup Asian_Americans A_Loyal_Character_Dancer was published by Soho_Press in the United_States . The language of the country is English_language and one of it ' s ethnic groups is Asian_Americans .
relative_adverb English_language spokenIn Great_Britain A_Loyal_Character_Dancer is published by Soho_Press in the United_States . The language of both the United_States and Great_Britain is English_language .
relative_subject United_States language English_language A_Loyal_Character_Dancer was published by Soho_Press located in the United_States . The language of the United_States is English_language and one of United_States ethnic groups is Native_Americans .
relative_adverb A_Loyal_Character_Dancer language English_language A_Loyal_Character_Dancer is written in English_language and published by Soho_Press who are based in the United_States . In addition Native_Americans are an ethnic group in the United_States .
existential United_States language English_language The United_States has an ethnic group called Asian_Americans but the language spoken there is English_language . A_Loyal_Character_Dancer is published by Soho_Press in the United_States .
existential Into_Battle_(novel) followedBy The_Violet_Keystone Above_the_Veil Viel is an Australians novel and the sequel to Aenir and Castle_(novel) . It was followed by Into_Battle_(novel) and The_Violet_Keystone .
relative_subject Cornell_University state New_York Affiliated with the Association_of_Public_and_Land-grant_Universities , Cornell_University , publisher of Administrative_Science_Quarterly , is the located in the city of Ithaca_,_New_York found in the state of New_York .
apposition Cornell_University president Elizabeth_Garrett Elizabeth_Garrett is president of Cornell_University in Ithaca_,_New_York . The University is affiliated with the Association_of_American_Universities and publishes Administrative_Science_Quarterly .
passive_voice Cornell_University state New_York Affiliated with the Association_of_Public_and_Land-grant_Universities , Cornell_University , publisher of Administrative_Science_Quarterly , is the located in the city of Ithaca_,_New_York found in the state of New_York .
coordinated_clauses Into_Battle_(novel) followedBy The_Violet_Keystone The novel " Into_Battle_(novel) ", that was followed by The_Violet_Keystone , followed " Above_the_Veil ", that was preceded By Aenir , which was written by Australians and after Castle_(novel) .
possessif Johns_Hopkins_University_Press parentCompany Johns_Hopkins_University The book American_Journal_of_Mathematics has the ISSN number "1080-6377" . It is published by Johns_Hopkins_University_Press , Johns_Hopkins_University , United_States .
relative_adverb A_Loyal_Character_Dancer publisher Soho_Press English_language is the language in Great_Britain and the United_States . A_Loyal_Character_Dancer was published by Soho_Press in the United_States .
relative_adverb United_States ethnicGroup Native_Americans Alcatraz_Versus_the_Evil_Librarians is from The United_States where one of the ethnic groups are the Native_Americans . The English_language is spoken in the United_States as well as Great_Britain .
relative_adverb United_States ethnicGroup Asian_Americans Asian_Americans are one of the ethnic groups in the United_States of which A_Fortress_of_Grey_Ice is also , the English_language is spoken in both the United_States and in Great_Britain .
coordinated_full_clauses United_States ethnicGroup African_Americans The African_Americans are an ethnic group in the United_States where A_Severed_Wasp originates . The language of both the United_States and Great_Britain is English_language .
direct_object United_States ethnicGroup Native_Americans A_Loyal_Character_Dancer is published in the United_States where Native_Americans are an ethnic group . The English_language is the national language of Great_Britain and the United_States .
direct_object United_States ethnicGroup Asian_Americans English_language is spoken in Great_Britain and the United_States . The book Alcatraz_Versus_the_Evil_Librarians comes from the United_States where one of the ethnic groups is Asian_Americans .
direct_object United_States ethnicGroup African_Americans English_language is spoken in Great_Britain and the United_States . A_Fortress_of_Grey_Ice is from the United_States where one of the ethnic groups are African_Americans .
apposition Viking_Press parentCompany Penguin_Random_House The book A_Long_Long_Way ( which was followed by The_Secret_Scripture ) , was published by Viking_Press of the United_States . Viking_Press has the parent company Penguin_Random_House .
existential School_of_Business_and_Social_Sciences_at_the_Aarhus_University affiliation European_University_Association The School_of_Business_and_Social_Sciences_at_the_Aarhus_University in Denmark was established in 1928 . It is affiliated with the European_University_Association and has 737 academic staff .
existential AWH_Engineering_College state Kerala AWH_Engineering_College of "Kuttikkattoor" , Kerala , India was established in the year 2001 .
existential School_of_Business_and_Social_Sciences_at_the_Aarhus_University country Denmark The School_of_Business_and_Social_Sciences_at_the_Aarhus_University in Aarhus , Denmark has 737 academic staff and 16000 students .
passive_voice Accademia_di_Architettura_di_Mendrisio academicStaffSize 100 Accademia_di_Architettura_di_Mendrisio in Mendrisio , Switzerland has an academic staff of 100 providing educational services to 600 students .
existential Accademia_di_Architettura_di_Mendrisio location Ticino The Accademia_di_Architettura_di_Mendrisio is located in Ticino , Mendrisio , Switzerland . It has 600 students .
direct_object School_of_Business_and_Social_Sciences_at_the_Aarhus_University affiliation European_University_Association An affiliate of the European_University_Association , the School_of_Business_and_Social_Sciences_at_the_Aarhus_University in Aarhus , Denmark has 16000 students enrolled .
passive_voice School_of_Business_and_Social_Sciences_at_the_Aarhus_University country Denmark The School_of_Business_and_Social_Sciences_at_the_Aarhus_University , Denmark ( established 1928 ) has an academic staff of 737 and a student roll of 16000 .
apposition Acharya_Institute_of_Technology country "India" The Acharya_Institute_of_Technology is located in the city of Bangalore in the state of Karnataka , "India" . It is affiliated with Visvesvaraya_Technological_University .
passive_voice Akron_Summit_Assault ground St_Vincent–St_Mary_High_School The ground of Akron_Summit_Assault is at St_Vincent–St_Mary_High_School in Akron_,_Ohio , Summit_County_,_Ohio , United_States .
passive_voice School_of_Business_and_Social_Sciences_at_the_Aarhus_University country Denmark The School_of_Business_and_Social_Sciences_at_the_Aarhus_University , Denmark , has 16000 students , 737 academic staff and is affiliated with the European_University_Association .
existential School_of_Business_and_Social_Sciences_at_the_Aarhus_University established 1928 The School_of_Business_and_Social_Sciences_at_the_Aarhus_University ( established 1928 in the city of Aarhus ) is affiliated with the European_University_Association which has its headquarters in Brussels .
relative_subject Acharya_Institute_of_Technology country "India" The Acharya_Institute_of_Technology in "India" has 700 post - graduate students and is affiliated with the Visvesvaraya_Technological_University in Belgaum .
relative_subject Alfa_Romeo_164 class "Mid-size_luxury_/_Executive_car" The Alfa_Romeo_164 is considered a "Mid-size_luxury_/_Executive_car" and has the alternative name of "Alfa_Romeo_168" . It has a "5-speed_manual" transmission and a Straight-four_engine .
relative_subject Plymouth_Plaza successor Plymouth_Satellite The Dodge manufactured 1955_Dodge and the DeSoto_Custom and the Plymouth_Plaza are related means of transportation . The Plymouth_Plaza was succeeded by the Plymouth_Satellite .
relative_subject DeSoto_Custom successor DeSoto_Firedome The 1955_Dodge , the DeSoto_Custom ( later succeeded by the DeSoto_Firedome ) and the Plymouth_Plaza are related means of transportation . The latter vehicle was made in Plymouth_(automobile) .
passive_voice DeSoto_Custom successor DeSoto_Firedome The Dodge manufactured 1955_Dodge and the DeSoto_Custom and the Chrysler_Newport are related means of transportation . The DeSoto_Custom ' s successor is the DeSoto_Firedome .
passive_voice A-Rosa_Luna topSpeed 24.0 The Cruise_ship A-Rosa_Luna is 125800.0_millimetres long , has a top speed of 24.0 and a displacement of 1850.0_tonnes .
relative_subject A-Rosa_Luna shipClass Cruise_ship The Cruise_ship A-Rosa_Luna is 125800.0_millimetres long , has a top speed of 24.0 and a displacement of 1850.0_tonnes .
existential Caterpillar_Inc foundationPlace California AIDA_Cruises are located in Germany and own the AIDAluna which is powered by the California founded Caterpillar_Inc engine .
direct_object AIDAstella builder Meyer_Werft The AIDAstella was built by Meyer_Werft . It is owned by Costa_Crociere and operated by AIDA_Cruises Line based in Rostock .
possessif AIDAluna shipBeam 32.2 The AIDAluna has a ship beam of 32.2 and is 252000.0_millimetres long . It is powered by Caterpillar_Inc and made the maiden voyage 2009-03-22 .
relative_subject AIDAstella topSpeed 38.892 AIDAstella is 253260.0_millimetres long and has a top speed of 38.892 . It was built by Meyer_Werft and is operated by AIDA_Cruises .
coordinated_clauses ALCO_RS-3 length 17068.8_millimetres The ALCO_RS-3 has a V12_engine and a length of 17068.8_millimetres . It was produced between "May_1950_-_August_1956" by the American_Locomotive_Company .
possessif ALCO_RS-3 cylinderCount 12 Produced between "May_1950_-_August_1956" , the ALCO_RS-3 has a V12_engine , with a cylinder count of 12 and was manufactured by the American_Locomotive_Company .
existential ALCO_RS-3 engine V12_engine The ALCO_RS-3 was built by the Montreal_Locomotive_Works , has 12 cylinders , a V12_engine and 17068.8_millimetres long .
apposition ALCO_RS-3 cylinderCount 12 The ALCO_RS-3 was built by the Montreal_Locomotive_Works and has 12 cylinders , a Four-stroke_engine and a length of 17068.8_millimetres .
existential ALCO_RS-3 cylinderCount 12 The ALCO_RS-3 , built by Montreal_Locomotive_Works , has a cylinder count of 12 , a V12_engine and a length of 17068.8_millimetres .
existential ALCO_RS-3 buildDate "May_1950_-_August_1956" The ALCO_RS-3 has a V12_engine and a length of 17068.8_millimetres . It was produced between "May_1950_-_August_1956" by the American_Locomotive_Company .
relative_subject ALCO_RS-3 powerType Diesel-electric_transmission The ALCO_RS-3 has a Four-stroke_engine and a Diesel-electric_transmission . It is 17068.8_millimetres long and was built by the American_Locomotive_Company .
relative_subject ALCO_RS-3 totalProduction 1418 The ALCO_RS-3 , built by the American_Locomotive_Company has a V12_engine and is 17068.8_millimetres long . Production time is 1418 .
passive_voice ALCO_RS-3 cylinderCount 12 The ALCO_RS-3 , built by Montreal_Locomotive_Works , has a cylinder count of 12 , a V12_engine and a length of 17068.8_millimetres .
coordinated_clauses ALCO_RS-3 engine Four-stroke_engine The ALCO_RS-3 has a Four-stroke_engine and a Diesel-electric_transmission . It is 17068.8_millimetres long and was built by the American_Locomotive_Company .
passive_voice ALCO_RS-3 engine V12_engine The ALCO_RS-3 , built by American_Locomotive_Company , has a V12_engine , a Diesel-electric_transmission and is 17068.8_millimetres .
passive_voice ALCO_RS-3 engine V12_engine The ALCO_RS-3 , built by the Montreal_Locomotive_Works , has a V12_engine , a Diesel-electric_transmission and is 17068.8_millimetres long .
passive_voice ALCO_RS-3 engine V12_engine The ALCO_RS-3 , built by the American_Locomotive_Company has a V12_engine and is 17068.8_millimetres long . Production time is 1418 .
coordinated_full_clauses United_States capital Washington ALV_X-1 originated in the United_States where Asian_Americans are an ethnic group . Washington is the capital of the United_States and the anthem is the The_Star-Spangled_Banner .
coordinated_full_clauses United_States anthem The_Star-Spangled_Banner ALV_X-1 originated in the United_States where Asian_Americans are an ethnic group . Washington is the capital of the United_States and the anthem is the The_Star-Spangled_Banner .
possessif United_States demonym Americans The Atlas_II is from the United_States , where one of the ethnic groups is African_Americans . Washington is the capital of the United_States and the inhabitants are called Americans .
relative_adverb United_States anthem The_Star-Spangled_Banner ALV_X-1 hails from the United_States , where Native_Americans are one of the ethnic groups . Washington is the capital of the United_States and The_Star-Spangled_Banner is the anthem .
coordinated_full_clauses United_States capital Washington The Atlas_II is from the United_States , where one of the ethnic groups is African_Americans . Washington is the capital of the United_States and the inhabitants are called Americans .
apposition United_States leaderTitle President_of_the_United_States The Atlas_II is from the United_States , where the people are Americans and African_Americans are among the ethnic groups . The leader of the United_States is the President_of_the_United_States .
coordinated_clauses AMC_Matador class Mid-size_car The AMC_Matador , alternatively known as the "American_Motors_Matador" was assembled in Kenosha_,_Wisconsin . It is a Mid-size_car and has an AMC_straight-6_engine .
existential AMC_Matador class Mid-size_car The Mid-size_car AMC_Matador , or "American_Motors_Matador" , is assembled in Thames_,_New_Zealand and has an AMC_V8_engine .
coordinated_clauses AMC_Matador bodyStyle Coupé The alternative name for AMC_Matador is "VAM_Classic" and it is assembled in Thames_,_New_Zealand . The AMC_Matador is a Coupé and has an AMC_straight-6_engine .
possessif AMC_Matador class Mid-size_car The AMC_Matador is a Mid-size_car also known as "American_Motors_Matador" and was assembled in Thames_,_New_Zealand with an AMC_straight-6_engine .
coordinated_clauses Abarth foundedBy Carlo_Abarth The Abarth foundation was founded in Italy by Carlo_Abarth . It manufactured the Abarth_1000_GT_Coupé which was designed by Gruppo_Bertone .
existential Abarth foundedBy Carlo_Abarth The Abarth_1000_GT_Coupé was designed by Gruppo_Bertone and made by Abarth . The Abarth foundation was founded in Italy by Carlo_Abarth .
coordinated_clauses Abarth foundedBy Carlo_Abarth Abarth , which was founded by Carlo_Abarth , in Bologna , is the manufacturer of the Abarth_1000_GT_Coupé . The Abarth_1000_GT_Coupé was designed by Gruppo_Bertone .
direct_object Abarth foundedBy Carlo_Abarth Abarth was founded by Carlo_Abarth in Bologna . It is the manufacturer of the Abarth_1000_GT_Coupé which was designed by Gruppo_Bertone .
coordinated_clauses Gruppo_Bertone locationCity Turin Gruppo_Bertone , who designed the Abarth_1000_GT_Coupé , is located in Turin . It was founded in Italy by Giovanni_Bertone .
coordinated_clauses Abarth_1000_GT_Coupé engine Straight-four_engine The Abarth_1000_GT_Coupé is a "Two_door_coupé" model with a Straight-four_engine and a wheelbase of 2160.0_millimetres . It rolled off the production line in 1958 .
passive_voice Honda division Acura Honda is the makes the Acura_TLX which possesses a V6_engine and is relative to the Honda_Accord . Honda has a division called Acura .
relative_adverb Finland demonym Finns Aleksey_Chirikov_(icebreaker) was built in Helsinki , Finland . Finland is led by Juha_Sipilä and the demonym is Finns .
relative_object Finland demonym Finns The icebreaker ship Aleksey_Chirikov_(icebreaker) was built in Helsinki , Finland . Juha_Sipilä is the leader of Finland and the people of Finland who are known as Finns .
apposition Aleksey_Chirikov_(icebreaker) status "In_service" The in "In_service" Aleksey_Chirikov_(icebreaker) was built at the Arctech_Helsinki_Shipyard . in Finland . It has a ship beam of 21.2 m .
apposition Aleksey_Chirikov_(icebreaker) shipBeam 21.2 Aleksey_Chirikov_(icebreaker) was built in Helsinki , in Finland . It is in "In_service" and has a ship beam of 21.2 m .
relative_subject AMC_Matador assembly Thames_,_New_Zealand The Mid-size_car AMC_Matador , or "American_Motors_Matador" , is assembled in Thames_,_New_Zealand and has an AMC_V8_engine .
relative_subject AMC_Matador assembly Thames_,_New_Zealand AMC_Matador is also known as "American_Motors_Matador" and is assembled in Thames_,_New_Zealand . It is classed as a Mid-size_car and it has an AMC_straight-6_engine .
existential Alfa_Romeo_164 bodyStyle Sedan_(automobile) The Luxury_vehicle Class , Alfa_Romeo_164 Sedan_(automobile) , known as the "Alfa_Romeo_168" , has a Straight-four_engine .
relative_subject Aston_Martin_V8 engine 5.3_litres The Aston_Martin_RHAM/1 , Aston_Martin_V8 and Aston_Martin_DBS are a related means of transport . It is assembled in the United_Kingdom and has a 5.3_litres engine .
apposition Alfa_Romeo_164 engine Straight-four_engine The Alfa_Romeo_164 , assembled in Italy , has a Straight-four_engine and is relative to the Saab_9000 and Lancia_Thema .