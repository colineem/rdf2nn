# Informations
Aims: Web Semantic Data and corpus generation

## Description
The dataset is here : 
[triples and texts annotated, xml format](https://gitlab.inria.fr/colineem/rdf2nn/blob/master/prepareCorpus/webnlgparser_data/working_data.xml)

## Prérequis construction données de travail ##
| Librairies      | version                                                                                                                              |
|-----------------|--------------------------------------------------------------------------------------------------------------------------------------|
| Python          | 3.5.2                                                                                                                                |
| BeautifulSoup   | beautifulsoup4==4.5.3 : embellisement XML en vue lecture humaine                                                                     |
| StanfordCoreNLP | pycorenlp==0.3.0 : interrogation locale serveur java StanforCoreNLP, le serveur est interrogé via un wrapper python (data.py)        |
| xml.etree       | Parcours et génération XML, inclus dans python3                                                                                      |
| pygraphviz      | pygraphviz==1.3.1 : construction et parcous de graphes, extractabilité de ceux-ci sous forme graphique                               |
| json            | inclus dans python3, version '2.0.9' : stockage objets pythons (données)                                                             |
| glob            | inclus dans python3 : parcours récursif d'une arborescence                                                                           |
| unicodedata     | inclus dans python3 : traitements sur caractères spéciaux

# WebNLGChallenge - entrées #
Fichiers utilisés : `data/crowdflower/benchmark_verified_manually_corrected/*/*.xml`<br/>

La structure des fichiers est construite ainsi :<br/>

```xml
    <?xml version='1.0' encoding='UTF-8'?>
    <benchmark>
      <entries>
        <entry category="Astronaut" eid="Id8" size="1">
          <originaltripleset>
            <otriple>Alan_Bean | status | "Retired"@en</otriple>
          </originaltripleset>
          <modifiedtripleset>
            <mtriple>Alan_Bean | status | "Retired"</mtriple>
          </modifiedtripleset>
          <lex comment="good" jobid="954293" lid="1">Alan Bean is retired.</lex>
          <lex comment="good" jobid="954293" lid="2">Alan Bean has retired.</lex>
          <lex comment="good" jobid="904853" lid="3">Alan Bean is now retired.</lex>
        </entry>
        [...]
      </entries>
    </benchmark>
```

# WebNLGChallenge - sorties #
## initialized_data.py ##
Nous parcourons les entrées (entries), prélevons les triplets définis dans <entry/modifiedtripleset/mtriple> et y associons les lexicalisations qui ont pour attribut commentaire "good"]
Chaque entrée se définie par un eid qui est propre au fichier en cours (numérotation reprise à zéro à chaque changement de fichier). Les lexicalisations sont repérée par un lid. Ce trio d'informations (nom fichier/eid/lid) est conservé tout au long des procédures pour retrouver les petits.<br/>

Ce travail est long. Il peut être accéléré au prix de pertes en minimisant la diversité de l'ordonnancement de la présentation des entités à trouver. Une entité peut être trouvée 0 (souci de mauvaise lexicalisation) à n fois. Elle peut être présente sous forme proche, suffisemment peu distincte pour être identifiée, très distinctes mais prévue (ex: Uk pour United Kingdown, courant pour notre corpus, sera identifié). Une entité peut être exploitée comme sous-partie d'une autre (ex: "Prime Minister of United Kingdom" peut être à rechercher parallèlement à "United Kingdom" pour uen même lexicalisation). Face à la diversité des situations, un système de roulement de l'ordre dans lequel les entités sont recherchées a été mis en place, au-delà d'un système de fenêtre pour rechercher les expressions très simlaires (proximité orthographique maximale partant du principe optimiste mais au cahier des charges que l'on devrait trouver la structure. Ce choix est cadré par une exigence : un taux de similarité expérimentalement établi à 0.88. Le ratio de proximité est calculé à l'aide de difflib, inclu dans les librairies stantard python (Ratcliff and Obershelp algorithm).

Les phrases correctement parsées sont stockées et relancer l'application `initialized_data.py` ne les perd pas.

Les lexicalisations sont analysées. Le triplet est décrit par les parties suivantes : `[sujet] | [relation] | [objet]`. Nous voulons trouver comment l'information se réalise, autrement dit quelle forme prend la relation pour un sujet et un objet donné.<br/>
Traitements :<br/>
   - repérer où se situe le sujet et l'objet dans la réalisation.
     --> `échecs stockés dans un fichier data/crowdflower/entities_not_found.xml`<br/>
La structure du fichier `entities_not_found` est la suivante:<br/>

```xml
<root>
 <file path="/home/emilie/data/webnlgChallenge/data2text/data/crowdflower/benchmark_verified_manually_corrected/2triples/2triples_Astronaut_958863_BMK_cleaned.xml">
  <entry category="Astronaut" eid="Id1" size="2">
   <tripleset>
    <triple>Alan_Bean | birthDate | "1932-03-15"</triple>
    <triple>Alan_Bean | almaMater | "UT Austin, B.S. 1955"</triple>
   </tripleset>
   <lex>Alan Bean was born on March 15, 1932 and received a Bachelor of Science degree at the University of Texas at Austin in 1955.</lex>
   <lex>Alan Bean was born on the 15th of March 1932 and graduated from UT Austin in 1955 with a B.S.</lex>
   <lex>Alan Bean (born on 1932-03-15) graduated from UT Austin in 1955.</lex>
  </entry>
  [...]
 </file>
 [...]
</root>
```
   - anonymiser la phrase en vue d'une généralisation : remplacement de chaque concept sujet et objet par un identifiant. Les concepts sont numérotés par ordre de rencontre, et sont préfixé par "xlid", postfixés par "xrid".
   - traiter les phrases originelles en analyse en dépendances (et en analyse en constituants). Les phrases rencontrants des problèmes majeurs d'analyse sont écartées. Ex: dans "Jens Härtel is in the 1. FC Magdeburg club." n'est pas repéré comme une entité. La phrase est considérée comme étant une succession de deux phrases. La segmentation de l'entité "1. FC Magdeburg club" permet de repérer le souci. 
   - --> les échecs sont stockés dans `entities_not_correctly_parsed.xml`
La structure du fichier `entities_not_correctly_parsed.xml` est la suivante :<br/>

```xml
<root>
 <file name="2triples_Astronaut_958863_BMK_cleaned.xml">
  <eid name="Id220">
   <tripleset>
    <triple>Jens_Härtel | club | 1._FC_Magdeburg</triple>
   </tripleset>
   <tripleset_encoded>
    <triple>xlid1101xrid | club | xlid1098xrid</triple>
   </tripleset_encoded>
   <lex lid="2">
    <encoded>xlid1101xrid is in the xlid1098xrid club.</encoded>
    <analysed>Jens Härtel is in the 1. FC Magdeburg club.</analysed>
   </lex>
   <lex lid="3">
    <encoded>xlid1101xrid plays for xlid1098xrid.</encoded>
    <analysed>Jens Härtel is in the 1. FC Magdeburg club.</analysed>
   </lex>
  </eid>
  [...]
 </file>
 [...]
</root>
```

Afin de pouvoir travailler en déconnecté et de limiter l'usage de l'analyseur en dépendance, les retours de corenlp sont stockés dans un dictionnaire python, qui à chaque phrase parsées fait correspondre le tableau json contenant les réponses du serveur. Une clé est ajoutée en cas de réponse allégée.<br/>
CoreNLP défaille parfois dans le traitement des coréférences (corefs). L'analyse lui est demandée une première fois avec corefs. Si elle échoue, l'analyse est redemandée sans corefs. La clé 'corefs_error' est conséquemment ajoutée au dictionnaire fourni pour la phrase par pycorenlp, et est alors positionnée à True (que l'on sache ultérieurement qu'il y a eu un échec et que des coréférences ne se sont pas vvues analysées. L'API de coreNLP détaille la structure json employée (https://stanfordnlp.github.io/CoreNLP/corenlp-server.html#api-documentation). pycorenlp n'est qu'un wrapper transférant dans un objet dictionnaire python l'ensemble des données issues de l'analyse, tel quelle (cf https://github.com/smilli/py-corenlp).<br/>
Les retours de corenlp sont stockés dans "stanford.json", la structure du dictionnaire associe à charge entrée (chaque lexicalisation parsée) le json renvoyé par le serveur : un dictionnaire dont les clés sont corefs, sentences, et corefs_error (ajouté et dont la valeur est "True" si l'analyse des co-références n'a pas eu lieu).<br/>

Les données de stanford sont couplées aux lexicalisations. Un fichier entities_parsed.xml et output.xml sont produits (le second sans utilisation de BeautifulSoup dont sans indentation ni passage à la ligne, il est plus léger et plus facile à charger, mais guère affichable). L'analyse de 12163 lexicalisations sur les 21458 initialement offertes aux traitements (12 573 avec entités toutes identifiables, les dernières phrases éliminées le sont pour des motivations liées au parsing).<br/>
La structure du fichier correspond à celle de cet exemple:<br/>

```xml
<root>
 <file name="2triples_Astronaut_958863_BMK_cleaned.xml">
  <eid name="Id1">
   <tripleset>
    <triple tid="0">Alan_Bean | birthDate | "1932-03-15"</triple>
    <triple tid="1">Alan_Bean | almaMater | "UT Austin, B.S. 1955"</triple>
   </tripleset>
   <tripleset_encoded>
    <triple tid="0">
     xlid0xrid | birthDate | xlid1xrid</triple>
    <triple tid="1">
     xlid0xrid | almaMater | xlid2xrid</triple>
   </tripleset_encoded>
   <lexicalisation coref="analyzed" lid="3">
    <encoded>xlid0xrid was born on the xlid1xrid and his Alma Mater is xlid2xrid.</encoded>
    <analysed>Alan Bean was born on the "1932-03-15" and his Alma Mater is "UT Austin, B.S. 1955".</analysed>
    <sentences nombre="1">
     <sentence sid="0">
      <triplets>
       <triplet tid="0"/>
       <triplet tid="1"/>
      </triplets>
      <graphe>
       strict digraph {
	17	 [pos=NNP, word=Austin];
	4	 [pos=VBN, word=born];
	17 -> 4 [key=csubj];
	14 [pos=VBZ, word=is];
	17 -> 14 [key=cop];
	15 [pos=OTHERS, word="\""];
	17 -> 15 [key=punct];
	16 [pos=NNP, word=UT];
	17 -> 16 [key=compound];
	18 [pos=OTHERS, word=","];
	17 -> 18 [key=punct];
	19 [pos=NNP, word="B.S."];
	17 -> 19 [key=appos];
	21 [pos=OTHERS, word="\""];
	17 -> 21 [key=punct];
	22 [pos=OTHERS, word="."];
	17 -> 22 [key=punct];
	0 -> 17 [key=ROOT];
	2 [pos=NNP, word=Bean];
	4 -> 2 [key=nsubjpass];
	3 [pos=VBD, word=was];
	4 -> 3 [key=auxpass];
	8 [pos=CD, word="1932-03-15"];
	4 -> 8 [key="nmod:on"];
	13 [pos=NNP, word=Mater];
	4 -> 13 [key="nmod:on"];
	20 [pos=CD, word=1955]; 
	19 -> 20 [key=nummod];
	1 [pos=NNP, word=Alan];
	2 -> 1 [key=compound];
	8 -> 13 [key="conj:and"];
	5 [pos=IN, word=on];
	8 -> 5 [key=case];
	6 [pos=DT, word=the];
	8 -> 6 [key=det];
	7 [pos=OTHERS, word="\""];
	8 -> 7 [key=punct];
	9 [pos=OTHERS, word="\""];
	8 -> 9 [key=punct];
	10 [pos=CC, word=and];
	8 -> 10 [key=cc];
	11 [pos="PRP$", word=his];
	13 -> 11 [key="nmod:poss"];
	12 [pos=NNP, word=Alma];
	13 -> 12 [key=compound];
      }
      </graphe>
      <ROOT idx="0">
       <embeddedTriples>
        <triple id="xlid0xrid">
         <item idx="1" ner="PERSON" pos="NNP"/>
         <item idx="2" ner="PERSON" pos="NNP"/>
        </triple>
        <triple id="xlid1xrid">
         <item idx="7" ner="O" pos="OTHERS"/>
         <item idx="8" ner="DATE" pos="CD"/>
         <item idx="9" ner="O" pos="OTHERS"/>
        </triple>
        <triple id="xlid0xrid">
         <item idx="11" ner="O" pos="PRP$"/>
        </triple>
        <triple id="xlid2xrid">
         <item idx="15" ner="O" pos="OTHERS"/>
         <item idx="16" ner="O" pos="NNP"/>
         <item idx="17" ner="LOCATION" pos="NNP"/>
         <item idx="18" ner="O" pos="OTHERS"/>
         <item idx="19" ner="O" pos="NNP"/>
         <item idx="20" ner="DATE" pos="CD"/>
         <item idx="21" ner="O" pos="OTHERS"/>
        </triple>
       </embeddedTriples>
       <rootItems>
        <item idx="1" lemma="Alan" ner="PERSON" pos="NNP">Alan</item>
        <item idx="2" lemma="Bean" ner="PERSON" pos="NNP">Bean</item>
        <item idx="3" lemma="be" ner="O" pos="VBD">was</item>
        <item idx="4" lemma="bear" ner="O" pos="VBN">born</item>
        <item idx="5" lemma="on" ner="O" pos="IN">on</item>
        <item idx="6" lemma="the" ner="O" pos="DT">the</item>
        <item idx="7" lemma="``" ner="O" pos="OTHERS">"</item>
        <item idx="8" lemma="1932-03-15" ner="DATE" pos="CD">1932-03-15</item>
        <item idx="9" lemma="''" ner="O" pos="OTHERS">"</item>
        <item idx="10" lemma="and" ner="O" pos="CC">and</item>
        <item idx="11" lemma="he" ner="O" pos="PRP$">his</item>
        <item idx="12" lemma="Alma" ner="ORGANIZATION" pos="NNP">Alma</item>
        <item idx="13" lemma="Mater" ner="ORGANIZATION" pos="NNP">Mater</item>
        <item idx="14" lemma="be" ner="O" pos="VBZ">is</item>
        <item idx="15" lemma="``" ner="O" pos="OTHERS">"</item>
        <item idx="16" lemma="UT" ner="O" pos="NNP">UT</item>
        <item idx="17" lemma="Austin" ner="LOCATION" pos="NNP">Austin</item>
        <item idx="18" lemma="," ner="O" pos="OTHERS">,</item>
        <item idx="19" lemma="B.S." ner="O" pos="NNP">B.S.</item>
        <item idx="20" lemma="1955" ner="DATE" pos="CD">1955</item>
        <item idx="21" lemma="''" ner="O" pos="OTHERS">"</item>
        <item idx="22" lemma="." ner="O" pos="OTHERS">.</item>
       </rootItems>
      </ROOT>
     </sentence>
    </sentences>
   </lexicalisation>
   [...]
  </eid>
  [...]
 </file>
 [...]
</root>
```

Aux étiquettes de dialogue stanford (`POS_TAGS=["CC", "CD", "DT", "EX", "FW", "IN", "JJ", "JJR", "JJS", "LS", "MD", "NN", "NNS", "NNP", "NNPS", "PDT", "POS", "PRP", "PRP$", "RB", "RBR", "RBS", "RP", "SYM", "TO", "UH", "VB", "VBD", "VBG", "VBN", "VBP", "VBZ", "WDT", "WP", "WP$", "WRB"]`) a été ajouté "`OTHERS`" car les éléments non taggués sont renvoyés tels quels (symbole renvoyé plutôt qu'un tag pré-défini). 
La redondance est minimalisée mais pensée pour favoriser l'exploitabilité.


# Détection de structures #

La [détection de structure](https://gitlab.inria.fr/colineem/rdf2nn/raw/master/doc/syntactical_rules.pdf) est relativement simple. 

```python
import rules
```

Les classes permettant de parser les graphes précédemment décrits se trouvent dans le fichier `rules.py`. Notre parser de graphe (`webnlgparser.py`) se dote d'un système de règles:

```python
	#création d'un système de règles
        self.system=rules.System()
        #on va créer nos règles

	#description d'une règle
        #relative subject
        rule_relativeSubject = rules.System.Rule(name="relative_subject")

	#noeud du graphe à considérer
        #soit x un noeud pos_taggué V.*
        x = rule_relativeSubject.add_node(pos='V.*')
        #soit y un noeud quelconque
        y = rule_relativeSubject.add_node()

	#on ajoute les liens décrivant la relation 
        #soit l1 le lien unissant y à x, de nature 'acl:relcl' : y est uni à x pour une relation de type relative clause
        #y a une relative clause dont l'évènement est x
        l1 = rule_relativeSubject.add_link(y, x, 'acl:relcl')
        #soit l2 le lien unissant x à y, de nature 'nsubj' : x a pour sujet y
        #x a pour sujet y
        l2 = rule_relativeSubject.add_link( x, y, 'nsubj.*')

	#et on ajoute la règle (les variables l1/l2 sont des pointeurs décoratifs)
        self.system.add_rule(rule_relativeSubject)

```

La création de règles se fait à partir de trois fonctions de base:
* `[system].<add_node(pos='', word='')>`, avec:
  * pos=[regex part-of-speech ou [`''`|`'*'`] si tout accepté]
  * word=[regex valeur_mot  ou [`''`|`'*'`] si tout accepté]
* `[system].<add_link(départflèche, arriveflèche, étiquetteflèche='')>`, avec:
  * départflèche, arriveflèche : des nœuds
  * étiquette_flèche=[nom relation | regex nom relation + `.*`| `''`/vide si tout lien accepté] ]
* `[system].<forbid_link(départflèche, arriveflèche, étiquetteflèche='')>`, avec:
  * départflèche, arriveflèche : des nœuds
  * étiquette_flèche=[nom relation | regex nom relation + `.*`| `''`/vide si tout lien refusé] ]
* `[system].<set_position(left_node, right_node)>`, avec:
  * left_node, right_node : des nœuds situés l'un par rapport à l'autre

<br/>

Un système de règles peut être doté d'un nombre libre de règles. Une fois existant, il peut s'appliquer à un graphe `pygraphviz` sous sa forme textuelle.

```python
	for graphe in graphes:
        #récupération dans le tableau `constraints` de la liste des noms de règles réalisables dans le graphe.
        constraints = self.system.apply_rules(graphe.text)
```

La détection de structure est détaillée ici : [https://gitlab.inria.fr/colineem/rdf2nn/raw/master/doc/syntactical_rules.pdf](https://gitlab.inria.fr/colineem/rdf2nn/raw/master/doc/syntactical_rules.pdf).

